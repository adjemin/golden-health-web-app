<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EntrepriseValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('entreprise_values')->insert([
        //     [
        //         ''
        //     ]
        // ]);

        DB::insert("INSERT INTO `entreprise_values` (`cover_url`, `description`, `title`, `entreprise_id`) VALUES
        ('https://goldenhealth.adjemincloud.com/customer/images/empathy.jpg', 'Votre santé et votre bien être, c’est notre succès', 'Empathie', 1),
        ('https://goldenhealth.adjemincloud.com/customer/images/passion.jpg', 'Nous sommes guidés par l’amour du sport et du bien-être', 'Passion', 1),
        ('https://goldenhealth.adjemincloud.com/customer/images/teamwork.jpg', 'Tous nous sommes plus intelligents que n\'importe lequel d\'entre nous. \r\n\r\nProverbe japonais.', 'Esprit d’équipe', 1),
        ('https://goldenhealth.adjemincloud.com/customer/images/professionalism.jpg', 'Avec éthique et intégrité, nous prenons à cœur la qualité du service fourni et l’excellence dans notre travail', 'Professionnalisme', 2),
        ('https://goldenhealth.adjemincloud.com/customer/images/innovation.jpg', 'À travers les nouvelles technologies, nous voulons rendre le bien être accessible à tous', 'Innovation', 1),
        ('https://goldenhealth.adjemincloud.com/customer/images/professionalism.jpg', 'Nous prenons à cœur la qualité du service fourni et l’excellence dans notre travail', 'Professionnalisme', 1);");
    }
}
