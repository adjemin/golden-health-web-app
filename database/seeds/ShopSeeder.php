<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("INSERT INTO `categories` (`name`, `slug`, `created_at`, `updated_at`, `deleted_at`)
        VALUES
        ('Sport', 'sport', '2020-09-21 19:05:16', '2020-09-21 19:05:16', NULL),
        ('Accessoires', 'accesoires', '2020-09-21 19:05:16', '2020-09-21 19:05:16', NULL),
        ('Exercice et remise en forme', 'exerice-et-remise-en-forme', '2020-09-21 19:05:16', '2020-09-21 19:05:16', NULL),
        ('Golf', 'golf', '2020-09-21 19:05:16', '2020-09-21 19:05:16', NULL),
        ('Natation', 'natation', '2020-09-21 19:05:16', '2020-09-21 19:05:16', NULL),
        ('Sports d\'equipe', 'sports-d-equipe', '2020-09-21 19:05:16', '2020-09-21 19:05:16', NULL)
        ;");
        //
        DB::insert("INSERT INTO
        `products` (`title`, `description`, `description_metadata`, `price`, `original_price`, `fees`, `currency_slug`, `old_price`, `has_promo`, `promo_percentage`, `medias`, `is_sold`, `sold_at`, `initial_count`, `published_at`, `is_published`, `slug`, `link`, `delivery_services`, `created_at`, `updated_at`, `deleted_at`)
        VALUES
        ('QUECHUA by decathlon SAC A DOS NH100 10L KAKI', 'Pratique sportive  RANDONNEE A LA JOURNEE NOMBRE DE POCHE  2 POCHES AVEC HOUSSE DE PLUIE  NON VOLUME  10 L OPTIONS 12h FRÉQUENCE  2 x 3 DURÉE  2 Heures        couleurs garantie  10 Années Comment est mesuré le volume ?  Le volume de chaque sac à dos est mesuré selon une méthode standardisée : nous utilisons des petites balles pour remplir le compartiment principal ainsi que chacune des poches. Nous vidons ensuite les balles dans un container gradué qui nous donne alors le volume équivalent en litres.   Comment régler votre sac ?  1 - Desserrez toutes les sangles (bretelles, ceinture, rappels de charge) 2 - Enfilez le sac 3 - Serrez les sangles des bretelles 4 - Ajustez les rappels de charge à votre convenance         Testé en laboratoire  Nous faisons plusieurs tests en laboratoire pour valider les composants du sac : tissus, boucles, sangles, mousses, zips.', NULL, 3700, 22000, 500, 'XOF', NULL, NULL, NULL, 'https://ci.jumia.is/unsafe/fit-in/500x500/filters:fill(white)/product/21/360241/1.jpg?2076', NULL, NULL, NULL, NULL, '1', 'QUECHUA-by-decathlon-SAC-A-DOS-NH100-10L-KAKI', NULL, NULL, '2020-09-21 17:54:15', '2020-09-21 17:54:15', NULL),

        ('Baskets De Sport - Semelle Compensée - Noir', 'Description du produit:
        Genre : Femme Type de produit: Baskets Type de motif : couleur unie
        Occasion : Occasionnel, quotidien Style : élégant et décontracté
        Matériel de revêtement: tissu Un matériau unique
        Matériel interne Appliquez la bonne taille et passez à la taille normale. Haute qualité, fond doux', NULL, 10900, 15000, 500, 'XOF', NULL, NULL, NULL, 'https://ci.jumia.is/unsafe/fit-in/500x500/filters:fill(white)/product/30/125631/1.jpg?8536', 1, NULL, NULL, NULL, '1', 'Baskets-De-Sport-Semelle-Compensée-Noir', NULL, NULL, '2020-09-21 22:48:04', '2020-09-21 22:48:04', NULL),

        ('Baskets - Style Décontracté - Rose', 'Cette paire de chaussures est une nouvelle version !contrôlé minutieusement avant l\'expédition.
        Un beau sac.
        Réponse rapide à vos questions.
        Description du produit :
        Genre : FemmeType de produit: Baskets Motif : couleur unieOccasion : Occasionnel, quotidienStyle : style élégant et décontractéAppliquez la bonne taille et passez à la taille normale.Haute qualité, fond doux  Caractéristiques : Légère et souple léger et confortable comme une bande de roulement en coton ;Sec et confortable : le tissu est respirant et les pieds sont aérésSûr et sécurisé : les lignes concaves et convexes de la semelle sont antidérapantes et résistantes à l\'usure ;', NULL, 10600, 15000, 500, 'XOF', NULL, NULL, NULL, 'https://ci.jumia.is/unsafe/fit-in/500x500/filters:fill(white)/product/20/125631/1.jpg?8608', 1, NULL, NULL, NULL, '1', 'Baskets-Style-Décontracté-Rose', NULL, NULL, '2020-09-21 22:54:29', '2020-09-21 22:54:29', NULL),

        ('Sac Banane Homme Femme Ceinture De Course Sport Sacoche Sac à La Taille Pochette Running Belt Avec Bande à Réflecteur- Noir', 'Matériau étanche : le matériau en néoprène est résistant à l\'eau, même si vous courez sous la pluie, la neige, la boue, il protège vos affaires de l\'eau, et l\'intérieur du sac de courses est toujours sec.Et à l’arrière, il est fait en des matériaux de plongée, ce qui le rend durable, robuste et léger.', NULL, 4500, 15000, 500, 'XOF', NULL, NULL, NULL, 'https://ci.jumia.is/unsafe/fit-in/300x300/filters:fill(white)/product/27/759041/1.jpg?1462', 1, NULL, NULL, NULL, '1', 'Sac-Banane-Homme-Femme-Ceinture-De-Course-Sport-Sacoche-Sac-à-La-Taille-Pochette-Running-Belt-Avec-Bande-à-Réflecteur-Noir', NULL, NULL, '2020-09-21 22:54:48', '2020-09-21 22:54:48', NULL),

        ('Genouillère Articulée', 'Notre attelle de genou articulée vous offre la meilleure expérience de port. Cela vous permet de vous éloigner des blessures aux articulations et de dire adieu à la douleur. Vous pouvez avoir et profiter librement de tous les sports.

        Cette genouillère articulée sera le cadeau le plus intime pour vos parents, vos maris ou femmes, vos copines ou copains et autres que vous aimez à Noël, anniversaire et autres festivals.

        Nous conseillons de laver la genouillère avec un détergent neutre après une utilisation quotidienne.
        Après le lavage, aérez la genouillère au soleil, s\'il vous plaît.', NULL, 3500, 15000, 500, 'XOF', NULL, 0, '10', 'https://ci.jumia.is/unsafe/fit-in/500x500/filters:fill(white)/product/42/036831/1.jpg?9171', 1, NULL, NULL, NULL, '1', 'Genouillère-Articuculpa', NULL, NULL, '2020-09-21 22:56:31', '2020-09-21 22:56:31', NULL),

        ('Revoflex Xtrem Équipement D\'Exercice Pour Musculation Abdominale & Remise En Forme', '« C’est décidé, je dois prendre des muscles et entretenir ma plastique» ! Si c’est ce que vous avez
        Nous disposons d’articles de musculations (Vélos, Haltères, Barre de Torsion etc….), pour vous aider à avoir la silhouette de vos rêves.

        Par contre, si dernièrement vous avez prévu, vous êtes surement à la bonne adresse. pris du poids et que vous avez besoin de perdre des calories, les tee-shirts de sudation et les machines à Abdos feront l’affaire..', NULL, 13000, 15000, 500, 'XOF', NULL, 0, '10', 'https://ci.jumia.is/unsafe/fit-in/500x500/filters:fill(white)/product/42/494721/1.jpg?2693', 1, NULL, NULL, NULL, '1', 'Revoflex-Xtrem-Équipement-D\'Exercice-Pour-Musculation-Abdominale-&-Remise-En-Forme', NULL, NULL, '2020-09-21 22:56:59', '2020-09-21 22:56:59', NULL),

        ('Hand Grip - Poignée De Musculation - Noir/Rouge', 'Rassurez-vous mesdames, vous n’êtes pas en marge. Notre sélection d’articles couvre tous les âges et tous les besoins. Vous serez au top !', NULL, 1700, 33000, NULL, 'XOF', NULL, 0, '25', 'https://ci.jumia.is/unsafe/fit-in/500x500/filters:fill(white)/product/28/909311/1.jpg?3254', 1, NULL, '0', NULL, '1', 'Hand Grip - Poignée-De-Musculation-Noir/Rouge', 'https://adjem.in/pBEK', NULL, '2020-09-21 22:58:38', '2020-09-21 23:00:43', NULL),

        ('Tapis De Gym/Yoga/Sport', 'Distinctio. Quidem f.', NULL, 8348, 33000, NULL, 'XOF', NULL, 0, '25', 'https://ci.jumia.is/unsafe/fit-in/500x500/filters:fill(white)/product/03/564331/1.jpg?1392', 1, NULL, '0', NULL, '1', 'Tapis-De-Gym/Yoga/Sport', 'https://adjem.in/pBEK', NULL, '2020-09-21 22:58:38', '2020-09-21 23:00:43', NULL),

        ('Tapis De Yoga Pour Exercices De Sport', 'La description:100% tout neuf et de haute qualitéPeut exercer vos abdominaux, vos bras, vos jambes et votre dosSerrez vos abdominauxGardez vos collants et fesses mincesDéveloppez vos épaules et votre dosIdéal pour les exercices de yoga et de PilatesParticulièrement bénéfique pour «l\'étirement en toute sécurité»Peut améliorer l\'amplitude des mouvements et la flexibilitéPeut se laver à l\'eau ou essuyer avec un chiffon humidePosture stable et contrôle de l\'extension de la distance, améliore efficacement l\'activité physiqueFaçonner la courbe corporelle parfaite est le meilleur accès aux fournitures auxiliaires de yoga et de Pilates.Peut augmenter le plaisir de l\'exercice, changer la façon dont un seul exercice.Rassemblez avec une bonne résilience, et entraînez-vous le yoga dans l\'action avec facilité, pour atteindre un meilleur objectif plastique.Adapté aux forces relativement petites des jeunes et des femmes, étirement efficace et exercice des muscles du corps entierMatériel: latex naturelScènes applicables: corps de fitness, équipement de fitness, massage de santé, sports de courseTaille: 1800 mm * 150mm * 0,35 mmCouleur bleuCaractéristiques:Serrez vos abdominauxGardez vos collants et fesses mincesDéveloppez vos épaules et votre dosIdéal pour les exercices de yoga et de PilatesPeut améliorer l\'amplitude des mouvements et la flexibilitéPeut se laver à l\'eau ou essuyer avec un chiffon humideEmballage inclus:1 x bande extensible de yogaRemarques:En raison de la différence entre les différents moniteurs, les images peuvent ne pas refléter la couleur réelle de l\'article.Comparez les tailles de détail avec les vôtres, veuillez permettre une erreur de 1 à 3 cm, en raison de la mesure manuelle.Veuillez laisser un message avant de donner la mauvaise rétroaction, si les produits ont des problèmes.Merci pour votre compréhension.', NULL, 3500, 33000, 1000, 'XOF', NULL, 0, '25', 'https://ci.jumia.is/unsafe/fit-in/500x500/filters:fill(white)/product/41/379831/1.jpg?1482', 1, NULL, '0', NULL, '1', 'Tapis-De-Yoga-Pour-Exercices-De-Sport', 'https://adjem.in/pBEK', NULL, '2020-09-21 22:58:38', '2020-09-21 23:00:43', NULL),

        ('Ballon De Basketball Molten En Cuir', 'Le ballon Molten Basketball GL7 en  tant que ballon de basket  officiel de la FIBA, la série Molten BGL Basketball a acquis une reconnaissance internationale et est acclamé comme un premier choix par les athlètes d\'élite du monde entier. Attendez-vous à des performances exceptionnelles et haut de gamme avec la technologie de basket-ball avancée de Molten; La surface de galets plats du BGL offre une adhérence et un contrôle comme vous ne l\'avez jamais vu, tandis que les coutures entièrement plates procurent une rotation arrière uniforme pour des tirs et des passes stables et précis, où que vous teniez la balle. Le GIUGIARO® ou \'G-Design\' de Molten améliore la visibilité et vous permet de vous concentrer sur le mouvement de la balle. Poussez les performances à la limite avec Molten GL Basketball.
        Veuillez noter que le ballon de basket-ball GL7 de Molten est un ballon de basket tout en cuir qui nécessite une période de rodage pour un jeu consistant avant de développer une sensation plus douce avec une jouabilité optimale.', NULL, 12000, 33000, NULL, 'XOF', NULL, 0, '25', 'https://ci.jumia.is/unsafe/fit-in/500x500/filters:fill(white)/product/84/883721/1.jpg?5145', 1, NULL, '0', NULL, '1', 'Ballon-De-Basketball-Molten-En-Cuir', 'https://adjem.in/pBEK', NULL, '2020-09-21 22:58:38', '2020-09-21 23:00:43', NULL),

        ('SPORT Lunette De Natation Avec Effet Brillant - Noir', '', NULL, 3725, 33000, NULL, 'XOF', NULL, 0, '25', 'https://ci.jumia.is/unsafe/fit-in/500x500/filters:fill(white)/product/28/544241/1.jpg?0728', 1, NULL, NULL, NULL, '1', 'SPORT-Lunette-De-Natation-Avec-Effet-Brillant-Noir', NULL, NULL, '2020-09-21 22:59:52', '2020-09-21 23:00:31', '2020-09-21 23:00:31')

        ;");
    }
}