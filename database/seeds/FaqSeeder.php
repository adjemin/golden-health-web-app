<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('faqs')->insert([
            [
                'question' => "Comment fonctionne TrainMe ?",
                'answer' => "
                    <p></p><p>TrainMe est une plateforme de réservation en ligne de coach sportif en France. Notre Service vous permet de réserver en moins de 2 minutes un coach sportif proche de chez vous de manière régulière ou ponctuelle et ce sans engagement ni abonnement.</p><ol><li>Renseignez votre adresse, votre sport et cliquez «&nbsp;GO »</li><li>Choisissez le coach qui vous convient selon les critères suivants&nbsp;:<ul><li>Prix</li><li>Localisation</li><li>Votre créneau</li><li>Objectif du cours (Initiation, Maintien de la forme, Remise en forme, Performances)</li><li>Lieu de l’entrainement</li><li>Ect…</li></ul></li><li>Sélectionnez un créneau</li><li>Payez en toute sécurité</li><li>Profitez d'un cours de sport de qualité</li></ol>
                "
            ],
            [
                'question' => "Comment réserver un coach ?",
                'answer' => "
                    1. Sélectionnez un Coach
                    Commencez par identifier un Coach en effectuant une recherche en indiquant votre sport et votre localité. Nous vous montrerons la liste des Coachs disponibles. Sélectionnez un Coach pour voir ses prestations.

                    2. Choisissez votre créneau

                    En sélectionnant un Coach, vous arriverez sur son profil. Vous y trouverez les créneaux proposés et les créneaux disponibles par le Coach. Cliquez sur le créneau de votre choix.

                    3. Réservez et réglez

                    Continuez les étapes en indiquant vos coordonnées (numéro de téléphone, adresse etc...), renseigner le lieu où vous souhaitez pratiquer et régler la prestation en ligne.
                "
            ],
            [
                'question' => "J'ai perdu mon identifiant ou mon mot de passe, comment puis-je les récupérer ?",
                'answer' => "
                    Si vous avez oublié votre mot de passe, il suffit d´entrer à nouveau votre adresse e-mail et nous vous enverrons un nouveau mot de passe.
                "
            ]
        ]);
    }
}
