<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EntrepriseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('entreprise')->insert(
            [
                'name' => 'Golden Health',

                'logo_url' => 'https://goldenhealth.adjemincloud.com/customer/images/gh1.png',
                'phone' => '77720507',
                'facebook_url' => NULL,
                'twitter_url' => NULL,
                'instagram_url' => NULL,
                'bio' =>
                    'La plateforme de Golden Health a pour but de promouvoir et d’encourager une pratique régulière et responsable du sport dans les ménages ivoiriens. Elle vise également à augmenter le nombre d’adepte à moyen et long terme afin d’assister à un recul des maladies chroniques (Hypertension artérielle, Diabètes etc…) qui ont gagnées du terrain en Côte d’Ivoire au cours de ces dernières années. Elle est portée par l’innovation, la qualité du service, l’esprit d\'équipe, la rigueur dans le travail , l\'intégrité, l’empathie et surtout la satisfaction complète du client.',
                'email' => 'ny.goldenhealth@gmail.com',
                'description' => NULL,
                'footer_description' =>
                    'Golden Health est une plateforme qui propose des services dans le domaine du bien-être par le sport. Elle met en relation des coach sportifs et des personnes désirant pratiquer une ou plusieurs discipline(s) sportive(s)'
                ,
                'catchphrase' => NULL,
                'whatsapp_number' => NULL,
                'created_at' => '2020-10-06 00:00:00',
                'updated_at' => NULL,
            ],
        );
    }
}