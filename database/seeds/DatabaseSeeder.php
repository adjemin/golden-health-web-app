<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        $this->call(DBSeeder::class);
        // $this->call(EntrepriseSeeder::class);
        // $this->call(EntrepriseValueSeeder::class);
        // $this->call(EventSeeder::class);
        // $this->call(FaqSeeder::class);
        // $this->call(ShopSeeder::class);
    }
}
