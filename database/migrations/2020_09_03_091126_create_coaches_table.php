<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoachesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coaches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('customer_id')->nullable();
            $table->text('bio')->nullable();
            $table->string('rating')->nullable();
            $table->string('experience')->nullable();
            $table->integer('qualification_id')->nullable();
            $table->string('facebook_url')->nullable();
            //$table->string('files')->nullable();
            $table->string('instagram_url')->nullable();
            $table->string('youtube_url')->nullable();
            $table->string('site_web')->nullable();
            $table->string('discovery_source')->nullable();
            $table->integer('dscipline_id')->nullable();
            $table->boolean('is_active')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //$table->foreign('qualification_id')->references('id')->on('qualifications');
            //$table->foreign('customer_id')->references('id')->on('customers');
            //$table->foreign('dscipline_id')->references('id')->on('disciplines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coaches');
    }
}
