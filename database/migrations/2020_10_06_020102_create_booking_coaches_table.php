<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingCoachesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_coaches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('booking_ref');
            $table->integer('course_id');
            $table->integer('customer_id');
            $table->integer('pack_id');
            $table->integer('nb_personne');
            $table->string('amount');
            $table->string('booking_date');
            $table->string('booking_time');
            $table->integer('course_lieu_id');
            $table->text('course_lieu_note');
            $table->string('address_name');
            $table->string('address_lat');
            $table->string('address_lon');
            $table->string('address_cpl');
            $table->integer('quantity');
            $table->boolean('coach_confirm');
            $table->integer('status');
            $table->string('status_name');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_coaches');
    }
}
