<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NavigatorSessionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('navigator_sessions', function (Blueprint $table) {
            $table->id();
            $table->string('uniqid')->nullable();
            $table->string('ip')->nullable();
            $table->string('mac')->nullable();
            $table->json('page_visited')->nullable();
            $table->boolean('is_admin')->nullable();
            $table->boolean('is_challenger')->nullable();
            $table->boolean('is_coach')->nullable();
            $table->boolean('is_visitor')->nullable();
            $table->string('provider_connection')->nullable();
            $table->string('provider_connection')->nullable();
            $table->json('provider_connection')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('navigator_sessions');
    }
}
