<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecapBMIsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recap_b_m_is', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('description')->nullable();
            $table->text('interval')->nullable();
            $table->string('color')->nullable();
            $table->double('weigth')->nullable();
            $table->double('height')->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
           // $table->foreign('customer_id')->references('id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recap_b_m_is');
    }
}
