<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoachDisciplinesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coach_disciplines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('coach_id')->nullable();
            $table->integer('discipline_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //$table->foreign('coach_id')->references('id')->on('coaches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coach_disciplines');
    }
}
