<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->string('address_name')->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->string('place')->nullable();
            $table->string('type_building')->nullable();
            $table->string('photo')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //$table->foreign('customer_id')->references('id')->on('customers');
            //$table->foreign('coach_id')->references('id')->on('coaches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addresses');
    }
}
