<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('name_en')->nullable();
            $table->string('slug')->nullable();
            $table->text('description')->nullable();
            $table->text('description_en')->nullable();
            $table->string('logo')->nullable();
            $table->string('price')->nullable();
            $table->string('price_en')->nullable();
            $table->string('price_description')->nullable();
            $table->string('price_description_en')->nullable();
            $table->boolean('price_is_variable')->nullable();
            $table->string('currency')->nullable();
            $table->timestamp('delay')->nullable();
            $table->timestamp('delay_en')->nullable();
            $table->timestamp('delay_time')->nullable();
            $table->timestamp('delay_time_unit')->nullable();
            $table->integer('rank')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('services');
    }
}
