<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackSubscriptionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pack_subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pack_id')->nullable();
            $table->bigInteger('course_id')->nullable();
            $table->integer('discipline_id')->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->bigInteger('coach_id')->nullable();
            $table->text('session_days')->nullable()->comment('["2020-09-02 18:00:00-19:00:00"] un tableau de date');
            $table->text('sessions_completed')->nullable()->comment('["2020-09-02 18:00:00-19:00:00"] un tableau de date');
            $table->text('sessions_waiting')->nullable()->comment('["2020-09-02 18:00:00-19:00:00"] un tableau de date');
            $table->text('sessions_failed')->nullable()->comment('["2020-09-02 18:00:00-19:00:00"] un tableau de date');
            $table->boolean('is_completed')->default(false);
            $table->timestamps();
            $table->softDeletes();
          //  $table->foreign('pack_id')->references('id')->on('packs');
            //$table->foreign('customer_id')->references('id')->on('customers');
            //$table->foreign('coach_id')->references('id')->on('coaches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pack_subscriptions');
    }
}
