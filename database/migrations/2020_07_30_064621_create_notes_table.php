<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('customer_id')->nullable();
            // $table->bigInteger('order_id')->nullable();
            $table->bigInteger('course_id')->nullable();
            $table->index('customer_id');
            $table->index('course_id');
            $table->text('message')->nullable();
            $table->integer('rate')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //$table->foreign('customer_id')->references('id')->on('customers');
            //$table->foreign('order_id')->references('id')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notes');
    }
}
