<?php

use App\Customer;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('last_name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('name')->nullable();
            $table->string('dial_code')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('phone')->nullable();
            $table->boolean('is_phone_verifed')->nullable();
            $table->timestamp('phone_verifed_at')->nullable();
            $table->boolean('is_active')->nullable();
            $table->timestamp('activation_date')->nullable();
            $table->timestamp('email_verifed_at')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('photo_url')->nullable()->default(Customer::DEFAULT_PHOTO_URL ?? "https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png");
            $table->unsignedBigInteger('photo_id')->nullable();
            $table->string('gender')->nullable();
            $table->double('size')->nullable(); // height***************
            $table->double('weight')->nullable();
            $table->double('target_weight')->nullable();
            $table->string('bio')->nullable();
            $table->timestamp('birthday')->nullable(); // dob (date of birth) *******
            $table->string('birth_location')->nullable();
            $table->string('language')->nullable();
            $table->string('seance_price')->nullable();
            $table->string('location_address')->nullable();
            $table->string('location_latitude')->nullable();
            $table->string('experience')->nullable();
            $table->string('location_longitude')->nullable();
            $table->string('location_gps')->nullable();
            $table->string('password')->nullable();
            $table->string('token')->nullable();
            $table->string('remember_token')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('google_id')->nullable();
            $table->string('type_account', 50)->nullable()->default("challenger");
            $table->boolean('is_cgu')->nullable();
            $table->boolean('is_newsletter')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
