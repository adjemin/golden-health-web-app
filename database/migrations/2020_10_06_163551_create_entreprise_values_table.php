<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntrepriseValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entreprise_values', function (Blueprint $table) {
            $table->id();
            $table->string("cover_url")->nullable();
            $table->string("description")->nullable(); // just a few words
            $table->string("title")->nullable(); //  even fewer words
            $table->unsignedBigInteger('entreprise_id')->default(1); // basically the only entreprise is goldenhealth
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entreprise_values');
    }
}