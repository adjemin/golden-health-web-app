<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->index('order_id');
            $table->index('customer_id');
            $table->string('reference')->unique();
            $table->string('service')->nullable();
            $table->string('link')->nullable();
            $table->string('subtotal')->nullable();
            $table->double('tax')->nullable();
            $table->double('discount')->nullable()->default(0);
            $table->integer('fees_delivery')->nullable();
            $table->double('total')->nullable();
            $table->string('status')->nullable()->default('unpaid');
            $table->boolean('is_paid')->default(false);
            $table->boolean('is_paid_by_customer')->nullable();
            $table->boolean('is_paid_by_delivery_service')->nullable();
            $table->string('currency_code')->nullable()->default("XOF");
            $table->string('currency_name')->nullable()->default("CFA");
            $table->timestamps();
            $table->softDeletes();
            //$table->foreign('order_id')->references('id')->on('orders');
            //$table->foreign('customer_id')->references('id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}