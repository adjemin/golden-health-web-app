<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortfoliosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('coach_id')->nullable();
            $table->string('title')->nullable();
            // $table->text('images_url')->nullable();
            // $table->string('video_url')->nullable();
            $table->string('media_type')->nullable();
            $table->string('media_path')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //$table->foreign('coach_id')->references('id')->on('coaches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('portfolios');
    }
}
