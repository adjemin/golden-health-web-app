<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_tickets', function (Blueprint $table) {
            $table->id();
            $table->string("code")->nullable();
            $table->integer("seats_count")->nullable();
            $table->double('amount')->nullable();
            $table->string('status')->nullable()->default("unpaid");
            $table->boolean('is_used')->nullable()->default(false);
            $table->boolean('is_active')->nullable()->default(false);
            $table->boolean('is_blocked')->nullable()->default(false);
            $table->boolean('is_cancelled')->nullable()->default(false);
            $table->boolean('is_paid')->nullable()->default(false);
            $table->timestamp('expires_in')->nullable();
            $table->unsignedBigInteger('event_id')->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->index('event_id');
            $table->index('customer_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_tickets');
    }
}