<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->text('description_metadata')->nullable();
            $table->double('price')->nullable();
            $table->double('original_price')->nullable();
            $table->double('fees')->nullable();
            $table->string('currency_slug')->nullable();
            $table->string('old_price')->nullable();
            $table->boolean('has_promo')->nullable();
            $table->string('promo_percentage')->nullable();
            $table->text('medias')->nullable()->default("https://goldenhealth.adjemincloud.com/customer/images/860b5c3.jpg");
            $table->boolean('is_sold')->nullable();
            $table->timestamp('sold_at')->nullable();
            $table->integer('initial_count')->nullable();
            $table->integer('stock')->nullable(); // How many of it is available rn
            $table->string('published_at')->nullable();
            $table->string('is_published')->nullable();
            $table->string('slug')->nullable();
            $table->string('link')->nullable();
            $table->text('delivery_services')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}