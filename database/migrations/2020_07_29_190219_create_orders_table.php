<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('customer_id')->nullable();
            $table->string('status')->nullable()->default("waiting");
            $table->string('amount')->nullable()->default(0);
            $table->string('currency_code')->nullable()->default('XOF');
            $table->string('currency_name')->nullable()->default('CFA');
            $table->string('payment_method_slug')->nullable()->default('online')->comment('online or cash');
            $table->string('delivery_fees')->nullable();
            $table->boolean('is_waiting')->nullable()->default(true);
            $table->timestamp('delivery_date')->nullable();
            $table->boolean('is_delivery')->nullable();
            $table->string('service_slug')->nullable()->comment('shop or coaching or event');
            $table->string('location_name')->nullable(); 
            $table->string('location_latitude')->nullable(); 
            $table->string('location_longitude')->nullable();  
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}