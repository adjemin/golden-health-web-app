<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->nullable();
            $table->string('service_slug')->nullable()->comment('shop or coaching or event');
            $table->bigInteger('meta_data_id')->nullable();
            $table->text('meta_data')->nullable();
            $table->integer('quantity')->nullable()->default(1);
            $table->string('unit_price')->nullable()->default("0");
            $table->string('total_amount')->nullable()->default("0");
            $table->string('currency_code')->nullable()->default("XOF");
            $table->string('currency_name')->nullable()->default("CFA");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_items');
    }
}