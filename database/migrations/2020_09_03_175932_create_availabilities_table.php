<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvailabilitiesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('availabilities', function (Blueprint $table) {
            $table-> bigIncrements('id');
            $table->string('date_debut')->nullable();
            $table->string('date_fin')->nullable();
            //$table->timestamp('day_time')->nullable();
            $table->bigInteger('coach_id')->nullable();
            //$table->string('day')->nullable()->comment('Monday 08:00-09:00');
            //$table->string('hour');
            //$table->boolean('statut');
            //$table->boolean('is_confirmed');
            $table->timestamps();
            $table->softDeletes();
            //$table->foreign('coach_id')->references('id')->on('coaches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('availabilities');
    }
}
