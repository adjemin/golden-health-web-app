<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->string('cover_url')->nullable()->default(asset('images/event.png'));
            $table->timestamp('date_event')->nullable();
            $table->timestamp('date_event_end')->nullable();
            $table->double('entry_fee')->nullable();
            $table->string('venue')->nullable();
            $table->text('description')->nullable();
            $table->integer('place_number')->nullable();
            $table->unsignedBigInteger('event_type_id')->nullable();
            $table->index('event_type_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
