<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id');
            $table->string('experience');
            $table->string('seance_type');
            $table->boolean('is_diplome');
            $table->integer('nb_person');
            //$table->boolean('is_formation');
            //$table->boolean('has_particulier');
            //$table->boolean('has_collectif');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('course_details');
    }
}
