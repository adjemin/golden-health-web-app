<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("shopping_cart_id")->nullable();
            $table->index("shopping_cart_id");
            $table->unsignedBigInteger("product_id")->nullable();
            $table->index("product_id");
            $table->string("color")->nullable();
            $table->string("size")->nullable();
            $table->double("price")->nullable();
            $table->integer("quantity")->nullable()->default(1);
            $table->double("amount")->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_items');
    }
}