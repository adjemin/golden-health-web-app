<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicePaymentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('invoice_id')->nullable();
            $table->string('payment_method')->nullable()->default("online");
            $table->string('payment_reference')->nullable();
            $table->string('amount')->nullable()->default("0");
            $table->string('currency_code')->nullable()->default('XOF');
            $table->bigInteger('creator_id')->nullable();
            $table->string('creator_name')->nullable();
            $table->string('creator')->nullable()->default("customers")->commment("customers or users");
            $table->string('status')->nullable();
            $table->boolean('is_waiting')->nullable()->default(true);
            $table->boolean('is_completed')->nullable()->default(false);
            $table->string('payment_gateway_trans_id')->nullable();
            $table->string('payment_gateway_custom')->nullable();
            $table->string('payment_gateway_currency')->nullable();
            $table->string('payment_gateway_amount')->nullable();
            $table->string('payment_gateway_payid')->nullable();
            $table->string('payment_gateway_payment_date')->nullable();
            $table->string('payment_gateway_payment_time')->nullable();
            $table->string('payment_gateway_payment_message')->nullable();
            $table->string('payment_gateway_payment_method')->nullable();
            $table->string('payment_gateway_payment_phone_prefixe')->nullable();
            $table->string('payment_gateway_payment_cel_phone_num')->nullable();
            $table->string('payment_gateway_ipn_ack')->nullable();
            $table->string('payment_gateway_created_at')->nullable();
            $table->string('payment_gateway_updated_at')->nullable();
            $table->string('payment_gateway_cpm_result')->nullable();
            $table->string('payment_gateway_trans_status')->nullable();
            $table->text('payment_gateway_designation')->nullable();
            $table->string('payment_gateway_buyer_name')->nullable();
            $table->string('payment_gateway_signature')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //$table->foreign('invoice_id')->references('id')->on('invoices');
           // $table->foreign('creator_id')->references('id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoice_payments');
    }
}
