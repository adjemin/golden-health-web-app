<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntrepriseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Entreprise représente les infos de base de l'entreprise
        // Comme le numéro les valeurs, la description etc
        Schema::create('entreprise', function (Blueprint $table) {
            $table->id();
            $table->string('whatsapp_number')->nullable();
            $table->string('logo_url')->nullable();
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('instagram_url')->nullable();
            $table->text('bio')->nullable();
            $table->string('email')->nullable();
            $table->text('description')->nullable();
            $table->text('footer_description')->nullable();
            $table->string('catchphrase')->nullable();
            $table->text('terms')->nullable()->comment("Termes et conditions d'utilisation en format html");
            $table->text('privacy')->nullable()->comment("Politique de confidentialité");
            $table->text('cookies')->nullable()->comment("Gestion cookies");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entreprise');
    }
}