<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('coach_id')->nullable();
            $table->integer('discipline_id')->nullable();
            // $table->integer('pack_id')->nullable();
            // $table->string('title')->nullable();
            //$table->string('person_number')->nullable();
            //$table->text('description')->nullable();
            //$table->text('experience')->nullable();
            $table->string('price')->nullable()->default("5000");
            $table->string('currency_code')->nullable()->default("XOF");
            $table->string('currency_name')->nullable()->default("CFA");
            $table->boolean('active')->nullable();
            $table->string('course_type')->nullable();
           //$table->string('type_course')->nullable();
            $table->timestamps();
            $table->softDeletes();
           // $table->foreign('pack_id')->references('id')->on('packs');
           // $table->foreign('discipline_id')->references('id')->on('disciplines');
            //$table->foreign('coach_id')->references('id')->on('coaches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('courses');
    }
}
