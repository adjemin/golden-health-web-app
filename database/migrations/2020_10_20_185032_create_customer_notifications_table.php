<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerNotificationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('title_en')->nullable();
            $table->string('subtitle')->nullable();
            $table->string('subtitle_en')->nullable();
            $table->string('action')->nullable();
            $table->string('action_by')->nullable();
            $table->text('meta_data')->nullable();
            $table->string('type_notification')->nullable();
            $table->boolean('is_read');
            $table->boolean('is_received');
            $table->text('data')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('data_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_notifications');
    }
}
