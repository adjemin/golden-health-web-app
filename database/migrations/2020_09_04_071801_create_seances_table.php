<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeancesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('discipline_id')->nullable();
            $table->integer('coach_id')->nullable();
            $table->string('duration')->nullable();
            $table->string('bilan');
            $table->string('week_duration');
            $table->string('time_hour_by_day');
            $table->string('objectif_long_terme');
            $table->string('objectif_court_terme');
            $table->timestamps();
            $table->softDeletes();
           // $table->foreign('discipline_id')->references('id')->on('disciplines');
           // $table->foreign('coach_id')->references('id')->on('coaches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seances');
    }
}
