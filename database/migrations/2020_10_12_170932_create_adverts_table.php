<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adverts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('cover_url')->nullable();
            $table->text('description')->nullable();
            $table->string('redirect_to')->nullable();
            $table->integer('priority')->nullable()->comment("Order de priorité de l'annonce - de 1 à 10 : 10 high priority, 1 low priority");
            $table->unsignedBigInteger('partner_id')->nullable();
            $table->index('partner_id');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adverts');
    }
}