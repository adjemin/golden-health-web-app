<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CourseObjective;
use Faker\Generator as Faker;

$factory->define(CourseObjective::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'slug' => $faker->word,
        'description' => $faker->text,
        'active' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
