<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CoachDiscipline;
use Faker\Generator as Faker;

$factory->define(CoachDiscipline::class, function (Faker $faker) {

    return [
        'coach_id' => $faker->randomDigitNotNull,
        'discipline_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
