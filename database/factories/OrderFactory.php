<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {

    return [
        'customer_id' => $faker->randomDigitNotNull,
        'status' => $faker->word,
        'amount' => $faker->word,
        'currency_code' => $faker->word,
        'payment_method_slug' => $faker->word,
        'delivery_fees' => $faker->word,
        'is_waiting' => $faker->word,
        'delivery_date' => $faker->date('Y-m-d H:i:s'),
        'is_delivery' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
