<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Address;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {

    return [
        'longitude' => $faker->word,
        'latitude' => $faker->word,
        'address_name' => $faker->word,
        'customer_id' => $faker->randomDigitNotNull,
        'coach_id' => $faker->randomDigitNotNull,
        'place' => $faker->word,
        'type_building' => $faker->word,
        'photo' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
