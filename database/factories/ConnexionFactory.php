<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Connexion;
use Faker\Generator as Faker;

$factory->define(Connexion::class, function (Faker $faker) {

    return [
        'username' => $faker->word,
        'email' => $faker->word,
        'phone' => $faker->word,
        'dial_code' => $faker->word,
        'phone_number' => $faker->word,
        'password' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
