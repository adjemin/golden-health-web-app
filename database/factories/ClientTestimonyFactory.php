<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ClientTestimony;
use Faker\Generator as Faker;

$factory->define(ClientTestimony::class, function (Faker $faker) {

    return [
        'client_name' => $faker->word,
        'client_photo_url' => $faker->text,
        'client_feedback' => $faker->text,
        'client_rating' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
