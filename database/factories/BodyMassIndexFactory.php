<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BodyMassIndex;
use Faker\Generator as Faker;

$factory->define(BodyMassIndex::class, function (Faker $faker) {

    return [
        'date' => $faker->date('Y-m-d H:i:s'),
        'value_BMI' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
