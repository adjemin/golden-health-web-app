<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Seance;
use Faker\Generator as Faker;

$factory->define(Seance::class, function (Faker $faker) {

    return [
        'discipline_id' => $faker->randomDigitNotNull,
        'coach_id' => $faker->randomDigitNotNull,
        'duration' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
