<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Advice;
use Faker\Generator as Faker;

$factory->define(Advice::class, function (Faker $faker) {

    return [
        'customer_id' => $faker->randomDigitNotNull,
        'coach_id' => $faker->randomDigitNotNull,
        'message' => $faker->text,
        'rate' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
