<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Availability;
use Faker\Generator as Faker;

$factory->define(Availability::class, function (Faker $faker) {

    return [
        'date_debut' => $faker->word,
        'date_fin' => $faker->word,
        'day_time' => $faker->date('Y-m-d H:i:s'),
        'coach_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
