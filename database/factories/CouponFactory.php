<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Coupon;
use Faker\Generator as Faker;

$factory->define(Coupon::class, function (Faker $faker) {

    return [
        'titre' => $faker->word,
        'code' => $faker->word,
        'porcentage' => $faker->randomDigitNotNull,
        'nbr_person' => $faker->randomDigitNotNull,
        'date_fin' => $faker->word,
        'image' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
