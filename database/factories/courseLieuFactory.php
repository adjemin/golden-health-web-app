<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\courseLieu;
use Faker\Generator as Faker;

$factory->define(courseLieu::class, function (Faker $faker) {

    return [
        'course_id' => $faker->randomDigitNotNull,
        'lieu_course_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
