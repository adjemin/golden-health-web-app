<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {

    return [
        'last_name' => $faker->word,
        'first_name' => $faker->word,
        'name' => $faker->word,
        'dial_code' => $faker->word,
        'phone_number' => $faker->word,
        'phone' => $faker->word,
        'is_phone_verifed' => $faker->word,
        'phone_verifed_at' => $faker->date('Y-m-d H:i:s'),
        'is_active' => $faker->word,
        'activation_date' => $faker->date('Y-m-d H:i:s'),
        'email' => $faker->word,
        'photo_url' => $faker->word,
        'gender' => $faker->word,
        'bio' => $faker->word,
        'birthday' => $faker->date('Y-m-d H:i:s'),
        'birth_location' => $faker->word,
        'language' => $faker->word,
        'location_address' => $faker->word,
        'location_latitude' => $faker->word,
        'location_longitude' => $faker->word,
        'location_gps' => $faker->word,
        'password' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
