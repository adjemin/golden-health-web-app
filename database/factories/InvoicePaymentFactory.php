<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\InvoicePayment;
use Faker\Generator as Faker;

$factory->define(InvoicePayment::class, function (Faker $faker) {

    return [
        'invoice_id' => $faker->randomDigitNotNull,
        'payment_method' => $faker->word,
        'payment_reference' => $faker->word,
        'amount' => $faker->word,
        'currency_code' => $faker->word,
        'creator_id' => $faker->randomDigitNotNull,
        'creator_name' => $faker->randomDigitNotNull,
        'creator_name' => $faker->word,
        'creator' => $faker->word,
        'status' => $faker->word,
        'status' => $faker->word,
        'is_waiting' => $faker->word,
        'is_completed' => $faker->word,
        'payment_gateway_trans_id' => $faker->word,
        'payment_gateway_custom' => $faker->word,
        'payment_gateway_currency' => $faker->word,
        'payment_gateway_amount' => $faker->word,
        'payment_gateway_payid' => $faker->word,
        'payment_gateway_payment_date' => $faker->word,
        'payment_gateway_payment_time' => $faker->word,
        'payment_gateway_payment_message' => $faker->word,
        'payment_gateway_payment_method' => $faker->word,
        'payment_gateway_payment_phone_prefixe' => $faker->word,
        'payment_gateway_payment_cel_phone_num' => $faker->word,
        'payment_gateway_ipn_ack' => $faker->word,
        'payment_gateway_created_at' => $faker->word,
        'payment_gateway_updated_at' => $faker->word,
        'payment_gateway_cpm_result' => $faker->word,
        'payment_gateway_trans_status' => $faker->word,
        'payment_gateway_designation' => $faker->word,
        'payment_gateway_buyer_name' => $faker->word,
        'payment_gateway_signature' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
