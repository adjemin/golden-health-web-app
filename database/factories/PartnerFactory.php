<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Partner;
use Faker\Generator as Faker;

$factory->define(Partner::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'email' => $faker->word,
        'dial_code' => $faker->word,
        'phone_number' => $faker->word,
        'phone' => $faker->word,
        'facebook_id' => $faker->word,
        'twitter_id' => $faker->word,
        'youtube_id' => $faker->word,
        'instagram' => $faker->word,
        'website' => $faker->word,
        'service_type' => $faker->randomDigitNotNull,
        'newsletter' => $faker->word,
        'photo_url' => $faker->word,
        'gender' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
