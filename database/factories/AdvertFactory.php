<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Advert;
use Faker\Generator as Faker;

$factory->define(Advert::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'cover_url' => $faker->word,
        'description' => $faker->text,
        'redirect_to' => $faker->word,
        'priority' => $faker->randomDigitNotNull,
        'partner_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
