<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Review;
use Faker\Generator as Faker;

$factory->define(Review::class, function (Faker $faker) {

    return [
        'customer_id' => $faker->word,
        'rating' => $faker->randomDigitNotNull,
        'title' => $faker->word,
        'content' => $faker->text,
        'service' => $faker->word,
        'source' => $faker->word,
        'source_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
