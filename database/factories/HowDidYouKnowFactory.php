<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\HowDidYouKnow;
use Faker\Generator as Faker;

$factory->define(HowDidYouKnow::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'name_en' => $faker->word,
        'user_source' => $faker->word,
        'user_id' => $faker->randomDigitNotNull,
        'description' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
