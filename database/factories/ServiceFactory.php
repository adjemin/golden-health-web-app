<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Service;
use Faker\Generator as Faker;

$factory->define(Service::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'name_en' => $faker->word,
        'slug' => $faker->word,
        'description' => $faker->text,
        'description_en' => $faker->text,
        'logo' => $faker->word,
        'price' => $faker->word,
        'price_en' => $faker->word,
        'price_description' => $faker->word,
        'price_description_en' => $faker->word,
        'price_is_variable' => $faker->word,
        'currency' => $faker->word,
        'delay' => $faker->date('Y-m-d H:i:s'),
        'delay_en' => $faker->date('Y-m-d H:i:s'),
        'delay_time' => $faker->date('Y-m-d H:i:s'),
        'delay_time_unit' => $faker->date('Y-m-d H:i:s'),
        'rank' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
