<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Invoice;
use Faker\Generator as Faker;

$factory->define(Invoice::class, function (Faker $faker) {

    return [
        'order_id' => $faker->randomDigitNotNull,
        'customer_id' => $faker->randomDigitNotNull,
        'reference' => $faker->word,
        'service' => $faker->word,
        'link' => $faker->word,
        'subtotal' => $faker->word,
        'tax' => $faker->word,
        'fees_delivery' => $faker->word,
        'total' => $faker->word,
        'status' => $faker->word,
        'is_paid_by_customer' => $faker->word,
        'is_paid_by_delivery_service' => $faker->word,
        'currency_code' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
