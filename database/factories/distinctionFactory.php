<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\distinction;
use Faker\Generator as Faker;

$factory->define(distinction::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'date_debut' => $faker->word,
        'date_fin' => $faker->word,
        'fichier_url' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
