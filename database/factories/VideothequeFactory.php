<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Videotheque;
use Faker\Generator as Faker;

$factory->define(Videotheque::class, function (Faker $faker) {

    return [
        'titre' => $faker->word,
        'description' => $faker->text,
        'link_youtube	string' => $faker->text,
        'video' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
