<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RecapBMI;
use Faker\Generator as Faker;

$factory->define(RecapBMI::class, function (Faker $faker) {

    return [
        'description' => $faker->text,
        'interval' => $faker->text,
        'color' => $faker->word,
        'weigth' => $faker->word,
        'height' => $faker->word,
        'customer_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
