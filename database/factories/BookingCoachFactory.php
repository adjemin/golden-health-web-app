<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BookingCoach;
use Faker\Generator as Faker;

$factory->define(BookingCoach::class, function (Faker $faker) {

    return [
        'course_id' => $faker->randomDigitNotNull,
        'customer_id' => $faker->randomDigitNotNull,
        'pack_id' => $faker->randomDigitNotNull,
        'nb_personne' => $faker->randomDigitNotNull,
        'amount' => $faker->word,
        'booking_ref' => $faker->word,
        'course_lieu_id' => $faker->randomDigitNotNull,
        'course_lieu_note' => $faker->text,
        'address_name' => $faker->word,
        'address_lat' => $faker->word,
        'address_lon' => $faker->word,
        'quantity' => $faker->randomDigitNotNull,
        'coach_confirm' => $faker->word,
        'status' => $faker->randomDigitNotNull,
        'status_name' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
