<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\courseDetails;
use Faker\Generator as Faker;

$factory->define(courseDetails::class, function (Faker $faker) {

    return [
        'course_id' => $faker->randomDigitNotNull,
        'experience' => $faker->word,
        'seance_type' => $faker->word,
        'is_diplome' => $faker->word,
        'is_formation' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
