<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {

    return [
        'order_id' => $faker->randomDigitNotNull,
        'status' => $faker->word,
        'creator' => $faker->word,
        'creator_id' => $faker->randomDigitNotNull,
        'creator_name' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
