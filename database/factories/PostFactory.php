<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {

    return [
        'title' => $faker->word,
        'description' => $faker->text,
        'slug' => $faker->word,
        'cover' => $faker->word,
        'status' => $faker->word,
        'is_published' => $faker->word,
        'author_id' => $faker->randomDigitNotNull,
        'category_id' => $faker->randomDigitNotNull,
        'shared_link' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
