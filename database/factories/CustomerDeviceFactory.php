<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CustomerDevice;
use Faker\Generator as Faker;

$factory->define(CustomerDevice::class, function (Faker $faker) {

    return [
        'customer_id' => $faker->word,
        'firebase_id' => $faker->word,
        'device_id' => $faker->word,
        'device_model' => $faker->word,
        'device_os' => $faker->word,
        'device_os_version' => $faker->word,
        'device_model_type' => $faker->word,
        'device_meta_data' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
