<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {

    return [
        'title' => $faker->word,
        'description' => $faker->text,
        'description_metadata' => $faker->text,
        'price' => $faker->text,
        'original_price' => $faker->text,
        'fees' => $faker->word,
        'currency_slug' => $faker->word,
        'old_price' => $faker->word,
        'has_promo' => $faker->word,
        'promo_percentage' => $faker->word,
        'medias' => $faker->text,
        'is_sold' => $faker->word,
        'sold_at' => $faker->date('Y-m-d H:i:s'),
        'initiale_count' => $faker->word,
        'published_at' => $faker->word,
        'is_published' => $faker->word,
        'slug' => $faker->word,
        'link' => $faker->word,
        'delivery_services' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
