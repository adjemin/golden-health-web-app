<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Experience;
use Faker\Generator as Faker;

$factory->define(Experience::class, function (Faker $faker) {

    return [
        'experience' => $faker->word,
        'title' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
