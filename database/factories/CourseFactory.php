<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {

    return [
        'pack_id' => $faker->randomDigitNotNull,
        'title' => $faker->word,
        'person_number' => $faker->word,
        'description' => $faker->text,
        'price' => $faker->word,
        'type_course' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
