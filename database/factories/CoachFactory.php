<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Coach;
use Faker\Generator as Faker;

$factory->define(Coach::class, function (Faker $faker) {

    return [
        'last_name' => $faker->word,
        'first_name' => $faker->word,
        'name' => $faker->word,
        'dial_code' => $faker->word,
        'phone_number' => $faker->word,
        'phone' => $faker->word,
        'email' => $faker->word,
        'is_phone_verifed' => $faker->word,
        'phone_verifed_at' => $faker->date('Y-m-d H:i:s'),
        'is_active' => $faker->word,
        'activation_date' => $faker->date('Y-m-d H:i:s'),
        'photo_id' => $faker->randomDigitNotNull,
        'photo_url' => $faker->word,
        'gender' => $faker->word,
        'bio' => $faker->text,
        'birthday' => $faker->date('Y-m-d H:i:s'),
        'birth_location' => $faker->word,
        'location_address' => $faker->word,
        'location_latitude' => $faker->word,
        'location_longitude' => $faker->word,
        'location_gps' => $faker->word,
        'rating' => $faker->word,
        'experience' => $faker->word,
        'qualification_id' => $faker->randomDigitNotNull,
        'facebook_id' => $faker->word,
        'twitter_id' => $faker->word,
        'youtube_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
