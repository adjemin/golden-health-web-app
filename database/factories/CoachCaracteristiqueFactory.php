<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CoachCaracteristique;
use Faker\Generator as Faker;

$factory->define(CoachCaracteristique::class, function (Faker $faker) {

    return [
        'title' => $faker->word,
        'image' => $faker->word,
        'description' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
