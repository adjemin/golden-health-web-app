<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CustomerNotification;
use Faker\Generator as Faker;

$factory->define(CustomerNotification::class, function (Faker $faker) {

    return [
        'title' => $faker->word,
        'title_en' => $faker->word,
        'subtitle' => $faker->word,
        'subtitle_en' => $faker->word,
        'action' => $faker->word,
        'action_by' => $faker->word,
        'meta_data' => $faker->text,
        'type_notification' => $faker->word,
        'is_read' => $faker->word,
        'is_received' => $faker->word,
        'data' => $faker->text,
        'user_id' => $faker->word,
        'data_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
