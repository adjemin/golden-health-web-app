<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Objective;
use Faker\Generator as Faker;

$factory->define(Objective::class, function (Faker $faker) {

    return [
        'course_id' => $faker->randomDigitNotNull,
        'principal_obj' => $faker->text,
        'listsecond_obj' => $faker->text,
        'description' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
