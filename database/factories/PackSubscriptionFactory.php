<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PackSubscription;
use Faker\Generator as Faker;

$factory->define(PackSubscription::class, function (Faker $faker) {

    return [
        'pack_id' => $faker->randomDigitNotNull,
        'customer_id' => $faker->randomDigitNotNull,
        'coach_id' => $faker->randomDigitNotNull,
        'is_active' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
