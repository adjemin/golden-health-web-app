<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pack;
use Faker\Generator as Faker;

$factory->define(Pack::class, function (Faker $faker) {

    return [
        'slug' => $faker->word,
        'name' => $faker->word,
        'description' => $faker->word,
        'quantity' => $faker->randomDigitNotNull,
        'quantity_fuel' => $faker->randomDigitNotNull,
        'price' => $faker->word,
        'image_url' => $faker->word,
        'currency_code' => $faker->word,
        'time_limit' => $faker->word,
        'time_limit_unit' => $faker->word,
        'has_delay' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
