<?php namespace Tests\Repositories;

use App\Models\Experience;
use App\Repositories\ExperienceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ExperienceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ExperienceRepository
     */
    protected $experienceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->experienceRepo = \App::make(ExperienceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_experience()
    {
        $experience = factory(Experience::class)->make()->toArray();

        $createdExperience = $this->experienceRepo->create($experience);

        $createdExperience = $createdExperience->toArray();
        $this->assertArrayHasKey('id', $createdExperience);
        $this->assertNotNull($createdExperience['id'], 'Created Experience must have id specified');
        $this->assertNotNull(Experience::find($createdExperience['id']), 'Experience with given id must be in DB');
        $this->assertModelData($experience, $createdExperience);
    }

    /**
     * @test read
     */
    public function test_read_experience()
    {
        $experience = factory(Experience::class)->create();

        $dbExperience = $this->experienceRepo->find($experience->id);

        $dbExperience = $dbExperience->toArray();
        $this->assertModelData($experience->toArray(), $dbExperience);
    }

    /**
     * @test update
     */
    public function test_update_experience()
    {
        $experience = factory(Experience::class)->create();
        $fakeExperience = factory(Experience::class)->make()->toArray();

        $updatedExperience = $this->experienceRepo->update($fakeExperience, $experience->id);

        $this->assertModelData($fakeExperience, $updatedExperience->toArray());
        $dbExperience = $this->experienceRepo->find($experience->id);
        $this->assertModelData($fakeExperience, $dbExperience->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_experience()
    {
        $experience = factory(Experience::class)->create();

        $resp = $this->experienceRepo->delete($experience->id);

        $this->assertTrue($resp);
        $this->assertNull(Experience::find($experience->id), 'Experience should not exist in DB');
    }
}
