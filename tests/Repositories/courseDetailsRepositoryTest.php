<?php namespace Tests\Repositories;

use App\Models\courseDetails;
use App\Repositories\courseDetailsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class courseDetailsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var courseDetailsRepository
     */
    protected $courseDetailsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->courseDetailsRepo = \App::make(courseDetailsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_course_details()
    {
        $courseDetails = factory(courseDetails::class)->make()->toArray();

        $createdcourseDetails = $this->courseDetailsRepo->create($courseDetails);

        $createdcourseDetails = $createdcourseDetails->toArray();
        $this->assertArrayHasKey('id', $createdcourseDetails);
        $this->assertNotNull($createdcourseDetails['id'], 'Created courseDetails must have id specified');
        $this->assertNotNull(courseDetails::find($createdcourseDetails['id']), 'courseDetails with given id must be in DB');
        $this->assertModelData($courseDetails, $createdcourseDetails);
    }

    /**
     * @test read
     */
    public function test_read_course_details()
    {
        $courseDetails = factory(courseDetails::class)->create();

        $dbcourseDetails = $this->courseDetailsRepo->find($courseDetails->id);

        $dbcourseDetails = $dbcourseDetails->toArray();
        $this->assertModelData($courseDetails->toArray(), $dbcourseDetails);
    }

    /**
     * @test update
     */
    public function test_update_course_details()
    {
        $courseDetails = factory(courseDetails::class)->create();
        $fakecourseDetails = factory(courseDetails::class)->make()->toArray();

        $updatedcourseDetails = $this->courseDetailsRepo->update($fakecourseDetails, $courseDetails->id);

        $this->assertModelData($fakecourseDetails, $updatedcourseDetails->toArray());
        $dbcourseDetails = $this->courseDetailsRepo->find($courseDetails->id);
        $this->assertModelData($fakecourseDetails, $dbcourseDetails->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_course_details()
    {
        $courseDetails = factory(courseDetails::class)->create();

        $resp = $this->courseDetailsRepo->delete($courseDetails->id);

        $this->assertTrue($resp);
        $this->assertNull(courseDetails::find($courseDetails->id), 'courseDetails should not exist in DB');
    }
}
