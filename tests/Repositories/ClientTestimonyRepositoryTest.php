<?php namespace Tests\Repositories;

use App\Models\ClientTestimony;
use App\Repositories\ClientTestimonyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ClientTestimonyRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ClientTestimonyRepository
     */
    protected $clientTestimonyRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->clientTestimonyRepo = \App::make(ClientTestimonyRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_client_testimony()
    {
        $clientTestimony = factory(ClientTestimony::class)->make()->toArray();

        $createdClientTestimony = $this->clientTestimonyRepo->create($clientTestimony);

        $createdClientTestimony = $createdClientTestimony->toArray();
        $this->assertArrayHasKey('id', $createdClientTestimony);
        $this->assertNotNull($createdClientTestimony['id'], 'Created ClientTestimony must have id specified');
        $this->assertNotNull(ClientTestimony::find($createdClientTestimony['id']), 'ClientTestimony with given id must be in DB');
        $this->assertModelData($clientTestimony, $createdClientTestimony);
    }

    /**
     * @test read
     */
    public function test_read_client_testimony()
    {
        $clientTestimony = factory(ClientTestimony::class)->create();

        $dbClientTestimony = $this->clientTestimonyRepo->find($clientTestimony->id);

        $dbClientTestimony = $dbClientTestimony->toArray();
        $this->assertModelData($clientTestimony->toArray(), $dbClientTestimony);
    }

    /**
     * @test update
     */
    public function test_update_client_testimony()
    {
        $clientTestimony = factory(ClientTestimony::class)->create();
        $fakeClientTestimony = factory(ClientTestimony::class)->make()->toArray();

        $updatedClientTestimony = $this->clientTestimonyRepo->update($fakeClientTestimony, $clientTestimony->id);

        $this->assertModelData($fakeClientTestimony, $updatedClientTestimony->toArray());
        $dbClientTestimony = $this->clientTestimonyRepo->find($clientTestimony->id);
        $this->assertModelData($fakeClientTestimony, $dbClientTestimony->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_client_testimony()
    {
        $clientTestimony = factory(ClientTestimony::class)->create();

        $resp = $this->clientTestimonyRepo->delete($clientTestimony->id);

        $this->assertTrue($resp);
        $this->assertNull(ClientTestimony::find($clientTestimony->id), 'ClientTestimony should not exist in DB');
    }
}
