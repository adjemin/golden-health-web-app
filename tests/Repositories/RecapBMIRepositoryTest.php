<?php namespace Tests\Repositories;

use App\Models\RecapBMI;
use App\Repositories\RecapBMIRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RecapBMIRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RecapBMIRepository
     */
    protected $recapBMIRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->recapBMIRepo = \App::make(RecapBMIRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_recap_b_m_i()
    {
        $recapBMI = factory(RecapBMI::class)->make()->toArray();

        $createdRecapBMI = $this->recapBMIRepo->create($recapBMI);

        $createdRecapBMI = $createdRecapBMI->toArray();
        $this->assertArrayHasKey('id', $createdRecapBMI);
        $this->assertNotNull($createdRecapBMI['id'], 'Created RecapBMI must have id specified');
        $this->assertNotNull(RecapBMI::find($createdRecapBMI['id']), 'RecapBMI with given id must be in DB');
        $this->assertModelData($recapBMI, $createdRecapBMI);
    }

    /**
     * @test read
     */
    public function test_read_recap_b_m_i()
    {
        $recapBMI = factory(RecapBMI::class)->create();

        $dbRecapBMI = $this->recapBMIRepo->find($recapBMI->id);

        $dbRecapBMI = $dbRecapBMI->toArray();
        $this->assertModelData($recapBMI->toArray(), $dbRecapBMI);
    }

    /**
     * @test update
     */
    public function test_update_recap_b_m_i()
    {
        $recapBMI = factory(RecapBMI::class)->create();
        $fakeRecapBMI = factory(RecapBMI::class)->make()->toArray();

        $updatedRecapBMI = $this->recapBMIRepo->update($fakeRecapBMI, $recapBMI->id);

        $this->assertModelData($fakeRecapBMI, $updatedRecapBMI->toArray());
        $dbRecapBMI = $this->recapBMIRepo->find($recapBMI->id);
        $this->assertModelData($fakeRecapBMI, $dbRecapBMI->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_recap_b_m_i()
    {
        $recapBMI = factory(RecapBMI::class)->create();

        $resp = $this->recapBMIRepo->delete($recapBMI->id);

        $this->assertTrue($resp);
        $this->assertNull(RecapBMI::find($recapBMI->id), 'RecapBMI should not exist in DB');
    }
}
