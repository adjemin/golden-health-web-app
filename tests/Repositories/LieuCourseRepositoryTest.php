<?php namespace Tests\Repositories;

use App\Models\LieuCourse;
use App\Repositories\LieuCourseRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class LieuCourseRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var LieuCourseRepository
     */
    protected $lieuCourseRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->lieuCourseRepo = \App::make(LieuCourseRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_lieu_course()
    {
        $lieuCourse = factory(LieuCourse::class)->make()->toArray();

        $createdLieuCourse = $this->lieuCourseRepo->create($lieuCourse);

        $createdLieuCourse = $createdLieuCourse->toArray();
        $this->assertArrayHasKey('id', $createdLieuCourse);
        $this->assertNotNull($createdLieuCourse['id'], 'Created LieuCourse must have id specified');
        $this->assertNotNull(LieuCourse::find($createdLieuCourse['id']), 'LieuCourse with given id must be in DB');
        $this->assertModelData($lieuCourse, $createdLieuCourse);
    }

    /**
     * @test read
     */
    public function test_read_lieu_course()
    {
        $lieuCourse = factory(LieuCourse::class)->create();

        $dbLieuCourse = $this->lieuCourseRepo->find($lieuCourse->id);

        $dbLieuCourse = $dbLieuCourse->toArray();
        $this->assertModelData($lieuCourse->toArray(), $dbLieuCourse);
    }

    /**
     * @test update
     */
    public function test_update_lieu_course()
    {
        $lieuCourse = factory(LieuCourse::class)->create();
        $fakeLieuCourse = factory(LieuCourse::class)->make()->toArray();

        $updatedLieuCourse = $this->lieuCourseRepo->update($fakeLieuCourse, $lieuCourse->id);

        $this->assertModelData($fakeLieuCourse, $updatedLieuCourse->toArray());
        $dbLieuCourse = $this->lieuCourseRepo->find($lieuCourse->id);
        $this->assertModelData($fakeLieuCourse, $dbLieuCourse->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_lieu_course()
    {
        $lieuCourse = factory(LieuCourse::class)->create();

        $resp = $this->lieuCourseRepo->delete($lieuCourse->id);

        $this->assertTrue($resp);
        $this->assertNull(LieuCourse::find($lieuCourse->id), 'LieuCourse should not exist in DB');
    }
}
