<?php namespace Tests\Repositories;

use App\Models\QualificationType;
use App\Repositories\QualificationTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class QualificationTypeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var QualificationTypeRepository
     */
    protected $qualificationTypeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->qualificationTypeRepo = \App::make(QualificationTypeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_qualification_type()
    {
        $qualificationType = factory(QualificationType::class)->make()->toArray();

        $createdQualificationType = $this->qualificationTypeRepo->create($qualificationType);

        $createdQualificationType = $createdQualificationType->toArray();
        $this->assertArrayHasKey('id', $createdQualificationType);
        $this->assertNotNull($createdQualificationType['id'], 'Created QualificationType must have id specified');
        $this->assertNotNull(QualificationType::find($createdQualificationType['id']), 'QualificationType with given id must be in DB');
        $this->assertModelData($qualificationType, $createdQualificationType);
    }

    /**
     * @test read
     */
    public function test_read_qualification_type()
    {
        $qualificationType = factory(QualificationType::class)->create();

        $dbQualificationType = $this->qualificationTypeRepo->find($qualificationType->id);

        $dbQualificationType = $dbQualificationType->toArray();
        $this->assertModelData($qualificationType->toArray(), $dbQualificationType);
    }

    /**
     * @test update
     */
    public function test_update_qualification_type()
    {
        $qualificationType = factory(QualificationType::class)->create();
        $fakeQualificationType = factory(QualificationType::class)->make()->toArray();

        $updatedQualificationType = $this->qualificationTypeRepo->update($fakeQualificationType, $qualificationType->id);

        $this->assertModelData($fakeQualificationType, $updatedQualificationType->toArray());
        $dbQualificationType = $this->qualificationTypeRepo->find($qualificationType->id);
        $this->assertModelData($fakeQualificationType, $dbQualificationType->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_qualification_type()
    {
        $qualificationType = factory(QualificationType::class)->create();

        $resp = $this->qualificationTypeRepo->delete($qualificationType->id);

        $this->assertTrue($resp);
        $this->assertNull(QualificationType::find($qualificationType->id), 'QualificationType should not exist in DB');
    }
}
