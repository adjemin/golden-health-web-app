<?php namespace Tests\Repositories;

use App\Models\BookingCoach;
use App\Repositories\BookingCoachRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class BookingCoachRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var BookingCoachRepository
     */
    protected $bookingCoachRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->bookingCoachRepo = \App::make(BookingCoachRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_booking_coach()
    {
        $bookingCoach = factory(BookingCoach::class)->make()->toArray();

        $createdBookingCoach = $this->bookingCoachRepo->create($bookingCoach);

        $createdBookingCoach = $createdBookingCoach->toArray();
        $this->assertArrayHasKey('id', $createdBookingCoach);
        $this->assertNotNull($createdBookingCoach['id'], 'Created BookingCoach must have id specified');
        $this->assertNotNull(BookingCoach::find($createdBookingCoach['id']), 'BookingCoach with given id must be in DB');
        $this->assertModelData($bookingCoach, $createdBookingCoach);
    }

    /**
     * @test read
     */
    public function test_read_booking_coach()
    {
        $bookingCoach = factory(BookingCoach::class)->create();

        $dbBookingCoach = $this->bookingCoachRepo->find($bookingCoach->id);

        $dbBookingCoach = $dbBookingCoach->toArray();
        $this->assertModelData($bookingCoach->toArray(), $dbBookingCoach);
    }

    /**
     * @test update
     */
    public function test_update_booking_coach()
    {
        $bookingCoach = factory(BookingCoach::class)->create();
        $fakeBookingCoach = factory(BookingCoach::class)->make()->toArray();

        $updatedBookingCoach = $this->bookingCoachRepo->update($fakeBookingCoach, $bookingCoach->id);

        $this->assertModelData($fakeBookingCoach, $updatedBookingCoach->toArray());
        $dbBookingCoach = $this->bookingCoachRepo->find($bookingCoach->id);
        $this->assertModelData($fakeBookingCoach, $dbBookingCoach->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_booking_coach()
    {
        $bookingCoach = factory(BookingCoach::class)->create();

        $resp = $this->bookingCoachRepo->delete($bookingCoach->id);

        $this->assertTrue($resp);
        $this->assertNull(BookingCoach::find($bookingCoach->id), 'BookingCoach should not exist in DB');
    }
}
