<?php namespace Tests\Repositories;

use App\Models\Availability;
use App\Repositories\AvailabilityRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AvailabilityRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AvailabilityRepository
     */
    protected $availabilityRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->availabilityRepo = \App::make(AvailabilityRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_availability()
    {
        $availability = factory(Availability::class)->make()->toArray();

        $createdAvailability = $this->availabilityRepo->create($availability);

        $createdAvailability = $createdAvailability->toArray();
        $this->assertArrayHasKey('id', $createdAvailability);
        $this->assertNotNull($createdAvailability['id'], 'Created Availability must have id specified');
        $this->assertNotNull(Availability::find($createdAvailability['id']), 'Availability with given id must be in DB');
        $this->assertModelData($availability, $createdAvailability);
    }

    /**
     * @test read
     */
    public function test_read_availability()
    {
        $availability = factory(Availability::class)->create();

        $dbAvailability = $this->availabilityRepo->find($availability->id);

        $dbAvailability = $dbAvailability->toArray();
        $this->assertModelData($availability->toArray(), $dbAvailability);
    }

    /**
     * @test update
     */
    public function test_update_availability()
    {
        $availability = factory(Availability::class)->create();
        $fakeAvailability = factory(Availability::class)->make()->toArray();

        $updatedAvailability = $this->availabilityRepo->update($fakeAvailability, $availability->id);

        $this->assertModelData($fakeAvailability, $updatedAvailability->toArray());
        $dbAvailability = $this->availabilityRepo->find($availability->id);
        $this->assertModelData($fakeAvailability, $dbAvailability->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_availability()
    {
        $availability = factory(Availability::class)->create();

        $resp = $this->availabilityRepo->delete($availability->id);

        $this->assertTrue($resp);
        $this->assertNull(Availability::find($availability->id), 'Availability should not exist in DB');
    }
}
