<?php namespace Tests\Repositories;

use App\Models\Connexion;
use App\Repositories\ConnexionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ConnexionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ConnexionRepository
     */
    protected $connexionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->connexionRepo = \App::make(ConnexionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_connexion()
    {
        $connexion = factory(Connexion::class)->make()->toArray();

        $createdConnexion = $this->connexionRepo->create($connexion);

        $createdConnexion = $createdConnexion->toArray();
        $this->assertArrayHasKey('id', $createdConnexion);
        $this->assertNotNull($createdConnexion['id'], 'Created Connexion must have id specified');
        $this->assertNotNull(Connexion::find($createdConnexion['id']), 'Connexion with given id must be in DB');
        $this->assertModelData($connexion, $createdConnexion);
    }

    /**
     * @test read
     */
    public function test_read_connexion()
    {
        $connexion = factory(Connexion::class)->create();

        $dbConnexion = $this->connexionRepo->find($connexion->id);

        $dbConnexion = $dbConnexion->toArray();
        $this->assertModelData($connexion->toArray(), $dbConnexion);
    }

    /**
     * @test update
     */
    public function test_update_connexion()
    {
        $connexion = factory(Connexion::class)->create();
        $fakeConnexion = factory(Connexion::class)->make()->toArray();

        $updatedConnexion = $this->connexionRepo->update($fakeConnexion, $connexion->id);

        $this->assertModelData($fakeConnexion, $updatedConnexion->toArray());
        $dbConnexion = $this->connexionRepo->find($connexion->id);
        $this->assertModelData($fakeConnexion, $dbConnexion->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_connexion()
    {
        $connexion = factory(Connexion::class)->create();

        $resp = $this->connexionRepo->delete($connexion->id);

        $this->assertTrue($resp);
        $this->assertNull(Connexion::find($connexion->id), 'Connexion should not exist in DB');
    }
}
