<?php namespace Tests\Repositories;

use App\Models\BodyMassIndex;
use App\Repositories\BodyMassIndexRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class BodyMassIndexRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var BodyMassIndexRepository
     */
    protected $bodyMassIndexRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->bodyMassIndexRepo = \App::make(BodyMassIndexRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_body_mass_index()
    {
        $bodyMassIndex = factory(BodyMassIndex::class)->make()->toArray();

        $createdBodyMassIndex = $this->bodyMassIndexRepo->create($bodyMassIndex);

        $createdBodyMassIndex = $createdBodyMassIndex->toArray();
        $this->assertArrayHasKey('id', $createdBodyMassIndex);
        $this->assertNotNull($createdBodyMassIndex['id'], 'Created BodyMassIndex must have id specified');
        $this->assertNotNull(BodyMassIndex::find($createdBodyMassIndex['id']), 'BodyMassIndex with given id must be in DB');
        $this->assertModelData($bodyMassIndex, $createdBodyMassIndex);
    }

    /**
     * @test read
     */
    public function test_read_body_mass_index()
    {
        $bodyMassIndex = factory(BodyMassIndex::class)->create();

        $dbBodyMassIndex = $this->bodyMassIndexRepo->find($bodyMassIndex->id);

        $dbBodyMassIndex = $dbBodyMassIndex->toArray();
        $this->assertModelData($bodyMassIndex->toArray(), $dbBodyMassIndex);
    }

    /**
     * @test update
     */
    public function test_update_body_mass_index()
    {
        $bodyMassIndex = factory(BodyMassIndex::class)->create();
        $fakeBodyMassIndex = factory(BodyMassIndex::class)->make()->toArray();

        $updatedBodyMassIndex = $this->bodyMassIndexRepo->update($fakeBodyMassIndex, $bodyMassIndex->id);

        $this->assertModelData($fakeBodyMassIndex, $updatedBodyMassIndex->toArray());
        $dbBodyMassIndex = $this->bodyMassIndexRepo->find($bodyMassIndex->id);
        $this->assertModelData($fakeBodyMassIndex, $dbBodyMassIndex->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_body_mass_index()
    {
        $bodyMassIndex = factory(BodyMassIndex::class)->create();

        $resp = $this->bodyMassIndexRepo->delete($bodyMassIndex->id);

        $this->assertTrue($resp);
        $this->assertNull(BodyMassIndex::find($bodyMassIndex->id), 'BodyMassIndex should not exist in DB');
    }
}
