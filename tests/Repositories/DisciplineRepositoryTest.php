<?php namespace Tests\Repositories;

use App\Models\Discipline;
use App\Repositories\DisciplineRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DisciplineRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DisciplineRepository
     */
    protected $disciplineRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->disciplineRepo = \App::make(DisciplineRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_discipline()
    {
        $discipline = factory(Discipline::class)->make()->toArray();

        $createdDiscipline = $this->disciplineRepo->create($discipline);

        $createdDiscipline = $createdDiscipline->toArray();
        $this->assertArrayHasKey('id', $createdDiscipline);
        $this->assertNotNull($createdDiscipline['id'], 'Created Discipline must have id specified');
        $this->assertNotNull(Discipline::find($createdDiscipline['id']), 'Discipline with given id must be in DB');
        $this->assertModelData($discipline, $createdDiscipline);
    }

    /**
     * @test read
     */
    public function test_read_discipline()
    {
        $discipline = factory(Discipline::class)->create();

        $dbDiscipline = $this->disciplineRepo->find($discipline->id);

        $dbDiscipline = $dbDiscipline->toArray();
        $this->assertModelData($discipline->toArray(), $dbDiscipline);
    }

    /**
     * @test update
     */
    public function test_update_discipline()
    {
        $discipline = factory(Discipline::class)->create();
        $fakeDiscipline = factory(Discipline::class)->make()->toArray();

        $updatedDiscipline = $this->disciplineRepo->update($fakeDiscipline, $discipline->id);

        $this->assertModelData($fakeDiscipline, $updatedDiscipline->toArray());
        $dbDiscipline = $this->disciplineRepo->find($discipline->id);
        $this->assertModelData($fakeDiscipline, $dbDiscipline->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_discipline()
    {
        $discipline = factory(Discipline::class)->create();

        $resp = $this->disciplineRepo->delete($discipline->id);

        $this->assertTrue($resp);
        $this->assertNull(Discipline::find($discipline->id), 'Discipline should not exist in DB');
    }
}
