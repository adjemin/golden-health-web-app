<?php namespace Tests\Repositories;

use App\Models\Objective;
use App\Repositories\ObjectiveRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ObjectiveRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ObjectiveRepository
     */
    protected $objectiveRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->objectiveRepo = \App::make(ObjectiveRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_objective()
    {
        $objective = factory(Objective::class)->make()->toArray();

        $createdObjective = $this->objectiveRepo->create($objective);

        $createdObjective = $createdObjective->toArray();
        $this->assertArrayHasKey('id', $createdObjective);
        $this->assertNotNull($createdObjective['id'], 'Created Objective must have id specified');
        $this->assertNotNull(Objective::find($createdObjective['id']), 'Objective with given id must be in DB');
        $this->assertModelData($objective, $createdObjective);
    }

    /**
     * @test read
     */
    public function test_read_objective()
    {
        $objective = factory(Objective::class)->create();

        $dbObjective = $this->objectiveRepo->find($objective->id);

        $dbObjective = $dbObjective->toArray();
        $this->assertModelData($objective->toArray(), $dbObjective);
    }

    /**
     * @test update
     */
    public function test_update_objective()
    {
        $objective = factory(Objective::class)->create();
        $fakeObjective = factory(Objective::class)->make()->toArray();

        $updatedObjective = $this->objectiveRepo->update($fakeObjective, $objective->id);

        $this->assertModelData($fakeObjective, $updatedObjective->toArray());
        $dbObjective = $this->objectiveRepo->find($objective->id);
        $this->assertModelData($fakeObjective, $dbObjective->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_objective()
    {
        $objective = factory(Objective::class)->create();

        $resp = $this->objectiveRepo->delete($objective->id);

        $this->assertTrue($resp);
        $this->assertNull(Objective::find($objective->id), 'Objective should not exist in DB');
    }
}
