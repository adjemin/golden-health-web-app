<?php namespace Tests\Repositories;

use App\Models\Diplome;
use App\Repositories\DiplomeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DiplomeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DiplomeRepository
     */
    protected $diplomeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->diplomeRepo = \App::make(DiplomeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_diplome()
    {
        $diplome = factory(Diplome::class)->make()->toArray();

        $createdDiplome = $this->diplomeRepo->create($diplome);

        $createdDiplome = $createdDiplome->toArray();
        $this->assertArrayHasKey('id', $createdDiplome);
        $this->assertNotNull($createdDiplome['id'], 'Created Diplome must have id specified');
        $this->assertNotNull(Diplome::find($createdDiplome['id']), 'Diplome with given id must be in DB');
        $this->assertModelData($diplome, $createdDiplome);
    }

    /**
     * @test read
     */
    public function test_read_diplome()
    {
        $diplome = factory(Diplome::class)->create();

        $dbDiplome = $this->diplomeRepo->find($diplome->id);

        $dbDiplome = $dbDiplome->toArray();
        $this->assertModelData($diplome->toArray(), $dbDiplome);
    }

    /**
     * @test update
     */
    public function test_update_diplome()
    {
        $diplome = factory(Diplome::class)->create();
        $fakeDiplome = factory(Diplome::class)->make()->toArray();

        $updatedDiplome = $this->diplomeRepo->update($fakeDiplome, $diplome->id);

        $this->assertModelData($fakeDiplome, $updatedDiplome->toArray());
        $dbDiplome = $this->diplomeRepo->find($diplome->id);
        $this->assertModelData($fakeDiplome, $dbDiplome->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_diplome()
    {
        $diplome = factory(Diplome::class)->create();

        $resp = $this->diplomeRepo->delete($diplome->id);

        $this->assertTrue($resp);
        $this->assertNull(Diplome::find($diplome->id), 'Diplome should not exist in DB');
    }
}
