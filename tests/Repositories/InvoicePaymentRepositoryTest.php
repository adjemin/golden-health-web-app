<?php namespace Tests\Repositories;

use App\Models\InvoicePayment;
use App\Repositories\InvoicePaymentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class InvoicePaymentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var InvoicePaymentRepository
     */
    protected $InvoicePaymentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->InvoicePaymentRepo = \App::make(InvoicePaymentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_invoice_payment()
    {
        $InvoicePayment = factory(InvoicePayment::class)->make()->toArray();

        $createdInvoicePayment = $this->InvoicePaymentRepo->create($InvoicePayment);

        $createdInvoicePayment = $createdInvoicePayment->toArray();
        $this->assertArrayHasKey('id', $createdInvoicePayment);
        $this->assertNotNull($createdInvoicePayment['id'], 'Created InvoicePayment must have id specified');
        $this->assertNotNull(InvoicePayment::find($createdInvoicePayment['id']), 'InvoicePayment with given id must be in DB');
        $this->assertModelData($InvoicePayment, $createdInvoicePayment);
    }

    /**
     * @test read
     */
    public function test_read_invoice_payment()
    {
        $InvoicePayment = factory(InvoicePayment::class)->create();

        $dbInvoicePayment = $this->InvoicePaymentRepo->find($InvoicePayment->id);

        $dbInvoicePayment = $dbInvoicePayment->toArray();
        $this->assertModelData($InvoicePayment->toArray(), $dbInvoicePayment);
    }

    /**
     * @test update
     */
    public function test_update_invoice_payment()
    {
        $InvoicePayment = factory(InvoicePayment::class)->create();
        $fakeInvoicePayment = factory(InvoicePayment::class)->make()->toArray();

        $updatedInvoicePayment = $this->InvoicePaymentRepo->update($fakeInvoicePayment, $InvoicePayment->id);

        $this->assertModelData($fakeInvoicePayment, $updatedInvoicePayment->toArray());
        $dbInvoicePayment = $this->InvoicePaymentRepo->find($InvoicePayment->id);
        $this->assertModelData($fakeInvoicePayment, $dbInvoicePayment->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_invoice_payment()
    {
        $InvoicePayment = factory(InvoicePayment::class)->create();

        $resp = $this->InvoicePaymentRepo->delete($InvoicePayment->id);

        $this->assertTrue($resp);
        $this->assertNull(InvoicePayment::find($InvoicePayment->id), 'InvoicePayment should not exist in DB');
    }
}
