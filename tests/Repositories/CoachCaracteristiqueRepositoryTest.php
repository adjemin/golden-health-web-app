<?php namespace Tests\Repositories;

use App\Models\CoachCaracteristique;
use App\Repositories\CoachCaracteristiqueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CoachCaracteristiqueRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CoachCaracteristiqueRepository
     */
    protected $coachCaracteristiqueRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->coachCaracteristiqueRepo = \App::make(CoachCaracteristiqueRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_coach_caracteristique()
    {
        $coachCaracteristique = factory(CoachCaracteristique::class)->make()->toArray();

        $createdCoachCaracteristique = $this->coachCaracteristiqueRepo->create($coachCaracteristique);

        $createdCoachCaracteristique = $createdCoachCaracteristique->toArray();
        $this->assertArrayHasKey('id', $createdCoachCaracteristique);
        $this->assertNotNull($createdCoachCaracteristique['id'], 'Created CoachCaracteristique must have id specified');
        $this->assertNotNull(CoachCaracteristique::find($createdCoachCaracteristique['id']), 'CoachCaracteristique with given id must be in DB');
        $this->assertModelData($coachCaracteristique, $createdCoachCaracteristique);
    }

    /**
     * @test read
     */
    public function test_read_coach_caracteristique()
    {
        $coachCaracteristique = factory(CoachCaracteristique::class)->create();

        $dbCoachCaracteristique = $this->coachCaracteristiqueRepo->find($coachCaracteristique->id);

        $dbCoachCaracteristique = $dbCoachCaracteristique->toArray();
        $this->assertModelData($coachCaracteristique->toArray(), $dbCoachCaracteristique);
    }

    /**
     * @test update
     */
    public function test_update_coach_caracteristique()
    {
        $coachCaracteristique = factory(CoachCaracteristique::class)->create();
        $fakeCoachCaracteristique = factory(CoachCaracteristique::class)->make()->toArray();

        $updatedCoachCaracteristique = $this->coachCaracteristiqueRepo->update($fakeCoachCaracteristique, $coachCaracteristique->id);

        $this->assertModelData($fakeCoachCaracteristique, $updatedCoachCaracteristique->toArray());
        $dbCoachCaracteristique = $this->coachCaracteristiqueRepo->find($coachCaracteristique->id);
        $this->assertModelData($fakeCoachCaracteristique, $dbCoachCaracteristique->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_coach_caracteristique()
    {
        $coachCaracteristique = factory(CoachCaracteristique::class)->create();

        $resp = $this->coachCaracteristiqueRepo->delete($coachCaracteristique->id);

        $this->assertTrue($resp);
        $this->assertNull(CoachCaracteristique::find($coachCaracteristique->id), 'CoachCaracteristique should not exist in DB');
    }
}
