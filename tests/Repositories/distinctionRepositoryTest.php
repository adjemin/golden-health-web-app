<?php namespace Tests\Repositories;

use App\Models\distinction;
use App\Repositories\distinctionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class distinctionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var distinctionRepository
     */
    protected $distinctionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->distinctionRepo = \App::make(distinctionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_distinction()
    {
        $distinction = factory(distinction::class)->make()->toArray();

        $createddistinction = $this->distinctionRepo->create($distinction);

        $createddistinction = $createddistinction->toArray();
        $this->assertArrayHasKey('id', $createddistinction);
        $this->assertNotNull($createddistinction['id'], 'Created distinction must have id specified');
        $this->assertNotNull(distinction::find($createddistinction['id']), 'distinction with given id must be in DB');
        $this->assertModelData($distinction, $createddistinction);
    }

    /**
     * @test read
     */
    public function test_read_distinction()
    {
        $distinction = factory(distinction::class)->create();

        $dbdistinction = $this->distinctionRepo->find($distinction->id);

        $dbdistinction = $dbdistinction->toArray();
        $this->assertModelData($distinction->toArray(), $dbdistinction);
    }

    /**
     * @test update
     */
    public function test_update_distinction()
    {
        $distinction = factory(distinction::class)->create();
        $fakedistinction = factory(distinction::class)->make()->toArray();

        $updateddistinction = $this->distinctionRepo->update($fakedistinction, $distinction->id);

        $this->assertModelData($fakedistinction, $updateddistinction->toArray());
        $dbdistinction = $this->distinctionRepo->find($distinction->id);
        $this->assertModelData($fakedistinction, $dbdistinction->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_distinction()
    {
        $distinction = factory(distinction::class)->create();

        $resp = $this->distinctionRepo->delete($distinction->id);

        $this->assertTrue($resp);
        $this->assertNull(distinction::find($distinction->id), 'distinction should not exist in DB');
    }
}
