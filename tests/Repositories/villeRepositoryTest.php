<?php namespace Tests\Repositories;

use App\Models\ville;
use App\Repositories\villeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class villeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var villeRepository
     */
    protected $villeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->villeRepo = \App::make(villeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_ville()
    {
        $ville = factory(ville::class)->make()->toArray();

        $createdville = $this->villeRepo->create($ville);

        $createdville = $createdville->toArray();
        $this->assertArrayHasKey('id', $createdville);
        $this->assertNotNull($createdville['id'], 'Created ville must have id specified');
        $this->assertNotNull(ville::find($createdville['id']), 'ville with given id must be in DB');
        $this->assertModelData($ville, $createdville);
    }

    /**
     * @test read
     */
    public function test_read_ville()
    {
        $ville = factory(ville::class)->create();

        $dbville = $this->villeRepo->find($ville->id);

        $dbville = $dbville->toArray();
        $this->assertModelData($ville->toArray(), $dbville);
    }

    /**
     * @test update
     */
    public function test_update_ville()
    {
        $ville = factory(ville::class)->create();
        $fakeville = factory(ville::class)->make()->toArray();

        $updatedville = $this->villeRepo->update($fakeville, $ville->id);

        $this->assertModelData($fakeville, $updatedville->toArray());
        $dbville = $this->villeRepo->find($ville->id);
        $this->assertModelData($fakeville, $dbville->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_ville()
    {
        $ville = factory(ville::class)->create();

        $resp = $this->villeRepo->delete($ville->id);

        $this->assertTrue($resp);
        $this->assertNull(ville::find($ville->id), 'ville should not exist in DB');
    }
}
