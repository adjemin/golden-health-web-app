<?php namespace Tests\Repositories;

use App\Models\Seance;
use App\Repositories\SeanceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SeanceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SeanceRepository
     */
    protected $seanceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->seanceRepo = \App::make(SeanceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_seance()
    {
        $seance = factory(Seance::class)->make()->toArray();

        $createdSeance = $this->seanceRepo->create($seance);

        $createdSeance = $createdSeance->toArray();
        $this->assertArrayHasKey('id', $createdSeance);
        $this->assertNotNull($createdSeance['id'], 'Created Seance must have id specified');
        $this->assertNotNull(Seance::find($createdSeance['id']), 'Seance with given id must be in DB');
        $this->assertModelData($seance, $createdSeance);
    }

    /**
     * @test read
     */
    public function test_read_seance()
    {
        $seance = factory(Seance::class)->create();

        $dbSeance = $this->seanceRepo->find($seance->id);

        $dbSeance = $dbSeance->toArray();
        $this->assertModelData($seance->toArray(), $dbSeance);
    }

    /**
     * @test update
     */
    public function test_update_seance()
    {
        $seance = factory(Seance::class)->create();
        $fakeSeance = factory(Seance::class)->make()->toArray();

        $updatedSeance = $this->seanceRepo->update($fakeSeance, $seance->id);

        $this->assertModelData($fakeSeance, $updatedSeance->toArray());
        $dbSeance = $this->seanceRepo->find($seance->id);
        $this->assertModelData($fakeSeance, $dbSeance->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_seance()
    {
        $seance = factory(Seance::class)->create();

        $resp = $this->seanceRepo->delete($seance->id);

        $this->assertTrue($resp);
        $this->assertNull(Seance::find($seance->id), 'Seance should not exist in DB');
    }
}
