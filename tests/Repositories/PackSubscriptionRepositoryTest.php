<?php namespace Tests\Repositories;

use App\Models\PackSubscription;
use App\Repositories\PackSubscriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PackSubscriptionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PackSubscriptionRepository
     */
    protected $packSubscriptionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->packSubscriptionRepo = \App::make(PackSubscriptionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_pack_subscription()
    {
        $packSubscription = factory(PackSubscription::class)->make()->toArray();

        $createdPackSubscription = $this->packSubscriptionRepo->create($packSubscription);

        $createdPackSubscription = $createdPackSubscription->toArray();
        $this->assertArrayHasKey('id', $createdPackSubscription);
        $this->assertNotNull($createdPackSubscription['id'], 'Created PackSubscription must have id specified');
        $this->assertNotNull(PackSubscription::find($createdPackSubscription['id']), 'PackSubscription with given id must be in DB');
        $this->assertModelData($packSubscription, $createdPackSubscription);
    }

    /**
     * @test read
     */
    public function test_read_pack_subscription()
    {
        $packSubscription = factory(PackSubscription::class)->create();

        $dbPackSubscription = $this->packSubscriptionRepo->find($packSubscription->id);

        $dbPackSubscription = $dbPackSubscription->toArray();
        $this->assertModelData($packSubscription->toArray(), $dbPackSubscription);
    }

    /**
     * @test update
     */
    public function test_update_pack_subscription()
    {
        $packSubscription = factory(PackSubscription::class)->create();
        $fakePackSubscription = factory(PackSubscription::class)->make()->toArray();

        $updatedPackSubscription = $this->packSubscriptionRepo->update($fakePackSubscription, $packSubscription->id);

        $this->assertModelData($fakePackSubscription, $updatedPackSubscription->toArray());
        $dbPackSubscription = $this->packSubscriptionRepo->find($packSubscription->id);
        $this->assertModelData($fakePackSubscription, $dbPackSubscription->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_pack_subscription()
    {
        $packSubscription = factory(PackSubscription::class)->create();

        $resp = $this->packSubscriptionRepo->delete($packSubscription->id);

        $this->assertTrue($resp);
        $this->assertNull(PackSubscription::find($packSubscription->id), 'PackSubscription should not exist in DB');
    }
}
