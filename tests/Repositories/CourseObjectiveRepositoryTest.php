<?php namespace Tests\Repositories;

use App\Models\CourseObjective;
use App\Repositories\CourseObjectiveRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CourseObjectiveRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CourseObjectiveRepository
     */
    protected $courseObjectiveRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->courseObjectiveRepo = \App::make(CourseObjectiveRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_course_objective()
    {
        $courseObjective = factory(CourseObjective::class)->make()->toArray();

        $createdCourseObjective = $this->courseObjectiveRepo->create($courseObjective);

        $createdCourseObjective = $createdCourseObjective->toArray();
        $this->assertArrayHasKey('id', $createdCourseObjective);
        $this->assertNotNull($createdCourseObjective['id'], 'Created CourseObjective must have id specified');
        $this->assertNotNull(CourseObjective::find($createdCourseObjective['id']), 'CourseObjective with given id must be in DB');
        $this->assertModelData($courseObjective, $createdCourseObjective);
    }

    /**
     * @test read
     */
    public function test_read_course_objective()
    {
        $courseObjective = factory(CourseObjective::class)->create();

        $dbCourseObjective = $this->courseObjectiveRepo->find($courseObjective->id);

        $dbCourseObjective = $dbCourseObjective->toArray();
        $this->assertModelData($courseObjective->toArray(), $dbCourseObjective);
    }

    /**
     * @test update
     */
    public function test_update_course_objective()
    {
        $courseObjective = factory(CourseObjective::class)->create();
        $fakeCourseObjective = factory(CourseObjective::class)->make()->toArray();

        $updatedCourseObjective = $this->courseObjectiveRepo->update($fakeCourseObjective, $courseObjective->id);

        $this->assertModelData($fakeCourseObjective, $updatedCourseObjective->toArray());
        $dbCourseObjective = $this->courseObjectiveRepo->find($courseObjective->id);
        $this->assertModelData($fakeCourseObjective, $dbCourseObjective->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_course_objective()
    {
        $courseObjective = factory(CourseObjective::class)->create();

        $resp = $this->courseObjectiveRepo->delete($courseObjective->id);

        $this->assertTrue($resp);
        $this->assertNull(CourseObjective::find($courseObjective->id), 'CourseObjective should not exist in DB');
    }
}
