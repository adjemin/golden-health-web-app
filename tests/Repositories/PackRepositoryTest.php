<?php namespace Tests\Repositories;

use App\Models\Pack;
use App\Repositories\PackRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PackRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PackRepository
     */
    protected $packRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->packRepo = \App::make(PackRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_pack()
    {
        $pack = factory(Pack::class)->make()->toArray();

        $createdPack = $this->packRepo->create($pack);

        $createdPack = $createdPack->toArray();
        $this->assertArrayHasKey('id', $createdPack);
        $this->assertNotNull($createdPack['id'], 'Created Pack must have id specified');
        $this->assertNotNull(Pack::find($createdPack['id']), 'Pack with given id must be in DB');
        $this->assertModelData($pack, $createdPack);
    }

    /**
     * @test read
     */
    public function test_read_pack()
    {
        $pack = factory(Pack::class)->create();

        $dbPack = $this->packRepo->find($pack->id);

        $dbPack = $dbPack->toArray();
        $this->assertModelData($pack->toArray(), $dbPack);
    }

    /**
     * @test update
     */
    public function test_update_pack()
    {
        $pack = factory(Pack::class)->create();
        $fakePack = factory(Pack::class)->make()->toArray();

        $updatedPack = $this->packRepo->update($fakePack, $pack->id);

        $this->assertModelData($fakePack, $updatedPack->toArray());
        $dbPack = $this->packRepo->find($pack->id);
        $this->assertModelData($fakePack, $dbPack->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_pack()
    {
        $pack = factory(Pack::class)->create();

        $resp = $this->packRepo->delete($pack->id);

        $this->assertTrue($resp);
        $this->assertNull(Pack::find($pack->id), 'Pack should not exist in DB');
    }
}
