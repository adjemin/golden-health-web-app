<?php namespace Tests\Repositories;

use App\Models\Advice;
use App\Repositories\AdviceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AdviceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AdviceRepository
     */
    protected $adviceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->adviceRepo = \App::make(AdviceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_advice()
    {
        $advice = factory(Advice::class)->make()->toArray();

        $createdAdvice = $this->adviceRepo->create($advice);

        $createdAdvice = $createdAdvice->toArray();
        $this->assertArrayHasKey('id', $createdAdvice);
        $this->assertNotNull($createdAdvice['id'], 'Created Advice must have id specified');
        $this->assertNotNull(Advice::find($createdAdvice['id']), 'Advice with given id must be in DB');
        $this->assertModelData($advice, $createdAdvice);
    }

    /**
     * @test read
     */
    public function test_read_advice()
    {
        $advice = factory(Advice::class)->create();

        $dbAdvice = $this->adviceRepo->find($advice->id);

        $dbAdvice = $dbAdvice->toArray();
        $this->assertModelData($advice->toArray(), $dbAdvice);
    }

    /**
     * @test update
     */
    public function test_update_advice()
    {
        $advice = factory(Advice::class)->create();
        $fakeAdvice = factory(Advice::class)->make()->toArray();

        $updatedAdvice = $this->adviceRepo->update($fakeAdvice, $advice->id);

        $this->assertModelData($fakeAdvice, $updatedAdvice->toArray());
        $dbAdvice = $this->adviceRepo->find($advice->id);
        $this->assertModelData($fakeAdvice, $dbAdvice->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_advice()
    {
        $advice = factory(Advice::class)->create();

        $resp = $this->adviceRepo->delete($advice->id);

        $this->assertTrue($resp);
        $this->assertNull(Advice::find($advice->id), 'Advice should not exist in DB');
    }
}
