<?php namespace Tests\Repositories;

use App\Models\HowDidYouKnow;
use App\Repositories\HowDidYouKnowRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class HowDidYouKnowRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var HowDidYouKnowRepository
     */
    protected $howDidYouKnowRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->howDidYouKnowRepo = \App::make(HowDidYouKnowRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_how_did_you_know()
    {
        $howDidYouKnow = factory(HowDidYouKnow::class)->make()->toArray();

        $createdHowDidYouKnow = $this->howDidYouKnowRepo->create($howDidYouKnow);

        $createdHowDidYouKnow = $createdHowDidYouKnow->toArray();
        $this->assertArrayHasKey('id', $createdHowDidYouKnow);
        $this->assertNotNull($createdHowDidYouKnow['id'], 'Created HowDidYouKnow must have id specified');
        $this->assertNotNull(HowDidYouKnow::find($createdHowDidYouKnow['id']), 'HowDidYouKnow with given id must be in DB');
        $this->assertModelData($howDidYouKnow, $createdHowDidYouKnow);
    }

    /**
     * @test read
     */
    public function test_read_how_did_you_know()
    {
        $howDidYouKnow = factory(HowDidYouKnow::class)->create();

        $dbHowDidYouKnow = $this->howDidYouKnowRepo->find($howDidYouKnow->id);

        $dbHowDidYouKnow = $dbHowDidYouKnow->toArray();
        $this->assertModelData($howDidYouKnow->toArray(), $dbHowDidYouKnow);
    }

    /**
     * @test update
     */
    public function test_update_how_did_you_know()
    {
        $howDidYouKnow = factory(HowDidYouKnow::class)->create();
        $fakeHowDidYouKnow = factory(HowDidYouKnow::class)->make()->toArray();

        $updatedHowDidYouKnow = $this->howDidYouKnowRepo->update($fakeHowDidYouKnow, $howDidYouKnow->id);

        $this->assertModelData($fakeHowDidYouKnow, $updatedHowDidYouKnow->toArray());
        $dbHowDidYouKnow = $this->howDidYouKnowRepo->find($howDidYouKnow->id);
        $this->assertModelData($fakeHowDidYouKnow, $dbHowDidYouKnow->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_how_did_you_know()
    {
        $howDidYouKnow = factory(HowDidYouKnow::class)->create();

        $resp = $this->howDidYouKnowRepo->delete($howDidYouKnow->id);

        $this->assertTrue($resp);
        $this->assertNull(HowDidYouKnow::find($howDidYouKnow->id), 'HowDidYouKnow should not exist in DB');
    }
}
