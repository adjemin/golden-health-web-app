<?php namespace Tests\Repositories;

use App\Models\Qualification;
use App\Repositories\QualificationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class QualificationRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var QualificationRepository
     */
    protected $qualificationRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->qualificationRepo = \App::make(QualificationRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_qualification()
    {
        $qualification = factory(Qualification::class)->make()->toArray();

        $createdQualification = $this->qualificationRepo->create($qualification);

        $createdQualification = $createdQualification->toArray();
        $this->assertArrayHasKey('id', $createdQualification);
        $this->assertNotNull($createdQualification['id'], 'Created Qualification must have id specified');
        $this->assertNotNull(Qualification::find($createdQualification['id']), 'Qualification with given id must be in DB');
        $this->assertModelData($qualification, $createdQualification);
    }

    /**
     * @test read
     */
    public function test_read_qualification()
    {
        $qualification = factory(Qualification::class)->create();

        $dbQualification = $this->qualificationRepo->find($qualification->id);

        $dbQualification = $dbQualification->toArray();
        $this->assertModelData($qualification->toArray(), $dbQualification);
    }

    /**
     * @test update
     */
    public function test_update_qualification()
    {
        $qualification = factory(Qualification::class)->create();
        $fakeQualification = factory(Qualification::class)->make()->toArray();

        $updatedQualification = $this->qualificationRepo->update($fakeQualification, $qualification->id);

        $this->assertModelData($fakeQualification, $updatedQualification->toArray());
        $dbQualification = $this->qualificationRepo->find($qualification->id);
        $this->assertModelData($fakeQualification, $dbQualification->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_qualification()
    {
        $qualification = factory(Qualification::class)->create();

        $resp = $this->qualificationRepo->delete($qualification->id);

        $this->assertTrue($resp);
        $this->assertNull(Qualification::find($qualification->id), 'Qualification should not exist in DB');
    }
}
