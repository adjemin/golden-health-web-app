<?php namespace Tests\Repositories;

use App\Models\Advert;
use App\Repositories\AdvertRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AdvertRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AdvertRepository
     */
    protected $advertRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->advertRepo = \App::make(AdvertRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_advert()
    {
        $advert = factory(Advert::class)->make()->toArray();

        $createdAdvert = $this->advertRepo->create($advert);

        $createdAdvert = $createdAdvert->toArray();
        $this->assertArrayHasKey('id', $createdAdvert);
        $this->assertNotNull($createdAdvert['id'], 'Created Advert must have id specified');
        $this->assertNotNull(Advert::find($createdAdvert['id']), 'Advert with given id must be in DB');
        $this->assertModelData($advert, $createdAdvert);
    }

    /**
     * @test read
     */
    public function test_read_advert()
    {
        $advert = factory(Advert::class)->create();

        $dbAdvert = $this->advertRepo->find($advert->id);

        $dbAdvert = $dbAdvert->toArray();
        $this->assertModelData($advert->toArray(), $dbAdvert);
    }

    /**
     * @test update
     */
    public function test_update_advert()
    {
        $advert = factory(Advert::class)->create();
        $fakeAdvert = factory(Advert::class)->make()->toArray();

        $updatedAdvert = $this->advertRepo->update($fakeAdvert, $advert->id);

        $this->assertModelData($fakeAdvert, $updatedAdvert->toArray());
        $dbAdvert = $this->advertRepo->find($advert->id);
        $this->assertModelData($fakeAdvert, $dbAdvert->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_advert()
    {
        $advert = factory(Advert::class)->create();

        $resp = $this->advertRepo->delete($advert->id);

        $this->assertTrue($resp);
        $this->assertNull(Advert::find($advert->id), 'Advert should not exist in DB');
    }
}
