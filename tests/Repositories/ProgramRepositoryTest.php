<?php namespace Tests\Repositories;

use App\Models\Program;
use App\Repositories\ProgramRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProgramRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProgramRepository
     */
    protected $programRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->programRepo = \App::make(ProgramRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_program()
    {
        $program = factory(Program::class)->make()->toArray();

        $createdProgram = $this->programRepo->create($program);

        $createdProgram = $createdProgram->toArray();
        $this->assertArrayHasKey('id', $createdProgram);
        $this->assertNotNull($createdProgram['id'], 'Created Program must have id specified');
        $this->assertNotNull(Program::find($createdProgram['id']), 'Program with given id must be in DB');
        $this->assertModelData($program, $createdProgram);
    }

    /**
     * @test read
     */
    public function test_read_program()
    {
        $program = factory(Program::class)->create();

        $dbProgram = $this->programRepo->find($program->id);

        $dbProgram = $dbProgram->toArray();
        $this->assertModelData($program->toArray(), $dbProgram);
    }

    /**
     * @test update
     */
    public function test_update_program()
    {
        $program = factory(Program::class)->create();
        $fakeProgram = factory(Program::class)->make()->toArray();

        $updatedProgram = $this->programRepo->update($fakeProgram, $program->id);

        $this->assertModelData($fakeProgram, $updatedProgram->toArray());
        $dbProgram = $this->programRepo->find($program->id);
        $this->assertModelData($fakeProgram, $dbProgram->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_program()
    {
        $program = factory(Program::class)->create();

        $resp = $this->programRepo->delete($program->id);

        $this->assertTrue($resp);
        $this->assertNull(Program::find($program->id), 'Program should not exist in DB');
    }
}
