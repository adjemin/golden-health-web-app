<?php namespace Tests\Repositories;

use App\Models\courseLieu;
use App\Repositories\courseLieuRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class courseLieuRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var courseLieuRepository
     */
    protected $courseLieuRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->courseLieuRepo = \App::make(courseLieuRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_course_lieu()
    {
        $courseLieu = factory(courseLieu::class)->make()->toArray();

        $createdcourseLieu = $this->courseLieuRepo->create($courseLieu);

        $createdcourseLieu = $createdcourseLieu->toArray();
        $this->assertArrayHasKey('id', $createdcourseLieu);
        $this->assertNotNull($createdcourseLieu['id'], 'Created courseLieu must have id specified');
        $this->assertNotNull(courseLieu::find($createdcourseLieu['id']), 'courseLieu with given id must be in DB');
        $this->assertModelData($courseLieu, $createdcourseLieu);
    }

    /**
     * @test read
     */
    public function test_read_course_lieu()
    {
        $courseLieu = factory(courseLieu::class)->create();

        $dbcourseLieu = $this->courseLieuRepo->find($courseLieu->id);

        $dbcourseLieu = $dbcourseLieu->toArray();
        $this->assertModelData($courseLieu->toArray(), $dbcourseLieu);
    }

    /**
     * @test update
     */
    public function test_update_course_lieu()
    {
        $courseLieu = factory(courseLieu::class)->create();
        $fakecourseLieu = factory(courseLieu::class)->make()->toArray();

        $updatedcourseLieu = $this->courseLieuRepo->update($fakecourseLieu, $courseLieu->id);

        $this->assertModelData($fakecourseLieu, $updatedcourseLieu->toArray());
        $dbcourseLieu = $this->courseLieuRepo->find($courseLieu->id);
        $this->assertModelData($fakecourseLieu, $dbcourseLieu->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_course_lieu()
    {
        $courseLieu = factory(courseLieu::class)->create();

        $resp = $this->courseLieuRepo->delete($courseLieu->id);

        $this->assertTrue($resp);
        $this->assertNull(courseLieu::find($courseLieu->id), 'courseLieu should not exist in DB');
    }
}
