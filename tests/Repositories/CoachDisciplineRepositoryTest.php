<?php namespace Tests\Repositories;

use App\Models\CoachDiscipline;
use App\Repositories\CoachDisciplineRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CoachDisciplineRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CoachDisciplineRepository
     */
    protected $coachDisciplineRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->coachDisciplineRepo = \App::make(CoachDisciplineRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_coach_discipline()
    {
        $coachDiscipline = factory(CoachDiscipline::class)->make()->toArray();

        $createdCoachDiscipline = $this->coachDisciplineRepo->create($coachDiscipline);

        $createdCoachDiscipline = $createdCoachDiscipline->toArray();
        $this->assertArrayHasKey('id', $createdCoachDiscipline);
        $this->assertNotNull($createdCoachDiscipline['id'], 'Created CoachDiscipline must have id specified');
        $this->assertNotNull(CoachDiscipline::find($createdCoachDiscipline['id']), 'CoachDiscipline with given id must be in DB');
        $this->assertModelData($coachDiscipline, $createdCoachDiscipline);
    }

    /**
     * @test read
     */
    public function test_read_coach_discipline()
    {
        $coachDiscipline = factory(CoachDiscipline::class)->create();

        $dbCoachDiscipline = $this->coachDisciplineRepo->find($coachDiscipline->id);

        $dbCoachDiscipline = $dbCoachDiscipline->toArray();
        $this->assertModelData($coachDiscipline->toArray(), $dbCoachDiscipline);
    }

    /**
     * @test update
     */
    public function test_update_coach_discipline()
    {
        $coachDiscipline = factory(CoachDiscipline::class)->create();
        $fakeCoachDiscipline = factory(CoachDiscipline::class)->make()->toArray();

        $updatedCoachDiscipline = $this->coachDisciplineRepo->update($fakeCoachDiscipline, $coachDiscipline->id);

        $this->assertModelData($fakeCoachDiscipline, $updatedCoachDiscipline->toArray());
        $dbCoachDiscipline = $this->coachDisciplineRepo->find($coachDiscipline->id);
        $this->assertModelData($fakeCoachDiscipline, $dbCoachDiscipline->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_coach_discipline()
    {
        $coachDiscipline = factory(CoachDiscipline::class)->create();

        $resp = $this->coachDisciplineRepo->delete($coachDiscipline->id);

        $this->assertTrue($resp);
        $this->assertNull(CoachDiscipline::find($coachDiscipline->id), 'CoachDiscipline should not exist in DB');
    }
}
