<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CoachDiscipline;

class CoachDisciplineApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_coach_discipline()
    {
        $coachDiscipline = factory(CoachDiscipline::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/coach_disciplines', $coachDiscipline
        );

        $this->assertApiResponse($coachDiscipline);
    }

    /**
     * @test
     */
    public function test_read_coach_discipline()
    {
        $coachDiscipline = factory(CoachDiscipline::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/coach_disciplines/'.$coachDiscipline->id
        );

        $this->assertApiResponse($coachDiscipline->toArray());
    }

    /**
     * @test
     */
    public function test_update_coach_discipline()
    {
        $coachDiscipline = factory(CoachDiscipline::class)->create();
        $editedCoachDiscipline = factory(CoachDiscipline::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/coach_disciplines/'.$coachDiscipline->id,
            $editedCoachDiscipline
        );

        $this->assertApiResponse($editedCoachDiscipline);
    }

    /**
     * @test
     */
    public function test_delete_coach_discipline()
    {
        $coachDiscipline = factory(CoachDiscipline::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/coach_disciplines/'.$coachDiscipline->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/coach_disciplines/'.$coachDiscipline->id
        );

        $this->response->assertStatus(404);
    }
}
