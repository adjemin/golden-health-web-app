<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\courseDetails;

class courseDetailsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_course_details()
    {
        $courseDetails = factory(courseDetails::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/course_details', $courseDetails
        );

        $this->assertApiResponse($courseDetails);
    }

    /**
     * @test
     */
    public function test_read_course_details()
    {
        $courseDetails = factory(courseDetails::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/course_details/'.$courseDetails->id
        );

        $this->assertApiResponse($courseDetails->toArray());
    }

    /**
     * @test
     */
    public function test_update_course_details()
    {
        $courseDetails = factory(courseDetails::class)->create();
        $editedcourseDetails = factory(courseDetails::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/course_details/'.$courseDetails->id,
            $editedcourseDetails
        );

        $this->assertApiResponse($editedcourseDetails);
    }

    /**
     * @test
     */
    public function test_delete_course_details()
    {
        $courseDetails = factory(courseDetails::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/course_details/'.$courseDetails->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/course_details/'.$courseDetails->id
        );

        $this->response->assertStatus(404);
    }
}
