<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Availability;

class AvailabilityApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_availability()
    {
        $availability = factory(Availability::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/availabilities', $availability
        );

        $this->assertApiResponse($availability);
    }

    /**
     * @test
     */
    public function test_read_availability()
    {
        $availability = factory(Availability::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/availabilities/'.$availability->id
        );

        $this->assertApiResponse($availability->toArray());
    }

    /**
     * @test
     */
    public function test_update_availability()
    {
        $availability = factory(Availability::class)->create();
        $editedAvailability = factory(Availability::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/availabilities/'.$availability->id,
            $editedAvailability
        );

        $this->assertApiResponse($editedAvailability);
    }

    /**
     * @test
     */
    public function test_delete_availability()
    {
        $availability = factory(Availability::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/availabilities/'.$availability->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/availabilities/'.$availability->id
        );

        $this->response->assertStatus(404);
    }
}
