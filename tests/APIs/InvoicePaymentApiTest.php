<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\InvoicePayment;

class InvoicePaymentApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_invoice_payment()
    {
        $InvoicePayment = factory(InvoicePayment::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/invoice_payments', $InvoicePayment
        );

        $this->assertApiResponse($InvoicePayment);
    }

    /**
     * @test
     */
    public function test_read_invoice_payment()
    {
        $InvoicePayment = factory(InvoicePayment::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/invoice_payments/'.$InvoicePayment->id
        );

        $this->assertApiResponse($InvoicePayment->toArray());
    }

    /**
     * @test
     */
    public function test_update_invoice_payment()
    {
        $InvoicePayment = factory(InvoicePayment::class)->create();
        $editedInvoicePayment = factory(InvoicePayment::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/invoice_payments/'.$InvoicePayment->id,
            $editedInvoicePayment
        );

        $this->assertApiResponse($editedInvoicePayment);
    }

    /**
     * @test
     */
    public function test_delete_invoice_payment()
    {
        $InvoicePayment = factory(InvoicePayment::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/invoice_payments/'.$InvoicePayment->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/invoice_payments/'.$InvoicePayment->id
        );

        $this->response->assertStatus(404);
    }
}
