<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Pack;

class PackApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_pack()
    {
        $pack = factory(Pack::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/packs', $pack
        );

        $this->assertApiResponse($pack);
    }

    /**
     * @test
     */
    public function test_read_pack()
    {
        $pack = factory(Pack::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/packs/'.$pack->id
        );

        $this->assertApiResponse($pack->toArray());
    }

    /**
     * @test
     */
    public function test_update_pack()
    {
        $pack = factory(Pack::class)->create();
        $editedPack = factory(Pack::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/packs/'.$pack->id,
            $editedPack
        );

        $this->assertApiResponse($editedPack);
    }

    /**
     * @test
     */
    public function test_delete_pack()
    {
        $pack = factory(Pack::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/packs/'.$pack->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/packs/'.$pack->id
        );

        $this->response->assertStatus(404);
    }
}
