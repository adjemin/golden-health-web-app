<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\BodyMassIndex;

class BodyMassIndexApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_body_mass_index()
    {
        $bodyMassIndex = factory(BodyMassIndex::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/body_mass_indices', $bodyMassIndex
        );

        $this->assertApiResponse($bodyMassIndex);
    }

    /**
     * @test
     */
    public function test_read_body_mass_index()
    {
        $bodyMassIndex = factory(BodyMassIndex::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/body_mass_indices/'.$bodyMassIndex->id
        );

        $this->assertApiResponse($bodyMassIndex->toArray());
    }

    /**
     * @test
     */
    public function test_update_body_mass_index()
    {
        $bodyMassIndex = factory(BodyMassIndex::class)->create();
        $editedBodyMassIndex = factory(BodyMassIndex::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/body_mass_indices/'.$bodyMassIndex->id,
            $editedBodyMassIndex
        );

        $this->assertApiResponse($editedBodyMassIndex);
    }

    /**
     * @test
     */
    public function test_delete_body_mass_index()
    {
        $bodyMassIndex = factory(BodyMassIndex::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/body_mass_indices/'.$bodyMassIndex->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/body_mass_indices/'.$bodyMassIndex->id
        );

        $this->response->assertStatus(404);
    }
}
