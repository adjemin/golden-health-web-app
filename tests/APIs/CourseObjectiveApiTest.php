<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CourseObjective;

class CourseObjectiveApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_course_objective()
    {
        $courseObjective = factory(CourseObjective::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/course_objectives', $courseObjective
        );

        $this->assertApiResponse($courseObjective);
    }

    /**
     * @test
     */
    public function test_read_course_objective()
    {
        $courseObjective = factory(CourseObjective::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/course_objectives/'.$courseObjective->id
        );

        $this->assertApiResponse($courseObjective->toArray());
    }

    /**
     * @test
     */
    public function test_update_course_objective()
    {
        $courseObjective = factory(CourseObjective::class)->create();
        $editedCourseObjective = factory(CourseObjective::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/course_objectives/'.$courseObjective->id,
            $editedCourseObjective
        );

        $this->assertApiResponse($editedCourseObjective);
    }

    /**
     * @test
     */
    public function test_delete_course_objective()
    {
        $courseObjective = factory(CourseObjective::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/course_objectives/'.$courseObjective->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/course_objectives/'.$courseObjective->id
        );

        $this->response->assertStatus(404);
    }
}
