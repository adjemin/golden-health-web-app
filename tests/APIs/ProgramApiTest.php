<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Program;

class ProgramApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_program()
    {
        $program = factory(Program::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/programs', $program
        );

        $this->assertApiResponse($program);
    }

    /**
     * @test
     */
    public function test_read_program()
    {
        $program = factory(Program::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/programs/'.$program->id
        );

        $this->assertApiResponse($program->toArray());
    }

    /**
     * @test
     */
    public function test_update_program()
    {
        $program = factory(Program::class)->create();
        $editedProgram = factory(Program::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/programs/'.$program->id,
            $editedProgram
        );

        $this->assertApiResponse($editedProgram);
    }

    /**
     * @test
     */
    public function test_delete_program()
    {
        $program = factory(Program::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/programs/'.$program->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/programs/'.$program->id
        );

        $this->response->assertStatus(404);
    }
}
