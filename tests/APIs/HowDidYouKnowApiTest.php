<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\HowDidYouKnow;

class HowDidYouKnowApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_how_did_you_know()
    {
        $howDidYouKnow = factory(HowDidYouKnow::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/how_did_you_knows', $howDidYouKnow
        );

        $this->assertApiResponse($howDidYouKnow);
    }

    /**
     * @test
     */
    public function test_read_how_did_you_know()
    {
        $howDidYouKnow = factory(HowDidYouKnow::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/how_did_you_knows/'.$howDidYouKnow->id
        );

        $this->assertApiResponse($howDidYouKnow->toArray());
    }

    /**
     * @test
     */
    public function test_update_how_did_you_know()
    {
        $howDidYouKnow = factory(HowDidYouKnow::class)->create();
        $editedHowDidYouKnow = factory(HowDidYouKnow::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/how_did_you_knows/'.$howDidYouKnow->id,
            $editedHowDidYouKnow
        );

        $this->assertApiResponse($editedHowDidYouKnow);
    }

    /**
     * @test
     */
    public function test_delete_how_did_you_know()
    {
        $howDidYouKnow = factory(HowDidYouKnow::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/how_did_you_knows/'.$howDidYouKnow->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/how_did_you_knows/'.$howDidYouKnow->id
        );

        $this->response->assertStatus(404);
    }
}
