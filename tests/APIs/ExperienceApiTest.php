<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Experience;

class ExperienceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_experience()
    {
        $experience = factory(Experience::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/experiences', $experience
        );

        $this->assertApiResponse($experience);
    }

    /**
     * @test
     */
    public function test_read_experience()
    {
        $experience = factory(Experience::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/experiences/'.$experience->id
        );

        $this->assertApiResponse($experience->toArray());
    }

    /**
     * @test
     */
    public function test_update_experience()
    {
        $experience = factory(Experience::class)->create();
        $editedExperience = factory(Experience::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/experiences/'.$experience->id,
            $editedExperience
        );

        $this->assertApiResponse($editedExperience);
    }

    /**
     * @test
     */
    public function test_delete_experience()
    {
        $experience = factory(Experience::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/experiences/'.$experience->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/experiences/'.$experience->id
        );

        $this->response->assertStatus(404);
    }
}
