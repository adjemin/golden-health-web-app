<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\RecapBMI;

class RecapBMIApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_recap_b_m_i()
    {
        $recapBMI = factory(RecapBMI::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/recap_b_m_is', $recapBMI
        );

        $this->assertApiResponse($recapBMI);
    }

    /**
     * @test
     */
    public function test_read_recap_b_m_i()
    {
        $recapBMI = factory(RecapBMI::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/recap_b_m_is/'.$recapBMI->id
        );

        $this->assertApiResponse($recapBMI->toArray());
    }

    /**
     * @test
     */
    public function test_update_recap_b_m_i()
    {
        $recapBMI = factory(RecapBMI::class)->create();
        $editedRecapBMI = factory(RecapBMI::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/recap_b_m_is/'.$recapBMI->id,
            $editedRecapBMI
        );

        $this->assertApiResponse($editedRecapBMI);
    }

    /**
     * @test
     */
    public function test_delete_recap_b_m_i()
    {
        $recapBMI = factory(RecapBMI::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/recap_b_m_is/'.$recapBMI->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/recap_b_m_is/'.$recapBMI->id
        );

        $this->response->assertStatus(404);
    }
}
