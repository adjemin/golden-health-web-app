<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\LieuCourse;

class LieuCourseApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_lieu_course()
    {
        $lieuCourse = factory(LieuCourse::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/lieu_courses', $lieuCourse
        );

        $this->assertApiResponse($lieuCourse);
    }

    /**
     * @test
     */
    public function test_read_lieu_course()
    {
        $lieuCourse = factory(LieuCourse::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/lieu_courses/'.$lieuCourse->id
        );

        $this->assertApiResponse($lieuCourse->toArray());
    }

    /**
     * @test
     */
    public function test_update_lieu_course()
    {
        $lieuCourse = factory(LieuCourse::class)->create();
        $editedLieuCourse = factory(LieuCourse::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/lieu_courses/'.$lieuCourse->id,
            $editedLieuCourse
        );

        $this->assertApiResponse($editedLieuCourse);
    }

    /**
     * @test
     */
    public function test_delete_lieu_course()
    {
        $lieuCourse = factory(LieuCourse::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/lieu_courses/'.$lieuCourse->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/lieu_courses/'.$lieuCourse->id
        );

        $this->response->assertStatus(404);
    }
}
