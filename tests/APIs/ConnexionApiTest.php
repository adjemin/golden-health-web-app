<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Connexion;

class ConnexionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_connexion()
    {
        $connexion = factory(Connexion::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/connexions', $connexion
        );

        $this->assertApiResponse($connexion);
    }

    /**
     * @test
     */
    public function test_read_connexion()
    {
        $connexion = factory(Connexion::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/connexions/'.$connexion->id
        );

        $this->assertApiResponse($connexion->toArray());
    }

    /**
     * @test
     */
    public function test_update_connexion()
    {
        $connexion = factory(Connexion::class)->create();
        $editedConnexion = factory(Connexion::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/connexions/'.$connexion->id,
            $editedConnexion
        );

        $this->assertApiResponse($editedConnexion);
    }

    /**
     * @test
     */
    public function test_delete_connexion()
    {
        $connexion = factory(Connexion::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/connexions/'.$connexion->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/connexions/'.$connexion->id
        );

        $this->response->assertStatus(404);
    }
}
