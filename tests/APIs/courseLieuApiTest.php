<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\courseLieu;

class courseLieuApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_course_lieu()
    {
        $courseLieu = factory(courseLieu::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/course_lieus', $courseLieu
        );

        $this->assertApiResponse($courseLieu);
    }

    /**
     * @test
     */
    public function test_read_course_lieu()
    {
        $courseLieu = factory(courseLieu::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/course_lieus/'.$courseLieu->id
        );

        $this->assertApiResponse($courseLieu->toArray());
    }

    /**
     * @test
     */
    public function test_update_course_lieu()
    {
        $courseLieu = factory(courseLieu::class)->create();
        $editedcourseLieu = factory(courseLieu::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/course_lieus/'.$courseLieu->id,
            $editedcourseLieu
        );

        $this->assertApiResponse($editedcourseLieu);
    }

    /**
     * @test
     */
    public function test_delete_course_lieu()
    {
        $courseLieu = factory(courseLieu::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/course_lieus/'.$courseLieu->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/course_lieus/'.$courseLieu->id
        );

        $this->response->assertStatus(404);
    }
}
