<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\QualificationType;

class QualificationTypeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_qualification_type()
    {
        $qualificationType = factory(QualificationType::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/qualification_types', $qualificationType
        );

        $this->assertApiResponse($qualificationType);
    }

    /**
     * @test
     */
    public function test_read_qualification_type()
    {
        $qualificationType = factory(QualificationType::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/qualification_types/'.$qualificationType->id
        );

        $this->assertApiResponse($qualificationType->toArray());
    }

    /**
     * @test
     */
    public function test_update_qualification_type()
    {
        $qualificationType = factory(QualificationType::class)->create();
        $editedQualificationType = factory(QualificationType::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/qualification_types/'.$qualificationType->id,
            $editedQualificationType
        );

        $this->assertApiResponse($editedQualificationType);
    }

    /**
     * @test
     */
    public function test_delete_qualification_type()
    {
        $qualificationType = factory(QualificationType::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/qualification_types/'.$qualificationType->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/qualification_types/'.$qualificationType->id
        );

        $this->response->assertStatus(404);
    }
}
