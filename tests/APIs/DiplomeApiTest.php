<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Diplome;

class DiplomeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_diplome()
    {
        $diplome = factory(Diplome::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/diplomes', $diplome
        );

        $this->assertApiResponse($diplome);
    }

    /**
     * @test
     */
    public function test_read_diplome()
    {
        $diplome = factory(Diplome::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/diplomes/'.$diplome->id
        );

        $this->assertApiResponse($diplome->toArray());
    }

    /**
     * @test
     */
    public function test_update_diplome()
    {
        $diplome = factory(Diplome::class)->create();
        $editedDiplome = factory(Diplome::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/diplomes/'.$diplome->id,
            $editedDiplome
        );

        $this->assertApiResponse($editedDiplome);
    }

    /**
     * @test
     */
    public function test_delete_diplome()
    {
        $diplome = factory(Diplome::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/diplomes/'.$diplome->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/diplomes/'.$diplome->id
        );

        $this->response->assertStatus(404);
    }
}
