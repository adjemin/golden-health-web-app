<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ClientTestimony;

class ClientTestimonyApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_client_testimony()
    {
        $clientTestimony = factory(ClientTestimony::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/client_testimonies', $clientTestimony
        );

        $this->assertApiResponse($clientTestimony);
    }

    /**
     * @test
     */
    public function test_read_client_testimony()
    {
        $clientTestimony = factory(ClientTestimony::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/client_testimonies/'.$clientTestimony->id
        );

        $this->assertApiResponse($clientTestimony->toArray());
    }

    /**
     * @test
     */
    public function test_update_client_testimony()
    {
        $clientTestimony = factory(ClientTestimony::class)->create();
        $editedClientTestimony = factory(ClientTestimony::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/client_testimonies/'.$clientTestimony->id,
            $editedClientTestimony
        );

        $this->assertApiResponse($editedClientTestimony);
    }

    /**
     * @test
     */
    public function test_delete_client_testimony()
    {
        $clientTestimony = factory(ClientTestimony::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/client_testimonies/'.$clientTestimony->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/client_testimonies/'.$clientTestimony->id
        );

        $this->response->assertStatus(404);
    }
}
