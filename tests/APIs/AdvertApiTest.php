<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Advert;

class AdvertApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_advert()
    {
        $advert = factory(Advert::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/adverts', $advert
        );

        $this->assertApiResponse($advert);
    }

    /**
     * @test
     */
    public function test_read_advert()
    {
        $advert = factory(Advert::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/adverts/'.$advert->id
        );

        $this->assertApiResponse($advert->toArray());
    }

    /**
     * @test
     */
    public function test_update_advert()
    {
        $advert = factory(Advert::class)->create();
        $editedAdvert = factory(Advert::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/adverts/'.$advert->id,
            $editedAdvert
        );

        $this->assertApiResponse($editedAdvert);
    }

    /**
     * @test
     */
    public function test_delete_advert()
    {
        $advert = factory(Advert::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/adverts/'.$advert->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/adverts/'.$advert->id
        );

        $this->response->assertStatus(404);
    }
}
