<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Discipline;

class DisciplineApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_discipline()
    {
        $discipline = factory(Discipline::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/disciplines', $discipline
        );

        $this->assertApiResponse($discipline);
    }

    /**
     * @test
     */
    public function test_read_discipline()
    {
        $discipline = factory(Discipline::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/disciplines/'.$discipline->id
        );

        $this->assertApiResponse($discipline->toArray());
    }

    /**
     * @test
     */
    public function test_update_discipline()
    {
        $discipline = factory(Discipline::class)->create();
        $editedDiscipline = factory(Discipline::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/disciplines/'.$discipline->id,
            $editedDiscipline
        );

        $this->assertApiResponse($editedDiscipline);
    }

    /**
     * @test
     */
    public function test_delete_discipline()
    {
        $discipline = factory(Discipline::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/disciplines/'.$discipline->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/disciplines/'.$discipline->id
        );

        $this->response->assertStatus(404);
    }
}
