<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Advice;

class AdviceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_advice()
    {
        $advice = factory(Advice::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/advice', $advice
        );

        $this->assertApiResponse($advice);
    }

    /**
     * @test
     */
    public function test_read_advice()
    {
        $advice = factory(Advice::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/advice/'.$advice->id
        );

        $this->assertApiResponse($advice->toArray());
    }

    /**
     * @test
     */
    public function test_update_advice()
    {
        $advice = factory(Advice::class)->create();
        $editedAdvice = factory(Advice::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/advice/'.$advice->id,
            $editedAdvice
        );

        $this->assertApiResponse($editedAdvice);
    }

    /**
     * @test
     */
    public function test_delete_advice()
    {
        $advice = factory(Advice::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/advice/'.$advice->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/advice/'.$advice->id
        );

        $this->response->assertStatus(404);
    }
}
