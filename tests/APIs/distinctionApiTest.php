<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\distinction;

class distinctionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_distinction()
    {
        $distinction = factory(distinction::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/distinctions', $distinction
        );

        $this->assertApiResponse($distinction);
    }

    /**
     * @test
     */
    public function test_read_distinction()
    {
        $distinction = factory(distinction::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/distinctions/'.$distinction->id
        );

        $this->assertApiResponse($distinction->toArray());
    }

    /**
     * @test
     */
    public function test_update_distinction()
    {
        $distinction = factory(distinction::class)->create();
        $editeddistinction = factory(distinction::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/distinctions/'.$distinction->id,
            $editeddistinction
        );

        $this->assertApiResponse($editeddistinction);
    }

    /**
     * @test
     */
    public function test_delete_distinction()
    {
        $distinction = factory(distinction::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/distinctions/'.$distinction->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/distinctions/'.$distinction->id
        );

        $this->response->assertStatus(404);
    }
}
