<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CustomerNotification;

class CustomerNotificationApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_customer_notification()
    {
        $customerNotification = factory(CustomerNotification::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/customer_notifications', $customerNotification
        );

        $this->assertApiResponse($customerNotification);
    }

    /**
     * @test
     */
    public function test_read_customer_notification()
    {
        $customerNotification = factory(CustomerNotification::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/customer_notifications/'.$customerNotification->id
        );

        $this->assertApiResponse($customerNotification->toArray());
    }

    /**
     * @test
     */
    public function test_update_customer_notification()
    {
        $customerNotification = factory(CustomerNotification::class)->create();
        $editedCustomerNotification = factory(CustomerNotification::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/customer_notifications/'.$customerNotification->id,
            $editedCustomerNotification
        );

        $this->assertApiResponse($editedCustomerNotification);
    }

    /**
     * @test
     */
    public function test_delete_customer_notification()
    {
        $customerNotification = factory(CustomerNotification::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/customer_notifications/'.$customerNotification->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/customer_notifications/'.$customerNotification->id
        );

        $this->response->assertStatus(404);
    }
}
