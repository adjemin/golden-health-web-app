<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\BookingCoach;

class BookingCoachApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_booking_coach()
    {
        $bookingCoach = factory(BookingCoach::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/booking_coaches', $bookingCoach
        );

        $this->assertApiResponse($bookingCoach);
    }

    /**
     * @test
     */
    public function test_read_booking_coach()
    {
        $bookingCoach = factory(BookingCoach::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/booking_coaches/'.$bookingCoach->id
        );

        $this->assertApiResponse($bookingCoach->toArray());
    }

    /**
     * @test
     */
    public function test_update_booking_coach()
    {
        $bookingCoach = factory(BookingCoach::class)->create();
        $editedBookingCoach = factory(BookingCoach::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/booking_coaches/'.$bookingCoach->id,
            $editedBookingCoach
        );

        $this->assertApiResponse($editedBookingCoach);
    }

    /**
     * @test
     */
    public function test_delete_booking_coach()
    {
        $bookingCoach = factory(BookingCoach::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/booking_coaches/'.$bookingCoach->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/booking_coaches/'.$bookingCoach->id
        );

        $this->response->assertStatus(404);
    }
}
