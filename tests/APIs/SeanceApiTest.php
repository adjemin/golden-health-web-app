<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Seance;

class SeanceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_seance()
    {
        $seance = factory(Seance::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/seances', $seance
        );

        $this->assertApiResponse($seance);
    }

    /**
     * @test
     */
    public function test_read_seance()
    {
        $seance = factory(Seance::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/seances/'.$seance->id
        );

        $this->assertApiResponse($seance->toArray());
    }

    /**
     * @test
     */
    public function test_update_seance()
    {
        $seance = factory(Seance::class)->create();
        $editedSeance = factory(Seance::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/seances/'.$seance->id,
            $editedSeance
        );

        $this->assertApiResponse($editedSeance);
    }

    /**
     * @test
     */
    public function test_delete_seance()
    {
        $seance = factory(Seance::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/seances/'.$seance->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/seances/'.$seance->id
        );

        $this->response->assertStatus(404);
    }
}
