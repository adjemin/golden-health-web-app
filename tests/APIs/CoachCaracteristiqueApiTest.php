<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CoachCaracteristique;

class CoachCaracteristiqueApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_coach_caracteristique()
    {
        $coachCaracteristique = factory(CoachCaracteristique::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/coach_caracteristiques', $coachCaracteristique
        );

        $this->assertApiResponse($coachCaracteristique);
    }

    /**
     * @test
     */
    public function test_read_coach_caracteristique()
    {
        $coachCaracteristique = factory(CoachCaracteristique::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/coach_caracteristiques/'.$coachCaracteristique->id
        );

        $this->assertApiResponse($coachCaracteristique->toArray());
    }

    /**
     * @test
     */
    public function test_update_coach_caracteristique()
    {
        $coachCaracteristique = factory(CoachCaracteristique::class)->create();
        $editedCoachCaracteristique = factory(CoachCaracteristique::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/coach_caracteristiques/'.$coachCaracteristique->id,
            $editedCoachCaracteristique
        );

        $this->assertApiResponse($editedCoachCaracteristique);
    }

    /**
     * @test
     */
    public function test_delete_coach_caracteristique()
    {
        $coachCaracteristique = factory(CoachCaracteristique::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/coach_caracteristiques/'.$coachCaracteristique->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/coach_caracteristiques/'.$coachCaracteristique->id
        );

        $this->response->assertStatus(404);
    }
}
