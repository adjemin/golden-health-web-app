<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Objective;

class ObjectiveApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_objective()
    {
        $objective = factory(Objective::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/objectives', $objective
        );

        $this->assertApiResponse($objective);
    }

    /**
     * @test
     */
    public function test_read_objective()
    {
        $objective = factory(Objective::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/objectives/'.$objective->id
        );

        $this->assertApiResponse($objective->toArray());
    }

    /**
     * @test
     */
    public function test_update_objective()
    {
        $objective = factory(Objective::class)->create();
        $editedObjective = factory(Objective::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/objectives/'.$objective->id,
            $editedObjective
        );

        $this->assertApiResponse($editedObjective);
    }

    /**
     * @test
     */
    public function test_delete_objective()
    {
        $objective = factory(Objective::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/objectives/'.$objective->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/objectives/'.$objective->id
        );

        $this->response->assertStatus(404);
    }
}
