<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PackSubscription;

class PackSubscriptionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_pack_subscription()
    {
        $packSubscription = factory(PackSubscription::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/pack_subscriptions', $packSubscription
        );

        $this->assertApiResponse($packSubscription);
    }

    /**
     * @test
     */
    public function test_read_pack_subscription()
    {
        $packSubscription = factory(PackSubscription::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/pack_subscriptions/'.$packSubscription->id
        );

        $this->assertApiResponse($packSubscription->toArray());
    }

    /**
     * @test
     */
    public function test_update_pack_subscription()
    {
        $packSubscription = factory(PackSubscription::class)->create();
        $editedPackSubscription = factory(PackSubscription::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/pack_subscriptions/'.$packSubscription->id,
            $editedPackSubscription
        );

        $this->assertApiResponse($editedPackSubscription);
    }

    /**
     * @test
     */
    public function test_delete_pack_subscription()
    {
        $packSubscription = factory(PackSubscription::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/pack_subscriptions/'.$packSubscription->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/pack_subscriptions/'.$packSubscription->id
        );

        $this->response->assertStatus(404);
    }
}
