
$(document).ready(function () {
    $token = $("input[name='_token']").val();

    $.ajaxSetup({
        headers: {
            // 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            "X-CSRF-TOKEN": $token
        }
    });

    $modal = $("#cartModal");
    // popsUp for quick testing
    // setTimeout(function () {
    //     if (typeof $modal != undefined) {

    //         $modal.css('display', 'block');
    //     }
    // }, 1000);

    $(".openCartModal").click(function() {
        $modal.css("display", "block");
    });
    $(".cart-modal-close").click(function () {
        $modal.css("display", "none");
    });
});
