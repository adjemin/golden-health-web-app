// **** Regroupe toutes les opérations sur un produit

$(document).ready(function() {
    $token = $("input[name='_token']").val();

    $.ajaxSetup({
        headers: {
            // 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            "X-CSRF-TOKEN": $token
        }
    });

    product = $("#productData");
    $processUrl = product.data("process-url");

    $order = {
        item: {
            itemId: product.data("id"),
            slug: product.data("slug"),
            title: product.data("title"),
            price: product.data("price"),
            description: product.data("description")
        },
        quantity: 1,
        amount: 1 * product.data("price"),
        delivery_fees: 0,
        taxes: 0,
        total: 0,
        // delivery
        delivery: {
            address: "",
            lat: "",
            lon: ""
        },
        payment: {
            method: "cash"
        }
    };
    // Order operations setup
    function setState(time = 300) {
        setInterval(() => {
            $order.amount = $order.quantity * $order.item.price;
            $order.total = $order.amount + $order.delivery_fees + $order.taxes;
            $order.delivery.address = $("#locat_delivery").val();
            $order.delivery.lat = $("#lat_delivery").val();
            $order.delivery.lon = $("#lon_delivery").val();
            //
            $("#orderQty").html($order.quantity);
            $("#orderAmount").html($order.amount);
            $("#orderTotal").html($order.total);

            // review

            $("#orderReviewItemTitle").html($order.item.title);
            $("#orderReviewPrice").html($order.item.price);
            $("#orderReviewQuantity").html($order.quantity);
            $("#orderReviewAmount").html($order.amount);
            $("#orderReviewTaxes").html($order.taxes);
            $("#orderReviewDeliveryFees").html($order.delivery_fees);
            if ($order.payment.method == "cash") {
                $("#orderReviewPaymentMethod").html("cash à la livraison");
            } else if ($order.payment.method == "online") {
                $("#orderReviewPaymentMethod").html("payment en ligne");
            }

            $("#orderReviewTotal").html($order.total);
            // payment

            // delivery
            $("#orderReviewDeliveryAddress").html($order.delivery.address);
        }, time);
    }

    // Token value for ajax requests

    // *** Ajout d'un produit comme favori
    $("#toggleFavoriteBtn").click(function() {
        console.log("Toggling :");
        var url = $(this).data("url");
        var productData = $("#product-data");
        var data = {
            product_id: productData.data("id")
            // '_token': $token
        };
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "json",
            success: function(response) {
                if (response.code == 00) {
                    // icon.setAttribute("class", "fa fa heart-0")
                    $("#toggleFavoriteBtn").html(
                        '<i class="fa fa-heart-o" aria-hidden="true" style="font-size: 50px"></i>'
                    );
                } else if (response.code == 11) {
                    $("#toggleFavoriteBtn").html(
                        '<i class="fa fa-heart" aria-hidden="true" style="font-size: 50px"></i>'
                    );
                }
            },
            error: function(response) {
                return false;
            }
        });
    });

    // ************************** //
    // *** Commande d'un article
    // ************************** //

    // Refreshes the displayed data each 300 milliseconds
    setState();
    // Quantity controllers
    $("#decrementQtyBtn").click(function() {
        if ($order.quantity > 1) {
            $order.quantity--;
        }
    });
    $("#incrementQtyBtn").click(function() {
        // if ($order.quantity > 0) {
        $order.quantity++;
        // }
    });

    //  Order modal
    $modal = $("#cartScreenModal");
    // popsUp for quick testing
    // setTimeout(function () {
    //     if (typeof $modal != undefined) {

    //         $modal.css('display', 'block');
    //     }
    // }, 1000);

    $("#openOrderModal").click(function() {
        $modal.css("display", "block");
    });
    $(".cart-screen-modal-close").click(function() {
        $modal.css("display", "none");
    });

    // *** Order calculations
    $("#decrementQtyBtn").click(function() {});
    $("#decrementQtyBtn").click(function() {});

    // *** Launching order backend processing
    $("#orderFinalizeBtn").click(function() {
        gotoNextTab(this);
        processOrder();
    });

    // **** Order form validation
    // ** Delivery location
    $("#delivery_tab_next_button").click(function() {
        var location = $("#locat_delivery").val();
        console.log(location);
        if (location == "") {
            // TODO make a cuter validation
            alert("Veuillez choisir une addresse de livraison");
            return false;
        } else {
            gotoNextTab(this);
        }
    });

    // **** PROCESSING ORDER IN AJAX
    function processOrder() {
        $.ajax({
            type: "POST",
            url: $processUrl,
            data: $order,
            dataType: "json",
            success: function(response) {
                console.log(response);

                if (response.code == 11) {
                    gotoNextTab("#gotoFailTabBtn");
                    gotoNextTab("#gotoSuccessTabBtn");
                    $("#responseMessage").html(response.message);
                    $("#openOrderModal").html(
                        '<i class="fa fa-check mr-2" aria-hidden="true"></i> Commandé'
                    );
                } else if (response.code == 00) {
                    // goto success
                    gotoNextTab("#gotoFailTabBtn");
                    $("#checkout").css("color", "red");
                    $("#responseMessage").html(response.message);
                } else {
                    gotoNextTab("#gotoFailTabBtn");
                    var checkout = $("#checkout");
                    // "\f00d"
                    checkout.removeClass("active");
                    checkout.addClass("failed");
                    // console.log(checkout);
                    $("#responseMessage").html(response.message);
                    console.log(response);
                }
                clearModal(5000);
            },
            error: function(response) {
                console.log("Error :");
                console.log(response);
                gotoNextTab("#gotoFailTabBtn");
                // displaying feedback
                $("#checkout").css("color", "red !important;");
                $("#responseMessage").html(
                    "Nous rencontrons un souci coté serveur. Merci de réessayer 😅"
                );
                clearModal(7000);
                return false;
            }
        });
    }

    function clearModal(delay) {
        setTimeout(function() {
            if (typeof $modal != undefined) {
                $modal.css("display", "none");
            }
        }, delay);
    }

    // **** ORDER FORM JS

    $(".next").click(function() {
        current_fs = $(this).parent();
        next_fs = $(this)
            .parent()
            .next();

        //Add Class Active
        $("#progressbar li")
            .eq($("fieldset").index(next_fs))
            .addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate(
            {
                opacity: 0
            },
            {
                step: function(now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        display: "none",
                        position: "relative"
                    });
                    next_fs.css({
                        opacity: opacity
                    });
                },
                duration: 600
            }
        );
    });

    function gotoNextTab(current) {
        current_fs = $(current).parent();
        next_fs = $(current)
            .parent()
            .next();

        //Add Class Active
        $("#progressbar li")
            .eq($("fieldset").index(next_fs))
            .addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate(
            {
                opacity: 0
            },
            {
                step: function(now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        display: "none",
                        position: "relative"
                    });
                    next_fs.css({
                        opacity: opacity
                    });
                },
                duration: 600
            }
        );
    }

    $(".previous").click(function() {
        current_fs = $(this).parent();
        previous_fs = $(this)
            .parent()
            .prev();

        //Remove class active
        $("#progressbar li")
            .eq($("fieldset").index(current_fs))
            .removeClass("active");

        //show the previous fieldset
        previous_fs.show();

        //hide the current fieldset with style
        current_fs.animate(
            {
                opacity: 0
            },
            {
                step: function(now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        display: "none",
                        position: "relative"
                    });
                    previous_fs.css({
                        opacity: opacity
                    });
                },
                duration: 600
            }
        );
    });

    $(".radio-group .radio").click(function() {
        $(this)
            .parent()
            .find(".radio")
            .removeClass("selected");
        $(this).addClass("selected");
    });

    $(".submit").click(function() {
        return false;
    });
});
