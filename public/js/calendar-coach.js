$(function () {


var today = new Date();

var events = [];
var new_events = [];
var array_availabity = [];
var all_availabity = [];
var tableAvailable;
/*console.log(today.getMonth() + 1 + "/18/" + today.getFullYear());
var events = [ {
    id: "imwyx6S",
    name: "Cours Yoga #3",
    time_start: "10:00",
    time_end: "13:00",
    date: today.getMonth() + 1 + "/18/" + today.getFullYear(),
    type: "event"
},
{
    id: "imwy34SFG",
    name: "Cours Pilates #4",
    time_start: "15:00",
    time_end: "17:00",
    date: today.getMonth() + 1 + "/18/" + today.getFullYear(),
    type: "holiday"
},
{
    id: "9jU6g6f",
    name: "Holiday #1",
    time_start: "10:00",
    time_end: "13:00",
    date: today.getMonth() + 1 + "/10/" + today.getFullYear(),
    type: "holiday"
}];*/

//$('#calendar-coach').evoCalendar();

$('#current_course_id').val()
tableAvailable = $('#availabityTable').DataTable({
    ajax: {
       url: "/api/availabilities?course_id="+$('#current_course_id').val(),
       method: "GET",
    },
    columns: [
        { data: "date_debut" },
        { data: "date_fin" },
        { data: null,
          render: function(data,type,full,meta) {
            return Math.floor((new Date(data.date_fin) - new Date(data.date_debut))/(1000*60*60*24));
          }
        },
        { data: null,
          render: function(data,type,full,meta) {
            return '<button type="button" class="btn btn-danger deleteDateAvailable" data-id="'+data.id+'"><i class="fa fa-trash"></i></button>';
          }
        },
    ],
});

getCalendarData();

function getCalendarData() {
  var coach_id = $('#current_coach_id').val();
  $.ajax({
     type: "GET",
     url: "/api/availabilities?coach_id="+coach_id,
     success: function(response)
     {
       response.data.forEach(function(event) {
           var $val = {
             id:event.id,
             date_debut:event.date_debut,
             date_fin:event.date_fin,
             time_start:event.start_time,
             time_end:event.end_time
           };
           console.log($val);
           all_availabity.push($val);
       });
     }
   });
}

//getEventData();

function getEventData() {
  var coach_id = $('#current_coach_id').val();
  $.ajax({
     type: "GET",
     url: "/api/availabilities?coach_id="+coach_id,
     success: function(response)
     {
       //console.log(response);
       response.data.forEach(function(event) {
           //console.log(event);
           var uUI = newUIRandom();
           var nameTitle = (event.course? event.course.discipline.name.trim()+" #"+event.id : "Cours "+uUI);
           var $val = {
             id:event.id,
             name:nameTitle,
             time_start:event.start_time,
             time_end:event.end_time,
             date:event.day,
             type: (event.id && event.id%2 == 0 ? 'holiday': 'event'),
             event_id:event.id
           };
           console.log($val);
           $("#calendar-coach").evoCalendar("addCalendarEvent", $val);
           events.push($val);
       });
     }
   });
}

showNewEvent();

function showNewEvent(){
  var newData = getCookie("newEvent");
  if (newData) {
    var newDataJson = JSON.parse(newData);
    new_events = newDataJson;
    $("#calendar-coach").evoCalendar("addCalendarEvent", newDataJson);
    console.log(newDataJson);
  }
}

function getFormattedDate(date) {
  console.log(date);
  const [day, month, year] = date.split('/');
  const dateObj = {month, day, year};

  var dateG = dateObj.month + '/' + dateObj.day + '/' + dateObj.year;
  console.log(new Date(dateG).toDateString());
  var finish = new Date(dateG).toDateString();
  return finish;
}


var active_events = [];
var disableTimeRanges = [];

var selectDate = today.getMonth() + 1 + "/"+today.getDate()+"/" + today.getFullYear();

console.log(selectDate);

var week_date = [];

var curAdd, curRmv;

function getRandom(a) {
    return Math.floor(Math.random() * a);
}

$('#calendar-coach').on('selectDate', function(event, newDate, oldDate) {
// code here...
//console.log(event);
//console.log(event.target.evoCalendar.$active);
var currentDate = event.target.evoCalendar.$current;
selectDate = event.target.evoCalendar.$active.event_date;
var activeEvent = event.target.evoCalendar.$active.events;
console.log(activeEvent);
activeEvent.forEach(function(event) {
    console.log(event);
    var $val = [""+event.time_start+'", "'+event.time_end+""];
    console.log($val);
    disableTimeRanges.push($val);
});

active_events.push(activeEvent);
//console.log(selectDate);
//console.log(currentDate.date);
(selectDate < currentDate.date ? $("#calendar-add-event").prop("disabled", true) : $("#calendar-add-event").prop("disabled", false));
});

$('#calendar-coach').on('selectEvent', function(event, activeEvent) {
  console.log(event);
  console.log("===================================");
  console.log("activeEvent");
  console.log(activeEvent);
  console.log("===================================");
  console.log(new_events);
  Swal.fire({
    title: 'Voulez-vous supprimer ce créneau ?',
    showCancelButton: true,
    confirmButtonText: `Supprimer`,
  }).then((result) => {
    if (result.isConfirmed) {
      new_events.splice(activeEvent.id, 1);
      $("#calendar-coach").evoCalendar("removeCalendarEvent", activeEvent.id);
      $.ajax({
         type: "DELETE",
         url: "/api/availabilities/"+activeEvent.id,
         success: function(response)
         {
           console.log("===================================");
           console.log("delete response");
           console.log(response);
         }
       });
      console.log(new_events);
      Swal.fire('Créneau supprimé avec succès', '', 'success')
    }
  });
  console.log("===================================");
  console.log(event.target.evoCalendar);
});

$("#calendar-add-event").click(function(a) {
  if (selectDate)
  $('#add-event-popup').modal('toggle');
  /*curAdd = getRandom(events.length);
  $("#calendar-coach").evoCalendar("addCalendarEvent", events[curAdd]);
  active_events.push(events[curAdd]);
  events.splice(curAdd, 1);
  if (0 === events.length) a.target.disabled = !0;
  if (active_events.length > 0) $("#calendar-add-event").prop("disabled", true);*/
});

$('#showAddEventPopup').click(function(a){
  var time_start = $('#time_start').val();
  var time_end =  $('#time_end').val();
  var discipline_id =  $('#discipline_id option:selected').val();
  var discipline_label = $("#discipline_id option:selected").attr("label");
  console.log(discipline_label);
  console.log(discipline_id);
  if (time_start > time_end) {
    alert("L'heure de début doit être inférieur à l'heure de fin");
    return false;
  }
  if (time_start && time_end) {
      var uUI = newUIRandom();
      console.log(uUI);
      var newEvent = {
         id: uUI,
         name: 'Cours '+discipline_label+ ' #'+parseInt(events.length),
         time_start: time_start,
         time_end: time_end,
         date: selectDate,
         type: (discipline_id && discipline_id%2 == 0 ? 'holiday': 'event')
    };
    console.log(newEvent);
    $("#calendar-coach").evoCalendar("addCalendarEvent", newEvent);
    $('#add-event-popup').modal('hide');
    new_events.push(newEvent);
  } else {
    alert("Heure début et Heure de fin obligatoire !");
  }
});

$("#removeBtn").click(function(a) {
    curRmv = getRandom(active_events.length);
    $("#demoEvoCalendar").evoCalendar("removeCalendarEvent", active_events[curRmv].id);
    events.push(active_events[curRmv]);
    active_events.splice(curRmv, 1);
    if (0 === active_events.length) a.target.disabled = !0;
    if (events.length > 0) $("#addBtn").prop("disabled", !1);
});

$("#CourseCalendarForm").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var form = $(this);
    var url = form.attr('action');
    $('#smartwizard').smartWizard("loader", "show");
    var json_str = JSON.stringify(array_availabity);
    setCookie("newEvent", json_str, 1);
    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
             console.log(data);
              $('#smartwizard').smartWizard("loader", "hide");
              // alert(data); // show response from the php script.
               $('#smartwizard').smartWizard("next");
               return true;
           }
  });
});

$("#CourseFinishForm").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var form = $(this);
    var url = form.attr('action');
    $('#smartwizard').smartWizard("loader", "show");
    $.ajax({
           type: "POST",
           url: url,
            data: form.serialize() + '&availability=' + JSON.stringify(array_availabity),
           success: function(data)
           {
             console.log(data);
             Swal.fire({
               icon: 'success',
               title: 'Succès!',
               text: 'Enregistrement effectué',
               showConfirmButton: false
             });
             array_availabity = [];
             setTimeout(function() {
               window.location.href = "/customer/coach/lessons"
             }, 500);
            //  eraseCookie("newEvent");
             $('#smartwizard').smartWizard("loader", "hide");
           },
           error: function(error){
               console.log(error);
               Swal.fire({
                   icon: 'warning',
                   title: 'Oops...',
                   text: 'Une erreur s\'est produite veuillez réessayer' ,
               });
               $('#smartwizard').smartWizard("loader", "hide");
           }
  });
});


function newUIRandom() {
   return Math.random().toString(36).substr(2, 9);
}

 $('.timePicker').timepicker({
    timeFormat: 'H:i'
 });

 function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
}

function eraseCookie(name) {
    document.cookie = name + '=; Max-Age=0'
}

$('#addNewAvailable').on('click', function(e){
  var datesRangeCourse = $('#datesRangeCourse').val();
  var allDay = $('#allDay').prop('checked');
  var start_time = $('#start_time').val();
  var end_time = $('#end_time').val();
  var startDate = getDateYMD(datesRangeCourse.split(' - ')[0]);
  var endDate = getDateYMD(datesRangeCourse.split(' - ')[1]);
  var uUI = newUIRandom();
  var startDatex = new Date(startDate);
  var endDatex = new Date(endDate);
  var difference = endDatex.getTime() - startDatex.getTime();
  var days = Math.ceil(difference / (1000 * 3600 * 24));
  ////////////////// COMPARE TWO DATE ////////////
  var responseAvailable = false;
  all_availabity.forEach(function(val) {
      console.log(val);
      if (dateCheck(val.startDate, val.endDate, val.time_start, val.time_end,startDate, start_time) || dateCheck(val.startDate, val.endDate, val.time_start, val.time_end,endDate, end_time)) {
       responseAvailable = true;
      }
  });
  // array_availabity.forEach(function(val) {
  //     console.log(val);
  //     if (dateCheck(val.startDate, val.endDate, val.time_start, val.time_end, startDate, start_time) || dateCheck(val.startDate, val.endDate, val.time_start, val.time_end, endDate, end_time)) {
  //      responseAvailable = true;
  //     }
  // });
  if (responseAvailable == true) {
    $('#message-error-calendar').text("Une autre indisponibilité empiète sur ce créneau");
    $('#message-error-content').fadeIn();
    setTimeout(function(){
      $("#message-error-content").fadeOut();
   },5000);
   return false;
  }
var availabityDate = {
   id:uUI,
   startDate: startDate,
   endDate: endDate,
   startTime: start_time,
   endTime: end_time,
   allDay: allDay,
   fullDate:datesRangeCourse,
   days:days
};
//console.log(availabityDate);
array_availabity.push(availabityDate);
tableAvailable.row.add({
  'date_debut': startDate +' - '+start_time,
  'date_fin': endDate +' - '+end_time,
  'day': days +' Jours',
  'id': uUI
}).draw();
});

  $(document).on("click", ".deleteDateAvailable", function(e) {
  var dateID = $(this).attr('data-id');
  Swal.fire({
    title: 'Voulez-vous supprimer ce créneau ?',
    showCancelButton: true,
    confirmButtonText: `Supprimer`,
  }).then((result) => {
    if (result.isConfirmed) {
      array_availabity.splice(dateID, 1);
      tableAvailable.row($(this).parents('tr')).remove().draw();
      $.ajax({
         type: "DELETE",
         url: "/api/availabilities/"+dateID,
         success: function(response)
         {
           console.log("===================================");
           console.log("delete response");
           console.log(response);
         }
       });
      console.log(new_events);
      Swal.fire('Créneau supprimé avec succès', '', 'success')
    }
  });
  console.log("===================================");
});

$('input[name="datesRangeCourse"]').daterangepicker();


function getDateYMD(strDate) {

  var date = new Date(strDate)
  yr = date.getFullYear(),
    month = date.getMonth()+ 1,
    day = date.getDate(),
    newDate = yr + '-' + month + '-' + day;
    return newDate;
}

function dateCheck(from,to, ftime, fend_time,check, cTime) {
  //first date to check fdate
  //left date to check ldate
  //first date to check cdate
  var fDate,lDate,csDate;
  var start_time = ftime.split(":")
  var end_time = fend_time.split(":")
  //TODO: get the start date with hours set
  fDate = new Date(Date.parse(from))
  fDate.setHours(start_time[0])
  fDate.setMinutes(start_time[1]);
  
  //TODO: get the end date with hours set
  lDate = new Date(Date.parse(to))
  lDate.setHours(end_time[0]);
  lDate.setMinutes(end_time[1]);

  
  //TODO: get the check start time with hours set
  var stime = cTime.split(":")
  csDate = new Date(Date.parse(check))
  csDate.setHours(stime[0])
  csDate.setMinutes(stime[1]);

  
  if(csDate > fDate && csDate <lDate) {
    return true;
  }
  return false;
}


});
