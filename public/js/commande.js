$(document).ready(function () {

    // *** FORM VALIDATION
    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;

    // All the infos about the form:
        // customer
        // services
        // delivery
    var $form_data = {};
    // var $form_data = {
    //     'customer': $customer,
    //     'services': $services,
    //     'delivery': $delivery
    // }

    // All the infos about the customer
    var $customer = {};
    // var $customer = {
    //     'name' : ,
    //     'last_name':,
    //     'email':,
    //     'phone_number':,
    //     'dial_code':
    //     'phone': dial_code + phone_number
    //     'account_type':
    //     'company_name':
    // }

    //** Account Tab Validation /

    $("#customer_tab_next_button").click(function () {
        // Validator
        var input_name = $("#input_name").val();
        var input_last_name = $("#input_last_name").val();
        var input_email = $("#input_email").val();
        var input_phone_number = $("#input_phone_number").val();
        var input_dial_code = $("#input_dial_code").val();
        var input_account_type = $("#input_account_type").val();
        var input_company_name = $("#input_company_name").val();

        function raiseError(input_id){
            if (input_id == '') {
                input_id.addClass('error');
                alert("error")
            }
            alert("not empty")
        }

        if (input_name == '' || input_last_name == '' || input_email == '' || input_dial_code == '' || input_phone_number == '') {
            alert("champs vides");

            raiseError(input_name);
            raiseError(input_last_name);
            raiseError(input_email);
            raiseError(input_phone_number);

            return false;
        } else {
            // var input_data = [];
            $customer = {
                'name': input_name,
                'last_name': input_last_name,
                'email': input_email,
                'phone_number': input_phone_number,
                'dial_code': input_dial_code,
                'phone': input_dial_code + input_phone_number,
                'account_type': input_account_type,
                'company_name': input_company_name,
            }

            // Show next tab
            showNextTab("#customer_tab_next_button");
            console.log($customer);
            $form_data['customer'] = $customer;
            console.log($form_data);

        }
    });
    //** Services Tab Validation /
    // Ajout de services

    // clothes selected in the service tab
    var $selected_services = [];
    // var selected_services_json = [{
    //     'type': 'repassage',
    //     'vetements': [{
    //         'slug': 'pantalon',
    //         'prix': 1500,
    //         'quantite': 2,
    //         'total': 3000
    //     }],
    //     'montant_total': 3000
    // }]

    var $current_service_slug = $("#select_service_slug").val();
    var $current_cloth_slug = $("#input_clothes_typehead").val();
    $current_clothe_price = "";
    // Total of all the clothes services
    $review_total_amount = 0;
    // $review_total_amount = parseInt($("#table_review_total_price"));
    // $review_total_amount_html = $("#table_review_total_price").html();
    $display_amount = $("#display_amount");


    // Rendering the selected_services as a disposable array in the htmls
    $services_table_review_body = $("#table_review_body");

    // Listen for changes in the service type slug
    $("#select_service_slug").on("change", function () {
        $current_service_slug = $(this).val();
        // alert($current_service_slug);
    });
    // Listen for changes in the vetement slug
    $("#input_clothes_typehead").on("change", function () {
        $current_cloth_slug = $(this).val();
    });

    // calculate price
    $("#input_clothes_quantity").on("change", function () {
        // $clothe_slug = $("#input_clothes_typehead").val();
        $clothe_slug = $current_cloth_slug;
        amount = 0;
        $quantity = $("#input_clothes_quantity").val();
        // $price_pattern =
        if ($clothe_slug != null) {
            $current_clothe_price = /\d{3,}/.exec($clothe_slug);
            $current_clothe_price = $current_clothe_price[0];
            amount = $current_clothe_price * $quantity;
            $display_amount.html(amount);
        }
    });
    // print current_clothe_price when typeahead is changed
    // $("#input_clothes_typehead").on("change", function () {
    //     alert("changed ! ")
    // });

    // **
    $("#add_clothers").click(function () {
        // Service
        // var service = $current_service_slug;
        var service = $("#select_service_slug").val();
        // Quantity
        var quantity = parseInt($("#input_clothes_quantity").val());
        // Slug
        var clothe_slug = $("#input_clothes_typehead").val();
        // Price
        // Amount
        var amount = parseInt($("#display_amount").html());
        // check if set
        if (service == null || quantity == null || clothe_slug == null || amount == 0) {
            alert("Champs vides !");
            // TODO add fields error validation
            return false;
        }

        // Checks for service duplicate
        // search for duplicate service slug
        function checkDuplicateServiceSlug(services) {
            for (var t = 0; t < services.length; t++){
                if (services[t]['type'] == service) {
                    return t;
                }
            }
            return -1;
        }
        // search for duplicate vetements
        function checkDuplicateVetementSlug(vetements, current_vetement) {
            for (var t = 0; t < vetements.length; t++) {
                if (vetements[t]['slug'] == current_vetement) {
                    return t;
                }
            }
            return -1;
        }
        // adds a new service with initial vetement
        function addNewServiceWithInitialVetement(){
            $selected_services.push({
                'type': service,
                'vetements': [{
                    'slug': clothe_slug,
                    'prix': $current_clothe_price,
                    'quantite': quantity,
                    'montant': $current_clothe_price * quantity
                }],
                'montant_total': amount
            });
        }
        // add a new vetement to a service
        function addNewVetementToService(service_index) {

            $selected_services[service_index]['vetements'].push({
                'slug': clothe_slug,
                'prix': $current_clothe_price,
                'quantite': quantity,
                'montant': $current_clothe_price * quantity
            });
            // update total_amount of service
            $selected_services[service_index]['montant_total'] += $current_clothe_price * quantity;
        }
        // increments vetement quantity and recalculate amount of specified service vetement
        function incrementQuantityAndRecalculateAmount(service_index, clothe_index) {
            // quantity += 1;
            // $display_amount.html(amount);
            $selected_services[service_index]['vetements'][clothe_index]['quantite'] += quantity;
            amount = $current_clothe_price * $selected_services[service_index]['vetements'][clothe_index]['quantite'];
            $selected_services[service_index]['vetements'][clothe_index]['montant'] = amount;
            $selected_services[service_index]['montant_total'] += amount;

        }
        // *** Deletion functions
        // TODO !!!
        function deleteVetement(service_index, clothe_index) {
            alert("delete " + clothe_index);
        }



        // If there is already a selected service entry
        if ($selected_services.length > 0) {

            // Check for duplicate service
            var duplicate_service_index = checkDuplicateServiceSlug($selected_services);
            // check if the new service slug doesnt already exists
            if (duplicate_service_index != -1) {
                // If duplicate service is found
                // Check for duplicate vetement
                if ($selected_services[duplicate_service_index]['vetements'].length > 0) {
                    var duplicate_clothe_index = checkDuplicateVetementSlug($selected_services[duplicate_service_index]['vetements'], clothe_slug);

                    // if duplicate vetement is found
                    if (duplicate_clothe_index != -1) {
                        // alert("duplicate at : " + duplicate_clothe_index);
                        incrementQuantityAndRecalculateAmount(duplicate_service_index, duplicate_clothe_index);
                    } else {
                        // else if duplicate is not found : add a new
                        addNewVetementToService(duplicate_service_index);
                    }
                } else {
                    // Else if there's no clothes at all
                    addNewVetementToService(duplicate_service_index);
                }
            } else {
                // If he doesnt find any duplicate
                addNewServiceWithInitialVetement();
            }
            // Display in the html
            // If the services has 0 record then add anew
        } else {
            addNewServiceWithInitialVetement();
        }
        // console.log($selected_services);


        // $services_table_review_body.html("<>")
        // For each selected service a table row like this
            // $tr_services (
            // <tr>
            //     <td rowspan="2">Repassage</td>

            //     <td>Veste</td>
            //     <td>qty</td>
            //     <td>prix</td>
            //     <td>amount</td>
            //     <td>action</td>
            // </tr> )
            // $tr_vetements (
            //     <tr>
            //         <td>Veste</td>
            //         <td>qty</td>
            //         <td>prix</td>
            //         <td>amount</td>
            //         <td>action</td>
            //     </tr> )
            //

        $tr_services = "";
        $tr_vetements = "";
        for (var s = 0; s < $selected_services.length; s++) {
            $tr_services += `<tr>
                <td rowspan="${$selected_services[s]['vetements'].length}">
                    ${$selected_services[s]['type']}
                </td>
                <td>
                    ${$selected_services[s]['vetements'][0]['slug']}
                </td>
                <td>
                    ${$selected_services[s]['vetements'][0]['prix']}
                </td>
                <td>
                    ${$selected_services[s]['vetements'][0]['quantite']}
                </td>
                <td>
                    ${$selected_services[s]['vetements'][0]['quantite'] * $selected_services[s]['vetements'][0]['prix']}
                </td>
                <td>
                    <i class="fa fa-trash text-danger" onClick="deleteVetement(${s},0);">

                    </i>
                </td>
            </tr>`;
            // console.log("service : " + $selected_services[s]['type'] + " nb v : " + $selected_services[s]['vetements'].length);
            // vetements list
            // console.log($selected_services[s]['vetements']);

            vetements = $selected_services[s]['vetements'];
            if (vetements.length > 1) {
                for (var v = 1; v < vetements.length; v++){
                    $tr_vetements = `
                        <tr>
                            <td>
                                ${vetements[v]['slug']}
                            </td>
                            <td>
                                ${vetements[v]['prix']}
                            </td>
                            <td>
                                ${vetements[v]['quantite']}
                            </td>
                            <td>
                                ${vetements[v]['quantite'] * vetements[v]['prix']}
                            </td>
                            <td>
                                <i class="fa fa-trash text-danger"></i>
                            </td>
                        </tr>`;
                        $tr_services += $tr_vetements;
                }
            }
            // Making a separation
            $tr_services += "<!-- Fin type de service -->";
            $review_total_amount = $selected_services[s]['montant_total'];
            $services_table_review_body.html($tr_services);
            // reset fields
                // Service
                $("#select_service_slug").val('');
                // Quantity
                $("#input_clothes_quantity").val('');
                // Slug
                $("#input_clothes_typehead").val('');
                // Price
                $("#display_amount").html(0);
        }
        // print selected_services
        console.log($selected_services);

        $form_data['services'] = $selected_services;
        console.log($form_data);

        // Display the total amount at the bottom of the review table
        $("#table_review_total_amount").html($review_total_amount);
        // console.log($selected_services)
        // $("#console").html($selected_services);

    });

    // Validation
    $("#services_tab_next_button").click(function () {
        // Validator
        //
        var v = $("#table_review_total_amount").text();

        // TODO uncomment !!!!!!!!
        // TODO uncomment !!!!!!!!
        // TODO uncomment !!!!!!!!
        // TODO uncomment !!!!!!!!
        if (v == '0') {
            alert("Aucun service sélectionné");
            return false;
        } else {
            // Show next tab
            showNextTab("#services_tab_next_button");
        }
    });






    //** Delivery Tab Validation /

    var $delivery_data = {};
    // var $delivery_data = {
    //     'formula': 'lite' || 'express',
    //     'agency': 'agency_name',
    //     'delivery_address': locat_pickup,
    //     'delivery_lat': lat_pickup,
    //     'delivery_lon': lon_pickup,
    //     'delivery_place': ,
    //     'delivery_town': ,
    //     'delivery_town': ,
    //     'delivery_date': ,
    //     'delivery_time': ,

    //     'pickup_lat': ,
    //     'pickup_lon': ,
    //     'pickup_place': ,
    //     'pickup_town': ,
    //     'pickup_date': ,
    //     'pickup_time': ,

    //     'price': ,
    // }


    //
    // var price = $("#input_price");
    $("#delivery_tab_next_button").click(function () {

        var formula = $("#select_formula").val();
        var agency_name = $("#select_business").val();
        //
        var pickup_address = $("#locat_pickup").val();
        var pickup_lat = $("#lat_pickup").val();
        var pickup_lon = $("#lon_pickup").val();
        var pickup_date = $("#pickup_date").val();
        var pickup_time = $("#pickup_time").val();
        //
        var delivery_address = $("#locat_delivery").val();
        var delivery_lat = $("#lat_delivery").val();
        var delivery_lon = $("#lon_delivery").val();
        var delivery_date = $("#delivery_date").val();
        var delivery_time = $("#delivery_time").val();
        //

        if (formula == '' || agency_name == '' || pickup_address == '' || pickup_date == '' || pickup_time == '' || delivery_address == '' || delivery_date == '' || delivery_time == '') {
            //
            alert("champs requis");
            return false;
        } else {
            // set up delivery data
            $delivery_data = {
                'formula': formula,
                'agency_name': agency_name,
                'delivery_address': delivery_address,
                'delivery_lat': delivery_lat,
                'delivery_lon': delivery_lon,
                // 'delivery_place': ,
                // 'delivery_town': ,
                'delivery_date': delivery_date,
                'delivery_time': delivery_time,
                //
                'pickup_address': pickup_address,
                'pickup_lat': pickup_lat,
                'pickup_lon': pickup_lon,
                // 'pickup_place': ,
                // 'pickup_town': ,
                'pickup_date': pickup_date,
                'pickup_time': pickup_time,
                // 'price': ,
            }
            console.log($delivery_data);

            $form_data['delivery'] = $delivery_data;
            console.log($form_data);

            // *** Populate the customer review section with info from the form_info
            $("#display_name").html($form_data['customer']['name']);
            $("#display_last_name").html($form_data['customer']['last_name']);
            $("#display_email").html($form_data['customer']['email']);
            $("#display_phone").html($form_data['customer']['dial_code'] + $form_data['customer']['phone']);
            $("#display_account_type").html($form_data['customer']['account_type']);
            $("#display_company_name").html($form_data['customer']['company_name']);

            // *** Populate the order review table with all the form_info
            // $("#order_review_table").
            populateOrderReviewTable();

            function populateOrderReviewTable() {
                $order_review_table_body = $("#order_review_body");

                $tr_services = "";
                $tr_vetements = "";
                $ss = $form_data['services'];
                for (var s = 0; s < $ss.length; s++) {
                    $tr_services += `<tr>
                    <td rowspan="${$ss[s]['vetements'].length}">
                        ${$ss[s]['type']}
                    </td>
                    <td>
                        ${$ss[s]['vetements'][0]['slug']}
                    </td>
                    <td>
                        ${$ss[s]['vetements'][0]['prix']}
                    </td>
                    <td>
                        ${$ss[s]['vetements'][0]['quantite']}
                    </td>
                    <td>
                        ${$ss[s]['vetements'][0]['quantite'] * $ss[s]['vetements'][0]['prix']}
                    </td>
                </tr>`;
                    // console.log("service : " + $selected_services[s]['type'] + " nb v : " + $selected_services[s]['vetements'].length);
                    // vetements list
                    // console.log($selected_services[s]['vetements']);

                    sv = $ss[s]['vetements'];
                    if (sv.length > 1) {
                        for (var v = 1; v < sv.length; v++) {
                            $tr_vetements = `
                            <tr>
                                <td>
                                    ${sv[v]['slug']}
                                </td>
                                <td>
                                    ${sv[v]['prix']}
                                </td>
                                <td>
                                    ${sv[v]['quantite']}
                                </td>
                                <td>
                                    ${sv[v]['quantite'] * sv[v]['prix']}
                                </td>
                            </tr>`;
                            $tr_services += $tr_vetements;
                        }
                    }
                    // Making a separation
                    $tr_services += "<!-- Fin type de service -->";
                    $review_total_amount = $ss[s]['montant_total'];
                    $order_review_table_body.html($tr_services);
                }


                // Display the total amount at the bottom of the review table
                $("#order_review_total_amount").html($review_total_amount);
            }

            // *** Populate the delivery review section

            $("#display_formula").html($form_data['delivery']['formula']);
            $("#display_agency").html($form_data['delivery']['agency_name']);

            $("#display_pickup_adress").html($form_data['delivery']['pickup_address']);
            $("#display_pickup_date").html($form_data['delivery']['pickup_date']);
            $("#display_pickup_time").html($form_data['delivery']['pickup_time']);

            $("#display_delivery_adress").html($form_data['delivery']['delivery_address']);
            $("#display_delivery_date").html($form_data['delivery']['delivery_date']);
            $("#display_delivery_time").html($form_data['delivery']['delivery_time']);


        // console.log($selected_services)
            //
            showNextTab("#delivery_tab_next_button");
        }
    });

    // *** End of Validation
    // Sending the $form_data to the server
    $("#order_tab_next_button").click(function () {

        sendData($form_data);

        function sendData(data) {
            var _token = $('#csrf').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': _token
                }
            });

            $.ajax({
                url: "/orders/storeAjax",
                method: "POST",
                data: data
            }).done((response, textStatus, jqXHR) => {
                console.log("success");
                console.log(response);
                // to next page
                showNextTab("#order_tab_next_button");

            }).fail((response) => {
                alert("Erreur, veuillez reprendre");
                console.log("failed, here's why :")
                console.log(response);
            });
        }
        //
    });


        //
        // *** Tab navigation functions
        // Shows next tab
    function showNextTab(currentButton) {
            // current_fs = $(this).parent();
            // next_fs = $(this).parent().next();
            current_fs = $(currentButton).parent();
            next_fs = $(currentButton).parent().next();

            //Add Class Active
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({
            opacity: 0
        }, {
            step: function (now) {
                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                next_fs.css({
                    'opacity': opacity
                });
            },
            duration: 600
        });
    }
    $(".next").click(function () {
        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //Add Class Active
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({
            opacity: 0
        }, {
            step: function (now) {
                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                next_fs.css({
                    'opacity': opacity
                });
            },
            duration: 600
        });
    });

    $(".previous").click(function () {

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();

        //hide the current fieldset with style
        current_fs.animate({
            opacity: 0
        }, {
            step: function (now) {
                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                previous_fs.css({
                    'opacity': opacity
                });
            },
            duration: 600
        });
    });

    $('.radio-group .radio').click(function () {
        $(this).parent().find('.radio').removeClass('selected');
        $(this).addClass('selected');
    });

    $(".submit").click(function () {
        return false;
    })

});
