$(function(){
    /*
  * Block / Unblock UI
  */
  function blockUI() {
      $.blockUI({
          message: "<img src='../../assets/img/loading.gif' alt='Loading ...' />",
          overlayCSS: {
              backgroundColor: '#1B2024',
              opacity: 0.55,
              cursor: 'wait'
          },
          css: {
              border: '4px #999 solid',
              padding: 15,
              backgroundColor: '#fff',
              color: 'inherit'
          }
      });
  }

  function unblockUI() {
      $.unblockUI();
  }


  $("#paymentfrm").submit(function(e) {
      e.preventDefault();
      blockUI();
      var oData = new FormData();
      oData.append("_token", $('input[name=_token]').val());
      oData.append("booking_id", $("#booking_id").val());
      $.ajax({
         url:"/customer/save/booking",
         method:"POST",
         data:oData,
         contentType:false,
         cache:false,
         processData:false,
         success:function(response){
           console.log(response);
            unblockUI();
            payWithAdjeminPay({
                amount: response.data.amount,
                transactionId: response.data.transaction_id,
                currency:"CFA",
                designation: response.data.transaction_designation,
                customField: response.data.status
            });
         },
         error: function(error){
             unblockUI();
             console.log(error);
             Swal.fire({
                 icon: 'warning',
                 title: 'Oops...',
                 text: 'Une erreur s\'est lors de l\'enregistrement veuillez réessayer' ,
             });
         }
      });
  });


  function payWithAdjeminPay(transactionData){
      if(!transactionData){
          alert(">>>> erreur paiement, veuillez réessayer");
          console.log("<< clicked");
      }
      console.log(">>> clicked");

      // Ecoute le feedback sur les erreurs
      AdjeminPay.on('error', function (e) {
          // la fonction que vous définirez ici sera exécutée en cas d'erreur
          console.log(e);
          console.log(">>> clicked");

      });

      // Lancer la procédure de paiement au click
      AdjeminPay.preparePayment({
          amount: transactionData.amount,
          transaction_id: transactionData.transactionId,
          currency: transactionData.currency,
          designation: transactionData.designation,
          custom: transactionData.customField
      });

      // Si l'étape précédante n'a pas d'erreur,
      // cette ligne génère et affiche l'interface de paiement AdjeminPay
      AdjeminPay.renderPaymentView();


      // Payment terminé
      AdjeminPay.on('paymentTerminated', function (e) {
          console.log('<<<<<<< Terminated !');
          console.log(e);

      });
      // Payment réussi
      AdjeminPay.on('paymentSuccessful', function (e) {
          console.log('<<<<<<< Successful !');
          console.log(e);
          setTimeout(()=> {
              window.location.reload();
              location.assign("/customer/dashboard");
              //
          }, 2000);
      });
      // Payment échoué
      AdjeminPay.on('paymentFailed', function (e) {
          console.log('<<<<<<< Failed !');
          console.log(e);
      });
      // Payment annulé
      AdjeminPay.on('paymentCancelled', function (e) {
          console.log('<<<<<<< Cancelled !');
          console.log(e);
      });
  }

});
