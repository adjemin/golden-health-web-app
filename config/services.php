<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect' => env('FACEBOOK_REDIRECT_URI')
    ],
    // 'facebook' => [
    //     'client_id' =>367990845131064,
    //     'client_secret' => 'df8a678700e486eb30f6d05e62ae8217',
    //     'redirect' => 'https://goldenhealth.ci/auth/facebook/callback'
    // ],

    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => env('GOOGLE_REDIRECT_URI')
    ],

    'adjeminpay' => [
        'app_id' => env('ADJEMINPAY_APP_ID'),
        'api_key' => env('ADJEMINPAY_API_KEY')
    ],

    'adjeminpayv2' => [
        'app_id' => env('APP_ADJEMINPAY_APP_ID'),
        'api_key' => env('APP_ADJEMINPAY_API_KEY')
    ],

    /* 'imgur' => [
        'client_id' => env('IMGUR_CLIENT_ID'),
        'client_secret' => env('IMGUR_CLIENT_SECRET'),
    ], */
];
