<?php

namespace App;

use App\ShoppingCart;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    //
    protected $guarded = [];

    public function shoppingCart()
    {
        return $this->belongsTo(ShoppingCart::class);
    }
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getColor(){
        if($this->color != null){

            return Color::where('name', $this->color)->first();
        }
        return null;

    }
}
