<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Customer;
use App\Models\Seance;
use App\Models\Availability;


class CustomerAvailability extends Notification
{
    use Queueable;

    protected $customer;
    protected $seance;
    protected $pack;
    protected $availability;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Customer $customer, int $seance, Availability $availability, int $pack)
    {
        $this->customer = $customer;
        $this->seance = $seance;
        $this->availability = $availability;
        $this->pack = $pack;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'customer_name' => $this->customer->name,
            'seance_id' => $this->seance,
            'availability' => $this->availability->id,
            'status'    => $this->availabitlity->status,
            'pack_id'   => $this->pack
        ];
    }
}
