<?php

namespace App;

use App\Models\Event;
use Illuminate\Database\Eloquent\Model;

class ParticipatesInEvent extends Model
{
    //

    protected $guarded = [];


    public function event()
    {
        return $this->belongsTo(Event::class);
    }
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}