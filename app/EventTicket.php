<?php

namespace App;

use App\Models\Event;
use Illuminate\Database\Eloquent\Model;

class EventTicket extends Model
{
    //

    protected $guarded = [];
    protected $appends = ['event'];


    public function isValid()
    {
        $r = true;
        if($this->is_used){
            $r = false;
        }

        if($this->is_cancelled){
            $r = false;
        }

        if($this->is_blocked){
            $r = false;
        }
        // if($this->event->date_event > today()){
        //     $r = false;
        // }
        return $r;
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    //
    public function amountText(){
        $c = '';
        $m = 'N/A';

        $m = 'Pas encore utilisé';

        if($this->is_used){
            $c = 'dark';
            $m = 'Utilisé';
        }

        if($this->event->date_event < today()){
            $c = 'warning';
            $m = 'Expiré';
        }

        return $this->event->isFree() ? "Gratuit" : "$this->amount CFA";
    }
    //
    public function statusText(){
        $c = '';
        $m = 'N/A';

        $m = 'Pas encore utilisé';

        if($this->is_used){
            $c = 'dark';
            $m = 'Utilisé';
        }

        if(!$this->is_paid){
            $c = '';
            $m = 'Non valide';
        }

        if($this->event->date_event < today()){
            $c = 'warning';
            $m = 'Expiré';
        }

        return "<span class='badge badge-$c'>$m</span>";
    }
    //
    public function codeText()
    {
        $c = 'dark';
        $m = $this->code;

        if($this->is_paid){
            $c = 'success';
        }

        return "<span class='text-$c font-weight-bold'>$m</span>";
    }
    public function paymentStatusText()
    {
        $c = 'dark';
        $m = 'Non payé';

        if($this->is_paid){
            $c = 'success';
            $m = 'Payé';
        }

        return "<span class='text-$c font-weight-bold'>$m</span>";
    }


    public function getEventAttribute(){
        return Event::where('id', $this->event_id)->first();
    }
}