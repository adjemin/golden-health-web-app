<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Customer;
use DB;
use App\Mail\BirthdayNotifyChallenger;

use App\Mail\BirthdayNotifyCoach;
use Illuminate\Support\Facades\Mail;
use App\Models\CustomerNotifications;
use App\Utils\CustomerNotificationUtils;

class NotifyBirthday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:every_birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command will notify the self-challenger or coachs every year';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $challengers = $this->getUsers('challenger');
        $coachs = $this->getUsers('coachs');
        if($challengers->count() > 0 || $coachs->count() > 0 ){
            echo "🎉";
        }
        $this->mailSender($challengers, "BirthdayNotifyChallenger");
        $this->mailSender($coachs, "BirthdayNotifyCoach");
        echo 'success \n';
    }

    public static function exec(){
        $this->handle();
    }

    private function getUsers($type){
        return Customer::where([
            'type_account' => $type,
            'is_active' =>  1
        ])->where(DB::raw('MONTH(created_at)'),'=', DB::raw('MONTH(NOW())'))
        ->where(DB::raw('DAY(created_at)'),'=', DB::raw('DAY(NOW())'))
        ->get();
    }

    private function mailSender($arr, $mail){
        $className = "App\Mail\\$mail";
        if(!is_null($arr)){
            foreach($arr as $item){
                Mail::to($item->email)->send(new $className());
                
                $customerNotification = new CustomerNotifications();
                
                $customerNotification->title  = "🎉🎉🎉🎉🎉🎉 Date d'anniversaire d'inscription 🎊🎊🎊🎊🎊";
                $customerNotification->title_en  = "🎉🎉🎉🎉🎉🎉 register birthday 🎊🎊🎊🎊🎊";
                $customerNotification->subtitle  = "Un an de plus passé avec nous c'est extraordinaire";
                $customerNotification->subtitle_en  = "One year again, since the last birthday's register";
                $customerNotification->action  = "";
                $customerNotification->action_by  = "";
                $customerNotification->type_notification  = "Birthday";
                $customerNotification->is_read  = false;
                $customerNotification->is_received  = false;
                $customerNotification->user_id  = $item->id;
                $customerNotification->save();

                CustomerNotificationUtils::notify($customerNotification);
            }
        }
    }
}
