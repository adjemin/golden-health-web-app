<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\BookingCoach;
use Carbon\Carbon;
use DB;
use Mail;
use App\Mail\RemindCourseNotifyCoach;
use App\Mail\RemindCourseNotifyChallenger;
use App\Models\CustomerNotifications;
use App\Utils\CustomerNotificationUtils;


class NotifyEveryThreeHour extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:every_three_hour';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command will notify the self-challenger or coachs 3 hour before the beginnig of a course';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $bookings = $this->getAvailabilities();
        
        if($bookings->count() > 0){
            echo "🎉";
        }

        foreach($bookings as $booking){
            $coach = $booking->course->coach ?? null; 
            $this->mailSender($coach, 'RemindCourseNotifyCoach');

            $customer = $booking->customer ?? null;
            $this->mailSender($customer, 'RemindCourseNotifyChallenger');
        }
        echo 'test succes  \n';
    }

    public static function exec(){
        $this->handle();
    }


    private function getAvailabilities(){
        $day_availabilities = BookingCoach::where('coach_confirm', 1)
        ->where('status_name', 'paid')
        ->where(DB::raw('STR_TO_DATE(start_date, "%d/%m/%Y")'),'=', DB::raw('DATE_FORMAT(NOW(), "%d/%m/%Y")'))
        ->get();

        return collect($day_availabilities)->filter(function($day_availability){
            $check = Carbon::parse($day_availabilities->start_date)->format('H');
            $now = Carbon::now()->format('H');
            return $check->diffInHours($now) == 3;
        })->reject(function($day_availability){
            return is_null($day_availability) || empty($day_availability);
        })->unique();
    }

    private function mailSender($item, $mail){
        $className = "App\Mail\\$mail";
        if(!is_null($item)){
            Mail::to($item->email)->send(new $className());

            $customerNotification = new CustomerNotifications();
                
            $customerNotification->title  = "Rappel de cours";
            $customerNotification->title_en  = "Remind course";
            $customerNotification->subtitle  = "Vous avez un cours dans 3 heures";
            $customerNotification->subtitle_en  = "You'll have a course in 3 hours";
            $customerNotification->action  = "";
            $customerNotification->action_by  = "";
            $customerNotification->type_notification  = "Remind";
            $customerNotification->is_read  = false;
            $customerNotification->is_received  = false;
            $customerNotification->user_id  = $item->id;
            $customerNotification->save();

            CustomerNotificationUtils::notify($customerNotification);
        }
    }
}
