<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCourseObjectiveRequest;
use App\Http\Requests\UpdateCourseObjectiveRequest;
use App\Repositories\CourseObjectiveRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CourseObjectiveController extends AppBaseController
{
    /** @var  CourseObjectiveRepository */
    private $courseObjectiveRepository;

    public function __construct(CourseObjectiveRepository $courseObjectiveRepo)
    {
        $this->courseObjectiveRepository = $courseObjectiveRepo;
    }

    /**
     * Display a listing of the CourseObjective.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $courseObjectives = $this->courseObjectiveRepository->all();

        return view('course_objectives.index')
            ->with('courseObjectives', $courseObjectives);
    }

    /**
     * Show the form for creating a new CourseObjective.
     *
     * @return Response
     */
    public function create()
    {
        return view('course_objectives.create');
    }

    /**
     * Store a newly created CourseObjective in storage.
     *
     * @param CreateCourseObjectiveRequest $request
     *
     * @return Response
     */
    public function store(CreateCourseObjectiveRequest $request)
    {
        $input = $request->all();

        $courseObjective = $this->courseObjectiveRepository->create($input);

        Flash::success('Course Objective saved successfully.');

        return redirect(route('courseObjectives.index'));
    }

    /**
     * Display the specified CourseObjective.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $courseObjective = $this->courseObjectiveRepository->find($id);

        if (empty($courseObjective)) {
            Flash::error('Course Objective not found');

            return redirect(route('courseObjectives.index'));
        }

        return view('course_objectives.show')->with('courseObjective', $courseObjective);
    }

    /**
     * Show the form for editing the specified CourseObjective.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $courseObjective = $this->courseObjectiveRepository->find($id);

        if (empty($courseObjective)) {
            Flash::error('Course Objective not found');

            return redirect(route('courseObjectives.index'));
        }

        return view('course_objectives.edit')->with('courseObjective', $courseObjective);
    }

    /**
     * Update the specified CourseObjective in storage.
     *
     * @param int $id
     * @param UpdateCourseObjectiveRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCourseObjectiveRequest $request)
    {
        $courseObjective = $this->courseObjectiveRepository->find($id);

        if (empty($courseObjective)) {
            Flash::error('Course Objective not found');

            return redirect(route('courseObjectives.index'));
        }

        $courseObjective = $this->courseObjectiveRepository->update($request->all(), $id);

        Flash::success('Course Objective updated successfully.');

        return redirect(route('courseObjectives.index'));
    }

    /**
     * Remove the specified CourseObjective from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $courseObjective = $this->courseObjectiveRepository->find($id);

        if (empty($courseObjective)) {
            Flash::error('Course Objective not found');

            return redirect(route('courseObjectives.index'));
        }

        $this->courseObjectiveRepository->delete($id);

        Flash::success('Course Objective deleted successfully.');

        return redirect(route('courseObjectives.index'));
    }
}
