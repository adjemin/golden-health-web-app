<?php

namespace App\Http\Controllers;

use App\Models\CouponCustomer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CouponCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CouponCustomer  $couponCustomer
     * @return \Illuminate\Http\Response
     */
    public function show(CouponCustomer $couponCustomer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CouponCustomer  $couponCustomer
     * @return \Illuminate\Http\Response
     */
    public function edit(CouponCustomer $couponCustomer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CouponCustomer  $couponCustomer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CouponCustomer $couponCustomer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CouponCustomer  $couponCustomer
     * @return \Illuminate\Http\Response
     */
    public function destroy(CouponCustomer $couponCustomer)
    {
        //
    }
}
