<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDisciplineRequest;
use App\Http\Requests\UpdateDisciplineRequest;
use App\Repositories\DisciplineRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Discipline;
use Illuminate\Http\Request;
use Flash;
use Response;
use Imgur;

class DisciplineController extends AppBaseController
{
    /** @var  DisciplineRepository */
    private $disciplineRepository;

    public function __construct(DisciplineRepository $disciplineRepo)
    {
        $this->disciplineRepository = $disciplineRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Discipline.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $disciplines = Discipline::orderBy('id', 'desc')->paginate(5);

        return view('disciplines.index')
            ->with('disciplines', $disciplines);
    }

    /**
     * Show the form for creating a new Discipline.
     *
     * @return Response
     */
    public function create()
    {
        return view('disciplines.create');
    }

    /**
     * Store a newly created Discipline in storage.
     *
     * @param CreateDisciplineRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        if ($request->file('image')) {

            $image = $request->file('image');
            $pictures = [];
            if ($image != null) {
                // dd($image);
                $productImage = Imgur::upload($image);
                $productImageLink = $productImage->link();
            }
        } else {
            $productImageLink = '';
        }

        $input["image"] = $productImageLink;
        $discipline = $this->disciplineRepository->create($input);

        Flash::success('Discipline saved successfully.');

        return redirect(route('disciplines.index'));
    }

    /**
     * Display the specified Discipline.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $discipline = $this->disciplineRepository->find($id);

        if (empty($discipline)) {
            Flash::error('Discipline not found');

            return redirect(route('disciplines.index'));
        }

        return view('disciplines.show')->with('discipline', $discipline);
    }

    /**
     * Show the form for editing the specified Discipline.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $discipline = $this->disciplineRepository->find($id);

        if (empty($discipline)) {
            Flash::error('Discipline not found');

            return redirect(route('disciplines.index'));
        }

        return view('disciplines.edit')->with('discipline', $discipline);
    }

    /**
     * Update the specified Discipline in storage.
     *
     * @param int $id
     * @param UpdateDisciplineRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $discipline = $this->disciplineRepository->find($id);

        if (empty($discipline)) {
            Flash::error('Discipline not found');

            return redirect(route('disciplines.index'));
        }

        $input = $request->all();
        // dd($request->file('image'));
        if ($request->file('image')) {

            $image = $request->file('image');
            $pictures = [];
            if ($image != null) {
                $productImage = Imgur::upload($image);
                $productImageLink = $productImage->link();
                // dd($productImageLink);
            }
        } else {
            $productImageLink = $discipline->image;
        }

        $input["image"] = $productImageLink;
        // dd($input);
        $discipline = $this->disciplineRepository->update($input, $id);

        Flash::success('Discipline updated successfully.');

        return redirect(route('disciplines.index'));
    }

    /**
     * Remove the specified Discipline from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $discipline = $this->disciplineRepository->find($id);

        if (empty($discipline)) {
            Flash::error('Discipline not found');

            return redirect(route('disciplines.index'));
        }

        $this->disciplineRepository->delete($id);

        Flash::success('Discipline deleted successfully.');

        return redirect(route('disciplines.index'));
    }
}
