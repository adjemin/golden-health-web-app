<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Repositories\CustomerRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Customer;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Hash;
use Laracasts\Flash\Flash as FlashFlash;
use Response;
use Imgur;
use PDF;

class CustomerController extends AppBaseController
{
    /** @var  CustomerRepository */
    private $customerRepository;

    public function __construct(CustomerRepository $customerRepo)
    {
        $this->middleware('auth');
        $this->customerRepository = $customerRepo;
    }

    /**
     * Display a listing of the Customer.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $customers = Customer::where('type_account', 'challenger')->orderBy('id', 'desc')->paginate(5);

        return view('customers.index')
            ->with('customers', $customers);
    }

    /**
     * Show the form for creating a new Customer.
     *
     * @return Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created Customer in storage.
     *
     * @param CreateCustomerRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $input = $request->all();

        $productImageLink = null;

        if ($request->file('medias')) {

            $image = $request->file('medias');
            $pictures = [];
            if ($image != null) {    
                $productImage = Imgur::upload($image);
                $productImageLink = $productImage->link();
            }
        } else {
            $productImageLink = "https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png";
        }

        if(isset($input["medias"])){
            unset($input["medias"]);
        }
        $input["photo_url"] = $productImageLink;

        if ( is_null($request->dial_code) || empty($request->dial_code)) {
            $input["dial_code"] = 225;
        }

        $input["phone_number"] = $input["dial_code"]."".$input["phone"];

        $input["last_name"] = ucfirst(strtolower($request->last_name));
        $input["first_name"] = ucfirst(strtolower($request->first_name));
        $input["name"] = $input["last_name"]." ".$input["first_name"];
        $input["password"] = Hash::make(trim($request->password));

        $customer = $this->customerRepository->create($input);

        Flash::success('Customer saved successfully.');

        return redirect(route('customers.index'));
    }

    /**
     * Display the specified Customer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        return view('customers.show')->with('customer', $customer);
    }

    /**
     * Show the form for editing the specified Customer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        return view('customers.edit')->with('customer', $customer);
    }

    /**
     * Update the specified Customer in storage.
     *
     * @param int $id
     * @param UpdateCustomerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerRequest $request)
    {
        $customer = $this->customerRepository->find($id);

        $productImageLink = null;

        $input = $request->all();
        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        if ($request->file('medias')) {

            $image = $request->file('medias');
            $pictures = [];
            if ($image != null) {    
                $productImage = Imgur::upload($image);
                $productImageLink = $productImage->link();
            }
        } else {
            $productImageLink = $customer->photo_url;
        }

        if(isset($input["medias"])){
            unset($input["medias"]);
        }
        $input["photo_url"] = $productImageLink;

        if ( is_null($request->dial_code) || empty($request->dial_code)) {
            $input["dial_code"] = 225;
        }

        $input["phone_number"] = $input["dial_code"]."".$input["phone"];

        $input["last_name"] = ucfirst(strtolower($request->last_name));
        $input["first_name"] = ucfirst(strtolower($request->first_name));
        $input["name"] = $input["last_name"]." ".$input["first_name"];
        $input["password"] = Hash::make(trim($request->password));

        $customer = $this->customerRepository->update($input, $id);

        Flash::success('Customer updated successfully.');

        return redirect(route('customers.index'));
    }

    /**
     * Remove the specified Customer from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        $this->customerRepository->delete($id);

        Flash::success('Customer deleted successfully.');

        return redirect(route('customers.index'));
    }

    public function searchCustomer(Request $request)
    {
        $customersFound = Customer::orWhere('name', 'like', '%' . $request->searchCustomer . '%')
            ->orWhere('email', 'like', '%' . $request->searchCustomer . '%')
            ->orWhere('phone', 'like', '%' . $request->searchCustomer . '%')
            ->orWhere('location_address', 'like', '%' . $request->searchCustomer . '%')
            ->where('type_account', 'challenger')
            ->get();
        return json_encode(view('customers.table', ['customers' => $customersFound])->render());
    }

    public function changeStatus($id, Request $request){
        $customer = $this->customerRepository->find($id);

        $input = $request->all();
        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        $customer->is_active = $request->is_active;
        $customer->activation_date =  is_null($customer->activation_date) ? now() : $customer->activation_date;
        $customer->email_verifed_at =  is_null($customer->email_verifed_at) ? now() : $customer->email_verifed_at;
        $customer->save();

        Flash::success('Customer updated successfully.');

        return redirect(route('customers.index'));
    }

    public function printAll(){
        $customers = Customer::all()->where('type_account', 'challenger')->sortByDesc('id');
        return view('customers.table_pdf', compact('customers'));
        // $pdf = PDF::loadView('customers.table_pdf', compact('customers'));
        // return $pdf->stream();
    }
}
