<?php

namespace App\Http\Controllers;

use App\Models\ProductReservation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Flash;
use App\Models\Product;

class ProductReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations = ProductReservation::orderBy('id', 'desc')->paginate(5);

        return view('product_reservation.index', compact('reservations'));
    }

    public function create()
    {
        $products = Product::all();
        return view('product_reservation.create', compact( 'products'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $input = $request->all();
        $reservation = ProductReservation::create($input);

        Flash::success('Votre réservation a été enregistrée avec succès.');
        return redirect()->back()->with('reservation', $reservation);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductReservation  $productReservation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reservation = ProductReservation::find($id);
        $products = Product::all();
        return view('product_reservation.edit', compact('reservation', 'products'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductReservation  $productReservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reservation = ProductReservation::find($id)->update($request->all());
        Flash::success('Réservation mise à jour avec succès.');

        return redirect()->route('product-reservation.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductReservation  $productReservation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reservation = ProductReservation::find($id)->delete();
        Flash::success('Réservation supprimée avec succès.');
        return redirect()->route('product-reservation.index');
    }
}
