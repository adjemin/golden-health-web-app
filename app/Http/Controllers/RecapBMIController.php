<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRecapBMIRequest;
use App\Http\Requests\UpdateRecapBMIRequest;
use App\Repositories\RecapBMIRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class RecapBMIController extends AppBaseController
{
    /** @var  RecapBMIRepository */
    private $recapBMIRepository;

    public function __construct(RecapBMIRepository $recapBMIRepo)
    {
        $this->recapBMIRepository = $recapBMIRepo;
    }

    /**
     * Display a listing of the RecapBMI.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $recapBMIs = $this->recapBMIRepository->all();

        return view('recap_b_m_is.index')
            ->with('recapBMIs', $recapBMIs);
    }

    /**
     * Show the form for creating a new RecapBMI.
     *
     * @return Response
     */
    public function create()
    {
        return view('recap_b_m_is.create');
    }

    /**
     * Store a newly created RecapBMI in storage.
     *
     * @param CreateRecapBMIRequest $request
     *
     * @return Response
     */
    public function store(CreateRecapBMIRequest $request)
    {
        $input = $request->all();

        $recapBMI = $this->recapBMIRepository->create($input);

        Flash::success('Recap B M I saved successfully.');

        return redirect(route('recapBMIs.index'));
    }

    /**
     * Display the specified RecapBMI.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $recapBMI = $this->recapBMIRepository->find($id);

        if (empty($recapBMI)) {
            Flash::error('Recap B M I not found');

            return redirect(route('recapBMIs.index'));
        }

        return view('recap_b_m_is.show')->with('recapBMI', $recapBMI);
    }

    /**
     * Show the form for editing the specified RecapBMI.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $recapBMI = $this->recapBMIRepository->find($id);

        if (empty($recapBMI)) {
            Flash::error('Recap B M I not found');

            return redirect(route('recapBMIs.index'));
        }

        return view('recap_b_m_is.edit')->with('recapBMI', $recapBMI);
    }

    /**
     * Update the specified RecapBMI in storage.
     *
     * @param int $id
     * @param UpdateRecapBMIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRecapBMIRequest $request)
    {
        $recapBMI = $this->recapBMIRepository->find($id);

        if (empty($recapBMI)) {
            Flash::error('Recap B M I not found');

            return redirect(route('recapBMIs.index'));
        }

        $recapBMI = $this->recapBMIRepository->update($request->all(), $id);

        Flash::success('Recap B M I updated successfully.');

        return redirect(route('recapBMIs.index'));
    }

    /**
     * Remove the specified RecapBMI from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $recapBMI = $this->recapBMIRepository->find($id);

        if (empty($recapBMI)) {
            Flash::error('Recap B M I not found');

            return redirect(route('recapBMIs.index'));
        }

        $this->recapBMIRepository->delete($id);

        Flash::success('Recap B M I deleted successfully.');

        return redirect(route('recapBMIs.index'));
    }
}
