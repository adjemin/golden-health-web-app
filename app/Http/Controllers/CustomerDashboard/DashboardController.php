<?php

namespace App\Http\Controllers\CustomerDashboard;

use Flash;
use QrCode;
use Response;
use SnappyPDF;
use Validator;
use App\Wishlist;
use Carbon\Carbon;
use App\EventTicket;
use App\Models\Order;
use App\Models\Review;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\OrderItem;
use App\Mail\AdminNewCourse;
use App\User;
use Illuminate\Http\Request;
use App\Models\BodyMassIndex;
use App\Models\InvoicePayment;
use App\Models\Enums\OrderType;
use App\Rules\MatchOldPassword;
use Illuminate\Validation\Rule;
use App\Models\Enums\PaymentMethod;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Services\Payments\StripeInitiator;
use App\Http\Controllers\AppBaseController;
use App\Repositories\BodyMassIndexRepository;
use App\Services\Payments\AdjeminPayInitiator;
use App\Services\Payments\PaymentGatewayInitiator;
use App\Mail\BookingPayNotifyMail;

class DashboardController extends AppBaseController
{

    const PAYMENT_GATEWAY_HANDLERS = [
        PaymentMethod::ADJEMINPAY => AdjeminPayInitiator::class,
        PaymentMethod::CREDIT_CARD => StripeInitiator::class,
    ];

    /** @var  CoachRepository */
    private $BodyMassIndexRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BodyMassIndexRepository $bmiRepo)
    {
        $this->middleware('customer');
        // $this->middleware();
        $this->BodyMassIndexRepository = $bmiRepo;
    }

    public function showDashboard()
    {

        $customer = Auth::guard('customer')->user();
        $coach = \App\Models\Coach::where('customer_id',$customer->id)->first();
        $coach_booking_wait = null;
        $coach_booking_conf = null;
        if ($customer->is_coach) {

            $courses = \App\Models\Course::where('coach_id', $customer->is_coach->id)->get();
            $ids_courses = collect($courses)->map(function ($value) {
                return $value->id;
            });

            $bookings = \App\Models\BookingCoach::whereIn('course_id', $ids_courses)->get();
            $coach_booking_conf = \App\Models\BookingCoach::whereIn('course_id', $ids_courses)->whereIn('status_name', ['paid', 'completed', 'confirmed', 'created'])->where('coach_confirm', 1)->orderBy('created_at', 'desc')->get();
            $coach_booking_wait = \App\Models\BookingCoach::whereIn('course_id', $ids_courses)->whereIn('status_name', ['paid', 'completed', 'confirmed', 'created'])->where('coach_confirm', 0)->orderBy('created_at', 'desc')->get();
            $availabilities = \App\Models\Availability::where([
                'coach_id' => $customer->is_coach->id,
            ])->orderBy('date_debut', 'asc')->get() ?? null;
        } else {
            $bookings = \App\Models\BookingCoach::where('customer_id', $customer->id)->orderBy('created_at', 'desc')->get();
            $availabilities = null;
            $coach_booking_conf = \App\Models\BookingCoach::where('customer_id', $customer->id)->where('coach_confirm', 1)->orderBy('created_at', 'desc')->get();
            $coach_booking_wait = \App\Models\BookingCoach::where('customer_id', $customer->id)->where('coach_confirm', 0)->orderBy('created_at', 'desc')->get();
        }

        if ($customer->is_active == true && !is_null($customer->email_verifed_at)) {
            return view('customer_frontOffice.dashboard', compact('customer', 'bookings', 'coach_booking_wait', 'coach_booking_conf', 'availabilities'));
        } else {
            return view('customer_frontOffice.go_to_active_your_account');
        }
    }

    public function showManagePofil()
    {
        $customer = Auth::guard('customer')->user();

        if ($customer->is_active == true && !is_null($customer->email_verifed_at)) {
            if (!is_null($customer->is_coach)) {
                $availabilities = \App\Models\Availability::where([
                    'coach_id' => $customer->is_coach->id,
                ])->orderBy('date_debut', 'asc')->get() ?? null;
            } else {
                $availabilities = null;
            }

            return view('customer_frontOffice.manage_profil', compact('customer', 'availabilities'));
        } else {
            return view('customer_frontOffice.go_to_active_your_account');
        }
    }

    public function managePofil(Request $request)
    {
        $customer = Auth::guard('customer')->user();
        $find = \App\Models\Customer::find($customer->id);
        if ($request->size) {
            $find->size = $request->size;
        }
        if ($request->weight) {
            $find->weight = $request->weight;
        }
        $find->save();

        Flash::success('Profil modifié avec succès');

        return redirect()->back();
    }

    public function settingManagePofil()
    {
        $customer = Auth::guard('customer')->user();

        return view('customer_frontOffice.profile.setting_profil', compact('customer'));
    }
    public function updateSettingProfile(Request $request)
    {
        $customer = Auth::guard('customer')->user();
        //dd($request->all());
        $update_customer = \App\Models\Customer::find($customer->id);
        if ($request->first_name) {
            $update_customer->first_name = $request->first_name;
        }
        if ($request->last_name) {
            $update_customer->last_name = $request->last_name;
        }
        if ($request->email) {
            $update_customer->email = $request->email;
        }
        if ($request->available) {
            $update_customer->available = $request->available;
        }

        if ($request->phone_number) {
            $update_customer->phone_number = $request->phone_number;
            if($request->dial_code){
                $update_customer->phone = $request->dial_code.$request->phone_number;
            }else{
                $update_customer->phone = $update_customer->dial_code.$request->phone_number;
            }
        }

        if ($request->size) {
            $update_customer->size = $request->size;
        }

        if ($request->weight) {
            $update_customer->weight = $request->weight;
        }

        if ($request->target_weight) {
            $update_customer->target_weight = $request->target_weight;
        }

        if ($request->bio) {
            $update_customer->bio = $request->bio;
        }

        if ($request->is_newsletter) {
            $update_customer->is_newsletter = $request->is_newsletter;
        }

        if ($request->commune) {
            $update_customer->commune = $request->commune;
            $update_customer->location_address = $request->commune;
        }

        if ($request->quartier) {
            $update_customer->quartier = $request->quartier;
        }

        if ($request->filled('lat')) {
            $update_customer->location_latitude = $request->lat;
        }

        if ($request->filled('lon')) {
            $update_customer->location_longitude = $request->lon;
        }

        $update_customer->save();

        $update_customer->name = $update_customer->first_name . ' ' . $update_customer->last_name;
        $update_customer->save();

        if ($request->new_password) {
            $request->validate([
                'current_password' => ['required', new MatchOldPassword],
                'new_password' => ['required'],
                'new_confirm_password' => ['same:new_password'],
            ]);

            \App\Models\Customer::find($customer->id)->update(['password' => Hash::make($request->new_password)]);
        }

        return redirect(route('customer.dashboard'))->with('message', 'Mise à jour effectuée avec succès !');
    }

    public function tickets()
    {
        $customer = Auth::guard('customer')->user();
        if (!$customer) {
            return redirect('customer.login');
        }
        $tickets = $customer->tickets;
        return view('customer_frontOffice.event_tickets.index', compact('customer', 'tickets'));
    }
    public function ticket($id)
    {
        $customer = Auth::guard('customer')->user();
        if (!$customer) {
            return view('errors.404');
        }

        $ticket = EventTicket::find($id);
        if (!$ticket) {
            return view('errors.404');
        }
        if ($ticket->customer_id != $customer->id) {
            return view('errors.404');
        }
        return view('customer_frontOffice.event_tickets.show', compact('customer', 'ticket'));
    }
    public function ticketView($id)
    {
        $customer = Auth::guard('customer')->user();
        if (!$customer) {
            return view('errors.404');
        }

        $ticket = EventTicket::find($id);
        if (!$ticket) {
            return view('errors.404');
        }
        if ($ticket->customer_id != $customer->id) {
            return view('errors.404');
        }
        return view('customer_frontOffice.event_tickets.view', compact('customer', 'ticket'));
    }

    public function ticketPdf($id)
    {
        $customer = Auth::guard('customer')->user();
        if (!$customer) {
            return view('errors.404');
        }

        $ticket = EventTicket::find($id);
        if (!$ticket) {
            return view('errors.404');
        }
        if ($ticket->customer_id != $customer->id) {
            return view('errors.404');
        }

        // $pdf = \PDF::loadView('pdf.download', compact('ticket', $ticket))->setOptions([
        //     'isRemoteEnabled'   => true,
        //     'isFontSubsettingEnabled'   => true,
        //     'isJavascriptEnabled'   => true,
        //     'isHtml5ParserEnabled'  =>  true,
        //     'defaultFont'   =>  'sans-serif'
        // ]);
        return view('pdf.download', compact('ticket', $ticket));

        // return $pdf->stream();
        return $pdf->download('Golden-Health-Ticket-' . $ticket->code . '.pdf');
    }

    public function shopOrders()
    {
        $customer = Auth::guard('customer')->user();

        if (!$customer) {
            return redirect('customer.login');
        }

        $orders = $customer->shopOrders();

        return view('customer_frontOffice.shop_orders.index', compact('customer', 'orders'));
    }

    public function coaches()
    {
        $customer = Auth::guard('customer')->user();

        if (!$customer) {
            return redirect('customer.login');
        }

        $coaches = \App\Models\Customer::find($customer->id)->coaches();
        return view('customer_frontOffice.coach.index', compact('customer', 'coaches'));
    }

    public function notifications()
    {
        $customer = Auth::guard('customer')->user();

        if (!$customer) {
            return redirect('customer.login');
        }

        $notifications = \App\Models\Customer::find($customer->id)->notifications();
        // dd($notifications);
        return view('customer_frontOffice.notifications.index', compact('customer', 'notifications'));
    }

    public function coupons()
    {
        $customer = Auth::guard('customer')->user();

        if (!$customer) {
            return redirect('customer.login');
        }

        $coupons = \App\Models\Customer::find($customer->id)->coupons();

        return view('customer_frontOffice.coupons.index', compact('customer', 'coupons'));
    }

    public function packs()
    {
        $customer = Auth::guard('customer')->user();

        if (!$customer) {
            return redirect('customer.login');
        }

        $packs = \App\Models\Customer::find($customer->id)->packs();

        return view('customer_frontOffice.packs.index', compact('customer', 'packs'));
    }

    public function shopOrder($orderId)
    {
        $customer = Auth::guard('customer')->user();
        if (!$customer) {
            return view('errors.404');
        }

        $order = $customer->shopOrders()->find($orderId);
        if (!$order) {
            return view('errors.404');
        }
        if ($order->customer_id != $customer->id) {
            return view('errors.404');
        }
        return view('customer_frontOffice.shop_orders.show', compact('customer', 'ticket'));
    }

    public function calculCustomerImc(Request $request)
    {
        $input = $request->all();
        $response = "not calcul";
        $customer_id = Auth::guard('customer')->user()->id;
        $input['customer_id'] = $customer_id;
        if ($input['customer_size'] && $input['customer_weight']) {
            $imc = intval($input['customer_weight']) / (intval($input['customer_size']) * intval($input['customer_size']));
            $imc_final = round($imc * 10000, 1);
            $queryBMI = BodyMassIndex::where('customer_id', $customer_id)->where('value_BMI', $imc_final)->first();
            if ($queryBMI == null) {
                $input['value_BMI'] = $imc_final;
                $input['date'] = date("Y-m-d H:i:s");
                $bmi = $this->BodyMassIndexRepository->create($input);
                $customer = \App\Customer::find($customer_id);
                if ($customer->size != intval($input['customer_size'])) {
                    $customer->size = intval($input['customer_size']);
                }
                if ($customer->weight != intval($input['customer_weight'])) {
                    $customer->weight = intval($input['customer_weight']);
                }
                $customer->save();
                return $this->sendResponse($bmi->toArray(), 'Body Indices saved successfully');
            } else {
                return response()->json($queryBMI);
            }
        }
    }

    public function CoachLesson()
    {

        $customer = Auth::guard('customer')->user();
        if (!$customer->is_coach) {
            return redirect()->back();
        }
        $courses = \App\Models\Course::where('coach_id', $customer->is_coach->id)->orderBy('updated_at', 'desc')->get();
        return view('customer_frontOffice.coach_lesson', compact('customer', 'courses'));
    }

    public function CoachLessonManage(Request $request)
    {
        $customer = Auth::guard('customer')->user();
        if (!$customer->is_coach) {
            return redirect()->back();
        }

        $disciplines = \App\Models\Discipline::all();
        $lieu_courses = \App\Models\LieuCourse::where('active', 1)->get();
        $objectives = \App\Models\Objective::where('active', 1)->get();
        $type_courses = \App\Models\CourseType::all();
        $diplomes = \App\Models\Diplome::where('coach_id', $customer->is_coach->id)->get();

        if ($request->has('action')) {
            if ($request->get('action') == 'update' && $request->has('course_id')) {
                $course = \App\Models\Course::where('id', $request->get('course_id'))->first();

                return view('customer_frontOffice.lesson_manage', compact('customer', 'disciplines', 'course', 'lieu_courses', 'objectives', 'type_courses', 'diplomes'));
            }
        }
        //$request->session()->remove('lessonCart');
        $cartGeneral = $request->session()->get('cartGeneral');
        $cartDetail = $request->session()->get('cartDetail');
        $cartCalendar = $request->session()->get('cartCalendar');
        //dd($cartDetail);
        if ($cartGeneral || $cartDetail || $cartCalendar) {
            return view('customer_frontOffice.lesson_manage', compact('customer', 'disciplines', 'lieu_courses', 'objectives', 'type_courses', 'cartGeneral', 'cartDetail', 'cartCalendar', 'diplomes'));
        } else {
            return view('customer_frontOffice.lesson_manage', compact('customer', 'disciplines', 'lieu_courses', 'objectives', 'type_courses', 'diplomes'));
        }
    }

    public function CoachLessonManageShow($id)
    {
        $course = \App\Models\Course::where('id', $id)->first();

        if (is_null($course)) {
            Flash::error("Course not found");
            return redirect("/customer/dashboard");
        }

        $course_lieu = \App\Models\CourseLieu::where('course_id', $course->id)->get();
        $course_objective = \App\Models\CourseObjective::where('course_id', $course->id)->get();
        $course_detail = \App\Models\CourseDetail::where('course_id', $course->id)->first();
        $availabilities = \App\Models\Availability::where('course_id', $course->id)->get();
        return view('customer_frontOffice.lesson_manage_show', compact('course', 'course_lieu', 'course_objective', 'course_detail', 'availabilities'));
    }

    public function CoachCalendar()
    {
        $customer = Auth::guard('customer')->user();
        if (!$customer->is_coach) {
            return redirect()->back();
        }

        $disciplines = \App\Models\Discipline::all();

        $elements = \App\Models\Availability::where([
            'is_active' => 1,
        ])->orderBy('start_time', 'asc')->get();

        foreach ($elements as $item) {
            if (\Carbon\Carbon::today()->greaterThan(\Carbon\Carbon::parse($item->day))) {
                $item->is_active = 0;
                $item->save();
            }
        }

        $elements = \App\Models\Availability::where([
            'coach_id' => $customer->is_coach->id,
            'is_active' => 1,
        ])->orderBy('start_time', 'asc')->get();

        $availabilities = collect($elements)->sortBy('start_time')->groupBy('day')->toArray();

        return view('customer_frontOffice.calendar_manage', compact('customer', 'disciplines', 'availabilities'));
    }

    public function CoachCalendarPost(Request $request)
    {
        $this->validate($request, [
            'coach_id' => 'required',
            'date' => 'required',
            'start_hour' => 'date_format:H:i|before:end_hour',
        ]);

        $input = $request->all();
        $dates = explode(",", $input['date']);

        $availabilities = \App\Models\Availability::where([
            'coach_id' => $input['coach_id'],
            'is_active' => 1,
        ])->get();

        // dd($availabilities);

        if (sizeof($dates) == 1) {
            foreach ($availabilities as $availability) {
                $startTime = \Carbon\Carbon::createFromFormat('H:i', $input['start_hour'])->setTimezone('UTC');
                $endTime = \Carbon\Carbon::createFromFormat('H:i', $input['end_hour'])->setTimezone('UTC');
                $startTimeAvailability = \Carbon\Carbon::createFromFormat('H:i:s', $availability->start_time)->setTimezone('UTC');
                $endTimeAvailability = \Carbon\Carbon::createFromFormat('H:i:s', $availability->end_time)->setTimezone('UTC');

                if (($startTimeAvailability->between($startTime, $endTime) && $dates[0] == $availability->day) || ($endTimeAvailability->between($startTime, $endTime) && $dates[0] == $availability->day)) {
                    Flash::error("Le créneau " . $input['start_hour'] . " - " . $input['end_hour'] . " est déjà utilisé pour la date " . $dates[0] . ".");
                    return redirect()->back();
                }
            }

            \App\Models\Availability::create([
                'coach_id' => $input['coach_id'],
                'day' => $dates[0],
                'start_time' => $input['start_hour'],
                'end_time' => $input['end_hour'],
                'is_active' => 1,
                'is_selected' => 0,
            ]);
        } else {

            foreach ($dates as $date) {
                foreach ($availabilities as $availability) {

                    $startTime = \Carbon\Carbon::createFromFormat('H:i', $input['start_hour']);
                    $endTime = \Carbon\Carbon::createFromFormat('H:i', $input['end_hour']);

                    $startTimeAvailability = \Carbon\Carbon::createFromFormat('H:i:s', $availability->start_time)->setTimezone('UTC');
                    $endTimeAvailability = \Carbon\Carbon::createFromFormat('H:i:s', $availability->end_time)->setTimezone('UTC');

                    if (($startTimeAvailability->between($startTime, $endTime) && $date == $availability->day) || ($endTimeAvailability->between($startTime, $endTime) && $date == $availability->day)) {
                        Flash::error("Le créneau " . $input['start_hour'] . " - " . $input['end_hour'] . " est déjà utilisé pour la date " . $date . ".");
                        return redirect()->back();
                    }
                }

                \App\Models\Availability::create([
                    'coach_id' => $input['coach_id'],
                    'day' => $date,
                    'start_time' => $input['start_hour'],
                    'end_time' => $input['end_hour'],
                    'is_active' => 1,
                    'is_selected' => 0,
                ]);
            }
        }

        Flash::success('Disponibilité enregistré avec succès.');
        return redirect('/customer/coach/calendar');
    }

    public function LessonStoreCart(Request $request)
    {
        $customer = Auth::guard('customer')->user();
        $input = $request->all();
        $requestInput = [];
        $lessonCart = $request->session()->get('lessonCart');
        if ($input['form_type'] == 'general') {
            $request->session()->put('cartGeneral', $input);
        }
        if ($input['form_type'] == 'detail') {
            $path_url = $this->fileUpload($request);
            $cartDetail = [
                'form_type' => $request->form_type,
                'lieucourse' => $request->lieucourse,
                'objectif' => $request->objectif,
                'type_course' => $request->type_course,
                'nb_person' => $request->nb_person,
                'file_diplome' => $path_url,
                'diplome' => $request->diplome
            ];
            $request->session()->put('cartDetail', $cartDetail);
        }
        if ($input['form_type'] == 'calendar') {
            $request->session()->put('cartCalendar', $input);
        }
        /*if ($lessonCart) {
        array_push($requestInput, $input);
        $request->session()->put('lessonCart', $requestInput);
        } else {
        $request->session()->put('lessonCart', $input);
        }*/
        return $this->sendResponse($input, 'Lesson Cart saved successfully');
    }


    public function CoachLessonSave(Request $request)
    {
        // try {

        $customer = Auth::guard('customer')->user();
        $cartGeneral = $request->session()->get('cartGeneral');
        $cartDetail = $request->session()->get('cartDetail');
        $cartCalendar = $request->session()->get('cartCalendar');
        $input = $request->all();
        $course_id = $request->course_id;
        if (!is_null($course_id)) {
            $course =  \App\Models\Course::where('id', $course_id)->first();
            $course_lieu =  \App\Models\CourseLieu::where('course_id', $course_id)->delete();
            $course_objective = \App\Models\CourseObjective::where('course_id', $course_id)->delete();
        } else {
            $course = new \App\Models\Course();
            $check_existe_discipline = \App\Models\Course::where('discipline_id', $cartGeneral['discipline_id'])
                ->where('course_type', $cartDetail['type_course'])
                ->where('coach_id', $customer->is_coach->id)
                ->first();
            if ($check_existe_discipline != null) {
                return $this->sendError('Un cour similaire existe déjà, vérifiez vos informations svp.');
            }
        }

        $course->coach_id = $customer->is_coach->id;
        $course->discipline_id = $cartGeneral['discipline_id'];
        $course->price = $input['price'];
        $course->course_type = $cartDetail['type_course'];
        if ($course->save()) {
            if (!is_null($course_id)) {
                $course = \App\Models\Course::where('id', $course_id)->first();
                $course_lieu = \App\Models\CourseLieu::where('course_id', $course_id)->delete();
                $course_objective = \App\Models\CourseObjective::where('course_id', $course_id)->delete();
                $course_detail = $course->detail;
            } else {
                $course_detail = new \App\Models\CourseDetail();
            }

            $course_detail->course_id = $course->id;
            $course_detail->experience = (isset($cartGeneral['experience']) ? $cartGeneral['experience'] : '');
            $course_detail->seance_type = (isset($cartGeneral['seance_type']) ? $cartGeneral['seance_type'] : '');
            $course_detail->is_diplome = isset($cartDetail['diplome']) ? 1 : 0;
            // dd($cartDetail);
            if (isset($cartDetail['diplome']) || !empty($cartDetail['diplome'])) {
                $diplome_id =  intval($cartDetail['diplome']);
                $diplome = \App\Models\Diplome::find($diplome_id);
                $course_detail->diplome_url = $diplome->image_url;
                $course_detail->diplome_name = $diplome->diplome_name;
                $course_detail->diplome_id = $diplome->id;
                $course_detail->save();
            }

            $course_detail->nb_person = isset($cartDetail['nb_person']) ? $cartDetail['nb_person'] : 1;
            $course_detail->has_collectif = (\Str::of($cartDetail['type_course'])->replace('-', ' ') != 'cour particulier' ? 1 : 0);
            $course_detail->has_particulier = (\Str::of($cartDetail['type_course'])->replace('-', ' ') == 'cour particulier' ? 1 : 0);
            $course_detail->save();

            // if (isset($cartDetail['file_diplome']) && isset($cartDetail['file_diplome']['url'])) {
            //   $new_diplome = new \App\Models\Diplome();
            //   $new_diplome->name = $cartDetail['file_diplome']['name'];
            //   $new_diplome->image_url = $cartDetail['file_diplome']['url'];
            //   $new_diplome->customer_id = $customer->id;
            //   $new_diplome->save();
            // }

            if (!empty($cartDetail['lieucourse'])) {
                foreach ($cartDetail['lieucourse'] as $key => $value) {
                    $course_lieu = new \App\Models\CourseLieu();
                    $course_lieu->course_id = $course->id;
                    $course_lieu->lieu_course_id = $value;
                    $course_lieu->save();
                }
            }

            if (!empty($cartDetail['objectif'])) {
                foreach ($cartDetail['objectif'] as $key => $value) {
                    $course_objective = new \App\Models\CourseObjective();
                    $course_objective->course_id = $course->id;
                    $course_objective->objective_id = $value;
                    $course_objective->save();
                }
            }
            // $course->coach_id = $customer->is_coach->id;
            // $course->discipline_id = $cartGeneral['discipline_id'];
            // $course->price = $input['price'];
            // $course->course_type = $cartDetail['type_course'];
            // if ($course->save()) {
            //     if ($course_id) {
            //         $course_detail = \App\Models\CourseDetail::where('course_id', $course_id)->first();
            //     } else {
            //         $course_detail = new \App\Models\CourseDetail();
            //     }
            // $course_detail->course_id = $course->id;
            // $course_detail->experience = (isset($cartGeneral['experience']) ? $cartGeneral['experience'] : '');
            // $course_detail->seance_type = (isset($cartGeneral['seance_type']) ? $cartGeneral['seance_type'] : '');
            // $course_detail->is_diplome = (isset($cartDetail['diplome']) ? 1 : 0);
            // if(isset($cartDetail['diplome'])){
            //     $diplome = \App\Models\Diplome::find((int) $cartDetail['diplome']);
            //     $course_detail->diplome_url = $diplome->image_url;
            //     $course_detail->diplome_name = $diplome->diplome_name;
            //     $course_detail->diplome_id = $diplome->id;
            // }

            // $course_detail->has_collectif = (\Str::of($cartDetail['type_course'])->replace('-', ' ') != 'cour particulier' ? 1 : 0);
            // $course_detail->has_particulier = (\Str::of($cartDetail['type_course'])->replace('-', ' ') == 'cour particulier' ? 1 : 0);
            // $course_detail->nb_person = isset($cartDetail['nb_person']) ? $cartDetail['nb_person'] : 1;
            // $course_detail->save();

            // if (!empty($cartDetail['lieucourse'])) {
            //     foreach ($cartDetail['lieucourse'] as $key => $value) {
            //         $course_lieu = new \App\Models\CourseLieu();
            //         $course_lieu->course_id = $course->id;
            //         $course_lieu->lieu_course_id = $value;
            //         $course_lieu->save();
            //     }
            // }

            // if (!empty($cartDetail['objectif'])) {
            //     foreach ($cartDetail['objectif'] as $key => $value) {
            //         $course_objective = new \App\Models\CourseObjective();
            //         $course_objective->course_id = $course->id;
            //         $course_objective->objective_id = $value;
            //         $course_objective->save();
            //     }
            // }

            // if (!empty($cartCalendar) || !is_null($cartCalendar)) {
            //     $date_ranges = explode("-",$cartCalendar['datesRangeCourse']);
            //     $period = \Carbon\CarbonPeriod::create(trim($date_ranges[0]), trim($date_ranges[1]));

            //     //dd($availability);
            //     foreach ($period as $value) {

            //         \App\Models\Availability::create([
            //             'coach_id' => $customer->is_coach->id,
            //             'course_id' => $course->id,
            //             'date_debut' => $value->format('Y-m-d'),
            //             'date_fin' => $value->format('Y-m-d'),
            //             'start_time' => $cartCalendar['start_time'],
            //             'end_time' => $cartCalendar['end_time'],
            //             'is_active' => 1,
            //             'is_selected' => 0,
            //         ]);
            //     }
            // }

            if (!empty($input['availability']) || !is_null($input['availability'])) {
                $availability = json_decode($input['availability']);
                //dd($availability);
                foreach ($availability as $key => $value) {

                    \App\Models\Availability::create([
                        'coach_id' => $customer->is_coach->id,
                        'course_id' => $course->id,
                        'date_debut' => $value->startDate,
                        'date_fin' => $value->endDate,
                        'start_time' => $value->startTime,
                        'end_time' => $value->endTime,
                        'is_active' => 1,
                        'is_selected' => 0,
                    ]);
                }
            }

            if (!empty($course)) {
                $newCoachDiscipline = \App\Models\CoachDiscipline::where([
                    'coach_id' => $customer->is_coach->id,
                    'discipline_id' => $course->discipline_id
                ])->first();

                if (empty($newCoachDiscipline)) {
                    $newCoachDiscipline = \App\Models\CoachDiscipline::create([
                        'coach_id' => $customer->is_coach->id,
                        'discipline_id' => $course->discipline_id
                    ]);
                }
            }


            // }
            $request->session()->forget('cartGeneral');
            $request->session()->forget('cartDetail');
            $request->session()->forget('cartCalendar');

            if ($course_id) {
                $data = [
                    'courseID' => $course->id,
                    'name' => $course->discipline->name,
                    'courseUrl' => env('APP_URL') . "courses/" . $course->id,
                ];

                // notifying admins
                foreach (\App\User::all() as $user) {
                    Mail::to($user->email)->send(new AdminNewCourse($data));
                }
            }
            return $this->sendResponse($course, 'Course saved successfully');
        }
        // } catch (\Exception $e) {
        //     return response()->json(["success" => false, "message" => $e->getMessage()]);
        // }
    }

    public function saveBookingCoach(Request $request)
    {
        try {

            $customer = Auth::guard('customer')->user();
            if ($request->get('booking_id')) {
                $last_booking = \App\Models\BookingCoach::where('id', $request->get('booking_id'))->first();
                if ($last_booking) {
                    $transaction = \DB::table('order_items')->where('order_items.meta_data_id', $last_booking->id)
                        ->join('invoices', 'order_items.order_id', 'invoices.order_id')
                        ->join('customers', 'invoices.customer_id', 'customers.id')
                        ->select('invoices.*', 'invoices.id as invoice_id', 'order_items.*', 'customers.*')
                        ->first();

                    $transactionId = self::generateTransId($transaction->customer_id, $transaction->invoice_id, $transaction->order_id);

                    $transactionNew = InvoicePayment::create([
                        'invoice_id' => $transaction->invoice_id,
                        'payment_method' => "online",
                        'payment_reference' => $transactionId,
                        'amount' => $transaction->total,
                        'currency_code' => $transaction->currency_code,
                        'currency_name' => $transaction->currency_name,
                        'creator_id' => $transaction->customer_id,
                        'creator' => 'customers',
                        'creator_name' => $transaction->name,
                        'status' => 'WAITING',
                        'is_waiting' => true,
                        'is_completed' => false,
                    ]);

                    $paymentSession = [
                        'payment_method' => $transactionNew->payment_method,
                        'transaction_id' => $transactionNew->payment_reference,
                        'transaction_designation' => 'Payer Coaching',
                        'amount' => intval($transactionNew->amount),
                        'currency_code' => $transactionNew->currency_code,
                        'currency_name' => "CFA",
                        'status' => $transactionNew->status,
                        'booking_id' => $transaction->order_id,
                    ];
                }
            } else {

                $booking = $request->session()->get('booking');

                $course = \App\Models\Course::where('id', $booking['course_id'])->first();
                $discipline = \App\Models\Discipline::where('id', $course->discipline_id)->first();
                $coach = \App\Models\Coach::where('id', $course->coach_id)->first();
                $pack = \App\Models\Pack::where('id', $booking['pack']->id)->first();
                $currency_code = $course->currency_code;
                $currency_name = $course->currency_name;
                $amount = $booking['totalprice'];
                $customer_id = $customer->id;
                $new_booking = new \App\Models\BookingCoach();
                $new_booking->course_id = $course->id;
                $new_booking->customer_id = $customer->id;
                $new_booking->pack_id = $pack->id;
                $new_booking->nb_personne = $booking['nbperson'];
                $new_booking->amount = $booking['totalprice'];
                $new_booking->course_lieu_id = $booking['type_lieu'];
                $new_booking->course_lieu_note = $booking['message'];
                $new_booking->address_name = $booking['address_autocomplete'];
                $new_booking->address_cpl = $booking['address_other_new'];
                $new_booking->quantity = $booking['nb_lesson'];
                $new_booking->start_date = $booking['start']->format('d/m/Y - h:i');
                $new_booking->end_date = $booking['end']->format('d/m/Y - h:i');
                $new_booking->coach_confirm = 0;
                $new_booking->status = 1;
                $new_booking->status_name = "created";
                if ($new_booking->save()) {
                    $last_booking = \App\Models\BookingCoach::where('id', $new_booking->id)->first();
                    $booking_data = $last_booking;
                    $sysOrdNo = 1000;
                    $last_booking->booking_ref = "ORD-" . ($sysOrdNo + $last_booking->id);
                    $last_booking->save();

                    if ($booking_data) {
                        $order = Order::create([
                            'customer_id' => $customer_id,
                            'amount' => $amount,
                            'currency_code' => $currency_code,
                            'currency_name' => $currency_name,
                            'payment_method_slug' => $request->payement_method_slug,
                            'delivery_fees' => "0",
                            'is_waiting' => true,
                            'is_delivery' => false,
                            'service_slug' => 'coaching',
                            'status' => 'waiting'
                        ]);

                        $orderItem = OrderItem::create([
                            'order_id' => $order->id,
                            'service_slug' => 'coaching',
                            'meta_data_id' =>  $booking_data->id,
                            //'meta_data' => json_encode($booking_data->toArray()),
                            'quantity' => 1,
                            'unit_price' => $amount,
                            'total_amount' => $amount,
                            'currency_code' => $currency_code,
                            'currency_name' => $currency_name
                        ]);

                        $orderItems = OrderItem::where('order_id', $order->id)->get();

                        $tax = 0;
                        $fees_delivery = 0;
                        //$total = $subtotal + $tax;
                        $invoice = Invoice::create([
                            'order_id' => $order->id,
                            'service' => 'coaching',
                            'customer_id' => $customer_id,
                            'reference' => 'C' . $order->id . '-' . $customer_id . '-' . time(),
                            'link' => '',
                            'subtotal' => $amount,
                            'tax' => $tax,
                            'fees_delivery' => $fees_delivery,
                            'total' => $amount,
                            'status' => 'unpaid',
                            'is_paid' => false,
                            'currency_code' => $currency_code,
                            'currency_name' => $currency_name,

                        ]);

                        if ($request->session()->has('coupons')) {
                            foreach ($request->session()->has('coupons') as $coupon) {
                                $amount_saved = $booking['price'] * (1 - ($coupon->pourcentage / 100));
                                $invoice->discount += $amount_saved;
                                $invoice->save();

                                CouponOrder::create([
                                    'order_id' => $order->id,
                                    'coupon_id' => $coupon->id,
                                    'amount_saved' => $amount_saved,
                                ]);

                                \App\CouponOrder::create([
                                    'order_id' => $order->id,
                                    'coupon_id' => $coupon->id,
                                    'amount_saved' => $amount_saved
                                ]);
                            }
                            $request->session()->forget('coupons');
                        }

                        $transactionId = self::generateTransId($customer_id, $invoice->id, $order->id);

                        $transaction = InvoicePayment::create([
                            'invoice_id' => $invoice->id,
                            'payment_method' => "online",
                            'payment_reference' => $transactionId,
                            'amount' => $invoice->total,
                            'currency_code' => $invoice->currency_code,
                            'currency_name' => $invoice->currency_name,
                            'creator_id' => $customer_id,
                            'creator' => 'customers',
                            'creator_name' => $customer->name,
                            'status' => 'WAITING',
                            'is_waiting' => true,
                            'is_completed' => false,
                        ]);

                        \App\CustomerAvailabilities::create([
                            'booking_id' => $booking_data->id,
                            'challenger_id' => $customer->id,
                            'availabities_id' => $booking['availability_id'],
                        ]);

                        $request->session()->forget('booking');

                        $paymentSession = [
                            'payment_method' => $transaction->payment_method,
                            'title' => $transactionId,
                            'transaction_designation' => 'Payer Coaching',
                            'amount' => intval($transaction->amount),
                            'currency_code' => $transaction->currency_code,
                            'currency_name' => "CFA",
                            'status' => $transaction->status,
                            'booking_id' => $booking_data->id,
                        ];

                        //$request->session()->put('payment', $paymentSession);

                        //Une fois la reservation du cours créée, envoiyer des notification Admin et coach

                        // notifying admins
                        $admin_email_data= [
                            'subject'=>"Notification réservation",
                            'title' =>'Nouvelle réservation',
                            'subtitle' =>'Un utilisateur vient de passer une nouvelle réservation',
                            'body' => "L'utilisateur <b>".$customer->name."</b> a effectué une réservation de cours avec le coach <b>".$coach->customer->name."</b> pour le <b>".$booking['start']->format('d/m/Y - h:i')."</b>.<br/><br/>Contact client : ".$customer->phone."<br/> Contact Coach : ".$coach->customer->phone."<br/>"


                        ];

                        foreach (User::all() as $user) {
                            Mail::to($user->email)->send(new BookingPayNotifyMail($admin_email_data));
                        }

                        // Notification Coach
                        $coach_email_data= [
                            'subject'=>"Notification réservation",
                            'title' =>'Nouvelle réservation',
                            'subtitle' =>'Un utilisateur vient de passer une nouvelle réservation',
                            'body' => "L'utilisateur ".$customer->name." a effectué une nouvelle réservation de cours de ". $course->discipline->name." pour le ".$booking['start']->format('d/m/Y - h:i')."<br/>"

                        ];
                        Mail::to($coach->customer->email)->send(new BookingPayNotifyMail($coach_email_data));

                    }
                }
            }
            return $this->sendResponse($paymentSession, 'Booking Coach saved successfully');
        } catch (\Exception $e) {

        }
    }

    public function CoachLessonDelete($id)
    {
        $course = \App\Models\Course::where('id', $id)->first();

        if (empty($course)) {
            Flash::error('Course not found');

            return redirect()->back();
        }

        \App\Models\Course::where('id', $id)->delete();

        \App\Models\Availability::where('course_id', $id)->delete();

        Flash::success('Course deleted successfully.');

        return redirect()->back();
    }

    public function validateTransaction($transactionId, $booking_id)
    {
        $transaction = InvoicePayment::where(['payment_reference' => $transactionId])->first();

        if ($transaction->status == 'SUCCESSFUL') {
            $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();
            $invoice->status = 'paid';
            $invoice->is_paid = 1;
            $invoice->save();

            $order = Order::where(['id' => $invoice->order_id])->first();

            $customer = \App\Models\Customer::where(['id' => $order->customer_id])->first();

            $order->current_status = Order::CUSTOMER_PAID;
            $order->is_waiting_payment = 0;
            $order->is_delivery = 1;
            $order->save();

            if ($order != null) {
                $customer_booking = \App\Models\BookingCoach::where([
                    'id' => $booking_id,
                ])->first();

                $customer_booking->status = 2;
                $customer_booking->status_name = 'paid';
                $customer_booking->save();
            }
        }
    }

    public function cancelTransaction($transactionId)
    {
        $transaction = InvoicePayment::where(['payment_reference' => $transactionId])->first();

        $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();
        $invoice->status = 'canceled';
        $invoice->is_paid = false;
        $invoice->save();

        $order = Order::where(['id' => $invoice->order_id])->first();
        $order->status = 'canceled';
        $order->save();
    }

    /**
     * generate transId
     * @return int
     */
    public static function generateTransId($customerId, $invoiceId, $orderId)
    {
        $timestamp = time();
        $parts = explode(' ', microtime());
        $id = ($timestamp + $parts[0] - strtotime('today 00:00')) * 10;
        $id = sprintf('%06d', $id) . mt_rand(100, 9999);
        $transId = $id . "-" . $customerId . "-" . $invoiceId . "-" . $orderId;

        $transactions = InvoicePayment::where(['payment_reference' => $transId])->get();
        if (count($transactions) == 0) {

            return $transId;
        } else {
            $autoIncrement = count($transactions) + 1;
            $transId = $transId . '-' . $autoIncrement;
            return $transId;
        }
    }

    public function GoPayementHandler(Request $request)
    {

        $data = request()->validate([
            'amount' => 'required|integer|gt:0',
            'payment_method' => ['required', Rule::in(PaymentMethod::values())],
            'order_id' => 'required|exists:booking_coaches,id',
        ]);

        $order = \App\Models\BookingCoach::findOrFail($data['order_id']);

        return $this->inferGatewayHandlerClass($data['payment_method'])->charge(OrderType::ACCOUNT_CUSTOMER, $order, $data['amount']);
    }

    private function inferGatewayHandlerClass(string $paymentGateway): PaymentGatewayInitiator
    {
        $gatewayHandlerClass = self::PAYMENT_GATEWAY_HANDLERS[$paymentGateway];

        return new $gatewayHandlerClass;
    }

    public function PagePay(Request $request)
    {

        $payment = $request->session()->get('payment');

        return view('customer_frontOffice.payment', compact('payment'));
    }

    public function getInvoice($id)
    {

        $invoice = \DB::table('invoices')->where('invoices.id', $id)
            ->join('orders', 'orders.id', 'invoices.order_id')
            ->join('order_items', 'orders.id', 'order_items.order_id')
            ->join('invoice_payments', 'invoices.id', 'invoice_payments.invoice_id')
            ->join('customers', 'invoices.customer_id', 'customers.id')
            ->select('invoices.*', 'invoices.created_at as invoice_date', 'order_items.*', 'customers.*')
            ->first();

        return view('customer_frontOffice.my_invoice', compact('invoice'));
    }

    public function PayNotify(Request $request)
    {
        //dd($request);
    }

    public function updatePofileAvatar(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        if ($validation->passes()) {

            $image = $request->file('avatar');
            $filename = $image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $new_name = sha1($filename . time()) . '.' . $extension;
            $destinationPath = public_path() . '/files_clients/';
            $image->move($destinationPath, $new_name);
            $destinationPath1 = 'http://' . $_SERVER['HTTP_HOST'] . '/files_clients/';
            $filest = array();
            $filest['name'] = $new_name;
            $filest['size'] = $this->get_file_size($destinationPath . $new_name);
            $filest['url'] = $destinationPath1 . $new_name;
            $filest['thumbnailUrl'] = $destinationPath1 . $new_name;
            $filesa['files'][] = $filest;

            $customer = Auth::guard('customer')->user();
            $find = \App\Models\Customer::find($customer->id);
            $find->photo_url = $destinationPath1 . $new_name;
            $find->save();

            return $filesa;
        }
    }

    protected function get_file_size($file_path, $clear_stat_cache = false)
    {
        if ($clear_stat_cache) {
            if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
                clearstatcache(true, $file_path);
            } else {
                clearstatcache();
            }
        }
        return $this->fix_integer_overflow(filesize($file_path));
    }

    protected function fix_integer_overflow($size)
    {
        if ($size < 0) {
            $size += 2.0 * (PHP_INT_MAX + 1);
        }
        return $size;
    }

    public function fileUpload(Request $request)
    {
        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $file = $request->file('file');
            $extension = $request->file->extension();
            $fileName = 'tmp' . round(microtime(true) * 1000) . '.' . $extension;
            $destinationPath = public_path() . '/temp/';
            $file->move($destinationPath, $fileName);
            $destinationPath1 = 'http://' . $_SERVER['HTTP_HOST'] . '/temp/';
            $path_url = $destinationPath1 . $fileName;
            $response = ['name' => $fileName, 'url' => $path_url];
            return $response;
        }
    }

    public function getDiplome()
    {

        $customer = Auth::guard('customer')->user();

        if (!$customer) {
            return redirect('customer.login');
        }

        $diplomes = \App\Models\Diplome::where('coach_id', $customer->is_coach->id)->get();

        return view('customer_frontOffice.coach.diplome', compact('customer', 'diplomes'));
    }

    public function getPortfolio()
    {

        $customer = Auth::guard('customer')->user();

        if (!$customer) {
            return redirect('customer.login');
        }

        $portfolios = \App\Models\Portfolio::where('coach_id', $customer->is_coach->id)->get();

        return view('customer_frontOffice.coach.portfolio', compact('customer', 'portfolios'));
    }

    public function UploadDiplome(Request $request)
    {
        $customer = Auth::guard('customer')->user();
        if ($request->hasFile('input-b9')) {
            $files = $request->file('input-b9');
            foreach ($files as $file) {
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $fileName = 'tmp' . round(microtime(true) * 1000) . '.' . $extension;
                $destinationPath = public_path() . '/files_clients/';
                $file->move($destinationPath, $fileName);
                $destinationPath1 = 'http://' . $_SERVER['HTTP_HOST'] . '/files_clients/';
                $path_url = $destinationPath1 . $fileName;

                $new_diplome = new \App\Models\Diplome();
                $new_diplome->diplome_name = $request->diplome_name;
                $new_diplome->name = $fileName;
                $new_diplome->image_url = $path_url;
                $new_diplome->coach_id = $customer->is_coach->id;
                $new_diplome->save();
            }
        }
        return response()->json(['uploaded']);
    }

    public function UploadPortfolio(Request $request)
    {
        $customer = Auth::guard('customer')->user();
        $new_diplome = new \App\Models\Portfolio();

        if ($request->hasFile('input-b9')) {
            $file = $request->file('input-b9');
        //  foreach($files as $file) {
            //$filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $fileName = 'tmp' . round(microtime(true) * 1000) . '.' . $extension;
            $destinationPath = public_path() . '/files_clients/';
            $file->move($destinationPath, $fileName);
            $destinationPath1 = 'http://' . $_SERVER['HTTP_HOST'] . '/files_clients/';
            $path_url = $destinationPath1 . $fileName;
            $new_diplome->images_url = $path_url;
            //  }
        }
        if($request->file_type == 'youtube') {
            $new_diplome->video_url = 'https://www.youtube.com/embed/' . $request->youtube_input;
        }
        if($request->file_type == 'facebook'){
            $facebook_link = explode(' ',$request->facebook_input);
            $facebook_link = $facebook_link[1];
            $facebook_link = substr(substr($facebook_link, 0, -1),5);
            $new_diplome->video_url = $facebook_link;
        }
        $new_diplome->title = $request->title;
        $new_diplome->media_type = $request->file_type ?? $request->file_type;
        $new_diplome->coach_id = $customer->is_coach->id;
        $new_diplome->save();
        if ($request->wantsJson()) {
            return response()->json(['uploaded']);
        } else {
            return redirect(url('/customer/coach/diplome'));
        }
        // return redirect(url('/customer/coach/portfolio'));
    }

    public function destroyDiplome($id)
    {   //For Deleting Users
        $Users = \App\Models\Diplome::find($id);
        $Users->delete($id);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function destroyPortfolio($id)
    {   //For Deleting Users
        $Users = \App\Models\Portfolio::find($id);
        $Users->delete($id);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function saveReview(Request $request)
    {

        $customer = Auth::guard('customer')->user();

        $booking = \App\Models\BookingCoach::where('id', $request->booking_id)->first();

        \App\Models\Note::create([
            'customer_id' => $customer->id,
            'course_id' => $booking->course_id,
            'message' => $request->message,
            'rate' => $request->rate
        ]);

        Flash::success('Enregistrement de la note effectuée avec succès.');

        return redirect()->back();
    }

    //   **** Whishlist / liste de souhaits à garder pour plus tard
    public function wishlistToggle($prodId)
    {
        $customer = Auth::guard('customer')->user();
        if (!$customer) {
            return redirect()->back();
        }

        $product = Product::find($prodId);
        if (!$product) {
            return redirect()->back();
        }


        $wishlistItem = Wishlist::where('product_id', $product->id)->where('customer_id', $customer->id)->first();

        if (!$wishlistItem) {
            $wishlistItem = Wishlist::create([
                'customer_id' => $customer->id,
                'product_id' => $product->id,
            ]);
            return redirect()->back();
        }

        $wishlistItem->delete();
        return redirect()->back();
    }

    public function showWishlist()
    {
        $customer = Auth::guard('customer')->user();
        if (!$customer) {
            return redirect()->back();
        }

        $products = $customer->wishlistedProducts;
        return view('customer_frontOffice.shops.wishlist', compact('products'));
    }
    //   **** Reviews / avis utilisateurs sur les produits

    public function showReviews()
    {
        $customer = Auth::guard('customer')->user();
        if (!$customer) {
            return redirect()->back();
        }
        // shop reviews

        // $shopReviews = Review::where('source')
        // coach reviews

        // $products = $customer->wishlistedProducts;

        $deliveredOrders = $customer->deliveredOrders();
        $bookings = \App\Models\Customer::find($customer->id)->booking()->get();
        $pastCourses = collect($bookings)->map(function ($booking) {
            return $booking->course()->first();
        })->unique()->filter(function ($course, $key) {
            if (is_null($course)) {
                return false;
            } else {
                return !is_null($course->availability) ? $course->availability->isPast() : false;
            }
        });

        $reviews = \App\Models\Customer::find($customer->id)->is_coach->reviews ?? null;

        // dd($pastCourses);
        // How to get if order i

        // dd($deliveredOrders->first()->productsToReview());

        return view('customer_frontOffice.reviews.index', compact('customer', 'deliveredOrders', 'pastCourses', 'reviews'));
    }
    public function createReviews($orderId, $prodId)
    {
        $customer = Auth::guard('customer')->user();
        if (!$customer) {
            Flash::error('Connectez vous');
            return redirect()->back();
        }
        // shop reviews

        // $shopReviews = Review::where('source')
        // coach reviews

        // $products = $customer->wishlistedProducts;
        $order = $customer->deliveredOrders()->where('id', $orderId)->first();
        if (!$order) {
            Flash::error('Produit invalide');
            return redirect()->back();
        }

        $product = $order->productsToReview()->where('id', $prodId)->first();
        if (!$product) {
            Flash::error('Produit invalide');
            return redirect()->back();
        }

        return view('customer_frontOffice.reviews.create', compact('customer', 'product'));
    }

    public function createReviewsCourse($courseId)
    {
        $customer = Auth::guard('customer')->user();
        if (!$customer) {
            Flash::error('Connectez vous');
            return redirect()->back();
        }

        $course = \App\Models\Course::where('id', $courseId)->first();
        if (is_null($course)) {
            Flash::error('Cours invalide');
            return redirect()->back();
        }

        return view('customer_frontOffice.reviews.create_course', compact('customer', 'course'));
    }

    public function storeReviews(Request $request)
    {

        $customer = Auth::guard('customer')->user();
        $input = $request->all();
        if (!isset($input['rating'])) {
            return redirect()->back();
        }

        if (!isset($input['product_id'])) {
            return redirect()->back();
        }

        $review = new Review();
        $review->rating = $input['rating'];
        $review->service = "shop";
        $review->source = "product";
        $review->source_id = $input['product_id'];

        if (isset($input['title'])) {
            $review->title = $input['title'];
        }

        $review->customer_id = $customer->id;

        if (isset($input['content'])) {
            $review->content = $input['content'];
        }

        $review->save();
        Flash::success('Merci pour votre avis');
        //return redirect('/customer/dashboard');
        return redirect()->back();
    }

    public function storeReviewsCourse(Request $request)
    {
        $customer = Auth::guard('customer')->user();
        $input = $request->all();

        if (!isset($input['rating'])) {
            return redirect()->back();
        }

        if (!isset($input['course_id'])) {
            return redirect()->back();
        }

        $review = new Review();
        $review->rating = $input['rating'];
        $review->service = "booking";
        $review->source = "course";
        $review->source_id = $input['course_id'];

        if (isset($input['title'])) {
            $review->title = $input['title'];
        }

        $review->customer_id = $customer->id;

        if (isset($input['content'])) {
            $review->content = $input['content'];
        }

        $review->save();

        //coach change rating
        $course = \App\Models\Course::find($input['course_id']);
        $coach_course = \App\Models\Coach::find($course->coach_id);
        $coach_course->nbr_personne = $coach_course->nbr_personne + 1;
        $coach_course->sum = $coach_course->sum + (int) $input['rating'];
        $coach_course->save();
        $coach_course->rating = (string) ((int) ($coach_course->sum /  $coach_course->nbr_personne));
        $coach_course->save();

        Flash::success('Merci pour votre avis');
        return redirect('/customer/dashboard');
    }
}
