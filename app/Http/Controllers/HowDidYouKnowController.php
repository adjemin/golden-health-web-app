<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateHowDidYouKnowRequest;
use App\Http\Requests\UpdateHowDidYouKnowRequest;
use App\Repositories\HowDidYouKnowRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class HowDidYouKnowController extends AppBaseController
{
    /** @var  HowDidYouKnowRepository */
    private $howDidYouKnowRepository;

    public function __construct(HowDidYouKnowRepository $howDidYouKnowRepo)
    {
        $this->howDidYouKnowRepository = $howDidYouKnowRepo;
    }

    /**
     * Display a listing of the HowDidYouKnow.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $howDidYouKnows = $this->howDidYouKnowRepository->all();

        return view('how_did_you_knows.index')
            ->with('howDidYouKnows', $howDidYouKnows);
    }

    /**
     * Show the form for creating a new HowDidYouKnow.
     *
     * @return Response
     */
    public function create()
    {
        return view('how_did_you_knows.create');
    }

    /**
     * Store a newly created HowDidYouKnow in storage.
     *
     * @param CreateHowDidYouKnowRequest $request
     *
     * @return Response
     */
    public function store(CreateHowDidYouKnowRequest $request)
    {
        $input = $request->all();

        $howDidYouKnow = $this->howDidYouKnowRepository->create($input);

        Flash::success('How Did You Know saved successfully.');

        return redirect(route('howDidYouKnows.index'));
    }

    /**
     * Display the specified HowDidYouKnow.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $howDidYouKnow = $this->howDidYouKnowRepository->find($id);

        if (empty($howDidYouKnow)) {
            Flash::error('How Did You Know not found');

            return redirect(route('howDidYouKnows.index'));
        }

        return view('how_did_you_knows.show')->with('howDidYouKnow', $howDidYouKnow);
    }

    /**
     * Show the form for editing the specified HowDidYouKnow.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $howDidYouKnow = $this->howDidYouKnowRepository->find($id);

        if (empty($howDidYouKnow)) {
            Flash::error('How Did You Know not found');

            return redirect(route('howDidYouKnows.index'));
        }

        return view('how_did_you_knows.edit')->with('howDidYouKnow', $howDidYouKnow);
    }

    /**
     * Update the specified HowDidYouKnow in storage.
     *
     * @param int $id
     * @param UpdateHowDidYouKnowRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHowDidYouKnowRequest $request)
    {
        $howDidYouKnow = $this->howDidYouKnowRepository->find($id);

        if (empty($howDidYouKnow)) {
            Flash::error('How Did You Know not found');

            return redirect(route('howDidYouKnows.index'));
        }

        $howDidYouKnow = $this->howDidYouKnowRepository->update($request->all(), $id);

        Flash::success('How Did You Know updated successfully.');

        return redirect(route('howDidYouKnows.index'));
    }

    /**
     * Remove the specified HowDidYouKnow from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $howDidYouKnow = $this->howDidYouKnowRepository->find($id);

        if (empty($howDidYouKnow)) {
            Flash::error('How Did You Know not found');

            return redirect(route('howDidYouKnows.index'));
        }

        $this->howDidYouKnowRepository->delete($id);

        Flash::success('How Did You Know deleted successfully.');

        return redirect(route('howDidYouKnows.index'));
    }
}
