<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVideothequeRequest;
use App\Http\Requests\UpdateVideothequeRequest;
use App\Repositories\VideothequeRepository;
use App\Models\Videotheque;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class VideothequeController extends AppBaseController
{
    /** @var  VideothequeRepository */
    private $videothequeRepository;

    public function __construct(VideothequeRepository $videothequeRepo)
    {
        $this->videothequeRepository = $videothequeRepo;
    }

    /**
     * Display a listing of the Videotheque.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $videotheques = $this->videothequeRepository->all();
        $videotheques = Videotheque::orderBy('id', 'desc')->paginate(10);

        return view('videotheques.index')
            ->with('videotheques', $videotheques);
    }

    /**
     * Show the form for creating a new Videotheque.
     *
     * @return Response
     */
    public function create()
    {
        return view('videotheques.create');
    }

    /**
     * Store a newly created Videotheque in storage.
     *
     * @param CreateVideothequeRequest $request
     *
     * @return Response
     */
    public function store(CreateVideothequeRequest $request)
    {
        $input = $request->all();

        if($request->file('video')){

            $video_validator = \Validator::make($request->all(), [
                'video'  =>  'mimes:mp4,mov,ogg,flv,m3u8,ts,3gp,mov,avi,wmv,qt,ogx,oga,ogv,webm|max:20000'
            ]);
     
            if($video_validator->fails()){
                Flash::error('Veuillez uploader une vidéo.');
                return redirect()->back()->withInput();
            }
    
            $filename = time() . '.' . $request->file('video')->getClientOriginalExtension();
            $input['video'] = $request->file('video')->move('storage/', $filename);
        }

        if(!is_null($input['link_youtube'])){
            $input['link_youtube'] = 'https://www.youtube.com/embed/'.$input['link_youtube'];
        }

        $videotheque = $this->videothequeRepository->create($input);

        Flash::success('Videotheque saved successfully.');

        return redirect(route('videotheques.index'));
    }

    /**
     * Display the specified Videotheque.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $videotheque = $this->videothequeRepository->find($id);

        if (empty($videotheque)) {
            Flash::error('Videotheque not found');

            return redirect(route('videotheques.index'));
        }

        return view('videotheques.show')->with('videotheque', $videotheque);
    }

    /**
     * Show the form for editing the specified Videotheque.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $videotheque = $this->videothequeRepository->find($id);

        if (empty($videotheque)) {
            Flash::error('Videotheque not found');

            return redirect(route('videotheques.index'));
        }

        return view('videotheques.edit')->with('videotheque', $videotheque);
    }

    /**
     * Update the specified Videotheque in storage.
     *
     * @param int $id
     * @param UpdateVideothequeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVideothequeRequest $request)
    {
        $videotheque = $this->videothequeRepository->find($id);

        if (empty($videotheque)) {
            Flash::error('Videotheque not found');

            return redirect(route('videotheques.index'));
        }

        $input = $request->all();
        if($request->file('video')){
            $video_validator = \Validator::make($request->all(), [
                'video'  =>  'mimes:mp4,mov,ogg,flv,m3u8,ts,3gp,mov,avi,wmv,qt,ogx,oga,ogv,webm|max:20000'
            ]);
     
            if($video_validator->fails()){
                Flash::error('Veuillez uploader une vidéo.');
                return redirect()->back()->withInput();
            }
            $filename = time() . '.' . $request->file('video')->getClientOriginalExtension();
            $input['video'] = $request->file('video')->move('storage/', $filename);
        }

        if(!is_null($input['link_youtube'])){
            $input['link_youtube'] = 'https://www.youtube.com/embed/'.$input['link_youtube'];
        }

        $videotheque = $this->videothequeRepository->update($request->all(), $id);

        Flash::success('Videotheque updated successfully.');

        return redirect(route('videotheques.index'));
    }

    /**
     * Remove the specified Videotheque from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $videotheque = $this->videothequeRepository->find($id);

        if (empty($videotheque)) {
            Flash::error('Videotheque not found');

            return redirect(route('videotheques.index'));
        }

        $this->videothequeRepository->delete($id);

        Flash::success('Videotheque deleted successfully.');

        return redirect(route('videotheques.index'));
    }
}
