<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Models\InvoicePayment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;

class PaymentController extends Controller
{
    //
    public function fromInvoice(Request $request){
        //
        $input = $request->all();
        // validation
        if(!isset($input['redirect_to'])){

        }
        if(!isset($input['invoice_id'])){

        }
        $invoice = Invoice::find($input['invoice_id']);
        if(!$invoice){

        }
        $order = $invoice->order;
        $customer = $order->customer;
        // creating transaction
        $transactionId = self::generateTransId($customer->id, $invoice->id, $order->id);

            // Event payment tickets is online by default
            $transactionData = null;

            $transaction = new InvoicePayment();

                $transaction->invoice_id = $invoice->id;
                $transaction->payment_method = 'online';
                $transaction->payment_reference = $transactionId;
                $transaction->amount= $invoice->total;
                $transaction->currency_code = $invoice->currency_code;
                $transaction->creator_id = $customer->id;
                $transaction->creator = 'customers';
                $transaction->creator_name = $customer->name;
            $transaction->save();

        if($input['redirect_to'] == "shop"){
            $redirect_to = "/customer/shop/checkout_finish/".$transactionId;
        }
        if($input['redirect_to'] == "event"){
            $redirect_to = "/customer/event/checkout_finish/".$transactionId;
            // If its a free event
            // redirect to notify; then go to paymement checkout
            //
        }

        if($order->amount == 0){

            $notifyUrl = route('api.payments.notify');

            // notify with success
            $response = Http::post($notifyUrl, [
                'transaction_id' => $transactionId,
                'status' => 'SUCCESSFUL',
            ]);
            // dd($response);
            // return to the return page
            return redirect($redirect_to);
        }



        $data = [
            'apikey' => env('ADJEMINPAY_API_KEY'),
            'application_id' => env('ADJEMINPAY_APP_ID'),
            'notify_url' => env('APP_PAYMENTS_NOTIFY'),

            'redirect_to' =>$redirect_to,
            'paymentMethod'=> $transaction->payment_method,
            'transaction_id' => $transactionId,
            'designation' => 'Golden Health Buying Product',
            'amount' => (int)$transaction->amount,
            'currency' => $transaction->currency_code ?? "XOF",
            'status' => $transaction->status,
            'custom' => "Golden Health Buying Product",

        ];
        return view('payment.adjeminpay', compact('data'));
    }



    public static function generateTransId($customerId, $invoiceId, $orderId)
    {
        $timestamp = time();
        $parts = explode(' ', microtime());
        $id = ($timestamp + $parts[0] - strtotime('today 00:00')) * 10;
        $id = sprintf('%06d', $id) . mt_rand(100, 9999);
        $transId = $id."-".$customerId."-".$invoiceId."-".$orderId;

        $transactions = InvoicePayment::where(['payment_reference' => $transId])->get();
        if( count($transactions) == 0){

            return  $transId;
        }else{
            $autoIncrement = count($transactions) + 1;
            $transId = $transId.'-'.$autoIncrement;
            return  $transId;
        }
    }

}