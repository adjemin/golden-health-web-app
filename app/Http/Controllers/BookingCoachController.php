<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBookingCoachRequest;
use App\Http\Requests\UpdateBookingCoachRequest;
use App\Repositories\BookingCoachRepository;
use App\Models\BookingCoach;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use PDF;
use Response;
use App\Models\CustomerNotifications;
use App\Utils\CustomerNotificationUtils;

class BookingCoachController extends AppBaseController
{
    /** @var  BookingCoachRepository */
    private $bookingCoachRepository;

    public function __construct(BookingCoachRepository $bookingCoachRepo)
    {
        $this->bookingCoachRepository = $bookingCoachRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the BookingCoach.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $bookingCoaches = BookingCoach::orderBy('id', 'desc')->paginate(10);

        return view('booking_coaches.index')
            ->with('bookingCoaches', $bookingCoaches);
    }

    /**
     * Show the form for creating a new BookingCoach.
     *
     * @return Response
     */
    public function create()
    {
        return view('booking_coaches.create');
    }

    /**
     * Store a newly created BookingCoach in storage.
     *
     * @param CreateBookingCoachRequest $request
     *
     * @return Response
     */
    public function store(CreateBookingCoachRequest $request)
    {
        $input = $request->all();

        $bookingCoach = $this->bookingCoachRepository->create($input);

        Flash::success('Booking Coach saved successfully.');

        return redirect(route('bookingCoaches.index'));
    }

    /**
     * Display the specified BookingCoach.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bookingCoach = $this->bookingCoachRepository->find($id);

        if (empty($bookingCoach)) {
            Flash::error('Booking Coach not found');

            return redirect(route('bookingCoaches.index'));
        }

        return view('booking_coaches.show')->with('bookingCoach', $bookingCoach);
    }

    /**
     * Show the form for editing the specified BookingCoach.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bookingCoach = $this->bookingCoachRepository->find($id);

        if (empty($bookingCoach)) {
            Flash::error('Booking Coach not found');

            return redirect(route('bookingCoaches.index'));
        }

        return view('booking_coaches.edit')->with('bookingCoach', $bookingCoach);
    }

    /**
     * Update the specified BookingCoach in storage.
     *
     * @param int $id
     * @param UpdateBookingCoachRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBookingCoachRequest $request)
    {
        $bookingCoach = $this->bookingCoachRepository->find($id);

        if (empty($bookingCoach)) {
            Flash::error('Booking Coach not found');

            return redirect(route('bookingCoaches.index'));
        }

        $bookingCoach = $this->bookingCoachRepository->update($request->all(), $id);

        $customerNotification = new CustomerNotifications();
                                
        $customerNotification->title  = "Reservation";
        $customerNotification->title_en  = "Booking";
        $customerNotification->subtitle  = "Votre réservation viens de changer de status";
        $customerNotification->subtitle_en  = "Your booking has been changing by coach";
        $customerNotification->action  = "";
        $customerNotification->action_by  = "";
        $customerNotification->type_notification  = "waiting";
        $customerNotification->is_read  = false;
        $customerNotification->is_received  = false;
        $customerNotification->user_id  = $bookingCoach->customer_id;
        $customerNotification->save();

        CustomerNotificationUtils::notify($customerNotification);

        Flash::success('Booking Coach updated successfully.');

        return redirect(route('bookingCoaches.index'));
    }

    /**
     * Remove the specified BookingCoach from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bookingCoach = $this->bookingCoachRepository->find($id);

        if (empty($bookingCoach)) {
            Flash::error('Booking Coach not found');

            return redirect(route('bookingCoaches.index'));
        }

        $this->bookingCoachRepository->delete($id);

        Flash::success('Booking Coach deleted successfully.');

        return redirect(route('bookingCoaches.index'));
    }

    public function searchBookingCoach(Request $request){
        $bookingsFound = BookingCoach::leftJoin('customers', 'customers.id', '=', 'booking_coaches.customer_id')
            ->leftJoin('courses', 'courses.id', '=', 'booking_coaches.course_id')
            ->leftJoin('packs', 'packs.id', '=', 'booking_coaches.pack_id')
            ->select('booking_coaches.*')
            ->where('booking_ref', 'like', '%' . $request->searchBookingCoach . '%')
            ->orWhere('address_name', 'like', '%' . $request->searchBookingCoach . '%')
            ->orWhere('customers.name', 'like', '%' . $request->searchBookingCoach . '%')
            ->orWhere('packs.name', 'like', '%' . $request->searchBookingCoach . '%')
            ->orWhere('courses.course_type', 'like', '%' . $request->searchBookingCoach . '%')
            ->orWhere('course_lieu_note', 'like', '%' . $request->searchBookingCoach . '%')
            ->orWhere('amount', 'like', '%' . $request->searchBookingCoach . '%')
            ->orWhere('nb_personne', 'like', '%' . $request->searchBookingCoach . '%')
            ->orWhere('quantity', 'like', '%' . $request->searchBookingCoach . '%')
            ->orWhere('status', 'like', '%' . $request->searchBookingCoach . '%')
            ->get();
        
        return json_encode(view('booking_coaches.table', ['bookingCoaches' => $bookingsFound])->render());
    }

    public function printAll(){
        $bookingCoaches = BookingCoach::all();
        // $pdf = PDF::loadView('booking_coaches.table_pdf', compact('bookingCoaches'));
        return view('booking_coaches.table_pdf', compact('bookingCoaches'));
        // return $pdf->stream();
    }
}
