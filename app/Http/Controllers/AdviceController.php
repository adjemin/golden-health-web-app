<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAdviceRequest;
use App\Http\Requests\UpdateAdviceRequest;
use App\Repositories\AdviceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class AdviceController extends AppBaseController
{
    /** @var  AdviceRepository */
    private $adviceRepository;

    public function __construct(AdviceRepository $adviceRepo)
    {
        $this->adviceRepository = $adviceRepo;
    }

    /**
     * Display a listing of the Advice.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $advice = $this->adviceRepository->all();

        return view('advice.index')
            ->with('advice', $advice);
    }

    /**
     * Show the form for creating a new Advice.
     *
     * @return Response
     */
    public function create()
    {
        return view('advice.create');
    }

    /**
     * Store a newly created Advice in storage.
     *
     * @param CreateAdviceRequest $request
     *
     * @return Response
     */
    public function store(CreateAdviceRequest $request)
    {
        $input = $request->all();

        $advice = $this->adviceRepository->create($input);

        Flash::success('Advice saved successfully.');

        return redirect(route('advice.index'));
    }

    /**
     * Display the specified Advice.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $advice = $this->adviceRepository->find($id);

        if (empty($advice)) {
            Flash::error('Advice not found');

            return redirect(route('advice.index'));
        }

        return view('advice.show')->with('advice', $advice);
    }

    /**
     * Show the form for editing the specified Advice.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $advice = $this->adviceRepository->find($id);

        if (empty($advice)) {
            Flash::error('Advice not found');

            return redirect(route('advice.index'));
        }

        return view('advice.edit')->with('advice', $advice);
    }

    /**
     * Update the specified Advice in storage.
     *
     * @param int $id
     * @param UpdateAdviceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAdviceRequest $request)
    {
        $advice = $this->adviceRepository->find($id);

        if (empty($advice)) {
            Flash::error('Advice not found');

            return redirect(route('advice.index'));
        }

        $advice = $this->adviceRepository->update($request->all(), $id);

        Flash::success('Advice updated successfully.');

        return redirect(route('advice.index'));
    }

    /**
     * Remove the specified Advice from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $advice = $this->adviceRepository->find($id);

        if (empty($advice)) {
            Flash::error('Advice not found');

            return redirect(route('advice.index'));
        }

        $this->adviceRepository->delete($id);

        Flash::success('Advice deleted successfully.');

        return redirect(route('advice.index'));
    }
}
