<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Repositories\OrderRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\CustomerNotifications;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\InvoicePayment;
use App\OrderHistory;
use App\Models\OrderItem;
use App\Utils\CustomerNotificationUtils;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Mail\OrderRejectMail;
use App\Mail\OrderConfirmMail;
use App\Mail\OrderDeliveredMail;
use Flash;
use Response;
use PDF;

class OrderController extends AppBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepo)
    {
        $this->orderRepository = $orderRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Order.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orders = Order::where('service_slug', 'shop')->orderBy('id', 'desc')->paginate(5);

        return view('orders.index')
            ->with('orders', $orders);
    }

    /**
     * Show the form for creating a new Order.
     *
     * @return Response
     */
    public function create()
    {
        return view('orders.create');
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param CreateOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderRequest $request)
    {
        $input = $request->all();

        $order = $this->orderRepository->create($input);

        Flash::success('Order saved successfully.');

        return redirect(route('orders.index'));
    }

    /**
     * Display the specified Order.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        return view('orders.show')->with('order', $order);
    }

    /**
     * Show the form for editing the specified Order.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        return view('orders.edit')->with('order', $order);
    }

    /**
     * Update the specified Order in storage.
     *
     * @param int $id
     * @param UpdateOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderRequest $request)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        $order = $this->orderRepository->update($request->all(), $id);

        Flash::success('Order updated successfully.');

        return redirect(route('orders.index'));
    }

    /**
     * Remove the specified Order from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        $this->orderRepository->delete($id);

        Flash::success('Order deleted successfully.');

        return redirect(route('orders.index'));
    }

    public function espece($id){
        $order = $this->orderRepository->find($id);
        $order->payment_method_slug = "cash";
        $order->save();

        $this->isPaid($id);
    }

    public function isPaid($id){
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        $invoice = Invoice::where('order_id', $order->id)->first();
        $transaction = InvoicePayment::where('invoice_id', $invoice->id)->first();

        $order->status = 'confirmed';
        $order->save();

        $user = \App\Models\Customer::find($transaction->creator_id);
        $orderHistory = $this->validateTransaction($transaction->payment_reference);
        if(!is_null($user->email)){
            \Mail::to($user->email)->send(new OrderConfirmMail($order));
        }

        $customerNotification = new CustomerNotifications();
        $customerNotification->title  = $user->name.", votre commande est traitée.";
        $customerNotification->title_en  = $user->name.", your order is managed.";
        $customerNotification->subtitle  = "Commande #".$order->id." prise en compte par notre équipe.";
        $customerNotification->subtitle_en  = "Order #".$order->id." have been managed by owner team.";
        $customerNotification->action  = "";
        $customerNotification->action_by  = "";
        if(!is_null($orderHistory)){
            $customerNotification->meta_data  = $orderHistory->toJson() ?? "";
        }
        $customerNotification->type_notification  = $request->status;
        $customerNotification->is_read  = false;
        $customerNotification->is_received  = false;
        if(!is_null($order)){
            $customerNotification->data  = $order->toJson() ?? "";
        }
        $customerNotification->user_id  = $user->id;
        $customerNotification->data_id  = $order->id;
        $customerNotification->save();

        CustomerNotificationUtils::notify($customerNotification);


        Flash::success('Order updated successfully.');

        return redirect(route('orders.index'));
    }

    public function changeStatus($id,Request $request){
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        $invoice = Invoice::where('order_id', $order->id)->first();
        $transaction = InvoicePayment::where('invoice_id', $invoice->id)->first();

        $order->status = $request->status;
        if($request->status == 'delivered'){
            $order->is_delivery = true;
        }
        $order->save();

        $orderHistory = null;

        $user = Customer::find($transaction->creator_id);

        if($request->status == 'confirmed'){
            $orderHistory = $this->validateTransaction($transaction->payment_reference);
            if(!is_null($user->email)){
                \Mail::to($user->email)->send(new OrderConfirmMail($order));
            }
        }else if($request->status == 'delivered'){
            $orderHistory = $this->deliveredTransaction($transaction->payment_reference);
            if(!is_null($user->email)){
                \Mail::to($user->email)->send(new OrderDeliveredMail($order));
            }
        }else{
            $orderHistory = $this->cancelTransaction($transaction->payment_reference);
            if(!is_null($user->email)){
                \Mail::to($user->email)->send(new OrderRejectMail($order));
            }
        }

        $customerNotification = new CustomerNotifications();
        $customerNotification->title  = $user->name.", votre commande est traitée.";
        $customerNotification->title_en  = $user->name.", your order is managed.";
        $customerNotification->subtitle  = "Commande #".$order->id." a été par notre équipe.";
        $customerNotification->subtitle_en  = "Order #".$order->id." have been managed by owner team.";
        $customerNotification->action  = "";
        $customerNotification->action_by  = "";
        if(!is_null($orderHistory)){
            $customerNotification->meta_data  = $orderHistory->toJson() ?? "";
        }
        $customerNotification->type_notification  = $request->status;
        $customerNotification->is_read  = false;
        $customerNotification->is_received  = false;
        if(!is_null($order)){
            $customerNotification->data  = $order->toJson() ?? "";
        }
        $customerNotification->user_id  = $user->id;
        $customerNotification->data_id  = $order->id;
        $customerNotification->save();

        CustomerNotificationUtils::notify($customerNotification);


        Flash::success('Order updated successfully.');

        return redirect(route('orders.index'));
    }

    public function deliveredTransaction($transactionId){

        $transaction = InvoicePayment::where(['payment_reference' => $transactionId])->first();

        $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();

        $order = Order::where(['id' => $invoice->order_id])->first();
        $order->status = 'delivered';
        $order->save();

        $orderHistory = new OrderHistory();
        $orderHistory->order_id = $order->id;
        $orderHistory->status = "delivered";
        $orderHistory->creator_id = $transaction->creator_id;
        $orderHistory->creator_name = $transaction->creator_name;
        $orderHistory->creator = "customers";
        $orderHistory->save();

        return $orderHistory;
    }

    public function cancelTransaction($transactionId){

        $transaction = InvoicePayment::where(['payment_reference' => $transactionId])->first();

        $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();
        $invoice->status = 'canceled';
        $invoice->is_paid_by_customer = false;
        $invoice->save();

        $order = Order::where(['id' => $invoice->order_id])->first();
        $order->status = 'canceled';
        $order->save();

        $orderHistory = new OrderHistory();
        $orderHistory->order_id = $order->id;
        $orderHistory->status = "canceled";
        $orderHistory->creator_id = $customer->id;
        $orderHistory->creator_name = $customer->name;
        $orderHistory->creator = "customers";
        $orderHistory->save();

        return $orderHistory;
    }

    public function validateTransaction($transactionId){
        $transaction = InvoicePayment::where(['payment_reference' => $transactionId])->first();

        $orderHistory = null;

        if($transaction->status == 'SUCCESSFUL'){
            $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();
            $invoice->status = 'paid';
            $invoice->is_paid = true;
            $invoice->is_paid_by_customer = true;
            $invoice->save();

            $order = Order::where(['id' => $invoice->order_id])->first();

            $customer = \App\Models\Customer::where('id' ,  $order->customer_id)->first();

            $orderHistory = new OrderHistory();
            $orderHistory->order_id = $order->id;
            $orderHistory->status = Order::CUSTOMER_PAID;
            $orderHistory->order_id = $order->id;
            $orderHistory->creator = 'customers';
            $orderHistory->creator_id = $customer->id;
            $orderHistory->creator_name = $customer->name;
            $orderHistory->save();

            // Managing services
            $transaction = InvoicePayment::where(['payment_reference' => $transactionId])->first();

            // ! service shop
            if($invoice->service == 'shop'){
                $orderHistory = new OrderHistory();
                $orderHistory->order_id = $order->id;
                $orderHistory->status = Order::WAITING_CUSTOMER_DELIVERY;
                $orderHistory->order_id = $order->id;
                $orderHistory->creator = 'customers';
                $orderHistory->creator_id = $customer->id;
                $orderHistory->creator_name = $customer->name;
                $orderHistory->save();
            }

        }

        return $orderHistory;
    }

    public function invoiceFacture($id)
    {
        $invoice = \App\Models\Invoice::find($id);

        if(is_null($invoice)){
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        return view('customer_frontOffice.invoice_pdf', compact('invoice'));
    }

    public function searchOrder(Request $request){
        $ordersFound = Order::leftJoin('customers', 'customers.id', '=', 'orders.customer_id')
            ->select('orders.*')
            ->orWhere('customers.name', 'like', '%' . $request->searchOrder . '%')
            ->orWhere('customers.email', 'like', '%' . $request->searchOrder . '%')
            ->orWhere('customers.location_address', 'like', '%' . $request->searchOrder . '%')
            ->orWhere('orders.status', 'like', '%' . $request->searchOrder . '%')
            ->orWhere('orders.amount', 'like', '%' . $request->searchOrder . '%')
            ->orWhere('orders.id', 'like', '%' . $request->searchOrder . '%')
            ->orWhere('orders.delivery_date', 'like', '%' . $request->searchOrder . '%')
            ->get();
        
        return json_encode(view('orders.table', ['orders' => $ordersFound])->render());
    }

    public function printAll(){
        $orders = Order::all()->sortByDesc('id');
        return view('orders.table_pdf', compact('orders'));
        // $pdf = PDF::loadView('orders.table_pdf', compact('orders'));
        // return $pdf->stream();
    }
}