<?php

namespace App\Http\Controllers;

use App\User;
use App\CartItem;
use Carbon\Carbon;
use App\CouponOrder;
use App\EventTicket;
use App\Models\Event;
use App\Models\Order;
// use Darryldecode\Cart\Cart;
use App\OrderHistory;
use App\ShoppingCart;
use App\Models\Coupon;
use App\Models\Invoice;
use App\Models\Product;
use Carbon\Traits\Date;
use Barryvdh\DomPDF\PDF;
use App\Models\OrderItem;
use Darryldecode\Cart\Cart;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Mail\AdminNotifyMail;
use App\Models\InvoicePayment;
use Illuminate\Support\Facades\DB;
use App\Mail\EventTicketNotifyMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\CustomerNotifications;
use App\Utils\CustomerNotificationUtils;


class AjaxController extends Controller
{
    //
    // ***** EVENTS

    public function eventParticipate(Request $request)
    {
        if(!Auth::guard('customer')->check()){
            return response()->json([
                'code' => 403,
                'status' => "MISSING_AUTH",
                'message' => "Unauthorized",
                'data' => null,
            ]);
        }

        $customer = Auth::guard('customer')->user();

        $input = $request->all();

        if($input == null){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => null,
            ]);
        }
        if(isset($input['event_id'])){
            $event = Event::find($input['event_id']);
        }

        if(!$event){
            return response()->json([
                'code' => -11,
                'status' => "EVENT_NOT_FOUND",
                'message' => "",
                'data' => null,
            ]);
        }

        $isParticipating = DB::table('participates_in_event')->where([
            [
                'event_id', $event->id
            ],
            [
                'customer_id', $customer->id
            ],
        ])->first();

        if($isParticipating && $isParticipating->is_paid == 1){
            return response()->json([
                'code' => 11,
                'status' => "ALREADY_EXISTS",
                'message' => "Is already participating",
                'data' => $isParticipating,
            ]);
        }

        if (!isset($input['invoice_id'])) {
            return response()->json([
                'code' => -11,
                'status' => "MISSING_INVOICE",
                'message' => "",
                'data' => null,
            ]);
        }

        $invoice = Invoice::find($input['invoice_id']);
        $order = $invoice->order;
        $customer = $order->customer;

        $order->status = "paid";
        $order->save();
        $invoice->status = "paid";
        $invoice->is_paid= true;
        $invoice->is_paid_by_customer = true;
        $invoice->save();
        foreach($order->tickets() as $ticket){
            $ticket->status = 'valid';
            $ticket->is_active = true;
            $ticket->is_paid = true;
            $ticket->save();
        }

        // Creating an entry in OrderHistories
        $orderHistory = new OrderHistory();
            $orderHistory->order_id = $order->id;

            $orderHistory->status = Order::CUSTOMER_PAID;

            $orderHistory->order_id = $order->id;
            $orderHistory->creator = 'customers';
            $orderHistory->creator_id = $customer->id;
            $orderHistory->creator_name = $customer->name;
        $orderHistory->save();

        // Creating transation (invoicePayment)
        $transactionId = self::generateTransId($customer->id, $invoice->id, $order->id);

        // Event payment tickets is online by default
        $transactionData = null;

        $transaction = new InvoicePayment();
            $transaction->invoice_id = $invoice->id;
            $transaction->payment_method = 'online';
            $transaction->payment_reference = $transactionId;
            $transaction->amount= $invoice->total;
            $transaction->currency_code = $invoice->currency_code;
            $transaction->creator_id = $customer->id;
            $transaction->creator = 'customers';
            $transaction->creator_name = $customer->name;

            $transaction->status = "SUCCESSFUL";
            $transaction->is_completed = true;

        $transaction->save();

        DB::table('participates_in_event')->insert([
            'event_id' => $event->id,
            'customer_id' => $customer->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);


        // Notifying via Mail
        $data = [
            'subject' => "Ticket Acheté",
            'title' => "Réservation confirmée",
            'subtitle' => "Réservation de ".$input['seats_count']." place(s) à l'event $event->title confirmée",
            // TODO make a ticket render body in html mail format
        ];
        $adminData = [
            'subject' => "Nouvel achat Ticket",
            'title' => $input['seats_count']." ticket(s) acheté(s) à l'instant",
            'subtitle' => "Ticket(s) réservé(s) pour l'event ".$event->title,
            'url' => env('APP_URL').'event_tickets/'
        ];

        Mail::to($customer->email)->send(new EventTicketNotifyMail($data));

        foreach (User::all() as $user) {
            Mail::to($user->email)->send(new AdminNotifyMail($adminData));

            $customerNotification = new CustomerNotifications();
                
            $customerNotification->title  = "Nouvel achat Ticket";
            $customerNotification->title_en  = "New bill buy by customer";
            $customerNotification->subtitle  = $input['seats_count']." ticket(s) acheté(s) à l'instant";
            $customerNotification->subtitle_en  = $input['seats_count']." bill(s) buy now";
            $customerNotification->action  = "";
            $customerNotification->action_by  = "";
            $customerNotification->type_notification  = "confirmed";
            $customerNotification->is_read  = false;
            $customerNotification->is_received  = false;
            $customerNotification->user_id  = $customer->id;
            $customerNotification->save();
        }

        // Sending push notifications

        $user = $customer;
            $customerNotification = new CustomerNotifications();
            $customerNotification->title  = $input['seats_count']." ticket(s) acheté(s)";

            $customerNotification->title_en  = $input['seats_count']." ticket(s) bought";

            $customerNotification->subtitle  = "Vous avez acheté ".$input['seats_count']." ticket(s) pour l'event ".$event->title;
            $customerNotification->subtitle_en  = "You bought ".$input['seats_count']." ticket(s) for event ".$event->title;
            $customerNotification->action  = "";
            $customerNotification->action_by  = "";
            if(!is_null($orderHistory)){
                $customerNotification->meta_data  = $orderHistory->toJson() ?? "";
            }
            $customerNotification->type_notification  = "confirmed";
            $customerNotification->is_read  = false;
            $customerNotification->is_received  = false;
            if(!is_null($order)){
                $customerNotification->data  = $order->toJson() ?? "";
            }
            $customerNotification->user_id  = $user->id;
            $customerNotification->data_id  = $order->id;
            $customerNotification->save();

        CustomerNotificationUtils::notify($customerNotification);


        //

        return response()->json([
            'code' => 11,
            'status' => "OK",
            'message' => "Success",
            'transaction' => $transaction,

        ]);
    }
    public function eventParticipate___(Request $request)
    {
        if(!Auth::guard('customer')->check()){
            return response()->json([
                'code' => 403,
                'status' => "MISSING_AUTH",
                'message' => "Unauthorized",
                'data' => null,
            ]);
        }

        $customer = Auth::guard('customer')->user();

        $input = $request->all();

        if($input == null){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => null,
            ]);
        }

        $event = Event::find($input['event_id']);

        if(!$event){
            return response()->json([
                'code' => -11,
                'status' => "NOT_FOUND",
                'message' => "",
                'data' => null,
            ]);
        }

        $isParticipating = DB::table('participates_in_event')->where([
            [
                'event_id', $event->id
            ],
            [
                'customer_id', $customer->id
            ],
        ])->first();

        if($isParticipating && $isParticipating->is_paid == 1){
            return response()->json([
                'code' => 11,
                'status' => "ALREADY_EXISTS",
                'message' => "Is already participating",
                'data' => $isParticipating,
            ]);
        }

        if ($event->isFree()) {
            $event->entry_fee = 0;
            $event->save();
        }


        // Creating an order
        $order = new Order();
            $order->customer_id = $customer->id;
            $order->status = "waiting";
            $order->is_waiting = true;
            if($event->entry_fee == 0){
                $order->status = "paid";
                $order->is_waiting = false;
            }
            $order->is_delivery = false;
            $order->service_slug = 'event';

            $subtotal = $event->entry_fee * $input['seats_count'];
            $tax = $subtotal * Invoice::TAXES;
            $total = $subtotal + $tax;
            $order->amount = $total;
        $order->save();

        // Creating an entry in OrderHistories
        $orderHistory = new OrderHistory();
            $orderHistory->order_id = $order->id;
            $orderHistory->status = Order::WAITING;
            if($event->entry_fee == 0){
                $orderHistory->status = Order::CUSTOMER_PAID;
            }
            $orderHistory->order_id = $order->id;
            $orderHistory->creator = 'customers';
            $orderHistory->creator_id = $customer->id;
            $orderHistory->creator_name = $customer->name;
        $orderHistory->save();

        // Creating each order Items and tickets
        for($x = 0; $x < $input['seats_count']; $x++){

            $prefix = "GDH::";
            $ticket = new EventTicket();
            $code = $prefix.strtoupper(Str::random(10));
            $code_exists = EventTicket::where('code', $code)->first();
            $qty = 1;

            if($code_exists){
                $code = $prefix.strtoupper(Str::random(10));
            }

            $ticket->code = $code;
            $ticket->seats_count = $qty;
            // $ticket->seats_count = $input['seats_count'];
            // $ticket->amount= $event->entry_fee * $input['seats_count'];
            $ticket->amount = $event->entry_fee;
            $ticket->event_id = $event->id;
            if ($event->entry_fee == 0) {
                $ticket->is_paid = true;
                $ticket->status = "paid";
                $ticket->is_active = true;
            }
            $ticket->customer_id= $customer->id;
            $ticket->save();

            $orderItem = new OrderItem();
            $orderItem->order_id = $order->id;
            $orderItem->service_slug = 'event';
            $orderItem->meta_data_id = $ticket->id;
            $orderItem->meta_data = $ticket->toJson();
            $orderItem->quantity = $qty;
            $orderItem->unit_price = $event->entry_fee;
            $orderItem->total_amount = $qty * $event->entry_fee;
            // $orderItem->days = ;
            $orderItem->save();
        }
        // Creating invoice
        $invoice = new Invoice();
            $invoice->order_id = $order->id;
            $invoice->customer_id = $customer->id;
            $invoice->reference = Invoice::generateID('EVENT', $order->id, $customer->id);
            $invoice->service = 'event';
            $invoice->link = '#';
            $invoice->subtotal = $subtotal;
            $invoice->tax = $tax;
            $invoice->fees_delivery = 0;
            $invoice->total = $total;
            $invoice->status = 'unpaid';
            if($event->entry_fee == 0){
                $invoice->status = 'paid';
            }
            $invoice->is_paid_by_customer = true;
            $invoice->is_paid_by_delivery_service = false;
        $invoice->save();

        // Creating transation (invoicePayment)
        $transactionId = self::generateTransId($customer->id, $invoice->id, $order->id);

        // Event payment tickets is online by default
        $transactionData = null;

        $transaction = new InvoicePayment();

            $transaction->invoice_id = $invoice->id;
            $transaction->payment_method = 'online';
            $transaction->payment_reference = $transactionId;
            $transaction->amount= $invoice->total;
            $transaction->currency_code = $invoice->currency_code;
            $transaction->creator_id = $customer->id;
            $transaction->creator = 'customers';
            $transaction->creator_name = $customer->name;
            if($event->entry_fee == 0){
                $transaction->status = "SUCCESSFUL";
                $transaction->is_completed = true;
            }
        $transaction->save();

        $transactionData = [
            'paymentMethod'=> $transaction->payment_method,
            'id' => $transactionId,
            'designation' => 'Golden Health Event Ticket',
            'amount' => (int)$transaction->amount,
            'currency' => $transaction->currency_code ?? "XOF",
            'status' => $transaction->status,
            'customField'
        ];


        DB::table('participates_in_event')->insert([
            'event_id' => $event->id,
            'customer_id' => $customer->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);


        // Notifying via Mail
        $data = [
            'subject' => "Ticket Acheté",
            'title' => "Réservation confirmée",
            'subtitle' => "Réservation de ".$input['seats_count']." place(s) à l'event $event->title confirmée",
            // TODO make a ticket render body in html mail format
        ];
        $adminData = [
            'subject' => "Nouvel achat Ticket",
            'title' => $input['seats_count']." ticket(s) acheté(s) à l'instant",
            'subtitle' => "Ticket(s) réservé(s) pour l'event ".$event->title,
            'url' => env('APP_URL').'event_tickets/'
        ];

        Mail::to($customer->email)->send(new EventTicketNotifyMail($data));

        foreach (User::all() as $user) {
            Mail::to($user->email)->send(new AdminNotifyMail($adminData));

            $customerNotification = new CustomerNotifications();
                
            $customerNotification->title  = "Nouvel achat Ticket";
            $customerNotification->title_en  = "New bill buy by customer";
            $customerNotification->subtitle  = $input['seats_count']." ticket(s) acheté(s) à l'instant";
            $customerNotification->subtitle_en  = $input['seats_count']." bill(s) buy now";
            $customerNotification->action  = "";
            $customerNotification->action_by  = "";
            $customerNotification->type_notification  = "confirmed";
            $customerNotification->is_read  = false;
            $customerNotification->is_received  = false;
            $customerNotification->user_id  = $customer->id;
            $customerNotification->save();
        }

        // Sending push notifications
        if($ticket->entry_fee == 0){

            $user = $customer;
            $customerNotification = new CustomerNotifications();
            $customerNotification->title  = $input['seats_count']." ticket(s) acheté(s)";

            $customerNotification->title_en  = $input['seats_count']." ticket(s) bought";

            $customerNotification->subtitle  = "Vous avez acheté ".$input['seats_count']." ticket(s) pour l'event ".$event->title;
            $customerNotification->subtitle_en  = "You bought ".$input['seats_count']." ticket(s) for event ".$event->title;
            $customerNotification->action  = "";
            $customerNotification->action_by  = "";
            if(!is_null($orderHistory)){
                $customerNotification->meta_data  = $orderHistory->toJson() ?? "";
            }
            $customerNotification->type_notification  = "confirmed";
            $customerNotification->is_read  = false;
            $customerNotification->is_received  = false;
            if(!is_null($order)){
                $customerNotification->data  = $order->toJson() ?? "";
            }
            $customerNotification->user_id  = $user->id;
            $customerNotification->data_id  = $order->id;
            $customerNotification->save();

            CustomerNotificationUtils::notify($customerNotification);
        }else{
            $user = $customer;
            $customerNotification = new CustomerNotifications();
            $customerNotification->title  = $input['seats_count']." ticket(s) acheté(s)";

            $customerNotification->title_en  = $input['seats_count']." ticket(s) bought";

            $customerNotification->subtitle  = "Vous avez acheté ".$input['seats_count']." ticket(s) pour l'event ".$event->title;
            $customerNotification->subtitle_en  = "You bought ".$input['seats_count']." ticket(s) for event ".$event->title;
            $customerNotification->action  = "";
            $customerNotification->action_by  = "";
            $customerNotification->type_notification  = "confirmed";
            $customerNotification->is_read  = false;
            $customerNotification->is_received  = false;
            $customerNotification->user_id  = $user->id;
            $customerNotification->save();

            CustomerNotificationUtils::notify($customerNotification);
        }

        //

        return response()->json([
            'code' => 11,
            'status' => "OK",
            'message' => "Success",
            'data' => [
                // 'ticket' => $ticket,
                'transaction' => $transactionData,
            ],
        ]);
    }
    // public function eventParticipate_(Request $request)
    // {
    //     if(!Auth::guard('customer')->check()){
    //         return response()->json([
    //             'code' => 403,
    //             'status' => "MISSING_AUTH",
    //             'message' => "Unauthorized",
    //             'data' => null,
    //         ]);
    //     }

    //     $customer = Auth::guard('customer')->user();

    //     $input = $request->all();

    //     if($input == null){
    //         return response()->json([
    //             'code' => -11,
    //             'status' => "MISSING_DATA",
    //             'message' => "",
    //             'data' => null,
    //         ]);
    //     }

    //     $event = Event::find($input['event_id']);

    //     if(!$event){
    //         return response()->json([
    //             'code' => -11,
    //             'status' => "NOT_FOUND",
    //             'message' => "",
    //             'data' => null,
    //         ]);
    //     }

    //     $isParticipating = DB::table('participates_in_event')->where([
    //         [
    //             'event_id', $event->id
    //         ],
    //         [
    //             'customer_id', $customer->id
    //         ],
    //     ])->first();

    //     if($isParticipating && $isParticipating->is_paid == 1){
    //         return response()->json([
    //             'code' => 11,
    //             'status' => "ALREADY_EXISTS",
    //             'message' => "Is already participating",
    //             'data' => $isParticipating,
    //         ]);
    //     }

    //     if($event->isFree()){
    //         DB::table('participates_in_event')->insert([
    //             'event_id' => $event->id,
    //             'is_paid' => true,
    //             'customer_id' => $customer->id,
    //             'created_at' => Carbon::now(),
    //             'updated_at' => Carbon::now(),
    //         ]);
    //         //
    //         // Creating each order Items and tickets
    //         for($x = 0; $x < $input['seats_count']; $x++){

    //             $prefix = "GDH::";
    //             $ticket = new EventTicket();
    //             $code = $prefix.strtoupper(Str::random(10));
    //             $code_exists = EventTicket::where('code', $code)->first();
    //             $qty = 1;

    //             if($code_exists){
    //                 $code = $prefix.strtoupper(Str::random(10));
    //             }

    //             $ticket->code = $code;
    //             $ticket->status = "free_ticket";
    //             $ticket->seats_count = $qty;
    //             $ticket->is_active = true;
    //             $ticket->is_paid= true;

    //             // $ticket->seats_count = $input['seats_count'];
    //             // $ticket->amount= $event->entry_fee * $input['seats_count'];
    //             $ticket->amount= $event->entry_fee;
    //             $ticket->event_id= $event->id;
    //             $ticket->customer_id= $customer->id;
    //             $ticket->save();
    //         }

    //         // Notifying via Mail
    //             $data = [
    //                 'subject' => "Ticket Acheté",
    //                 'title' => "Réservation confirmée",
    //                 'subtitle' => "Réservation de ".$input['seats_count']." place(s) à l'event $event->title confirmée",
    //                 // TODO make a ticket render body in html mail format
    //             ];
    //             $adminData = [
    //                 'subject' => "Nouvel achat Ticket",
    //                 'title' => "Un ticket vient d'être acheté",
    //                 'subtitle' => "Un ticket a été réservé pour l'event ".$ticket->event->title,
    //                 'url' => env('APP_URL').'event_tickets/'.$ticket->id
    //             ];

    //             Mail::to($customer->email)->send(new EventTicketNotifyMail($data));

    //             foreach (User::all() as $user) {
    //                 Mail::to($user->email)->send(new AdminNotifyMail($adminData));
    //             }

    //         return response()->json([
    //             'code' => 11,
    //             'status' => "FREE_PASS",
    //             'message' => "Success",
    //             'data' => [
    //                 // 'ticket' => $ticket,
    //                 'transaction' => null,
    //             ],
    //         ]);
    //     }

    //     // Creating an order
    //     $order = new Order();
    //         $order->customer_id = $customer->id;
    //         $order->status = "waiting";
    //         $order->is_waiting = true;
    //         $order->is_delivery = false;
    //         $order->service_slug = 'event';

    //         $subtotal = $event->entry_fee * $input['seats_count'];
    //         $tax = $subtotal * Invoice::TAXES;
    //         $total = $subtotal + $tax;
    //         $order->amount = $total;
    //     $order->save();

    //     // Creating an entry in OrderHistories
    //     $orderHistory = new OrderHistory();
    //         $orderHistory->order_id = $order->id;
    //         $orderHistory->status = Order::WAITING;
    //         $orderHistory->order_id = $order->id;
    //         $orderHistory->creator = 'customers';
    //         $orderHistory->creator_id = $customer->id;
    //         $orderHistory->creator_name = $customer->name;
    //     $orderHistory->save();

    //     // Creating each order Items and tickets
    //     for($x = 0; $x < $input['seats_count']; $x++){

    //         $prefix = "GDH::";
    //         $ticket = new EventTicket();
    //         $code = $prefix.strtoupper(Str::random(10));
    //         $code_exists = EventTicket::where('code', $code)->first();
    //         $qty = 1;

    //         if($code_exists){
    //             $code = $prefix.strtoupper(Str::random(10));
    //         }

    //         $ticket->code = $code;
    //         $ticket->seats_count = $qty;
    //         // $ticket->seats_count = $input['seats_count'];
    //         // $ticket->amount= $event->entry_fee * $input['seats_count'];
    //         $ticket->amount= $event->entry_fee;
    //         $ticket->event_id= $event->id;
    //         $ticket->customer_id= $customer->id;
    //         $ticket->save();

    //         $orderItem = new OrderItem();
    //         $orderItem->order_id = $order->id;
    //         $orderItem->service_slug = 'event';
    //         $orderItem->meta_data_id = $ticket->id;
    //         $orderItem->meta_data = $ticket->toJson();
    //         $orderItem->quantity = $qty;
    //         $orderItem->unit_price = $event->entry_fee;
    //         $orderItem->total_amount = $qty * $event->entry_fee;
    //         // $orderItem->days = ;
    //         $orderItem->save();
    //     }
    //     // Creating invoice
    //     $invoice = new Invoice();
    //         $invoice->order_id = $order->id;
    //         $invoice->customer_id = $customer->id;
    //         $invoice->reference = Invoice::generateID('EVENT', $order->id, $customer->id);
    //         $invoice->service = 'event';
    //         $invoice->link = '#';
    //         $invoice->subtotal = $subtotal;
    //         $invoice->tax = $tax;
    //         $invoice->fees_delivery = 0;
    //         $invoice->total = $total;
    //         $invoice->status = 'unpaid';
    //         $invoice->is_paid_by_customer = true;
    //         $invoice->is_paid_by_delivery_service = false;
    //     $invoice->save();

    //     // Creating transation (invoicePayment)
    //     $transactionId = self::generateTransId($customer->id, $invoice->id, $order->id);

    //     // Event payment tickets is online by default
    //     $transactionData = null;

    //     $transaction = new InvoicePayment();

    //         $transaction->invoice_id = $invoice->id;
    //         $transaction->payment_method = 'online';
    //         $transaction->payment_reference = $transactionId;
    //         $transaction->amount= $invoice->total;
    //         $transaction->currency_code = $invoice->currency_code;
    //         $transaction->creator_id = $customer->id;
    //         $transaction->creator = 'customers';
    //         $transaction->creator_name = $customer->name;
    //     $transaction->save();

    //     $transactionData = [
    //         'paymentMethod'=> $transaction->payment_method,
    //         'id' => $transactionId,
    //         'designation' => 'Golden Health Event Ticket',
    //         'amount' => (int)$transaction->amount,
    //         'currency' => $transaction->currency_code ?? "XOF",
    //         'status' => $transaction->status,
    //         'customField'
    //     ];


    //     DB::table('participates_in_event')->insert([
    //         'event_id' => $event->id,
    //         'customer_id' => $customer->id,
    //         'created_at' => Carbon::now(),
    //         'updated_at' => Carbon::now(),
    //     ]);

    //     return response()->json([
    //         'code' => 11,
    //         'status' => "OK",
    //         'message' => "Success",
    //         'data' => [
    //             // 'ticket' => $ticket,
    //             'transaction' => $transactionData,
    //         ],
    //     ]);
    // }
    // Prepares checkout
    public function eventPrepareCheckout(Request $request)
    {
        if(!Auth::guard('customer')->check()){
            return response()->json([
                'code' => 403,
                'status' => "MISSING_AUTH",
                'message' => "Unauthorized",
                'data' => null,
            ]);
        }

        $customer = Auth::guard('customer')->user();

        $input = $request->all();

        if($input == null){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => null,
            ]);
        }

        $event = Event::find($input['event_id']);

        if(!$event){
            return response()->json([
                'code' => -11,
                'status' => "NOT_FOUND",
                'message' => "",
                'data' => null,
            ]);
        }

        $isParticipating = DB::table('participates_in_event')->where([
            [
                'event_id', $event->id
            ],
            [
                'customer_id', $customer->id
            ],
        ])->first();

        if($isParticipating && $isParticipating->is_paid == 1){
            return response()->json([
                'code' => 11,
                'status' => "ALREADY_EXISTS",
                'message' => "Is already participating",
                'data' => $isParticipating,
            ]);
        }

        // Create order here

        if ($event->isFree()) {
            $event->entry_fee = 0;
            $event->save();
        }


        // Creating an order
        $order = new Order();
            $order->customer_id = $customer->id;
            $order->status = "waiting";
            $order->is_waiting = true;
            if($event->entry_fee == 0){
                $order->status = "paid";
                $order->is_waiting = false;
            }
            $order->is_delivery = false;
            $order->service_slug = 'event';

            $subtotal = $event->entry_fee * $input['seats_count'];
            $tax = $subtotal * Invoice::TAXES;
            $total = $subtotal + $tax;
            $order->amount = $total;
        $order->save();

        // Creating an entry in OrderHistories
        $orderHistory = new OrderHistory();
            $orderHistory->order_id = $order->id;
            $orderHistory->status = Order::WAITING;
            if($event->entry_fee == 0){
                $orderHistory->status = Order::CUSTOMER_PAID;
            }
            $orderHistory->order_id = $order->id;
            $orderHistory->creator = 'customers';
            $orderHistory->creator_id = $customer->id;
            $orderHistory->creator_name = $customer->name;
        $orderHistory->save();

        // Creating each order Items and tickets
        for($x = 0; $x < $input['seats_count']; $x++){

            $prefix = "GDH::";
            $ticket = new EventTicket();
            $code = $prefix.strtoupper(Str::random(10));
            $code_exists = EventTicket::where('code', $code)->first();
            $qty = 1;

            if($code_exists){
                $code = $prefix.strtoupper(Str::random(10));
            }

            $ticket->code = $code;
            $ticket->seats_count = $qty;
            // $ticket->seats_count = $input['seats_count'];
            // $ticket->amount= $event->entry_fee * $input['seats_count'];
            $ticket->amount = $event->entry_fee;
            $ticket->event_id = $event->id;
            // if ($event->entry_fee == 0) {
            //     $ticket->is_paid = true;
            //     $ticket->status = "paid";
            //     $ticket->is_active = true;
            // }
            $ticket->customer_id= $customer->id;
            $ticket->save();

            $orderItem = new OrderItem();
            $orderItem->order_id = $order->id;
            $orderItem->service_slug = 'event';
            $orderItem->meta_data_id = $ticket->id;
            $orderItem->meta_data = $ticket->toJson();
            $orderItem->quantity = $qty;
            $orderItem->unit_price = $event->entry_fee;
            $orderItem->total_amount = $qty * $event->entry_fee;
            // $orderItem->days = ;
            $orderItem->save();
        }



        // Creating invoice
        $invoice = new Invoice();
            $invoice->order_id = $order->id;
            $invoice->customer_id = $customer->id;
            $invoice->reference = Invoice::generateID('EVENT', $order->id, $customer->id);
            $invoice->service = 'event';
            $invoice->link = '#';
            $invoice->subtotal = $subtotal;
            $invoice->tax = $tax;
            $invoice->fees_delivery = 0;
            $invoice->total = $total;
            $invoice->status = 'unpaid';
            // if($event->entry_fee == 0){
            //     $invoice->status = 'paid';
            // }
            $invoice->is_paid_by_customer = true;
            $invoice->is_paid_by_delivery_service = false;
        $invoice->save();

        return response()->json([
            'code' => 11,
            'status' => "OK",
            'message' => "Success",
            'data' => [
                // 'ticket' => $ticket,
                'checkout_url' => '/ajax/event/'.$invoice->id.'/'.$event->id.'/checkout/'.$input['seats_count'],
            ],
        ]);

    }
    // Show checkout
    public function eventShowCheckout($invoiceId, $eventId, $seatsCount)
    {
        if (!Auth::guard('customer')->check()) {
            return redirect('/customer/login');
        }

        $customer = Auth::guard('customer')->user();

        $event = Event::find($eventId);

        if (!$event) {
            return view('errors.404');
        }

        $invoice = Invoice::find($invoiceId);
        $order = $invoice->order;

        return view('customer_frontOffice.events.checkout', compact('event', 'customer', 'seatsCount', 'order', 'invoice'));
    }

    public function eventParticipateCancel(Request $request)
    {
        if(!Auth::guard('customer')->check()){
            return response()->json([
                'code' => 403,
                'status' => "MISSING_AUTH",
                'message' => "Unauthorized",
                'data' => null,
            ]);
        }

        $customer = Auth::guard('customer')->user();

        $input = $request->all();

        if($input == null){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => null,
            ]);
        }

        $event = Event::find($input['event_id']);

        if(!$event){
            return response()->json([
                'code' => -11,
                'status' => "NOT_FOUND",
                'message' => "",
                'data' => null,
            ]);
        }

        $isParticipating = DB::table('participates_in_event')->where([
            [
                'event_id', $event->id
            ],
            [
                'customer_id', $customer->id
            ],
        ])->delete();

        if($isParticipating){
            return response()->json([
                'code' => 11,
                'status' => "OK",
                'message' => "Cancelled participation",
                'data' => $isParticipating,
            ]);
        }

        return response()->json([
            'code' => 11,
            'status' => "OK",
            'message' => "Could not cancel participation",
            'data' => false,
        ]);
    }

    public function eventList()
    {
        $e = Event::all();
        // $c_json = $c->tojson();
        return response()->json(["data" => $e]);
    }

    // *** searchs an event
    public function eventSearch(Request $request)
    {
        // if(!Auth::guard('customer')->check()){
        //     return response()->json([
        //         'code' => 403,
        //         'message' => "Unauthorized",
        //         'data' => null,
        //     ]);
        // }

        // $customer = Auth::guard('customer')->user();

        $input = $request->all();
        //
        if($input == null){
            return response()->json([
                'code' => -11,
                'message' => "",
                'data' => null,
            ]);
        }

        // return response()->json([
        //     'code' => -333,
        //     'message' => "",
        //     'data' => $request->all(),
        // ]);

        $value = $input['value'];
        // $excludedEmails = $input['exclude'];

        $suggestions = Event::select('*')->where('title', 'like', '%'.$value.'%')->orWhere('date_event', 'like', '%'.$value.'%')->orWhere('description', 'like', '%'.$value.'%')->take(5)->get();

        return response()->json([
            'code' => 11,
            'message' => "OK",
            'data' => $suggestions,
        ]);
    }

    // ***** CART
    public function cartClear()
    {
        ShoppingCart::Clear();
        $shoppingCart = ShoppingCart::ShoppingCart();

        return response()->json([
            'code' => 11,
            'status' => "OK",
            'message' => "cart cleared",
            'data' => $shoppingCart,
        ]);
    }
    public function CartAddDetail(Request $request)
    {
        $input = $request->all();



        if (!isset($input['item_id'])) {
            //
            return response()->json([
                'status' => "MISSING_ITEM_DATA",
                'error' => [
                    'message' => "MISSING_ITEM_DATA"
                ]
            ]);
        }
        if (!isset($input['detail_name'])) {
            //
            return response()->json([
                'status' => "MISSING_DETAIL_NAME",
                'error' => [
                    'message' => "MISSING_DETAIL_NAME"
                ]
            ]);
        }
        if (!isset($input['detail_content'])) {
            //
            return response()->json([
                'status' => "MISSING_DETAIL_CONTENT",
                'error' => [
                    'message' => "MISSING_DETAIL_CONTENT"
                ]
            ]);
        }

        $shoppingCart = ShoppingCart::ShoppingCart();

        $item = $shoppingCart->items->find($input['item_id']);

        if($input['detail_name'] == "size"){
            $item->size = $input['detail_content'];
        }
        if($input['detail_name'] == "color"){
            $item->color = $input['detail_content'];
        }
        $item->save();

        return response()->json([
            'status' => "OK"
        ]);
    }
    public function cartDecrement($id)
    {
        ShoppingCart::DecrementItem($id);
        return redirect()->back();
    }
    public function cartIncrement($id)
    {
        ShoppingCart::IncrementItem($id);
        return redirect()->back();
    }
    public function cartManage(Request $request)
    {
        // if(!Auth::guard('customer')->check()){
        //     return response()->json([
        //         'code' => 403,
        //         'status' => "MISSING_AUTH",
        //         'message' => "Unauthorized",
        //         'data' => null,
        //     ]);
        // }
        // checking for session
        $customer = Auth::guard('customer')->user();

        $shoppingCart = ShoppingCart::ShoppingCart();


        $input = $request->all();

        if($input == null){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => [
                    'itemCount' => ShoppingCart::ShoppingCart()->items->count(),
                ]
            ]);
        }



        $product = Product::find($input['product_id']);
            // 'product_id': cartData.pId,
            // 'product_name': cartData.pName,
            // 'product_quantity': cartData.pQuantity,
            // 'product_price': cartData.pPrice,
        $cartItem = $shoppingCart->items->where('product_id', $product->id)->first();
        // quantity == 0 => remove item
        if($input['product_quantity'] < 1){
            if($cartItem){
                $cartItem->delete();
            }
            return response()->json([
                'code' => 11,
                'status' => "REMOVED",
                'message' => "produit retiré",
                'data' => [
                    'cartItem' => $cartItem,
                    'itemCount' =>ShoppingCart::ShoppingCart()->items->count(),
                ]
            ]);
        }
        //
        if(!$cartItem){
            $cartItem = new CartItem();
            $cartItem->shopping_cart_id = $shoppingCart->id;
            $cartItem->product_id = $product->id;
            $cartItem->price = $product->getPrice();
            // $itemCount = 1;

        }
        if(isset($input['product_size']) && $input['product_size'] != null){
            $cartItem->size = $input['product_size'];
        }
        if(isset($input['product_color']) && $input['product_color'] != null){
            $cartItem->color = $input['product_color'];
        }
        $cartItem->quantity = $input['product_quantity'];
        $cartItem->amount = $product->getPrice() *$input['product_quantity'];
        $cartItem->save();

        return response()->json([
            'code' => 11,
            'status' => "OK",
            'message' => "Article ajouté",
            'data' => [
                'cartItem' => $cartItem,
                'itemCount' => ShoppingCart::ShoppingCart()->items->count(),
            ]
        ]);
    }
    public function cartManageUnique(Request $request)
    {
        // if(!Auth::guard('customer')->check()){
        //     return response()->json([
        //         'code' => 403,
        //         'status' => "MISSING_AUTH",
        //         'message' => "Unauthorized",
        //         'data' => null,
        //     ]);
        // }
        // checking for session
        $customer = Auth::guard('customer')->user();

        $shoppingCart = ShoppingCart::ShoppingCart();

        $input = $request->all();

        if($input == null){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => [
                    'itemCount' => ShoppingCart::ShoppingCart()->items->count(),
                ]
            ]);
        }

        $product = Product::find($input['product_id']);
        // 'product_id': cartData.pId,
        // 'product_name': cartData.pName,
        // 'product_quantity': cartData.pQuantity,
        // 'product_price': cartData.pPrice,
        $cartItem = $shoppingCart->items->where('product_id', $product->id)->get();
        // quantity == 0 => remove item
        if($input['product_quantity'] < 1){
            if($cartItem){
                foreach($cartItem as $cItem){
                    $cItem->delete();
                }
            }
            return response()->json([
                'code' => 11,
                'status' => "REMOVED",
                'message' => "produit retiré",
                'data' => [
                    'cartItem' => $cartItem,
                    'itemCount' =>ShoppingCart::ShoppingCart()->items->count(),
                ]
            ]);
        }
        //
        if(!$cartItem){
            // For each size
            if($product->sizes()->count()>0){
                foreach($product->sizes() as $size)
                {

                    $cartItem = new CartItem();
                    $cartItem->shopping_cart_id = $shoppingCart->id;
                    $cartItem->product_id = $product->id;
                    $cartItem->price = $product->getPrice();
                    $cartItem->size = $size;
                    $cartItem->quantity = 1;
                    $cartItem->amount = $product->getPrice();
                    if($product->getProductColor()->first() != null){
                        $cartItem->color = $product->getProductColor()->first()->name;
                    }
                    $cartItem->save();
                }
            }

        }


        return response()->json([
            'code' => 11,
            'status' => "OK",
            'message' => "Article ajouté",
            'data' => [
                'cartItem' => $cartItem,
                'itemCount' => ShoppingCart::ShoppingCart()->items->count(),
            ]
        ]);
    }
    public function cartRemove($id)
    {
        ShoppingCart::Remove($id);
        return redirect()->back();
    }
    // *** Cart Components
    public function CartComponentQuantity()
    {
        return view('customer_frontOffice.shops.components.cart_quantity');
    }
    // *** Shop Create order Only
    public function createOrderOnly(Request $request)
    {
        if(!Auth::guard('customer')->check()){
            return response()->json([
                'code' => 403,
                'status' => "MISSING_AUTH",
                'message' => "Unauthorized",
                'data' => null,
            ]);
        }


        $customer = Auth::guard('customer')->user();

        $input = $request->all();

        if($input == null){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => null,
            ]);
        }

        $delivery_name = $input['delivery_name'];

        if(!$delivery_name){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => null,
            ]);
        }
        $delivery_lat = $input['delivery_lat'];

        if(!$delivery_lat){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => null,
            ]);
        }
        $delivery_lng = $input['delivery_lng'];

        if(!$delivery_lng){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => null,
            ]);
        }

        $shoppingCart = ShoppingCart::ShoppingCart();

        // Creating an order
            $order = new Order();
            $order->customer_id = $customer->id;
            $order->status = "waiting";
            $order->is_waiting = true;
            $order->is_delivery = false;
            $order->service_slug = 'shop';
            $order->save();

            $subtotal = 0;
            $order->location_name = $delivery_name;
            $order->location_latitude = $delivery_lat;
            $order->location_longitude = $delivery_lng;

            $currencyCode = 'XOF';
            $currencyName = 'CFA';

            foreach ($shoppingCart->items as $item){

                $subtotal += $item->amount;
                OrderItem::create([
                    'order_id'=> $order->id,
                    'meta_data_id' => $item->product_id,
                    'meta_data' => $item->product,
                    'quantity' => $item->quantity,
                    'unit_price' => $item->price,
                    'total_amount' => $item->amount,
                    'currency_code' => $currencyCode,
                    'currency_name' => "CFA",
                    'color_choice' => $item->color,
                    'size_choice' => $item->size,

                ]);
            }

            $tax = $subtotal * 0.0;/*Invoice::TAXES;*/
            $total = $subtotal + $tax;

            $order->amount = $total;
                $order->currency_code =  $currencyCode;
                $order->currency_name =  $currencyName;
            $order->save();
            // empty cart total

            // Creating an entry in OrderHistories
            $orderHistory = new OrderHistory();
                $orderHistory->order_id = $order->id;
                $orderHistory->status = Order::WAITING;
                $orderHistory->order_id = $order->id;
                $orderHistory->creator = 'customers';
                $orderHistory->creator_id = $customer->id;
                $orderHistory->creator_name = $customer->name;
            $orderHistory->save();


            $invoice = Invoice::create([
                'order_id' => $order->id,
                'customer_id' => $customer->id,
                'reference' => Invoice::generateID('SHOP', $order->id, $customer->id),
                'link' => '#',
                'subtotal' => $subtotal,
                'service' => 'shop',
                'tax' => $tax,
                // 'fees_delivery' => 0,
                'total' => $total,
                'status' => 'unpaid',
                'is_paid' => false,
                'is_paid_by_customer' => false,
                'is_paid_by_delivery_service' => false,
                'currency_code' => $currencyCode,
                'currency_name' => $currencyName
            ]);


        return response()->json([
            'code' => 11,
            'status' => "OK",
            'message' => "Success",
            'data' => [
                // 'ticket' => $ticket,
                'checkout_url' => '/customer/shop/checkout/'.$invoice->id,
                'invoice_id' => $invoice->id,
            ],
        ]);
    }

    public function createOrder(Request $request)
    {
        if(!Auth::guard('customer')->check()){
            return response()->json([
                'code' => 403,
                'status' => "MISSING_AUTH",
                'message' => "Unauthorized",
                'data' => null,
            ]);
        }


        $customer = Auth::guard('customer')->user();

        $input = $request->all();

        if($input == null){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => null,
            ]);
        }

        $delivery_name = $input['delivery_name'];

        if(!$delivery_name){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => null,
            ]);
        }
        $delivery_lat = $input['delivery_lat'];

        if(!$delivery_lat){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => null,
            ]);
        }
        $delivery_lng = $input['delivery_lng'];

        if(!$delivery_lng){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => null,
            ]);
        }

        $shoppingCart = ShoppingCart::ShoppingCart();

        // Creating an order
            $order = new Order();
            $order->customer_id = $customer->id;
            $order->status = "waiting";
            $order->is_waiting = true;
            $order->is_delivery = false;
            $order->payment_method_slug = $input['payment_method'] ?? 'online';
            $order->delivery_fees = $shoppingCart->getDeliveryFeeTotal();
            $order->service_slug = 'shop';
            $order->save();

            $subtotal = $shoppingCart->getSousTotal();
            // $subtotal = 0;
            $order->location_name = $delivery_name;
            $order->location_latitude = $delivery_lat;
            $order->location_longitude = $delivery_lng;
            $order->location_detail = $input['detail'] ?? "";


            $currencyCode = 'XOF';
            $currencyName = 'CFA';

            foreach ($shoppingCart->items as $item){

                // $subtotal += $item->amount;
                OrderItem::create([
                    'order_id'=> $order->id,
                    'meta_data_id' => $item->product_id,
                    'meta_data' => $item->product,
                    'quantity' => $item->quantity,
                    'unit_price' => $item->price,
                    'total_amount' => $item->amount,
                    'currency_code' => $currencyCode,
                    'currency_name' => "CFA",
                    'size_choice' => $item->size,
                    'size' => $item->size,
                    'color_choice' => $item->getColor()->code_color ?? "",
                    'color' => $item->getColor()->code_color ?? "",
                    'service_slug'  =>  'shop'
                ]);
            }

            $tax = 0.0;/*Invoice::TAXES;*/
            // $total = $subtotal + $tax;

            // $order->amount = $total;
                $order->currency_code =  $currencyCode;
                $order->currency_name =  $currencyName;
            $order->save();
            // empty cart total

            // Creating an entry in OrderHistories
            $orderHistory = new OrderHistory();
                $orderHistory->order_id = $order->id;
                $orderHistory->status = Order::WAITING;
                $orderHistory->order_id = $order->id;
                $orderHistory->creator = 'customers';
                $orderHistory->creator_id = $customer->id;
                $orderHistory->creator_name = $customer->name;
            $orderHistory->save();

            $discount = 0;
            $total =0;
            if($request->session()->has('coupons')){
                foreach($request->session()->get('coupons') as $coupon){
                    $discount += $invoice->total * $coupon->pourcentage;
                
                    $using_coupon = CouponOrder::create([
                        'order_id' => $order->id,
                        'coupon_id' => $coupon->id,
                        'amount_saved' => $discount,
                    ]);
                }

                $request->session()->forget('coupons');

                $total = round ($shoppingCart->getTotal() + $tax - $discount);    
            }else {
                $total = round ($shoppingCart->getTotal() + $tax);    
            }

            $order->amount = $total;
            $order->save();

            $invoice = Invoice::create([
                'order_id' => $order->id,
                'customer_id' => $customer->id,
                'reference' => Invoice::generateID('SHOP', $order->id, $customer->id),
                'link' => '#',
                'subtotal' => $subtotal,
                'service' => 'shop',
                'tax' => $tax,
                'fees_delivery' => $shoppingCart->getDeliveryFeeTotal(),
                'total' => $total,
                'status' => 'unpaid',
                'is_paid' => false,
                'is_paid_by_customer' => false,
                'is_paid_by_delivery_service' => false,
                'currency_code' => $currencyCode,
                'currency_name' => $currencyName,
                'discount'  => $discount
            ]);


            // Creating transation (invoicePayment)
            $transactionId = self::generateTransId($customer->id, $invoice->id, $order->id);

            // Event payment tickets is online by default
            $transactionData = null;

            $transaction = new InvoicePayment();

                $transaction->invoice_id = $invoice->id;
                $transaction->payment_method = $input['payment_method'] ?? 'online';
                $transaction->payment_reference = $transactionId;
                $transaction->amount= $invoice->total;
                $transaction->currency_code = $invoice->currency_code;
                $transaction->creator_id = $customer->id;
                $transaction->creator = 'customers';
                $transaction->creator_name = $customer->name;
            $transaction->save();

            $transactionData = [
                'paymentMethod'=> $transaction->payment_method,
                'id' => $transactionId,
                'designation' => 'Golden Health Buying Product',
                'amount' => (int)$transaction->amount,
                'currency' => $transaction->currency_code ?? "XOF",
                'status' => $transaction->status,
                'customField' => "Golden Health Buying Product",
            ];

        return response()->json([
            'code' => 11,
            'status' => "OK",
            'message' => "Success",
            'data' => [
                // 'ticket' => $ticket,
                'checkout_url' => '/customer/shop/checkout/'.$order->id,
                'transaction' => $transactionData,
            ],
        ]);
    }

    // *** Shop Create transaction from order created above
    public function createTransaction(Request $request){
        if(!Auth::guard('customer')->check()){
            return response()->json([
                'code' => 403,
                'status' => "MISSING_AUTH",
                'message' => "Unauthorized",
                'data' => null,
            ]);
        }

        $customer = Auth::guard('customer')->user();

        $input = $request->all();

        if($input == null){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => null,
            ]);
        }

        $invoice_id = null;
        if(isset($input['invoice_id'])){

            $invoice_id = $input['invoice_id'];
        }

        if(!$invoice_id){
            return response()->json([
                'code' => -11,
                'status' => "MISSING_DATA",
                'message' => "",
                'data' => null,
            ]);
        }

        $invoice = Invoice::find($invoice_id);
        if(!$invoice){
            return response()->json([
                'code' => -11,
                'status' => "INVOICE_NOT_FOUND",
                'message' => "",
                'data' => null,
            ]);
        }
        $order = $invoice->order;

        // Creating transation (invoicePayment)
            $transactionId = self::generateTransId($customer->id, $invoice->id, $order->id);

            // Event payment tickets is online by default
            $transactionData = null;

            $transaction = new InvoicePayment();

            $transaction->invoice_id = $invoice->id;
            $transaction->payment_method = 'online';
            $transaction->payment_reference = $transactionId;
            $transaction->amount= $invoice->total;
            $transaction->currency_code = $invoice->currency_code;
            $transaction->creator_id = $customer->id;
            $transaction->creator = 'customers';
            $transaction->creator_name = $customer->name;
            $transaction->save();

            $transactionData = [
                'paymentMethod'=> $transaction->payment_method,
                'id' => $transactionId,
                'designation' => 'Golden Health Buying Product',
                'amount' => (int)$transaction->amount,
                'currency' => $transaction->currency_code ?? "XOF",
                'status' => $transaction->status,
                'customField' => "Golden Health Buying Product",
            ];

        return response()->json([
            'code' => 11,
            'status' => "OK",
            'message' => "Success",
            'data' => [
                // 'ticket' => $ticket,
                'checkout_url' => '/customer/shop/checkout/'.$order->id,
                'transaction' => $transactionData,
            ],
        ]);
    }


    public function shopCheckout($invoiceId){

        $shoppingCart = ShoppingCart::ShoppingCart();
        if($shoppingCart->items->count() < 1){
            return redirect()->back();
        }

        $invoice = Invoice::find($invoiceId);
        if(!$invoice){
            return view('errors\404');
        }

        $order = $invoice->order;
        if(!$order){
            return view('errors\404');
        }

        // If transaction already completed and successful
        if($order->status != Order::WAITING){

            $transaction = InvoicePayment::where('invoice_id', $invoice->id)->first();

            return redirect('/customer/shop/checkout_finish/'.$transaction->reference);

            // return view('customer_frontOffice.shops.checkout', compact('order', 'invoice'));
        }

        return view('customer_frontOffice.shops.checkout', compact('order', 'invoice'));
    }

    // public function shopCheckout($orderId){

    //     $shoppingCart = ShoppingCart::ShoppingCart();
    //     if($shoppingCart->items->count() < 1){
    //         return redirect()->back();
    //     }

    //     $order = Order::find($orderId);
    //     if(!$order){
    //         return view('errors.404');
    //     }

    //     $invoice = Invoice::where('order_id', $order->id)->first();
    //     if(!$invoice){
    //         return view('errors.404');
    //     }
    //     $transaction = InvoicePayment::where('invoice_id', $invoice->id)->first();
    //     if (!$transaction) {
    //         return view('errors.404');
    //     }
    //     // If transaction already completed and successful
    //     if($order->status != Order::WAITING){


    //         if($transaction->is_completed){
    //             if(Auth::guard('customer')->user()){
    //                 if(Auth::guard('customer')->user()->id == $order->customer_id){

    //                     return view('customer_frontOffice.shops.checkout_finish', compact('order', 'invoice', 'transaction'));
    //                 }
    //             }
    //             return view('erros.404');
    //         }
    //         return view('erros.404');
    //     }

    //     $transactionData = [
    //         'paymentMethod'=> $transaction->payment_method,
    //         'id' => $transaction->payment_reference,
    //         'designation' => 'Golden Health Buying Product',
    //         'amount' => (int)$transaction->amount,
    //         'currency' => $transaction->currency_code ?? "XOF",
    //         'status' => $transaction->status,
    //         'customField' => "Golden Health Buying Product",
    //     ];

    //     return view('customer_frontOffice.shops.checkout', compact('order', 'invoice', 'transactionData'));
    // }
    //

    // Notify result pages
    public function shopCheckoutFinish($transId){

        $customer = Auth::guard('customer')->user();
        if(!$customer){
            return redirect('customer/login');
        }

        $transaction = InvoicePayment::where('payment_reference', $transId)->first();

        if (!$transaction) {
            return redirect('errors.404');
        }

        $invoice = Invoice::find($transaction->invoice_id);

        if (!$invoice) {
            return redirect('errors.404');
        }

        $order = Order::find($invoice->order_id);

        if (!$order) {
            return redirect('errors.404');
        }

        if($customer->id != $order->customer_id){
            return view('errors.404');
        }

        if($transaction->status == "SUCCESSFUL"){
            ShoppingCart::Clear();
        }

        return view('customer_frontOffice.shops.checkout_finish', compact('order', 'invoice', 'transaction'));
    }
    //
    public function eventCheckoutFinish($transId){

        $customer = Auth::guard('customer')->user();
        if(!$customer){
            return redirect('customer/login');
        }

        $transaction = InvoicePayment::where('payment_reference', $transId)->first();

        if (!$transaction) {
            return view('errors.404');
        }

        $invoice = Invoice::find($transaction->invoice_id);

        if (!$invoice) {
            return view('errors.404');
        }

        $order = Order::find($invoice->order_id);

        if (!$order) {
            return view('errors.404');
        }

        if($customer->id != $order->customer_id){
            return view('errors.404');
        }

        // Finding out event
        $latest_ticket = EventTicket::find($order->orderItems->first()->meta_data_id);

        $event = $latest_ticket->event;

        if(!$event){
            return view('errors.404');
        }



        return view('customer_frontOffice.events.checkout_finish', compact('order', 'invoice', 'transaction', 'event'));
    }



    public static function generateTransId($customerId, $invoiceId, $orderId)
    {
        $timestamp = time();
        $parts = explode(' ', microtime());
        $id = ($timestamp + $parts[0] - strtotime('today 00:00')) * 10;
        $id = sprintf('%06d', $id) . mt_rand(100, 9999);
        $transId = $id."-".$customerId."-".$invoiceId."-".$orderId;

        $transactions = InvoicePayment::where(['payment_reference' => $transId])->get();
        if( count($transactions) == 0){

            return  $transId;
        }else{
            $autoIncrement = count($transactions) + 1;
            $transId = $transId.'-'.$autoIncrement;
            return  $transId;
        }
    }

    // coupon
    /*public function couponAdd(Request $request) {

      $input = $request->all();
      $shopping_key = session()->get('shopping_key');

      if (!$shopping_key) {
          return response()->json([
              'status' => "MISSING_CART",
              'error' => [
                  'message' => "Shopping Cart"
              ]
          ]);
      }

      $shoppingCart = ShoppingCart::where('key', $shopping_key)->first();

      if (!isset($input['coupon_code'])) {
          return response()->json([
              'status' => "MISSING_COUPON",
              'error' => [

                  'message' => "Coupon non défini"
              ]
          ]);
      }
      $coupon_code = $input['coupon_code'];

      $coupon = Coupon::where('code', $coupon_code)->first();
      if(!$coupon){
          return response()->json([
              'status' => "INVALID_COUPON",
              'error' => [
                  'message' => "Coupon Invalide"
              ]
          ]);
      }


      // check if its not already used
      $used_coupons = CouponOrder::where('coupon_id', $coupon->id);
      if($used_coupons->count() > 0){
          // if used check if number of ppl reached
          if($used_coupons->count() >= $coupon->nbr_person)
          {
              return response()->json([
                  'status' => "COUPON_LIMIT_REACHED",
                  'error' => [
                      'message' => "Ce coupon n'est plus utilisable"
                  ]
              ]);
          }
          // if less than nbr ppl
      }
      $amount = $shoppingCart->getTotal();
      $amount_saved = 0;
      if($coupon->pourcentage != null){
          $amount_saved = ($amount * $coupon->pourcentage)/100;
      }

      $amount_saved = floor($amount_saved);

      return response()->json([
          'status' => "OK"
      ]);
    }*/


    public function couponAdd(Request $request){
        $input = $request->all();
        $invoice_id = null;
        $coupon_code = null;
        if (!isset($input['invoice_id'])) {

            return response()->json([
                'status' => "MISSING_INVOICE",
                'error' => [

                    'message' => "Commande non définie"
                ]
            ]);
        }
        $invoice_id = $input['invoice_id'];

        if (!isset($input['coupon_code'])) {
            return response()->json([
                'status' => "MISSING_COUPON",
                'error' => [

                    'message' => "Commande non définie"
                ]
            ]);
        }
        $coupon_code = $input['coupon_code'];

        $invoice = Invoice::find($invoice_id);


        if(!$invoice){
            return response()->json([
                'status' => "INVOICE_NOT_FOUND",
                'invoice' => $invoice,
                'error' => [
                    'message' => "Facture introuvable"
                ]
            ]);
        }
        $order = $invoice->order;

        if(!$order){
            return response()->json([
                'status' => "ORDER_NOT_FOUND",
                'order' => $order,
                'error' => [
                    'message' => "Commande introuvable"
                ]
            ]);
        }

        $coupon = Coupon::where('code', $coupon_code)->first();
        if(!$coupon){
            return response()->json([
                'status' => "INVALID_COUPON",
                'error' => [
                    'message' => "Coupon Invalide"
                ]
            ]);
        }

        if(!is_null($coupon->date_fin)){
            if(\Carbon\Carbon::today()->greaterThan(\Carbon\Carbon::parse($coupon->date_fin))){
                return response()->json([
                    'status' => "MISSING_COUPON",
                    'error' => [
                        'message' => "Le coupon à atteint son nombre maximum d'utilisation"
                    ]
                ]);
            }
        }

        if(!is_null($coupon->nbr_person)){
            if($coupon->nbr_person == 0){
                return response()->json([
                    'status' => "MISSING_COUPON",
                    'error' => [
                        'message' => "Le coupon à atteint son nombre maximum d'utilisation"
                    ]
                ]);
            }
            $coupon->nbr_person -= 1;
            $coupon->save();
        }

        $is_coupon_already_used_by_order = $order->coupons->where('code', $coupon_code)->first();
        if($is_coupon_already_used_by_order){
            return response()->json([
                'status' => "COUPON_CURRENTLY_USED",
                'error' => [
                    'message' => "Vous avez déjà appliqué ce coupon"
                ]
            ]);
        }
        // check if its not already used
        $used_coupons = CouponOrder::where('coupon_id', $coupon->id);
        if($used_coupons->count() > 0){
            // if used check if number of ppl reached
            if($used_coupons->count() >= $coupon->nbr_person)
            {
                return response()->json([
                    'status' => "COUPON_LIMIT_REACHED",
                    'error' => [
                        'message' => "Ce coupon n'est plus utilisable"
                    ]
                ]);
            }
            // if less than nbr ppl
        }
        // create new instance
        $amount_saved = 0;
        if($coupon->pourcentage != null){
            $amount_saved = ($order->amount * $coupon->pourcentage)/100;
        }
        $amount_saved = floor($amount_saved);

        $using_coupon = CouponOrder::create([
            'order_id' => $order->id,
            'coupon_id' => $coupon->id,
            'amount_saved' => $amount_saved,
        ]);
        // reducing order amount
        $order->amount -= $amount_saved;
        if($order->amount < 0){
            $order->amount = 0;
        }
        $order->save();

        $invoice->discount += $amount_saved;
        $invoice->total = $order->amount;
        $invoice->save();

        // TODO : create orderItem and invoicePayment

        return response()->json([
            'status' => "OK",
        ]);

    }

    public function couponAddStorage(Request $request){
        $input = $request->all();
        $invoice_id = null;
        $coupon_code = null;
        
        if (!isset($input['coupon_code']) || is_null($input['coupon_code'])) {
            return response()->json([
                'status' => "MISSING_COUPON",
                'error' => [
                    'message' => "le code du coupon est obligatoire"
                ]
            ]);
        }
        
        $coupon_code = $input['coupon_code'];
        
        $coupon = Coupon::where('code', $coupon_code)->first();
        if(!$coupon){
            return response()->json([
                'status' => "INVALID_COUPON",
                'error' => [
                    'message' => "Coupon Invalide"
                ]
            ]);
        }

        if(!is_null($coupon->date_fin)){
            if(\Carbon\Carbon::today()->greaterThan(\Carbon\Carbon::parse($coupon->date_fin))){
                return response()->json([
                    'status' => "MISSING_COUPON",
                    'error' => [
                        'message' => "Le coupon à atteint son nombre maximum d'utilisation"
                    ]
                ]);
            }
        }

        if(!is_null($coupon->nbr_person)){
            if($coupon->nbr_person == 0){
                return response()->json([
                    'status' => "MISSING_COUPON",
                    'error' => [
                        'message' => "Le coupon à atteint son nombre maximum d'utilisation"
                    ]
                ]);
            }
            $coupon->nbr_person -= 1;
            $coupon->save();
        }

        $coupons = $request->session()->get('coupons') ?? [];
        if(!empty($coupons)){
            foreach ($coupons as $item) {
                if($item->code == $input['coupon_code']){
                    return response()->json([
                        'status' => "COUPON_CURRENTLY_USED",
                        'error' => [
                            'message' => "Vous avez déjà appliqué ce coupon"
                        ]
                    ]);
                }
            }
        }
        
        // $using_coupon = CouponOrder::create([
        //     'order_id' => $order->id,
        //     'coupon_id' => $coupon->id,
        //     'amount_saved' => $amount_saved,
        // ]);
        
        // $invoice->discount += $amount_saved;
        // $invoice->total = $order->amount;
        // $invoice->save();

        array_push($coupons, $coupon);
        $coupons = $request->session()->put('coupons', $coupons); 

        return response()->json([
            'status' => "OK",
        ]);
    }

    // *** PDF generating
    public function invoicePDF($id) {
        $order = $this->orderRepository->find($id);

        $pdf = \PDF::loadView('pdf.invoicepdf', compact('order', $order));

        return $pdf->stream('invoice.pdf');
   }
}
