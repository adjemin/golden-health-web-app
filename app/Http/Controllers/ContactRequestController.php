<?php

namespace App\Http\Controllers;

use App\User;
use App\ContactRequest;
use Illuminate\Http\Request;
use App\Mail\AdminNotifyMail;
use App\Mail\ContactRequestMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminSignupNotificationMail;
use PDF;
use App\Models\CustomerNotifications;
use App\Utils\CustomerNotificationUtils;

class ContactRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = ContactRequest::orderBy('id', 'desc')->paginate(10);
        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //
        $data = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'subject' => 'required',
            'message' => 'required|min:10',

        ]);
        $contactRequest = new ContactRequest();
        $contactRequest->name = $data['name'];
        $contactRequest->email = $data['email'];
        $contactRequest->phone = $data['phone'];
        $contactRequest->subject = $data['subject'];
        $contactRequest->message = $data['message'];

        $contactRequest->save();

        session()->flash('success', "Candidature enregistrée avec success");

        session(['name'=> $contactRequest->name]);

        Mail::to($data['email'])->send(new ContactRequestMail());
        $data = [
            'subject' => "Nouveau message",
            'title' => "Vous avez reçu un nouveau message",
            'subtitle' => "$contactRequest->name vous a envoyé un message depuis la page contact.",
        ];


        // $customerNotification = new CustomerNotifications();
        // $customerNotification->title  = "Nouveau message";

        // $customerNotification->title_en  = "New message sent";

        // $customerNotification->subtitle  = "Vous avez envoyé un nouveau message.";
        // $customerNotification->subtitle_en  = "You send a new request to admin";
        // $customerNotification->action  = "";
        // $customerNotification->action_by  = "";
        // $customerNotification->type_notification  = "confirmed";
        // $customerNotification->is_read  = false;
        // $customerNotification->is_received  = false;
        // $customerNotification->user_id  = $user->id;
        // $customerNotification->save();

        // CustomerNotificationUtils::notify($customerNotification);

        foreach (User::all() as $user) {
            Mail::to($user->email)->send(new AdminNotifyMail($data));
        }

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContactRequest  $contactRequest
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = ContactRequest::find($id);

        if (empty($contact)) {
            Flash::error('Contact not found');

            return redirect(route('contacts.index'));
        }

        return view('contacts.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContactRequest  $contactRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactRequest $contactRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContactRequest  $contactRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContactRequest $contactRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactRequest  $contactRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = ContactRequest::find($id);
    
        if (empty($contact)) {
            Flash::error('Contact not found');

            return redirect(route('contacts.index'));
        }

        Flash::success('Contact deleted successfully.');

        return redirect(route('contacts.index'));
    }

    public function printAll(){
        $contacts = ContactRequest::all();
        return view('contacts.table_pdf', compact('contacts'));
        // $pdf = PDF::loadView('contacts.table_pdf', compact('contacts'));
        // return $pdf->stream();
    }
}