<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateClientTestimonyRequest;
use App\Http\Requests\UpdateClientTestimonyRequest;
use App\Repositories\ClientTestimonyRepository;
use App\Models\ClientTestimony;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Imgur;

class ClientTestimonyController extends AppBaseController
{
    /** @var  ClientTestimonyRepository */
    private $clientTestimonyRepository;

    public function __construct(ClientTestimonyRepository $clientTestimonyRepo)
    {
        $this->clientTestimonyRepository = $clientTestimonyRepo;
    }

    /**
     * Display a listing of the ClientTestimony.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $clientTestimonies = $this->clientTestimonyRepository->all();
        $clientTestimonies = ClientTestimony::orderBy('id', 'desc')->paginate(10);
        return view('client_testimonies.index')
            ->with('clientTestimonies', $clientTestimonies);
    }

    /**
     * Show the form for creating a new ClientTestimony.
     *
     * @return Response
     */
    public function create()
    {
        return view('client_testimonies.create');
    }

    /**
     * Store a newly created ClientTestimony in storage.
     *
     * @param CreateClientTestimonyRequest $request
     *
     * @return Response
     */
    public function store(CreateClientTestimonyRequest $request)
    {
        $input = $request->all();

        if ($request->file('client_photo_url')) {
            $image = $request->file('client_photo_url');
            $pictures = [];
            if ($image != null) {    
                $productImage = Imgur::upload($image);
                $productImageLink = $productImage->link();
            }
        }else{
            $productImageLink = "";
        }

        $input['client_photo_url'] = $productImageLink;
        $clientTestimony = $this->clientTestimonyRepository->create($input);

        Flash::success('Client Testimony saved successfully.');

        return redirect(route('clientTestimonies.index'));
    }

    /**
     * Display the specified ClientTestimony.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $clientTestimony = $this->clientTestimonyRepository->find($id);

        if (empty($clientTestimony)) {
            Flash::error('Client Testimony not found');

            return redirect(route('clientTestimonies.index'));
        }

        return view('client_testimonies.show')->with('clientTestimony', $clientTestimony);
    }

    /**
     * Show the form for editing the specified ClientTestimony.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $clientTestimony = $this->clientTestimonyRepository->find($id);

        if (empty($clientTestimony)) {
            Flash::error('Client Testimony not found');

            return redirect(route('clientTestimonies.index'));
        }

        return view('client_testimonies.edit')->with('clientTestimony', $clientTestimony);
    }

    /**
     * Update the specified ClientTestimony in storage.
     *
     * @param int $id
     * @param UpdateClientTestimonyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClientTestimonyRequest $request)
    {
        $clientTestimony = $this->clientTestimonyRepository->find($id);

        if (empty($clientTestimony)) {
            Flash::error('Client Testimony not found');

            return redirect(route('clientTestimonies.index'));
        }

        $input = $request->all();

        if ($request->file('client_photo_url')) {
            $image = $request->file('client_photo_url');
            $pictures = [];
            if ($image != null) {    
                $productImage = Imgur::upload($image);
                $productImageLink = $productImage->link();
            }
        }else{
            $productImageLink = "";
        }

        $input['client_photo_url'] = $productImageLink;

        $clientTestimony = $this->clientTestimonyRepository->update($input, $id);

        Flash::success('Client Testimony updated successfully.');

        return redirect(route('clientTestimonies.index'));
    }

    /**
     * Remove the specified ClientTestimony from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $clientTestimony = $this->clientTestimonyRepository->find($id);

        if (empty($clientTestimony)) {
            Flash::error('Client Testimony not found');

            return redirect(route('clientTestimonies.index'));
        }

        $this->clientTestimonyRepository->delete($id);

        Flash::success('Client Testimony deleted successfully.');

        return redirect(route('clientTestimonies.index'));
    }
}
