<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCourseRequest;
use App\Http\Requests\UpdateCourseRequest;
use App\Repositories\CourseRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Mail;
use App\Mail\CourseConfirm;
use App\Mail\CourseDisabled;
use App\Models\Course;
use App\Models\Pack;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\CustomerNotifications;
use App\Utils\CustomerNotificationUtils;

class CourseController extends AppBaseController
{
    /** @var  CourseRepository */
    private $courseRepository;

    public function __construct(CourseRepository $courseRepo)
    {
        $this->courseRepository = $courseRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Course.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $courses = $this->courseRepository->all();
        $courses = Course::orderBy('id', 'desc')->paginate(5);

        return view('courses.index')
            ->with('courses', $courses);
    }

    /**
     * Show the form for creating a new Course.
     *
     * @return Response
     */
    public function create()
    {
        $packItems = Pack::all();
        return view('courses.create', compact('packItems'));
    }

    /**
     * Store a newly created Course in storage.
     *
     * @param CreateCourseRequest $request
     *
     * @return Response
     */
    public function store(CreateCourseRequest $request)
    {
        $input = $request->all();

        $course = $this->courseRepository->create($input);

        Flash::success('Course saved successfully.');

        return redirect(route('courses.index'));
    }

    /**
     * Display the specified Course.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $course = $this->courseRepository->find($id);

        if (empty($course)) {
            Flash::error('Course not found');

            return redirect(route('courses.index'));
        }

        return view('courses.show')->with('course', $course);
    }

    /**
     * Show the form for editing the specified Course.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $course = $this->courseRepository->find($id);

        if (empty($course)) {
            Flash::error('Course not found');

            return redirect(route('courses.index'));
        }

        return view('courses.edit')->with('course', $course);
    }

    /**
     * Update the specified Course in storage.
     *
     * @param int $id
     * @param UpdateCourseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCourseRequest $request)
    {
        $course = $this->courseRepository->find($id);

        if (empty($course)) {
            Flash::error('Course not found');

            return redirect(route('courses.index'));
        }

        $course = $this->courseRepository->update($request->all(), $id);

        Flash::success('Course updated successfully.');

        return redirect(route('courses.index'));
    }

    /**
     * Remove the specified Course from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $course = $this->courseRepository->find($id);

        if (empty($course)) {
            Flash::error('Course not found');

            return redirect(route('courses.index'));
        }

        $this->courseRepository->delete($id);

        Flash::success('Course deleted successfully.');

        return redirect(route('courses.index'));
    }

    public function changeStatus($id) {
        $course = $this->courseRepository->find($id);

        if (empty($course)) {
            Flash::error('Course not found');

            return redirect(route('courses.index'));
        }

        if ($course->active == 1) {
            $course->active = false;
            if(isset($course->coach) && isset($course->coach->customer)) {
              $data = [
                  'courseID' => $course->id,
                  'name' => $course->discipline->name,
                  'courseUrl' => env('APP_URL')."customer/coach/lessons"
              ];
                Mail::to($course->coach->customer->email)->send(new CourseDisabled($data));

                $customerNotification = new CustomerNotifications();
                $customerNotification->title  = "Cours désactivé";

                $customerNotification->title_en  = "Course disabled";

                $customerNotification->subtitle  = "Votre cours à été désactivé par un admin";
                $customerNotification->subtitle_en  = "Your course is disable by admin";
                $customerNotification->action  = "";
                $customerNotification->action_by  = "";
                $customerNotification->type_notification  = "confirmed";
                $customerNotification->is_read  = false;
                $customerNotification->is_received  = false;
                $customerNotification->user_id  = $course->coach->customer->id;
                $customerNotification->save();

                CustomerNotificationUtils::notify($customerNotification);
            }
        } else {
            $course->active = true;
            if(isset($course->coach) && isset($course->coach->customer)) {
              $data = [
                  'courseID' => $course->id,
                  'name' => $course->discipline->name,
                  'courseUrl' => env('APP_URL')."customer/coach/lessons"
              ];
              Mail::to($course->coach->customer->email)->send(new CourseConfirm($data));

                $customerNotification = new CustomerNotifications();
                $customerNotification->title  = "Cours Confirmer";

                $customerNotification->title_en  = "Course confirm";

                $customerNotification->subtitle  = "Votre cours à été confirmer par un admin";
                $customerNotification->subtitle_en  = "Your course is confirmed by admin";
                $customerNotification->action  = "";
                $customerNotification->action_by  = "";
                $customerNotification->type_notification  = "confirmed";
                $customerNotification->is_read  = false;
                $customerNotification->is_received  = false;
                $customerNotification->user_id  = $course->coach->customer->id;
                $customerNotification->save();

                CustomerNotificationUtils::notify($customerNotification);
            }
        }

        $course->save();

        Flash::success('Course updated successfully.');

        return redirect(route('courses.index'));
    }



}
