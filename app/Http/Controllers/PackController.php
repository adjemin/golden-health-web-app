<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePackRequest;
use App\Http\Requests\UpdatePackRequest;
use App\Repositories\PackRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PackController extends AppBaseController
{
    /** @var  PackRepository */
    private $packRepository;

    public function __construct(PackRepository $packRepo)
    {
        $this->packRepository = $packRepo;
    }

    /**
     * Display a listing of the Pack.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $packs = $this->packRepository->all();

        return view('packs.index')
            ->with('packs', $packs);
    }

    /**
     * Show the form for creating a new Pack.
     *
     * @return Response
     */
    public function create()
    {
        return view('packs.create');
    }

    /**
     * Store a newly created Pack in storage.
     *
     * @param CreatePackRequest $request
     *
     * @return Response
     */
    public function store(CreatePackRequest $request)
    {
        $input = $request->all();

        $pack = $this->packRepository->create($input);

        Flash::success('Pack saved successfully.');

        return redirect(route('packs.index'));
    }

    /**
     * Display the specified Pack.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pack = $this->packRepository->find($id);

        if (empty($pack)) {
            Flash::error('Pack not found');

            return redirect(route('packs.index'));
        }

        return view('packs.show')->with('pack', $pack);
    }

    /**
     * Show the form for editing the specified Pack.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pack = $this->packRepository->find($id);

        if (empty($pack)) {
            Flash::error('Pack not found');

            return redirect(route('packs.index'));
        }

        return view('packs.edit')->with('pack', $pack);
    }

    /**
     * Update the specified Pack in storage.
     *
     * @param int $id
     * @param UpdatePackRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePackRequest $request)
    {
        $pack = $this->packRepository->find($id);

        if (empty($pack)) {
            Flash::error('Pack not found');

            return redirect(route('packs.index'));
        }

        $pack = $this->packRepository->update($request->all(), $id);

        Flash::success('Pack updated successfully.');

        return redirect(route('packs.index'));
    }

    /**
     * Remove the specified Pack from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pack = $this->packRepository->find($id);

        if (empty($pack)) {
            Flash::error('Pack not found');

            return redirect(route('packs.index'));
        }

        $this->packRepository->delete($id);

        Flash::success('Pack deleted successfully.');

        return redirect(route('packs.index'));
    }
}
