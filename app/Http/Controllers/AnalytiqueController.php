<?php

namespace App\Http\Controllers;

// use Analytics;
// use Spatie\Analytics\Period;
// use Period;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\NavigatorSession;

class AnalytiqueController extends Controller
{
    public function index(Request $request){
        // dd($this->fetchTimeOnPages($request));
        return view('navigator.index');
    }
 

    //helper for get all user and pages views
    /**
     * @return DateTime $date
     * @return mixed $visitors
     * @return string $pageTitle 
     * @return mixed $pageViews
     */
    public function fetchVisitorsAndPageViews(Request $request){
        return $this->performUrlQuery($request);
    }

    //helper for get the total visitor and pages 
    /**
     * @return DateTime $date
     * @return int $visitors
     * @return int $pageViews
     */
    public function fetchTotalVisitorsAndPageViews(Request $request){
        return $this->performQuery($request);
    }
    

    /**
     * @return DateTime $url
     * @return string $pageTitle
     * @return string $pageViews
     */
    public function fetchMostVisitedPages(Request $request, int $maxResults=0){
        $result = $this->mostUrlQuery($request)->map(function($items, $key){
            $count=0;
            foreach ($items as $value) {
                $count += count($value['time']);
            }

            return [
                'url'   => $key,
                'numberView'  => $count
            ];
        });

        $sorted = $result->sortByDesc(function($result, $key){
            return $result['numberView'];
        });
        return ($maxResults != 0) ?  $sorted->take($maxResults) : $sorted;
    }

    public function fetchTimeOnPages(Request $request, int $maxResults=0){
        $result = $this->mostUrlQuery($request)->map(function($items, $key){
            $total= 0;
            foreach ($items as $value) {
                foreach($value['time'] as $time){
                    if(!is_null($time['end'])){
                        $total += Carbon::parse($time['end'])->diffInSeconds(Carbon::parse($time['start']));
                    }
                }
            }

            return [
                'url'   => $key,
                'timeConnection'  => $total
            ];
        });

        $sorted = $result->sortByDesc(function($result, $key){
            return $result['timeConnection'];
        })->reject(function($result){
            return $result['timeConnection'] == 0;
        });

        return ($maxResults != 0) ?  $sorted->take($maxResults) : $sorted;
    }
    /**
     * @return DateTime $url
     * @return string $pageViews
     */
    public function fetchTopReferrers(Request $request, int $maxResults=20){
        return $this->mostUrlQuery($request)->take($maxResults)->toArray();
    }

    /**
     * @return string $type 
     * @return string $sessions
     */
    public function fetchUserTypes(Request $request){
        return collect($this->performQuery($request)->toArray())->groupBy(
            function($item){
                if($item['is_visitor']){
                    return "Visitor";
                }

                if($item['is_challenger']){
                    return "Challenger";
                }

                if($item['is_coach']){
                    return 'Coach';
                }

                if($item['is_admin']){
                    return 'Admin';
                }
            }
        )->toArray();
    }

    /**
     * @return string $type 
     * @return string $sessions
     */
    public function fetchBrowsers(Request $request){
        return collect($this->performQuery($request)->toArray())->groupBy([
            'browser',
            'ip',
            function($item){
                if($item['is_visitor']){
                    return "Visitor";
                }

                if($item['is_challenger']){
                    return "Challenger";
                }

                if($item['is_coach']){
                    return 'Coach';
                }

                if($item['is_admin']){
                    return 'Admin';
                }
            }
        ])->toArray();
    }


    public function performQuery(Request $request){
        $startDate = $request->query('startDate') ?? $request->startDate;
        $endDate = $request->query('endDate') ?? $request->endDate;
        $method = $request->query('method') ?? $request->method;
        $value = $request->query('value') ?? $request->value;
        
        if(in_array($method, ['days', 'months', 'years'])){
            $endDate = Carbon::now();
            if(is_null($value)){
                if($method == 'years'){
                    $startDate = Carbon::today()->subYears(1)->startOfDay();
                }else if($method == 'months'){
                    $startDate = Carbon::today()->subMonths(1)->startOfDay();
                }else{
                    $startDate = Carbon::today()->startOfDay();
                }
            }else{
                if($method == 'years'){
                    $startDate = Carbon::today()->subYears($value)->startOfDay();
                }else if($method == 'months'){
                    $startDate = Carbon::today()->subMonths($value)->startOfDay();
                }else{
                    $startDate = Carbon::today()->subDays($value)->startOfDay();
                }
            }
        }else{
            $startDate = Carbon::parse($startDate);
            $endDate = Carbon::parse($endDate);
            // dd($startDate->format('Y-m-d'));
            if($startDate > $endDate){
                throw new Exception("Start date `{$startDate->format('Y-m-d')}` cannot be after end date `{$endDate->format('Y-m-d')}`.", 1);
            }
        }
        
        return NavigatorSession::orderBy('id', 'desc')->whereBetween('created_at', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])->get();
    }

    public function performUrlQuery(Request $request){
        return collect($this->performQuery($request)->toArray())->map(
            function($item){
                return json_decode($item["page_visited"], true);
            }
        )->collapse()->toArray();
    }

    public function mostUrlQuery(Request $request){
        return collect($this->performUrlQuery($request))->groupBy('page')->sortBy(function($pages, $key){
            return count($pages);
        })->reverse();
    }
}