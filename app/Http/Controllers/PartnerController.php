<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePartnerRequest;
use App\Http\Requests\UpdatePartnerRequest;
use App\Repositories\PartnerRepository;
use App\Models\CustomerNotifications;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Mail\PartnerWelcomeMail;
use App\Models\Partner;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Imgur;
use PDF;

class PartnerController extends AppBaseController
{
    /** @var  PartnerRepository */
    private $partnerRepository;

    public function __construct(PartnerRepository $partnerRepo)
    {
        $this->partnerRepository = $partnerRepo;
        //$this->middleware('auth');
    }

    /**
     * Display a listing of the Partner.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $partners = $this->partnerRepository->all();
        $partners = Partner::orderBy('id', 'desc')->paginate(10);

        return view('partners.index')
            ->with('partners', $partners);
    }

    /**
     * Show the form for creating a new Partner.
     *
     * @return Response
     */
    public function create()
    {
        return view('partners.create');
    }

    /**
     * Store a newly created Partner in storage.
     *
     * @param CreatePartnerRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $rules =  [
            'gender' => 'required',
            'name' => 'required|max:255',
            'phone_number' => 'required',
            'phone' => 'nullable',
            'type_account' => 'required',
            'email' => 'required|email|max:255|unique:partners',
            'is_cgu' => 'required',
            'g-recaptcha-response'=>'required',
            'facebook_url'    =>  'nullable|url',
            'instagram_url'    =>  'nullable|url',
            'youtube_url'    =>  'nullable|url',
            'site_web'    =>  'nullable|url',
            'size' => 'nullable|numeric',
            'lat' => 'nullable',
            'lon' => 'nullable',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect(url('/customer/register/partner'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $partner = Partner::create([
            'name'  => ucfirst(strtolower($request->name)),
            'email'  => strtolower($request->email),
            'dial_code'  => $request->dial_code,
            'phone_number'  => $request->phone_number,
            'phone' => $request->dial_code . $request->phone_number,
            'facebook_id'  => $request->facebook_url ?? null,
            'instagram'  => $request->instagram_url ?? null,
            'web_site'  => $request->site_web ?? null,
            'twitter_id'  => $request->twitter_id ?? null,
            'newsletter'  => $request->is_newsletter??0,
            'gender'  => strtoupper($request->gender),
            'description'  => $request->description ?? null,
            'service_type'  => $request->service_type,
            'discovery_source'  => $request->discovery_source,
            'service_type_other'    =>  $request->service_type_other
        ]);

        // notifying partner
        Mail::to($partner->email)->send(new PartnerWelcomeMail());
        session()->flash('success', "Candidature enregistrée avec success");
        session(['name' => $partner->name]);

        // notifying every admin
        $accountUri = 'partners';

        $email_data = [
            'accountName' => $request['name'],
            'accountType' => $request['type_account'],
            'accountUrl' => env('APP_URL') . "/$accountUri/$partner->id"
        ];
                
        $customerNotification = new CustomerNotifications();

        $customerNotification->title  = "Inscription";
        $customerNotification->title_en  = "Register";
        $customerNotification->subtitle  = "Vous venez de vous inscrire";
        $customerNotification->subtitle_en  = "You register yourself";
        $customerNotification->action  = "";
        $customerNotification->action_by  = "";
        $customerNotification->type_notification  = "waiting";
        $customerNotification->is_read  = false;
        $customerNotification->is_received  = false;
        $customerNotification->user_id  = $partner->id;
        $customerNotification->save();

        //notifying admins
        /*foreach (User::all() as $user) {
            Mail::to($user->email)->send(new AdminSignupNotificationMail($email_data));
        }*/
        //'/customer/register/partner'
        return view('customer_frontOffice.register.partner')->with('partner', $partner);
    }

    /**
     * Display the specified Partner.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $partner = $this->partnerRepository->find($id);

        if (empty($partner)) {
            Flash::error('Partner not found');

            return redirect(route('partners.index'));
        }

        return view('partners.show')->with('partner', $partner);
    }

    /**
     * Show the form for editing the specified Partner.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $partner = $this->partnerRepository->find($id);

        if (empty($partner)) {
            Flash::error('Partner not found');

            return redirect(route('partners.index'));
        }

        return view('partners.edit')->with('partner', $partner);
    }

    /**
     * Update the specified Partner in storage.
     *
     * @param int $id
     * @param UpdatePartnerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartnerRequest $request)
    {
        $partner = $this->partnerRepository->find($id);

        if (empty($partner)) {
            Flash::error('Partner not found');

            return redirect(route('partners.index'));
        }

        $productImageLink = null;

       
        if ($request->file('photo_url')) {

            $image = $request->file('photo_url');
            $pictures = [];
            if ($image != null) {    
                $productImage = Imgur::upload($image);
                $productImageLink = $productImage->link();
            }
        } else {
            $productImageLink = $partner->photo_url;
        }

        $input = $request->all();
        
        $input['phone'] = $input['dial_code'].' '.$input['phone_number'];
        $input['photo_url'] = $productImageLink;
        $input['phone'] = $input['dial_code'].' '.$input['phone_number'];
        $partner = $this->partnerRepository->update($input, $id);

        Flash::success('Partner updated successfully.');

        return redirect(route('partners.index'));
    }

    /**
     * Remove the specified Partner from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $partner = $this->partnerRepository->find($id);

        if (empty($partner)) {
            Flash::error('Partner not found');

            return redirect(route('partners.index'));
        }

        $this->partnerRepository->delete($id);

        Flash::success('Partner deleted successfully.');

        return redirect(route('partners.index'));
    }

    public function searchPartner(Request $request)
    {
        $partnersFound = Partner::where('name', 'like', '%' . $request->searchPartner . '%')
            ->orWhere('email', 'like', '%' . $request->searchPartner . '%')
            ->orWhere('phone', 'like', '%' . $request->searchPartner . '%')
            ->get();
        return json_encode(view('partners.table', ['partners' => $partnersFound])->render());
    }

    public function printAll(){
        $partners = Partner::all();
        return view('partners.table_pdf', compact('partners'));
        // $pdf = PDF::loadView('partners.table_pdf', compact('partners'));
        // return $pdf->stream();
    }
}
