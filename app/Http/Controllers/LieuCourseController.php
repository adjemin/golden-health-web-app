<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLieuCourseRequest;
use App\Http\Requests\UpdateLieuCourseRequest;
use App\Repositories\LieuCourseRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class LieuCourseController extends AppBaseController
{
    /** @var  LieuCourseRepository */
    private $lieuCourseRepository;

    public function __construct(LieuCourseRepository $lieuCourseRepo)
    {
        $this->lieuCourseRepository = $lieuCourseRepo;
    }

    /**
     * Display a listing of the LieuCourse.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $lieuCourses = $this->lieuCourseRepository->all();

        return view('lieu_courses.index')
            ->with('lieuCourses', $lieuCourses);
    }

    /**
     * Show the form for creating a new LieuCourse.
     *
     * @return Response
     */
    public function create()
    {
        return view('lieu_courses.create');
    }

    /**
     * Store a newly created LieuCourse in storage.
     *
     * @param CreateLieuCourseRequest $request
     *
     * @return Response
     */
    public function store(CreateLieuCourseRequest $request)
    {
        $input = $request->all();

        $lieuCourse = $this->lieuCourseRepository->create($input);

        Flash::success('Lieu Course saved successfully.');

        return redirect(route('lieuCourses.index'));
    }

    /**
     * Display the specified LieuCourse.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $lieuCourse = $this->lieuCourseRepository->find($id);

        if (empty($lieuCourse)) {
            Flash::error('Lieu Course not found');

            return redirect(route('lieuCourses.index'));
        }

        return view('lieu_courses.show')->with('lieuCourse', $lieuCourse);
    }

    /**
     * Show the form for editing the specified LieuCourse.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $lieuCourse = $this->lieuCourseRepository->find($id);

        if (empty($lieuCourse)) {
            Flash::error('Lieu Course not found');

            return redirect(route('lieuCourses.index'));
        }

        return view('lieu_courses.edit')->with('lieuCourse', $lieuCourse);
    }

    /**
     * Update the specified LieuCourse in storage.
     *
     * @param int $id
     * @param UpdateLieuCourseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLieuCourseRequest $request)
    {
        $lieuCourse = $this->lieuCourseRepository->find($id);

        if (empty($lieuCourse)) {
            Flash::error('Lieu Course not found');

            return redirect(route('lieuCourses.index'));
        }

        $lieuCourse = $this->lieuCourseRepository->update($request->all(), $id);

        Flash::success('Lieu Course updated successfully.');

        return redirect(route('lieuCourses.index'));
    }

    /**
     * Remove the specified LieuCourse from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $lieuCourse = $this->lieuCourseRepository->find($id);

        if (empty($lieuCourse)) {
            Flash::error('Lieu Course not found');

            return redirect(route('lieuCourses.index'));
        }

        $this->lieuCourseRepository->delete($id);

        Flash::success('Lieu Course deleted successfully.');

        return redirect(route('lieuCourses.index'));
    }
}
