<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
class CinetPayPaiementController extends Controller
{

    private $CINETPAY_APIKEY ;
    private $CINETPAY_SITE_ID ;
    private $SANDBOX;
    private $CINETPAY_URL_SIGNATURE;
    private $CINETPAY_URL_CHECKPAY_STATUT;
    private $CINETPAY_URL_TRANS_HISTORY;
    private $CINETPAY_URL_ACCOUNT_STATUT;

    public function __construct($SANDBOX =false )
    {
        $this->CINETPAY_APIKEY = env('CINETPAY_APIKEY');
        $this->CINETPAY_SITE_ID = env('CINETPAY_SITE_ID');
        $this->CINETPAY_URL_SIGNATURE ="https://api.cinetpay.com/v1/?method=getSignatureByPost";
        $this->CINETPAY_URL_CHECKPAY_STATUT ="https://api.cinetpay.com/v1/?method=checkPayStatus";
        $this->CINETPAY_URL_ACCOUNT_STATUT ="https://api.cinetpay.com/v1/?method=getCompteStatus";
        $this->CINETPAY_URL_TRANS_HISTORY ="https://api.cinetpay.com/v1/?method=getTransHistory";
    }

    private function postApi($option = [],$url_post)
    {
        //définition de la variable post à poster
        $data = [
            "cpm_site_id" =>  $this->CINETPAY_SITE_ID,
            "apikey" =>  $this->CINETPAY_APIKEY
        ];

        //fusion de tableau de données locales et optionnelles
        $data = array_merge($data,$option);

        //initialisation de la requète
        $curl = curl_init();

        curl_setopt_array($curl,[
            CURLOPT_URL => $url_post,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => http_build_query($data)
        ]);


        $responseArray = [];

        $response = curl_exec($curl);
        //on parse la chaine de caractère en tableau de données
        parse_str($response,$responseArray);

        if(isset($responseArray["L_SHORTMESSAGE0"])){
            echo "error function postApi() :  <br/>";
            var_dump($responseArray);
            die();
        }

        return $response;

    }

    public function getSignature(Request $request)
    {
        $option =
            [
                "cpm_amount" => $request->montant,
                "cpm_currency" => "CFA",
                "cpm_trans_id" => $this->CINETPAY_SITE_ID,
                "cpm_trans_date" => \date('mdYHis'),
                "cpm_payment_config" => "SINGLE",
                "cpm_page_action" => "PAYMENT",
                "cpm_designation" => $request->designation,
                "cpm_custom" => $request->custom
            ];

        $response = $this->postApi($option,$this->CINETPAY_URL_SIGNATURE);

        return $response;

    }

    /*******
     *
     */
    public function doPaiement(Request $request)
    {
        try
        {

            $paiement = \App\Paiement::where('trans_id_paiement',$request->cpm_trans_id)->first();

            $option =
                [
                    "cpm_trans_id" => $paiement->trans_id_paiement
                ];

            $response = json_decode($this->getPayStatus($option));


            if($response->transaction && $response->transaction->cpm_result == '00' && $response->transaction->cpm_trans_status == "ACCEPTED")
            {
                //on valide le payment

                $arrayAction = explode('#',$paiement->lib_paiement);
                $action = $arrayAction[0];
                $param = $arrayAction[1];

                if($paiement && $param)
                {

                    $booking = \App\Models\Order::where('id', $param)->first();

                    $user = \App\Customer::where('id', $booking->customer_id)->first();

                    $course = \App\Course::where('id', $booking->course_id)->first();

                    $ref_code = "C".$booking->id."-".rand();

                    $booking->booking_confirm = 1;
                    $booking->booking_ref= $ref_code;
                    $booking->booking_etape = 1;

                    $booking->save();

                    $paiement->statut_paiement = 1;
                    $paiement->save();

                    if($booking){
                        //Generate Invoice and save
                        $invoice = new \App\Invoice();
                        $invoice->reference = $ref_code;
                        $invoice->id_editor = $user->id;
                        $invoice->link = $this->generateInvoiceWithLink($booking, $ref_code);
                        $invoice->invoice_date = date('Y-m-d H:i:s');
                        $invoice->id_booking = $booking->id_booking;
                        $invoice->payment_method = 'cash';
                        $win_from_the_owner = $booking->duration *  $vehicule->original_price;
                        $customer_fees = $booking->price;
                        $invoice->win_from_the_owner = $win_from_the_owner;
                        $invoice->customer_fees = $customer_fees;
                        $invoice->win_of_ahoko = $customer_fees - $win_from_the_owner;
                        $invoice->customer_payed = false;
                        $invoice->payed_to_owner= false;
                        $invoice->booking_status = $booking->status;
                        $invoice->status = 2;
                        $invoice->save();
                    }


                    $total_day_booking = $this->dayCount($booking->start_date, $booking->end_date);


                    $data = array(
                        'nom' => $user->nom,
                        'prenom' => $user->prenom,
                        'ref_code' => $ref_code,
                        'vehicule' => null,
                        'number_date' => $total_day_booking,
                        'pricing' => $booking->price,
                        'invoice' =>$invoice->link
                    );

                    $user->notify(new EmailBooking($user, $data));


                }

            }

        }
        catch(\Exception $e)
        {
            throw new \Exception(['message' => $e->getMessage()]);
        }

    }


    public function getPayStatus($option)
    {
        try
        {
            return $this->postApi($option,$this->CINETPAY_URL_CHECKPAY_STATUT);
        }
        catch (Exception $e)
        {
            throw new \Exception(['message' => $e->getMessage()]);
        }
    }

    public function getAllPay()
    {
        try
        {
            return $this->postApi($this->CINETPAY_URL_TRANS_HISTORY);
        }
        catch (Exception $e)
        {
            throw new \Exception(['message' => $e->getMessage()]);
        }
    }

    public function getStatutAccount()
    {
        try
        {
            return $this->postApi($this->CINETPAY_URL_ACCOUNT_STATUT);
        }
        catch (Exception $e)
        {
            throw new \Exception(['message' => $e->getMessage()]);
        }
    }

    public function returnPaiement(Request $request)
    {

    $paiement = Paiement::where('trans_id_paiement',$request->cpm_trans_id)->first();

    $arrayAction = explode('#',$paiement->lib_paiement);
    $action = $arrayAction[0];
    $param = $arrayAction[1];
    //dd($request->input());

    if($paiement && $param)
    {

      $booking = \App\Booking::where('id', $param)->first();
        
      return redirect('/reserve/process/confirm');

    }
    else{
        return "paiement not identified";
    }
    }

    public function annulPaiement(Request $request)
    {
        //dd($request->session()->previousUrl());
        return redirect($request->session()->previousUrl());
    }
    
    function dayCount($from, $to) {
        $first_date = strtotime($from);
        $second_date = strtotime($to);
        $days_diff = $second_date - $first_date;
        return date('d',$days_diff);
    }


}
