<?php

namespace App\Http\Controllers;
use \DateTime;
use DB;
use Flash;
use App\FAQ;
use Response;
use App\CartItem;
use App\Customer;
use Carbon\Carbon;
use App\Entreprise;
use App\Models\Pack;
use App\Models\Post;
use App\Models\Coach;
use App\Models\Videotheque;

use App\Models\Event;
use App\ShoppingCart;
use App\Models\Advert;
use App\Models\Course;
use App\ContactRequest;
use App\Models\Partner;
use App\Models\Product;
use App\Models\Program;
use App\Models\Language;
use App\Models\CourseLieu;
use App\Models\Discipline;
use Darryldecode\Cart\Cart;
use App\Models\Availability;
use Illuminate\Http\Request;
use App\Mail\CoachWelcomeMail;
use App\Models\ClientTestimony;
use App\Models\CoachDiscipline;
use App\Models\ProductCategory;
use App\Mail\PartnerWelcomeMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Repositories\CoachRepository;
use Laracasts\Flash\Flash as FlashFlash;
use App\Http\Requests\CreateCoachRequest;
use App\Http\Requests\UpdateCoachRequest;
use App\Mail\AdminSignupNotificationMail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\AppBaseController;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\CoachDisciplineRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;

class CustomerFrontOfficeController extends AppBaseController
{

    /** @var  CoachRepository */
    private $coachRepository;

    /** @var  CoachDisciplineRepository */
    private $CoachDisciplineRepository;

    public function __construct(CoachRepository $coachRepo, CoachDisciplineRepository $CoachDisciplineRepo)
    {
        $this->coachRepository = $coachRepo;
        $this->CoachDisciplineRepository = $CoachDisciplineRepo;
    }

    public function index()
    {

        // // Creating session
        // $shopping_key = session()->get('shopping_key');

        // if(!$shopping_key){
        //     session()->put('shopping_key', Carbon::now()->toIso8601String());
        // }
        //
        $slides = \App\Models\Slide::where('page', 'home')->get();
        $all_disciplines = \App\Models\Discipline::all();
        $disciplines = \App\Models\Discipline::take(7)->get();
        $customer = Auth::guard('customer')->user();
        $priorityAd = Advert::where('file_type', 'image')->orderBy('priority', 'desc')->first();
        //
        $testimonies = ClientTestimony::take(7)->get();
        // $testimonies = ClientTestimony::where('client_rating', '>=', 4)->take(3)->get();
        return view('customer_frontOffice.index', compact('disciplines', 'customer', 'priorityAd', 'testimonies', 'slides', 'all_disciplines'));
    }
    // **** LEGAL STUFF
    public function legalPrivacy()
    {
        //
        return view('customer_frontOffice.legal.privacy');
    }
    public function legalTerms()
    {
        //
        return view('customer_frontOffice.legal.terms');
    }
    public function legalCookies()
    {
        //
        return view('customer_frontOffice.legal.cookies');
    }
    //
    public function beCoach()
    {

        $disciplines = \App\Models\Discipline::all();

        return view('customer_frontOffice.become_coach', compact('disciplines'));
    }

    function disciplines()
    {
        $disciplines = \App\Models\Discipline::all();

        return view('customer_frontOffice.discipline', compact('disciplines'));
    }

    public function beCoach_register(CreateCoachRequest $request)
    {

        $input = $request->all();

        $customer = null;

        if (Auth::guard('customer')->user() == null) {

            $validation = $this->validator($input);

            if ($validation->fails()) {
                return response()->json($validation->errors()->toArray());
            } else {

                $customer = \App\Customer::create(
                    [
                        'last_name' => ucfirst(strtolower($input['last_name'])),
                        'first_name' => ucfirst(strtolower($input['first_name'])),
                        'name' => ucfirst(strtolower($input['first_name'])) . " " . ucfirst(strtolower($input['last_name'])),
                        'gender' =>  strtoupper($input['gender']),
                        'phone_number' => $input['phone_number'],
                        'dial_code' => $input['dial_code'],
                        'phone' => $input['dial_code'] . $input['phone_number'],
                        'is_cgu' => $input['is_cgu'],
                        'is_newsletter' => (isset($input['is_newsletter']) ? $input['is_newsletter'] : 0),
                        'email' => $input['email'],
                        'discipline' => $input['discipline'],
                        'password' => bcrypt($input['password']),
                        'photo_url' => 'https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png',
                        'type_account' => 'coach',
                        'is_active' => false,
                        'birthday'  =>  $input['birthday'],
                        'commune'   =>  $input['commune'],
                        'quartier'   =>  $input['quartier'],
                        'discovery_source' => $input['discovery_source'],
                    ]
                );

                unset($input['quartier']);
                unset($input['commune']);
                unset($input['birthday']);

                Auth::guard('customer')->login($customer);
                $input['customer_id'] = $customer->id;
                $input['is_active'] = false;

                $coach = $this->coachRepository->create($input);

                Mail::to($customer->email)->send(new CoachWelcomeMail());

                // notifying every admin
                $accountUri = $customer->type_account == 'partner' ? 'partners' : ($customer->type_account == 'coach' ? 'coaches' : 'customers');

                $data = [
                    'accountName' => $customer->name,
                    'accountType' => $customer->type_account,
                    'accountUrl' => env('APP_URL') . "/$accountUri/$customer->id"
                ];

                // notifying admins
                foreach (\App\User::all() as $user) {
                    Mail::to($user->email)->send(new AdminSignupNotificationMail($data));
                }

                return $this->sendResponse($coach->toArray(), 'Coach saved successfully');
            }
        } else {

            return response()->json(["error" => "Error found"]);
        }
    }

    public function videothequeIndex()
    {
        //$videos = Videotheque::all();
        $videos = DB::table('videotheques')->get();
        return view('customer_frontOffice.videotheque.index', compact('videos'));
    }

    // **** Separate Register views for coach, partner, and self-challenger
    public function registerFormCoach()
    {
        $disciplines = \App\Models\Discipline::all();
        $langues = Language::all();
        return view('customer_frontOffice.register.coach', compact('disciplines', 'langues'));
    }
    public function registerFormSelfChallenger()
    {
        return view('customer_frontOffice.register.self_challenger');
    }
    public function registerFormPartner()
    {
        return view('customer_frontOffice.register.partner');
    }


    public function bePartner()
    {
        return view('customer_frontOffice.become_partner');
    }

    public function RegisterPartner(Request $request)
    {
        $input = $request->all();

        $validators = \Validator::make($request->all(), [
            'gender'    => 'required',
            'name'  =>  'required',
            'email' =>  'required|email',
            'service_type'  => 'required',
            'instagram_url' =>  'nullable|url',
            'facebook_url' =>  'nullable|url',
            'youtube_url' =>  'nullable|url',
            'site_web_partner' =>  'nullable|url',
        ]);

        if ($validators->fails()) {
            return response()->json($validators->errors()->toArray());
        }

        $partner = \App\Models\Partner::create(
            [
                'gender' => $input['gender'],
                'name' => $input['name'],
                'dial_code' => $input['dial_code'],
                'phone_number' => $input['phone_number'],
                'phone' =>  $input['dial_code'] . '' . $input['phone_number'],
                'service_type' => $input['service_type'],
                'service_type_other' => $input['service_type_other'],
                'facebook_id' => $input['facebook_url'],
                'instagram' => $input['instagram_url'],
                'youtube_id' => $input['youtube_url'],
                'website' => $input['site_web_partner'],
                'discovery_source' => $input['discovery_source'],
                'description' => $input['description'],
                'newsletter' => (isset($input['is_newsletter']) ? $input['is_newsletter'] : 0),
                'email' => $input['email']
            ]
        );

        Mail::to($partner->email)->send(new PartnerWelcomeMail());

        return $this->sendResponse($partner->toArray(), 'Partner saved successfully');
        // return redirect()->back()->with('message', 'Enregistrement effectué avec succès !');
    }

    public function allCoach(Request $request)
    {
        $users = $this->getUsers($request);
        $gender = $request->gender ?? $request->query('gender');
        $course_type = $request->course_type ?? $request->query('course_type');
        $course_lieu = $request->course_lieu ?? $request->query('course_lieu');
        $experience = $request->experience ?? $request->query('experience');
        $language = $request->language ?? $request->query('language');
        $available = $request->available ?? $request->query('available');
        $maxprice = $request->maxprice ?? $request->query('maxprice');

        // dd($language);
        if ($gender == 'mr') {
            $users = $users->where(function ($query) {
                $query->whereIn('gender', ['mr', 'MR', 'Mr', 'mR']);
            })->get();
            // dd($users);
            $users = collect($users)->filter(function ($user) {
                return in_array($user->gender, ['mr', 'MR', 'Mr', 'mR']);
            });
        } else if ($gender == 'mme') {
            $users = $users->where(function ($query) {
                $query->whereIn('gender', ['mme', 'mlle', 'MME', 'MLLE', 'MMME', 'mmme'])->get();
            })->get();
            // dd($users);
            $users = collect($users)->filter(function ($user, $key) {
                return in_array($user->gender, ['mme', 'mlle', 'MME', 'MLLE', 'MMME', 'mmme']);
            });
        } else if ($experience == '0-2') {
            $users = Customer::where('experience', $experience)->get();
            // $users = $users->where(function ($query) {
            //     $query->whereIn('experience', ['0-2', '02']);
            // })->get();
            // dd($users);
            $users = collect($users)->filter(function ($user) {
                return in_array($user->experience, ['0-2', '02']);
            });
        } else if ($experience == '2-7') {
            $users = Customer::where('experience', $experience)->get();
            // dd($users);
            $users = collect($users)->filter(function ($user) {
                return in_array($user->experience, ['2-7', '27']);
            });
        } else if ($experience == '7plus') {
            $users = Customer::where('experience', $experience)->get();
            // dd($users);
            $users = collect($users)->filter(function ($user) {
                return in_array($user->experience, ['7plus', '7+']);
            });
        } else if ($language == 'Français') {
            $users = Customer::where('language', $language)->get();
            // dd($users);
            $users = collect($users)->filter(function ($user) {
                return in_array($user->language, ['Français', 'Fr']);
            });
        } else if ($language == 'Anglais') {
            $users = Customer::where('language', $language)->get();
            // dd($users);
            $users = collect($users)->filter(function ($user) {
                return in_array($user->language, ['Anglais', 'En']);
            });
        } else if ($language == 'Espagnole') {
            $users = Customer::where('language', $language)->get();
            // dd($users);
            $users = collect($users)->filter(function ($user) {
                return in_array($user->language, ['Espagnole', 'En']);
            });
        } else if ($available == 'flexible') {
            $users = Customer::where('available', $available)->get();
            // dd($users);
            $users = collect($users)->filter(function ($user) {
                return in_array($user->available, ['flexible', 'flex']);
            });
        } else if ($available == 'as_soon_as_possible') {
            $users = Customer::where('available', $available)->get();
            // dd($users);
            $users = collect($users)->filter(function ($user) {
                return in_array($user->available, ['as_soon_as_possible', 'possible']);
            });
        } else if ($available == 'specific_date') {
            $users = Customer::where('available', $available)->get();
            // dd($users);
            $users = collect($users)->filter(function ($user) {
                return in_array($user->available, ['specific_date', 'possible']);
            });
        } else {
            $users = $users->get();
        }

        // if ($experience == '0-2') {
        //     $users = $users->where(function ($query) {
        //         $query->whereIn('experience', ['0-2', '02']);
        //     })->get();
        //     // dd($users);
        //     $users = collect($users)->filter(function ($user) {
        //         return in_array($user->experience, ['0-2', '02']);
        //     });
        // } else if ($experience == '2-7') {
        //     $users = $users->where(function ($query) {
        //         $query->whereIn('experience', ['2-7', '27']);
        //     })->get();
        //     // dd($users);
        //     $users = collect($users)->filter(function ($user) {
        //         return in_array($user->experience, ['2-7', '27']);
        //     });
        // } else if ($experience == '7+') {
        //     $users = $users->where(function ($query) {
        //         $query->whereIn('experience', ['7+', '7ans+']);
        //     })->get();
        //     // dd($users);
        //     $users = collect($users)->filter(function ($user) {
        //         return in_array($user->experience, ['7+', '7ans+']);
        //     });
        // } else {
        //     $users = $users->get();
        // }

        $ids = collect($users)->map(function ($user) {
            return $user->id;
        });
        // dd($users);

        $coaches = null;

        $discipline = $request->discipline ?? $request->query('discipline');

        $coaches = Coach::whereIn('customer_id', $ids)->where('is_active', 1)->get();
        $ids_coaches = collect($coaches)->map(function ($value) {
            return $value->id;
        });
        // dd($ids_coaches);
        $courses = null;

        if (!is_null($language)) {
            $customers =  Customer::where('language', $language)->get();

            // dd($customers);

            $coaches = [];

            foreach ($customers as $customer) {
                array_push($coaches, $customer->is_coach);
            }

            $coaches_id = [];

            foreach ($coaches as $key => $coach) {

                if ($coach != null) {

                    // dd($coach);
                    array_push($coaches_id, $coach->id);
                }
            }
            $courses = Course::whereIn('coach_id', $coaches_id);

            // dd($courses);
        }

        if (!is_null($experience)) {
            $customers =  Customer::where('experience', $experience)->get();

            $coaches = [];

            foreach ($customers as $customer) {
                array_push($coaches, $customer->is_coach);
            }

            $coaches_id = [];

            foreach ($coaches as $key => $coach) {

                if ($coach != null) {
                    // dd($coach);
                    array_push($coaches_id, $coach->id);
                }
            }

            $courses = Course::whereIn('coach_id', $coaches_id);
        }

        if (!is_null($available)) {
            $customers =  Customer::where('available', $available)->get();


            $coaches = [];

            foreach ($customers as $customer) {
                array_push($coaches, $customer->is_coach);
            }

            $coaches_id = [];

            foreach ($coaches as $key => $coach) {

                if ($coach != null) {
                    // dd($coach);
                    array_push($coaches_id, $coach->id);
                }
            }

            $courses = Course::whereIn('coach_id', $coaches_id);
        }


        if (!empty($discipline)) {


            $id_discipline = explode(',', $discipline);
            if (count($id_discipline) > 1) {
                $ids_disciplines = [];
                foreach ($id_discipline as $id) {
                    array_push($ids_disciplines, base64_decode($id));
                }
                // dd($ids_disciplines);
                $courses = Course::whereIn('discipline_id', $ids_disciplines);
            } else {
                $courses = Course::where('discipline_id', base64_decode($discipline));
            }
            // dd($ids_coaches);
            if (!is_null($course_type)) {
                $courses = $courses->whereIn('coach_id', $ids_coaches)->where('active', 1)->where('course_type', $course_type)->orderBy('id', 'desc')->get();
            } else {
                $courses = $courses->whereIn('coach_id', $ids_coaches)->where('active', 1)->orderBy('id', 'desc')->get();
            }

            if (!is_null($course_lieu)) {
                $courses = collect($courses)->filter(function ($course) use ($course_lieu) {
                    $lieux = $course->lieu()->get();
                    foreach ($lieux as $lieu) {
                        if ($lieu->count() > 0) {
                            $lieu_course = $lieu->lieuCourse;
                            if (!is_null($lieu_course)) {
                                if ($lieu_course->first()->slug == $course_lieu) {
                                    return true;
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    }
                    return false;
                });
            }
        } else {
            // $courses = Course::where('discipline_id', base64_decode($request->discipline))
            if (!is_null($course_type)) {
                $courses = Course::whereIn('coach_id', $ids_coaches)->where('course_type', $course_type)->where('active', 1)->orderBy('id', 'desc')->get();
            } else {
                $courses = Course::whereIn('coach_id', $ids_coaches)->where('active', 1)->orderBy('id', 'desc')->get();
            }

            if (!is_null($language)) {
                $customers =  Customer::where('language', $language)->get();

                // dd($customers);

                $coaches = [];

                foreach ($customers as $customer) {
                    array_push($coaches, $customer->is_coach);
                }

                $coaches_id = [];

                foreach ($coaches as $coach) {

                    // if ($coach != null) {
                    //     array_push($coaches_id, $coach->id);
                    // }

                    if (!empty($coach) || !is_null($coach)) {
                        array_push($coaches_id, $coach->id);
                    }
                }
                $courses = Course::whereIn('coach_id', $coaches_id);

                // dd($courses);
            }

            if (!is_null($experience)) {
                $customers =  Customer::where('experience', $experience)->get();

                $coaches = [];

                foreach ($customers as $customer) {
                    array_push($coaches, $customer->is_coach);
                }

                $coaches_id = [];
                foreach ($coaches as $coach) {

                    if (!empty($coach) || !is_null($coach)) {
                        array_push($coaches_id, $coach->id);
                    }
                }

                $courses = Course::whereIn('coach_id', $coaches_id);
            }

            if (!is_null($course_lieu)) {
                $courses = collect($courses)->filter(function ($course) use ($course_lieu) {
                    $lieux = $course->lieu()->get();
                    foreach ($lieux as $lieu) {
                        if ($lieu->count() > 0) {
                            $lieu_course = $lieu->lieuCourse;
                            if (!is_null($lieu_course)) {
                                if ($lieu_course->first()->slug == $course_lieu) {
                                    return true;
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    }
                    return false;
                });
            }
        }

        if($maxprice != null ){
            $courses = $courses->where('price', '<=', $maxprice);
        }

        $disciplines = \App\Models\Discipline::all();
        $courseTypes = \App\Models\CourseType::all();
        $course_lieus = \App\Models\LieuCourse::all();
        $all_disciplines = \App\Models\Discipline::all();
        $languages = \App\Models\Language::all();


        return view('customer_frontOffice.coachs', compact('courses', 'coaches', 'disciplines', 'courseTypes', 'all_disciplines', 'course_lieus', 'languages', 'maxprice'));
    }

    public function filterCoach(Request $request, Customer $customer)
    {

        $filters = $request->get('filters') ?? [];
        $sort = $request->get('sort');
        $coachs = DB::table('coaches')
            ->where('coaches.is_active', 1)
            ->join('customers', 'coaches.customer_id', '=', 'customers.id')
            ->join('courses', 'coaches.id', '=', 'courses.coach_id')
            ->leftJoin('disciplines', 'courses.discipline_id', '=', 'disciplines.id');

        if (array_key_exists('discipline', $filters) || array_key_exists('genrer', $filters)) {
            if (array_key_exists('genrer', $filters)) {
                $coachs->where(function ($query) use ($filters) {
                    if ($filters && isset($filters['genrer'])) {
                        foreach ($filters['genrer'] as $key => $filter) {
                            $query->where('customers.gender', '=', $filter);
                        }
                    }
                });
            }

            if (array_key_exists('discipline', $filters)) {
                $coachs->where(function ($query) use ($filters) {
                    if ($filters && isset($filters['discipline'])) {
                        foreach ($filters['discipline'] as $key => $filter) {
                            $query->where('courses.discipline_id', '=', $filter);
                        }
                    }
                });
            }
        }

        $coachs->select('coaches.*', 'customers.*', 'courses.id as course_id', 'courses.price as course_price', 'disciplines.name as discipline_name')
            ->get();
        // $data = collect($coachs)->map(function($coach){ return (array) $coach; })->toArray();
        return $this->sendResponse($coachs, 'Data successfully');
    }

    public function showCoachProfil($slug)
    {
        $course_id = base64_decode($slug);
        $course = \App\Models\Course::where('id', $course_id)->first();

        if (is_null($course)) {
            return redirect()->back();
        }

        $coach = $this->coachRepository->find($course->coach_id);

        $elements = \App\Models\Availability::where([
            'coach_id' => $coach->id,
            'course_id' => $course->id,
            'is_active' => 1
        ])->orderBy('start_time', 'asc')->get();

        // dd($elements);

        foreach ($elements as $item) {
            if (\Carbon\Carbon::today()->greaterThan(\Carbon\Carbon::parse($item->date_fin))) {
                $item->is_active = 0;
                $item->save();
            }
        }
        // dd($elements);

        // $items = \App\Models\Availability::where([
        //     'coach_id' => $coach->id,
        //     'course_id' => $course->id,
        //     'is_active' => 1
        // ])->get();
        $items = $course->availabilities;
        // dd($items);


        // ->whereBetween('date_fin', [\Carbon\Carbon::now()->format('Y-m-d'), \Carbon\Carbon::now()->addDays(7)->format('Y-m-d') ] )
        // ->get();

        // dd($items);

        //$availabilities = collect($elements)->sortBy('start_time')->groupBy('day')->toArray();
        /*
        $availabilities = [];
        $availArray = [];
        $slotsArray = [];

        foreach ($items as $key => $value) {
          $start = $value->date_debut." ".$value->start_time;
          $end = $value->date_fin." ".$value->end_time;
          $date_start = strtotime($start);
          $date_end = strtotime($end);
          $my_data = [
            'id' => $value->id,
            'title' =>'Cours '.$value->course->discipline->name,
            'start' => date('Y-m-d H:i:s', $date_start),
            'end' => date('Y-m-d H:i:s', $date_end),
          ];
          array_push($availabilities, $my_data);
        }
        */
        $availabilities = new Collection([]);
        $availArray = new Collection([]);
        // $slotsArray = [];

        foreach ($items as $key => $value) {
            $start_datetime = $value->date_debut . " " . $value->start_time;
            $end_datetime = $value->date_fin . " " . $value->end_time;
            //   dd(['end_datetime' => $end_datetime, 'start_datetime' => $start_datetime]);

            $slots = $this->getTimeSlot(60, $start_datetime, $end_datetime);
            //   dd(['slots' => $slots]);

            if (count($slots) > 0) {
                $title = 'Cours ' . str_replace("'", " ", $value->course->discipline->name) ?? '';
                $id = $value->id;
                $periods = $this->getDateTimeSlotV2($slots, $value->date_debut, $value->date_fin, $value->start_time, $value->end_time, $title, $id);
                // array_push($availArray, $periods);
                $availArray->push($periods);
            }
        }

        // dd($availArray);

        $availArray->map(function ($item) use ($availabilities) {
            foreach ($item as $va) {
                $availabilities->push($va);
            }
        });
        // foreach($availArray as $value) {
        //   foreach ($value as $va) {
        //     $my_data = [
        //       'id' => $va['id'],
        //       'title' => $va['title'],
        //       'start' => $va['start'],
        //       'end' => $va['end']
        //     ];
        //     array_push($availabilities,$va);

        //   }
        // }

        // dd($availabilities);
        $packs = \App\Models\Pack::all();
        $all_disciplines = Discipline::all();
        $portfolios = \App\Models\Portfolio::where('coach_id', 3000)->get();

        $lieus = \App\Models\CourseLieu::where('course_id', $course->id)->get();
        $course_type = \App\Models\CourseType::all();
        $reviews = \App\Models\Review::where('source_id', $course->id)->where('source', 'course')->get();

        return view('customer_frontOffice.coach_profil', compact('coach', 'all_disciplines', 'course', 'packs', 'lieus', 'availabilities', 'course_type', 'reviews', 'portfolios'));
    }

    public function getTimeSlot($sometimeOut, $start, $end)
    {
        $time =  new Collection([]);
        $start = new \DateTime($start);
        $end = new \DateTime($end);
        $BeginTimeStemp = $start->format('H:i'); // Get time Format in Hour and minutes
        $lastTimeStemp = $end->format('H:i');
        // $i=0;
        while (strtotime($BeginTimeStemp) <= strtotime($lastTimeStemp)) {
            $start = $BeginTimeStemp;
            $end = date('H:i', strtotime('+' . $sometimeOut . ' minutes', strtotime($BeginTimeStemp)));
            $BeginTimeStemp = date('H:i', strtotime('+' . $sometimeOut . ' minutes', strtotime($BeginTimeStemp)));
            // $i++;
            if (strtotime($BeginTimeStemp) <= strtotime($lastTimeStemp)) {
                $time->push([
                    'start' =>  $start,
                    'end'   => $end
                ]);
                // $time[$i]['start'] = $start;
                // $time[$i]['end'] = $end;
            }
        }
        return $time;
    }

    public function getDateTimeSlotV2($slots, $start_date, $end_date, $start_time, $end_time, $title, $id)
    {
        $start_date_stamp = strtotime($start_date);
        $end_date_stamp = strtotime($end_date);
        $startdate_timestamp = strtotime($start_date . ' ' . $start_time);
        $enddate_timestamp = strtotime($end_date . ' ' . $end_time);
        // [/helper]
        $today = strtotime(date("Y-m-d"));
        $cur_date_stamp = $start_date_stamp;
        $result = array();
        if ($enddate_timestamp >= $startdate_timestamp && $enddate_timestamp >= $cur_date_stamp) {
            while ($cur_date_stamp <= $end_date_stamp) {
                if ($cur_date_stamp >= $today) {
                    foreach ($slots as $key => $slval) {
                        $date_start = strtotime(date('Y-m-d', $cur_date_stamp) . " " . $slval['start']);
                        $date_end = strtotime(date('Y-m-d', $cur_date_stamp) . " " . $slval['end']);
                        $datetimeNow = strtotime(date("Y-m-d H:i"));
                        if ($date_start >= $datetimeNow) {
                            $result[] = array(
                                'id' => $id,
                                'title' => $title,
                                'start' => date('Y-m-d H:i:s', $date_start),
                                'end' => date('Y-m-d H:i:s', $date_end)
                            );
                        }
                    }
                }
                $cur_date_stamp = strtotime('+1 day', $cur_date_stamp);
            }
        }
        return $result;
    }

    public function showReservation($id, Request $request)
    {
        $course_id = base64_decode($id);
        $pack_id =  base64_decode($request->get('pack'));
        $course = \App\Models\Course::where('id', $course_id)->first();
        $pack = \App\Models\Pack::where('id', $pack_id)->first();
        $availability = \App\Models\Availability::where('id', (base64_decode($request->get('slot'))))->first();
        // if (!$course || $request->get('date') == null || $request->get('time') == null)  {
        // dd($availability);
        if (empty($course) || empty($availability)) {
            return redirect()->back();
        }

        // ########## START FORMAT DATE START AND END COURSE ########## //
        $start = $request->get('start');
        $end = $request->get('end');
        //dd(strtotime($start));
        $start = explode(' ', $start);
        $end = explode(' ', $end);
        //dd($start);
        $startdate = $start['0'].' '.$start['1'].' '.$start['2'].' '.$start['3'].' '.$start['4'];
        $enddate = $end['0'].' '.$end['1'].' '.$end['2'].' '.$end['3'].' '.$end['4'];
        //$startdate = date("Y m d", strtotime($startdate));
        $mystartdate =date("Y-m-d ",strtotime($startdate));
        $mystartdate = $mystartdate.' '.$start['4'];

        $myenddate =date("Y-m-d ",strtotime($enddate));
        $myenddate = $myenddate.' '.$end['4'];
        //dd($time);
        $dates = [];
        $dates[] = $start = new DateTime($mystartdate);
        $dates[] = $end = new DateTime($myenddate);

        // ########## END FORMAT DATE START AND END COURSE ########## //


        //$format=   Carbon::createFromFormat('Y-m-d H', $start)->toDateTimeString();
        //$format = \Carbon\Carbon::parse($start)->format('j F, Y');

        session(['start' => $start]);
        session(['end' => $end]);

        $coupons = $request->session()->get('coupons') ?? [];
        $coach = $this->coachRepository->find($course->coach_id);
        $packs = \App\Models\Pack::all();
        $nbperson = $request->get('nbperson');
        $lieus = \App\Models\CourseLieu::where('course_id', $course->id)->get();

        return view('customer_frontOffice.reserver', compact('coach', 'course', 'pack', 'packs', 'nbperson', 'lieus', 'availability', 'coupons', 'dates'));
    }

    public function cartReservation(Request $request)
    {
        $pack = \App\Models\Pack::where('id', $request->pack)->first();
        $course = \App\Models\Course::where('id', $request->course_id)->first();

        if (is_null($pack) || is_null($course)) {
            return $this->sendError("Veuillez vous connectez ou vous participez déjà à cet évènement");
        }

        $discount = (($course->price * $pack->percentage_discount) / 100);
        $price = ($course->price - $discount);
        $total_price = $price * $pack->nb_lesson;

        if ($request->session()->has('coupons')) {
            foreach ($request->session()->get('coupons') as $coupon) {
                $total_price *= (1 - ((int) $coupon->pourcentage / 100));
            }
        }


        $stringDateStart = strstr($request->start, " GMT", true);
        $date_start = Carbon::parse($stringDateStart, 'UTC');

        $stringDateEnd = strstr($request->end, " GMT", true);
        $date_end = Carbon::parse($stringDateEnd, 'UTC');


        $data = array(
            'availability_id' => $request->availability_id,
            'course_id' => $course->id,
            'start' => $date_start->format('Y-m-d h:i' ),
            'end' => $date_end->format('Y-m-d h:i' ),
            'type_lieu' => $request->select_delivery,
            'pack' => $pack,
            'address_autocomplete' => $request->address_autocomplete,
            'address_other_new' => $request->address_other_new,
            'message' => $request->message,
            'nbperson' => $request->nbperson,
            'nb_lesson' => $pack->nb_lesson,
            'price' => $price,
            'totalprice' => $total_price
        );
        if($request->session()->has('booking')){
            $request->session()->forget('booking');
            $request->session()->put('booking', $data);
        }else{
            $request->session()->put('booking', $data);
        }

        return $this->sendResponse($data, 'Booking saved successfully');
    }

    public function FinalBooking(Request $request)
    {

        $coupons = $request->session()->get('coupons') ?? [];

        if (!$request->session()->has('booking')) {
            return redirect()->back();
        }
        $booking = $request->session()->get('booking');
        $booking["start"] = $request->session()->get('start');
        $booking["end"] = $request->session()->get('end');
        $request->session()->forget('booking');
        $request->session()->put('booking', $booking);

        $start = $request->session()->get('start');
        $end = $request->session()->get('end');
        $course = \App\Models\Course::where('id', $booking['course_id'])->first();
        $discipline = \App\Models\Discipline::where('id', $course->discipline_id)->first();
        $coach = $this->coachRepository->find($course->coach_id);
        // $order = $booking->order;
        return view('customer_frontOffice.checkout_booking', compact('course', 'discipline', 'coach', 'booking', 'coupons', 'start', 'end'));
    }
    // *** Logs the user in then redirects him back from where he came
    public function loginAndRedirectTo(Request $request)
    {

        session()->put('redirectTo', $request['url']);

        return redirect('customer/login');
    }

    public function showConnexion()
    {
        return view('customer_frontOffice.connexion');
    }

    public function showBlog(Request $request)
    {
        if ($request->filled('categorie')) {
            $categorie = \App\Models\Category::where('name', $request->query('categorie'))->first();
            $posts = Post::where('category_id', $categorie->id)->where(['status' => 'published'])->orderBy('id', 'desc')->paginate(24);
            $recentPosts = Post::where('category_id', $categorie->id)->where('created_at', '>', Carbon::now()->subDays(5))->where(['status' => 'published'])->take(3);
            $oldPosts = Post::where('category_id', $categorie->id)->where('created_at', '<', Carbon::now()->subDays(5))->where(['status' => 'published'])->take(3);
        } else {
            $posts = Post::where(['status' => 'published'])->orderBy('id', 'desc')->paginate(24);
            $recentPosts = Post::all()->where('created_at', '>', Carbon::now()->subDays(5))->where(['status' => 'published'])->take(3);
            $oldPosts = Post::all()->where('created_at', '<', Carbon::now()->subDays(5))->where(['status' => 'published'])->take(3);
        }

        return view('customer_frontOffice.blog', compact('posts', 'recentPosts', 'oldPosts'));
    }

    public function showBlogArticle($slug)
    {
        $post = Post::where('slug', '=', $slug)->firstOrfail();
        $recentPosts = Post::all()->where('created_at', '>', Carbon::now()->subDays(30))->where(['status' => 'published'])->take(3);
        $oldPosts = Post::all()->where('created_at', '<', Carbon::now()->subDays(5))->where(['status' => 'published'])->take(3);
        $posts = Post::all();
        $post_grouped = collect($posts->toArray())->groupBy('category_id');
        $categories = \App\Models\Category::all();

        return view('customer_frontOffice.blog-article', compact('post', 'recentPosts', 'oldPosts', 'post_grouped', 'categories'));
    }

    public function showSelfChallenger()
    {
        return view('customer_frontOffice.self-challenger');
    }


    public function showCoachDescription()
    {
        return view('customer_frontOffice.coach-description');
    }

    public function showProgrammes()
    {
        $programs = Program::orderBy('id', 'desc')->paginate(10);
        return view('customer_frontOffice.programmes.index', compact('programs'));
    }

    public function showProgramme($slug)
    {
        $program = Program::where(['slug' => $slug])->first();
        return view('customer_frontOffice.programmes.show', compact('program'));
    }


    public function showQuiSommesNous()
    {
        // ***
        $entreprise = Entreprise::first();
        if (!$entreprise) {
            return view('errors.404');
        }
        return view('customer_frontOffice.qui-sommes-nous', compact('entreprise'));
    }


    public function showContact()
    {
        return view('customer_frontOffice.contact');
    }


    public function showFaq()
    {
        $faqs = FAQ::orderBy('rank', 'desc')->get();
        return view('customer_frontOffice.faq', compact('faqs'));
    }


    public function showRegisterCourse()
    {
        return view('customer_frontOffice.register-course');
    }


    public function showInscription()
    {
        return view('customer_frontOffice.inscription');
    }


    public function showDashboard()
    {
        return view('customer_frontOffice.dashboard');
    }

    public function allEvent()
    {
        $slides = \App\Models\Slide::where('page', 'event')->get();
        $events = \App\Models\Event::orderBy('date_event', 'desc')->get();
        $past_events = Event::where('date_event', '<', today())->orderBy('date_event', 'desc')->get();
        $upcoming_events = Event::where('date_event', '>=', today())->orderBy('date_event', 'desc')->get();
        // dd($upcoming_events);
        return view('customer_frontOffice.events.index', compact('events', 'past_events', 'upcoming_events', 'slides'));
    }

    public function showEvent($slug)
    {
        $event = Event::where('slug', $slug)->first();
        if (!$event) {
            return view('errors.404');
        }

        // $is_current_customer_participating = false;
        $customer = Auth::guard('customer')->user();
        $ticket = null;
        if ($customer) {
            $ticket = $customer->tickets->where('event_id', $event->id)->first();
        }
        // $firstLine = $this->first_sentence($event->description);

        return view('customer_frontOffice.events.show', compact('event', 'ticket'));
    }


    public function reserver()
    {
        return view('customer_frontOffice.reserver');
    }

    public function profile()
    {
        return view('customer_frontOffice.profile.index');
    }


    /*** Shop ***/
    public function showBasket()
    {
        // $shoppingCart = null;
        $customer = Auth::guard('customer')->user();
        $shopping_key = session()->get('shopping_key');

        if (!$customer) {
            if (!$shopping_key) {
                $shopping_key = session()->put('shopping_key', Carbon::now()->toIso8601String());
            }
            $shoppingCart = ShoppingCart::where('key', $shopping_key)->first();

            if (!$shoppingCart) {
                $shoppingCart = new ShoppingCart();
                $shoppingCart->key = $shopping_key;
                $shoppingCart->save();
            }
        }

        if ($customer && $customer->is_active == true) {
            if (!$shopping_key) {
                $shopping_key = session()->put('shopping_key', $customer->id);
                //
            }
            $shoppingCart = $customer->shoppingCart;
            if (!$shoppingCart) {
                $shoppingCart = new ShoppingCart();
                $shoppingCart->key = $customer->id;
                $shoppingCart->customer_id = $customer->id;
                $shoppingCart->save();
            }
        }

        $cartItems = $shoppingCart->items;
        return view('customer_frontOffice.shops.basket', compact('cartItems', 'shoppingCart'));
    }

    public function addBasket($prodId)
    {

        $product = Product::find($prodId);
        if (!$product) {
            //
            return view('customer_frontOffice.shops.basket');
        }
        $shoppingCart = ShoppingCart::ShoppingCart();

        $cartItem = $shoppingCart->items->where('product_id', $product->id)->first();
        //
        if (!$cartItem) {
            $cartItem = new CartItem();
            $cartItem->shopping_cart_id = $shoppingCart->id;
            $cartItem->product_id = $product->id;
            $cartItem->price = $product->getPrice();
            // $itemCount = 1;
        }
        $cartItem->quantity = 1;
        $cartItem->amount = $product->getPrice() * 1;
        $cartItem->save();
        //
        return view('customer_frontOffice.shops.basket');
    }

    public function showCheckout(Request $request)
    {
        $customer = Auth::guard('customer')->user();
        $shoppingCart = ShoppingCart::ShoppingCart();

        $coupons = $request->session()->get('coupons') ?? [];

        if ($shoppingCart->items->count() < 1) {
            return redirect()->route('shop.basket');
        }

        $cartItems = $shoppingCart->items;

        return view('customer_frontOffice.shops.checkout_new', compact('cartItems', 'shoppingCart', 'coupons'));
    }

    public function filterShop(Request $request)
    {
        //dd($request->json()->all());
        // $input = $request->all();
        //dd($input['categories']);
        $shopping_key = session()->get('shopping_key');
        $categories = \App\Models\Category::all();
        if (!$request->filled('categories')) {
            $products = \App\Models\Product::latest()->paginate(5);
            return response()->json([
                'data'  =>  view('customer_frontOffice.shops.components.products', compact('products'))->render()
            ]);
        }

        /////////////////// FILTER //////////////
        $categorie_filter = \App\Models\Category::whereIn('name', $request->query('categories'))->get();
        $id_category = collect($categorie_filter)->map(function ($category) {
            return $category->id;
        });
        $productCategories = \App\Models\ProductCategory::whereIn('category_id', $id_category)->get();
        $id_productCategory = collect($productCategories)->map(function ($pc) {
            return $pc->product_id;
        });

        $products = Product::whereIn('id', $id_productCategory)->paginate(5);

        return response()->json([
            'data'  =>  view('customer_frontOffice.shops.components.products', compact('products', 'categories', 'shopping_key'))->render()
        ]);
    }

    public function indexShop(Request $request)
    {
        // Starting shopping session
        // Creating session
        $shoppingCart = ShoppingCart::ShoppingCart();
        $ads = Advert::orderBy('priority', 'desc')->take(3)->get();

        $categories = \App\Models\Category::all();
        $gender = $request->gender ?? $request->query('gender');
        $max_price = $request->query('max-price');

        if ($request->filled('categories')) {
            $categories_arr = explode(',', $request->query('categories'));
            $categorie_filter = \App\Models\Category::whereIn('name', $categories_arr)->get();

            $id_category = collect($categorie_filter)->filter(function ($category, $key) use ($categories_arr) {
                return in_array($category->name, $categories_arr);
            })->map(function ($category) {
                return $category->id;
            })->toArray();

            $productCategories = \App\Models\ProductCategory::whereIn('category_id', $id_category)->get();

            $id_productCategory = collect($productCategories)->filter(function ($value, $key) use ($id_category) {
                return in_array($value->category_id, $id_category);
            })->map(function ($pc) {
                return $pc->product_id;
            })->toArray();

            $products = Product::whereIn('id', $id_productCategory)->orWhere('initial_count', '>', 0)->orWhere('stock', '>', 0)->orWhere('sold_at', '>', \Carbon\Carbon::today()->format('Y-m-d'));
        } else {
            $products = Product::latest()->orWhere('initial_count', '>', 0)->orWhere('stock', '>', 0)->orWhere('sold_at', '>', \Carbon\Carbon::today()->format('Y-m-d'));
        }

        if (!is_null($gender)) {
            $products = $products->where('gender', $gender);
        }
        if(!is_null($max_price)){
            $products = $products->where('original_price', '<=', $max_price);
        }

        $products = $products->paginate(6);
        if (isset($id_productCategory)) {
            $products = $products->filter(function ($product) use ($id_productCategory) {
                return in_array($product->id, $id_productCategory);
            })->paginate(6);
            // $products->setItems($filteredProducts);
        }
        return view('customer_frontOffice.shops.index', compact('products', 'shoppingCart', 'categories', 'ads', 'max_price'));
    }

    public function productDetailsShop($slug)
    {

        $product = Product::where('slug', $slug)->first();
        if (!$product) {
            return view('errors.404');
        }

        return view('customer_frontOffice.shops.product_details', compact('product'));
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'gender' => 'required',
            'last_name' => 'required|max:255',
            'first_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:customers',
            'password' => 'required|min:6|confirmed',
            'is_cgu' => 'required',
        ]);
    }

    public function getUsers(Request $request)
    {
        $lat = $request->lat ?? $request->query('lat');
        $lon = $request->lon ?? $request->query('lon');
        $city = $request->city ?? $request->query('city');
        $gender = $request->gender ?? $request->query('gender');
        $experience = $request->experience ?? $request->query('experience');

        if (is_null($lat) || is_null($lon)) {
            if (!is_null($city)) {
                if ($gender == 'mr') {
                    $response = \App\Models\Customer::whereIn('gender', ['mr', 'MR', 'Mr', 'mR'])->where('type_account', 'coach')->where('is_active', 1)->where('commune', 'like', '%' . $city . '%');
                } else if ($gender == 'mme') {
                    $response = \App\Models\Customer::whereIn('gender', ['mme', 'mlle', 'MME', 'MLLE', 'MMME', 'mmme'])->where('type_account', 'coach')->where('is_active', 1)->where('commune', 'like', '%' . $city . '%');
                } else {
                    $response = \App\Models\Customer::where('type_account', 'coach')->where('is_active', 1)->where('commune', 'like', '%' . $city . '%');
                }

                if ($experience == '0-2') {
                    $response = \App\Models\Customer::whereIn('experience', ['0-2', '02'])->where('type_account', 'coach')->where('is_active', 1)->where('commune', 'like', '%' . $city . '%');
                } else if ($experience == '2-7') {
                    $response = \App\Models\Customer::whereIn('experience', ['2-7', '27'])->where('type_account', 'coach')->where('is_active', 1)->where('commune', 'like', '%' . $city . '%');
                } else if ($experience == '7plus') {
                    $response = \App\Models\Customer::whereIn('experience', ['7plus', '7+'])->where('type_account', 'coach')->where('is_active', 1)->where('commune', 'like', '%' . $city . '%');
                } else {
                    $response = \App\Models\Customer::where('type_account', 'coach')->where('is_active', 1)->where('commune', 'like', '%' . $city . '%');
                }

                // $response = \App\Models\Customer::where('type_account', 'coach')->where('is_active', 1)->where('commune', 'like', '%'.$city.'%');

                // dd($response);

                $response = $response->orWhere('location_address', 'like', '%' . $city . '%')->orWhere('quartier', 'like', '%' . $city . '%');
            } else {
                $response = \App\Models\Customer::where('is_active', 1)->where('type_account', 'coach');
            }
            return $response;
        } else {
            $latitude = $lat;
            $longitude = $lon;
            $method = $request->method ?? 'geofence';

            if ($method == "geofence") {
                $inner_radius = 0;

                $outer_radius = 100;
                $response = \App\Models\Customer::geofence($latitude, $longitude, $inner_radius, $outer_radius)->where('commune', 'like', '%' . $city . '%')->orWhere('location_address', 'like', '%' . $city . '%')->orWhere('quartier', 'like', '%' . $city . '%')->where('is_active', 1);
                return $response;
            } else {
                $response = \App\Models\Customer::distance($latitude, $longitude)->where('commune', 'like', '%' . $city . '%')->orWhere('location_address', 'like', '%' . $city . '%')->orWhere('quartier', 'like', '%' . $city . '%')->where('is_active', 1);
                return $response;
            }
        }
    }

    function first_sentence($content)
    {

        $pos = strpos($content, '.');
        return substr($content, 0, $pos + 1);
    }

    public function getReviews($slug)
    {
        $product = Product::where('slug', $slug)->first();
        $reviews = $product->reviews();
        return view('customer_frontOffice.shops.commentaires', compact('reviews', 'product'));
    }
}
