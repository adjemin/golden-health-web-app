<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSeanceRequest;
use App\Http\Requests\UpdateSeanceRequest;
use App\Repositories\SeanceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SeanceController extends AppBaseController
{
    /** @var  SeanceRepository */
    private $seanceRepository;

    public function __construct(SeanceRepository $seanceRepo)
    {
        $this->seanceRepository = $seanceRepo;
    }

    /**
     * Display a listing of the Seance.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $seances = $this->seanceRepository->all();

        return view('seances.index')
            ->with('seances', $seances);
    }

    /**
     * Show the form for creating a new Seance.
     *
     * @return Response
     */
    public function create()
    {
        return view('seances.create');
    }

    /**
     * Store a newly created Seance in storage.
     *
     * @param CreateSeanceRequest $request
     *
     * @return Response
     */
    public function store(CreateSeanceRequest $request)
    {
        $input = $request->all();

        $seance = $this->seanceRepository->create($input);

        Flash::success('Seance saved successfully.');

        return redirect(route('seances.index'));
    }

    /**
     * Display the specified Seance.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $seance = $this->seanceRepository->find($id);

        if (empty($seance)) {
            Flash::error('Seance not found');

            return redirect(route('seances.index'));
        }

        return view('seances.show')->with('seance', $seance);
    }

    /**
     * Show the form for editing the specified Seance.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $seance = $this->seanceRepository->find($id);

        if (empty($seance)) {
            Flash::error('Seance not found');

            return redirect(route('seances.index'));
        }

        return view('seances.edit')->with('seance', $seance);
    }

    /**
     * Update the specified Seance in storage.
     *
     * @param int $id
     * @param UpdateSeanceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSeanceRequest $request)
    {
        $seance = $this->seanceRepository->find($id);

        if (empty($seance)) {
            Flash::error('Seance not found');

            return redirect(route('seances.index'));
        }

        $seance = $this->seanceRepository->update($request->all(), $id);

        Flash::success('Seance updated successfully.');

        return redirect(route('seances.index'));
    }

    /**
     * Remove the specified Seance from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $seance = $this->seanceRepository->find($id);

        if (empty($seance)) {
            Flash::error('Seance not found');

            return redirect(route('seances.index'));
        }

        $this->seanceRepository->delete($id);

        Flash::success('Seance deleted successfully.');

        return redirect(route('seances.index'));
    }
}
