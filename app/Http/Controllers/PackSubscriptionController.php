<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePackSubscriptionRequest;
use App\Http\Requests\UpdatePackSubscriptionRequest;
use App\Repositories\PackSubscriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PackSubscriptionController extends AppBaseController
{
    /** @var  PackSubscriptionRepository */
    private $packSubscriptionRepository;

    public function __construct(PackSubscriptionRepository $packSubscriptionRepo)
    {
        $this->packSubscriptionRepository = $packSubscriptionRepo;
    }

    /**
     * Display a listing of the PackSubscription.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $packSubscriptions = $this->packSubscriptionRepository->all();

        return view('pack_subscriptions.index')
            ->with('packSubscriptions', $packSubscriptions);
    }

    /**
     * Show the form for creating a new PackSubscription.
     *
     * @return Response
     */
    public function create()
    {
        return view('pack_subscriptions.create');
    }

    /**
     * Store a newly created PackSubscription in storage.
     *
     * @param CreatePackSubscriptionRequest $request
     *
     * @return Response
     */
    public function store(CreatePackSubscriptionRequest $request)
    {
        $input = $request->all();

        $packSubscription = $this->packSubscriptionRepository->create($input);

        Flash::success('Pack Subscription saved successfully.');

        return redirect(route('packSubscriptions.index'));
    }

    /**
     * Display the specified PackSubscription.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $packSubscription = $this->packSubscriptionRepository->find($id);

        if (empty($packSubscription)) {
            Flash::error('Pack Subscription not found');

            return redirect(route('packSubscriptions.index'));
        }

        return view('pack_subscriptions.show')->with('packSubscription', $packSubscription);
    }

    /**
     * Show the form for editing the specified PackSubscription.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $packSubscription = $this->packSubscriptionRepository->find($id);

        if (empty($packSubscription)) {
            Flash::error('Pack Subscription not found');

            return redirect(route('packSubscriptions.index'));
        }

        return view('pack_subscriptions.edit')->with('packSubscription', $packSubscription);
    }

    /**
     * Update the specified PackSubscription in storage.
     *
     * @param int $id
     * @param UpdatePackSubscriptionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePackSubscriptionRequest $request)
    {
        $packSubscription = $this->packSubscriptionRepository->find($id);

        if (empty($packSubscription)) {
            Flash::error('Pack Subscription not found');

            return redirect(route('packSubscriptions.index'));
        }

        $packSubscription = $this->packSubscriptionRepository->update($request->all(), $id);

        Flash::success('Pack Subscription updated successfully.');

        return redirect(route('packSubscriptions.index'));
    }

    /**
     * Remove the specified PackSubscription from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $packSubscription = $this->packSubscriptionRepository->find($id);

        if (empty($packSubscription)) {
            Flash::error('Pack Subscription not found');

            return redirect(route('packSubscriptions.index'));
        }

        $this->packSubscriptionRepository->delete($id);

        Flash::success('Pack Subscription deleted successfully.');

        return redirect(route('packSubscriptions.index'));
    }
}
