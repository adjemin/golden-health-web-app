<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEventRequest;
use App\Http\Requests\UpdateEventRequest;
use App\Repositories\EventRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Event;
use Illuminate\Http\Request;
use Flash;
use Response;
use Imgur;
use PDF;

class EventController extends AppBaseController
{
    /** @var  EventRepository */
    private $eventRepository;

    public function __construct(EventRepository $eventRepo)
    {
        $this->eventRepository = $eventRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Event.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $events = Event::orderBy('id', 'desc')->paginate(5);

        return view('events.index')
            ->with('events', $events);
    }

    /**
     * Show the form for creating a new Event.
     *
     * @return Response
     */
    public function create()
    {
        return view('events.create');
    }

    /**
     * Store a newly created Event in storage.
     *
     * @param CreateEventRequest $request
     *
     * @return Response
     */
    public function store(CreateEventRequest $request)
    {
        $input = $request->all();
        $this->validate($request,[
            'date_event'    => 'before:date_event_end'
        ]);

        if ($request->hasFile('cover_url')) {

            $image = $request->file('cover_url');
            $pictures = [];
            if ($image != null) {
                $productImage = Imgur::upload($image);
                $productImageLink = $productImage->link();
            }
        } else {
            $productImageLink = '';
        }

        $input = $request->all();
        unset($input['cover_url']);
        unset($input['is_free']);
        $input['cover_url'] = $productImageLink;
        $input['slug'] = \Str::slug($input['title'], "-");
        $event = $this->eventRepository->create($input);

        Flash::success('Event saved successfully.');

        return redirect(route('events.index'));
    }

    /**
     * Display the specified Event.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $event = $this->eventRepository->find($id);

        if (empty($event)) {
            Flash::error('Event not found');

            return redirect(route('events.index'));
        }

        return view('events.show')->with('event', $event);
    }

    /**
     * Show the form for editing the specified Event.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $event = $this->eventRepository->find($id);

        if (empty($event)) {
            Flash::error('Event not found');

            return redirect(route('events.index'));
        }

        return view('events.edit')->with('event', $event);
    }

    /**
     * Update the specified Event in storage.
     *
     * @param int $id
     * @param UpdateEventRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEventRequest $request)
    {
        $event = $this->eventRepository->find($id);
        $this->validate($request,[
            'date_event'    => 'before:date_event_end'
        ]);

        if (empty($event)) {
            Flash::error('Event not found');

            return redirect(route('events.index'));
        }

        if ($request->file('medias')) {

            $image = $request->file('medias');
            $pictures = [];
            if ($image != null) {
                $productImage = Imgur::upload($image);
                $productImageLink = $productImage->link();
            }
        } else {
            $productImageLink = $event->cover_url;
        }

        $input = $request->all();
        unset($input['medias']);
        unset($input['is_free']);
        $input['cover_url'] = $productImageLink;
        $input['slug'] = \Str::slug($input['title'], "-");
        $input['date_event'] = $event->date_event;
        $input['date_event_end'] = $event->date_event_end;
        $event = $this->eventRepository->update($input, $id);

        Flash::success('Event updated successfully.');

        return redirect(route('events.index'));
    }

    /**
     * Remove the specified Event from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $event = $this->eventRepository->find($id);

        if (empty($event)) {
            Flash::error('Event not found');

            return redirect(route('events.index'));
        }

        $this->eventRepository->delete($id);

        Flash::success('Event deleted successfully.');

        return redirect(route('events.index'));
    }

    public function searchEvent(Request $request)
    {
        $eventsFound = Event::where('title', 'like', '%' . $request->searchEvent . '%')
            ->orWhere('venue', 'like', '%' . $request->searchEvent . '%')
            ->orWhere('description', 'like', '%' . $request->searchEvent . '%')
            ->orWhere('date_event', 'like', '%' . $request->searchEvent . '%')
            ->orWhere('date_event_end', 'like', '%' . $request->searchEvent . '%')
            ->latest()->get();
        return json_encode(view('events.table', ['events' => $eventsFound])->render());
    }

    public function printAll(){
        $events = Event::all()->sortByDesc('id');
        return view('events.table_pdf', compact('events'));
        // $pdf = PDF::loadView('events.table_pdf', compact('events'));
        // return $pdf->stream();
    }
}
