<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatecourseLieuRequest;
use App\Http\Requests\UpdatecourseLieuRequest;
use App\Repositories\courseLieuRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class courseLieuController extends AppBaseController
{
    /** @var  courseLieuRepository */
    private $courseLieuRepository;

    public function __construct(courseLieuRepository $courseLieuRepo)
    {
        $this->courseLieuRepository = $courseLieuRepo;
    }

    /**
     * Display a listing of the courseLieu.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $courseLieus = $this->courseLieuRepository->all();

        return view('course_lieus.index')
            ->with('courseLieus', $courseLieus);
    }

    /**
     * Show the form for creating a new courseLieu.
     *
     * @return Response
     */
    public function create()
    {
        return view('course_lieus.create');
    }

    /**
     * Store a newly created courseLieu in storage.
     *
     * @param CreatecourseLieuRequest $request
     *
     * @return Response
     */
    public function store(CreatecourseLieuRequest $request)
    {
        $input = $request->all();

        $courseLieu = $this->courseLieuRepository->create($input);

        Flash::success('Course Lieu saved successfully.');

        return redirect(route('courseLieus.index'));
    }

    /**
     * Display the specified courseLieu.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $courseLieu = $this->courseLieuRepository->find($id);

        if (empty($courseLieu)) {
            Flash::error('Course Lieu not found');

            return redirect(route('courseLieus.index'));
        }

        return view('course_lieus.show')->with('courseLieu', $courseLieu);
    }

    /**
     * Show the form for editing the specified courseLieu.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $courseLieu = $this->courseLieuRepository->find($id);

        if (empty($courseLieu)) {
            Flash::error('Course Lieu not found');

            return redirect(route('courseLieus.index'));
        }

        return view('course_lieus.edit')->with('courseLieu', $courseLieu);
    }

    /**
     * Update the specified courseLieu in storage.
     *
     * @param int $id
     * @param UpdatecourseLieuRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecourseLieuRequest $request)
    {
        $courseLieu = $this->courseLieuRepository->find($id);

        if (empty($courseLieu)) {
            Flash::error('Course Lieu not found');

            return redirect(route('courseLieus.index'));
        }

        $courseLieu = $this->courseLieuRepository->update($request->all(), $id);

        Flash::success('Course Lieu updated successfully.');

        return redirect(route('courseLieus.index'));
    }

    /**
     * Remove the specified courseLieu from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $courseLieu = $this->courseLieuRepository->find($id);

        if (empty($courseLieu)) {
            Flash::error('Course Lieu not found');

            return redirect(route('courseLieus.index'));
        }

        $this->courseLieuRepository->delete($id);

        Flash::success('Course Lieu deleted successfully.');

        return redirect(route('courseLieus.index'));
    }
}
