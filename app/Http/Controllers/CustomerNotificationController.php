<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerNotificationRequest;
use App\Http\Requests\UpdateCustomerNotificationRequest;
use App\Repositories\CustomerNotificationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Mail;
use App\Mail\SelfChallengerWelcomeMail;
use Illuminate\Http\Request;
use App\Customer;
use Flash;
use Response;

class CustomerNotificationController extends AppBaseController
{
    /** @var  CustomerNotificationRepository */
    private $customerNotificationRepository;

    public function __construct(CustomerNotificationRepository $customerNotificationRepo)
    {
        $this->customerNotificationRepository = $customerNotificationRepo;
    }

    /**
     * Display a listing of the CustomerNotification.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $customerNotifications = $this->customerNotificationRepository->paginate(10);

        return view('customer_notifications.index')
            ->with('customerNotifications', $customerNotifications);
    }

    /**
     * Show the form for creating a new CustomerNotification.
     *
     * @return Response
     */
    public function create()
    {
        return view('customer_notifications.create');
    }

    /**
     * Store a newly created CustomerNotification in storage.
     *
     * @param CreateCustomerNotificationRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerNotificationRequest $request)
    {
        $input = $request->all();

        $customerNotification = $this->customerNotificationRepository->create($input);

        Flash::success('Customer Notification saved successfully.');

        return redirect(route('customerNotifications.index'));
    }

    public function resendEmailSubcription(Request $request)
    {
        $id = $request->id;
        $customer = Customer::find($id);
        if($customer){
            $token = csrf_token();
            $actionUrl = "https://goldenhealth.ci/customer/confirm-challenger-email?customer_email=" . $customer->email . "&token=" . $token . "&csrf-token=" . $token;
            Mail::to($customer->email)->send(new SelfChallengerWelcomeMail($customer, $actionUrl));
        }
        
        Flash::success("L'email a été renvoyé avec succès, veuillez revérifier votre boite mail.");
        
        return redirect(url('customer/dashboard'))->with('customer', $customer);
    }
    /**
     * Display the specified CustomerNotification.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customerNotification = $this->customerNotificationRepository->find($id);

        if (empty($customerNotification)) {
            Flash::error('Customer Notification not found');

            return redirect(route('customerNotifications.index'));
        }

        return view('customer_notifications.show')->with('customerNotification', $customerNotification);
    }

    /**
     * Show the form for editing the specified CustomerNotification.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customerNotification = $this->customerNotificationRepository->find($id);

        if (empty($customerNotification)) {
            Flash::error('Customer Notification not found');

            return redirect(route('customerNotifications.index'));
        }

        return view('customer_notifications.edit')->with('customerNotification', $customerNotification);
    }

    /**
     * Update the specified CustomerNotification in storage.
     *
     * @param int $id
     * @param UpdateCustomerNotificationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerNotificationRequest $request)
    {
        $customerNotification = $this->customerNotificationRepository->find($id);

        if (empty($customerNotification)) {
            Flash::error('Customer Notification not found');

            return redirect(route('customerNotifications.index'));
        }

        $customerNotification = $this->customerNotificationRepository->update($request->all(), $id);

        Flash::success('Customer Notification updated successfully.');

        return redirect(route('customerNotifications.index'));
    }

    /**
     * Remove the specified CustomerNotification from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customerNotification = $this->customerNotificationRepository->find($id);

        if (empty($customerNotification)) {
            Flash::error('Customer Notification not found');

            return redirect(route('customerNotifications.index'));
        }

        $this->customerNotificationRepository->delete($id);

        Flash::success('Customer Notification deleted successfully.');

        return redirect(route('customerNotifications.index'));
    }
}
