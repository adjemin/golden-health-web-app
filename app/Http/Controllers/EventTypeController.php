<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\EventType;
use Illuminate\Http\Request;

class EventTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the EventType.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $eventTypes = EventType::paginate(5);

        return view('eventTypes.index')
            ->with('eventTypes', $eventTypes);
    }

    /**
     * Show the form for creating a new Category.
     *
     * @return Response
     */
    public function create()
    {
        return view('eventTypes.create');
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  =>  'required|unique:event_types'
        ]);
        
        EventType::create([
            'name'  =>  $request->name
        ]);

        Flash::success('Event Type saved successfully.');

        return redirect(route('eventTypes.index'));
    }

    /**
     * Display the specified Category.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $eventType = EventType::find($id);

        if (empty($eventType)) {
            Flash::error('Event type not found');

            return redirect(route('eventTypes.index'));
        }

        return view('eventTypes.show')->with('eventType', $eventType);
    }

    /**
     * Show the form for editing the specified Category.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $eventType = EventType::find($id);

        if (empty($eventType)) {
            Flash::error('Event type not found');

            return redirect(route('eventTypes.index'));
        }

        return view('eventTypes.edit')->with('eventType', $eventType);
    }

    /**
     * Update the specified Category in storage.
     *
     * @param int $id
     * @param UpdateCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryRequest $request)
    {
        $eventType = EventType::find($id);

        if (empty($eventType)) {
            Flash::error('Event type not found');

            return redirect(route('eventTypes.index'));
        }

        $this->validate($request, [
            'name'  =>  'required|unique:event_types'
        ]);

        $eventType->name = $request->name;
        $eventType->save();

        Flash::success('Event type updated successfully.');

        return redirect(route('eventTypes.index'));
    }

    /**
     * Remove the specified Category from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $eventType = EventType::find($id);

        if (empty($eventType)) {
            Flash::error('Event type not found');

            return redirect(route('eventTypes.index'));
        }

        $eventType->delete();

        Flash::success('Event type deleted successfully.');

        return redirect(route('eventTypes.index'));
    }
}
