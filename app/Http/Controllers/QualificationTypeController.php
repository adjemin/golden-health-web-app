<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateQualificationTypeRequest;
use App\Http\Requests\UpdateQualificationTypeRequest;
use App\Repositories\QualificationTypeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class QualificationTypeController extends AppBaseController
{
    /** @var  QualificationTypeRepository */
    private $qualificationTypeRepository;

    public function __construct(QualificationTypeRepository $qualificationTypeRepo)
    {
        $this->qualificationTypeRepository = $qualificationTypeRepo;
    }

    /**
     * Display a listing of the QualificationType.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $qualificationTypes = $this->qualificationTypeRepository->all();

        return view('qualification_types.index')
            ->with('qualificationTypes', $qualificationTypes);
    }

    /**
     * Show the form for creating a new QualificationType.
     *
     * @return Response
     */
    public function create()
    {
        return view('qualification_types.create');
    }

    /**
     * Store a newly created QualificationType in storage.
     *
     * @param CreateQualificationTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateQualificationTypeRequest $request)
    {
        $input = $request->all();

        $qualificationType = $this->qualificationTypeRepository->create($input);

        Flash::success('Qualification Type saved successfully.');

        return redirect(route('qualificationTypes.index'));
    }

    /**
     * Display the specified QualificationType.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $qualificationType = $this->qualificationTypeRepository->find($id);

        if (empty($qualificationType)) {
            Flash::error('Qualification Type not found');

            return redirect(route('qualificationTypes.index'));
        }

        return view('qualification_types.show')->with('qualificationType', $qualificationType);
    }

    /**
     * Show the form for editing the specified QualificationType.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $qualificationType = $this->qualificationTypeRepository->find($id);

        if (empty($qualificationType)) {
            Flash::error('Qualification Type not found');

            return redirect(route('qualificationTypes.index'));
        }

        return view('qualification_types.edit')->with('qualificationType', $qualificationType);
    }

    /**
     * Update the specified QualificationType in storage.
     *
     * @param int $id
     * @param UpdateQualificationTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQualificationTypeRequest $request)
    {
        $qualificationType = $this->qualificationTypeRepository->find($id);

        if (empty($qualificationType)) {
            Flash::error('Qualification Type not found');

            return redirect(route('qualificationTypes.index'));
        }

        $qualificationType = $this->qualificationTypeRepository->update($request->all(), $id);

        Flash::success('Qualification Type updated successfully.');

        return redirect(route('qualificationTypes.index'));
    }

    /**
     * Remove the specified QualificationType from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $qualificationType = $this->qualificationTypeRepository->find($id);

        if (empty($qualificationType)) {
            Flash::error('Qualification Type not found');

            return redirect(route('qualificationTypes.index'));
        }

        $this->qualificationTypeRepository->delete($id);

        Flash::success('Qualification Type deleted successfully.');

        return redirect(route('qualificationTypes.index'));
    }
}
