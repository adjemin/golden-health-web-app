<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatecourseDetailsRequest;
use App\Http\Requests\UpdatecourseDetailsRequest;
use App\Repositories\courseDetailsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class courseDetailsController extends AppBaseController
{
    /** @var  courseDetailsRepository */
    private $courseDetailsRepository;

    public function __construct(courseDetailsRepository $courseDetailsRepo)
    {
        $this->courseDetailsRepository = $courseDetailsRepo;
    }

    /**
     * Display a listing of the courseDetails.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $courseDetails = $this->courseDetailsRepository->all();

        return view('course_details.index')
            ->with('courseDetails', $courseDetails);
    }

    /**
     * Show the form for creating a new courseDetails.
     *
     * @return Response
     */
    public function create()
    {
        return view('course_details.create');
    }

    /**
     * Store a newly created courseDetails in storage.
     *
     * @param CreatecourseDetailsRequest $request
     *
     * @return Response
     */
    public function store(CreatecourseDetailsRequest $request)
    {
        $input = $request->all();

        $courseDetails = $this->courseDetailsRepository->create($input);

        Flash::success('Course Details saved successfully.');

        return redirect(route('courseDetails.index'));
    }

    /**
     * Display the specified courseDetails.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $courseDetails = $this->courseDetailsRepository->find($id);

        if (empty($courseDetails)) {
            Flash::error('Course Details not found');

            return redirect(route('courseDetails.index'));
        }

        return view('course_details.show')->with('courseDetails', $courseDetails);
    }

    /**
     * Show the form for editing the specified courseDetails.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $courseDetails = $this->courseDetailsRepository->find($id);

        if (empty($courseDetails)) {
            Flash::error('Course Details not found');

            return redirect(route('courseDetails.index'));
        }

        return view('course_details.edit')->with('courseDetails', $courseDetails);
    }

    /**
     * Update the specified courseDetails in storage.
     *
     * @param int $id
     * @param UpdatecourseDetailsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecourseDetailsRequest $request)
    {
        $courseDetails = $this->courseDetailsRepository->find($id);

        if (empty($courseDetails)) {
            Flash::error('Course Details not found');

            return redirect(route('courseDetails.index'));
        }

        $courseDetails = $this->courseDetailsRepository->update($request->all(), $id);

        Flash::success('Course Details updated successfully.');

        return redirect(route('courseDetails.index'));
    }

    /**
     * Remove the specified courseDetails from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $courseDetails = $this->courseDetailsRepository->find($id);

        if (empty($courseDetails)) {
            Flash::error('Course Details not found');

            return redirect(route('courseDetails.index'));
        }

        $this->courseDetailsRepository->delete($id);

        Flash::success('Course Details deleted successfully.');

        return redirect(route('courseDetails.index'));
    }
}
