<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBodyMassIndexRequest;
use App\Http\Requests\UpdateBodyMassIndexRequest;
use App\Repositories\BodyMassIndexRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class BodyMassIndexController extends AppBaseController
{
    /** @var  BodyMassIndexRepository */
    private $bodyMassIndexRepository;

    public function __construct(BodyMassIndexRepository $bodyMassIndexRepo)
    {
        $this->bodyMassIndexRepository = $bodyMassIndexRepo;
    }

    /**
     * Display a listing of the BodyMassIndex.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $bodyMassIndices = $this->bodyMassIndexRepository->all();

        return view('body_mass_indices.index')
            ->with('bodyMassIndices', $bodyMassIndices);
    }

    /**
     * Show the form for creating a new BodyMassIndex.
     *
     * @return Response
     */
    public function create()
    {
        return view('body_mass_indices.create');
    }

    /**
     * Store a newly created BodyMassIndex in storage.
     *
     * @param CreateBodyMassIndexRequest $request
     *
     * @return Response
     */
    public function store(CreateBodyMassIndexRequest $request)
    {
        $input = $request->all();

        $bodyMassIndex = $this->bodyMassIndexRepository->create($input);

        Flash::success('Body Mass Index saved successfully.');

        return redirect(route('bodyMassIndices.index'));
    }

    /**
     * Display the specified BodyMassIndex.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bodyMassIndex = $this->bodyMassIndexRepository->find($id);

        if (empty($bodyMassIndex)) {
            Flash::error('Body Mass Index not found');

            return redirect(route('bodyMassIndices.index'));
        }

        return view('body_mass_indices.show')->with('bodyMassIndex', $bodyMassIndex);
    }

    /**
     * Show the form for editing the specified BodyMassIndex.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bodyMassIndex = $this->bodyMassIndexRepository->find($id);

        if (empty($bodyMassIndex)) {
            Flash::error('Body Mass Index not found');

            return redirect(route('bodyMassIndices.index'));
        }

        return view('body_mass_indices.edit')->with('bodyMassIndex', $bodyMassIndex);
    }

    /**
     * Update the specified BodyMassIndex in storage.
     *
     * @param int $id
     * @param UpdateBodyMassIndexRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBodyMassIndexRequest $request)
    {
        $bodyMassIndex = $this->bodyMassIndexRepository->find($id);

        if (empty($bodyMassIndex)) {
            Flash::error('Body Mass Index not found');

            return redirect(route('bodyMassIndices.index'));
        }

        $bodyMassIndex = $this->bodyMassIndexRepository->update($request->all(), $id);

        Flash::success('Body Mass Index updated successfully.');

        return redirect(route('bodyMassIndices.index'));
    }

    /**
     * Remove the specified BodyMassIndex from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bodyMassIndex = $this->bodyMassIndexRepository->find($id);

        if (empty($bodyMassIndex)) {
            Flash::error('Body Mass Index not found');

            return redirect(route('bodyMassIndices.index'));
        }

        $this->bodyMassIndexRepository->delete($id);

        Flash::success('Body Mass Index deleted successfully.');

        return redirect(route('bodyMassIndices.index'));
    }
}
