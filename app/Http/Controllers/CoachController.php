<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateCoachRequest;
use App\Http\Requests\UpdateCoachRequest;
use App\Models\Coach;
use App\Customer;
use App\Mail\CoachConfirm;
use Illuminate\Support\Facades\Mail;
use App\Repositories\CoachRepository;
use Flash;
use Illuminate\Http\Request;
use Response;
use PDF;
use Imgur;

class CoachController extends AppBaseController
{
    /** @var  CoachRepository */
    private $coachRepository;

    public function __construct(CoachRepository $coachRepo)
    {
        $this->coachRepository = $coachRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Coach.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $coaches = Coach::orderBy('id', 'desc')->paginate(5);

        return view('coaches.index')
            ->with('coaches', $coaches);
    }

    /**
     * Show the form for creating a new Coach.
     *
     * @return Response
     */
    public function create()
    {
        return view('coaches.create');
    }

    /**
     * Store a newly created Coach in storage.
     *
     * @param CreateCoachRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'gender' => 'required',
            'last_name' => 'required|max:255',
            'first_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:customers',
            'password' => 'required|min:6|confirmed',
            'facebook_url'    =>  'nullable|url',
            'instagram_url'    =>  'nullable|url',
            'youtube_url'    =>  'nullable|url',
            'site_web_coach'    =>  'nullable|url',
        ]);
        
        if ($request->file('photo_url')) {

            $image = $request->file('photo_url');
            $pictures = [];
            if ($image != null) {    
                $productImage = Imgur::upload($image);
                $productImageLink = $productImage->link();
            }
        } else {
            $productImageLink = 'https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png';
        }

        $data = $request->all();

        $customer = Customer::create(
            [
                'last_name' => $data['last_name'],
                'first_name' => $data['first_name'],
                'name' => $data['last_name'] . " " . $data['first_name'],
                'gender' => $data['gender'],
                'is_cgu' => 1,
                'is_newsletter' => 0,
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'photo_url'=>  $productImageLink,
                'type_account' => $data['type_account'],
                'activation_date' => now(),
                'email_verifed_at' => now(),
                'is_active' => 1
            ]
        );

        // if($data['type_account'] == 'coach'){
            Coach::create([
                'customer_id'  => $customer->id,
                'name'  => $customer->name,
                'email'  => $customer->name,
                'facebook_url'  => $data['facebook_url'],
                'youtube_url'  => $data['youtube_url'],
                'instagram_url'  => $data['instagram_url'],
                'site_web'  => $data['site_web_coach'],
                'discovery_source'  => $data['discovery_source'],
                'gender'  => $customer->gender,
                'is_active' =>  1
            ]);
        // }

        // $coach = $this->coachRepository->create($input);
        Mail::to($customer->email)->send(new CoachConfirm());

        Flash::success('Coach saved successfully.');

        return redirect(route('coaches.index'));
    }

    /**
     * Display the specified Coach.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $coach = $this->coachRepository->find($id);
        // dd($coach);

        if (empty($coach)) {
            Flash::error('Coach not found');

            return redirect(route('coaches.index'));
        }

        return view('coaches.show')->with('coach', $coach);
    }

    /**
     * Show the form for editing the specified Coach.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $coach = $this->coachRepository->find($id);

        if (empty($coach)) {
            Flash::error('Coach not found');

            return redirect(route('coaches.index'));
        }

        $customer = Customer::find($coach->customer_id);

        return view('coaches.edit')
            ->with('customer', $customer)
            ->with('coach', $coach);
    }

    /**
     * Update the specified Coach in storage.
     *
     * @param int $id
     * @param UpdateCoachRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'gender' => 'required',
            'last_name' => 'required|max:255',
            'first_name' => 'required|max:255',
            'email' => 'required|max:255',
            'password' => 'required|min:6|confirmed',
            'facebook_url'    =>  'nullable|url',
            'instagram_url'    =>  'nullable|url',
            'youtube_url'    =>  'nullable|url',
            'site_web_coach'    =>  'nullable|url',
        ]);

        $coach = $this->coachRepository->find($id);

        if (empty($coach)) {
            Flash::error('Coach not found');

            return redirect(route('coaches.index'));
        }
        
        if ($request->file('photo_url')) {

            $image = $request->file('photo_url');
            $pictures = [];
            if ($image != null) {    
                $productImage = Imgur::upload($image);
                $productImageLink = $productImage->link();
            }
        }

        $input = $request->all();
        $customer = Customer::find($coach->customer_id);
        $customer->first_name = $input['first_name'];
        $customer->last_name = $input['last_name'];
        $customer->name = $input['first_name'].' '.$input['last_name'];
        $customer->email = $input['email'];
        if(!is_null($productImageLink)){
            $customer->photo_url = $productImageLink;
        }
        $customer->password = $input['password'];
        $customer->save();

        $coach->name = $customer->name;
        $coach->email = $customer->email;
        $coach->delivery_source = $input['discovery_source'];
        $coach->facebook_url = $data['facebook_url'];
        $coach->youtube_url  = $data['youtube_url'];
        $coach->instagram_url  = $data['instagram_url'];
        $coach->site_web  = $data['site_web_coach'];
        $coach->save();

        // $coach = $this->coachRepository->update($request->all(), $id);

        Flash::success('Coach updated successfully.');

        return redirect(route('coaches.index'));
    }

    /**
     * Remove the specified Coach from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $coach = $this->coachRepository->find($id);

        if (empty($coach)) {
            Flash::error('Coach not found');

            return redirect(route('coaches.index'));
        }

        $customer = Customer::find($coach->customer_id);
        if (empty($customer)) {
            Flash::error('Coach not found');

            $this->coachRepository->delete($id);

            return redirect(route('coaches.index'));
        }
        $customer->delete();
        $this->coachRepository->delete($id);

        Flash::success('Coach deleted successfully.');

        return redirect(route('coaches.index'));
    }

    public function searchCoach(Request $request)
    {
        $coachsFound = Coach::leftJoin('customers', 'customers.id', '=', 'coaches.customer_id')
            ->select('coaches.*')
            ->where('customers.name', 'like', '%' . $request->searchCoach . '%')
            ->orWhere('customers.email', 'like', '%' . $request->searchCoach . '%')
            ->orWhere('customers.phone', 'like', '%' . $request->searchCoach . '%')
            ->orWhere('customers.location_address', 'like', '%' . $request->searchCoach . '%')
            ->get();
        return json_encode(view('coaches.table', ['coaches' => $coachsFound])->render());
    }

    public function changeStatus($id, Request $request){
        $coach = $this->coachRepository->find($id);
        // dd($request->all());

        $input = $request->all();
        if (empty($coach )) {
            Flash::error('Coach not found');

            return redirect(route('customers.index'));
        }

        $coach->is_active = $request->is_active;
        $coach->save();

        $customer = \App\Models\Customer::where('id', $coach->customer_id)->first();
        $customer->is_active = $request->is_active;
        $customer->activation_date =  is_null($customer->activation_date) ? now() : $customer->activation_date;
        $customer->email_verifed_at =  is_null($customer->email_verifed_at) ? now() : $customer->email_verifed_at;
        $customer->save();
        
        Mail::to($customer->email)->send(new CoachConfirm());

        Flash::success('Coach updated successfully.');

        return redirect(route('coaches.index'));
    }

    public function printAll(){
        $coaches = Coach::all();
        return view('coaches.table_pdf', compact('coaches'));
        // $pdf = PDF::loadView('coaches.table_pdf', compact('coaches'));
        // return $pdf->stream();
    }
}
