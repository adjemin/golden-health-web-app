<?php

namespace App\Http\Controllers\CustomerAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Socialite;
use App\Customer;
use App\User;
use App\Mail\SelfChallengerWelcomeMail;
use App\Mail\AdminSignupNotificationMail;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    protected function authenticated(Request $request, $user)
    {
        if(session()->get('redirectTo'))
        {
            $url = session()->pull('redirectTo');
            return redirect($url);
        }

        if($user->account_type = 'partner'){
            session()->flash('message', "Candidature enregistrée avec success");
            session(['name'=> $user->name]);
            return redirect('/');
        }

        return redirect($this->redirectTo);
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/customer/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('customer.guest', ['except' => 'logout']);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('customer');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('customer_frontOffice.auth.login');
    }

    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {

        // $user = $this->guard()->user();
        // $user->remember_token = null;
        // $user->save();
        $this->guard()->logout();

        $request->session()->invalidate();
        $request->session()->flush();

        $request->session()->regenerateToken();
        $request->session()->regenerate();

        return redirect($this->redirectTo);
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
      try {
        if ($provider == "google") {
          $user = Socialite::driver($provider)->user();
        } else {
          $user = Socialite::driver($provider)->fields([
            'first_name', 'last_name', 'email','picture','short_name','name'
          ])->user();
        }
      } catch (\Exception $e) {
        return redirect('auth/'.$provider);
      }

      // dd($user);
      if($user->email){
        if ($provider == "facebook") {
              $authUser = Customer::where('facebook_id', $user->id)->orWhere('email', $user->email)->first();
            if (!is_null($authUser)) {
              if(is_null($authUser->email_verifed_at) || $authUser->is_active == 0){
                $authUser->email_verifed_at = now();
                $authUser->activation_date = now();
                $authUser->is_active = 1;
                $authUser->save();
              }
                //return $authUser;
                // $aut = Auth::login($authUser, true);
                Auth::guard('customer')->login($authUser);
                return redirect($this->redirectTo);
            }else{
              // $data = [
              //   'name' => $user->name,
              //   'first_name' => $user->user['first_name'],
              //   'last_name' => $user->user['last_name'],
              //   'email' => $user->user['email'],
              //   'provider' => 'facebook',
              //   'facebook_id' => $user->id
              // ];
              // return Redirect::route('customer.register.self-challenger')->with('data', $data);
              $new_user = new Customer();
              $new_user->name = $user->name;
              $new_user->first_name = $user->user['first_name'];
              $new_user->last_name = $user->user['last_name'];
              $new_user->email = $user->email;
              $new_user->facebook_id = $user->id;
              $new_user->activation_date = now();
              $new_user->email_verifed_at = now();
              $new_user->is_cgu = true;
              $new_user->is_newsletter = true;
              $new_user->is_active = true;
              $new_user->photo_url = $user->avatar ?? 'https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png';
              $new_user->save();
              $this->notify($new_user);
              return redirect($this->redirectTo);
            }
        } else if ($provider == "google") {
            //dd($user);
            $authUser = Customer::where('google_id', $user->id)->orWhere('email', $user->email)->first();
            
            if (!is_null($authUser)) {
              if(is_null($authUser->email_verifed_at) || $authUser->is_active == 0){
                $authUser->email_verifed_at = now();
                $authUser->activation_date = now();
                $authUser->is_active = 1;
                $authUser->save();
              }
              // $aut = Auth::login($authUser, true);
                Auth::guard('customer')->login($authUser);
                return redirect($this->redirectTo);
            }else{
              // $data = [
              //   'name' => $user->name,
              //   'first_name' => $user->user['given_name'],
              //   'last_name' => $user->user['family_name'],
              //   'email' => $user->email,
              //   'provider' => 'google',
              //   'google_id' => $user->id
              // ];
              $new_user = new Customer();
              $new_user->name = $user->name;
              $new_user->first_name = $user->user['given_name'];
              $new_user->last_name = $user->user['family_name'];
              $new_user->email = $user->email;
              $new_user->google_id = $user->id;
              $new_user->activation_date = now();
              $new_user->email_verifed_at = now();
              $new_user->is_cgu = true;
              $new_user->is_newsletter = true;
              $new_user->is_active = true;
              $new_user->photo_url = $user->avatar;
              $new_user->save();
              // return $new_user;
              $this->notify($new_user);
              return redirect($this->redirectTo);
            }
        }

        //$aut = Auth::login($authUser, true);
        //Auth::guard('customer')->login($authUser);

        //return redirect($this->redirectTo);

      }else{
        return Redirect::route('customer.login');
        /*return Redirect::route( 'register' )
    		->with( 'user', $user );*/
      }
    }

     /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
      try {
          //dd($user->user['first_name']);
          if ($provider == "facebook") {
              $authUser = Customer::where('facebook_id', $user->id)->orWhere('email', $user->email)->first();
              if(!is_null($authUser)) {
                  return $authUser;
              }else{
                $data = [
                  'name' => $user->name,
                  'first_name' => $user->user['first_name'],
                  'last_name' => $user->user['last_name'],
                  'email' => $user->email,
                  'provider' => 'facebook',
                  'facebook_id' => $user->id
                ];
                return Redirect::route('register')->with('data', $data);
                /*$new_user = new Customer();
                $new_user->name = $user->name;
                $new_user->first_name = $user->user['first_name'];
                $new_user->last_name = $user->user['last_name'];
                $new_user->email = $user->email;
                if ($provider == "facebook") {
                    $new_user->facebook_id = $user->id;
                } else if ($provider == "google") {
                    $new_user->google_id = $user->id;
                }
                $new_user->save();
                return $new_user;*/
              }
          } else if ($provider == "google") {
                $authUser = Customer::where('google_id', $user->id)->orWhere('email', $user->email)->first();
              if (!is_null($authUser)) {
                  return $authUser;
              }else{
                $data = [
                  'name' => $user->name,
                  'first_name' => $user->user['first_name'],
                  'last_name' => $user->user['last_name'],
                  'email' => $user->email,
                  'provider' => 'google',
                  'google_id' => $user->id
                ];
                return Redirect::route('register')->with('data', $data);
                /*$new_user = new Customer();
                $new_user->name = $user->name;
                $new_user->first_name = $user->user['given_name'];
                $new_user->last_name = $user->user['family_name'];
                $new_user->email = $user->email;
                $new_user->google_id = $user->id;
                $new_user->save();
                return $new_user;*/
              }
          }


      } catch (\Exception $e) {

      }

    }

    public function notify($customer){
      if($customer->type_account != "partner" && $customer->type_account != 'coach'){
        $token = csrf_token();
        $actionUrl = env('APP_URL')."/customer/confirm-challenger-email?customer_email=".$customer->email."&token=".$token."&csrf-token=".$token;
        Mail::to($customer->email)->send(new SelfChallengerWelcomeMail($customer, $actionUrl));
      }

      // notifying every admin
      $accountUri = $customer->type_account == 'partner' ? 'partners' : ($customer->type_account == 'coach' ? 'coaches' : 'customers');

      $data = [
          'accountName' => $customer->name,
          'accountType' => $customer->type_account,
          'accountUrl' => env('APP_URL')."/$accountUri/$customer->id"
      ];

      // notifying admins
      foreach(User::all() as $user){
        Mail::to($user->email)->send(new AdminSignupNotificationMail($data));
      }
    }
}
