<?php

namespace App\Http\Controllers\CustomerAuth;
use Illuminate\Support\Facades\Redirect;

use App\User;
use App\Customer;
use App\Models\Coach;
use App\Models\Partner;
use App\Mail\CoachWelcomeMail;
use App\Models\CoachDiscipline;
use App\Mail\PartnerWelcomeMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\SelfChallengerWelcomeMail;
use App\Mail\AdminSignupNotificationMail;
use App\Models\Qualification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Models\CustomerNotifications;
use App\Models\distinction;
use App\Utils\CustomerNotificationUtils;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new admins as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/customer/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('master.guest:master');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // $recaptcha = $data['g-recaptcha-response'];
        // $res = $this->reCaptcha($recaptcha);
        // $data['g-recaptcha-response'] = $res['success'];
        return Validator::make($data, [
            'gender' => 'required',
            'last_name' => 'required|max:255',
            'first_name' => 'required|max:255',
            'name' => 'nullable',
            'phone_number' => 'required',
            'phone' => 'nullable',
            'type_account' => 'required',
            'email' => 'required|email|max:255|unique:customers',
            'password' => 'required|min:6|confirmed',
            'is_cgu' => 'required',
            //'experience' => 'required',
            //'language' => 'required',
            //'seance_price' => 'required',
            'g-recaptcha-response'=>'required',
            'is_recaptcha_success'=>'true',
            'facebook_url'    =>  'nullable|url',
            'instagram_url'    =>  'nullable|url',
            'youtube_url'    =>  'nullable|url',
            'site_web'    =>  'nullable|url',
            'weight' => 'nullable|numeric',
            'size' => 'nullable|numeric',
            'target_weight' => 'nullable|numeric',
            'birthday' => 'nullable',
            'lat' => 'nullable',
            'lon' => 'nullable',
            'qualification_names' => 'nullable',
            'qualification_start_dates' => 'nullable',
            'qualification_files' => 'nullable',
            'discipline_id' => 'nullable',
            'service_type' => isset($data['service_type']) ? 'required' : 'nullable',
            'service_type_other' => isset($data['service_type']) ? ($data['service_type'] == '3' ? 'required|max:255' : 'nullable') : 'nullable',

        ]);
    }

    public function reCaptcha($recaptcha){
        $secret = "6LcWRaQdAAAAAK6a4zYaJVW6l_lq0M_mlJE3baG8";
        $ip = $_SERVER['REMOTE_ADDR'];
      
        $postvars = array("secret"=>$secret, "response"=>$recaptcha, "remoteip"=>$ip);
        $url = "https://www.google.com/recaptcha/api/siteverify";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
        $data = curl_exec($ch);
        curl_close($ch);
      
        return json_decode($data, true);
      }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Master
     */
    protected function create(array $data)
    {

        // Register self-challenger or Coach

        $customer = new Customer();

        $data['last_name'] = ucfirst(strtolower($data['last_name']));
        $data['first_name'] = ucfirst(strtolower($data['first_name']));
        $data['name'] = $data['first_name'] . " " . $data['last_name'];
        $customer->last_name = $data['last_name'];
        $customer->first_name = $data['first_name'];
        $customer->name = $data['name'];
        $customer->gender = strtoupper($data['gender']);
        $customer->is_cgu = $data['is_cgu'];
        $customer->phone_number = $data['phone_number'];
        $customer->dial_code = $data['dial_code'];
        if(isset($data['language'])){
            $customer->language = $data['language'];
        }
        if(isset($data['seance_price'])){
            $customer->seance_price = $data['seance_price'];
        }
        if(isset($data['phone_number'])){
            $customer->phone = $data['dial_code'] . $data['phone_number'];
        }
    
        $customer->weight = isset($data['weight']) ? $data['weight'] : null;
        $customer->target_weight = isset($data['target_weight']) ? $data['target_weight'] : null;
        $customer->birthday = isset($data['birthday']) ? $data['birthday'] : null;
        $customer->size = isset($data['size']) ? $data['size'] : null;
        $customer->is_newsletter = (isset($data['is_newsletter']) ? $data['is_newsletter'] : 0);
        $customer->email = $data['email'];
        $customer->password = bcrypt($data['password']);
        $customer->photo_url = 'https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png';
        $customer->type_account = $data['type_account'] == "" ? "challenger" : $data['type_account'];
        $customer->facebook_id = isset($data['facebook_id']) ? $data['facebook_id'] : null;
        $customer->google_id = isset($data['google_id']) ? $data['google_id'] : null;
        $customer->is_active = false;
        $customer->location_address = isset($data['commune']) ? $data['commune'] : null;
        $customer->commune = isset($data['commune']) ? $data['commune'] : null;
        $customer->quartier  =  $data['quartier'];
        $customer->location_longitude  =  isset($data['lon']) ? $data['lon'] : null;
        $customer->location_latitude =  isset($data['lat']) ? $data['lat'] : null;
        $customer->discovery_source =  $data['discovery_source'];
        $customer->save();


        if ($customer->type_account == 'coach') {
            $coach = Coach::create([
                'customer_id'  => $customer->id,
                'name'  => $customer->name,
                'email'  => $customer->email,
                'facebook_url'  => $data['facebook_url'],
                'youtube_url'  => $data['youtube_url'],
                'instagram_url'  => $data['instagram_url'],
                'site_web'  => $data['site_web'],
                'discovery_source'  => $data['discovery_source'],
                'gender'  => $customer->gender,
                'is_active' =>  false
            ]);
            if($data['experience'] != null){
                $coach->experience = $data['experience'];
                $coach->save();
            }

            if (isset($data['discipline_id']) && $data['discipline_id'] != null) {
                for ($i = 0; $i < sizeof($data['discipline_id']); $i++) {
                    CoachDiscipline::create([
                        'coach_id' => $coach->id,
                        'discipline_id' => $data['discipline_id'][$i],
                    ]);
                }
            }
            //
            if ($data['qualification_names'] != null) {

                for ($i = 0; $i < sizeof($data['qualification_names']); $i++) {
                    $qualification = new distinction();
                    // $qualification = new Qualification();

                    if (isset($data['qualification_names'][$i]) && $data['qualification_names'][$i] != null) {
                        $qualification->name = $data['qualification_names'][$i];
                    }

                    if (isset($data['qualification_start_dates'][$i]) && $data['qualification_start_dates'][$i] != null) {
                        $qualification->date_debut = $data['qualification_start_dates'][$i];
                    }
                    // if (isset($data['qualification_files'][$i]) && $data['qualification_files'][$i] != null) {
                    //     $qualification->fichier_url = $data['qualification_files'][$i];
                    // }
                    $qualification->coach_id = $coach->id;
                    $qualification->save();
                    //
                    // if (isset('qualification_start_dates')) {

                    //     $image = $request->file('cover_url');
                    //     $pictures = [];
                    //     if ($image != null) {
                    //         $productImage = Imgur::upload($image);
                    //         $productImageLink = $productImage->link();
                    //     }
                    // } else {
                    //     $productImageLink = "";
                    // }

                    // $input['cover_url'] = $productImageLink;
                }

                // Qualification::create([
                //     'name' => $data['qualification_names'][$i],
                //     'date_debut' => $data['qualification_start_dates'][$i],
                //     'coach_id' => $coach->id,
                // ]);
            }
            // notifying coach
            Mail::to($customer->email)->send(new CoachWelcomeMail());
            // Mail::to($data['email'])->send(new CoachWelcomeMail());
        }

        // notifying selfchallenger
        if ($customer->type_account != 'coach') {

            $token = csrf_token();
            $actionUrl = "https://goldenhealth.ci/customer/confirm-challenger-email?customer_email=" . $customer->email . "&token=" . $token . "&csrf-token=" . $token;
            //$actionUrl = env('APP_URL')."/customer/confirm-challenger-email?customer_email=".$customer->email;
            Mail::to($customer->email)->send(new SelfChallengerWelcomeMail($customer, $actionUrl));
        }

        // notifying every admin
        $accountUri = $data['type_account'] == 'partner' ? 'partners' : ($customer->type_account == 'coach' ? 'coaches' : 'customers');

        $email_data = [
            'accountName' => $data['name'],
            'accountType' => $data['type_account'],
            'accountUrl' => env('APP_URL') . "/$accountUri/$customer->id"
        ];
        
        $customerNotification = new CustomerNotifications();

        $customerNotification->title  = "Inscription";
        $customerNotification->title_en  = "Register";
        $customerNotification->subtitle  = "Vous venez de vous inscrire";
        $customerNotification->subtitle_en  = "You register yourself";
        $customerNotification->action  = "";
        $customerNotification->action_by  = "";
        $customerNotification->type_notification  = "waiting";
        $customerNotification->is_read  = false;
        $customerNotification->is_received  = false;
        $customerNotification->user_id  = $customer->id;
        $customerNotification->save();

        // notifying admins
        foreach (User::all() as $user) {
            Mail::to($user->email)->send(new AdminSignupNotificationMail($email_data));
        }

        return $customer;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('customer_frontOffice.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('customer');
    }

    public function confirmChallengerEmail(Request $request)
    {

        $customer_email = $request->get('customer_email');

        $customer  = Customer::where('email', $customer_email)->first();

        if (empty($customer)) {
            return view('errors.404');
        }

        $customer->email_verifed_at = now();
        $customer->is_active = true;
        $customer->activation_date = now();

        $customer->save();

        return redirect($this->redirectTo);
    }
}
