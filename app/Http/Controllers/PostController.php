<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Repositories\PostRepository;
use App\Utils\AdjUtils;
use Flash;
use Illuminate\Http\Request;
use Response;
use Imgur;

class PostController extends AppBaseController
{
    /** @var  PostRepository */
    private $postRepository;

    public function __construct(PostRepository $postRepo)
    {
        $this->postRepository = $postRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Post.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $posts = $this->postRepository->paginate(5);
        $posts = Post::orderBy('id', 'desc')->paginate(5);

        return view('posts.index')
            ->with('posts', $posts);
    }

    /**
     * Show the form for creating a new Post.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('posts.create', compact('categories'));
    }

    /**
     * Store a newly created Post in storage.
     *
     * @param CreatePostRequest $request
     *
     * @return Response
     */
    public function store(CreatePostRequest $request)
    {
        $input = $request->all();

        // $input["shared_link"] = $shortUrl;

        if ($request->file('cover')) {

            $image = $request->file('cover');
            if ($image != null) {
                $postCover = Imgur::upload($image);
                $postCoverLink = $postCover->link();
            }
        } else {
            $postCoverLink = '';
        }

        

        /* if ($request->file('cover')) {

            $image = $request->file('cover');
            if ($image != null) {                   
                $productImage = Imgur::upload($image);
                $productImageLink = $productImage->link();
            }
        } else {
            $productImageLink = '';
        } */



        $input["cover"] = $postCoverLink;

        $post = $this->postRepository->create($input);

        $postToUpdate = Post::find($post->id);

        $longUrl = "http://goldenhealth.adjemincloud.com/blog-article/" . $post->id;
        $shortUrl = AdjUtils::createShortUrl($longUrl);
        $shortUrl = AdjUtils::BASE_URL . '/' . $shortUrl->get('short_url');
        $postToUpdate->shared_link = $shortUrl;
        $postToUpdate->save();


        Flash::success('Post saved successfully.');

        return redirect(route('posts.index'));
    }

    /**
     * Display the specified Post.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $post = $this->postRepository->find($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('posts.index'));
        }

        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified Post.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categories = Category::all();

        $post = $this->postRepository->find($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('posts.index'));
        }

        return view('posts.edit')->with('post', $post)->with('categories', $categories);
    }

    /**
     * Update the specified Post in storage.
     *
     * @param int $id
     * @param UpdatePostRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePostRequest $request)
    {
        $post = $this->postRepository->find($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('posts.index'));
        }

        if ($request->file('cover')) {

            $image = $request->file('cover');
            $pictures = [];
            if ($image != null) {
                $postCover = Imgur::upload($image);
                $postCoverLink = $postCover->link();
            }
        } else {
            $postCoverLink = '';
        }

        $input["cover"] = $postCoverLink;

        $post = $this->postRepository->update($request->all(), $id);

        Flash::success('Post updated successfully.');

        return redirect(route('posts.index'));
    }

    /**
     * Remove the specified Post from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $post = $this->postRepository->find($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('posts.index'));
        }

        $this->postRepository->delete($id);

        Flash::success('Post deleted successfully.');

        return redirect(route('posts.index'));
    }
}
