<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePackSubscriptionAPIRequest;
use App\Http\Requests\API\UpdatePackSubscriptionAPIRequest;
use App\Models\PackSubscription;
use App\Repositories\PackSubscriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PackSubscriptionController
 * @package App\Http\Controllers\API
 */

class PackSubscriptionAPIController extends AppBaseController
{
    /** @var  PackSubscriptionRepository */
    private $packSubscriptionRepository;

    public function __construct(PackSubscriptionRepository $packSubscriptionRepo)
    {
        $this->packSubscriptionRepository = $packSubscriptionRepo;
    }

    /**
     * Display a listing of the PackSubscription.
     * GET|HEAD /packSubscriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $packSubscriptions = $this->packSubscriptionRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($packSubscriptions->toArray(), 'Pack Subscriptions retrieved successfully');
    }

    /**
     * Store a newly created PackSubscription in storage.
     * POST /packSubscriptions
     *
     * @param CreatePackSubscriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePackSubscriptionAPIRequest $request)
    {
        $input = $request->all();

        $packSubscription = $this->packSubscriptionRepository->create($input);

        return $this->sendResponse($packSubscription->toArray(), 'Pack Subscription saved successfully');
    }

    /**
     * Display the specified PackSubscription.
     * GET|HEAD /packSubscriptions/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PackSubscription $packSubscription */
        $packSubscription = $this->packSubscriptionRepository->find($id);

        if (empty($packSubscription)) {
            return $this->sendError('Pack Subscription not found');
        }

        return $this->sendResponse($packSubscription->toArray(), 'Pack Subscription retrieved successfully');
    }

    /**
     * Update the specified PackSubscription in storage.
     * PUT/PATCH /packSubscriptions/{id}
     *
     * @param int $id
     * @param UpdatePackSubscriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePackSubscriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var PackSubscription $packSubscription */
        $packSubscription = $this->packSubscriptionRepository->find($id);

        if (empty($packSubscription)) {
            return $this->sendError('Pack Subscription not found');
        }

        $packSubscription = $this->packSubscriptionRepository->update($input, $id);

        return $this->sendResponse($packSubscription->toArray(), 'PackSubscription updated successfully');
    }

    /**
     * Remove the specified PackSubscription from storage.
     * DELETE /packSubscriptions/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PackSubscription $packSubscription */
        $packSubscription = $this->packSubscriptionRepository->find($id);

        if (empty($packSubscription)) {
            return $this->sendError('Pack Subscription not found');
        }

        $packSubscription->delete();

        return $this->sendSuccess('Pack Subscription deleted successfully');
    }
}
