<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerNotificationsAPIRequest;
use App\Http\Requests\API\UpdateCustomerNotificationsAPIRequest;
use App\Models\CustomerNotifications;
use App\Repositories\CustomerNotificationsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CustomerNotificationsController
 * @package App\Http\Controllers\API
 */

class CustomerNotificationsAPIController extends AppBaseController
{
    /** @var  CustomerNotificationsRepository */
    private $customerNotificationsRepository;

    public function __construct(CustomerNotificationsRepository $customerNotificationsRepo)
    {
        $this->customerNotificationsRepository = $customerNotificationsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/customerNotifications",
     *      summary="Get a listing of the CustomerNotifications.",
     *      tags={"CustomerNotifications"},
     *      description="Get all CustomerNotifications",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CustomerNotifications")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $customerNotifications = $this->customerNotificationsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($customerNotifications->toArray(), 'Customer Notifications retrieved successfully');
    }

    /**
     * @param CreateCustomerNotificationsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/customerNotifications",
     *      summary="Store a newly created CustomerNotifications in storage",
     *      tags={"CustomerNotifications"},
     *      description="Store CustomerNotifications",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CustomerNotifications that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CustomerNotifications")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerNotifications"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
        $requestBody  = $request->json();

        if (!$requestBody->has('meta_data')) {
            return $this->sendError('meta_data is required');
        }

        if (!$requestBody->has('meta_data_id')) {
            return $this->sendError('meta_data_id is required');
        }

        if (!$requestBody->has('type_notification')) {
            return $this->sendError('type_notification is required');
        }

        if (!$requestBody->has('receivers')) {
            return $this->sendError('receivers is required');
        }

        if (!$requestBody->has('data')) {
            return $this->sendError('data is required');
        }

        if (!$requestBody->has('data_id')) {
            return $this->sendError('data_id is required');
        }

        $meta_data = $requestBody->get('meta_data');
        $meta_data_id = $requestBody->get('meta_data_id');
        $type_notification = $requestBody->get('type_notification');
        $receivers = (array)$requestBody->get('receivers');
        $data = $requestBody->get('data');
        $data_id = $requestBody->get('data_id');

        //$input = $request->all();

        $receivers = Collection::make($receivers);
        $receivers->each(function ($receiver)use ($meta_data, $meta_data_id, $data, $data_id){
            MessageNotificationUtils::notifyWithFirestoreMessage($receiver, $meta_data, $meta_data_id, $data, $data_id);
        });

        $customerNotifications = CustomerNotification::where(['meta_data_id' => $meta_data_id])->get();

        return $this->sendResponse($customerNotifications->toArray(), 'Notification sent successfully');

    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/customerNotifications/{id}",
     *      summary="Display the specified CustomerNotifications",
     *      tags={"CustomerNotifications"},
     *      description="Get CustomerNotifications",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerNotifications",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerNotifications"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CustomerNotifications $customerNotifications */
        $customerNotifications = $this->customerNotificationsRepository->find($id);

        if (empty($customerNotifications)) {
            return $this->sendError('Customer Notifications not found');
        }

        return $this->sendResponse($customerNotifications->toArray(), 'Customer Notifications retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCustomerNotificationsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/customerNotifications/{id}",
     *      summary="Update the specified CustomerNotifications in storage",
     *      tags={"CustomerNotifications"},
     *      description="Update CustomerNotifications",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerNotifications",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CustomerNotifications that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CustomerNotifications")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerNotifications"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCustomerNotificationsAPIRequest $request)
    {
        $input = $request->all();

        /** @var CustomerNotifications $customerNotifications */
        $customerNotifications = $this->customerNotificationsRepository->find($id);

        if (empty($customerNotifications)) {
            return $this->sendError('Customer Notifications not found');
        }

        $customerNotifications = $this->customerNotificationsRepository->update($input, $id);

        return $this->sendResponse($customerNotifications->toArray(), 'CustomerNotifications updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/customerNotifications/{id}",
     *      summary="Remove the specified CustomerNotifications from storage",
     *      tags={"CustomerNotifications"},
     *      description="Delete CustomerNotifications",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerNotifications",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CustomerNotifications $customerNotifications */
        $customerNotifications = $this->customerNotificationsRepository->find($id);

        if (empty($customerNotifications)) {
            return $this->sendError('Customer Notifications not found');
        }

        $customerNotifications->delete();

        return $this->sendResponse($id, 'Customer Notifications deleted successfully');
    }
}
