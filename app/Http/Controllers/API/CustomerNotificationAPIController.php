<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerNotificationAPIRequest;
use App\Http\Requests\API\UpdateCustomerNotificationAPIRequest;
use App\Models\CustomerNotification;
use App\Repositories\CustomerNotificationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CustomerNotificationController
 * @package App\Http\Controllers\API
 */

class CustomerNotificationAPIController extends AppBaseController
{
    /** @var  CustomerNotificationRepository */
    private $customerNotificationRepository;

    public function __construct(CustomerNotificationRepository $customerNotificationRepo)
    {
        $this->customerNotificationRepository = $customerNotificationRepo;
    }

    /**
     * Display a listing of the CustomerNotification.
     * GET|HEAD /customerNotifications
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $customerNotifications = $this->customerNotificationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($customerNotifications->toArray(), 'Customer Notifications retrieved successfully');
    }

    /**
     * Store a newly created CustomerNotification in storage.
     * POST /customerNotifications
     *
     * @param CreateCustomerNotificationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerNotificationAPIRequest $request)
    {
        $input = $request->all();

        $customerNotification = $this->customerNotificationRepository->create($input);

        return $this->sendResponse($customerNotification->toArray(), 'Customer Notification saved successfully');
    }

    /**
     * Display the specified CustomerNotification.
     * GET|HEAD /customerNotifications/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CustomerNotification $customerNotification */
        $customerNotification = $this->customerNotificationRepository->find($id);

        if (empty($customerNotification)) {
            return $this->sendError('Customer Notification not found');
        }

        return $this->sendResponse($customerNotification->toArray(), 'Customer Notification retrieved successfully');
    }

    /**
     * Update the specified CustomerNotification in storage.
     * PUT/PATCH /customerNotifications/{id}
     *
     * @param int $id
     * @param UpdateCustomerNotificationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerNotificationAPIRequest $request)
    {
        $input = $request->all();

        /** @var CustomerNotification $customerNotification */
        $customerNotification = $this->customerNotificationRepository->find($id);

        if (empty($customerNotification)) {
            return $this->sendError('Customer Notification not found');
        }

        $customerNotification = $this->customerNotificationRepository->update($input, $id);

        return $this->sendResponse($customerNotification->toArray(), 'CustomerNotification updated successfully');
    }

    /**
     * Remove the specified CustomerNotification from storage.
     * DELETE /customerNotifications/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CustomerNotification $customerNotification */
        $customerNotification = $this->customerNotificationRepository->find($id);

        if (empty($customerNotification)) {
            return $this->sendError('Customer Notification not found');
        }

        $customerNotification->delete();

        return $this->sendSuccess('Customer Notification deleted successfully');
    }
}
