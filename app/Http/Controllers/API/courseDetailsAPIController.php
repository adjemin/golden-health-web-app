<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatecourseDetailsAPIRequest;
use App\Http\Requests\API\UpdatecourseDetailsAPIRequest;
use App\Models\CourseDetail;
use App\Repositories\courseDetailsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class courseDetailsController
 * @package App\Http\Controllers\API
 */

class courseDetailsAPIController extends AppBaseController
{
    /** @var  courseDetailsRepository */
    private $courseDetailsRepository;

    public function __construct(courseDetailsRepository $courseDetailsRepo)
    {
        $this->courseDetailsRepository = $courseDetailsRepo;
    }

    /**
     * Display a listing of the courseDetails.
     * GET|HEAD /courseDetails
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $courseDetails = $this->courseDetailsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($courseDetails->toArray(), 'Course Details retrieved successfully');
    }

    /**
     * Store a newly created courseDetails in storage.
     * POST /courseDetails
     *
     * @param CreatecourseDetailsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatecourseDetailsAPIRequest $request)
    {
        $input = $request->all();

        $courseDetails = $this->courseDetailsRepository->create($input);

        return $this->sendResponse($courseDetails->toArray(), 'Course Details saved successfully');
    }

    /**
     * Display the specified courseDetails.
     * GET|HEAD /courseDetails/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var courseDetails $courseDetails */
        $courseDetails = $this->courseDetailsRepository->find($id);

        if (empty($courseDetails)) {
            return $this->sendError('Course Details not found');
        }

        return $this->sendResponse($courseDetails->toArray(), 'Course Details retrieved successfully');
    }

    /**
     * Update the specified courseDetails in storage.
     * PUT/PATCH /courseDetails/{id}
     *
     * @param int $id
     * @param UpdatecourseDetailsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecourseDetailsAPIRequest $request)
    {
        $input = $request->all();

        /** @var courseDetails $courseDetails */
        $courseDetails = $this->courseDetailsRepository->find($id);

        if (empty($courseDetails)) {
            return $this->sendError('Course Details not found');
        }

        $courseDetails = $this->courseDetailsRepository->update($input, $id);

        return $this->sendResponse($courseDetails->toArray(), 'courseDetails updated successfully');
    }

    /**
     * Remove the specified courseDetails from storage.
     * DELETE /courseDetails/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var courseDetails $courseDetails */
        $courseDetails = $this->courseDetailsRepository->find($id);

        if (empty($courseDetails)) {
            return $this->sendError('Course Details not found');
        }

        $courseDetails->delete();

        return $this->sendSuccess('Course Details deleted successfully');
    }
}
