<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLieuCourseAPIRequest;
use App\Http\Requests\API\UpdateLieuCourseAPIRequest;
use App\Models\LieuCourse;
use App\Repositories\LieuCourseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class LieuCourseController
 * @package App\Http\Controllers\API
 */

class LieuCourseAPIController extends AppBaseController
{
    /** @var  LieuCourseRepository */
    private $lieuCourseRepository;

    public function __construct(LieuCourseRepository $lieuCourseRepo)
    {
        $this->lieuCourseRepository = $lieuCourseRepo;
    }

    /**
     * Display a listing of the LieuCourse.
     * GET|HEAD /lieuCourses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $lieuCourses = $this->lieuCourseRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($lieuCourses->toArray(), 'Lieu Courses retrieved successfully');
    }

    /**
     * Store a newly created LieuCourse in storage.
     * POST /lieuCourses
     *
     * @param CreateLieuCourseAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLieuCourseAPIRequest $request)
    {
        $input = $request->all();

        $lieuCourse = $this->lieuCourseRepository->create($input);

        return $this->sendResponse($lieuCourse->toArray(), 'Lieu Course saved successfully');
    }

    /**
     * Display the specified LieuCourse.
     * GET|HEAD /lieuCourses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var LieuCourse $lieuCourse */
        $lieuCourse = $this->lieuCourseRepository->find($id);

        if (empty($lieuCourse)) {
            return $this->sendError('Lieu Course not found');
        }

        return $this->sendResponse($lieuCourse->toArray(), 'Lieu Course retrieved successfully');
    }

    /**
     * Update the specified LieuCourse in storage.
     * PUT/PATCH /lieuCourses/{id}
     *
     * @param int $id
     * @param UpdateLieuCourseAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLieuCourseAPIRequest $request)
    {
        $input = $request->all();

        /** @var LieuCourse $lieuCourse */
        $lieuCourse = $this->lieuCourseRepository->find($id);

        if (empty($lieuCourse)) {
            return $this->sendError('Lieu Course not found');
        }

        $lieuCourse = $this->lieuCourseRepository->update($input, $id);

        return $this->sendResponse($lieuCourse->toArray(), 'LieuCourse updated successfully');
    }

    /**
     * Remove the specified LieuCourse from storage.
     * DELETE /lieuCourses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var LieuCourse $lieuCourse */
        $lieuCourse = $this->lieuCourseRepository->find($id);

        if (empty($lieuCourse)) {
            return $this->sendError('Lieu Course not found');
        }

        $lieuCourse->delete();

        return $this->sendSuccess('Lieu Course deleted successfully');
    }
}
