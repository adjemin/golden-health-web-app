<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCoachCaracteristiqueAPIRequest;
use App\Http\Requests\API\UpdateCoachCaracteristiqueAPIRequest;
use App\Models\CoachCaracteristique;
use App\Repositories\CoachCaracteristiqueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CoachCaracteristiqueController
 * @package App\Http\Controllers\API
 */

class CoachCaracteristiqueAPIController extends AppBaseController
{
    /** @var  CoachCaracteristiqueRepository */
    private $coachCaracteristiqueRepository;

    public function __construct(CoachCaracteristiqueRepository $coachCaracteristiqueRepo)
    {
        $this->coachCaracteristiqueRepository = $coachCaracteristiqueRepo;
    }

    /**
     * Display a listing of the CoachCaracteristique.
     * GET|HEAD /coachCaracteristiques
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $coachCaracteristiques = $this->coachCaracteristiqueRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($coachCaracteristiques->toArray(), 'Coach Caracteristiques retrieved successfully');
    }

    /**
     * Store a newly created CoachCaracteristique in storage.
     * POST /coachCaracteristiques
     *
     * @param CreateCoachCaracteristiqueAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCoachCaracteristiqueAPIRequest $request)
    {
        $input = $request->all();

        $coachCaracteristique = $this->coachCaracteristiqueRepository->create($input);

        return $this->sendResponse($coachCaracteristique->toArray(), 'Coach Caracteristique saved successfully');
    }

    /**
     * Display the specified CoachCaracteristique.
     * GET|HEAD /coachCaracteristiques/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CoachCaracteristique $coachCaracteristique */
        $coachCaracteristique = $this->coachCaracteristiqueRepository->find($id);

        if (empty($coachCaracteristique)) {
            return $this->sendError('Coach Caracteristique not found');
        }

        return $this->sendResponse($coachCaracteristique->toArray(), 'Coach Caracteristique retrieved successfully');
    }

    /**
     * Update the specified CoachCaracteristique in storage.
     * PUT/PATCH /coachCaracteristiques/{id}
     *
     * @param int $id
     * @param UpdateCoachCaracteristiqueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCoachCaracteristiqueAPIRequest $request)
    {
        $input = $request->all();

        /** @var CoachCaracteristique $coachCaracteristique */
        $coachCaracteristique = $this->coachCaracteristiqueRepository->find($id);

        if (empty($coachCaracteristique)) {
            return $this->sendError('Coach Caracteristique not found');
        }

        $coachCaracteristique = $this->coachCaracteristiqueRepository->update($input, $id);

        return $this->sendResponse($coachCaracteristique->toArray(), 'CoachCaracteristique updated successfully');
    }

    /**
     * Remove the specified CoachCaracteristique from storage.
     * DELETE /coachCaracteristiques/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CoachCaracteristique $coachCaracteristique */
        $coachCaracteristique = $this->coachCaracteristiqueRepository->find($id);

        if (empty($coachCaracteristique)) {
            return $this->sendError('Coach Caracteristique not found');
        }

        $coachCaracteristique->delete();

        return $this->sendSuccess('Coach Caracteristique deleted successfully');
    }
}
