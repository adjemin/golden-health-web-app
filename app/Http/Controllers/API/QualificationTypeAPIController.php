<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateQualificationTypeAPIRequest;
use App\Http\Requests\API\UpdateQualificationTypeAPIRequest;
use App\Models\QualificationType;
use App\Repositories\QualificationTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class QualificationTypeController
 * @package App\Http\Controllers\API
 */

class QualificationTypeAPIController extends AppBaseController
{
    /** @var  QualificationTypeRepository */
    private $qualificationTypeRepository;

    public function __construct(QualificationTypeRepository $qualificationTypeRepo)
    {
        $this->qualificationTypeRepository = $qualificationTypeRepo;
    }

    /**
     * Display a listing of the QualificationType.
     * GET|HEAD /qualificationTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $qualificationTypes = $this->qualificationTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($qualificationTypes->toArray(), 'Qualification Types retrieved successfully');
    }

    /**
     * Store a newly created QualificationType in storage.
     * POST /qualificationTypes
     *
     * @param CreateQualificationTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateQualificationTypeAPIRequest $request)
    {
        $input = $request->all();

        $qualificationType = $this->qualificationTypeRepository->create($input);

        return $this->sendResponse($qualificationType->toArray(), 'Qualification Type saved successfully');
    }

    /**
     * Display the specified QualificationType.
     * GET|HEAD /qualificationTypes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var QualificationType $qualificationType */
        $qualificationType = $this->qualificationTypeRepository->find($id);

        if (empty($qualificationType)) {
            return $this->sendError('Qualification Type not found');
        }

        return $this->sendResponse($qualificationType->toArray(), 'Qualification Type retrieved successfully');
    }

    /**
     * Update the specified QualificationType in storage.
     * PUT/PATCH /qualificationTypes/{id}
     *
     * @param int $id
     * @param UpdateQualificationTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQualificationTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var QualificationType $qualificationType */
        $qualificationType = $this->qualificationTypeRepository->find($id);

        if (empty($qualificationType)) {
            return $this->sendError('Qualification Type not found');
        }

        $qualificationType = $this->qualificationTypeRepository->update($input, $id);

        return $this->sendResponse($qualificationType->toArray(), 'QualificationType updated successfully');
    }

    /**
     * Remove the specified QualificationType from storage.
     * DELETE /qualificationTypes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var QualificationType $qualificationType */
        $qualificationType = $this->qualificationTypeRepository->find($id);

        if (empty($qualificationType)) {
            return $this->sendError('Qualification Type not found');
        }

        $qualificationType->delete();

        return $this->sendSuccess('Qualification Type deleted successfully');
    }
}
