<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateObjectiveAPIRequest;
use App\Http\Requests\API\UpdateObjectiveAPIRequest;
use App\Models\Objective;
use App\Repositories\ObjectiveRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ObjectiveController
 * @package App\Http\Controllers\API
 */

class ObjectiveAPIController extends AppBaseController
{
    /** @var  ObjectiveRepository */
    private $objectiveRepository;

    public function __construct(ObjectiveRepository $objectiveRepo)
    {
        $this->objectiveRepository = $objectiveRepo;
    }

    /**
     * Display a listing of the Objective.
     * GET|HEAD /objectives
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $objectives = $this->objectiveRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($objectives->toArray(), 'Objectives retrieved successfully');
    }

    /**
     * Store a newly created Objective in storage.
     * POST /objectives
     *
     * @param CreateObjectiveAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateObjectiveAPIRequest $request)
    {
        $input = $request->all();

        $objective = $this->objectiveRepository->create($input);

        return $this->sendResponse($objective->toArray(), 'Objective saved successfully');
    }

    /**
     * Display the specified Objective.
     * GET|HEAD /objectives/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Objective $objective */
        $objective = $this->objectiveRepository->find($id);

        if (empty($objective)) {
            return $this->sendError('Objective not found');
        }

        return $this->sendResponse($objective->toArray(), 'Objective retrieved successfully');
    }

    /**
     * Update the specified Objective in storage.
     * PUT/PATCH /objectives/{id}
     *
     * @param int $id
     * @param UpdateObjectiveAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateObjectiveAPIRequest $request)
    {
        $input = $request->all();

        /** @var Objective $objective */
        $objective = $this->objectiveRepository->find($id);

        if (empty($objective)) {
            return $this->sendError('Objective not found');
        }

        $objective = $this->objectiveRepository->update($input, $id);

        return $this->sendResponse($objective->toArray(), 'Objective updated successfully');
    }

    /**
     * Remove the specified Objective from storage.
     * DELETE /objectives/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Objective $objective */
        $objective = $this->objectiveRepository->find($id);

        if (empty($objective)) {
            return $this->sendError('Objective not found');
        }

        $objective->delete();

        return $this->sendSuccess('Objective deleted successfully');
    }
}
