<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAdvertAPIRequest;
use App\Http\Requests\API\UpdateAdvertAPIRequest;
use App\Models\Advert;
use App\Repositories\AdvertRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class AdvertController
 * @package App\Http\Controllers\API
 */

class AdvertAPIController extends AppBaseController
{
    /** @var  AdvertRepository */
    private $advertRepository;

    public function __construct(AdvertRepository $advertRepo)
    {
        $this->advertRepository = $advertRepo;
    }

    /**
     * Display a listing of the Advert.
     * GET|HEAD /adverts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $adverts = $this->advertRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($adverts->toArray(), 'Adverts retrieved successfully');
    }

    /**
     * Store a newly created Advert in storage.
     * POST /adverts
     *
     * @param CreateAdvertAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAdvertAPIRequest $request)
    {
        $input = $request->all();

        $advert = $this->advertRepository->create($input);

        return $this->sendResponse($advert->toArray(), 'Advert saved successfully');
    }

    /**
     * Display the specified Advert.
     * GET|HEAD /adverts/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Advert $advert */
        $advert = $this->advertRepository->find($id);

        if (empty($advert)) {
            return $this->sendError('Advert not found');
        }

        return $this->sendResponse($advert->toArray(), 'Advert retrieved successfully');
    }

    /**
     * Update the specified Advert in storage.
     * PUT/PATCH /adverts/{id}
     *
     * @param int $id
     * @param UpdateAdvertAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAdvertAPIRequest $request)
    {
        $input = $request->all();

        /** @var Advert $advert */
        $advert = $this->advertRepository->find($id);

        if (empty($advert)) {
            return $this->sendError('Advert not found');
        }

        $advert = $this->advertRepository->update($input, $id);

        return $this->sendResponse($advert->toArray(), 'Advert updated successfully');
    }

    /**
     * Remove the specified Advert from storage.
     * DELETE /adverts/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Advert $advert */
        $advert = $this->advertRepository->find($id);

        if (empty($advert)) {
            return $this->sendError('Advert not found');
        }

        $advert->delete();

        return $this->sendSuccess('Advert deleted successfully');
    }
}
