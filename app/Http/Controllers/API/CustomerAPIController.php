<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerAPIRequest;
use App\Http\Requests\API\UpdateCustomerAPIRequest;
use App\Customer;
use App\Models\Coach;
use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Response;
use App\Mail\SelfChallengerWelcomeMail;
use App\Mail\AdminSignupNotificationMail;
use App\Mail\CoachWelcomeMail;
use App\Models\CustomerNotifications;
use App\Utils\CustomerNotificationUtils;

/**
 * Class CustomerController
 * @package App\Http\Controllers\API
 */

class CustomerAPIController extends AppBaseController
{
    /** @var  CustomerRepository */
    private $customerRepository;

    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepository = $customerRepo;
    }

    /**
     * Display a listing of the Customer.
     * GET|HEAD /customers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $customers = $this->customerRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($customers->toArray(), 'Customers retrieved successfully');
    }

    /**
     * Store a newly created Customer in storage.
     * POST /customers
     *
     * @param CreateCustomerAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerAPIRequest $request)
    {
        
        $input = $request->all();


        if(empty($request->email)){
            return $this->sendError('Email obligatoire', 400);
        }

        if(empty($request->last_name)){
            return $this->sendError('Nom obligatoire', 400);
        }

        if(empty($request->first_name)){
            return $this->sendError('Prénoms obligatoire', 400);
        }

        if(empty($request->gender)){
            return $this->sendError('Sexe obligatoire', 400);
        }

        if(empty($request->facebook_id) && empty($request->google_id)){

            if(empty($request->password)){
                return $this->sendError('Mot de passe obligatoire', 400);
            }else{

                $input["password"] = Hash::make(trim($request->password));
            }

        }

    
        

        $customer = Customer::where('email', $request->email)->first();

        if (!empty($customer)) {
            return $this->sendError('Email déjà utilisé', 400);
        }
        

        $input["last_name"] = ucfirst(strtolower($request->last_name));
        $input["first_name"] = ucfirst(strtolower($request->first_name));
        $input["name"] = $input["last_name"]." ".$input["first_name"];

       
       //

       $type_account = $request->get('type_account', "challenger");

       $input['type_account'] = $type_account;


        if(array_key_exists ("photo_url" , $input )){
            if (is_null($input["photo_url"]) || empty($input["photo_url"]) ) {
                $input["photo_url"] = "https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png";
            }
        }
 

        $input["is_active"] = false; 
        $input["is_cgu"] = true;
        $input["is_newsletter"] = true;


        $customer = $this->customerRepository->create($input);

        if(!empty($request->facebook_id)){
            $customer->email_verifed_at = now();
            $customer->is_active = true;
            $customer->activation_date = now();
            $customer->save();
        }

        if(!empty($request->google_id)){
            $customer->email_verifed_at = now();
            $customer->is_active = true;
            $customer->activation_date = now();
            $customer->save();
        }

        $data = [
            "customer" =>  $customer->toArray()
        ];

        if($type_account == "coach"){
            $coach = Coach::create([
                "customer_id" => $customer->id,
                'facebook_url' => $request->get('facebook_url'),
                'instagram_url' => $request->get('instagram_url'),
                'youtube_url' => $request->get('youtube_url'),
                'site_web' => $request->get('site_web'),
                "is_active" => false,
                "bio" => ""
            ]);

            $data["coach"] = $coach;

            // notifying coach
            Mail::to($customer->email)->send(new CoachWelcomeMail());
        }


         // notifying selfchallenger
         if($customer->type_account == "challenger"){

            $token = csrf_token();
            $actionUrl = env('APP_URL')."/customer/confirm-challenger-email?customer_email=".$customer->email."&token=".$token."&csrf-token=".$token;
            Mail::to($customer->email)->send(new SelfChallengerWelcomeMail($customer, $actionUrl));
        }


       
        $customerNotification = new CustomerNotifications();
                
        $customerNotification->title  = "Inscription";
        $customerNotification->title_en  = "Register";
        $customerNotification->subtitle  = "Vous venez de vous inscrire";
        $customerNotification->subtitle_en  = "You register yourself";
        $customerNotification->action  = "";
        $customerNotification->action_by  = "";
        $customerNotification->type_notification  = "waiting";
        $customerNotification->is_read  = false;
        $customerNotification->is_received  = false;
        $customerNotification->user_id  = $customer->id;
        $customerNotification->save();

        CustomerNotificationUtils::notify($customerNotification);

        return $this->sendResponse($data, 'Customer saved successfully');
    }

    /**
     * Display the specified Customer.
     * GET|HEAD /customers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Customer $customer */
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            return $this->sendError('Customer not found');
        }

        return $this->sendResponse($customer->toArray(), 'Customer retrieved successfully');
    }

    /**
     * Update the specified Customer in storage.
     * PUT/PATCH /customers/{id}
     *
     * @param int $id
     * @param UpdateCustomerAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerAPIRequest $request)
    {
        $input = $request->all();

        /** @var Customer $customer */
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            return $this->sendError('Customer not found');
        }

        $customer = $this->customerRepository->update($input, $id);
        

        $data = [
            "customer" =>  $customer->toArray()
        ];


        if($customer->type_account == "coach"){
            $coach = Coach::where([ "customer_id" => $customer->id])->first();

            if(!empty($coach)){
                $inputCoach  = [];
                
                if($request->has("bio")){
                    $inputCoach['bio'] = $request->get("bio");
                }
                
                if($request->has("rating")){
                    $inputCoach['rating'] = $request->get("rating");
                }

                if($request->has("experience")){
                    $inputCoach['experience'] = $request->get("experience");
                }
                
                if($request->has("qualification_id")){
                    $inputCoach['qualification_id'] = $request->get("qualification_id");
                }

                if($request->has("facebook_url")){
                    $inputCoach['facebook_url'] = $request->get("facebook_url");
                }

                if($request->has("instagram_url")){
                    $inputCoach['instagram_url'] = $request->get("instagram_url");
                }

                if($request->has("youtube_url")){
                    $inputCoach['youtube_url'] = $request->get("youtube_url");
                }

                if($request->has("site_web")){
                    $inputCoach['site_web'] = $request->get("site_web");
                }

           

                $coach = Coach::where('is_active', true)
                ->where('customer_id', $customer->id)
                ->update($inputCoach);
    
                $data["coach"] = $coach;

            }

        }

        $coachs = Coach::where([ "customer_id" => $customer->id])->get();

        if(!empty($coachs)){
            $coach = Coach::where([ "customer_id" => $customer->id])->first();

            $data["coach"] = $coach;
        }


        return $this->sendResponse($data, 'Customer updated successfully');

    }

    /**
     * Remove the specified Customer from storage.
     * DELETE /customers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Customer $customer */
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            return $this->sendError('Customer not found');
        }

        $customer->delete();

        return $this->sendSuccess('Customer deleted successfully');
    }

    public function login(Request $request){

        if(empty($request->email)){
            return $this->sendError('Email obligatoire', 400);
        }

        if(empty($request->password)){
            return $this->sendError('Mot de passe obligatoire', 400);
        }

        $customer = Customer::where('email', $request->email)->first();

        if (empty($customer)) {
            return $this->sendError('Aucun compte existe avec cette adresse email', 404);
        }

        if(!Hash::check($request->password, $customer->password)){
            return $this->sendError('Votre mot de passe est incorrect', 401);
        }

        if(is_null($customer->is_active)|| $customer->is_active == false){
            return $this->sendError('Votre compte est inactif', 401);
        }

        if($customer->type_account== "challenger"){

            if(empty($customer->email_verifed_at) ){
                return $this->sendError('Confirmez votre email afin de vous connecter', 401);
            }

           
        }

        $data = [
            "customer" =>  $customer->toArray()
        ];

        if($customer->type_account == "coach"){
            $coach = Coach::where([ "customer_id" => $customer->id])->first();


            if($coach != null){
                if(is_null($coach->is_active) || $coach->is_active  == false){
                    return $this->sendError('Votre compte est inactif. Contactez les administrateurs.', 401);
                }

                $data["coach"] = $coach;
            }

           

        }



        return $this->sendResponse($data, 'Customer updated successfully');

    }

    public function loginWithSocial(Request $request){

        if(empty($request->social_id)){
            return $this->sendError('social_id obligatoire', 400);
        }

        if(empty($request->social_network)){
            return $this->sendError('social_network obligatoire', 400);
        }

        $customer = null;

        if($request->social_network == "google"){
            $customer = Customer::where('google_id', $request->social_id)->first();
        }

        if($request->social_network == "facebook"){
            $customer = Customer::where('facebook_id', $request->social_id)->first();
        }

        if(is_null($customer) || empty($customer)){
            $customer = Customer::where('email', $request->email)->first();
        }

        if (empty($customer)) {
            return $this->sendError('Aucun compte existe', 404);
        }



        $data = [
            "customer" =>  $customer->toArray()
        ];


        if($customer->type_account == "coach"){
            $coach = Coach::where([ "customer_id" => $customer->id])->first();

            if($coach != null){
                $data["coach"] = $coach;
            }
        }


        return $this->sendResponse($data, 'Customer updated successfully');

    }

    public function statistique($customerId){
        $customer = Customer::find($customerId);

        if(is_null($customer) || empty($customer)){
            return $this->sendError("Utilisateur n'existe pas");
        }

        if($customer->is_coach){
            $data = [
                'passed'    =>  $customer->is_coach->pastCourseCount(),
                'points'    =>  $customer->is_coach->goldenPoint(),
                'left'  =>  $customer->is_coach->reservationLeftCount()
            ];
        }else{
            $passed = $customer->booking()->where('status', 4)->count();

            $data = [
                'passed'    =>  $passed,
                'points'    =>  $passed * 2,
                'left'  =>  $customer->booking()->where('coach_confirm', 0)->whereIn('status_name', ['paid', 'completed','confirmed'])->count()
            ];
        }

        return $this->sendResponse($data, "data retrieved successfully");
    }  

    public function coaches($customerId){
        $coaches = \App\Models\Customer::find($customerId)->coaches();
        return $this->sendresponse($coaches ,"coaches retrieved successfully");
    }

    public function coupons($customerId){
        $coupons = \App\Models\Customer::find($customerId)->coupons();
        return $this->sendresponse($coupons ,"coupons retrieved successfully");
    }

    public function wishlist($customerId){
        $wishlist = \App\Customer::find($customerId)->wishlistedProducts;
        return $this->sendresponse($wishlist ,"wishlist retrieved successfully");
    }

    public function diplomes($coachId){
        $diplome = \App\Models\Diplome::where('coach_id',$coachId)->get();
        return $this->sendresponse($diplome ,"diplomes retrieved successfully");
    }

    // public function passwordForget($customerId){

    // }
}
