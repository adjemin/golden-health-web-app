<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateExperienceAPIRequest;
use App\Http\Requests\API\UpdateExperienceAPIRequest;
use App\Models\Experience;
use App\Repositories\ExperienceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ExperienceController
 * @package App\Http\Controllers\API
 */

class ExperienceAPIController extends AppBaseController
{
    /** @var  ExperienceRepository */
    private $experienceRepository;

    public function __construct(ExperienceRepository $experienceRepo)
    {
        $this->experienceRepository = $experienceRepo;
    }

    /**
     * Display a listing of the Experience.
     * GET|HEAD /experiences
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $experiences = $this->experienceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($experiences->toArray(), 'Experiences retrieved successfully');
    }

    /**
     * Store a newly created Experience in storage.
     * POST /experiences
     *
     * @param CreateExperienceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateExperienceAPIRequest $request)
    {
        $input = $request->all();

        $experience = $this->experienceRepository->create($input);

        return $this->sendResponse($experience->toArray(), 'Experience saved successfully');
    }

    /**
     * Display the specified Experience.
     * GET|HEAD /experiences/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Experience $experience */
        $experience = $this->experienceRepository->find($id);

        if (empty($experience)) {
            return $this->sendError('Experience not found');
        }

        return $this->sendResponse($experience->toArray(), 'Experience retrieved successfully');
    }

    /**
     * Update the specified Experience in storage.
     * PUT/PATCH /experiences/{id}
     *
     * @param int $id
     * @param UpdateExperienceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateExperienceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Experience $experience */
        $experience = $this->experienceRepository->find($id);

        if (empty($experience)) {
            return $this->sendError('Experience not found');
        }

        $experience = $this->experienceRepository->update($input, $id);

        return $this->sendResponse($experience->toArray(), 'Experience updated successfully');
    }

    /**
     * Remove the specified Experience from storage.
     * DELETE /experiences/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Experience $experience */
        $experience = $this->experienceRepository->find($id);

        if (empty($experience)) {
            return $this->sendError('Experience not found');
        }

        $experience->delete();

        return $this->sendSuccess('Experience deleted successfully');
    }
}
