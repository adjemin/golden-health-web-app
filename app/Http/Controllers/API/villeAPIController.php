<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatevilleAPIRequest;
use App\Http\Requests\API\UpdatevilleAPIRequest;
use App\Models\ville;
use App\Repositories\villeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class villeController
 * @package App\Http\Controllers\API
 */

class villeAPIController extends AppBaseController
{
    /** @var  villeRepository */
    private $villeRepository;

    public function __construct(villeRepository $villeRepo)
    {
        $this->villeRepository = $villeRepo;
    }

    /**
     * Display a listing of the ville.
     * GET|HEAD /villes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $villes = $this->villeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($villes->toArray(), 'Villes retrieved successfully');
    }

    /**
     * Store a newly created ville in storage.
     * POST /villes
     *
     * @param CreatevilleAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatevilleAPIRequest $request)
    {
        $input = $request->all();

        $ville = $this->villeRepository->create($input);

        return $this->sendResponse($ville->toArray(), 'Ville saved successfully');
    }

    /**
     * Display the specified ville.
     * GET|HEAD /villes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ville $ville */
        $ville = $this->villeRepository->find($id);

        if (empty($ville)) {
            return $this->sendError('Ville not found');
        }

        return $this->sendResponse($ville->toArray(), 'Ville retrieved successfully');
    }

    /**
     * Update the specified ville in storage.
     * PUT/PATCH /villes/{id}
     *
     * @param int $id
     * @param UpdatevilleAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatevilleAPIRequest $request)
    {
        $input = $request->all();

        /** @var ville $ville */
        $ville = $this->villeRepository->find($id);

        if (empty($ville)) {
            return $this->sendError('Ville not found');
        }

        $ville = $this->villeRepository->update($input, $id);

        return $this->sendResponse($ville->toArray(), 'ville updated successfully');
    }

    /**
     * Remove the specified ville from storage.
     * DELETE /villes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ville $ville */
        $ville = $this->villeRepository->find($id);

        if (empty($ville)) {
            return $this->sendError('Ville not found');
        }

        $ville->delete();

        return $this->sendSuccess('Ville deleted successfully');
    }
}
