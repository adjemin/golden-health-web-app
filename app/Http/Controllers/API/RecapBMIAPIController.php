<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRecapBMIAPIRequest;
use App\Http\Requests\API\UpdateRecapBMIAPIRequest;
use App\Models\RecapBMI;
use App\Repositories\RecapBMIRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class RecapBMIController
 * @package App\Http\Controllers\API
 */

class RecapBMIAPIController extends AppBaseController
{
    /** @var  RecapBMIRepository */
    private $recapBMIRepository;

    public function __construct(RecapBMIRepository $recapBMIRepo)
    {
        $this->recapBMIRepository = $recapBMIRepo;
    }

    /**
     * Display a listing of the RecapBMI.
     * GET|HEAD /recapBMIs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $recapBMIs = $this->recapBMIRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($recapBMIs->toArray(), 'Recap B M Is retrieved successfully');
    }

    /**
     * Store a newly created RecapBMI in storage.
     * POST /recapBMIs
     *
     * @param CreateRecapBMIAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRecapBMIAPIRequest $request)
    {
        $input = $request->all();

        $recapBMI = $this->recapBMIRepository->create($input);

        return $this->sendResponse($recapBMI->toArray(), 'Recap B M I saved successfully');
    }

    /**
     * Display the specified RecapBMI.
     * GET|HEAD /recapBMIs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RecapBMI $recapBMI */
        $recapBMI = $this->recapBMIRepository->find($id);

        if (empty($recapBMI)) {
            return $this->sendError('Recap B M I not found');
        }

        return $this->sendResponse($recapBMI->toArray(), 'Recap B M I retrieved successfully');
    }

    /**
     * Update the specified RecapBMI in storage.
     * PUT/PATCH /recapBMIs/{id}
     *
     * @param int $id
     * @param UpdateRecapBMIAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRecapBMIAPIRequest $request)
    {
        $input = $request->all();

        /** @var RecapBMI $recapBMI */
        $recapBMI = $this->recapBMIRepository->find($id);

        if (empty($recapBMI)) {
            return $this->sendError('Recap B M I not found');
        }

        $recapBMI = $this->recapBMIRepository->update($input, $id);

        return $this->sendResponse($recapBMI->toArray(), 'RecapBMI updated successfully');
    }

    /**
     * Remove the specified RecapBMI from storage.
     * DELETE /recapBMIs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RecapBMI $recapBMI */
        $recapBMI = $this->recapBMIRepository->find($id);

        if (empty($recapBMI)) {
            return $this->sendError('Recap B M I not found');
        }

        $recapBMI->delete();

        return $this->sendSuccess('Recap B M I deleted successfully');
    }
}
