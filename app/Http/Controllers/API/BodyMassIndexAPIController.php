<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBodyMassIndexAPIRequest;
use App\Http\Requests\API\UpdateBodyMassIndexAPIRequest;
use App\Models\BodyMassIndex;
use App\Repositories\BodyMassIndexRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class BodyMassIndexController
 * @package App\Http\Controllers\API
 */

class BodyMassIndexAPIController extends AppBaseController
{
    /** @var  BodyMassIndexRepository */
    private $bodyMassIndexRepository;

    public function __construct(BodyMassIndexRepository $bodyMassIndexRepo)
    {
        $this->bodyMassIndexRepository = $bodyMassIndexRepo;
    }

    /**
     * Display a listing of the BodyMassIndex.
     * GET|HEAD /bodyMassIndices
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $bodyMassIndices = $this->bodyMassIndexRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($bodyMassIndices->toArray(), 'Body Mass Indices retrieved successfully');
    }

    /**
     * Store a newly created BodyMassIndex in storage.
     * POST /bodyMassIndices
     *
     * @param CreateBodyMassIndexAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBodyMassIndexAPIRequest $request)
    {
        $input = $request->all();

        $bodyMassIndex = $this->bodyMassIndexRepository->create($input);

        return $this->sendResponse($bodyMassIndex->toArray(), 'Body Mass Index saved successfully');
    }

    /**
     * Display the specified BodyMassIndex.
     * GET|HEAD /bodyMassIndices/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BodyMassIndex $bodyMassIndex */
        $bodyMassIndex = $this->bodyMassIndexRepository->find($id);

        if (empty($bodyMassIndex)) {
            return $this->sendError('Body Mass Index not found');
        }

        return $this->sendResponse($bodyMassIndex->toArray(), 'Body Mass Index retrieved successfully');
    }

    /**
     * Update the specified BodyMassIndex in storage.
     * PUT/PATCH /bodyMassIndices/{id}
     *
     * @param int $id
     * @param UpdateBodyMassIndexAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBodyMassIndexAPIRequest $request)
    {
        $input = $request->all();

        /** @var BodyMassIndex $bodyMassIndex */
        $bodyMassIndex = $this->bodyMassIndexRepository->find($id);

        if (empty($bodyMassIndex)) {
            return $this->sendError('Body Mass Index not found');
        }

        $bodyMassIndex = $this->bodyMassIndexRepository->update($input, $id);

        return $this->sendResponse($bodyMassIndex->toArray(), 'BodyMassIndex updated successfully');
    }

    /**
     * Remove the specified BodyMassIndex from storage.
     * DELETE /bodyMassIndices/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BodyMassIndex $bodyMassIndex */
        $bodyMassIndex = $this->bodyMassIndexRepository->find($id);

        if (empty($bodyMassIndex)) {
            return $this->sendError('Body Mass Index not found');
        }

        $bodyMassIndex->delete();

        return $this->sendSuccess('Body Mass Index deleted successfully');
    }
}
