<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBookingCoachAPIRequest;
use App\Http\Requests\API\UpdateBookingCoachAPIRequest;
use App\Models\BookingCoach;
use App\Models\Customer;
use App\Models\Coach;
use App\Models\Course;
use App\Models\Pack;
use App\Models\Order;
use App\OrderHistory;
use App\Models\OrderItem;
use App\Models\Invoice;
use App\Models\InvoicePayment;
use App\Repositories\BookingCoachRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Mail\BookingPayNotifyMail;
use Illuminate\Support\Facades\Mail;
use App\User;
use Response;

use App\Models\CustomerNotifications;
use App\Utils\CustomerNotificationUtils;
use Symfony\Component\HttpFoundation\ParameterBag;
use Illuminate\Support\Collection;
use App\CouponOrder;


/**
 * Class BookingCoachController
 * @package App\Http\Controllers\API
 */

class BookingCoachAPIController extends AppBaseController
{
    /** @var  BookingCoachRepository */
    private $bookingCoachRepository;

    public function __construct(BookingCoachRepository $bookingCoachRepo)
    {
        $this->bookingCoachRepository = $bookingCoachRepo;
    }

    /**
     * Display a listing of the BookingCoach.
     * GET|HEAD /bookingCoaches
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $bookingCoaches = $this->bookingCoachRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($bookingCoaches->toArray(), 'Booking Coaches retrieved successfully');
    }

    /**
     * Store a newly created BookingCoach in storage.
     * POST /bookingCoaches
     *
     * @param CreateBookingCoachAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBookingCoachAPIRequest $request)
    {
        if(empty($request->course_id)){

            return $this->sendError('Cours introuvable', 400);

        }
        if(empty($request->customer_id)){

            return $this->sendError('Compte utilisateur introuvable', 400);

        }

        if(empty($request->pack_id)){

            return $this->sendError('Choisir un Pack', 400);

        }

        $paymentMethod  = $request->get("payment_method_slug", "online");

        $course = Course::where('id', $request->course_id)->first();
        $customer = Customer::where('id', $request->customer_id)->first();
        $pack = Pack::where('id', $request->pack_id)->first();

        if(empty($course)){

            return $this->sendError('Cours introuvable', 400);

        }

        if(empty($customer)){

            return $this->sendError('Compte utilisateur introuvable', 400);

        }


        /*if(empty($request->course_lieu_id)){

            return $this->sendError('Le lieu ?', 400);

        }*/

        //$input = $request->all();

        $customer_id = $request->customer_id;
        $amount = (int)$course->price * (int)$pack->nb_lesson;
        $discount = ($amount * (int)$pack->percentage_discount )/100;

        $discount_coupons = (int) $request->json()->get('discount');

        if($discount_coupons != 0){
            $discount += $discount_coupons;
        }

        $amount = $amount - $discount;
        $currency_code = $course->currency_code;
        $currency_name = $course->currency_name;



        $bookingCoach = $this->bookingCoachRepository->create([
            'course_id' => $course->id,
            'customer_id' => $customer->id,
            'pack_id' => $pack->id,
            'nb_personne' => 1,
            'amount' => $amount,
            //'booking_date' => $request->booking_date,
            //'booking_time' => $request->booking_time,
            'course_lieu_id' => $request->get('course_lieu_id', null),
            'course_lieu_note' => $request->get('course_lieu_note', null),
            'address_name' => '',
            'address_lat' => '',
            'address_lon' => '',
            'address_cpl' => '',
            'quantity' => (int)$pack->nb_lesson,
            'coach_confirm' => false,
            'status' => 1,
            'status_name' => 'created'
        ]);

        $last_booking = \App\Models\BookingCoach::where('id', $bookingCoach->id)->first();
        $sysOrdNo = 1000;
        $last_booking->booking_ref = "ORD-".($sysOrdNo + $last_booking->id);
        $last_booking->save();
        $bookingCoach = $last_booking;




        $order = Order::create([
            'customer_id' => $customer_id,
            'amount' => $amount,
            'currency_code' => $currency_code,
            'currency_name' => $currency_name,
            'payment_method_slug' => $paymentMethod,
            'delivery_fees'=> "0",
            'is_waiting' => true,
            'is_delivery' => false,
            'service_slug' => 'coaching',
            'status' => 'waiting'
        ]);

        $coupons =  (array) $request->json()->get('coupons');

        if(!empty($coupons)){
            $new_price = $order->amount;
            foreach ($coupons as $item){
                $item = new ParameterBag(Collection::make($item)->toArray());
                $id = (int) $item->get('id');
                $pourcentage = (int)$item->get('pourcentage');
                $amount_saved = ($pourcentage/100) * $new_price;
                $new_price = (1 - ($pourcentage/100)) * $new_price;

                $using_coupon = CouponOrder::create([
                    'order_id' => $order->id,
                    'coupon_id' => $id,
                    'amount_saved' => $amount_saved,
                ]);
            }
        }

         // Creating an entry in OrderHistories
         $orderHistory = new OrderHistory();
         $orderHistory->order_id = $order->id;
         $orderHistory->status = Order::WAITING;
         $orderHistory->order_id = $order->id;
         $orderHistory->creator = 'customers';
         $orderHistory->creator_id = $customer->id;
         $orderHistory->creator_name = $customer->name;
         $orderHistory->save();

        $orderItem = OrderItem::create([
            'order_id' => $order->id,
            'service_slug' => 'coaching',
            'meta_data_id' =>  $bookingCoach->id,
            'meta_data' => json_encode($bookingCoach->toArray()),
            'quantity' => 1,
            'unit_price' => $amount,
            'total_amount' => $amount,
            'currency_code' => $currency_code,
            'currency_name' => $currency_name
        ]);

        $orderItems = OrderItem::where('order_id', $order->id)->get();

        $subtotal = 0;
        foreach($orderItems as $orderItem){
            $subtotal = $subtotal + (int)$orderItem->total_amount;
        }

        $tax = 0;
        $fees_delivery = 0;
        $total = $subtotal + $tax;

        $invoice = Invoice::create([
            'order_id' => $order->id,
            'service' => 'coaching',
            'customer_id' => $customer_id,
            'reference' => 'C'.$order->id.'-'.$customer_id.'-'.time(),
            'link' => '',
            'subtotal' => $subtotal,
            'tax' => $tax,
            'fees_delivery' => $fees_delivery,
            'total' => $total,
            'status' => 'unpaid',
            'is_paid' => false,
            'is_paid_by_customer' => false,
            'currency_code' => $currency_code,
            'currency_name' => $currency_name,
            'discount'  =>  $discount
        ]);

        $transactionId = self::generateTransId($customer_id, $invoice->id, $order->id);

        $transaction = InvoicePayment::create([
            'invoice_id' =>  $invoice->id,
            'payment_method'=> "online",
            'payment_reference' => $transactionId,
            'amount' => $invoice->total,
            'currency_code' => $invoice->currency_code,
            'currency_name' => $invoice->currency_name,
            'creator_id' => $customer_id,
            'creator' => 'customers',
            'creator_name' => $customer->name,
            'status' => 'WAITING',
            'is_waiting' => true,
            'is_completed'=> false
        ]);


        return $this->sendResponse([
            'booking_coach' => $bookingCoach,
            'transaction' => [
                'payment_method'=> $transaction->payment_method,
                'transaction_id' => $transactionId,
                'transaction_designation' => 'Payer Coaching',
                'amount' => $transaction->amount,
                'currency_code' => $transaction->currency_code,
                'currency_name' => $transaction->currency_name,
                'status' => $transaction->status
            ]
        ], 'Booking saved successfully');


    }


            /**
     * generate transId
     * @return int
     */
    public static function generateTransId($customerId, $invoiceId, $orderId)
    {
        $timestamp = time();
        $parts = explode(' ', microtime());
        $id = ($timestamp + $parts[0] - strtotime('today 00:00')) * 10;
        $id = sprintf('%06d', $id) . mt_rand(100, 9999);
        $transId = $id."-".$customerId."-".$invoiceId."-".$orderId;

        $transactions = InvoicePayment::where(['payment_reference' => $transId])->get();
        if( count($transactions) == 0){

            return  $transId;
        }else{
            $autoIncrement = count($transactions) + 1;
            $transId = $transId.'-'.$autoIncrement;
            return  $transId;
        }


    }


    /**
     * Display the specified BookingCoach.
     * GET|HEAD /bookingCoaches/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BookingCoach $bookingCoach */
        $bookingCoach = $this->bookingCoachRepository->find($id);
        if (empty($bookingCoach)) {
            return $this->sendError('Booking Coach not found');
        }

        return $this->sendResponse($bookingCoach->toArray(), 'Booking Coach retrieved successfully');
    }

    /**
     * Update the specified BookingCoach in storage.
     * PUT/PATCH /bookingCoaches/{id}
     *
     * @param int $id
     * @param UpdateBookingCoachAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBookingCoachAPIRequest $request)
    {
        $input = $request->all();

        /** @var BookingCoach $bookingCoach */
        $bookingCoach = $this->bookingCoachRepository->find($id);

        if (empty($bookingCoach)) {
            return $this->sendError('Booking Coach not found');
        }

        $bookingCoach = $this->bookingCoachRepository->update($input, $id);

        // notifying admins
        $admin_email_data= [
            'subject'=>"Notification d'annulation",
            'title' =>'Annulation de réservation',
            'subtitle' =>'',
            'body' => "Le coach <b>".$bookingCoach->course->coach->customer->name.
                        "</b> a annulé la réservation du ".$bookingCoach->start_date.
                        " avec le client <b>".$bookingCoach->customer->name.".</b><br/>"
        ];

        foreach (User::all() as $user) {
            Mail::to($user->email)->send(new BookingPayNotifyMail($admin_email_data));
        }

        // Notification Customer
        $client_email_data= [
            'subject'=>"Notification d'annulation",
            'title' =>'Annulation de réservation',
            'subtitle' =>'',
            'body' => "Pour raison de disponibilité, le coach <b>".$bookingCoach->course->coach->customer->name."</b> a annulé votre réservation de cours de ".$bookingCoach->course->discipline->name." du ".$bookingCoach->start_date.".<br/><br/><br/>"

        ];
        Mail::to($bookingCoach->customer->email)->send(new BookingPayNotifyMail($client_email_data));


        return $this->sendResponse($bookingCoach->toArray(), 'BookingCoach updated successfully');
    }

    /**
     * Remove the specified BookingCoach from storage.
     * DELETE /bookingCoaches/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BookingCoach $bookingCoach */
        $bookingCoach = $this->bookingCoachRepository->find($id);

        if (empty($bookingCoach)) {
            return $this->sendError('Booking Coach not found');
        }

        $bookingCoach->delete();

        return $this->sendSuccess('Booking Coach deleted successfully');
    }


        /**
     * Display the specified BookingCoach.
     * GET|HEAD /bookingCoaches/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function findByCoach($coachId)
    {

        $coach = Coach::where('id', $coachId)->first();

        if (empty($coach)) {
            return $this->sendError('Coach not found');
        }

        $courses = \App\Models\Course::where('coach_id', $coachId)->get();

        $ids_courses = collect($courses)->map(function($value){
            return $value->id;
        });

        $bookings =  \App\Models\BookingCoach::whereIn('course_id', $ids_courses)->orderBy('created_at', 'desc')->get();

        return $this->sendResponse( $bookings, 'Coachings');
    }

    public function findByCustomer($customerId)
    {

        $customer = \App\Models\Customer::where('id', $customerId)->first();

        if (empty($customer)) {
            return $this->sendError('Customer not found');
        }


        $bookings =  \App\Models\BookingCoach::where('customer_id', $customer->id)->orderBy('created_at', 'desc')->get();

        return $this->sendResponse($bookings, 'Coachings');
    }

    public function changeStatus(Request $request){

         /** @var BookingCoach $bookingCoach */
         $bookingCoach = $this->bookingCoachRepository->find($request->booking_coach_id);

         if (empty($bookingCoach)) {
             return $this->sendError('Booking Coach not found');
         }

         $status = $request->status;
         $customerNotification = new CustomerNotifications();

        $customerNotification->action  = "";
        $customerNotification->action_by  = "";
        $customerNotification->is_read  = false;
        $customerNotification->is_received  = false;
        $customerNotification->user_id  = $bookingCoach->customer_id;

         if($status == BookingCoach::CONFIRMED_STATUS_ID){
            $customerNotification->title  = "Confirmation de réservation";
            $customerNotification->title_en  = "Booking Confirmation";
            $customerNotification->subtitle  = "Votre reservation a été confirmée";
            $customerNotification->subtitle_en  = "Booking confirmed";
            $customerNotification->type_notification  = BookingCoach::CONFIRMED;
            $bookingCoach->coach_confirm = true;
            $bookingCoach->status = BookingCoach::CONFIRMED_STATUS_ID;
            $bookingCoach->status_name = BookingCoach::CONFIRMED;
            $bookingCoach->save();
         }

         if($status == BookingCoach::REJECTED_STATUS_ID){
            $customerNotification->title  = "Confirmation réjecté";
            $customerNotification->title_en  = "Booking rejected";
            $customerNotification->subtitle  = "Reservation réjeté par le coach";
            $customerNotification->subtitle_en  = "Booking rejected by coach";
            $customerNotification->type_notification  = BookingCoach::REJECTED;

            $bookingCoach->coach_confirm = 2;
            $bookingCoach->status = BookingCoach::REJECTED_STATUS_ID;
            $bookingCoach->status_name = BookingCoach::REJECTED;
            $bookingCoach->save();
         }

         if($status == BookingCoach::CANCELED_STATUS_ID){
            $customerNotification->title  = "Confirmation annulée";
            $customerNotification->title_en  = "Booking canceled";
            $customerNotification->subtitle  = "Reservation annulée par le coach";
            $customerNotification->subtitle_en  = "Booking canceled by coach";
            $customerNotification->type_notification  = BookingCoach::CANCELED;

            $bookingCoach->status = BookingCoach::CANCELED_STATUS_ID;
            $bookingCoach->status_name = BookingCoach::CANCELED;
            $bookingCoach->save();
         }

            /** @var BookingCoach $bookingCoach */
            $bookingCoach = BookingCoach::where('id',$request->booking_coach_id)->first();

            $customerNotification->save();

            CustomerNotificationUtils::notify($customerNotification);

         return $this->sendResponse($bookingCoach->toArray(), 'Booking Coach retrieved successfully');

    }
}
