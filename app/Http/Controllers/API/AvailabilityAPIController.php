<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAvailabilityAPIRequest;
use App\Http\Requests\API\UpdateAvailabilityAPIRequest;
use App\Models\Availability;
use App\Models\Coach;
use App\Repositories\AvailabilityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Carbon\Carbon;

/**
 * Class AvailabilityController
 * @package App\Http\Controllers\API
 */

class AvailabilityAPIController extends AppBaseController
{
    /** @var  AvailabilityRepository */
    private $availabilityRepository;

    public function __construct(AvailabilityRepository $availabilityRepo)
    {
        $this->availabilityRepository = $availabilityRepo;
    }

    /**
     * Display a listing of the Availability.
     * GET|HEAD /availabilities
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $availabilities = $this->availabilityRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($availabilities->toArray(), 'Availabilities retrieved successfully');
    }

    /**
     * Store a newly created Availability in storage.
     * POST /availabilities
     *
     * @param CreateAvailabilityAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAvailabilityAPIRequest $request)
    {
        $input = $request->all();

        $availability = $this->availabilityRepository->create($input);

        return $this->sendResponse($availability->toArray(), 'Availability saved successfully');
    }

    /**
     * Display the specified Availability.
     * GET|HEAD /availabilities/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Availability $availability */
        $availability = $this->availabilityRepository->find($id);

        if (empty($availability)) {
            return $this->sendError('Availability not found');
        }

        return $this->sendResponse($availability->toArray(), 'Availability retrieved successfully');
    }

    /**
     * Update the specified Availability in storage.
     * PUT/PATCH /availabilities/{id}
     *
     * @param int $id
     * @param UpdateAvailabilityAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAvailabilityAPIRequest $request)
    {
        $input = $request->all();

        /** @var Availability $availability */
        $availability = $this->availabilityRepository->find($id);

        if (empty($availability)) {
            return $this->sendError('Availability not found');
        }

        $availability = $this->availabilityRepository->update($input, $id);

        return $this->sendResponse($availability->toArray(), 'Availability updated successfully');
    }

    /**
     * Remove the specified Availability from storage.
     * DELETE /availabilities/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Availability $availability */
        $availability = $this->availabilityRepository->find($id);

        if (empty($availability)) {
            return $this->sendError('Availability not found');
        }

        $availability->delete();

        return $this->sendSuccess('Availability deleted successfully');
    }

    public function praticTime($coachId){
        $coach = Coach::find($coachId);
        if(is_null($coach) || empty($coach)){
            return $this->sendError("Coach n'existe pas");
        }

        $availabilities = Availability::where('coach_id', $coachId)->where('date_fin', '>',Carbon::now()->format('Y-m-d'))->get();

        return $this->sendResponse($availabilities->toArray(),"pratice time retrieved successfully");
    }
}
