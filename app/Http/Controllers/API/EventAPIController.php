<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEventAPIRequest;
use App\Http\Requests\API\UpdateEventAPIRequest;
use App\Models\Event;
use App\EventTicket;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Invoice;
use App\OrderHistory;
use App\Models\InvoicePayment;
use App\Repositories\EventRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Models\CustomerNotifications;
use App\Utils\CustomerNotificationUtils;
use Symfony\Component\HttpFoundation\ParameterBag;
use Illuminate\Support\Collection;
use App\CouponOrder;
/**
 * Class EventController
 * @package App\Http\Controllers\API
 */

class EventAPIController extends AppBaseController
{
    /** @var  EventRepository */
    private $eventRepository;

    public function __construct(EventRepository $eventRepo)
    {
        $this->eventRepository = $eventRepo;
    }

    /**
     * Display a listing of the Event.
     * GET|HEAD /events
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $events = $this->eventRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($events->toArray(), 'Events retrieved successfully');
    }



    /**
     * Display a listing of the Event.
     * GET|HEAD /find_events
     *
     * @param Request $request
     * @return Response
     */
    public function findEvents(Request $request)
    {

        $next_events = Event::isUpcomingEvent()->orderBy('created_at', 'desc')->get();
        $past_events = Event::isPastEvent()->orderBy('created_at', 'desc')->get();

        $result = [
            'next_events' => $next_events,
            'past_events' => $past_events
        ];

        return $this->sendResponse($result, 'Events retrieved successfully');
    }


        /**
     * Store a newly created Event in storage.
     * POST /participate_events
     *
     * @param Request $request
     *
     * @return Response
     */
    public function participateEvent(Request $request)
    {

        $customer = Customer::where('id', $request->customer_id)->first();

        if(empty($customer)){

            return $this->sendError('Compte utilisateur introuvable', 400);

        }

        if(empty($request->event_id)){

            return $this->sendError('Evènement introuvable', 400);

        }

        if(empty($request->customer_id)){

            return $this->sendError('Compte utilisateur introuvable', 400);

        }

        if(empty($request->seats_count)){

            return $this->sendError('Définir le nombre de place', 400);

        }

        /** @var Event $event */
        $event = $this->eventRepository->find($request->event_id);

        if (empty($event)) {
            return $this->sendError('Evènement introuvable');
        }

        $seats_count = $request->seats_count;

        $tickets = [];

        $ticketAmount  = intval($event->entry_fee);

        $isFree = $ticketAmount == 0;


        $discount_coupons = (int) $request->json()->get('discount') ?? 0;

        $amount = ($ticketAmount * $seats_count) - $discount_coupons;

        $customer_id = $request->customer_id;
        $currency_code = "XOF";
        $currency_name = "CFA";
        $payment_method_slug = "online";



        $order = Order::create([
            'customer_id' => $customer_id,
            'amount' => $amount,
            'currency_code' => $currency_code,
            'currency_name' => $currency_name,
            'payment_method_slug' => $payment_method_slug,
            'delivery_fees'=> "0",
            'is_waiting' => true,
            'is_delivery' => false,
            'service_slug' => 'event',
            'status' => 'waiting'
        ]);

        $coupons =  (array) $request->json()->get('coupons');
        
        if(!empty($coupons)){
            $new_price = $order->amount;
            foreach ($coupons as $item){
                $item = new ParameterBag(Collection::make($item)->toArray());
                $id = (int) $item->get('id');
                $pourcentage = (int)$item->get('pourcentage');
                $amount_saved = ($pourcentage/100) * $new_price;
                $new_price = (1 - ($pourcentage/100)) * $new_price;
                
                $using_coupon = CouponOrder::create([
                    'order_id' => $order->id,
                    'coupon_id' => $id,
                    'amount_saved' => $amount_saved,
                ]);
            }
        }

        // Creating an entry in OrderHistories
        $orderHistory = new OrderHistory();
        $orderHistory->order_id = $order->id;
        $orderHistory->status = Order::WAITING;
        $orderHistory->order_id = $order->id;
        $orderHistory->creator = 'customers';
        $orderHistory->creator_id = $customer->id;
        $orderHistory->creator_name = $customer->name;
        $orderHistory->save();

        for($i=0; $i <$seats_count; $i++){

            $ticket = new EventTicket();

            $code = $customer->id."-".Str::random(10)."-".time();
            $code_exists = EventTicket::where('code', $code)->first();
            if($code_exists){
                $code = $customer->id."-".Str::random(10)."-".time()."-".Str::random(10);
            }
            $ticket->code = $code;
            $ticket->seats_count = 1;
            $ticket->amount= $event->entry_fee;

            $ticket->event_id= $event->id;
            $ticket->customer_id= $customer->id;
            $ticket->save();

            $orderItem = OrderItem::create([
                'order_id' => $order->id,
                'service_slug' => 'event',
                'meta_data_id' =>  $ticket->id,
                'meta_data' => json_encode($ticket->toArray()),
                'quantity' => $seats_count,
                'unit_price' => $amount,
                'total_amount' => $amount,
                'currency_code' => $currency_code,
                'currency_name' => $currency_name
            ]);


            array_push($tickets,$ticket);

        }



        $isOk = DB::table('participates_in_event')->insert([
            'event_id' => $event->id,
           // 'ticket_id' => $ticket->id,
            'customer_id' => $customer->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);



        $orderItems = OrderItem::where('order_id', $order->id)->get();

        $subtotal = $amount;

        $tax = 0;
        $fees_delivery = 0;
        $total = $subtotal + $tax;

        $invoice = Invoice::create([
            'order_id' => $order->id,
            'service' => 'event',
            'customer_id' => $customer_id,
            'reference' => Invoice::generateID('EVENT', $order->id, $customer->id),
            'link' => '#',
            'subtotal' => $subtotal,
            'tax' => $tax,
            'fees_delivery' => $fees_delivery,
            'total' => $total,
            'status' => 'unpaid',
            'is_paid' => false,
            'is_paid_by_customer' => false,
            'is_paid_by_delivery_service' => false,
            'currency_code' => $currency_code,
            'currency_name' => $currency_name,
            'discount'  =>  $discount_coupons
        ]);

        $transactionId = self::generateTransId($customer_id, $invoice->id, $order->id);

        $transaction = InvoicePayment::create([
            'invoice_id' =>  $invoice->id,
            'payment_method'=> "online",
            'payment_reference' => $transactionId,
            'amount' => $invoice->total,
            'currency_code' => $invoice->currency_code,
            'currency_name' => $invoice->currency_name,
            'creator_id' => $customer_id,
            'creator' => 'customers',
            'creator_name' => $customer->name,
            'status' => 'WAITING',
            'is_waiting' => true,
            'is_completed'=> false
        ]);

        if($isFree){
            $transaction->status = 'SUCCESSFUL';
            $transaction->is_waiting = false;
            $transaction->is_completed = true;
            $transaction->save();
            $this->validateTransaction($transactionId);
        }


        $result = [
            "tickets" => $tickets,
            "order" => $order,
            "is_free" => $isFree,
            'transaction' => [
                'payment_method'=> $transaction->payment_method,
                'transaction_id' => $transactionId,
                'transaction_designation' => 'Payer un Ticket',
                'amount' => $transaction->amount,
                'currency_code' => $transaction->currency_code,
                'currency_name' => $transaction->currency_name,
                'status' => $transaction->status
            ]
        ];

        return $this->sendResponse($result, 'Participation saved successfully');
    }

                /**
     * generate transId
     * @return int
     */
    public static function generateTransId($customerId, $invoiceId, $orderId)
    {
        $timestamp = time();
        $parts = explode(' ', microtime());
        $id = ($timestamp + $parts[0] - strtotime('today 00:00')) * 10;
        $id = sprintf('%06d', $id) . mt_rand(100, 9999);
        $transId = $id."-".$customerId."-".$invoiceId."-".$orderId;

        $transactions = InvoicePayment::where(['payment_reference' => $transId])->get();
        if( count($transactions) == 0){

            return  $transId;
        }else{
            $autoIncrement = count($transactions) + 1;
            $transId = $transId.'-'.$autoIncrement;
            return  $transId;
        }


    }

    public function findTicketsByCustomer($id){

        $customer = Customer::where('id', $id)->first();

        if(empty($customer)){

            return $this->sendError('Compte utilisateur introuvable', 400);

        }

        $result = EventTicket::where([
            'customer_id' => $id,
            'status' => 'valid'
        ])->get();

        return $this->sendResponse($result, 'Tickets');

    }


    /**
     * Store a newly created Event in storage.
     * POST /events
     *
     * @param CreateEventAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEventAPIRequest $request)
    {
        $input = $request->all();

        $event = $this->eventRepository->create($input);

        return $this->sendResponse($event->toArray(), 'Event saved successfully');
    }

    /**
     * Display the specified Event.
     * GET|HEAD /events/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Event $event */
        $event = $this->eventRepository->find($id);

        if (empty($event)) {
            return $this->sendError('Event not found');
        }

        return $this->sendResponse($event->toArray(), 'Event retrieved successfully');
    }

    /**
     * Update the specified Event in storage.
     * PUT/PATCH /events/{id}
     *
     * @param int $id
     * @param UpdateEventAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEventAPIRequest $request)
    {
        $input = $request->all();

        /** @var Event $event */
        $event = $this->eventRepository->find($id);

        if (empty($event)) {
            return $this->sendError('Event not found');
        }

        $event = $this->eventRepository->update($input, $id);

        return $this->sendResponse($event->toArray(), 'Event updated successfully');
    }

    /**
     * Remove the specified Event from storage.
     * DELETE /events/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Event $event */
        $event = $this->eventRepository->find($id);

        if (empty($event)) {
            return $this->sendError('Event not found');
        }

        $event->delete();

        return $this->sendSuccess('Event deleted successfully');
    }

    public function validateTransaction($transactionId){
        $transaction = InvoicePayment::where(['payment_reference' => $transactionId])->first();

        if($transaction->status == 'SUCCESSFUL'){
            $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();
            $invoice->status = 'paid';
            $invoice->is_paid = true;
            $invoice->is_paid_by_customer = true;
            $invoice->save();

            $order = Order::where(['id' => $invoice->order_id])->first();

            $customer = Customer::where('id' ,  $order->customer_id)->first();

            $orderHistory = new OrderHistory();
            $orderHistory->order_id = $order->id;
            $orderHistory->status = Order::CUSTOMER_PAID;
            $orderHistory->order_id = $order->id;
            $orderHistory->creator = 'customers';
            $orderHistory->creator_id = $customer->id;
            $orderHistory->creator_name = $customer->name;
            $orderHistory->save();

            // Managing services
            $transaction = InvoicePayment::where(['payment_reference' => $transactionId])->first();

            // *** MANAGING SERVICES CONTENT STATES
            // ! service event
            if($invoice->service == 'event'){
                $order = Order::where(['id' => $invoice->order_id])->first();
                foreach($order->orderItems as $orderItem){
                    $ticket = EventTicket::find($orderItem->meta_data_id);
                    if(!$ticket){
                        break;
                    }
                    $ticket->status = 'valid';
                    $ticket->is_active = true;
                    $ticket->is_paid = true;
                    $ticket->save();
                }

               
            }
            // ! service shop
            if($invoice->service == 'shop'){
                $orderItem = OrderItem::where(["order_id" => $order->id])->whereNotNull("meta_data_id")->first();
   

                $orderHistory = new OrderHistory();
                $orderHistory->order_id = $order->id;
                $orderHistory->status = Order::WAITING_CUSTOMER_DELIVERY;
                $orderHistory->order_id = $order->id;
                $orderHistory->creator = 'customers';
                $orderHistory->creator_id = $customer->id;
                $orderHistory->creator_name = $customer->name;
                $orderHistory->save();


                

            }

            if($invoice->service == 'coaching'){

              

            }

    
        }
    }

}
