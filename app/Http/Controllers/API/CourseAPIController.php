<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCourseAPIRequest;
use App\Http\Requests\API\UpdateCourseAPIRequest;
use App\Models\Course;
use App\Repositories\CourseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Coach;
use App\Models\CoachDiscipline;
use Symfony\Component\HttpFoundation\ParameterBag;
use Carbon\Carbon;


use App\Models\CustomerNotifications;
use App\Utils\CustomerNotificationUtils;
/**
 * Class CourseController
 * @package App\Http\Controllers\API
 */

class CourseAPIController extends AppBaseController
{
    /** @var  CourseRepository */
    private $courseRepository;

    public function __construct(CourseRepository $courseRepo)
    {
        $this->courseRepository = $courseRepo;
    }

    /**
     * Display a listing of the Course.
     * GET|HEAD /courses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $elements = \App\Models\Availability::where([
            'is_active' => 1
        ])->orderBy('start_time', 'asc')->get();
  
  
        foreach ($elements as $item) {
            if(\Carbon\Carbon::today()->greaterThan(\Carbon\Carbon::parse($item->date_fin))){
                $item->is_active = 0;
                $item->save();
            }
        }
        
        $courses = $this->courseRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($courses->toArray(), 'Courses retrieved successfully');
    }

    /**
     * Store a newly created Course in storage.
     * POST /courses
     *
     * @param CreateCourseAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCourseAPIRequest $request)
    {
        $input = $request->all();

        if($request->isJson()){

            /** @var ParameterBag */
            $bodyJson = $request->json();

            /** @var array */
            if(is_array($bodyJson->get('calendars'))){
                $calendars = $bodyJson->get('calendars');
            }else{
                $calendars = (array)json_decode($bodyJson->get('calendars'));
            }

            /** @var array */
            if(is_array($bodyJson->get('course_lieus'))){
                $course_lieus = $bodyJson->get('course_lieus');
            }else{
                $course_lieus = (array)json_decode($bodyJson->get('course_lieus'));
            }

            
            /** @var array */
            if(is_array($bodyJson->get('objectives'))){
                $objectives = $bodyJson->get('objectives');
            }else{
                $objectives = (array)json_decode($bodyJson->get('objectives'));
            }

            /** @var ParameterBag */
            $details = new ParameterBag($bodyJson->get('details'));

            $coachDiscipline = \App\Models\CoachDiscipline::where([  "coach_id" => $bodyJson->get('coach_id'),
            "discipline_id" => $bodyJson->get('discipline_id')])->first();

            if(empty($coachDiscipline)){
                $coachDiscipline = \App\Models\CoachDiscipline::create(
                    [ 
                         "coach_id" => $bodyJson->get('coach_id'),
                         "discipline_id" => $bodyJson->get('discipline_id')
                    ]

                );
            }


            $course = $this->courseRepository->create([
                "coach_id" => $bodyJson->get('coach_id'),
                "discipline_id" => $bodyJson->get('discipline_id'),
                "price" => $bodyJson->get("price", '100'),
                "currency_code" => $bodyJson->get('currency_code', 'XOF'),
                "currency_name" => $bodyJson->get('currency_name', 'CFA'),
                "active" => false,
                "is_active" => false,
                "course_type" => $bodyJson->get('course_type', 'cour-particulier')
            ]);

            $bodyJson->remove('details');
            $bodyJson->remove('course_lieus');
            $bodyJson->remove('calendars');
            $bodyJson->remove('objectives');


            if(!empty($course_lieus)){

                foreach($course_lieus as $course_lieu_id){
                    $LieuCourse = \App\Models\LieuCourse::where('id', $course_lieu_id)->first();
                    if(!empty($LieuCourse)){
                        $courseLieu = \App\Models\CourseLieu::create([
                            "course_id" => $course->id,
                            'lieu_course_id' => $course_lieu_id
                        ]);
                    }
                }
                
            }



            if(!empty($calendars)){

                foreach($calendars as $calendar){
                    $calendarJson = new ParameterBag($calendar);

                    if(!empty($calendarJson)){
                        $availability = \App\Models\Availability::create([
                            'coach_id' => $course->coach_id,
                            "course_id" => $course->id,
                            'date_debut' => Carbon::parse($calendarJson->get('date_debut'))->format('Y-m-d'),
                            'date_fin' => Carbon::parse($calendarJson->get('date_fin'))->format('Y-m-d'),
                            'start_time' => $calendarJson->get('start_time'),
                            'end_time' => $calendarJson->get('end_time'),
                        ]);
                    }
                }
            }

            if(!empty($objectives)){

                foreach($objectives as $objectiveId){
                    $objective = \App\Models\Objective::where('id', $objectiveId)->first();
                    if(!empty($objective)){
                        $courseObjective = \App\Models\CourseObjective::create([
                            "course_id" => $course->id,
                            'objective_id' => $objectiveId
                        ]);
                    }
                }
                
            }

            if(!empty($details)){
                $courseDetail = \App\Models\CourseDetail::create([
                    'course_id'=> $course->id,
                    "experience" => $details->get('experience', ''),
                    "seance_type" => $details->get('seance_type', ''),
                    "is_diplome" => $details->getBoolean('is_diplome', false),
                    "diplome_name" => $details->get('diplome_name', null),
                    "diplome_url" => $details->get('diplome_url', null),
                    "nb_person" => $details->getInt('nb_person', 1),
                    "is_formation" => false,
                    "has_particulier" => $bodyJson->get('course_type') == "cour-particulier",
                    "has_collectif" => $bodyJson->get('course_type') != "cour-particulier"
                ]);

            }

            
            // $customerNotification = new CustomerNotifications();
                
            // $customerNotification->action  = "";
            // $customerNotification->action_by  = "";
            // $customerNotification->is_read  = false;
            // $customerNotification->is_received  = false;
            // $customerNotification->user_id  = $course->coach->_id;





            return $this->sendResponse($course->toArray(), 'Course saved successfully');

        }else{
            return $this->sendError('JSON not found'); 
            
        }

 
    }

    /**
     * Display the specified Course.
     * GET|HEAD /courses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Course $course */
        $course = $this->courseRepository->find($id);

        if (empty($course)) {
            return $this->sendError('Course not found');
        }

        return $this->sendResponse($course->toArray(), 'Course retrieved successfully');
    }

    /**
     * Update the specified Course in storage.
     * PUT/PATCH /courses/{id}
     *
     * @param int $id
     * @param UpdateCourseAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCourseAPIRequest $request)
    {
        $input = $request->all();

        /** @var Course $course */
        $course = $this->courseRepository->find($id);

        if (empty($course)) {
            return $this->sendError('Course not found');
        }

        if($request->isJson()){

            /** @var ParameterBag */
            $bodyJson = $request->json();

            /** @var array */
            if(is_array($bodyJson->get('calendars'))){
                $calendars = $bodyJson->get('calendars');
            }else{
                $calendars = (array)json_decode($bodyJson->get('calendars'));
            }

            /** @var array */
            if(is_array($bodyJson->get('objectives'))){
                $objectives = $bodyJson->get('objectives');
            }else{
                $objectives = (array)json_decode($bodyJson->get('objectives'));
            }

            /** @var array */
            if(is_array($bodyJson->get('course_lieus'))){
                $course_lieus = $bodyJson->get('course_lieus');
            }else{
                $course_lieus = (array)json_decode($bodyJson->get('course_lieus'));
            }

            /** @var ParameterBag */
            $details = new ParameterBag($bodyJson->get('details'));

            $coachDiscipline = \App\Models\CoachDiscipline::where([  "coach_id" => $bodyJson->get('coach_id'),
            "discipline_id" => $bodyJson->get('discipline_id')])->first();

            if(empty($coachDiscipline)){
                $coachDiscipline = \App\Models\CoachDiscipline::create(
                    [ 
                         "coach_id" => $bodyJson->get('coach_id'),
                         "discipline_id" => $bodyJson->get('discipline_id')
                    ]

                );
            }

            $course = $this->courseRepository->update([
                "coach_id" => $bodyJson->get('coach_id'),
                "discipline_id" => $bodyJson->get('discipline_id'),
                "price" => $bodyJson->get("price", '100'),
                "currency_code" => $bodyJson->get('currency_code', 'XOF'),
                "currency_name" => $bodyJson->get('currency_name', 'CFA'),
                "course_type" => $bodyJson->get('course_type', 'cour-particulier')
            ], $id);

            $bodyJson->remove('details');
            $bodyJson->remove('course_lieus');
            $bodyJson->remove('calendars');
            $bodyJson->remove('objectives');




            if(!empty($course_lieus)){

                $courseLieuList = \App\Models\CourseLieu::create([
                    "course_id" => $course->id
                ])->get();

                foreach($courseLieuList as $courseLieuElement){
                    $courseLieuElement->delete();
                }


                foreach($course_lieus as $course_lieu_id){
                    $LieuCourse = \App\Models\LieuCourse::where('id', $course_lieu_id)->first();
                    if(!empty($LieuCourse)){
                        $courseLieu = \App\Models\CourseLieu::create([
                            "course_id" => $course->id,
                            'lieu_course_id' => $course_lieu_id
                        ]);
                    }
                }
                
            }

            if(!empty($objectives)){

                $courseObjectiveList = \App\Models\CourseObjective::where([
                    "course_id" => $course->id
                ])->get();

                foreach($courseObjectiveList as $courseObjective){
                    $courseObjective->delete();
                }

                foreach($objectives as $objectiveId){
                    $objective = \App\Models\Objective::where('id', $objectiveId)->first();
                    if(!empty($objective)){
                        $courseObjective = \App\Models\CourseObjective::create([
                            "course_id" => $course->id,
                            'objective_id' => $objectiveId
                        ]);
                    }
                }
                
            }


            if(!empty($calendars)){

                $availabilityList  = \App\Models\Availability::where([
                    "course_id" => $course->id
                ])->get();

                if(!empty($availabilityList)){
                    foreach($availabilityList as $availability){
                        $availability->delete();
                    }
                }


                foreach($calendars as $calendar){
                    $calendarJson = new ParameterBag($calendar);

                    if(!empty($calendarJson)){
                        $availability = \App\Models\Availability::create([
                            'coach_id' => $course->coach_id,
                            "course_id" => $course->id,
                            'date_debut' => Carbon::parse($calendarJson->get('date_debut'))->format('Y-m-d'),
                            'date_fin' => Carbon::parse($calendarJson->get('date_fin'))->format('Y-m-d'),
                            'start_time' => $calendarJson->get('start_time'),
                            'end_time' => $calendarJson->get('end_time'),
                        ]);
                    }
                }
            }

            if(!empty($details)){

                $courseDetailResult = \App\Models\CourseDetail::where([
                    'course_id'=> $course->id
                ])->first();

                if($courseDetailResult != null){
                    $courseDetailResult->delete();
                }

                $courseDetail = \App\Models\CourseDetail::create([
                    'course_id'=> $course->id,
                    "experience" => $details->get('experience', ''),
                    "seance_type" => $details->get('seance_type', ''),
                    "is_diplome" => $details->getBoolean('is_diplome', false),
                    "diplome_url" => $details->get('diplome_url', null),
                    "diplome_name" => $details->get('diplome_name', null),
                    "nb_person" => $details->getInt('nb_person', 1),
                    "is_formation" => false,
                    "has_particulier" => $bodyJson->get('course_type') == "cour-particulier",
                    "has_collectif" => $bodyJson->get('course_type') != "cour-particulier"
                ]);

            }

            






            return $this->sendResponse($course->toArray(), 'Course saved successfully');

        }else{
            return $this->sendError('JSON not found'); 
            
        }
    }

    /**
     * Remove the specified Course from storage.
     * DELETE /courses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Course $course */
        $course = $this->courseRepository->find($id);

        if (empty($course)) {
            return $this->sendError('Course not found');
        }

        $course->delete();

        return $this->sendSuccess('Course deleted successfully');
    }

        /**
     * Display the specified Course.
     * GET|HEAD /courses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function findCoursesByCoach($id)
    {
        
        $coach = Coach::where('id', $id)->first();

        if (empty($coach)) {
            return $this->sendError('Coach not found');
        }

        $disciplines = $coach->getDisciplinesAttribute();

        if(empty($disciplines)){
            return $this->sendError('Discipline not found');
        }
        
        /** @var Course $course */
        $courses = Course::where('coach_id', $id)->get();

      /*  if (empty($courses)) {
            return $this->sendError('Courses not found');
        }*/

        $disciplinesResult = [];

        foreach($disciplines as $discipline){
            $courses_list = Course::where(['coach_id' => $id, 'discipline_id' => $discipline->id])->get();
            array_push($disciplinesResult, [
                "discipline" => $discipline,
                "courses" => $courses_list
            ]);
        }

        $result = [
            "courses" => $courses,
            "disciplines_list" => $disciplinesResult
        ];

        return $this->sendResponse($result, 'Course retrieved successfully');
    }
}
