<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCoachDisciplineAPIRequest;
use App\Http\Requests\API\UpdateCoachDisciplineAPIRequest;
use App\Models\CoachDiscipline;
use App\Repositories\CoachDisciplineRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CoachDisciplineController
 * @package App\Http\Controllers\API
 */

class CoachDisciplineAPIController extends AppBaseController
{
    /** @var  CoachDisciplineRepository */
    private $coachDisciplineRepository;

    public function __construct(CoachDisciplineRepository $coachDisciplineRepo)
    {
        $this->coachDisciplineRepository = $coachDisciplineRepo;
    }

    /**
     * Display a listing of the CoachDiscipline.
     * GET|HEAD /coachDisciplines
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $coachDisciplines = $this->coachDisciplineRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($coachDisciplines->toArray(), 'Coach Disciplines retrieved successfully');
    }

    /**
     * Store a newly created CoachDiscipline in storage.
     * POST /coachDisciplines
     *
     * @param CreateCoachDisciplineAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCoachDisciplineAPIRequest $request)
    {
        $input = $request->all();

        $coachDiscipline = $this->coachDisciplineRepository->create($input);

        return $this->sendResponse($coachDiscipline->toArray(), 'Coach Discipline saved successfully');
    }

    /**
     * Display the specified CoachDiscipline.
     * GET|HEAD /coachDisciplines/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CoachDiscipline $coachDiscipline */
        $coachDiscipline = $this->coachDisciplineRepository->find($id);

        if (empty($coachDiscipline)) {
            return $this->sendError('Coach Discipline not found');
        }

        return $this->sendResponse($coachDiscipline->toArray(), 'Coach Discipline retrieved successfully');
    }

    /**
     * Update the specified CoachDiscipline in storage.
     * PUT/PATCH /coachDisciplines/{id}
     *
     * @param int $id
     * @param UpdateCoachDisciplineAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCoachDisciplineAPIRequest $request)
    {
        $input = $request->all();

        /** @var CoachDiscipline $coachDiscipline */
        $coachDiscipline = $this->coachDisciplineRepository->find($id);

        if (empty($coachDiscipline)) {
            return $this->sendError('Coach Discipline not found');
        }

        $coachDiscipline = $this->coachDisciplineRepository->update($input, $id);

        return $this->sendResponse($coachDiscipline->toArray(), 'CoachDiscipline updated successfully');
    }

    /**
     * Remove the specified CoachDiscipline from storage.
     * DELETE /coachDisciplines/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CoachDiscipline $coachDiscipline */
        $coachDiscipline = $this->coachDisciplineRepository->find($id);

        if (empty($coachDiscipline)) {
            return $this->sendError('Coach Discipline not found');
        }

        $coachDiscipline->delete();

        return $this->sendSuccess('Coach Discipline deleted successfully');
    }
}
