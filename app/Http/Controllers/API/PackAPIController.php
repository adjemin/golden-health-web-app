<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePackAPIRequest;
use App\Http\Requests\API\UpdatePackAPIRequest;
use App\Models\Pack;
use App\Repositories\PackRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PackController
 * @package App\Http\Controllers\API
 */

class PackAPIController extends AppBaseController
{
    /** @var  PackRepository */
    private $packRepository;

    public function __construct(PackRepository $packRepo)
    {
        $this->packRepository = $packRepo;
    }

    /**
     * Display a listing of the Pack.
     * GET|HEAD /packs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $packs = $this->packRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($packs->toArray(), 'Packs retrieved successfully');
    }

    /**
     * Store a newly created Pack in storage.
     * POST /packs
     *
     * @param CreatePackAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePackAPIRequest $request)
    {
        $input = $request->all();

        $pack = $this->packRepository->create($input);

        return $this->sendResponse($pack->toArray(), 'Pack saved successfully');
    }

    /**
     * Display the specified Pack.
     * GET|HEAD /packs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Pack $pack */
        $pack = $this->packRepository->find($id);

        if (empty($pack)) {
            return $this->sendError('Pack not found');
        }

        return $this->sendResponse($pack->toArray(), 'Pack retrieved successfully');
    }

    /**
     * Update the specified Pack in storage.
     * PUT/PATCH /packs/{id}
     *
     * @param int $id
     * @param UpdatePackAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePackAPIRequest $request)
    {
        $input = $request->all();

        /** @var Pack $pack */
        $pack = $this->packRepository->find($id);

        if (empty($pack)) {
            return $this->sendError('Pack not found');
        }

        $pack = $this->packRepository->update($input, $id);

        return $this->sendResponse($pack->toArray(), 'Pack updated successfully');
    }

    /**
     * Remove the specified Pack from storage.
     * DELETE /packs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Pack $pack */
        $pack = $this->packRepository->find($id);

        if (empty($pack)) {
            return $this->sendError('Pack not found');
        }

        $pack->delete();

        return $this->sendSuccess('Pack deleted successfully');
    }
}
