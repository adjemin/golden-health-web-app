<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use Carbon\Carbon;
use App\User;
use App\Models\Customer;

use App\Http\Controllers\AppBaseController;


class CustomerAuthAPIController extends AppBaseController
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        // $request->validate([
        //     'first_name' => 'required|string',
        //     'last_name' => 'required|string',
        //     'gender' => 'required|string',
        //     'email' => 'required|email|unique:customers',
        //     'password' => 'required'
        // ]);

        // dd($request->all());

        $user = Customer::create([
            'first_name' => $request->json()->get('first_name'),
            'last_name' => $request->json()->get('last_name'),
            'gender' => $request->json()->get('gender'),
            'name' => $request->json()->get('first_name').' '.$request->json()->get('last_name'),
            'email' => $request->json()->get('email'),
            'password' => bcrypt($request->json()->get('password')),
            'location_address'  => $request->json()->get('location_address'),
            'location_latitude'  => $request->json()->get('location_latitude'),
            'location_longitude'  => $request->json()->get('location_longitude')
        ]);

        // $user->save();

        // return $this->sendResponse($user, "Customer retrieved successfully");x
        return response()->json([
            'data' => $user,
            'messsage' => "Customer retrieved successfully",
            'success' => true,
        ]);

        // return response()->json([
        //     'message' => 'Successfully created user!'
        // ], 201);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        

        
        $data = [
            'customer'  => $user,
            'coach' => $coach,

        ];

        return $this->sendResponse($data, "Session retrieved successfully");

        // return response()->json([
        //     'message' => 'Successfully logged out'
            // 'access_token' => $tokenResult->accessToken,
            // 'token_type' => 'Bearer',
            // 'expires_at' => Carbon::parse(
            //     $tokenResult->token->expires_at
            // )->toDateTimeString()
        // ]);

    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}