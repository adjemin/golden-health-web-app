<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSeanceAPIRequest;
use App\Http\Requests\API\UpdateSeanceAPIRequest;
use App\Models\Seance;
use App\Models\Coach;
use App\Models\Availability;
use App\Customer;
use App\Repositories\SeanceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

use App\Models\CustomerNotifications;
use App\Utils\CustomerNotificationUtils;
/**
 * Class SeanceController
 * @package App\Http\Controllers\API
 */

class SeanceAPIController extends AppBaseController
{
    /** @var  SeanceRepository */
    private $seanceRepository;

    public function __construct(SeanceRepository $seanceRepo)
    {
        $this->seanceRepository = $seanceRepo;
    }

    /**
     * Display a listing of the Seance.
     * GET|HEAD /seances
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $seances = $this->seanceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($seances->toArray(), 'Seances retrieved successfully');
    }

    /**
     * Store a newly created Seance in storage.
     * POST /seances
     *
     * @param CreateSeanceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSeanceAPIRequest $request)
    {
        $input = $request->all();

        $seance = $this->seanceRepository->create($input);

        return $this->sendResponse($seance->toArray(), 'Seance saved successfully');
    }

    /**
     * Display the specified Seance.
     * GET|HEAD /seances/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Seance $seance */
        $seance = $this->seanceRepository->find($id);

        if (empty($seance)) {
            return $this->sendError('Seance not found');
        }

        return $this->sendResponse($seance->toArray(), 'Seance retrieved successfully');
    }

    /**
     * Update the specified Seance in storage.
     * PUT/PATCH /seances/{id}
     *
     * @param int $id
     * @param UpdateSeanceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSeanceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Seance $seance */
        $seance = $this->seanceRepository->find($id);

        if (empty($seance)) {
            return $this->sendError('Seance not found');
        }

        $seance = $this->seanceRepository->update($input, $id);

        return $this->sendResponse($seance->toArray(), 'Seance updated successfully');
    }

    /**
     * Remove the specified Seance from storage.
     * DELETE /seances/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Seance $seance */
        $seance = $this->seanceRepository->find($id);

        if (empty($seance)) {
            return $this->sendError('Seance not found');
        }

        $seance->delete();

        return $this->sendSuccess('Seance deleted successfully');
    }


    public function reservationSeance(Request $request){
        $coachId = $request->json()->get('coachId');
        $customerId = $request->json()->get('customerId');
        $seanceId = $request->json()->get('seanceId');
        $packId = $request->json()->get('packId');
        $day = $request->json()->get('availabity');
        $hour = $request->json()->get('hour');

        $customer = Customer::find($customerId);

        if(empty($customer)){
            return $this->sendError('Customer not found');
        }

        $coach = Customer::find($coachId);

        if(empty($coach)){
            return $this->sendError('coach not found');
        }

        $seance = Seance::find($seanceId);

        if(empty($seance)){
            return $this->sendError('Seance not found');
        }

        $availability = Availability::create([
            'date_debut'    =>  $day,    
            'date_fin'    =>  $day,
            'hour'      => $hour,
            'coach_id'  =>  $coach->id,
            'status'    =>  0,
            'is_confirmed'  =>  0
        ]);

        $coach->notify(new CustomerAvailability($customer, $seanceId, $availability, $packId));
        
        return $this->sendResponse($availability, 'Availability created successfully');
    }


    public function ConfirmRejectSeance(Request $request){
        $customerId = $request->json()->get('customerId');
        $coachId = $request->json()->get('coachId');
        $availabilityId = $request->json()->get('availabilityId');
        $seanceId = $request->json()->get('seanceId');
        $status = $request->json()->get('status');

        $customer = Customer::find($customerId);

        if(empty($customer)){
            return $this->sendError('Customer not found');
        }

        $coach = Customer::find($coachId);

        if(empty($coach)){
            return $this->sendError('coach not found');
        }

        
        if($status == 1){
            $availability = Availability::find($availabilityId);
    
            if(empty($availability)){
                return $this->sendError('Availability not found');
            }
            
            $availability->is_confirmed = 1;
            $availability->save();
        }

        $customer->notify(new CustomerAvailability($customer, $seanceId, $availability, $packId));
        
        return $this->sendResponse($availability, 'Availability created successfully');
    }
}
