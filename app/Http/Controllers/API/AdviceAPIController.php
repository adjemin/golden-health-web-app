<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAdviceAPIRequest;
use App\Http\Requests\API\UpdateAdviceAPIRequest;
use App\Models\Advice;
use App\Repositories\AdviceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class AdviceController
 * @package App\Http\Controllers\API
 */

class AdviceAPIController extends AppBaseController
{
    /** @var  AdviceRepository */
    private $adviceRepository;

    public function __construct(AdviceRepository $adviceRepo)
    {
        $this->adviceRepository = $adviceRepo;
    }

    /**
     * Display a listing of the Advice.
     * GET|HEAD /advice
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $advice = $this->adviceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($advice->toArray(), 'Advice retrieved successfully');
    }

    /**
     * Store a newly created Advice in storage.
     * POST /advice
     *
     * @param CreateAdviceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAdviceAPIRequest $request)
    {
        $input = $request->all();

        $advice = $this->adviceRepository->create($input);

        return $this->sendResponse($advice->toArray(), 'Advice saved successfully');
    }

    /**
     * Display the specified Advice.
     * GET|HEAD /advice/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Advice $advice */
        $advice = $this->adviceRepository->find($id);

        if (empty($advice)) {
            return $this->sendError('Advice not found');
        }

        return $this->sendResponse($advice->toArray(), 'Advice retrieved successfully');
    }

    /**
     * Update the specified Advice in storage.
     * PUT/PATCH /advice/{id}
     *
     * @param int $id
     * @param UpdateAdviceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAdviceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Advice $advice */
        $advice = $this->adviceRepository->find($id);

        if (empty($advice)) {
            return $this->sendError('Advice not found');
        }

        $advice = $this->adviceRepository->update($input, $id);

        return $this->sendResponse($advice->toArray(), 'Advice updated successfully');
    }

    /**
     * Remove the specified Advice from storage.
     * DELETE /advice/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Advice $advice */
        $advice = $this->adviceRepository->find($id);

        if (empty($advice)) {
            return $this->sendError('Advice not found');
        }

        $advice->delete();

        return $this->sendSuccess('Advice deleted successfully');
    }
}
