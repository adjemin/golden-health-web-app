<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDisciplineAPIRequest;
use App\Http\Requests\API\UpdateDisciplineAPIRequest;
use App\Models\Discipline;
use App\Repositories\DisciplineRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DisciplineController
 * @package App\Http\Controllers\API
 */

class DisciplineAPIController extends AppBaseController
{
    /** @var  DisciplineRepository */
    private $disciplineRepository;

    public function __construct(DisciplineRepository $disciplineRepo)
    {
        $this->disciplineRepository = $disciplineRepo;
    }

    /**
     * Display a listing of the Discipline.
     * GET|HEAD /disciplines
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $disciplines = $this->disciplineRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($disciplines->toArray(), 'Disciplines retrieved successfully');
    }

    /**
     * Store a newly created Discipline in storage.
     * POST /disciplines
     *
     * @param CreateDisciplineAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDisciplineAPIRequest $request)
    {
        $input = $request->all();

        $discipline = $this->disciplineRepository->create($input);

        return $this->sendResponse($discipline->toArray(), 'Discipline saved successfully');
    }

    /**
     * Display the specified Discipline.
     * GET|HEAD /disciplines/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Discipline $discipline */
        $discipline = $this->disciplineRepository->find($id);

        if (empty($discipline)) {
            return $this->sendError('Discipline not found');
        }

        return $this->sendResponse($discipline->toArray(), 'Discipline retrieved successfully');
    }

    /**
     * Update the specified Discipline in storage.
     * PUT/PATCH /disciplines/{id}
     *
     * @param int $id
     * @param UpdateDisciplineAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDisciplineAPIRequest $request)
    {
        $input = $request->all();

        /** @var Discipline $discipline */
        $discipline = $this->disciplineRepository->find($id);

        if (empty($discipline)) {
            return $this->sendError('Discipline not found');
        }

        $discipline = $this->disciplineRepository->update($input, $id);

        return $this->sendResponse($discipline->toArray(), 'Discipline updated successfully');
    }

    /**
     * Remove the specified Discipline from storage.
     * DELETE /disciplines/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Discipline $discipline */
        $discipline = $this->disciplineRepository->find($id);

        if (empty($discipline)) {
            return $this->sendError('Discipline not found');
        }

        $discipline->delete();

        return $this->sendSuccess('Discipline deleted successfully');
    }
}
