<?php

namespace App\Http\Controllers\API;

use App\User;
use Response;
use App\Customer;
use App\EventTicket;
use App\Models\Order;
use App\OrderHistory;
use App\Models\Invoice;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use App\Mail\AdminNotifyMail;
use App\Models\InvoicePayment;
use App\Mail\ShopOrderNotifyMail;
use App\Http\Controllers\CinetPay;
use App\Mail\ShopOrderWaitingMail;
use App\Mail\EventTicketNotifyMail;
use App\Mail\BookingPayNotifyMail;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\AppBaseController;
use App\Repositories\InvoicePaymentRepository;
use App\Http\Requests\API\CreateInvoicePaymentAPIRequest;
use App\Http\Requests\API\UpdateInvoicePaymentAPIRequest;
use App\Models\CustomerNotifications;
use App\Utils\CustomerNotificationUtils;

/**
 * Class InvoicePaymentController
 * @package App\Http\Controllers\API
 */

class InvoicePaymentAPIController extends AppBaseController
{
    /** @var  InvoicePaymentRepository */
    private $invoicePaymentRepository;

    public function __construct(InvoicePaymentRepository $invoicePaymentRepo)
    {
        $this->invoicePaymentRepository = $invoicePaymentRepo;
    }

    /**
     * Display a listing of the InvoicePayment.
     * GET|HEAD /invoicePayments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $invoicePayments = $this->invoicePaymentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($invoicePayments->toArray(), 'Invoice Payments retrieved successfully');
    }

    /**
     * Store a newly created InvoicePayment in storage.
     * POST /invoicePayments
     *
     * @param CreateInvoicePaymentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInvoicePaymentAPIRequest $request)
    {
        $input = $request->all();

        $invoicePayment = $this->invoicePaymentRepository->create($input);

        return $this->sendResponse($invoicePayment->toArray(), 'Invoice Payment saved successfully');
    }

    /**
     * Display the specified InvoicePayment.
     * GET|HEAD /invoicePayments/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var InvoicePayment $invoicePayment */
        $invoicePayment = $this->invoicePaymentRepository->find($id);

        if (empty($invoicePayment)) {
            return $this->sendError('Invoice Payment not found');
        }

        return $this->sendResponse($invoicePayment->toArray(), 'Invoice Payment retrieved successfully');
    }

    /**
     * Update the specified InvoicePayment in storage.
     * PUT/PATCH /invoicePayments/{id}
     *
     * @param int $id
     * @param UpdateInvoicePaymentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInvoicePaymentAPIRequest $request)
    {
        $input = $request->all();

        /** @var InvoicePayment $invoicePayment */
        $invoicePayment = $this->invoicePaymentRepository->find($id);

        if (empty($invoicePayment)) {
            return $this->sendError('Invoice Payment not found');
        }

        $invoicePayment = $this->invoicePaymentRepository->update($input, $id);

        return $this->sendResponse($invoicePayment->toArray(), 'InvoicePayment updated successfully');
    }

    /**
     * Remove the specified InvoicePayment from storage.
     * DELETE /invoicePayments/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var InvoicePayment $invoicePayment */
        $invoicePayment = $this->invoicePaymentRepository->find($id);

        if (empty($invoicePayment)) {
            return $this->sendError('Invoice Payment not found');
        }

        $invoicePayment->delete();

        return $this->sendSuccess('Invoice Payment deleted successfully');
    }





    public function notify(Request $request){

        $input = $request->all();
        //

        $transaction_reference = $input['transaction_id'];
        $status = $input['status'];

        if(empty($transaction_reference)){
            return response()->json([
                'error' => [
                    'message' => "Transaction not found",
                    'transaction_reference' => $transaction_reference
                ],
            ]);
        }

         // Recuperation de la ligne de la transaction dans votre base de données
        $transaction = InvoicePayment::where(['payment_reference' =>  $transaction_reference])->first();

        $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();

        $responseData = null;
        if($invoice->status == 'unpaid'){

            if ($transaction != null) {

                /*$transaction->payment_gateway_trans_id = $cpm_trans_id;
                $transaction->payment_gateway_custom = $cpm_custom;
                $transaction->payment_gateway_currency = $cpm_currency;
                $transaction->payment_gateway_amount = $cpm_amount;
                $transaction->payment_gateway_payid = $cpm_payid;
                $transaction->payment_gateway_payment_date = $cpm_payment_date;
                $transaction->payment_gateway_payment_time = $cpm_payment_time;
                $transaction->payment_gateway_error_message = $cpm_error_message;
                $transaction->payment_gateway_payment_method = $payment_method;
                $transaction->payment_gateway_phone_prefixe = $cpm_phone_prefixe;
                $transaction->payment_gateway_cel_phone_num = $cel_phone_num;
                $transaction->payment_gateway_ipn_ack = $cpm_ipn_ack;
                $transaction->payment_gateway_created_at = $created_at;
                $transaction->payment_gateway_updated_at = $updated_at;
                $transaction->payment_gateway_cpm_result = $cpm_result;
                $transaction->payment_gateway_trans_status = $cpm_trans_status;
                $transaction->payment_gateway_designation = $cpm_designation;
                $transaction->payment_gateway_buyer_name = $buyer_name;
                $transaction->payment_gateway_signature = $signature;*/

                switch ($status) {
                    case 'SUCCESSFUL' :
                        $transaction->status = 'SUCCESSFUL';
                        $transaction->is_waiting = false;
                        $transaction->is_completed = true;
                        $transaction->save();
                        $this->validateTransaction($transaction_reference);
                    break;
                    case 'FAILED' :
                        $transaction->status = 'FAILED';
                            $transaction->is_waiting = false;
                            $transaction->is_completed = true;
                            $transaction->save();
                            $this->cancelTransaction($transaction_reference);
                        break;
                    case 'CANCELLED' :
                        $transaction->status = 'CANCELLED';
                        $transaction->is_waiting = false;
                        $transaction->is_completed = true;
                        $transaction->save();

                        $this->cancelTransaction($transaction_reference);

                        break;
                    case 'EXPIRED' :
                        $transaction->status = 'EXPIRED';
                        $transaction->is_waiting = false;
                        $transaction->is_completed = true;
                        $transaction->save();
                    break;
                    default:
                        return response()->json([
                            'error' => [
                                'message' => 'MISSING_TRANSACTION_STATUS'
                            ],
                        ]);
                    break;
                }
            }
        }


        $customer = Customer::find($invoice->customer_id);


        // operation
        return response()->json([
            'status'=> "OK",
            'messsage' => "received",
            'transaction' => $transaction,
            'invoice' => $invoice,
            'customer' => $customer,
        ]);
    }



    public function notifyCinetPay(Request $request){

        if($request->has('cpm_trans_id')){

            try {
                $id_transaction = $request->cpm_trans_id;
                $apiKey = '18122291995d9845221e7d53.99745292';
                $site_id = '182538';
                $CinetPay = new CinetPay($site_id, $apiKey);
                // Reprise exacte des bonnes données chez CinetPay
                $CinetPay->setTransId($id_transaction)->getPayStatus();
                $cpm_site_id = $CinetPay->_cpm_site_id;
                $signature = $CinetPay->_signature;
                $cpm_amount = $CinetPay->_cpm_amount;
                $cpm_trans_id = $CinetPay->_cpm_trans_id;
                $cpm_custom = $CinetPay->_cpm_custom;
                $cpm_currency = $CinetPay->_cpm_currency;
                $cpm_payid = $CinetPay->_cpm_payid;
                $cpm_payment_date = $CinetPay->_cpm_payment_date;
                $cpm_payment_time = $CinetPay->_cpm_payment_time;
                $cpm_error_message = $CinetPay->_cpm_error_message;
                $payment_method = $CinetPay->_payment_method;
                $cpm_phone_prefixe = $CinetPay->_cpm_phone_prefixe;
                $cel_phone_num = $CinetPay->_cel_phone_num;
                $cpm_ipn_ack = $CinetPay->_cpm_ipn_ack;
                $created_at = $CinetPay->_created_at;
                $updated_at = $CinetPay->_updated_at;
                $cpm_result = $CinetPay->_cpm_result;
                $cpm_trans_status = $CinetPay->_cpm_trans_status;
                $cpm_designation = $CinetPay->_cpm_designation;
                $buyer_name = $CinetPay->_buyer_name;

                // Recuperation de la ligne de la transaction dans votre base de données
                $transaction = InvoicePayment::where(['payment_reference' => $id_transaction])->first();

                $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();

                $order = Order::where(['id' => $invoice->order_id])->first();
                // Verification de l'etat du traitement de la commande
                if($invoice->status == 'unpaid'){

                    if($transaction != null){

                        $transaction->payment_gateway_trans_id = $cpm_trans_id;
                        $transaction->payment_gateway_custom = $cpm_custom;
                        $transaction->payment_gateway_currency = $cpm_currency;
                        $transaction->payment_gateway_amount = $cpm_amount;
                        $transaction->payment_gateway_payid = $cpm_payid;
                        $transaction->payment_gateway_payment_date = $cpm_payment_date;
                        $transaction->payment_gateway_payment_time = $cpm_payment_time;
                        $transaction->payment_gateway_error_message = $cpm_error_message;
                        $transaction->payment_gateway_payment_method = $payment_method;
                        $transaction->payment_gateway_phone_prefixe = $cpm_phone_prefixe;
                        $transaction->payment_gateway_cel_phone_num = $cel_phone_num;
                        $transaction->payment_gateway_ipn_ack = $cpm_ipn_ack;
                        $transaction->payment_gateway_created_at = $created_at;
                        $transaction->payment_gateway_updated_at = $updated_at;
                        $transaction->payment_gateway_cpm_result = $cpm_result;
                        $transaction->payment_gateway_trans_status = $cpm_trans_status;
                        $transaction->payment_gateway_designation = $cpm_designation;
                        $transaction->payment_gateway_buyer_name = $buyer_name;
                        $transaction->payment_gateway_signature = $signature;
                        $transaction->save();

                        // On verifie que le montant payé chez CinetPay correspond à notre montant en base de données pour cette transaction
                        if($invoice->total == $cpm_amount ){

                            // On verifie que le paiement est valide
                            if ($CinetPay->isValidPayment()) {
                                $transaction->status = 'SUCCESSFUL';
                                $transaction->is_waiting = false;
                                $transaction->is_completed = true;
                                $transaction->save();

                                $this->validateTransaction($id_transaction);

                                echo 'Felicitation, votre paiement a été effectué avec succès';
                                die();
                            } else {

                                if($cpm_result == '623'){
                                    echo "".$CinetPay->_cpm_error_message;
                                    die();
                                }else{
                                    $this->cancelTransaction($id_transaction);
                                    echo 'Echec, votre paiement a échoué pour cause : ' . $CinetPay->_cpm_error_message;
                                    die();
                                }

                            }

                        }else{
                            //Fraude : montant payé ' . $cpm_amount . ' ne correspond pas au montant de la commande
                            $transaction->status = 'REFUSED';
                            $transaction->is_waiting = false;
                            $transaction->is_completed = true;
                            $transaction->save();
                            $this->cancelTransaction($id_transaction);

                            echo 'Fraude : montant payé ' . $cpm_amount . ' ne correspond pas au montant de la commande';
                            die();

                        }

                    }

                }else{
                    // Si le paiement est bon alors ne traitez plus cette transaction : die();
                    // La commande a été déjà traité
                    // Arret du script
                    die();
                }



            } catch (\Exception $e) {
                echo "Erreur :" . $e->getMessage();
                // Une erreur s'est produite
            }

        }else {
            // redirection vers la page d'accueil
            die();
        }

    }

    public function validateTransaction($transactionId){
        $transaction = InvoicePayment::where(['payment_reference' => $transactionId])->first();


        if($transaction->status == 'SUCCESSFUL'){
            $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();
            $invoice->status = 'paid';
            $invoice->is_paid = true;
            $invoice->is_paid_by_customer = true;
            $invoice->save();

            $order = Order::where(['id' => $invoice->order_id])->first();
            //TODO: check if it's shop service
            if($order->service_slug == "shop"){
                //TODO: change stock value of the current product
                foreach($order->orderItems()->get() as $item){
                    $product = $item->product()->first();
                    if(!in_array($product->stock, ['', null, 0, '0'])){
                        $product->stock =  (int) $product->stock - (int) json_decode($item->meta_data)->quantity;
                    }else{
                        if(!in_array($product->initial_count, ['', null, 0, '0'])){
                            $product->stock =  (int) $product->stock - (int) json_decode($item->meta_data)->quantity;
                        }
                    }
                    $product->save();
                }
            }
           
            $order->status = Order::WAITING;
            $order->is_waiting = true;
            $order->save();

            $customer = Customer::where('id' ,  $order->customer_id)->first();

            $orderHistory = new OrderHistory();
            $orderHistory->order_id = $order->id;
            $orderHistory->status = Order::CUSTOMER_PAID;
            $orderHistory->order_id = $order->id;
            $orderHistory->creator = 'customers';
            $orderHistory->creator_id = $customer->id;
            $orderHistory->creator_name = $customer->name;
            $orderHistory->save();

            // Managing services
            $transaction = InvoicePayment::where(['payment_reference' => $transactionId])->first();

            $user = $customer;
            // *** MANAGING SERVICES CONTENT STATES
            // ! service event
            if($invoice->service == 'event'){
                $order = Order::where(['id' => $invoice->order_id])->first();
                foreach($order->orderItems as $orderItem){
                    $ticket = EventTicket::find($orderItem->meta_data_id);
                    if(!$ticket){
                        break;
                    }
                    $ticket->status = 'valid';
                    $ticket->is_active = true;
                    $ticket->is_paid = true;
                    $ticket->save();
                }

                $event = $ticket->event;

                // Notifying via Mail
                    $data = [
                        'subject' => "Ticket Acheté",
                        'title' => "Réservation confirmée",
                        'subtitle' => "Réservation de ".$order->orderItems->count()." place(s) à l'event $event->title confirmée",
                        // TODO make a ticket render body in html mail format
                    ];
                    $adminData = [
                        'subject' => "Nouvel achat Ticket",
                        'title' => $order->orderItems->count()." ticket(s) acheté(s) à l'instant",
                        'subtitle' => "Ticket(s) réservé(s) pour l'event ".$event->title,
                        'url' => env('APP_URL').'event_tickets/'
                    ];

                    Mail::to($customer->email)->send(new EventTicketNotifyMail($data));

                    foreach (User::all() as $user) {
                        Mail::to($user->email)->send(new AdminNotifyMail($adminData));
                    }
                // /
                // Push Notification
                $user = $customer;
                    $customerNotification = new CustomerNotifications();
                    $customerNotification->title  = $order->orderItems->count()." ticket(s) acheté(s)";

                    $customerNotification->title_en  = $order->orderItems->count()." ticket(s) bought";

                    $customerNotification->subtitle  = "Vous avez acheté ".$order->orderItems->count()." ticket(s) pour l'event ".$event->title;
                    $customerNotification->subtitle_en  = "You bought ".$order->orderItems->count()." ticket(s) for event ".$event->title;
                    $customerNotification->action  = "";
                    $customerNotification->action_by  = "";
                    if(!is_null($orderHistory)){
                        $customerNotification->meta_data  = $orderHistory->toJson() ?? "";
                    }
                    $customerNotification->type_notification  = "confirmed";
                    $customerNotification->is_read  = false;
                    $customerNotification->is_received  = false;
                    if(!is_null($order)){
                        $customerNotification->data  = $order->toJson() ?? "";
                    }
                    $customerNotification->user_id  = $user->id;
                    $customerNotification->data_id  = $order->id;
                    $customerNotification->save();

                CustomerNotificationUtils::notify($customerNotification);
                //
            }
            // ! service shop
            if($invoice->service == 'shop'){
                $orderItem = OrderItem::where(["order_id" => $order->id])->whereNotNull("meta_data_id")->first();


                $orderHistory = new OrderHistory();
                $orderHistory->order_id = $order->id;
                $orderHistory->status = Order::WAITING_CUSTOMER_DELIVERY;
                $orderHistory->order_id = $order->id;
                $orderHistory->creator = 'customers';
                $orderHistory->creator_id = $customer->id;
                $orderHistory->creator_name = $customer->name;
                $orderHistory->save();


                // Notifying via Mail
                $data = [
                    'subject' => "Votre commande ".$order->id." a été confirmée",
                    'title' => "Commande confirmée",
                    'subtitle' => "La commande de ".$order->items->count()." articles a été confirmée",
                    // TODO make a order render body in html mail format
                ];
                $adminData = [
                    'subject' => "Nouvelle commande boutique",
                    'title' => "Une nouvelle commande vient d'être confirmée",
                    'subtitle' => "",
                    'url' => env('APP_URL').'shop/orders/'.$order->id
                ];

                // $action_url = env('APP_URL').'customer/orders/shop/'.$order->id;
                $action_url = env('APP_URL').'/customer/orders/shop';

                Mail::to($customer->email)->send(new ShopOrderWaitingMail($action_url, $order));

                foreach (User::all() as $user) {
                    Mail::to($user->email)->send(new AdminNotifyMail($adminData));
                }
                // Sending push notification
                $user = $customer;
                $customerNotification = new CustomerNotifications();
                $customerNotification->title  = $user->name.", votre commande est traitée.";
                    $customerNotification->title_en  = $user->name.", your order is managed.";
                    $customerNotification->subtitle  = "Commande #".$order->id." prise en compte par notre équipe.";
                    $customerNotification->subtitle_en  = "Order #".$order->id." have been managed by owner team.";
                    $customerNotification->action  = "";
                    $customerNotification->action_by  = "";
                    if(!is_null($orderHistory)){
                        $customerNotification->meta_data  = $orderHistory->toJson() ?? "";
                    }
                    $customerNotification->type_notification  = CustomerNotifications::SHOP;
                    $customerNotification->is_read  = false;
                    $customerNotification->is_received  = false;
                    if(!is_null($order)){
                        $customerNotification->data  = $order->toJson() ?? "";
                    }
                    $customerNotification->user_id  = $user->id;
                    $customerNotification->data_id  = $order->id;
                    $customerNotification->save();

                CustomerNotificationUtils::notify($customerNotification);

                //
            }

            if($invoice->service == 'coaching'){

                $orderItem = OrderItem::where(["order_id" => $order->id])->whereNotNull("meta_data_id")->first();

                if ($orderItem) {
                    $bookingCoach = \App\Models\BookingCoach::where('id', $orderItem->meta_data_id)->first();
                    $bookingCoach->status = 2;
                    $bookingCoach->status_name = "paid";
                    $bookingCoach->save();

                    $orderHistory = new OrderHistory();
                    $orderHistory->order_id = $order->id;
                    $orderHistory->status = Order::WAITING_CUSTOMER_DELIVERY;
                    $orderHistory->order_id = $order->id;
                    $orderHistory->creator = 'customers';
                    $orderHistory->creator_id = $customer->id;
                    $orderHistory->creator_name = $customer->name;
                    $orderHistory->save();

                    if($bookingCoach) {
                    // Notifying via Mail
                        if($bookingCoach->getCourseAttribute() != null){

                            $discipline_name = isset($bookingCoach->getCourseAttribute()->discipline) ? $bookingCoach->getCourseAttribute()->discipline->name : "";
                            $data = [
                                'subject' => "Paiement de la séance effectuée",
                                'title' => "Paiement confirmé",
                                'subtitle' => "Réservation  de votre séance de ".$discipline_name." confirmée",
                                // TODO make a ticket render body in html mail format
                            ];

                            Mail::to($customer->email)->send(new BookingPayNotifyMail($data));

                            $coach = $bookingCoach->course->coach->getCustomerAttribute();
                            Mail::to($coach->email)->send(new BookingPayNotifyMail($data));
                            
                            $customerNotification = new CustomerNotifications();
                                
                            $customerNotification->title  = "Paiement de la séance effectuée";
                            $customerNotification->title_en  = "Payment of course";
                            $customerNotification->subtitle  = "Paiement confirmé";
                            $customerNotification->subtitle_en  = "Payment confirmed";
                            $customerNotification->action  = "";
                            $customerNotification->action_by  = "";
                            $customerNotification->type_notification  = "completed";
                            $customerNotification->is_read  = false;
                            $customerNotification->is_received  = false;
                            $customerNotification->user_id  = $customer->id;
                            $customerNotification->save();

                            CustomerNotificationUtils::notify($customerNotification);

                            foreach (User::all() as $user) {
                                Mail::to($user->email)->send(new AdminNotifyMail($data));
                            }
                        }

                    }
                }
            }


        }
    }

    public function cancelTransaction($transactionId){

        $transaction = InvoicePayment::where(['payment_reference' => $transactionId])->first();

        $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();
        $invoice->status = 'canceled';
        $invoice->is_paid_by_customer = false;
        $invoice->save();

        $order = Order::where(['id' => $invoice->order_id])->first();
        $order->status = 'canceled';
        $order->save();

        $customer = Customer::where('id' ,  $order->customer_id)->first();

        $orderHistory = new OrderHistory();
        $orderHistory->order_id = $order->id;
        $orderHistory->status = "canceled";
        $orderHistory->creator_id = $customer->id;
        $orderHistory->creator_name = $customer->name;
        $orderHistory->creator = "customers";
        $orderHistory->save();
    }

    public function findPaymentsByUser($customerId){

        $customer = Customer::where('id' ,  $customerId)->first();

        if (empty($customer)) {
            return $this->sendError('Customer not found', 400);
        }

        $invoicePayments = InvoicePayment::where(['creator_id' => $customerId, 'creator' => "customers"])->orderBy('created_at', 'desc')->limit(100)->get();

        return $this->sendResponse($invoicePayments->toArray(), 'Invoice Payments retrieved successfully');


   }
}