<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Password;

class ForgetPasswordApiController extends Controller
{
    public function forgot(Request  $request) {
        $input = $request->json()->all() ?? $request->all();

        if(isset($input['email']) || is_null($input['email']) ){
            return $this->sendError("Email is required");
        }

        if(!filter_var($input['email'], FILTER_VALIDATE_EMAIL)){
            return $this->sendError("this field is a email");
        }

        Password::sendResetLink($input['email']);

        return response()->json(["msg" => 'Reset password link sent on your email id.']);
    }


    public function reset(Request $request) {
        $input = $request->json()->all() ?? $request->all();

        if(isset($input['email']) || is_null($input['email']) ){
            return $this->sendError("Email is required");
        }

        if(!filter_var($input['email'], FILTER_VALIDATE_EMAIL)){
            return $this->sendError("this field is a email");
        }

        $reset_password_status = Password::reset($credentials, function ($user, $password) {
            $user->password = $password;
            $user->save();
        });

        if ($reset_password_status == Password::INVALID_TOKEN) {
            return response()->json(["msg" => "Invalid token provided"], 400);
        }

        return response()->json(["msg" => "Password has been successfully changed"]);
    }
}
