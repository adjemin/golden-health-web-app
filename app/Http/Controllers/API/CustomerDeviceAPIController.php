<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerDeviceAPIRequest;
use App\Http\Requests\API\UpdateCustomerDeviceAPIRequest;
use App\Models\CustomerDevice;
use App\Repositories\CustomerDeviceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CustomerDeviceController
 * @package App\Http\Controllers\API
 */

class CustomerDeviceAPIController extends AppBaseController
{
    /** @var  CustomerDeviceRepository */
    private $customerDeviceRepository;

    public function __construct(CustomerDeviceRepository $customerDeviceRepo)
    {
        $this->customerDeviceRepository = $customerDeviceRepo;
    }

    /**
     * Display a listing of the CustomerDevice.
     * GET|HEAD /customerDevices
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $customerDevices = $this->customerDeviceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($customerDevices->toArray(), 'Customer Devices retrieved successfully');
    }

    /**
     * Store a newly created CustomerDevice in storage.
     * POST /customerDevices
     *
     * @param CreateCustomerDeviceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerDeviceAPIRequest $request)
    {
        $input = $request->all();

        $customerDevice = $this->customerDeviceRepository->create($input);

        return $this->sendResponse($customerDevice->toArray(), 'Customer Device saved successfully');
    }

    /**
     * Display the specified CustomerDevice.
     * GET|HEAD /customerDevices/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CustomerDevice $customerDevice */
        $customerDevice = $this->customerDeviceRepository->find($id);

        if (empty($customerDevice)) {
            return $this->sendError('Customer Device not found');
        }

        return $this->sendResponse($customerDevice->toArray(), 'Customer Device retrieved successfully');
    }

    /**
     * Update the specified CustomerDevice in storage.
     * PUT/PATCH /customerDevices/{id}
     *
     * @param int $id
     * @param UpdateCustomerDeviceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerDeviceAPIRequest $request)
    {
        $input = $request->all();

        /** @var CustomerDevice $customerDevice */
        $customerDevice = $this->customerDeviceRepository->find($id);

        if (empty($customerDevice)) {
            return $this->sendError('Customer Device not found');
        }

        $customerDevice = $this->customerDeviceRepository->update($input, $id);

        return $this->sendResponse($customerDevice->toArray(), 'CustomerDevice updated successfully');
    }

    /**
     * Remove the specified CustomerDevice from storage.
     * DELETE /customerDevices/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CustomerDevice $customerDevice */
        $customerDevice = $this->customerDeviceRepository->find($id);

        if (empty($customerDevice)) {
            return $this->sendError('Customer Device not found');
        }

        $customerDevice->delete();

        return $this->sendSuccess('Customer Device deleted successfully');
    }
}
