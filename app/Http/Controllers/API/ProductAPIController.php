<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProductAPIRequest;
use App\Http\Requests\API\UpdateProductAPIRequest;
use App\Models\Product;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Symfony\Component\HttpFoundation\ParameterBag;

use Carbon\Carbon;
use App\Models\Order;
use App\CouponOrder;
use App\Models\OrderItem;
use App\OrderHistory;
use App\Models\Invoice;
use App\Models\InvoicePayment;
use App\Models\Customer;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Models\CustomerNotifications;
use App\Utils\CustomerNotificationUtils;

/**
 * Class ProductController
 * @package App\Http\Controllers\API
 */

class ProductAPIController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
    }

    /**
     * Display a listing of the Product.
     * GET|HEAD /products
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // $products = $this->productRepository->all(
        //     $request->except(['skip', 'limit']),
        //     $request->get('skip'),
        //     $request->get('limit')
        // );
        $products = Product::where([
            'is_published'  =>  1
        ])->orWhere('initial_count', '>', 0)->orWhere('stock', '>', 0)->orWhere('sold_at', '>', \Carbon\Carbon::today()->format('Y-m-d'))->get();

        return $this->sendResponse($products->toArray(), 'Products retrieved successfully');
    }

    /**
     * Store a newly created Product in storage.
     * POST /products
     *
     * @param CreateProductAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProductAPIRequest $request)
    {
        $input = $request->all();

        $product = $this->productRepository->create($input);

        return $this->sendResponse($product->toArray(), 'Product saved successfully');
    }

    /**
     * Display the specified Product.
     * GET|HEAD /products/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Product $product */
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            return $this->sendError('Product not found');
        }

        return $this->sendResponse($product->toArray(), 'Product retrieved successfully');
    }

    /**
     * Update the specified Product in storage.
     * PUT/PATCH /products/{id}
     *
     * @param int $id
     * @param UpdateProductAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductAPIRequest $request)
    {
        $input = $request->all();

        /** @var Product $product */
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            return $this->sendError('Product not found');
        }

        $product = $this->productRepository->update($input, $id);

        return $this->sendResponse($product->toArray(), 'Product updated successfully');
    }

    /**
     * Remove the specified Product from storage.
     * DELETE /products/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Product $product */
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            return $this->sendError('Product not found');
        }

        $product->delete();

        return $this->sendSuccess('Product deleted successfully');
    }

    public function orderProduct(Request $request){

        if($request->isJson()){

            /** @var ParameterBag $jsonBody */
            $jsonBody = $request->json();

            if(empty($request->json()->get('customer_id'))){

                return $this->sendError('Compte utilisateur introuvable', 400);
    
            }
    
            $customer = Customer::where('id', $request->json()->get('customer_id'))->first();
    
            if(empty($customer)){
    
                return $this->sendError('Compte utilisateur introuvable', 400);
    
            }

            if(empty($request->json()->get('items'))){

                return $this->sendError('Choix de produits introuvable', 400);
    
            }

            if(empty($request->json()->get('location_name'))){

                return $this->sendError('Lieu de livraison introuvable', 400);
    
            }

            if(empty($request->json()->get('location_latitude'))){

                return $this->sendError('Lieu de livraison introuvable', 400);
    
            }

            if(empty($request->json()->get('location_longitude'))){

                return $this->sendError('Lieu de livraison introuvable', 400);
    
            }

            try{

                $customer_id  = $request->json()->get('customer_id');
                $paymentMethod =  $request->json()->get('payment_method', "online");
                $items =  (array) $request->json()->get('items');
                $location_name = $request->json()->get('location_name');
                $location_latitude = $request->json()->get('location_latitude');
                $location_longitude = $request->json()->get('location_longitude');


                if(empty($items)){
                    return $this->sendError('Choix de produits introuvable', 400);
                }

                $currencyCode = 'XOF';
                $currencyName = 'CFA';

                $delivery_fees = (int) $request->json()->get('delivery_fees');
                $discount = (int) $request->json()->get('discount');
                // $delivery_fees = $request->json()->get('delivery_fees', "1500");

                $order = new Order();
                $order->customer_id = $customer->id;
                $order->status = Order::WAITING;
                $order->is_waiting = true;
                $order->is_delivery = false;
                $order->service_slug = 'shop';
                $order->delivery_fees =  $delivery_fees;
                $order->payment_method_slug  = $paymentMethod;
                $order->location_name =  $location_name;
                $order->location_latitude =  $location_latitude;
                $order->location_longitude =  $location_longitude;
                $order->save();

                $subtotal = 0;

                foreach ($items as $item){

                    $item = new ParameterBag(Collection::make($item)->toArray());

                    $subtotal += (double)$item->get('amount');
                    OrderItem::create([
                        'order_id'=> $order->id,
                        'meta_data_id' => $item->get('product_id'),
                        'meta_data' => json_encode($item->get('product')),
                        'quantity' => $item->get('quantity'),
                        'unit_price' => $item->get('price'),
                        'total_amount' => $item->get('amount'),
                        'color' => $item->get('color'),
                        'color_choice' => $item->get('color'),
                        'size' => $item->get('size'),
                        'size_choice' => $item->get('size'),
                        'currency_code' => $currencyCode,
                        'currency_name' => $currencyName
                    ]);
            
                }

                $tax = $subtotal * 0.0;/*Invoice::TAXES;*/
                if($discount == 0){
                    $total = $delivery_fees + $subtotal + $tax;
                }else{
                    $coupons =  (array) $request->json()->get('coupons');
                    $new_price = $subtotal;
                    foreach ($coupons as $item){
                        $item = new ParameterBag(Collection::make($item)->toArray());
                        $id = (int) $item->get('id');
                        $pourcentage = (int)$item->get('pourcentage');
                        $amount_saved = ($pourcentage/100) * $new_price;
                        $new_price = (1 - ($pourcentage/100)) * $new_price;
                        
                        $using_coupon = CouponOrder::create([
                            'order_id' => $order->id,
                            'coupon_id' => $id,
                            'amount_saved' => $amount_saved,
                        ]);
                    }
                    $total = $delivery_fees + $subtotal + $tax - $discount;
                }
    
                $order->amount = $total;
                $order->currency_code =  $currencyCode;
                $order->currency_name =  $currencyName;
                $order->save();

                // Creating an entry in OrderHistories
                $orderHistory = new OrderHistory();
                $orderHistory->order_id = $order->id;
                $orderHistory->status = Order::WAITING;
                $orderHistory->order_id = $order->id;
                $orderHistory->creator = 'customers';
                $orderHistory->creator_id = $customer->id;
                $orderHistory->creator_name = $customer->name;
                $orderHistory->save();

                $invoice = Invoice::create([
                    'order_id' => $order->id,
                    'customer_id' => $customer->id,
                    'reference' => Invoice::generateID('SHOP', $order->id, $customer->id),
                    'link' => '#',
                    'subtotal' => $subtotal,
                    'service' => 'shop',
                    'tax' => $tax,
                    'fees_delivery' => $delivery_fees,
                    'discount'  =>  $discount,
                    'total' => $total,
                    'status' => 'unpaid',
                    'is_paid' => false,
                    'is_paid_by_customer' => false,
                    'is_paid_by_delivery_service' => false,
                    'currency_code' => $currencyCode,
                    'currency_name' => $currencyName
                ]);

                $transaction = null;

                if($paymentMethod=="online"){

                    $transactionId = self::generateTransId($customer->id, $invoice->id, $order->id);


                    $transaction = InvoicePayment::create([
                        'invoice_id' =>  $invoice->id,
                        'payment_method'=> "online",
                        'payment_reference' => $transactionId,
                        'amount' => $invoice->total,
                        'currency_code' => $invoice->currency_code,
                        'currency_name' => $invoice->currency_name,
                        'creator_id' => $customer->id,
                        'creator' => 'customers',
                        'creator_name' => $customer->name,
                        'status' => 'WAITING',
                        'is_waiting' => true,
                        'is_completed'=> false
                    ]);
                }

                // $final_price = (int) $transaction->total;
                $final_price = (int) $transaction->amount;

                $result = [
                    "order" => $order,
                    'transaction' => $transaction == null? null:[
                        'payment_method'=> $transaction->payment_method,
                        'transaction_id' => $transactionId,
                        'transaction_designation' => 'Golden Health Buying Product',
                        'amount' => $final_price,
                        'currency_code' => $transaction->currency_code,
                        'currency_name' => $transaction->currency_name,
                        'status' => $transaction->status
                    ]
                ];
        
                return $this->sendResponse($result, 'Participation saved successfully');



            }catch (\Exception $exception){
                return $this->sendError('Error '.$exception, 503);
            }



        }else{
            return $this->sendError('JSON introuvable', 400); 
        }


    }

    /**
     * generate transId
     * @return int
     */
    public static function generateTransId($customerId, $invoiceId, $orderId)
    {
        $timestamp = time();
        $parts = explode(' ', microtime());
        $id = ($timestamp + $parts[0] - strtotime('today 00:00')) * 10;
        $id = sprintf('%06d', $id) . mt_rand(100, 9999);
        $transId = $id."-".$customerId."-".$invoiceId."-".$orderId;

        $transactions = InvoicePayment::where(['payment_reference' => $transId])->get();
        if( count($transactions) == 0){

            return  $transId;
        }else{
            $autoIncrement = count($transactions) + 1;
            $transId = $transId.'-'.$autoIncrement;
            return  $transId;
        }


    }
}
