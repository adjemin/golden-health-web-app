<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateConnexionAPIRequest;
use App\Http\Requests\API\UpdateConnexionAPIRequest;
use App\Models\Connexion;
use App\Repositories\ConnexionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ConnexionController
 * @package App\Http\Controllers\API
 */

class ConnexionAPIController extends AppBaseController
{
    /** @var  ConnexionRepository */
    private $connexionRepository;

    public function __construct(ConnexionRepository $connexionRepo)
    {
        $this->connexionRepository = $connexionRepo;
    }

    /**
     * Display a listing of the Connexion.
     * GET|HEAD /connexions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $connexions = $this->connexionRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($connexions->toArray(), 'Connexions retrieved successfully');
    }

    /**
     * Store a newly created Connexion in storage.
     * POST /connexions
     *
     * @param CreateConnexionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateConnexionAPIRequest $request)
    {
        $input = $request->all();

        $connexion = $this->connexionRepository->create($input);

        return $this->sendResponse($connexion->toArray(), 'Connexion saved successfully');
    }

    /**
     * Display the specified Connexion.
     * GET|HEAD /connexions/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Connexion $connexion */
        $connexion = $this->connexionRepository->find($id);

        if (empty($connexion)) {
            return $this->sendError('Connexion not found');
        }

        return $this->sendResponse($connexion->toArray(), 'Connexion retrieved successfully');
    }

    /**
     * Update the specified Connexion in storage.
     * PUT/PATCH /connexions/{id}
     *
     * @param int $id
     * @param UpdateConnexionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateConnexionAPIRequest $request)
    {
        $input = $request->all();

        /** @var Connexion $connexion */
        $connexion = $this->connexionRepository->find($id);

        if (empty($connexion)) {
            return $this->sendError('Connexion not found');
        }

        $connexion = $this->connexionRepository->update($input, $id);

        return $this->sendResponse($connexion->toArray(), 'Connexion updated successfully');
    }

    /**
     * Remove the specified Connexion from storage.
     * DELETE /connexions/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Connexion $connexion */
        $connexion = $this->connexionRepository->find($id);

        if (empty($connexion)) {
            return $this->sendError('Connexion not found');
        }

        $connexion->delete();

        return $this->sendSuccess('Connexion deleted successfully');
    }

    public function connexion(Request $request){
        
    }
}
