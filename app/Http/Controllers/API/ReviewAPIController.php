<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateReviewAPIRequest;
use App\Http\Requests\API\UpdateReviewAPIRequest;
use App\Models\Review;
use App\Repositories\ReviewRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ReviewController
 * @package App\Http\Controllers\API
 */

class ReviewAPIController extends AppBaseController
{
    /** @var  ReviewRepository */
    private $reviewRepository;

    public function __construct(ReviewRepository $reviewRepo)
    {
        $this->reviewRepository = $reviewRepo;
    }

    /**
     * Display a listing of the Review.
     * GET|HEAD /reviews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $reviews = $this->reviewRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($reviews->toArray(), 'Reviews retrieved successfully');
    }

    /**
     * Store a newly created Review in storage.
     * POST /reviews
     *
     * @param CreateReviewAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateReviewAPIRequest $request)
    {
        $input = $request->all();

        $review = $this->reviewRepository->create($input);

        return $this->sendResponse($review->toArray(), 'Review saved successfully');
    }

    /**
     * Display the specified Review.
     * GET|HEAD /reviews/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Review $review */
        $review = $this->reviewRepository->find($id);

        if (empty($review)) {
            return $this->sendError('Review not found');
        }

        return $this->sendResponse($review->toArray(), 'Review retrieved successfully');
    }

    /**
     * Update the specified Review in storage.
     * PUT/PATCH /reviews/{id}
     *
     * @param int $id
     * @param UpdateReviewAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReviewAPIRequest $request)
    {
        $input = $request->all();

        /** @var Review $review */
        $review = $this->reviewRepository->find($id);

        if (empty($review)) {
            return $this->sendError('Review not found');
        }

        $review = $this->reviewRepository->update($input, $id);

        return $this->sendResponse($review->toArray(), 'Review updated successfully');
    }

    /**
     * Remove the specified Review from storage.
     * DELETE /reviews/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Review $review */
        $review = $this->reviewRepository->find($id);

        if (empty($review)) {
            return $this->sendError('Review not found');
        }

        $review->delete();

        return $this->sendSuccess('Review deleted successfully');
    }

    public function reviewProduct($product_id, Request $request){
        $reviews = Review::where([
            'service' =>  'shop',
            'source_id' =>  $product_id
        ])->get();

        return $this->sendResponse($reviews->toArray(), 'reviews retrieved successfully');
    }

    public function reviewCourse($course_id, Request $request){
        $reviews = Review::where([
            'service' =>  'booking',
            'source_id' =>  $course_id
        ])->get();

        return $this->sendResponse($reviews->toArray(), 'reviews retrieved successfully');
    }

    public function showReviews($customerId){
        $customer = \App\Customer::find($customerId);

        if(is_null($customer) || empty($customer)){
            return $this->sendError("Customer not found");
        }
        
        $deliveredOrders = $customer->productsToReviewApi();

        if(is_null($deliveredOrders)){
            $deliveredOrders = [];
        }

        $bookings = \App\Models\Customer::find($customer->id)->booking()->get();
        $pastCourses = collect($bookings)->map(function($booking){
            return $booking->course()->first();
        })->unique()->filter(function($course, $key){
            if(is_null($course)){
                return false;
            }else{
                return !is_null($course->availability) ? $course->availability->isPast() : false;
            }
        })->toArray();

        if(\App\Models\Customer::find($customer->id)->is_coach){
            $reviews = \App\Models\Customer::find($customer->id)->is_coach->reviews;
        }else{
            $reviews = [];
        }

        $data = [
            'reviews'   =>  $reviews,
            'pastCourses'   =>  $pastCourses,
            'deliveredOrders'   =>  $deliveredOrders
        ];

        return $this->sendResponse($data, "data retrieved successully");
    }

    public function storeReviews(Request $request){
        //
        $customer = \App\Customer::find($request->customerId);

        if(is_null($customer) || empty($customer)){
            return $this->sendError("Customer not found");
        }

        $input = $request->json()->all() ?? $request->all();

        if (!isset($input['rating'])) {
            return $this->sendError("rating not found");
        }

        if (!isset($input['product_id'])) {
            return $this->sendError("product_id not found");
        }

        $review = new Review();
        $review->rating = $input['rating'];
        $review->service = "shop";
        $review->source = "product";
        $review->source_id = $input['product_id'];

        if (isset($input['title'])) {
            $review->title = $input['title'];
        }

        $review->customer_id = $customer->id;

        if (isset($input['content'])) {
            $review->content = $input['content'];
        }

        $review->save();

        return $this->sendResponse($review,"Vous venez de noter le cours avec succès");
    }

    public function storeReviewsCourse(Request $request){
        $customer = \App\Customer::find($request->customerId);

        if(is_null($customer) || empty($customer)){
            return $this->sendError("Customer not found");
        }

        $input = $request->all();

        if (!isset($input['rating'])) {
            return $this->sendError("rating not found");
        }

        if (!isset($input['course_id'])) {
            return $this->sendError("course_id not found");
        }

        $review = new Review();
        $review->rating = $input['rating'];
        $review->service = "booking";
        $review->source = "course";
        $review->source_id = $input['course_id'];

        if (isset($input['title'])) {
            $review->title = $input['title'];
        }

        $review->customer_id = $customer->id;

        if (isset($input['content'])) {
            $review->content = $input['content'];
        }

        $review->save();

        //coach change rating
        $course = \App\Models\Course::find($input['course_id']);
        $coach_course = \App\Models\Coach::find($course->coach_id);
        $coach_course->nbr_personne = $coach_course->nbr_personne + 1;
        $coach_course->sum = $coach_course->sum + (int) $input['rating'];
        $coach_course->save();
        $coach_course->rating = (string) ((int) ($coach_course->sum /  $coach_course->nbr_personne));
        $coach_course->save();

        return $this->sendResponse($review,"Vous venez de noter le cours avec succès");
    }
}
