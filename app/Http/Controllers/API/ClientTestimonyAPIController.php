<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateClientTestimonyAPIRequest;
use App\Http\Requests\API\UpdateClientTestimonyAPIRequest;
use App\Models\ClientTestimony;
use App\Repositories\ClientTestimonyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ClientTestimonyController
 * @package App\Http\Controllers\API
 */

class ClientTestimonyAPIController extends AppBaseController
{
    /** @var  ClientTestimonyRepository */
    private $clientTestimonyRepository;

    public function __construct(ClientTestimonyRepository $clientTestimonyRepo)
    {
        $this->clientTestimonyRepository = $clientTestimonyRepo;
    }

    /**
     * Display a listing of the ClientTestimony.
     * GET|HEAD /clientTestimonies
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $clientTestimonies = $this->clientTestimonyRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($clientTestimonies->toArray(), 'Client Testimonies retrieved successfully');
    }

    /**
     * Store a newly created ClientTestimony in storage.
     * POST /clientTestimonies
     *
     * @param CreateClientTestimonyAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateClientTestimonyAPIRequest $request)
    {
        $input = $request->all();

        $clientTestimony = $this->clientTestimonyRepository->create($input);

        return $this->sendResponse($clientTestimony->toArray(), 'Client Testimony saved successfully');
    }

    /**
     * Display the specified ClientTestimony.
     * GET|HEAD /clientTestimonies/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ClientTestimony $clientTestimony */
        $clientTestimony = $this->clientTestimonyRepository->find($id);

        if (empty($clientTestimony)) {
            return $this->sendError('Client Testimony not found');
        }

        return $this->sendResponse($clientTestimony->toArray(), 'Client Testimony retrieved successfully');
    }

    /**
     * Update the specified ClientTestimony in storage.
     * PUT/PATCH /clientTestimonies/{id}
     *
     * @param int $id
     * @param UpdateClientTestimonyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClientTestimonyAPIRequest $request)
    {
        $input = $request->all();

        /** @var ClientTestimony $clientTestimony */
        $clientTestimony = $this->clientTestimonyRepository->find($id);

        if (empty($clientTestimony)) {
            return $this->sendError('Client Testimony not found');
        }

        $clientTestimony = $this->clientTestimonyRepository->update($input, $id);

        return $this->sendResponse($clientTestimony->toArray(), 'ClientTestimony updated successfully');
    }

    /**
     * Remove the specified ClientTestimony from storage.
     * DELETE /clientTestimonies/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ClientTestimony $clientTestimony */
        $clientTestimony = $this->clientTestimonyRepository->find($id);

        if (empty($clientTestimony)) {
            return $this->sendError('Client Testimony not found');
        }

        $clientTestimony->delete();

        return $this->sendSuccess('Client Testimony deleted successfully');
    }
}
