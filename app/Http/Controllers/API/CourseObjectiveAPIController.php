<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCourseObjectiveAPIRequest;
use App\Http\Requests\API\UpdateCourseObjectiveAPIRequest;
use App\Models\CourseObjective;
use App\Repositories\CourseObjectiveRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CourseObjectiveController
 * @package App\Http\Controllers\API
 */

class CourseObjectiveAPIController extends AppBaseController
{
    /** @var  CourseObjectiveRepository */
    private $courseObjectiveRepository;

    public function __construct(CourseObjectiveRepository $courseObjectiveRepo)
    {
        $this->courseObjectiveRepository = $courseObjectiveRepo;
    }

    /**
     * Display a listing of the CourseObjective.
     * GET|HEAD /courseObjectives
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $courseObjectives = $this->courseObjectiveRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($courseObjectives->toArray(), 'Course Objectives retrieved successfully');
    }

    /**
     * Store a newly created CourseObjective in storage.
     * POST /courseObjectives
     *
     * @param CreateCourseObjectiveAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCourseObjectiveAPIRequest $request)
    {
        $input = $request->all();

        $courseObjective = $this->courseObjectiveRepository->create($input);

        return $this->sendResponse($courseObjective->toArray(), 'Course Objective saved successfully');
    }

    /**
     * Display the specified CourseObjective.
     * GET|HEAD /courseObjectives/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CourseObjective $courseObjective */
        $courseObjective = $this->courseObjectiveRepository->find($id);

        if (empty($courseObjective)) {
            return $this->sendError('Course Objective not found');
        }

        return $this->sendResponse($courseObjective->toArray(), 'Course Objective retrieved successfully');
    }

    /**
     * Update the specified CourseObjective in storage.
     * PUT/PATCH /courseObjectives/{id}
     *
     * @param int $id
     * @param UpdateCourseObjectiveAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCourseObjectiveAPIRequest $request)
    {
        $input = $request->all();

        /** @var CourseObjective $courseObjective */
        $courseObjective = $this->courseObjectiveRepository->find($id);

        if (empty($courseObjective)) {
            return $this->sendError('Course Objective not found');
        }

        $courseObjective = $this->courseObjectiveRepository->update($input, $id);

        return $this->sendResponse($courseObjective->toArray(), 'CourseObjective updated successfully');
    }

    /**
     * Remove the specified CourseObjective from storage.
     * DELETE /courseObjectives/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CourseObjective $courseObjective */
        $courseObjective = $this->courseObjectiveRepository->find($id);

        if (empty($courseObjective)) {
            return $this->sendError('Course Objective not found');
        }

        $courseObjective->delete();

        return $this->sendSuccess('Course Objective deleted successfully');
    }
}
