<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDiplomeAPIRequest;
use App\Http\Requests\API\UpdateDiplomeAPIRequest;
use App\Models\Diplome;
use App\Repositories\DiplomeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DiplomeController
 * @package App\Http\Controllers\API
 */

class DiplomeAPIController extends AppBaseController
{
    /** @var  DiplomeRepository */
    private $diplomeRepository;

    public function __construct(DiplomeRepository $diplomeRepo)
    {
        $this->diplomeRepository = $diplomeRepo;
    }

    /**
     * Display a listing of the Diplome.
     * GET|HEAD /diplomes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $diplomes = $this->diplomeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($diplomes->toArray(), 'Diplomes retrieved successfully');
    }

    /**
     * Store a newly created Diplome in storage.
     * POST /diplomes
     *
     * @param CreateDiplomeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDiplomeAPIRequest $request)
    {
        $input = $request->all();

        $diplome = $this->diplomeRepository->create($input);

        return $this->sendResponse($diplome->toArray(), 'Diplome saved successfully');
    }

    /**
     * Display the specified Diplome.
     * GET|HEAD /diplomes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Diplome $diplome */
        $diplome = $this->diplomeRepository->find($id);

        if (empty($diplome)) {
            return $this->sendError('Diplome not found');
        }

        return $this->sendResponse($diplome->toArray(), 'Diplome retrieved successfully');
    }

    /**
     * Update the specified Diplome in storage.
     * PUT/PATCH /diplomes/{id}
     *
     * @param int $id
     * @param UpdateDiplomeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDiplomeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Diplome $diplome */
        $diplome = $this->diplomeRepository->find($id);

        if (empty($diplome)) {
            return $this->sendError('Diplome not found');
        }

        $diplome = $this->diplomeRepository->update($input, $id);

        return $this->sendResponse($diplome->toArray(), 'Diplome updated successfully');
    }

    /**
     * Remove the specified Diplome from storage.
     * DELETE /diplomes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Diplome $diplome */
        $diplome = $this->diplomeRepository->find($id);

        if (empty($diplome)) {
            return $this->sendError('Diplome not found');
        }

        $diplome->delete();

        return $this->sendSuccess('Diplome deleted successfully');
    }
}
