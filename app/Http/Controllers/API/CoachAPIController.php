<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCoachAPIRequest;
use App\Http\Requests\API\UpdateCoachAPIRequest;
use App\Models\Coach;
use App\Models\Course;
use App\Models\CoachDiscipline;
use App\Models\Customer;
use App\Repositories\CoachRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CoachController
 * @package App\Http\Controllers\API
 */

class CoachAPIController extends AppBaseController
{
    /** @var  CoachRepository */
    private $coachRepository;

    public function __construct(CoachRepository $coachRepo)
    {
        $this->coachRepository = $coachRepo;
    }

    /**
     * Display a listing of the Coach.
     * GET|HEAD /coaches
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $coaches = $this->coachRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($coaches->toArray(), 'Coaches retrieved successfully');
    }

    /**
     * Store a newly created Coach in storage.
     * POST /coaches
     *
     * @param CreateCoachAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCoachAPIRequest $request)
    {
        $input = $request->all();

        $coach = $this->coachRepository->create($input);

        return $this->sendResponse($coach->toArray(), 'Coach saved successfully');
    }

    /**
     * Display the specified Coach.
     * GET|HEAD /coaches/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Coach $coach */
        $coach = $this->coachRepository->find($id);

        if (empty($coach)) {
            return $this->sendError('Coach not found');
        }

        return $this->sendResponse($coach->toArray(), 'Coach retrieved successfully');
    }

    /**
     * Update the specified Coach in storage.
     * PUT/PATCH /coaches/{id}
     *
     * @param int $id
     * @param UpdateCoachAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCoachAPIRequest $request)
    {
        $input = $request->all();

        /** @var Coach $coach */
        $coach = $this->coachRepository->find($id);

        if (empty($coach)) {
            return $this->sendError('Coach not found');
        }

        $coach = $this->coachRepository->update($input, $id);

        return $this->sendResponse($coach->toArray(), 'Coach updated successfully');
    }

    /**
     * Remove the specified Coach from storage.
     * DELETE /coaches/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Coach $coach */
        $coach = $this->coachRepository->find($id);

        if (empty($coach)) {
            return $this->sendError('Coach not found');
        }

        $coach->delete();

        return $this->sendSuccess('Coach deleted successfully');
    }

    public function searchCoachesByLocationAndDiscipline(Request $request){

        $discipline_id = $request->discipline_id;
        $latitude = $request->latitude;
        $longitude = $request->longitude;

        if (empty($discipline_id)) {
            return $this->sendError('discipline_id not found');
        }

        if (empty($latitude)) {
            return $this->sendError('latitude not found');
        }

        if (empty($longitude)) {
            return $this->sendError('longitude not found');
        }

        
        $users = $this->getUsers($request);
        
        $ids = collect($users->get())->map(function($user){
            return $user->id;
        });

        $coaches = Coach::whereIn('customer_id', $ids)->get();

        $courses = null;

        if(!empty($request->discipline_id)){

            $coach_ids =  collect($coaches)->map(function($coach){
                return $coach->id;
            });

            $disciplines = CoachDiscipline::where('discipline_id', $request->discipline_id)
                ->whereIn('coach_id', $coach_ids)->get();


                $coach_ids =  collect($disciplines)->map(function($coachDiscipline){
                    return $coachDiscipline->coach_id;
                });    

            
            $courses = Course::where('active', true)->whereIn('coach_id', $coach_ids)->get();
        }else{
            $coach_ids =  collect($coaches)->map(function($user){
                return $user->id;
            });
            $courses = Course::where('active', true)->whereIn('coach_id', $coach_ids)->get();
        }


        return $this->sendResponse($courses, 'Coach retrieved successfully');
    }

    public function searchCoachesByLocationAndDisciplineV2(Request $request){
        $discipline_id = $request->discipline_id;
        $ville = $request->ville ?? $request->query('ville') ?? $request->commune ?? $request->query('commune');

        if (empty($discipline_id)) {
            return $this->sendError('discipline_id not found');
        }

        if (empty($ville)) {
            return $this->sendError('ville not found');
        }

        $users = $this->getUsers($request);
        
        $ids = collect($users->get())->map(function($user){
            return $user->id;
        });

        $coaches = Coach::whereIn('customer_id', $ids)->get();

        $coach_ids =  collect($coaches)->map(function($coach){
            return $coach->id;
        });

        // $disciplines = CoachDiscipline::where('discipline_id', $request->discipline_id)
        //     ->whereIn('coach_id', $coach_ids)->get();


        // $coach_ids =  collect($disciplines)->map(function($coachDiscipline){
        //     return $coachDiscipline->coach_id;
        // });
        
        $courses = Course::where([
            'active' => true,
            'discipline_id' => $discipline_id
        ])->whereIn('coach_id', $coach_ids)->latest()->get();

        return $this->sendResponse($courses, 'Course retrieved successfully');
    }

    public function getUsers(Request $request){
        $city =  $request->ville ?? $request->query('ville') ?? $request->commune ?? $request->query('commune');
        $lat = $request->lat ?? $request->query('lat');
        $lon = $request->lon ?? $request->query('lon');

        if(is_null($lat) || is_null($lon)) {
            $response = Customer::whereNull('deleted_at')->where('is_active', 1)->orWhere('commune', 'like', '%'.$city.'%')->orWhere('location_address', 'like', '%'.$city.'%')->orWhere('quartier', 'like', '%'.$city.'%');
            return $response;
        }else {
            $latitude = $lat;
            $longitude = $lon;
            $method = $request->method ?? 'geofence';

            if($method == "geofence") {
                $inner_radius = 0;

                $outer_radius = 100;
                $response = Customer::geofence($latitude, $longitude, $inner_radius, $outer_radius)->orWhere('commune', 'like', '%'.$city.'%')->orWhere('location_address', 'like', '%'.$city.'%')->orWhere('quartier', 'like', '%'.$city.'%')->where('is_active', 1);
                return $response;
            }else {
                $response = Customer::distance($latitude, $longitude)->orWhere('commune', 'like', '%'.$city.'%')->orWhere('location_address', 'like', '%'.$city.'%')->orWhere('quartier', 'like', '%'.$city.'%')->where('is_active', 1);
                return $response;
            }
        }
    }

    public function filter(Request $request){

        $genders = $request->json()->get('genders');
        $course_type = $request->json()->get('type_course');
        $course_lieu = $request->json()->get('lieu_course');
        $disciplines = (array)$request->json()->get('disciplines');

        $genderList  = [];

        if(!empty($genders)){

            foreach($genders as $gender){
                if($gender == "homme"){
                    array_push($genderList, "MR");
                }

                if($gender == "femme"){
                    array_push($genderList, "MLLE");
                    array_push($genderList, "MME");
                }
            }

        }



        if(empty($genderList)){
            $customers = Customer::where('type_account','coach')->get(); 
        }else{
            $customers = Customer::where('type_account','coach')->whereIn('gender',$genderList)->get(); 
        }
       

        $ids = collect($customers)->map(function($user){
            return $user->id;
        });

        $coaches = Coach::whereIn('customer_id', $ids)->get();

      

        if(empty($disciplines)){

            $coach_ids =  collect($coaches)->map(function($coach){
                return $coach->id;
            });

            if(!is_null($course_type)){
                $courses = Course::whereIn('coach_id', $coach_ids)->where('course_type', $course_type)->where('active', 1)->orderBy('id', 'desc')->latest()->get();
            }else{
                $courses = Course::whereIn('coach_id', $coach_ids)->where('active', 1)->orderBy('id', 'desc')->latest()->get();
            }

            if(!is_null($course_lieu)){
                $courses = collect($courses)->filter(function($course) use ($course_lieu){
                    $lieux = $course->lieu()->get();
                    foreach ($lieux as $lieu) {
                        if($lieu->count() > 0)
                        {
                            $lieu_course = $lieu->lieuCourse;
                            if(!is_null($lieu_course)){
                                if($lieu_course->first()->slug == $course_lieu)
                                {
                                    return true;
                                }
                            }else{
                                continue;
                            }
                        }else{
                            continue;
                        }
                    }
                    return false;
                });
            }

            // $courses = Course::where('active', true)->whereIn('coach_id', $coach_ids)->get();

        }else{

            // $disciplines = CoachDiscipline::whereIn('discipline_id', $disciplines)
            // ->whereIn('coach_id', $ids)->get();

            // $coach_ids =  collect($disciplines)->map(function($coachDiscipline){
            //     return $coachDiscipline->coach_id;
            // });

            $coach_ids =  collect($coaches)->map(function($coach){
                return $coach->id;
            });
            
            if(!is_null($course_type)){
                $courses = Course::whereIn('coach_id', $coach_ids)->where('active', 1)->where('course_type', $course_type)->orderBy('id', 'desc')->latest()->get();
            }else{
                $courses = Course::whereIn('coach_id', $coach_ids)->where('active', 1)->orderBy('id', 'desc')->latest()->get();
            }

            if(!is_null($course_lieu)){
                $courses = collect($courses)->filter(function($course) use ($course_lieu){
                    $lieux = $course->lieu()->get();
                    foreach ($lieux as $lieu) {
                        if($lieu->count() > 0)
                        {
                            $lieu_course = $lieu->lieuCourse;
                            if(!is_null($lieu_course)){
                                if($lieu_course->first()->slug == $course_lieu)
                                {
                                    return true;
                                }
                            }else{
                                continue;
                            }
                        }else{
                            continue;
                        }
                    }
                    return false;
                });
            }
            
            // $courses = Course::where('active', true)->whereIn('coach_id', $coach_ids)->get();

        }

        return $this->sendResponse($courses, 'Coach retrieved successfully');

    }


}
