<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatedistinctionAPIRequest;
use App\Http\Requests\API\UpdatedistinctionAPIRequest;
use App\Models\distinction;
use App\Repositories\distinctionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class distinctionController
 * @package App\Http\Controllers\API
 */

class distinctionAPIController extends AppBaseController
{
    /** @var  distinctionRepository */
    private $distinctionRepository;

    public function __construct(distinctionRepository $distinctionRepo)
    {
        $this->distinctionRepository = $distinctionRepo;
    }

    /**
     * Display a listing of the distinction.
     * GET|HEAD /distinctions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $distinctions = $this->distinctionRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($distinctions->toArray(), 'Distinctions retrieved successfully');
    }

    /**
     * Store a newly created distinction in storage.
     * POST /distinctions
     *
     * @param CreatedistinctionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatedistinctionAPIRequest $request)
    {
        $input = $request->all();

        $distinction = $this->distinctionRepository->create($input);

        return $this->sendResponse($distinction->toArray(), 'Distinction saved successfully');
    }

    /**
     * Display the specified distinction.
     * GET|HEAD /distinctions/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var distinction $distinction */
        $distinction = $this->distinctionRepository->find($id);

        if (empty($distinction)) {
            return $this->sendError('Distinction not found');
        }

        return $this->sendResponse($distinction->toArray(), 'Distinction retrieved successfully');
    }

    /**
     * Update the specified distinction in storage.
     * PUT/PATCH /distinctions/{id}
     *
     * @param int $id
     * @param UpdatedistinctionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatedistinctionAPIRequest $request)
    {
        $input = $request->all();

        /** @var distinction $distinction */
        $distinction = $this->distinctionRepository->find($id);

        if (empty($distinction)) {
            return $this->sendError('Distinction not found');
        }

        $distinction = $this->distinctionRepository->update($input, $id);

        return $this->sendResponse($distinction->toArray(), 'distinction updated successfully');
    }

    /**
     * Remove the specified distinction from storage.
     * DELETE /distinctions/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var distinction $distinction */
        $distinction = $this->distinctionRepository->find($id);

        if (empty($distinction)) {
            return $this->sendError('Distinction not found');
        }

        $distinction->delete();

        return $this->sendSuccess('Distinction deleted successfully');
    }
}
