<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHowDidYouKnowAPIRequest;
use App\Http\Requests\API\UpdateHowDidYouKnowAPIRequest;
use App\Models\HowDidYouKnow;
use App\Repositories\HowDidYouKnowRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class HowDidYouKnowController
 * @package App\Http\Controllers\API
 */

class HowDidYouKnowAPIController extends AppBaseController
{
    /** @var  HowDidYouKnowRepository */
    private $howDidYouKnowRepository;

    public function __construct(HowDidYouKnowRepository $howDidYouKnowRepo)
    {
        $this->howDidYouKnowRepository = $howDidYouKnowRepo;
    }

    /**
     * Display a listing of the HowDidYouKnow.
     * GET|HEAD /howDidYouKnows
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $howDidYouKnows = $this->howDidYouKnowRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($howDidYouKnows->toArray(), 'How Did You Knows retrieved successfully');
    }

    /**
     * Store a newly created HowDidYouKnow in storage.
     * POST /howDidYouKnows
     *
     * @param CreateHowDidYouKnowAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHowDidYouKnowAPIRequest $request)
    {
        $input = $request->all();

        $howDidYouKnow = $this->howDidYouKnowRepository->create($input);

        return $this->sendResponse($howDidYouKnow->toArray(), 'How Did You Know saved successfully');
    }

    /**
     * Display the specified HowDidYouKnow.
     * GET|HEAD /howDidYouKnows/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var HowDidYouKnow $howDidYouKnow */
        $howDidYouKnow = $this->howDidYouKnowRepository->find($id);

        if (empty($howDidYouKnow)) {
            return $this->sendError('How Did You Know not found');
        }

        return $this->sendResponse($howDidYouKnow->toArray(), 'How Did You Know retrieved successfully');
    }

    /**
     * Update the specified HowDidYouKnow in storage.
     * PUT/PATCH /howDidYouKnows/{id}
     *
     * @param int $id
     * @param UpdateHowDidYouKnowAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHowDidYouKnowAPIRequest $request)
    {
        $input = $request->all();

        /** @var HowDidYouKnow $howDidYouKnow */
        $howDidYouKnow = $this->howDidYouKnowRepository->find($id);

        if (empty($howDidYouKnow)) {
            return $this->sendError('How Did You Know not found');
        }

        $howDidYouKnow = $this->howDidYouKnowRepository->update($input, $id);

        return $this->sendResponse($howDidYouKnow->toArray(), 'HowDidYouKnow updated successfully');
    }

    /**
     * Remove the specified HowDidYouKnow from storage.
     * DELETE /howDidYouKnows/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var HowDidYouKnow $howDidYouKnow */
        $howDidYouKnow = $this->howDidYouKnowRepository->find($id);

        if (empty($howDidYouKnow)) {
            return $this->sendError('How Did You Know not found');
        }

        $howDidYouKnow->delete();

        return $this->sendSuccess('How Did You Know deleted successfully');
    }
}
