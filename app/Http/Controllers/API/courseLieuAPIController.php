<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatecourseLieuAPIRequest;
use App\Http\Requests\API\UpdatecourseLieuAPIRequest;
use App\Models\CourseLieu;
use App\Repositories\courseLieuRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class courseLieuController
 * @package App\Http\Controllers\API
 */

class courseLieuAPIController extends AppBaseController
{
    /** @var  courseLieuRepository */
    private $courseLieuRepository;

    public function __construct(courseLieuRepository $courseLieuRepo)
    {
        $this->courseLieuRepository = $courseLieuRepo;
    }

    /**
     * Display a listing of the courseLieu.
     * GET|HEAD /courseLieus
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $courseLieus = $this->courseLieuRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($courseLieus->toArray(), 'Course Lieus retrieved successfully');
    }

    /**
     * Store a newly created courseLieu in storage.
     * POST /courseLieus
     *
     * @param CreatecourseLieuAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatecourseLieuAPIRequest $request)
    {
        $input = $request->all();

        $courseLieu = $this->courseLieuRepository->create($input);

        return $this->sendResponse($courseLieu->toArray(), 'Course Lieu saved successfully');
    }

    /**
     * Display the specified courseLieu.
     * GET|HEAD /courseLieus/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var courseLieu $courseLieu */
        $courseLieu = $this->courseLieuRepository->find($id);

        if (empty($courseLieu)) {
            return $this->sendError('Course Lieu not found');
        }

        return $this->sendResponse($courseLieu->toArray(), 'Course Lieu retrieved successfully');
    }

    /**
     * Update the specified courseLieu in storage.
     * PUT/PATCH /courseLieus/{id}
     *
     * @param int $id
     * @param UpdatecourseLieuAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecourseLieuAPIRequest $request)
    {
        $input = $request->all();

        /** @var courseLieu $courseLieu */
        $courseLieu = $this->courseLieuRepository->find($id);

        if (empty($courseLieu)) {
            return $this->sendError('Course Lieu not found');
        }

        $courseLieu = $this->courseLieuRepository->update($input, $id);

        return $this->sendResponse($courseLieu->toArray(), 'courseLieu updated successfully');
    }

    /**
     * Remove the specified courseLieu from storage.
     * DELETE /courseLieus/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var courseLieu $courseLieu */
        $courseLieu = $this->courseLieuRepository->find($id);

        if (empty($courseLieu)) {
            return $this->sendError('Course Lieu not found');
        }

        $courseLieu->delete();

        return $this->sendSuccess('Course Lieu deleted successfully');
    }
}
