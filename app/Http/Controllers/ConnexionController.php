<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateConnexionRequest;
use App\Http\Requests\UpdateConnexionRequest;
use App\Repositories\ConnexionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ConnexionController extends AppBaseController
{
    /** @var  ConnexionRepository */
    private $connexionRepository;

    public function __construct(ConnexionRepository $connexionRepo)
    {
        $this->connexionRepository = $connexionRepo;
    }

    /**
     * Display a listing of the Connexion.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $connexions = $this->connexionRepository->all();

        return view('connexions.index')
            ->with('connexions', $connexions);
    }

    /**
     * Show the form for creating a new Connexion.
     *
     * @return Response
     */
    public function create()
    {
        return view('connexions.create');
    }

    /**
     * Store a newly created Connexion in storage.
     *
     * @param CreateConnexionRequest $request
     *
     * @return Response
     */
    public function store(CreateConnexionRequest $request)
    {
        $input = $request->all();

        $connexion = $this->connexionRepository->create($input);

        Flash::success('Connexion saved successfully.');

        return redirect(route('connexions.index'));
    }

    /**
     * Display the specified Connexion.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $connexion = $this->connexionRepository->find($id);

        if (empty($connexion)) {
            Flash::error('Connexion not found');

            return redirect(route('connexions.index'));
        }

        return view('connexions.show')->with('connexion', $connexion);
    }

    /**
     * Show the form for editing the specified Connexion.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $connexion = $this->connexionRepository->find($id);

        if (empty($connexion)) {
            Flash::error('Connexion not found');

            return redirect(route('connexions.index'));
        }

        return view('connexions.edit')->with('connexion', $connexion);
    }

    /**
     * Update the specified Connexion in storage.
     *
     * @param int $id
     * @param UpdateConnexionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateConnexionRequest $request)
    {
        $connexion = $this->connexionRepository->find($id);

        if (empty($connexion)) {
            Flash::error('Connexion not found');

            return redirect(route('connexions.index'));
        }

        $connexion = $this->connexionRepository->update($request->all(), $id);

        Flash::success('Connexion updated successfully.');

        return redirect(route('connexions.index'));
    }

    /**
     * Remove the specified Connexion from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $connexion = $this->connexionRepository->find($id);

        if (empty($connexion)) {
            Flash::error('Connexion not found');

            return redirect(route('connexions.index'));
        }

        $this->connexionRepository->delete($id);

        Flash::success('Connexion deleted successfully.');

        return redirect(route('connexions.index'));
    }
}
