<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatevilleRequest;
use App\Http\Requests\UpdatevilleRequest;
use App\Repositories\villeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class villeController extends AppBaseController
{
    /** @var  villeRepository */
    private $villeRepository;

    public function __construct(villeRepository $villeRepo)
    {
        $this->villeRepository = $villeRepo;
    }

    /**
     * Display a listing of the ville.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $villes = $this->villeRepository->all();

        return view('villes.index')
            ->with('villes', $villes);
    }

    /**
     * Show the form for creating a new ville.
     *
     * @return Response
     */
    public function create()
    {
        return view('villes.create');
    }

    /**
     * Store a newly created ville in storage.
     *
     * @param CreatevilleRequest $request
     *
     * @return Response
     */
    public function store(CreatevilleRequest $request)
    {
        $input = $request->all();

        $ville = $this->villeRepository->create($input);

        Flash::success('Ville saved successfully.');

        return redirect(route('villes.index'));
    }

    /**
     * Display the specified ville.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ville = $this->villeRepository->find($id);

        if (empty($ville)) {
            Flash::error('Ville not found');

            return redirect(route('villes.index'));
        }

        return view('villes.show')->with('ville', $ville);
    }

    /**
     * Show the form for editing the specified ville.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ville = $this->villeRepository->find($id);

        if (empty($ville)) {
            Flash::error('Ville not found');

            return redirect(route('villes.index'));
        }

        return view('villes.edit')->with('ville', $ville);
    }

    /**
     * Update the specified ville in storage.
     *
     * @param int $id
     * @param UpdatevilleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatevilleRequest $request)
    {
        $ville = $this->villeRepository->find($id);

        if (empty($ville)) {
            Flash::error('Ville not found');

            return redirect(route('villes.index'));
        }

        $ville = $this->villeRepository->update($request->all(), $id);

        Flash::success('Ville updated successfully.');

        return redirect(route('villes.index'));
    }

    /**
     * Remove the specified ville from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ville = $this->villeRepository->find($id);

        if (empty($ville)) {
            Flash::error('Ville not found');

            return redirect(route('villes.index'));
        }

        $this->villeRepository->delete($id);

        Flash::success('Ville deleted successfully.');

        return redirect(route('villes.index'));
    }
}
