<?php

namespace App\Http\Controllers;

use App\Color;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Category;
use App\Models\Partner;
use App\Models\Product;
use App\Models\Review;
use App\Models\ProductCategory;
use App\Repositories\ProductRepository;
use App\Utils\AdjUtils;
use Flash;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Imgur;
use Response;
use PDF;
use Symfony\Component\HttpFoundation\ParameterBag;

class ProductController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Product.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $products = Product::orderBy('id', 'desc')->paginate(5);

        return view('products.index')
            ->with('products', $products)
            /* ->with('colorData', $colorData) */;
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Category::all();
        $partners = Partner::all();
        $colors = Color::all();

        return view('products.create', compact('categories', 'partners', 'colors'));
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $input = $this->validate_and_save($request);
        $categories = $input['product_categories'];

        if(isset($input['colors'])){
            $input['colors'] = json_encode($input['colors']);
        }

        unset($input['product_categories']);

        $product = $this->productRepository->create($input);

        foreach ($categories as $categorie) {
            ProductCategory::create([
                'product_id' => $product->id,
                'category_id' => (int) $categorie,
            ]);
        }

        $productToUpdate = Product::find($product->id);

        $longUrl = "http://goldenhealth.adjemincloud.com/product_details/" . $product->id;
        $shortUrl = AdjUtils::createShortUrl($longUrl);
        $shortUrl = AdjUtils::BASE_URL . '/' . $shortUrl->get('short_url');
        $productToUpdate->link = $shortUrl;
        $productToUpdate->save();

        Flash::success('Product saved successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Display the specified Product.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $product = $this->productRepository->find($id);

        $colorData = Collection::make([]);
        $colors = new ParameterBag(json_decode($product->colors));
        foreach ($colors as $color) {
            if ($colors != null && count($colors) > 0) {
                $colorCode = Color::where(['id' => $color])->first();
                $colorData = $colorData->push($colorCode);
            }
        }

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        // dd($colorData);

        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $categories = Category::all();
        $partners = Partner::all();
        $colors = Color::all();

        return view('products.edit')
            ->with('categories', $categories)
            ->with('partners', $partners)
            ->with('colors', $colors)
            ->with('product', $product);
    }

    /**
     * Update the specified Product in storage.
     *
     * @param int $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $input = $this->validate_and_save($request);

        $prod_categories = $input['product_categories'];

        $input['colors'] = json_encode($input['colors']);
        if ($input['medias'] == '') {
            unset($input['medias']);
        }
        unset($input['product_categories']);

        $product = $this->productRepository->update($input, $id);

        foreach ($product->categories as $categorie) {
            $categorie->delete();
        }

        foreach ($prod_categories as $categorie) {
            ProductCategory::create([
                'product_id' => $product->id,
                'category_id' => (int) $categorie,
            ]);
        }

        Flash::success('Product updated successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $this->productRepository->delete($id);

        Flash::success('Product deleted successfully.');

        return redirect(route('products.index'));
    }

    public function validate_and_save(request $request)
    {
        // dd($request->all());
        $input = $request->all();

        $this->validate($request, [
            'title' => 'required',
            'product_categories' => 'required',
            'medias'    =>  'nullable',
            'medias.*'  =>  'nullable|image|max:2593752',
            'colors'    =>  'required',
            'sizes' =>  'required'
        ]);

        if ($request->hasFile('medias')) {

            // if ($image != null) {
            //     $productImage = Imgur::upload($image);
            //     $productImageLink = $productImage->link();
            // }
            $photos = $request->file('medias');
            $image_urls = [];
            if ($photos != null) {
                for ($i = 0; $i < sizeof($photos); $i++) {
                    ${'image' . ($i)} = Imgur::upload($photos[$i]) ?? null;
                    ${'picture' . ($i)} = ${'image' . ($i)}->link() ?? null;
                    array_push($image_urls, ${'picture' . ($i)});
                }
            }

            // dd($image_urls);
    
            $productImageLink = json_encode($image_urls);
        } else {
            $productImageLink = '';
        }

        $input["medias"] = $productImageLink;
        // $input['fees']  = 0;

        $totalPrice = (double) $input["original_price"] + (double) $input["fees"];

        $input["slug"] = $input["slug"] ?? \Str::slug($input['title'], '-');
        $input["price"] = $totalPrice;
        $input["stock"] = $input['initial_count'] ?? null;
        $input['old_price'] = $input['has_promo'] == 1 ? ($input["original_price"] * $input['promo_percentage']) / 100 : 0;
        $input['published_at'] = $input['is_published'] == 1 ? \Carbon\Carbon::now() : null;
        $input['description_metadata'] = json_encode($input['description']);

        return $input;
    }

    public function stock($id, Request $request){
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $product->stock = (int) $product->stock + (int) $request->stock;
        $product->save();

        Flash::success('Product updated successfully.');

        return redirect(route('products.index'));
    }

    public function searchProduct(Request $request){
        $productsFound = Product::where('title', 'like', '%' . $request->searchProduct . '%')
            ->orWhere('description', 'like', '%' . $request->searchProduct . '%')
            ->orWhere('description_metadata', 'like', '%' . $request->searchProduct . '%')
            ->orWhere('price', 'like', '%' . $request->searchProduct . '%')
            ->get();
        
        return json_encode(view('products.table', ['products' => $productsFound])->render());
    }

    public function printAll(){
        $products = Product::all()->sortByDesc('id');
        return view('products.table_pdf', compact('products'));
        // $pdf = PDF::loadView('products.table_pdf', compact('products'));
        // return $pdf->stream();
    }
}
