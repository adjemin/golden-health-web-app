<?php

namespace App\Http\Controllers;

use App\Models\Coach;
use App\Models\Customer;
use App\Models\Discipline;
use App\Models\Event;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Notifications\SendMailNewLetter;
use App\Mail\NewLetter;
use Illuminate\Support\Facades\Mail;

use Flash;
use Imgur;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coach = Coach::all();
        $countCoach = count($coach);

        $customers = Customer::where('type_account', 'challenger')->orWhereNull('type_account')->get();
        $countCustomers = count($customers);

        $disciplines = Discipline::all();
        $countDisciplines = count($disciplines);

        $events = Event::all();
        $countEvents = count($events);

        $orders = Order::orderBy('id', 'desc')->paginate(10);

        $data = \DB::table('customers')
            ->select(\DB::raw('COUNT(id) compte'), \DB::raw('DATE_FORMAT(created_at,"%m/%Y") periode'))
            ->groupBy('periode')->get();

        $months = collect($data)->map(function($item){
            return $item->periode;
        });

        $amountData = collect($data)->map(function($item){
            return $item->compte;
        });


        return view('home', compact('orders', 'countCoach', 'countCustomers', 'countDisciplines', 'countEvents', 'amountData', 'months'));
    }
    // comments
    public function sendMail(Request $request){
        return view('home_sendmail');
    }

    public function sendMailPost(Request $request){
        $customers =  \App\Customer::where('is_newsletter', 1)->get();

        $details = [
            'subject' => $request->subject,
            'greeting' => $request->greeting,
            'body' => $request->body,
            'thanks' => $request->thanks
        ];

        if(!is_null($request->action_url)){
            $details['action'] = [
                'message' => $request->action_message,
                'url' => $request->action_url
            ];
        }else if(is_null($request->action_url) && !is_null($request->action_message)){
            Flash::error('Action url non definie');
            return redirect()->back()->withInput();
        }


        if ($request->hasFile('medias')) {
            $photos = $request->file('medias');
            $image_urls = [];
            if ($photos != null) {
                for ($i = 0; $i < sizeof($photos); $i++) {
                    ${'image' . ($i)} = Imgur::upload($photos[$i]) ?? null;
                    ${'picture' . ($i)} = ${'image' . ($i)}->link() ?? null;
                    array_push($image_urls, ${'picture' . ($i)});
                }
            }

            $details['attachments'] = $image_urls;
        }else{
            $details['attachments'] = [];
        }
        foreach($customers->chunk(5) as $chunkcustomers){
            foreach($chunkcustomers as $user){
                if(filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
                    //$user->notify(new SendMailNewLetter($details));
                    Mail::to($user->email)->send(new NewLetter($details));
                }
            }
        }

        Flash::success('Mails envoyés à tous les utilisateurs ayant souscris à la newsletter');
        return redirect(route('sendMail'));
    }
}
