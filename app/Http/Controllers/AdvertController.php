<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAdvertRequest;
use App\Http\Requests\UpdateAdvertRequest;
use App\Repositories\AdvertRepository;
use App\Models\Partner;
use App\Models\Advert;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Imgur;
use Response;

class AdvertController extends AppBaseController
{
    /** @var  AdvertRepository */
    private $advertRepository;

    public function __construct(AdvertRepository $advertRepo)
    {
        $this->advertRepository = $advertRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Advert.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $adverts = Advert::orderBy('id', 'desc')->paginate(10);

        return view('adverts.index')
            ->with('adverts', $adverts);
    }

    /**
     * Show the form for creating a new Advert.
     *
     * @return Response
     */
    public function create()
    {
        $partners = Partner::all();
        return view('adverts.create', compact('partners'));
    }

    /**
     * Store a newly created Advert in storage.
     *
     * @param CreateAdvertRequest $request
     *
     * @return Response
     */
    public function store(CreateAdvertRequest $request)
    {
        $input = $request->all();

        $productImageLink = null;

        if ($request->file('cover_url')) {
            $validator = \Validator::make($request->all(), [
               'cover_url'  => 'image'
            ]); 
            
            if($validator->fails()){
                $video_validator = \Validator::make($request->all(), [
                    'cover_url'  =>  'mimes:mp4,mov,ogg,flv,m3u8,ts,3gp,mov,avi,wmv,qt,ogx,oga,ogv,webm|max:20000'
                ]);

                
                if($video_validator->fails()){
                    Flash::error('Veuillez uploader une vidéo ou un fichier image.');
                    return redirect()->back()->withInput();
                }

                $filename = time() .'.'. $request->file('cover_url')->getClientOriginalExtension();
                $productImageLink = $request->file('cover_url')->move('storage/', $filename);

                
                $input['file_type'] = 'video';
            }else{
                $image = $request->file('cover_url');
                $pictures = [];
                if ($image != null) {    
                    $productImage = Imgur::upload($image);
                    $productImageLink = $productImage->link();
                }
                
                $input['file_type'] = 'image';
            }
        } else {
            $productImageLink = "";
        }

        $input['cover_url'] = $productImageLink;

        $advert = $this->advertRepository->create($input);

        Flash::success('Advert saved successfully.');

        return redirect(route('adverts.index'));
    }

    /**
     * Display the specified Advert.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $advert = $this->advertRepository->find($id);

        if (empty($advert)) {
            Flash::error('Advert not found');

            return redirect(route('adverts.index'));
        }

        return view('adverts.show')->with('advert', $advert);
    }

    /**
     * Show the form for editing the specified Advert.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $advert = $this->advertRepository->find($id);

        if (empty($advert)) {
            Flash::error('Advert not found');

            return redirect(route('adverts.index'));
        }

        $partners = Partner::all();
        return view('adverts.edit')
            ->with('partners', $partners)
            ->with('advert', $advert);
    }

    /**
     * Update the specified Advert in storage.
     *
     * @param int $id
     * @param UpdateAdvertRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAdvertRequest $request)
    {
        $advert = $this->advertRepository->find($id);

        if (empty($advert)) {
            Flash::error('Advert not found');

            return redirect(route('adverts.index'));
        }

        $productImageLink = null;

        if ($request->file('cover_url')) {
            $validator = \Validator::make($request->all(), [
               'cover_url'  => 'image'
            ]); 
            
            if($validator->fails()){
                $video_validator = \Validator::make($request->all(), [
                    'cover_url'  =>  'mimes:mp4,mov,ogg,flv,m3u8,ts,3gp,mov,avi,wmv,qt,ogx,oga,ogv,webm|max:20000'
                ]);

                
                if($video_validator->fails()){
                    Flash::error('Veuillez uploader une vidéo ou un fichier.');
                    return redirect()->back()->withInput();
                }

                $filename = time() . '.' . $file->getClientOriginalExtension();
                $productImageLink = $request->file('cover_url')->move('storage/', $filename);

                
                $input['file_type'] = 'video';
            }else{
                $image = $request->file('cover_url');
                $pictures = [];
                if ($image != null) {    
                    $productImage = Imgur::upload($image);
                    $productImageLink = $productImage->link();
                }
                
                $input['file_type'] = 'image';
            }
        } else {
            $productImageLink = $advert->cover_url;
        }

        $input['cover_url'] = $productImageLink;

        $advert = $this->advertRepository->update($input, $id);

        Flash::success('Advert updated successfully.');

        return redirect(route('adverts.index'));
    }

    /**
     * Remove the specified Advert from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $advert = $this->advertRepository->find($id);

        if (empty($advert)) {
            Flash::error('Advert not found');

            return redirect(route('adverts.index'));
        }

        $this->advertRepository->delete($id);

        Flash::success('Advert deleted successfully.');

        return redirect(route('adverts.index'));
    }
}
