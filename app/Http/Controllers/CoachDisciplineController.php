<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCoachDisciplineRequest;
use App\Http\Requests\UpdateCoachDisciplineRequest;
use App\Repositories\CoachDisciplineRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CoachDisciplineController extends AppBaseController
{
    /** @var  CoachDisciplineRepository */
    private $coachDisciplineRepository;

    public function __construct(CoachDisciplineRepository $coachDisciplineRepo)
    {
        $this->coachDisciplineRepository = $coachDisciplineRepo;
    }

    /**
     * Display a listing of the CoachDiscipline.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $coachDisciplines = $this->coachDisciplineRepository->all();

        return view('coach_disciplines.index')
            ->with('coachDisciplines', $coachDisciplines);
    }

    /**
     * Show the form for creating a new CoachDiscipline.
     *
     * @return Response
     */
    public function create()
    {
        return view('coach_disciplines.create');
    }

    /**
     * Store a newly created CoachDiscipline in storage.
     *
     * @param CreateCoachDisciplineRequest $request
     *
     * @return Response
     */
    public function store(CreateCoachDisciplineRequest $request)
    {
        $input = $request->all();

        $coachDiscipline = $this->coachDisciplineRepository->create($input);

        Flash::success('Coach Discipline saved successfully.');

        return redirect(route('coachDisciplines.index'));
    }

    /**
     * Display the specified CoachDiscipline.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $coachDiscipline = $this->coachDisciplineRepository->find($id);

        if (empty($coachDiscipline)) {
            Flash::error('Coach Discipline not found');

            return redirect(route('coachDisciplines.index'));
        }

        return view('coach_disciplines.show')->with('coachDiscipline', $coachDiscipline);
    }

    /**
     * Show the form for editing the specified CoachDiscipline.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $coachDiscipline = $this->coachDisciplineRepository->find($id);

        if (empty($coachDiscipline)) {
            Flash::error('Coach Discipline not found');

            return redirect(route('coachDisciplines.index'));
        }

        return view('coach_disciplines.edit')->with('coachDiscipline', $coachDiscipline);
    }

    /**
     * Update the specified CoachDiscipline in storage.
     *
     * @param int $id
     * @param UpdateCoachDisciplineRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCoachDisciplineRequest $request)
    {
        $coachDiscipline = $this->coachDisciplineRepository->find($id);

        if (empty($coachDiscipline)) {
            Flash::error('Coach Discipline not found');

            return redirect(route('coachDisciplines.index'));
        }

        $coachDiscipline = $this->coachDisciplineRepository->update($request->all(), $id);

        Flash::success('Coach Discipline updated successfully.');

        return redirect(route('coachDisciplines.index'));
    }

    /**
     * Remove the specified CoachDiscipline from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $coachDiscipline = $this->coachDisciplineRepository->find($id);

        if (empty($coachDiscipline)) {
            Flash::error('Coach Discipline not found');

            return redirect(route('coachDisciplines.index'));
        }

        $this->coachDisciplineRepository->delete($id);

        Flash::success('Coach Discipline deleted successfully.');

        return redirect(route('coachDisciplines.index'));
    }
}
