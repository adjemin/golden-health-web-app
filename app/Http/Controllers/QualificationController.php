<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateQualificationRequest;
use App\Http\Requests\UpdateQualificationRequest;
use App\Repositories\QualificationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class QualificationController extends AppBaseController
{
    /** @var  QualificationRepository */
    private $qualificationRepository;

    public function __construct(QualificationRepository $qualificationRepo)
    {
        $this->qualificationRepository = $qualificationRepo;
    }

    /**
     * Display a listing of the Qualification.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $qualifications = $this->qualificationRepository->all();

        return view('qualifications.index')
            ->with('qualifications', $qualifications);
    }

    /**
     * Show the form for creating a new Qualification.
     *
     * @return Response
     */
    public function create()
    {
        return view('qualifications.create');
    }

    /**
     * Store a newly created Qualification in storage.
     *
     * @param CreateQualificationRequest $request
     *
     * @return Response
     */
    public function store(CreateQualificationRequest $request)
    {
        $input = $request->all();

        $qualification = $this->qualificationRepository->create($input);

        Flash::success('Qualification saved successfully.');

        return redirect(route('qualifications.index'));
    }

    /**
     * Display the specified Qualification.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $qualification = $this->qualificationRepository->find($id);

        if (empty($qualification)) {
            Flash::error('Qualification not found');

            return redirect(route('qualifications.index'));
        }

        return view('qualifications.show')->with('qualification', $qualification);
    }

    /**
     * Show the form for editing the specified Qualification.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $qualification = $this->qualificationRepository->find($id);

        if (empty($qualification)) {
            Flash::error('Qualification not found');

            return redirect(route('qualifications.index'));
        }

        return view('qualifications.edit')->with('qualification', $qualification);
    }

    /**
     * Update the specified Qualification in storage.
     *
     * @param int $id
     * @param UpdateQualificationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQualificationRequest $request)
    {
        $qualification = $this->qualificationRepository->find($id);

        if (empty($qualification)) {
            Flash::error('Qualification not found');

            return redirect(route('qualifications.index'));
        }

        $qualification = $this->qualificationRepository->update($request->all(), $id);

        Flash::success('Qualification updated successfully.');

        return redirect(route('qualifications.index'));
    }

    /**
     * Remove the specified Qualification from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $qualification = $this->qualificationRepository->find($id);

        if (empty($qualification)) {
            Flash::error('Qualification not found');

            return redirect(route('qualifications.index'));
        }

        $this->qualificationRepository->delete($id);

        Flash::success('Qualification deleted successfully.');

        return redirect(route('qualifications.index'));
    }
}
