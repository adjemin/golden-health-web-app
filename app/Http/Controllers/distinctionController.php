<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatedistinctionRequest;
use App\Http\Requests\UpdatedistinctionRequest;
use App\Repositories\distinctionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class distinctionController extends AppBaseController
{
    /** @var  distinctionRepository */
    private $distinctionRepository;

    public function __construct(distinctionRepository $distinctionRepo)
    {
        $this->distinctionRepository = $distinctionRepo;
    }

    /**
     * Display a listing of the distinction.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $distinctions = $this->distinctionRepository->all();

        return view('distinctions.index')
            ->with('distinctions', $distinctions);
    }

    /**
     * Show the form for creating a new distinction.
     *
     * @return Response
     */
    public function create()
    {
        return view('distinctions.create');
    }

    /**
     * Store a newly created distinction in storage.
     *
     * @param CreatedistinctionRequest $request
     *
     * @return Response
     */
    public function store(CreatedistinctionRequest $request)
    {
        $input = $request->all();

        $distinction = $this->distinctionRepository->create($input);

        Flash::success('Distinction saved successfully.');

        return redirect(route('distinctions.index'));
    }

    /**
     * Display the specified distinction.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $distinction = $this->distinctionRepository->find($id);

        if (empty($distinction)) {
            Flash::error('Distinction not found');

            return redirect(route('distinctions.index'));
        }

        return view('distinctions.show')->with('distinction', $distinction);
    }

    /**
     * Show the form for editing the specified distinction.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $distinction = $this->distinctionRepository->find($id);

        if (empty($distinction)) {
            Flash::error('Distinction not found');

            return redirect(route('distinctions.index'));
        }

        return view('distinctions.edit')->with('distinction', $distinction);
    }

    /**
     * Update the specified distinction in storage.
     *
     * @param int $id
     * @param UpdatedistinctionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatedistinctionRequest $request)
    {
        $distinction = $this->distinctionRepository->find($id);

        if (empty($distinction)) {
            Flash::error('Distinction not found');

            return redirect(route('distinctions.index'));
        }

        $distinction = $this->distinctionRepository->update($request->all(), $id);

        Flash::success('Distinction updated successfully.');

        return redirect(route('distinctions.index'));
    }

    /**
     * Remove the specified distinction from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $distinction = $this->distinctionRepository->find($id);

        if (empty($distinction)) {
            Flash::error('Distinction not found');

            return redirect(route('distinctions.index'));
        }

        $this->distinctionRepository->delete($id);

        Flash::success('Distinction deleted successfully.');

        return redirect(route('distinctions.index'));
    }
}
