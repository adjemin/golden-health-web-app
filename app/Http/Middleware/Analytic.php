<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\NavigatorSession;
class Analytic
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->exists('navigatorSession')){
            $navigator = $request->session()->get('navigatorSession');
            $pages = json_decode($navigator->page_visited, true);
            if(url()->previous() != $request->fullUrl()){
                $isset = false;
                $page_url = collect($pages)->map(function($page){
                    return $page['page'];
                })->toArray();
                // dd($page_url);
                
                //set the end time of the previous page
                $previousIndex = array_search(url()->previous(), $page_url) ?? null;
                
                if(!is_null($previousIndex)){
                    $pages[$previousIndex]['time'][count($pages[$previousIndex]['time']) -1]['end'] = now();
                }
                
                if(!in_array($request->fullUrl(), $page_url)){
                    array_push($page_url, $request->fullUrl());
                    array_push($pages, ["page"  =>  $request->fullUrl(),'time' => [['start' => now(),'end'   => null]]]);
                }else{
                    $toUpdate = array_search($request->fullUrl(), $page_url) ?? null;
                    array_push($pages[$toUpdate]['time'], ['start' => now(),'end'   => null]);
                }
            }
            // dd($pages);

            $is_visitor = (auth()->guard('customer')->check() == false && auth()->check()  == false) ? true : false;
            $is_admin = auth()->check() ? true : false;
            if(!$is_visitor && !$is_admin){
                if(isset(auth()->guard('customer')->user()->is_coach)){
                    $is_coach = true;
                }else{
                    $is_challenger = true;
                }
            }
            
            $provider = json_decode($navigator->provider_connection, true);
            
            if(empty($provider) || is_null($provider)){
                if(isset($is_admin)){
                    if($is_admin)
                    array_push($provider, ['table' =>  'User','id'    =>  auth()->user()->id]);
                }

                if(isset($is_coach)){
                    if($is_coach)
                    array_push($provider, ['table' =>  'Coach','id'    =>  auth()->guard('customer')->user()->is_coach->id]);
                }

                if(isset($is_challenger)){
                    if($is_challenger)
                    array_push($provider, ['table' =>  'Customer','id'    =>  auth()->guard('customer')->user()->id]);
                }
            }else{
                
                $provider_name = collect($provider)->map(function($item){
                    return $item['table'];
                })->toArray();

                if(isset($is_admin) && !in_array('User', $provider_name)){
                    if($is_admin)
                    array_push($provider, ['table' =>  'User','id'    =>  auth()->user()->id]);
                }

                if(isset($is_coach) && !in_array('Coach', $provider_name)){
                    if($is_coach)
                    array_push($provider, ['table' =>  'Coach','id'    =>  auth()->guard('customer')->user()->is_coach->id]);
                }
                
                if(isset($is_challenger) && !in_array('Customer', $provider_name)){
                    if($is_coach)
                    array_push($provider, ['table' =>  'Customer','id'    =>  auth()->guard('customer')->user()->id]);
                }
            }

            $navigator->update([
                'page_visited'  =>  json_encode($pages) ?? $navigator->page_visited,
                'is_admin'  =>  $is_admin,
                'is_challenger' =>  $is_challenger ?? false,
                'is_coach'  =>  $is_coach ?? false,
                'is_visitor'    =>  $is_visitor,
                'provider_connection'   =>  json_encode($provider) ?? $navigator->provider_connection
            ]);

            // session(['navigatorSession' => $navigator]);
            $request->session()->put('navigatorSession', $navigator);
        }else{
            $is_visitor = (auth()->guard('customer')->check() == false && auth()->check()  == false) ? true : false;
            $is_admin = auth()->check() ? true : false;
            $provider = [];
            
            if($is_admin){
                $provider = [
                    [
                        'table' =>  'User',
                        'id'    =>  auth()->user()->id
                    ]
                ];
            }

            if(!$is_visitor && !$is_admin){
                if(isset(auth()->guard('customer')->user()->is_coach)){
                    $is_coach = true;
                    $provider = [
                        [
                            'table' =>  'Coach',
                            'id'    =>  auth()->guard('customer')->user()->is_coach->id
                        ]
                    ];
                }else {
                    $is_challenger = true;
                    $provider = [
                        [
                            'table' =>  'Customer',
                            'id'    =>  auth()->guard('customer')->user()->id
                        ]
                    ];
                }
            }

            $page_visited = [
                [
                    "page"  =>  $request->fullUrl(),
                    'time' => [
                        [
                            'start' => now(),
                            'end'   => null
                        ]
                    ]
                ]
            ];

            $session = NavigatorSession::create([
                'uniqid'    => uniqid(),
                'ip'    =>  $_SERVER['HTTP_CLIENT_IP'] ?? $_SERVER['HTTP_X_FORWARDED_FOR'] ?? $_SERVER['HTTP_X_FORWARDED'] ?? $_SERVER['HTTP_FORWARDED_FOR'] ?? $_SERVER['HTTP_FORWARDED'] ?? $_SERVER['REMOTE_ADDR'] ?? $request->ip(),
                'browser'    =>  (count($this->getBrowserAndOs()) == 8 ? 'Firefox' : (count($this->getBrowserAndOs()) == 11 ? 'Chrome' : (count($this->getBrowserAndOs()) == 12 ? 'Microsoft Edge' : 'Opera'))),
                'platform'    =>  $this->getBrowserAndOs()[2],
                'mac'   =>  $this->getMac(),
                'page_visited'  =>  json_encode($page_visited),
                'is_admin'  =>  $is_admin,
                'is_challenger' =>  $is_challenger ?? false,
                'is_coach'  =>  $is_coach ?? false,
                'is_visitor'    =>  $is_visitor,
                'provider_connection'   =>  json_encode($provider)
            ]);

            $request->session()->put('navigatorSession', $session);
            // session(['navigatorSession' => $session]);
        }

        return $next($request);
    }

    private function getMAC(){
        ob_start();
        system('getmac');
        $content = ob_get_contents();
        ob_clean();
        return substr($content, strpos($content,"\\")-20, 17);
    }

    public function getBrowserAndOs(){
        $pattern = "/[\/();,]/i";
        return preg_split($pattern, $_SERVER['HTTP_USER_AGENT']);
    }
}
