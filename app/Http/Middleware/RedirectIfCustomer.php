<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfCustomer
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'customer')
	{
	    if (Auth::guard($guard)->check()) {
				if ($request->has('action')) {
					if($request->get("action") == "checkout")
						return redirect("/final/checkout");
				} else {
					return redirect('/customer/dashboard');
				}
	    }
	    return $next($request);
	}
}
