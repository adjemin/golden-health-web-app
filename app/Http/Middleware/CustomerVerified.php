<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CustomerVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $customer = Auth::guard('customer')->user();
        
        // if(!empty($customer)){
        //     if(is_null($customer->email_verifed_at)){
        //         return view('customer_frontOffice.go_to_active_your_account');
        //     }
        // }else{
        //     return redirect()->back()->withInput();
        // }
        
        return $next($request);
    }
}
