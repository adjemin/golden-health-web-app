<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
    //
    protected $guarded = [];

    public function items()
    {
        return $this->hasMany(CartItem::class);
    }
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function getTotal()
    {
        return $this->getSousTotal() + $this->getDeliveryFeeTotal();
    }

    public function getSousTotal()
    {
        $total = 0;
        foreach($this->items as $i){
            $total+= $i->amount;
        }
        return $total;
    }

    public function getDeliveryFeeTotal()
    {
        $total = 0;
        foreach($this->items as $i){
            $total+= (int) $i->product->fees;
        }
        return $total;
    }

    public static function ShoppingCart()
    {
        $customer = Auth::guard('customer')->user();
        $shopping_key = session()->get('shopping_key');

        if(!$customer){
            if(!$shopping_key){
                $shopping_key = session()->put('shopping_key', Carbon::now()->toIso8601String());
            }
            $shoppingCart = ShoppingCart::where('key', $shopping_key)->first();

            if(!$shoppingCart){
                $shoppingCart = new ShoppingCart();
                $shoppingCart->key = $shopping_key;
                $shoppingCart->save();
            }
        }

        if($customer){
            if(!$shopping_key){
                $shopping_key = session()->put('shopping_key', $customer->id);
                //
            }

            $shoppingCart = ShoppingCart::where('key', $shopping_key)->first();
            if($shoppingCart){
                if($shoppingCart->customer_id == null){
                    $shoppingCart->customer_id = $customer->id;
                    $shoppingCart->save();
                }
                return $shoppingCart;
            }

            $shoppingCart = $customer->shoppingCart;
            if(!$shoppingCart){
                $shoppingCart = new ShoppingCart();
                $shoppingCart->key = $customer->id;
                $shoppingCart->customer_id = $customer->id;
                $shoppingCart->save();
            }
        }
        return $shoppingCart;
    }

    public static function Clear()
    {
        $shoppingCart = ShoppingCart::ShoppingCart();
        foreach($shoppingCart->items as $i){
            $i->delete();
        }
        return true;
    }
    public static function Remove($id)
    {
        $shoppingCart = ShoppingCart::ShoppingCart();
        $i = $shoppingCart->items->find($id);
        return $i ? $i->delete() : false;
    }

    public static function DecrementItem($id)
    {
        $shoppingCart = ShoppingCart::ShoppingCart();
        $i = $shoppingCart->items->find($id);
        if($i->quantity <= 1){
            $i->delete();
            return $i;
        }
        $i->quantity -= 1;
        $i->amount = $i->quantity * $i->product->getPrice();
        $i->save();

        return $i;
    }
    public static function IncrementItem($id)
    {
        $shoppingCart = ShoppingCart::ShoppingCart();
        $i = $shoppingCart->items->find($id);
        $i->quantity += 1;
        $i->amount = $i->quantity * $i->product->getPrice();
        $i->save();
        return $i;
    }
}
