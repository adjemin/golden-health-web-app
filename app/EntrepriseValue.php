<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntrepriseValue extends Model
{
    //
    protected $guarded = [];

    public function entreprise()
    {
        return $this->belongsTo(Entreprise::class);
    }
}