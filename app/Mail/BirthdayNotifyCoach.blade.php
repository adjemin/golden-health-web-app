<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BirthdayNotifyCoach extends Mailable
{
    use Queueable, SerializesModels;
    // public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->from("yemakarios70@gmail.com", "Goldenhealth")
          ->subject("🎉🎉🎉🎉🎉🎉 Date d'anniversaire d'inscription Coach 🎊🎊🎊🎊🎊")
          ->view('emails.notify.birthday_coach');
    }
}
