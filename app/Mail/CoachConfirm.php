<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CoachConfirm extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     public function __construct()
     {
         //
     }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->from("yemakarios70@gmail.com", "Goldenhealth")
          ->subject("Confirmation de votre compte")
          ->view('emails.coach.confirm_status');
    }
}
