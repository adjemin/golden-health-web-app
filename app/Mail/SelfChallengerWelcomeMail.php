<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Customer;

class SelfChallengerWelcomeMail extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * The order instance.
     *
     * @var Customer
     */
    public $customer;

    public $action_link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Customer $customer, string $link)
    {
        //
        $this->customer = $customer;
        $this->action_link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from("yemakarios70@gmail.com", "Goldenhealth")
            ->subject("Confirmation d’inscription")
            ->view('emails.self_challenger.confirm');
    }
}
