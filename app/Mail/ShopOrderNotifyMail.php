<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ShopOrderNotifyMail extends Mailable
{
    use Queueable, SerializesModels;

    public $action_url;
    public $order;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($action_url, $order)
    {
        //
        $this->action_url = $action_url;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from("yemakarios70@gmail.com", "Goldenhealth")
            ->subject("Votre commande a été validée")
            ->view('emails.shop.notify')
            ->with([
                'action_url', $this->action_url,
                'order', $this->order,
            ]);
    }
}
