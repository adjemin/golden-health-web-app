<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Customer;

class NewLetter extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * The order instance.
     *
     * @var Customer
     */
    public $customer;

    public $action_link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        //
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $message = $this->from("yemakarios70@gmail.com", "Goldenhealth")
            ->subject($this->data['subject'])
            ->view('emails.newsletter.notify')->with([
                'data' =>  $this->data
            ]);
        foreach($this->data['attachments'] as $attachment)
        {
            $message->attach($attachment);
        }
        return $message;
    }
}
