<?php

namespace App\Repositories;

use App\Models\InvoicePayment;
use App\Repositories\BaseRepository;

/**
 * Class InvoicePaymentRepository
 * @package App\Repositories
 * @version July 30, 2020, 5:32 am UTC
*/

class InvoicePaymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'invoice_id',
        'payment_method',
        'payment_reference',
        'amount',
        'currency_code',
        'creator_id',
        'creator_name',
        'creator_name',
        'creator',
        'status',
        'status',
        'is_waiting',
        'is_completed',
        'payment_gateway_trans_id',
        'payment_gateway_custom',
        'payment_gateway_currency',
        'payment_gateway_amount',
        'payment_gateway_payid',
        'payment_gateway_payment_date',
        'payment_gateway_payment_time',
        'payment_gateway_payment_message',
        'payment_gateway_payment_method',
        'payment_gateway_payment_phone_prefixe',
        'payment_gateway_payment_cel_phone_num',
        'payment_gateway_ipn_ack',
        'payment_gateway_created_at',
        'payment_gateway_updated_at',
        'payment_gateway_cpm_result',
        'payment_gateway_trans_status',
        'payment_gateway_designation',
        'payment_gateway_buyer_name',
        'payment_gateway_signature'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InvoicePayment::class;
    }
}