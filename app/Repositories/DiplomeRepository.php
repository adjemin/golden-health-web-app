<?php

namespace App\Repositories;

use App\Models\Diplome;
use App\Repositories\BaseRepository;

/**
 * Class DiplomeRepository
 * @package App\Repositories
 * @version October 20, 2020, 4:07 pm UTC
*/

class DiplomeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'diplome_name',
        'name',
        'image_url',
        'coach_id',
        'course_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Diplome::class;
    }
}
