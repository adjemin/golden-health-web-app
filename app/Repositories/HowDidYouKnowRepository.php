<?php

namespace App\Repositories;

use App\Models\HowDidYouKnow;
use App\Repositories\BaseRepository;

/**
 * Class HowDidYouKnowRepository
 * @package App\Repositories
 * @version August 18, 2020, 10:24 am UTC
*/

class HowDidYouKnowRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'name_en',
        'user_source',
        'user_id',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return HowDidYouKnow::class;
    }
}
