<?php

namespace App\Repositories;

use App\Models\CoachCaracteristique;
use App\Repositories\BaseRepository;

/**
 * Class CoachCaracteristiqueRepository
 * @package App\Repositories
 * @version September 2, 2020, 4:29 pm UTC
*/

class CoachCaracteristiqueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'image',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CoachCaracteristique::class;
    }
}
