<?php

namespace App\Repositories;

use App\Models\Videotheque;
use App\Repositories\BaseRepository;

/**
 * Class VideothequeRepository
 * @package App\Repositories
 * @version October 19, 2020, 12:32 pm UTC
*/

class VideothequeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titre',
        'description',
        'link_youtube',
        'video'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Videotheque::class;
    }
}
