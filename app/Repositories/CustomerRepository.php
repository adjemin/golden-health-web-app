<?php

namespace App\Repositories;

use App\Customer;
use App\Repositories\BaseRepository;

/**
 * Class CustomerRepository
 * @package App\Repositories
 * @version July 29, 2020, 9:31 am UTC
*/

class CustomerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'last_name',
        'first_name',
        'name',
        'dial_code',
        'phone_number',
        'phone',
        'is_phone_verifed',
        'phone_verifed_at',
        'is_active',
        'activation_date',
        'email',
        'photo_url',
        'gender',
        'size',
        'weight',
        'bio',
        'birthday',
        'birth_location',
        'language',
        'location_address',
        'location_latitude',
        'location_longitude',
        'location_gps',
        'password'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Customer::class;
    }
}
