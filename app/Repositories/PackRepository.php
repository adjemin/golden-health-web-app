<?php

namespace App\Repositories;

use App\Models\Pack;
use App\Repositories\BaseRepository;

/**
 * Class PackRepository
 * @package App\Repositories
 * @version July 29, 2020, 6:20 pm UTC
*/

class PackRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'name',
        'description',
        'percentage_discount'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pack::class;
    }
}
