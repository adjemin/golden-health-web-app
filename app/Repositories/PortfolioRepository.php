<?php

namespace App\Repositories;

use App\Models\Portfolio;
use App\Repositories\BaseRepository;

/**
 * Class PortfolioRepository
 * @package App\Repositories
 * @version July 29, 2020, 6:31 pm UTC
*/

class PortfolioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'coach_id',
        'title',
        'images_url',
        'video_url'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Portfolio::class;
    }
}
