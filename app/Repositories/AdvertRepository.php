<?php

namespace App\Repositories;

use App\Models\Advert;
use App\Repositories\BaseRepository;

/**
 * Class AdvertRepository
 * @package App\Repositories
 * @version October 12, 2020, 5:09 pm UTC
*/

class AdvertRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'cover_url',
        'description',
        'redirect_to',
        'priority',
        'partner_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Advert::class;
    }
}
