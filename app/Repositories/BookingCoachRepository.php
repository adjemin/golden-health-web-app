<?php

namespace App\Repositories;

use App\Models\BookingCoach;
use App\Repositories\BaseRepository;

/**
 * Class BookingCoachRepository
 * @package App\Repositories
 * @version October 6, 2020, 2:01 am UTC
*/

class BookingCoachRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'course_id',
        'customer_id',
        'pack_id',
        'nb_personne',
        'amount',
        'booking_ref',
        'course_lieu_id',
        'course_lieu_note',
        'address_name',
        'address_lat',
        'address_lon',
        'quantity',
        'coach_confirm',
        'status',
        'status_name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BookingCoach::class;
    }
}
