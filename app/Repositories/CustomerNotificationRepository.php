<?php

namespace App\Repositories;

use App\Models\CustomerNotification;
use App\Repositories\BaseRepository;

/**
 * Class CustomerNotificationRepository
 * @package App\Repositories
 * @version October 20, 2020, 6:50 pm UTC
*/

class CustomerNotificationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'title_en',
        'subtitle',
        'subtitle_en',
        'action',
        'action_by',
        'meta_data',
        'type_notification',
        'is_read',
        'is_received',
        'data',
        'user_id',
        'data_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomerNotification::class;
    }
}
