<?php

namespace App\Repositories;

use App\Models\CourseDetail;
use App\Repositories\BaseRepository;

/**
 * Class courseDetailsRepository
 * @package App\Repositories
 * @version October 5, 2020, 8:32 pm UTC
*/

class courseDetailsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'course_id',
        'experience',
        'seance_type',
        'is_diplome',
        'diplome_id',
        'nb_person',
        'is_formation',
        'has_particulier',
        'has_collectif'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CourseDetail::class;
    }
}
