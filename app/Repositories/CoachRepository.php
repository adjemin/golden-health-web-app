<?php

namespace App\Repositories;

use App\Models\Coach;
use App\Repositories\BaseRepository;

/**
 * Class CoachRepository
 * @package App\Repositories
 * @version July 29, 2020, 9:11 am UTC
*/

class CoachRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'bio',
        'rating',
        'experience',
        'qualification_id',
        'facebook_url',
        'instagram_url',
        'youtube_url',
        'is_active',
        'discovery_source'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Coach::class;
    }
}
