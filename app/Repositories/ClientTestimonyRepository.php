<?php

namespace App\Repositories;

use App\Models\ClientTestimony;
use App\Repositories\BaseRepository;

/**
 * Class ClientTestimonyRepository
 * @package App\Repositories
 * @version October 20, 2020, 11:51 am UTC
*/

class ClientTestimonyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'client_name',
        'client_photo_url',
        'client_feedback',
        'client_rating'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ClientTestimony::class;
    }
}
