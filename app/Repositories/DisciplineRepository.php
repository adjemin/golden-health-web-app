<?php

namespace App\Repositories;

use App\Models\Discipline;
use App\Repositories\BaseRepository;

/**
 * Class DisciplineRepository
 * @package App\Repositories
 * @version July 29, 2020, 6:04 pm UTC
*/

class DisciplineRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'image',
        'slug',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Discipline::class;
    }
}
