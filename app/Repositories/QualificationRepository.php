<?php

namespace App\Repositories;

use App\Models\Qualification;
use App\Repositories\BaseRepository;

/**
 * Class QualificationRepository
 * @package App\Repositories
 * @version July 30, 2020, 4:00 am UTC
*/

class QualificationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'period',
        'qualifi_id',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Qualification::class;
    }
}
