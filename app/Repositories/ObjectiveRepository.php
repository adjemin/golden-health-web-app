<?php

namespace App\Repositories;

use App\Models\Objective;
use App\Repositories\BaseRepository;

/**
 * Class ObjectiveRepository
 * @package App\Repositories
 * @version July 30, 2020, 7:06 am UTC
*/

class ObjectiveRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
      'name',
      'slug',
      'description',
      'active'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Objective::class;
    }
}
