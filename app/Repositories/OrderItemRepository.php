<?php

namespace App\Repositories;

use App\Models\OrderItem;
use App\Repositories\BaseRepository;

/**
 * Class OrderItemRepository
 * @package App\Repositories
 * @version September 16, 2020, 12:14 pm UTC
*/

class OrderItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'service_slug',
        'meta_data_id',
        'meta_data',
        'quantity',
        'unit_price',
        'total_amount',
        'currency_code',
        'currency_name',
        'days',
        'status',
        'creator',
        'creator_id',
        'creator_name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderItem::class;    
    }
}