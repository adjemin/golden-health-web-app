<?php

namespace App\Repositories;

use App\Models\Availability;
use App\Repositories\BaseRepository;

/**
 * Class AvailabilityRepository
 * @package App\Repositories
 * @version July 29, 2020, 5:59 pm UTC
*/

class AvailabilityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date_debut',
        'date_fin',
        'start_time',
        'end_time',
        'coach_id',
        'course_id',
        'status',
        'is_confirmed'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Availability::class;
    }
}
