<?php

namespace App\Repositories;

use App\Models\CoachDiscipline;
use App\Repositories\BaseRepository;

/**
 * Class CoachDisciplineRepository
 * @package App\Repositories
 * @version July 29, 2020, 6:11 pm UTC
*/

class CoachDisciplineRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'coach_id',
        'discipline_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CoachDiscipline::class;
    }
}
