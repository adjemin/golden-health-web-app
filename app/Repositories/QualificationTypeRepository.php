<?php

namespace App\Repositories;

use App\Models\QualificationType;
use App\Repositories\BaseRepository;

/**
 * Class QualificationTypeRepository
 * @package App\Repositories
 * @version July 30, 2020, 3:53 am UTC
*/

class QualificationTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug',
        'abreviation'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return QualificationType::class;
    }
}
