<?php

namespace App\Repositories;

use App\Models\Connexion;
use App\Repositories\BaseRepository;

/**
 * Class ConnexionRepository
 * @package App\Repositories
 * @version August 18, 2020, 11:18 am UTC
*/

class ConnexionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'username',
        'email',
        'phone',
        'dial_code',
        'phone_number',
        'password'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Connexion::class;
    }
}
