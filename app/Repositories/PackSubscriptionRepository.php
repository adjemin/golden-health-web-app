<?php

namespace App\Repositories;

use App\Models\PackSubscription;
use App\Repositories\BaseRepository;

/**
 * Class PackSubscriptionRepository
 * @package App\Repositories
 * @version July 29, 2020, 6:26 pm UTC
*/

class PackSubscriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pack_id',
        'customer_id',
        'course_id',
        'discipline_id',
        'coach_id',
        'is_active',
        'session_days',
        'sessions_completed',
        'sessions_waiting',
        'sessions_failed',
        'is_completed'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PackSubscription::class;
    }
}
