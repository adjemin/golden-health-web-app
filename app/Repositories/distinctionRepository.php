<?php

namespace App\Repositories;

use App\Models\distinction;
use App\Repositories\BaseRepository;

/**
 * Class distinctionRepository
 * @package App\Repositories
 * @version October 14, 2020, 4:55 pm UTC
*/

class distinctionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'date_debut',
        'date_fin',
        'fichier_url'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return distinction::class;
    }
}
