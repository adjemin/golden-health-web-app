<?php

namespace App\Repositories;

use App\Models\Advice;
use App\Repositories\BaseRepository;

/**
 * Class AdviceRepository
 * @package App\Repositories
 * @version July 30, 2020, 6:50 am UTC
*/

class AdviceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'coach_id',
        'message',
        'rate'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Advice::class;
    }
}
