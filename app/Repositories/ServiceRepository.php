<?php

namespace App\Repositories;

use App\Models\Service;
use App\Repositories\BaseRepository;

/**
 * Class ServiceRepository
 * @package App\Repositories
 * @version July 29, 2020, 8:39 pm UTC
*/

class ServiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'name_en',
        'slug',
        'description',
        'description_en',
        'logo',
        'price',
        'price_en',
        'price_description',
        'price_description_en',
        'price_is_variable',
        'currency',
        'delay',
        'delay_en',
        'delay_time',
        'delay_time_unit',
        'rank'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Service::class;
    }
}
