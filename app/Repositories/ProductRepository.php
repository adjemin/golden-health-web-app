<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\BaseRepository;

/**
 * Class ProductRepository
 * @package App\Repositories
 * @version July 30, 2020, 6:38 am UTC
*/

class ProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description',
        'description_metadata',
        'price',
        'original_price',
        'fees',
        'currency_slug',
        'old_price',
        'has_promo',
        'promo_percentage',
        'medias',
        'is_sold',
        'sold_at',
        'initiale_count',
        'published_at',
        'is_published',
        'slug',
        'link',
        'delivery_services'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Product::class;
    }
}
