<?php

namespace App\Repositories;

use App\Models\RecapBMI;
use App\Repositories\BaseRepository;

/**
 * Class RecapBMIRepository
 * @package App\Repositories
 * @version July 29, 2020, 7:33 pm UTC
*/

class RecapBMIRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'description',
        'interval',
        'color',
        'weigth',
        'height',
        'customer_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RecapBMI::class;
    }
}
