<?php

namespace App\Repositories;

use App\Models\BodyMassIndex;
use App\Repositories\BaseRepository;

/**
 * Class BodyMassIndexRepository
 * @package App\Repositories
 * @version July 29, 2020, 7:17 pm UTC
*/

class BodyMassIndexRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'date',
        'value_BMI'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BodyMassIndex::class;
    }
}
