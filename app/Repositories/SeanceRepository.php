<?php

namespace App\Repositories;

use App\Models\Seance;
use App\Repositories\BaseRepository;

/**
 * Class SeanceRepository
 * @package App\Repositories
 * @version July 30, 2020, 7:18 am UTC
*/

class SeanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'discipline_id',
        'coach_id',
        'duration',
        'duration',
        'bilan',
        'objectif_long_terme',
        'objectif_court_terme',
        'time_hour_by_day',
        'week_duration'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Seance::class;
    }
}
