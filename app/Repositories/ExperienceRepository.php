<?php

namespace App\Repositories;

use App\Models\Experience;
use App\Repositories\BaseRepository;

/**
 * Class ExperienceRepository
 * @package App\Repositories
 * @version July 30, 2020, 3:35 am UTC
*/

class ExperienceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'experience',
        'title'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Experience::class;
    }
}
