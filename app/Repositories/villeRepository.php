<?php

namespace App\Repositories;

use App\Models\ville;
use App\Repositories\BaseRepository;

/**
 * Class villeRepository
 * @package App\Repositories
 * @version December 7, 2020, 6:09 pm UTC
*/

class villeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ville::class;
    }
}
