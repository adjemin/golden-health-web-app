<?php

namespace App\Repositories;

use App\Models\LieuCourse;
use App\Repositories\BaseRepository;

/**
 * Class LieuCourseRepository
 * @package App\Repositories
 * @version October 5, 2020, 8:37 pm UTC
*/

class LieuCourseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug',
        'description',
        'active'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return LieuCourse::class;
    }
}
