<?php

namespace App\Repositories;

use App\Models\CourseLieu;
use App\Repositories\BaseRepository;

/**
 * Class courseLieuRepository
 * @package App\Repositories
 * @version October 5, 2020, 8:35 pm UTC
*/

class courseLieuRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'course_id',
        'lieu_course_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CourseLieu::class;
    }
}
