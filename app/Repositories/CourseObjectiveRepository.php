<?php

namespace App\Repositories;

use App\Models\CourseObjective;
use App\Repositories\BaseRepository;

/**
 * Class CourseObjectiveRepository
 * @package App\Repositories
 * @version October 5, 2020, 9:08 pm UTC
*/

class CourseObjectiveRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
      'course_id',
      'objective_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CourseObjective::class;
    }
}
