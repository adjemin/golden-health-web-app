<?php

namespace App;

use App\Models\Event;
use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    //
    protected $guarded = [];

    public function events()
    {
        return $this->hasMany(Event::class);
    }
}