<?php 

namespace App\Utils;

use Carbon\Carbon;

/**
 * This classes allows to perform somme specifis operations on Datetime objects 
 */
class Date
{
	// Days in french
	var $frenchDays = [
		0 => "Lundi",
		1 => "Mardi",
		2 => "Mercredi",
		3 => "Jeudi",
		4 => "Vendredi",
		5 => "Samedi",
		6 => "Dimanche"
    ];
    // Days in english
    var $englishDays = [
		0 => "Monday",
		1 => "Tuesday",
		2 => "Wednesday",
		3 => "Thursday",
		4 => "Friday",
		5 => "Saturnday",
		6 => "Sunday"
	];
	// Months in french
	var $frenchMonths = [
		0 => "Janvier",
		1 => "Février",
		2 => "Mars",
		3 => "Avril",
		4 => "Mai",
		5 => "Juin",
		6 => "Juillet",
		7 => "Août",
		8 => "Septembre",
		9 => "Octobre",
		10 => "Novembre",
		11 => "Décembre"
	];
	// Months in english
	var $englishMonths = [
		0 => "January",
		1 => "February",
		2 => "March",
		3 => "April",
		4 => "May",
		5 => "June",
		6 => "July",
		7 => "August",
		8 => "September",
		9 => "October",
		10 => "November",
		11 => "December"
	];

	function __construct()
	{
		$this->carbon = new Carbon();
	}

	/**
	 * Adds a '0' if the given param is a single digit
	 * 
	 * @param string temp like '2'
	 * 
	 * @return string temp like '02
	 */
	function ajust($temp){
		if(strlen($temp) == 1){
			return "0".$temp;
		}return $temp;
	}

	public function get_hour($temp)
	{
		return $this->ajust($this->get_date($temp)->hour);
	}

	public function get_minute($temp)
	{
		return $this->ajust($this->get_date($temp)->minute);
	}

	public function get_second($temp)
	{
		return $this->ajust($this->get_date($temp)->second);
	}

	public function get_day($temp, $lang)
	{
		if($lang == 'en')
		{
			return $this->englishDays[$this->get_date($temp)->dayOfWeek==0? 6:$this->get_date($temp)->dayOfWeek-1];
		}
		elseif($lang == 'fr')
		{
			return $this->frenchDays[$this->get_date($temp)->dayOfWeek==0? 6:$this->get_date($temp)->dayOfWeek-1];
		}
	}

	public function get_day_number($temp)
	{
		return $this->get_date($temp)->day;
	}

	public function get_month($temp, $lang)
	{
		if($lang == 'en')
		{
			return $this->englishMonths[$this->get_date($temp)->month-1];
		}
		elseif($lang == 'fr')
		{
			return $this->frenchMonths[$this->get_date($temp)->month-1];
		}
		
	}

	public function get_year($temp)
	{
		return $this->get_date($temp)->year;
	}

	public function get_date($temp)
	{
		return new Carbon($temp);
	}

}
