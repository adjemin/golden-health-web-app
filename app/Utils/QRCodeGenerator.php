<?php

namespace App\Utils;

use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QRCodeGenerator
{
    public function qrcode($secret, $format)
    {
        return base64_encode(QrCode::format($format)->size(100)->generate($secret));
    }
}