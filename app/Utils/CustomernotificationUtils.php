<?php


namespace App\Utils;


use App\Models\Customer;
use App\Models\CustomerDevice;
use App\Models\CustomerNotifications;
use Illuminate\Support\Collection;

class CustomerNotificationUtils
{

    const WAITING = "waiting";
    const CONFIRMED = "confirmed";
    const EDITED = "edited";
    const REFUSED = "refused";
    const DELIVERED = "delivered";
    const FAILED = "failed";
    const CANCELED = "canceled";
    const NOTED = "noted";
    const REPORTED = "reported";
    const WAITING_CUSTOMER_PICKUP = "waiting_customer_pickup";
    const WAITING_CUSTOMER_DELIVERY = "waiting_customer_delivery";
    const CUSTOMER_PAID = "customer_paid";
    const HAS_SELLER_PAID = "has_seller_paid";

    /**
     * @param CustomerNotifications $customerNotification
     */
    public static function notify($customerNotification){
        $customer = Customer::where(["id" => $customerNotification->user_id])->first();

        $defaultLanguage = $customer->default_language;
        if($defaultLanguage == "FR"){
            $title = $customerNotification->title;
            $subtitle = $customerNotification->subtitle;
        }else{
            $title = $customerNotification->title_en;
            $subtitle = $customerNotification->subtitle_en;
        }

        $customerDevices = CustomerDevice::where([
            "customer_id" => $customerNotification->user_id,
            "deleted_at" => null
            ])->orderBy('id', 'DESC')/*->take(1)*/->get();


        $devices = Collection::make([]);
        foreach ($customerDevices as $customerDevice){
            
            $metadata = $customerNotification->toArray();
            $result =  FirebaseMessagingUtils::sendNotification($title, $subtitle, $metadata, $customerDevice->firebase_id);


            $devices->push([
                "phone" => $customerDevice->firebase_id,
                "result" => $result
            ]);
        }

       
        if($customerDevices != null && count($customerDevices) >0){
            //$customerDevice = $customerDevices[0];
            //$device = "".$customerDevice->firebase_id;

            return $devices;
        }else{
            return [];
        }

    }

}
