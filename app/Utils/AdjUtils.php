<?php
namespace App\Utils;
use Symfony\Component\HttpFoundation\ParameterBag;
class AdjUtils
{
    const BASE_URL = "https://adjem.in";
        /**
     * @param $dial_code
     * @param $phone_number
     * @return ParameterBag
     */
    public static function createShortUrl($longUrl){
        $url = 'https://adjem.in/api/v1/url';
        $fields  = [
            'url' => $longUrl,
            'privateUrl' => 1,
            'customUrl'=> '',
            'hideUrlStats' => 0
        ];
        
        $headers = array (
           // 'Content-Type: application/json'
        );

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
        $result = curl_exec ( $ch );
        curl_close ( $ch );
        $result = (array)json_decode($result, true);
        //TODO Transfor array to @ParameterBag
        $resultJson = new ParameterBag($result);
        return $resultJson;
    }
}