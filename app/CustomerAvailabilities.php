<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerAvailabilities extends Model
{
    protected $fillable = [
        'challenger_id',
        'availabities_id',
        'booking_id'
    ];

    public function challenger(){
        return $this->belongsTo(Customer::class, 'challenger_id', 'id');
    }

    public function availabity(){
        return $this->belongsTo(\App\Models\Customer::class, 'availabities_id', 'id');
    }

    public function booking(){
        return $this->belongsTo(\App\Models\BookingCoach::class, 'booking_id', 'id');
    }
}
