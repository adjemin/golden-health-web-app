<?php

namespace App;

use App\Models\Event;
use App\Models\Order;
use App\Models\Review;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use Notifiable;

    public $table = 'customers';

    const DEFAULT_PHOTO_URL = "https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png";

    // public static $SHOP_SERVICE_SLUG = 'shop';

    public $fillable = [
        'last_name',
        'first_name',
        'name',
        'dial_code',
        'phone_number',
        'phone',
        'is_phone_verifed',
        'phone_verifed_at',
        'is_active',
        'activation_date',
        'email',
        'photo_url',
        'gender',
        'size',
        'weight',
        'bio',
        'birthday',
        'birth_location',
        'language',
        'location_address',
        'location_latitude',
        'location_longitude',
        'location_gps',
        'password',
        'is_cgu',
        'type_account',
        'is_newsletter',
        'commune',
        'quartier',
        'email_verifed_at',
        'facebook_id',
        'google_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'last_name' => 'string',
        'first_name' => 'string',
        'name' => 'string',
        'dial_code' => 'string',
        'phone_number' => 'string',
        'phone' => 'string',
        'is_phone_verifed' => 'boolean',
        'is_active' => 'boolean',
        'email' => 'string',
        'photo_url' => 'string',
        'gender' => 'string',
        'bio' => 'string',
        'birth_location' => 'string',
        'language' => 'string',
        'location_address' => 'string',
        'location_latitude' => 'string',
        'location_longitude' => 'string',
        'location_gps' => 'string',
        'password' => 'string'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Validation rules
     *
     * @var array
     */

    public function getAuthPassword()
    {
        return $this->password;
    }

    public static $rules = [

    ];

    public function is_coach()
    {
        return $this->hasOne('App\Models\Coach');
    }

    public function profileCompletion(){

        $percentage = 0;

        $completedInfosCount = 0;

        // $requiredInfos = [
        //     'first_name',
        //     'name',
        //     'dial_code',
        //     'phone_number',
        //     'phone',
        //     // 'is_phone_verifed',
        //     'email',
        //     // 'email_verifed_at',
        //     'photo_url',
        //     // 'gender',
        //     // 'size',
        //     // 'weight',
        //     // 'bio',
        //     // 'birthday',
        // ];
        $requiredInfos = [
            'this->first_name',
            'this->name',
            'this->dial_code',
            'this->phone_number',
            'this->phone',
            // 'this->is_phone_verifed',
            'this->email',
            'this->email_verifed_at',
            'this->photo_url',
            'this->gender',
            // 'this->size',
            // 'this->weight',
            // 'this->bio',
            // 'this->birthday',
            // 'this->language',
        ];
        // ajouter ici ↑ les nouveaux champs de customer à include dans le pourcentage

        $requiredInfosCount = sizeof($requiredInfos);
        if($requiredInfosCount > 0 ){

            // for($i=0; $i<$requiredInfosCount; $i++){
            //     if($$requiredInfos[$i] != null){
            //         $completedInfosCount++;
            //     }
            // }
            //
                if($this->first_name != null){
                    $completedInfosCount++;
                }
                if($this->last_name != null){
                    $completedInfosCount++;
                }
                if($this->name != null){
                    $completedInfosCount++;
                }
                if($this->dial_code != null){
                    $completedInfosCount++;
                }
                if($this->phone_number != null){
                    $completedInfosCount++;
                }
                if($this->phone != null){
                    $completedInfosCount++;
                }
                if($this->email != null){
                    $completedInfosCount++;
                }
                if($this->photo_url != null){
                    if($this->photo_url != Customer::DEFAULT_PHOTO_URL){
                        $completedInfosCount++;
                    }
                }
                // if($this->is_phone_verified != null && $this->is_phone_verified){
                //     $completedInfosCount++;
                // }
                if($this->gender != null){
                    $completedInfosCount++;
                }
                // if($this->size != null){
                //     $completedInfosCount++;
                // }
                // if($this->weight != null){
                //     $completedInfosCount++;
                // }
                // if($this->bio != null){
                //     $completedInfosCount++;
                // }
                // if($this->birthday != null){
                //     $completedInfosCount++;
                // }
                // if($this->language != null){
                //     $completedInfosCount++;
                // }

            // répeter ↑ pour les nouveaux champs de customer à include dans le pourcentage

            $percentage = ($completedInfosCount*100)/$requiredInfosCount;
            $percentage = ceil($percentage);
            if($percentage > 100){
                $percentage = 100;
            }
        }

        return $percentage;
    }
    public function profileCompletionProgress(){


        return "<div class='progress mb-3'>
                    <div class='progress-bar progress-bar-striped progress-bar-animated' role='progressbar' aria-valuenow='".$this->profileCompletion()."' aria-valuemin='0' aria-valuemax='100' style='width: ".$this->profileCompletion()."%'>".$this->profileCompletion()."%
                    </div>
                </div>";
    }


    public function eventsParticipatingIn()
    {
        return $this->belongsToMany(Event::class, 'participates_in_event', 'customer_id', 'event_id');
    }

    public function tickets()
    {
        return $this->hasMany(EventTicket::class)->latest();
    }

    public function shoppingCart()
    {
        return $this->hasOne(ShoppingCart::class);
    }

    public function shopOrders()
    {
        $customer = Auth::guard('customer')->user();
        if(!$customer){
            return null;
        }
        return Order::where('customer_id', $customer->id)->where('service_slug', 'shop')->orderBy('created_at', 'desc')->get();
        // return $this->hasMany(Order::class);
        //
    }

    public function shopOrdersApi()
    {
        return Order::where('customer_id', $this->id)->where('service_slug', 'shop')->orderBy('created_at', 'desc')->get();
    }

    // get delivered orders
    public function deliveredOrders(){
        $customer = Auth::guard('customer')->user();
        if(!$customer){
            return null;
        }
        $deliveredOrders = $this->shopOrders()->where('status', Order::DELIVERED);

        return $deliveredOrders;
    }

    public function deliveredOrdersApi(){
        return $this->shopOrdersApi()->where('status', Order::DELIVERED);
    }

    public function productsToReviewApi(){
        $deliveredProducts = $this->deliveredProductsApi();
        // dd($deliveredProducts);
        $productsToReview = new Collection([]);
        foreach($deliveredProducts as $product){
            if(is_null($product)){
                continue;
            }
            // check if product is reviewed
            // dd($product);
            if(!$this->shopReviews->where('source_id', $product->id)->first()){
                // if not reviewed, add to review
                $productsToReview = $productsToReview->push($product);
            }
        }
        return $productsToReview;
    }

    public function deliveredProductsApi(){
        $deliveredOrders = $this->deliveredOrdersApi();

        $products = new Collection([]);

        foreach($deliveredOrders as $order){
            // fetch order items
            foreach($order->orderItems as $orderItem){
                // find each product
                $products = $products->push($orderItem->product);
            }
        }

        return $products;
    }

    // get products that got delivered
    public function deliveredProducts(){
        $customer = Auth::guard('customer')->user();
        if(!$customer){
            return null;
        }

        $deliveredOrders = $this->deliveredOrders();

        $products = new Collection([]);

        foreach($deliveredOrders as $order){
            // fetch order items
            foreach($order->orderItems as $orderItem){
                // find each product
                $products = $products->push($orderItem->product);
            }
        }

        return $products;
    }

    // get products that got delivered but have no reviews from current user
    public function productsToReview(){
        // these products must have been delivered first
        $deliveredProducts = $this->deliveredProducts();

        $productsToReview = new Collection([]);
        foreach($deliveredProducts as $product){
            // check if product is reviewed
            if(!$this->shopReviews->where('source_id', $product->id)->first()){
                // if not reviewed, add to review
                $productsToReview = $productsToReview->push($product);
            }
        }
        // filter the ones who have comments or not
        return $productsToReview;
    }

    public function eventOrders()
    {
        $customer = Auth::guard('customer')->user();
        if(!$customer){
            return null;
        }
        return Order::where('customer_id', $customer->id)->where('service_slug', 'event')->orderBy('created_at')->get();
        // return $this->hasMany(Order::class);
        //
    }

    public function wishlistedProducts(){
        return $this->belongsToMany(Product::class, 'wishlists');
    }
    public function favoritedProducts(){
        return $this->belongsToMany(Product::class, 'favorite_products');
    }


    // *** reviews
    public function reviews(){
        return $this->hasMany(Review::class);
    }
    public function shopReviews(){
        return $this->hasMany(Review::class)->where('source', 'product');
    }
    public function bookingReviews(){
        return $this->hasMany(Review::class)->where('source', 'booking');
    }

    public function booking(){
        return $this->hasMany(\App\Models\BookingCoach::class)->where('customer_id', $this->id);
    }
}
