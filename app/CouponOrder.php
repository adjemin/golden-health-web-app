<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponOrder extends Model
{
    //
    protected $guarded = [];
    protected $appends = ['coupon'];


    public function order(){
        return $this->belongsTo(Order::class);
    }
    public function coupon(){
        return $this->belongsTo(\App\Models\Coupon::class);
    }


    public function getCouponAttribute(){
        return \App\Models\Coupon::find($this->coupon_id);
    }
}
