<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entreprise extends Model
{
    //
    protected $table = "entreprise";
    protected $guarded = [];

    //
    public function values()
    {
        return $this->hasMany(EntrepriseValue::class);
    }

    public static function Instance(){
        return Entreprise::first();
    }
}
