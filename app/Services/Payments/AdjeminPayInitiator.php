<?php

namespace App\Services\Payments;

use Exception;
use App\Models\Order;
use App\Models\BookingCoach;
use AdjeminPay\AdjeminPay;
use AdjeminPay\Transaction;
use AdjeminPay\Exception\AdjeminPayBadRequest;

class AdjeminPayInitiator implements PaymentGatewayInitiator
{
    public function charge(string $orderType, BookingCoach $order, int $orderAmount)
    {

      //dd('AdjeminPay');
      return redirect('/customer/page/payment');

      /*$adjeminPay = new \AdjeminPay\AdjeminPay(config('services.adjeminpay.ADJEMINPAY_APP_ID'), config('services.adjeminpay.ADJEMINPAY_API_KEY'));

      try {

        $transaction = $adjeminPay->getTransanctionByReference($order->booking_ref);

        // Verification de l'etat du traitement de la commande
        if($transaction->is_successfull){
            echo 'Bravo, votre paiement a été effectué avec succès';
            //$my_transaction->setStatus($transaction->status);
            die();
        }

        // Verification du montant de la transaction
        try {
            if($transaction->amount == $order->amount ){
                if (in_array($transaction->status, ['CANCELLED', 'EXPIRED', 'FAILED'])){
                    echo "La transaction n'as pas pu se dérouler comme prévue";
                }else{
                    //$my_transaction->setStatus($transaction->status);
                }
            }else{
                throw new AdjeminPayBadRequest("Tentative de fraude", 500);
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }


      } catch (\Exception $e) {
        abort(500, 'Whoops! Une erreur est survenue au niveau du serveur.');
      }
      */

    }
}
