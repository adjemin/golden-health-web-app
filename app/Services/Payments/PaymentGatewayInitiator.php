<?php

namespace App\Services\Payments;

use App\Models\BookingCoach;
use App\Models\Order;

interface PaymentGatewayInitiator
{
    public function charge(string $orderType, BookingCoach $order, int $orderAmount);
}
