<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderHistory extends Model
{
    use SoftDeletes;

    public $table = 'order_histories';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'order_id',
        'status',
        'creator',
        'creator_id',
        'creator_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'status' => 'string',
        'creator' => 'string',
        'creator_id' => 'integer',
        'creator_name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function order(){
        return $this->belongsTo(Order::class);
    }

}