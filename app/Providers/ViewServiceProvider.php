<?php

namespace App\Providers;








use App\Models\Course;
use App\Models\Invoice;
use App\Models\QualifictionType;
use App\Models\Order;
use App\Models\Customer;
use App\Models\Pack;
use App\Models\Discipline;
use App\Models\Coach;

use Illuminate\Support\ServiceProvider;
use View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['reviews.fields'], function ($view) {
            $customerItems = Customer::pluck('name','id')->toArray();
            $view->with('customerItems', $customerItems);
        });
        View::composer(['seances.fields'], function ($view) {
            $coachItems = Coach::pluck('name','id')->toArray();
            $view->with('coachItems', $coachItems);
        });
        View::composer(['seances.fields'], function ($view) {
            $disciplineItems = Discipline::pluck('name','id')->toArray();
            $view->with('disciplineItems', $disciplineItems);
        });
        View::composer(['objectives.fields'], function ($view) {
            $courseItems = Course::pluck('name','id')->toArray();
            $view->with('courseItems', $courseItems);
        });
        View::composer(['advice.fields'], function ($view) {
            $coachItems = Coach::pluck('name','id')->toArray();
            $view->with('coachItems', $coachItems);
        });
        View::composer(['advice.fields'], function ($view) {
            $customerItems = Customer::pluck('name','id')->toArray();
            $view->with('customerItems', $customerItems);
        });
        View::composer(['invoice_payment'], function ($view) {
            $orderItems = Order::pluck('name','id')->toArray();
            $view->with('orderItems', $orderItems);
        });
        View::composer(['invoice_payment'], function ($view) {
            $customerItems = Customer::pluck('name','id')->toArray();
            $view->with('customerItems', $customerItems);
        });
        View::composer(['invoice_payments.fields'], function ($view) {
            $customerItems = Customer::pluck('name','idd')->toArray();
            $view->with('customerItems', $customerItems);
        });
        View::composer(['invoice_payments.fields'], function ($view) {
            $customerItems = Customer::pluck('name','id')->toArray();
            $view->with('customerItems', $customerItems);
        });
        View::composer(['invoice_payments.fields'], function ($view) {
            $customerItems = Customer::pluck('name','id')->toArray();
            $view->with('customerItems', $customerItems);
        });
        View::composer(['invoice_payments.fields'], function ($view) {
            $invoiceItems = Invoice::pluck('name','id')->toArray();
            $view->with('invoiceItems', $invoiceItems);
        });
        View::composer(['invoice_payments.fields'], function ($view) {
            $invoiceItems = Invoice::pluck('reference','id')->toArray();
            $view->with('invoiceItems', $invoiceItems);
        });
        View::composer(['qualifications.fields'], function ($view) {
            $qualifiction_typeItems = QualifictionType::pluck('slug','id')->toArray();
            $view->with('qualifiction_typeItems', $qualifiction_typeItems);
        });
        View::composer(['courses.fields'], function ($view) {
            $packItems = Pack::pluck('name','id')->toArray();
            $view->with('packItems', $packItems);
        });
        View::composer(['invoices.fields'], function ($view) {
            $customerItems = Customer::pluck('name','id')->toArray();
            $view->with('customerItems', $customerItems);
        });
        View::composer(['invoices.fields'], function ($view) {
            $orderItems = Order::pluck('name','id')->toArray();
            $view->with('orderItems', $orderItems);
        });
        View::composer(['addresses.fields'], function ($view) {
            $coachItems = Coach::pluck('name','id')->toArray();
            $view->with('coachItems', $coachItems);
        });
        View::composer(['addresses.fields'], function ($view) {
            $customerItems = Customer::pluck('name','id')->toArray();
            $view->with('customerItems', $customerItems);
        });
        View::composer(['recap_b_m_is.fields'], function ($view) {
            $customerItems = Customer::pluck('name','id*')->toArray();
            $view->with('customerItems', $customerItems);
        });
        View::composer(['orders.fields'], function ($view) {
            $customerItems = Customer::pluck('name','id')->toArray();
            $view->with('customerItems', $customerItems);
        });
        View::composer(['portfolios.fields'], function ($view) {
            $coachItems = Coach::pluck('name','id')->toArray();
            $view->with('coachItems', $coachItems);
        });
        View::composer(['pack_subscriptions.fields'], function ($view) {
            $coachItems = Coach::pluck('name','id')->toArray();
            $view->with('coachItems', $coachItems);
        });
        View::composer(['pack_subscriptions.fields'], function ($view) {
            $customerItems = Customer::pluck('name','id')->toArray();
            $view->with('customerItems', $customerItems);
        });
        View::composer(['pack_subscriptions.fields'], function ($view) {
            $packItems = Pack::pluck('name','id')->toArray();
            $view->with('packItems', $packItems);
        });
        View::composer(['coach_disciplines.fields'], function ($view) {
            $disciplineItems = Discipline::pluck('name','id')->toArray();
            $view->with('disciplineItems', $disciplineItems);
        });
        View::composer(['coach_disciplines.fields'], function ($view) {
            $coachItems = Coach::pluck('name','id')->toArray();
            $view->with('coachItems', $coachItems);
        });
        View::composer(['availabilities.fields'], function ($view) {
            $coachItems = Coach::pluck('name','id')->toArray();
            $view->with('coachItems', $coachItems);
        });
        //
    }
}
