<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class HowDidYouKnow
 * @package App\Models
 * @version August 18, 2020, 10:24 am UTC
 *
 * @property string $name
 * @property string $name_en
 * @property string $user_source
 * @property integer $user_id
 * @property string $description
 */
class HowDidYouKnow extends Model
{
    use SoftDeletes;

    public $table = 'how_did_you_knows';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'name_en',
        'user_source',
        'user_id',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'name_en' => 'string',
        'user_source' => 'string',
        'user_id' => 'integer',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
