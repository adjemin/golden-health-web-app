<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CoachDiscipline
 * @package App\Models
 * @version July 29, 2020, 6:11 pm UTC
 *
 * @property integer $coach_id
 * @property integer $discipline_id
 */
class CoachDiscipline extends Model
{
    use SoftDeletes;

    public $table = 'coach_disciplines';
    

    protected $dates = ['deleted_at'];

    public $appends = [
        'coachs',
        'discipline'
    ];

    public $fillable = [
        'coach_id',
        'discipline_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'coach_id' => 'integer',
        'discipline_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getDisciplineAttribute(){
        return Discipline::where("id", $this->discipline_id)->first();
    }
    
    public function getCoachsAttribute(){
        return Coach::where("id", $this->coach_id)->get() ?? [];
    }

}
