<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Portfolio
 * @package App\Models
 * @version July 29, 2020, 6:31 pm UTC
 *
 * @property integer $coach_id
 * @property string $title
 * @property string $images_url
 * @property string $video_url
 */
class Portfolio extends Model
{
    use SoftDeletes;

    public $table = 'portfolios';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'coach_id',
        'title',
        'images_url',
        'video_url'
        //'media_type',
        //'media_path'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'coach_id' => 'integer',
        'title' => 'string',
        'images_url' => 'string',
        'video_url' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
