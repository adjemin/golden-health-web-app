<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Experience
 * @package App\Models
 * @version July 30, 2020, 3:35 am UTC
 *
 * @property string $experience
 * @property string $title
 */
class Experience extends Model
{
    use SoftDeletes;

    public $table = 'experiences';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'experience',
        'title'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'experience' => 'string',
        'title' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
