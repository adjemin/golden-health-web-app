<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class LieuCourse
 * @package App\Models
 * @version October 5, 2020, 8:37 pm UTC
 *
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property boolean $active
 */
class LieuCourse extends Model
{
    use SoftDeletes;

    public $table = 'lieu_courses';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'slug',
        'description',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

}
