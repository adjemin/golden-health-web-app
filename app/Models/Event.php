<?php

namespace App\Models;

use App\EventTicket;
use App\EventType;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * Class Event
 * @package App\Models
 * @version July 29, 2020, 6:48 pm UTC
 *
 * @property string $title
 * @property string $date_event
 * @property string $place
 * @property string $description
 * @property string $place_number
 */
class Event extends Model
{
    use SoftDeletes;

    public $table = 'events';


    protected $dates = ['deleted_at'];
    protected $appends = ['place'];



    public $fillable = [
        'title',
        'slug',
        'date_event',
        'venue',
        'description',
        'place_number',
        'cover_url',
        'entry_fee',
        'date_event_end',
        'event_type_id',
        'start_time',
        'end_time'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'venue' => 'string',
        'description' => 'string',
        'place_number' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function type()
    {
        return $this->belongsTo(EventType::class);
    }


    public function scopeIsUpcoming($query)
    {
        return $query->where('date_event', '>=', today());
    }

    public function scopeIsPast($query)
    {
        return $query->where('date_event', '<', today());
    }

    public function scopeIsUpcomingEvent($query)
    {
        if($this->date_event_end != null){
            return $query->where('date_event_end', '>=', today());
        }else{
            return $query->where('date_event', '>=', today());
        }
    }

    public function scopeIsPastEvent($query)
    {
        if($this->date_event_end != null){
            return $query->where('date_event_end', '<', today());
        }else{
            return $query->where('date_event', '<', today());
        }
    }


    public function isUpcoming()
    {
        if($this->date_event_end != null){
            return $this->date_event_end >= today();
        }else{
            return $this->date_event>=today();
        }
    }
    public function isPast()
    {
        if($this->date_event_end != null){
            return $this->date_event_end < today();
        }else{
            return $this->date_event < today();
        }
    }

    public function showEntryFees()
    {
        $content = $this->entry_fee == null || $this->entry_fee == 0 ? "Gratuit" : $this->entry_fee ." XOF";

        return '<a href="#" class="free mb-2 mb-md-0">'.$content.'</a>';
    }

    public function participants()
    {
        return $this->belongsToMany(Customer::class, 'participates_in_event', 'event_id', 'customer_id');
    }

    public function paidParticipants()
    {
        return $this->participants()->where('is_paid', true);
    }

    public function isCurrentCustomerParticipating()
    {
        $r = false;
        if (Auth::guard('customer')->check()) {
            // if ($this->participants()->find(Auth::guard('customer')->user()->id)) {
            //     $r = true;
            // }
            if ($this->paidParticipants()->find(Auth::guard('customer')->user()->id)) {
                $r = true;
            }
        }
        return $r;
    }

    public function isFull()
    {
        // return $this->participants()->count() - ($this->place_number ?? 0) >= 0 ? true : false;
        // $t = false;

        // return $this->paidParticipants()->count() - ($this->place_number ?? 0) >= 0 ? true : false;
        return $this->availableSeatsCount() <= 0;
    }

    public function isFree()
    {
        return $this->entry_fee <= 0 || $this->entry_fee == null;
    }

    public function isAttendable()
    {
        return $this->place_number > 0 && $this->place_number != null;
        // return $this->place_number > 0;
    }

    public function availableSeatsCount_()
    {

        if($this->isAttendable()){
            $available_seats = $this->place_number;
            foreach($this->tickets as $t){
                $available_seats -= $t->seats_count;
            }
            if($available_seats < 0){
                return 0;
            }
            return $available_seats;
            // return $this->place_number - $this->participants()->count();
        }
        return 0;
    }
    public function availableSeatsCount()
    {

        if($this->isAttendable()){
            $available_seats = $this->place_number;
            // foreach($this->tickets as $t){
            //     $available_seats -= $t->seats_count;
            // }
            foreach($this->paidTickets() as $t){
                $available_seats -= $t->seats_count;
            }
            if($available_seats < 0){
                return 0;
            }
            return $available_seats;
            // return $this->place_number - $this->participants()->count();
        }
        return 0;
    }

    public function availableSeatsText()
    {
        $t = '';
        if($this->isAttendable()){
            $t = $this->availableSeatsCount() > 1 ? ' Places restantes' : ' Place restante';
        }
        return $t;

    }

    public function tickets()
    {
        return $this->hasMany(EventTicket::class);
    }
    public function paidTickets()
    {
        return $this->tickets->where('is_paid', true);
    }

    public function getPlaceAttribute(){
        return (int) $this->place_number - (int) $this->participants->count();
    }
}
