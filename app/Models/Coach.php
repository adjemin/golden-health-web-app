<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Coach
 * @package App\Models
 * @version July 29, 2020, 9:11 am UTC
 *
 * @property string $customer_id
 * @property string $bio
 * @property string $rating
 * @property string $experience
 * @property integer $qualification_id
 * @property string $facebook_url
 * @property string $instagram_url
 * @property string $youtube_url
 */
class Coach extends Model
{
    use SoftDeletes;

    public $table = 'coaches';


    protected $dates = ['deleted_at'];
    protected $appends = ['caracteristiques', 'customer', 'disciplines', 'portfolios'];


    public $fillable = [
        'customer_id',
        'title',
        'bio',
        'rating',
        'experience',
        'qualification_id',
        'facebook_url',
        'instagram_url',
        'youtube_url',
        'site_web',
        'is_active',
        'discovery_source',
        'nbr_personne',
        'sum'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'title' => 'string',
        'bio' => 'string',
        'rating' => 'string',
        'experience' => 'string',
        'qualification_id' => 'integer',
        'facebook_url' => 'string',
        'instagram_url' => 'string',
        'youtube_url' => 'string',
        'site_web' => 'string',
        'discovery_source' => 'string',
        'is_active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getCustomerAttribute(){
        //$this->hasOne(Customer::class, 'customer_id');
        return Customer::find($this->customer_id);
    }

    public function getCaracteristiquesAttribute(){
        return CoachCaracteristique::where('coach_id', $this->id)->get();
    }

    public function getCoursesAttribute(){
        return Course::where('id', $this->id)->get();
    }

    public function getDiplomeAttribute(){
        return Diplome::where('coach_id', $this->id)->get();
    }

    public function getPortfoliosAttribute(){
        return Portfolio::where('coach_id', $this->id)->get();
    }

    public function getDisciplinesAttribute(){
        $coachDisciplines =  CoachDiscipline::where('coach_id', $this->id)->get();
        $result= [];

        foreach($coachDisciplines as $coachDiscipline){

            $discipline = Discipline::where('id', $coachDiscipline->discipline_id)->first();
            if(!empty($discipline)){
                array_push($result, $discipline);
            }

        }

        return $result;
    }
    public function getCoachDisciplinesAttribute(){
        $coachDisciplines =  CoachDiscipline::where('coach_id', $this->id)->get();
        $result= [];

        foreach($coachDisciplines as $coachDiscipline){

            $discipline = Discipline::where('id', $coachDiscipline->discipline_id)->first();
            if(!empty($discipline)){
                array_push($result, $discipline);
            }

        }

        return $result;
    }

    public function courses(){
        return $this->hasMany(Course::class);
    }

    public function portfolios(){
        return $this->hasMany(Portfolio::class);
    }

    public function rating(){
        $r = 0;
        $cs = $this->courses;
        if(!$cs){
            return $r;
        }
        foreach($cs as $c){
            $r+= $c->rating();
        }

        return $r/$cs->count();
    }

    public function ratingView(){
        $m = "<div class='mb-0'>";
        for($i = 0; $i < $this->rating; $i++){
            $m .= "<a><img src='".asset('customer/images/stars.png')."' class='icon'></a>";
        }
        for($i = $this->rating; $i < 5; $i++){
            $m .= "<a><img src='".asset('customer/images/star.png')."' class='icon'></a>";
        }
        $m .= "</div>";

        return $m;
    }

    public function pastCourseCount(){
        $availabities = collect($this->courses()->get())->filter(function($course){
            return $course->isPast() > 0;
        })->count();

        return $availabities;
        // dd($availabities);
        // $availabities = collect($this->courses()->get())->map(function($course){
        //     return $course->availability;
        // })->filter(function($availability, $key){
        //     if(!is_null($availability)){
        //         return $availability->isPast() == true;
        //     }else{
        //         return false;
        //     }
        // });

        // return count($availabities);
    }


    public function goldenPoint(){
        return $this->pastCourseCount() * 2;
    }

    public function reservationLeftCount(){
        $course_ids = collect($this->courses()->get())->map(function($course){
            return $course->id;
        })->toArray();

        return \App\Models\BookingCoach::where([
            'coach_confirm' => 0
        ])->whereIn('course_id', $course_ids)->count();

        // $courses = collect($this->courses()->get())->filter(function($course, $key){
        //     if(!is_null($course->availability)){
        //         return !$course->availability->isPast();
        //     }else{
        //         return false;
        //     }
        // });

        // $total = collect($courses)->reduce(function($carry, $course){
        //     return $carry + $course->bookings()->where('coach_confirm', 0)->count();
        // }, 0);

        // return $total;
    }

    public function distinctions(){
        return $this->hasMany(distinction::class, 'coach_id', 'id');
    }

    public function reviews(){
        $courses = $this->courses()->get();
        $course_ids = collect($courses)->map(function($course){
            return $course->id;
        });

        $reviews = \App\Models\Review::all();
        $course_reviews = collect($reviews)->map(function($review){
            if(!is_null($review->course()->first())){
                return $review;
            }
            return null;
        })->reject(function($review){
            return is_null($review);
        })->toArray();

        $my_review_course = collect($course_reviews)->filter(function($review) use ($course_ids){
            return in_array($review->course()->first()->id,  $course_ids);
        })->toArray();

        return $my_review_course;
    }

    public function canAddPortfolio(){
        return $this->portfolios()->count() <= 5 ;
    }
}
