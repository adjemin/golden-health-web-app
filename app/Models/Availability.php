<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Availability
 * @package App\Models
 * @version July 29, 2020, 5:59 pm UTC
 *
 * @property string $date_debut
 * @property string $date_fin
 * @property string $day_time
 * @property string $hour
 * @property integer $coach_id
 */
class Availability extends Model
{
    use SoftDeletes;

    public $table = 'availabilities';


    protected $dates = ['deleted_at'];

    //protected $appends = ['course'];

    public $fillable = [
        'start_time',
        'end_time',
        'date_debut',
        'date_fin',
        'coach_id',
        'course_id',
        'is_selected',
        'is_active',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'start_time' => 'string',
        'end_time' => 'string',
        'coach_id' => 'integer',
        'course_id' => 'integer',
        'is_active' => 'boolean',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function availabity(){
        return $this->hasMany(\App\CustomerAvailabilities::class, 'availabities_id', 'id');
    }

    public function course(){
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }

    /*public function getCourseAttribute(){
        return \App\Models\Course::where(['id' => $this->course_id])->first();
    }*/

    public function isPast(){
        return \Carbon\Carbon::now() > \Carbon\Carbon::parse($this->date_fin)->addDay();
    }
}
