<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Seance
 * @package App\Models
 * @version July 30, 2020, 7:18 am UTC
 *
 * @property integer $discipline_id
 * @property integer $coach_id
 * @property string $duration
 */
class Seance extends Model
{
    use SoftDeletes;

    public $table = 'seances';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'discipline_id',
        'coach_id',
        'duration',
        'bilan',
        'objectif_long_terme',
        'objectif_court_terme',
        'time_hour_by_day',
        'week_duration'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'discipline_id' => 'integer',
        'coach_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
