<?php

namespace App\Models;

use App\CartItem;
use App\Color;
use Carbon\Carbon;
use App\ShoppingCart;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class Product
 * @package App\Models
 * @version July 30, 2020, 6:38 am UTC
 *
 * @property string $title
 * @property string $description
 * @property string $description_metadata
 * @property string $price
 * @property string $original_price
 * @property string $fees
 * @property string $currency_slug
 * @property string $old_price
 * @property boolean $has_promo
 * @property string $promo_percentage
 * @property string $medias
 * @property boolean $is_sold
 * @property string $sold_at
 * @property string $initiale_count
 * @property string $published_at
 * @property string $is_published
 * @property string $slug
 * @property string $link
 * @property string $delivery_services
 */
class Product extends Model
{
    use SoftDeletes;

    public $table = 'products';


    protected $dates = ['deleted_at'];





    public $fillable = [
        'title',
        'description',
        'description_metadata',
        'price',
        'original_price',
        'fees',
        'currency_slug',
        'old_price',
        'has_promo',
        'promo_percentage',
        'medias',
        'is_sold',
        'sold_at',
        'initial_count',
        'published_at',
        'is_published',
        'stock',
        'slug',
        'link',
        'delivery_services',
        'partner_id',
        'colors',
        'sizes',
        'partner_id',
        'gender'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'description_metadata' => 'string',
        'price' => 'string',
        'original_price' => 'string',
        'fees' => 'string',
        'currency_slug' => 'string',
        'old_price' => 'string',
        'has_promo' => 'boolean',
        'promo_percentage' => 'string',
        'medias' => 'string',
        'is_sold' => 'boolean',
        'initial_count' => 'integer',
        'published_at' => 'string',
        'is_published' => 'string',
        'slug' => 'string',
        'link' => 'string',
        'delivery_services' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function categories(){
        return $this->hasMany(ProductCategory::class);
    }

    public function getCategories(){
        return $this->belongsToMany(Category::class, 'product_categories');
    }

    public function cartItems()
    {
        return $this->hasMany(CartItem::class);
    }

    public function wishlistingCustomers(){
        return $this->belongsToMany(Customer::class, 'wishlists');
    }
    public function favoritedCustomers(){
        return $this->belongsToMany(Customer::class, 'favorite_products');
    }



    public function isCurrentlyInShoppingCart()
    {

        $r = false;

        $p = ShoppingCart::ShoppingCart()->items->where('product_id', $this->id)->first();
        if($p){
            $r = true;
        }

        return $r;
    }

    public function currentCartItem()
    {
        $r = null;
        if($this->isCurrentlyInShoppingCart()){
            $r = ShoppingCart::ShoppingCart()->items->where('product_id', $this->id)->first();
        }
        return $r;
    }

    public function partner(){
        return $this->belongsTo(Partner::class, 'partner_id', 'id');
    }

    public function hasColors(){
        return $this->getProductColor()->count() > 0;
    }

    public function getProductColor() {
        $colorData = Collection::make([]);
        $colors = new ParameterBag(json_decode($this->colors));
        foreach ($colors as $color) {
            if ($colors != null && count($colors) > 0) {
                $colorCode = Color::where(['id' => $color])->first();
                $colorData = $colorData->push($colorCode);
            }
        }
        return $colorData;
    }

    public function hasSizes(){
        return $this->sizes()->count() > 0;
    }


    public function sizes(){
        $sizesCollection = Collection::make([]);
        $rawSizes = preg_split("/,/", $this->sizes);
        for($i = 0; $i < sizeof($rawSizes); $i++){
            if($rawSizes[$i] != ""){
                if (strlen($rawSizes[$i]) > 0) {
                    $sizesCollection->push($rawSizes[$i]);
                }
            }
        }
        return $sizesCollection;
    }

    public function uniqueCartItem($size, $color){
        $cartItem = null;

        if($this->isCurrentlyInShoppingCart()){

            if(isset($size)){
                if(isset($color)){
                    $cartItem = ShoppingCart::ShoppingCart()->items->where('product_id', $this->id)->where('size', $size)->where('color', $color)->first();
                }else{
                    $cartItem = ShoppingCart::ShoppingCart()->items->where('product_id', $this->id)->where('size', $size)->first();
                }
            } else{
                if (isset($color)) {
                    $cartItem = ShoppingCart::ShoppingCart()->items->where('product_id', $this->id)->where('color', $color)->first();
                }
            }
        }
        return $cartItem;
    }


    //
    public function getSimilarProducts()
    {

        $categories = $this->getCategories;

        $similar_products = new Collection([]);

        if($categories->count() == 0){
            $similar_products = Product::inRandomOrder()->where('id', '!=', $this->id)->orWhere('initial_count', '>', 0)->orWhere('stock', '>', 0)->orWhere('sold_at', '>', \Carbon\Carbon::today()->format('Y-m-d'))->limit(6)->get();
        }else{

            // dd($categories);

            foreach($categories as $category){
                // dd(Product::all());
                $producCategories = ProductCategory::where('category_id', $category->id)->get();
                
                $products = collect($producCategories)->map(function($productCategory){
                    return $productCategory->product;
                })->reject(function($product) {
                    return null == $product || $this->id == $product->id || in_array($product->initial_count, [0]) || in_array($product->initial_count, [0]) || (!is_null($product->sold_at) && $product->sold_at < \Carbon\Carbon::today()->format('Y-m-d'));
                })->unique();

                foreach($products as $product){
                    $similar_products->push(
                        $product
                    );
                }
                // dd($similar_products);

                // foreach($products as $product){
                //     $product = Product::where('id', $productCategory->product_id)->where('id', '!=', $this->id)->orWhere('initial_count', '>', 0)->orWhere('stock', '>', 0)->orWhere('sold_at', '>', \Carbon\Carbon::today()->format('Y-m-d'))->first();
                //     if($product){
                //         $similar_products = $similar_products->push(
                //             $product
                //         );
                //     }
                // }
            }

            if($similar_products->count() > 6){
                $similar_products = $similar_products->random(6);
            }
            // dd($similar_products);
        }

        if($similar_products->count() == 0){
            $similar_products = Product::inRandomOrder()->limit(6)->get();
        }
        return $similar_products;
    }

    public function isPromoted(){
        $promo = false;

        if($this->has_promo){
            if($this->promo_percentage != null){
                if ($this->promo_percentage > 0) {
                    $promo = true;
                }
            }
        }

        return $promo;
    }

    public function getPrice(){

        // $price = $this->price;
        $price = intval($this->original_price);
        if($this->has_promo){
            if($this->promo_percentage != null){
                $price = $price - ($price * (int)$this->promo_percentage)/100;
            }
        }

        return $price;
    }

    public function hasMultipleImages(){
        return json_decode($this->medias) != null;
    }

    public function getImageUrl(){
        return json_decode($this->medias)[0] ?? $this->medias;
    }
    public function getImagesUrl(){
        if($this->hasMultipleImages()){
            return json_decode($this->medias);
        };
    }

    public function imageData(){
        if($this->hasMultipleImages()){
            $imageData = new Collection([]);
            foreach(json_decode($this->medias) as $image){
                $imageData = $imageData->push($image);
            }
            return $imageData;
        };
    }

    public function getImagesWidget(){
        $view = "<div>";
        if(is_array(json_decode($this->medias))){
            foreach (json_decode($this->medias) as $image) {
                $view.= "<img class='img-fluid zoomable' src='$image' width='90' class='img-fluid'/>";
            }
            $view .= "</div>";
        } else {
            $view = "<img class='img-fluid zoomable' src='$this->medias' width='90' />";
        }
        return $view;
    }
    public function getOtherImagesWidget(){
        $view = "<div>";
        if (is_array(json_decode($this->medias))) {
            for ($i = 1; $i < sizeof(json_decode($this->medias)); $i++) {
                $view.= "
                <div class='col' style='max-height:30px;'>
                    <img src='".json_decode($this->medias)[$i]."' class='img-fluid' />
                </div>
                ";
            }
            $view .= "</div>";
        } else {
            $view = "<img class='img-fluid zoomable' src='$this->medias' height='90' />";
        }
        return $view;
    }

    public function wishlistWidget(){
        $login_and_action = route('customer.redirectTo', ['url' => "/customer/wishlist/toggle/".$this->id]);
        $url = "/customer/wishlist/toggle/".$this->id;
        $icon = 'favorite_border';
        $text = 'Garder pour plus tard';



        $customer = Auth::guard('customer')->user();

        if(!$customer){

            $widget = "
                <a href='".$login_and_action."' class='m-w-150 text-secondary px-2'>
                    <i class='material-icons'>$icon</i>
                        $text
                </a>
            ";
            return $widget;
        }

        if($this->wishlistingCustomers->where('id', $customer->id)->first()){
            $icon = 'favorite';
            $text = 'Enregistré dans la liste de souhaits';
        }

        $widget = "
            <a href='".$url."' class='m-w-150 text-secondary px-2'>
                <i class='material-icons'>$icon</i>
                    $text
            </a>
        ";

        return $widget;
    }

    public function favoriteWidget(){
        $widget = "
            <a href='".route('customer.redirectTo', ['url' => "/customer/wishlist/add/".$this->id])."' class='m-w-150 text-secondary px-2'>
                <i class='material-icons'>playlist_add</i>
                    Garder pour plus tard
            </a>
        ";

        $customer = Auth::guard('customer')->user();

        if(!$customer){
            $widget = "
                <a href='' class=' m-w-150 text-secondary px-2'>
                    Garder pour plus tard
                </a>
            ";
        }

        // if()

        return $widget;
    }

    // reviews and rating

    public function reviews(){
        // return $this->hasMany(Review::class, );
        return Review::latest()->where('service', "shop")->where('source', "product")->where('source_id', $this->id)->get();
    }

    public function isReviewed(){
        // returns if current user has rated this product
        $customer = Auth::guard('customer')->user();

        if(!$customer){
            return null;
        }
        return $this->reviews()->where('customer_id', $customer->id)->first() != null;
    }

    // gets 1 - 5 rating number
    public function getRating(){
        $rating = 0;
        foreach($this->reviews() as $review){
            $rating += $review->rating;
        }
        if($this->reviews()->count() > 0){
            $rating /= $this->reviews()->count();
        }
        return $rating;
    }

    public function starRatingWidget(){
        $rating = floor($this->getRating());
        if($rating == 0){
            return "";
        }

        $widget = "<div class='row justify-content-center my-2 star_rating star-rating-sm'>";
        for($i = 0; $i < $rating; $i++){
            $widget .="
            <img src='".asset('customer/images/stars.png')."' class='icon mr-2'>";
        }
        for($i = $rating; $i < 5; $i++){
            $widget .="
            <img src='".asset('customer/images/star.png')."' class='icon mr-2'>";
        }
        $widget .= "</div>";
        return $widget;
    }


}