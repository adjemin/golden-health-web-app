<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CustomerNotification
 * @package App\Models
 * @version October 20, 2020, 6:50 pm UTC
 *
 * @property string $title
 * @property string $title_en
 * @property string $subtitle
 * @property string $subtitle_en
 * @property string $action
 * @property string $action_by
 * @property string $meta_data
 * @property string $type_notification
 * @property boolean $is_read
 * @property boolean $is_received
 * @property string $data
 * @property integer $user_id
 * @property integer $data_id
 */
class CustomerNotification extends Model
{
    use SoftDeletes;

    public const COACHING = "coaching";
    public const EVENT = "event";
    public const SHOP = "shop";




    public $table = 'customer_notifications';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'title',
        'title_en',
        'subtitle',
        'subtitle_en',
        'action',
        'action_by',
        'meta_data',
        'type_notification',
        'is_read',
        'is_received',
        'data',
        'user_id',
        'data_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'title_en' => 'string',
        'subtitle' => 'string',
        'subtitle_en' => 'string',
        'action' => 'string',
        'action_by' => 'string',
        'meta_data' => 'string',
        'type_notification' => 'string',
        'is_read' => 'boolean',
        'is_received' => 'boolean',
        'data' => 'string',
        'user_id' => 'integer',
        'data_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

}
