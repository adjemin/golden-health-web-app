<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Service
 * @package App\Models
 * @version July 29, 2020, 8:39 pm UTC
 *
 * @property string $name
 * @property string $name_en
 * @property string $slug
 * @property string $description
 * @property string $description_en
 * @property string $logo
 * @property string $price
 * @property string $price_en
 * @property string $price_description
 * @property string $price_description_en
 * @property boolean $price_is_variable
 * @property string $currency
 * @property string $delay
 * @property string $delay_en
 * @property string $delay_time
 * @property string $delay_time_unit
 * @property integer $rank
 */
class Service extends Model
{
    use SoftDeletes;

    public $table = 'services';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'name_en',
        'slug',
        'description',
        'description_en',
        'logo',
        'price',
        'price_en',
        'price_description',
        'price_description_en',
        'price_is_variable',
        'currency',
        'delay',
        'delay_en',
        'delay_time',
        'delay_time_unit',
        'rank'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'name_en' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'description_en' => 'string',
        'logo' => 'string',
        'price' => 'string',
        'price_en' => 'string',
        'price_description' => 'string',
        'price_description_en' => 'string',
        'price_is_variable' => 'boolean',
        'currency' => 'string',
        'rank' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
