<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Note
 * @package App\Models
 * @version July 30, 2020, 6:46 am UTC
 *
 * @property integer $customer_id
 * @property integer $order_id
 * @property string $message
 * @property integer $rate
 */
class Note extends Model
{
    use SoftDeletes;

    public $table = 'notes';
    

    protected $dates = ['deleted_at'];

    protected $appends = ['customer'];

    public $fillable = [
        'customer_id',
        'course_id',
        'message',
        'rate'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'course_id' => 'integer',
        'message' => 'string',
        'rate' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getCustomerAttribute(){
        return \App\Models\Customer::where('id', $this->customer_id)->first();
    }

    
}
