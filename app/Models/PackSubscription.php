<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PackSubscription
 * @package App\Models
 * @version July 29, 2020, 6:26 pm UTC
 *
 * @property integer $pack_id
 * @property integer $customer_id
 * @property integer $coach_id
 * @property boolean $is_active
 */
class PackSubscription extends Model
{
    use SoftDeletes;

    public $table = 'pack_subscriptions';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'pack_id',
        'customer_id',
        'course_id',
        'discipline_id',
        'coach_id',
        'is_active',
        'session_days',
        'sessions_completed',
        'sessions_waiting',
        'sessions_failed',
        'is_completed'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pack_id' => 'integer',
        'customer_id' => 'integer',
        'course_id'=> 'integer',
        'discipline_id'=> 'integer',
        'coach_id'=> 'integer',
        'is_active'=> 'boolean',
        'session_days'=> 'string',
        'sessions_completed'=> 'string',
        'sessions_waiting'=> 'string',
        'sessions_failed'=> 'string',
        'is_completed' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
