<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Advert
 * @package App\Models
 * @version October 12, 2020, 5:09 pm UTC
 *
 * @property string $name
 * @property string $cover_url
 * @property string $description
 * @property string $redirect_to
 * @property integer $priority
 * @property integer $partner_id
 */
class Advert extends Model
{
    use SoftDeletes;

    public $table = 'adverts';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'cover_url',
        'description',
        'redirect_to',
        'priority',
        'partner_id',
        'file_type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'cover_url' => 'string',
        'description' => 'string',
        'redirect_to' => 'string',
        'priority' => 'integer',
        'partner_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function partner(){
        return $this->belongsTo(Partner::class, 'partner_id', 'id');
    }
}
