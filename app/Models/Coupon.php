<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Coupon
 * @package App\Models
 * @version October 19, 2020, 12:10 pm UTC
 *
 * @property string $titre
 * @property string $code
 * @property integer $porcentage
 * @property integer $nbr_person
 * @property string $date_fin
 * @property string $image
 */
class Coupon extends Model
{
    use SoftDeletes;

    public $table = 'coupons';


    protected $dates = ['deleted_at'];

    public $fillable = [
        'titre',
        'code',
        'pourcentage',
        'nbr_person',
        'date_deb',
        'date_fin',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'titre' => 'string',
        'code' => 'string',
        'pourcentage' => 'integer',
        'nbr_person' => 'integer',
        'date_fin' => 'date',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'titre' => 'required',
        'code' => 'required',
        'pourcentage' => 'required'
    ];

    public function is_expired(){
        if(isset($this->date_fin) || is_null($this->date_fin)){
            if(\Carbon\Carbon::now() < \Carbon\Carbon::parse($this->date_fin)){
                return false;
            }
        }

        if(isset($this->nbr_person) || is_null($this->nbr_person)){
            if($this->nbr_person == 0 ){
                return false;
            }
        }

        return true;
    }

    public function is_expired_text(){
        return $this->is_expired() ? "<span class='text-success'>Active</span>" : "<span class='text-danger'>Expiré</span>";
    }

    public function order(){
        return $this->belongsToMany(Order::class, 'coupon_orders');
    }
}
