<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class distinction
 * @package App\Models
 * @version October 14, 2020, 4:55 pm UTC
 *
 * @property string $name
 * @property string $date_debut
 * @property string $date_fin
 * @property string $fichier_url
 */
class distinction extends Model
{
    use SoftDeletes;

    public $table = 'distinctions';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'date_debut',
        'date_fin',
        'fichier_url',
        'coach_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'date_debut' => 'date',
        'date_fin' => 'date',
        'fichier_url' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'date_debut' => 'required',
        'date_fin' => 'required'
    ];

    
}
