<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseType extends Model
{
    //

    public $table = 'course_types';


    public $fillable = [
        'name',
        'slug',
        'has_location',
        'is_display_planning'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'slug' => 'string',
        'name' => 'string',
        'has_location' => 'boolean',
        'is_display_planning' => 'boolean'
    ];

}
