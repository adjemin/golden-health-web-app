<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Invoice
 * @package App\Models
 * @version July 29, 2020, 8:11 pm UTC
 *
 * @property integer $order_id
 * @property integer $customer_id
 * @property string $reference
 * @property string $service
 * @property string $link
 * @property string $subtotal
 * @property string $tax
 * @property string $fees_delivery
 * @property string $total
 * @property string $status
 * @property boolean $is_paid_by_customer
 * @property boolean $is_paid_by_delivery_service
 * @property string $currency_code
 */
class Invoice extends Model
{
    use SoftDeletes;

    public $table = 'invoices';

    const TAXES = 0.0;
    protected $dates = ['deleted_at'];

    protected $appends = ['order'];



    public $fillable = [
        'order_id',
        'customer_id',
        'reference',
        'service',
        'link',
        'subtotal',
        'tax',
        'discount',
        'fees_delivery',
        'total',
        'status',
        'is_paid_by_customer',
        'is_paid_by_delivery_service',
        'currency_code'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'customer_id' => 'integer',
        'reference' => 'string',
        'service' => 'string',
        'link' => 'string',
        'subtotal' => 'string',
        'tax' => 'string',
        'discount' => 'string',
        'fees_delivery' => 'string',
        'total' => 'string',
        'status' => 'string',
        'is_paid_by_customer' => 'boolean',
        'is_paid_by_delivery_service' => 'boolean',
        'currency_code' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public static function generateID($service, $orderId, $customerId){
        //get last record
        $record = Invoice::count() + 1;

        return $service.'-'.$record.'-'.$orderId.'-'.$customerId.'-'.time();
    }

    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
        # code...
    }

    public function getOrderAttribute(){
        return Order::where('id', $this->order_id)->first();
    }

}
