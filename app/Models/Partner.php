<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Partner
 * @package App\Models
 * @version August 18, 2020, 10:49 am UTC
 *
 * @property string $name
 * @property string $email
 * @property string $dial_code
 * @property string $phone_number
 * @property string $phone
 * @property string $facebook_id
 * @property string $twitter_id
 * @property string $youtube_id
 * @property string $instagram
 * @property string $website
 * @property integer $service_type
 * @property boolean $newsletter
 * @property string $photo_url
 * @property string $gender
 */
class Partner extends Model
{
    use SoftDeletes;

    public $table = 'partners';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'email',
        'dial_code',
        'phone_number',
        'phone',
        'facebook_id',
        'twitter_id',
        'youtube_id',
        'instagram',
        'website',
        'service_type',
        'newsletter',
        'photo_url',
        'gender',
        'discovery_source',
        'description',
        'service_type_other'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'dial_code' => 'string',
        'phone_number' => 'string',
        'phone' => 'string',
        'facebook_id' => 'string',
        'twitter_id' => 'string',
        'youtube_id' => 'string',
        'instagram' => 'string',
        'website' => 'string',
        'service_type' => 'integer',
        'newsletter' => 'boolean',
        'photo_url' => 'string',
        'gender' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function typeService(){
        switch ($this->service_type) {
            case 1:
                return 'Publicité/Marketing';
                break;
            case 2:
                return 'Vente de produits';
                break;

            default:
                return 'Autres';
                break;
        }
    }
}
