<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Program
 * @package App\Models
 * @version October 20, 2020, 12:48 pm UTC
 *
 * @property string $title
 * @property string $cover
 * @property string $description
 */
class Program extends Model
{
    use SoftDeletes;

    public $table = 'programs';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug',
        'cover',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'cover' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
