<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Review
 * @package App\Models
 * @version October 27, 2020, 5:18 pm UTC
 *
 * @property unsignedBigInteger $customer_id
 * @property integer $rating
 * @property string $title
 * @property string $content
 * @property string $service
 * @property string $source
 * @property unsignedBigInteger $source_id
 */
class Review extends Model
{
    use SoftDeletes;

    public $table = 'reviews';
    protected $appends = ['customer'];


    protected $dates = ['deleted_at'];



    public $fillable = [
        'customer_id',
        'rating',
        'title',
        'content',
        'service',
        'source',
        'source_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'rating' => 'double',
        'title' => 'string',
        'content' => 'string',
        'service' => 'string',
        'source' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


    public function getCustomerAttribute(){
        return Customer::find($this->customer_id);
    }

    public function customer(){
        return $this->belongsTo(Customer::class);
    }

    public function product(){
        return $this->belongsTo(Product::class, 'source_id', 'id');
    }

    public function course(){
        if($this->service == 'booking'){
            return $this->belongsTo(Course::class, 'source_id', 'id');
        }
    }

    public function produit(){
        if($this->service == 'shop'){
            return Product::where('id', $this->source_id)->first();
        }
    }

    // public function coach(){
    //     if($this->service == 'booking'){
    //         return $this->belongsTo(Customer::class);
    //     }
    // }

    public function ratingWidget(){
        if($this->rating == null){
            return "";
        }

        $widget = "<div class='d-none row justify-content-center my-2 star_rating star-rating-sm'>";
        for($i = 0; $i < $this->rating; $i++){
            $widget.="
            <img src='".asset('customer/images/stars.png')."' class='icon mr-2'>";

        }
        for($i = $this->rating; $i < 5; $i++){
            $widget.="
            <img src='".asset('customer/images/star.png')."' class='icon mr-2'>";
        }
        $widget .="</div>";

        return $widget;
    }
}
