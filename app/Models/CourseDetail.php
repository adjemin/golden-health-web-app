<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class courseDetails
 * @package App\Models
 * @version October 5, 2020, 8:32 pm UTC
 *
 * @property integer $course_id
 * @property string $experience
 * @property string $seance_type
 * @property boolean $is_diplome
 * @property boolean $is_formation
 */
class CourseDetail extends Model
{
    use SoftDeletes;

    public $table = 'course_details';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'course_id',
        'experience',
        'seance_type',
        'is_diplome',
        'diplome_id',
        'diplome_name',
        'diplome_url',
        'nb_person',
        'is_formation',
        'has_particulier',
        'has_collectif'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'course_id' => 'integer',
        'experience' => 'string',
        'seance_type' => 'string',
        'is_diplome' => 'boolean',
        'is_formation' => 'boolean',
        'has_particulier' => 'boolean',
        'has_collectif' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'course_id' => 'required'
    ];


}
