<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Address
 * @package App\Models
 * @version July 29, 2020, 7:53 pm UTC
 *
 * @property string $longitude
 * @property string $latitude
 * @property string $address_name
 * @property integer $customer_id
 * @property integer $coach_id
 * @property string $place
 * @property string $type_building
 * @property string $photo
 */
class Address extends Model
{
    use SoftDeletes;

    public $table = 'addresses';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'longitude',
        'latitude',
        'address_name',
        'customer_id',
        'coach_id',
        'place',
        'type_building',
        'photo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'longitude' => 'string',
        'latitude' => 'string',
        'address_name' => 'string',
        'customer_id' => 'integer',
        'coach_id' => 'integer',
        'place' => 'string',
        'type_building' => 'string',
        'photo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
