<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pack
 * @package App\Models
 * @version July 29, 2020, 6:20 pm UTC
 *
 * @property string $slug
 * @property string $name
 * @property string $description
 * @property string $percentage_discount
 */
class Pack extends Model
{
    use SoftDeletes;

    public $table = 'packs';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'slug',
        'name',
        'description',
        'percentage_discount'

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'slug' => 'string',
        'name' => 'string',
        'description' => 'string',
        'percentage_discount' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
