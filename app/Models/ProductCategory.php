<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductCategory
 * @package App\Models
 * @version October 6, 2020, 9:44 am UTC
 *
 * @property integer $product_id
 * @property integer $category_id
 */
class ProductCategory extends Model
{
    use SoftDeletes;

    public $table = 'product_categories';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'product_id',
        'category_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'category_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'product_id' => 'required',
        'category_id' => 'required'
    ];

    public function categorie(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function product(){
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
