<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Videotheque
 * @package App\Models
 * @version October 19, 2020, 12:32 pm UTC
 *
 * @property string $titre
 * @property string $description
 * @property string $link_youtube	string
 * @property string $video
 */
class Videotheque extends Model
{
    use SoftDeletes;

    public $table = 'videotheques';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'titre',
        'description',
        'link_youtube',
        'video'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'titre' => 'string',
        'description' => 'string',
        'link_youtube' => 'string',
        'video' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'titre' => 'required'
    ];

    
}
