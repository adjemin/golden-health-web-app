<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CustomerDevice
 * @package App\Models
 * @version October 20, 2020, 6:41 pm UTC
 *
 * @property integer $customer_id
 * @property string $firebase_id
 * @property string $device_id
 * @property string $device_model
 * @property string $device_os
 * @property string $device_os_version
 * @property string $device_model_type
 * @property string $device_meta_data
 */
class CustomerDevice extends Model
{
    use SoftDeletes;

    public $table = 'customer_devices';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'customer_id',
        'firebase_id',
        'device_id',
        'device_model',
        'device_os',
        'device_os_version',
        'device_model_type',
        'device_meta_data'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'firebase_id' => 'string',
        'device_id' => 'string',
        'device_model' => 'string',
        'device_os' => 'string',
        'device_os_version' => 'string',
        'device_model_type' => 'string',
        'device_meta_data' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
