<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Slide
 * @package App\Models
 * @version October 20, 2020, 3:38 pm UTC
 *
 * @property string $titre
 * @property string $subtitle
 * @property string $image
 * @property string $page
 */
class Slide extends Model
{
    use SoftDeletes;

    public $table = 'slides';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'titre',
        'subtitle',
        'image',
        'page'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'titre' => 'string',
        'subtitle' => 'string',
        'image' => 'string',
        'page' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        // 'titre' => 'required',
        'image' => 'required|image',
        'page' => 'required'
    ];

    
}
