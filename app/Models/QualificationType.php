<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class QualificationType
 * @package App\Models
 * @version July 30, 2020, 3:53 am UTC
 *
 * @property string $name
 * @property string $slug
 * @property string $abreviation
 */
class QualificationType extends Model
{
    use SoftDeletes;

    public $table = 'qualification_types';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'slug',
        'abreviation'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'abreviation' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
