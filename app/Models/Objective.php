<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Objective
 * @package App\Models
 * @version July 30, 2020, 7:06 am UTC
 *
 * @property integer $course_id
 * @property string $principal_obj
 * @property string $listsecond_obj
 * @property string $description
 */
class Objective extends Model
{
    use SoftDeletes;

    public $table = 'objectives';


    protected $dates = ['deleted_at'];



    public $fillable = [
      'name',
      'slug',
      'description',
      'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
