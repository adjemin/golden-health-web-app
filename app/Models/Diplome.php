<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Diplome
 * @package App\Models
 * @version October 20, 2020, 4:07 pm UTC
 *
 * @property string $name
 * @property string $image_url
 * @property integer $customer_id
 */
class Diplome extends Model
{
    use SoftDeletes;

    public $table = 'diplomes';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'diplome_name',
        'name',
        'image_url',
        'coach_id',
        'course_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'image_url' => 'string',
        'coach_id' => 'integer',
        'course_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
