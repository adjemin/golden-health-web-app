<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BookingCoach
 * @package App\Models
 * @version October 6, 2020, 2:01 am UTC
 *
 * @property integer $course_id
 * @property integer $customer_id
 * @property integer $pack_id
 * @property integer $nb_personne
 * @property string $amount
 * @property string $booking_ref
 * @property integer $course_lieu_id
 * @property string $course_lieu_note
 * @property string $address_name
 * @property string $address_lat
 * @property string $address_lon
 * @property integer $quantity
 * @property boolean $coach_confirm
 * @property integer $status
 * @property string $status_name
 */
class BookingCoach extends Model
{
    use SoftDeletes;

    public $table = 'booking_coaches';


    protected $dates = ['deleted_at'];

    protected $appends = ['course', 'pack', 'customer', 'course_lieu'];

    const CREATED = "created"; //1
    const PAID = "paid"; //2
    const CONFIRMED = "confirmed"; //3
    const REJECTED = "completed"; //4
    const CANCELED = "canceled"; //5

    const CREATED_STATUS_ID = 1;
    const PAID_STATUS_ID = 2;
    const CONFIRMED_STATUS_ID = 3;
    const REJECTED_STATUS_ID = 4;
    const CANCELED_STATUS_ID = 5;



    public $fillable = [
        'course_id',
        'customer_id',
        'pack_id',
        'nb_personne',
        'amount',
        'booking_ref',
        'booking_date',
        'booking_time',
        'course_lieu_id',
        'course_lieu_note',
        'address_name',
        'address_lat',
        'address_lon',
        'address_cpl',
        'quantity',
        'coach_confirm',
        'status',
        'status_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'course_id' => 'integer',
        'customer_id' => 'integer',
        'pack_id' => 'integer',
        'nb_personne' => 'integer',
        'amount' => 'string',
        'booking_ref' => 'string',
        'course_lieu_id' => 'integer',
        'course_lieu_note' => 'string',
        'address_name' => 'string',
        'address_lat' => 'string',
        'address_lon' => 'string',
        'quantity' => 'integer',
        'status' => 'integer',
        'status_name' => 'string',
        'coach_confirm' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
      //  'course_id' => 'required',
      //  'customer_id' => 'required',
      //  'pack_id' => 'required'
    ];

    public function course()
    {
        return $this->hasOne('App\Models\Course', 'id', 'course_id');
    }

    public function pack()
    {
        return $this->hasOne('App\Models\Pack', 'id', 'pack_id');
    }

    public function getCourseAttribute(){
        return \App\Models\Course::where(['id' => $this->course_id])->first();
    }

    public function getPackAttribute(){
        return \App\Models\Pack::where(['id' => $this->pack_id])->first();
    }

    public function getCustomerAttribute(){
        return \App\Models\Customer::where(['id' => $this->customer_id])->first();
    }

    public function getCourseLieuAttribute(){
        return \App\Models\CourseLieu::where(['id' => $this->course_lieu_id])->first();
    }

    public function orderItems(){
        return $this->hasOne(OrderItem::class, 'meta_data_id', 'id');
    }

    public function order(){
        return $this->orderItems->order;
    }

    public function isPaid()
    {
        // $orderItem = OrderItem::where('meta_data_id', $this->id)->first();
        // if (!empty($orderItem)) {
        //   $order = Order::where('id', $orderItem->order_id)->first();
        //   if (!empty($order)) {
        //     $invoice = Invoice::where('order_id', $order->id)->first();
        //     if (!empty($invoice) && $invoice->status == 'paid') {
        //       return true;
        //     } else {
        //       return false;
        //     }
        //   } else {
        //     return false;
        //   }
        // } else {
        //   return false;
        // }

        $result = false;

        $orderItem = OrderItem::where('meta_data_id', $this->id)->first();
        if ($orderItem) {
            $order = Order::where('id', $orderItem->order_id)->first();
            if ($order) {
                $invoice = Invoice::where('order_id', $order->id)->first();
                if ($invoice && $invoice->status == 'paid') {
                    $result = true;
                }
            }
        }
        return $result;

    }


    public function pastBooking(){
        return \Carbon\Carbon::yesterday()  < \Carbon\Carbon::today();
    }

}
