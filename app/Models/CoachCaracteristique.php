<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CoachCaracteristique
 * @package App\Models
 * @version September 2, 2020, 4:29 pm UTC
 *
 * @property string $diplome
 * @property string $image
 * @property string $description
 */
class CoachCaracteristique extends Model
{
    use SoftDeletes;

    public $table = 'coach_caracteristiques';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'image',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'image' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

}
