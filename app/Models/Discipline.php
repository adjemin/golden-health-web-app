<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Discipline
 * @package App\Models
 * @version July 29, 2020, 6:04 pm UTC
 *
 * @property string $name
 * @property string $slug
 * @property string $description
 */
class Discipline extends Model
{
    use SoftDeletes;

    public $table = 'disciplines';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'image',
        'slug',
        'description',
        'icon'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'image' => 'string',
        'name' => 'string',
        'slug' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
}
