<?php

namespace App\Models;

use App\CartItem;
use App\Color;
use Carbon\Carbon;
use App\ShoppingCart;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use Symfony\Component\HttpFoundation\ParameterBag;

class ProductReservation extends Model
{
    use SoftDeletes;

    public $table = 'product_reservation';


    protected $dates = ['deleted_at'];

    protected $appends = ['product'];


    public $fillable = [
        'product_id',
        'description',
        'product_quantity',
        'customer_name',
        'customer_email',
        'customer_phone'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'product_quantity' => 'integer',
        'customer_name' => 'string',
        'customer_email' => 'string',
        'customer_phone' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function product(){
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function getProductAttribute(){
        return Product::find($this->product_id);
    }

}
