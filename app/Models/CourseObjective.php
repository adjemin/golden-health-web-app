<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CourseObjective
 * @package App\Models
 * @version October 5, 2020, 9:08 pm UTC
 *
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property boolean $active
 */
class CourseObjective extends Model
{
    use SoftDeletes;

    public $table = 'course_objectives';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'course_id',
        'objective_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'course_id' => 'integer',
        'objective_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'course_id' => 'required',
        'objective_id' => 'required'
    ];

    public function objective()
    {
        return $this->hasOne('App\Models\Objective', 'id', 'objective_id');
    }

    public function course()  {
      return $this->belongsTo('App\Models\Course');
    }


}
