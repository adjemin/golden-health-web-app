<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class courseLieu
 * @package App\Models
 * @version October 5, 2020, 8:35 pm UTC
 *
 * @property integer $course_id
 * @property integer $lieu_course_id
 */
class CourseLieu extends Model
{
    use SoftDeletes;

    public $table = 'course_lieus';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'course_id',
        'lieu_course_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'course_id' => 'integer',
        'lieu_course_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'course_id' => 'required',
        'lieu_course_id' => 'required'
    ];

    public function lieuCourse()
    {
        return $this->hasOne('App\Models\LieuCourse', 'id', 'lieu_course_id');
    }

    public function course()  {
      return $this->belongsTo('App\Models\Course');
    }

}
