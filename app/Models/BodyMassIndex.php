<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BodyMassIndex
 * @package App\Models
 * @version July 29, 2020, 7:17 pm UTC
 *
 * @property string $date
 * @property number $value_BMI
 */
class BodyMassIndex extends Model
{
    use SoftDeletes;

    public $table = 'body_mass_indices';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'customer_id',
        'date',
        'value_BMI'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'value_BMI' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
