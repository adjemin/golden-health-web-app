<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Connexion
 * @package App\Models
 * @version August 18, 2020, 11:18 am UTC
 *
 * @property string $username
 * @property string $email
 * @property string $phone
 * @property string $dial_code
 * @property string $phone_number
 * @property string $password
 */
class Connexion extends Model
{
    use SoftDeletes;

    public $table = 'connexions';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'username',
        'email',
        'phone',
        'dial_code',
        'phone_number',
        'password'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'username' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'dial_code' => 'string',
        'phone_number' => 'string',
        'password' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
