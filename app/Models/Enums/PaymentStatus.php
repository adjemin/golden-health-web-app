<?php

namespace App\Models\Enums;

class PaymentStatus
{
    use HasEnums;

    const PENDING = 'pending';
    const SUCCEEDED = 'succeeded';
    const FAILED = 'failed';
}
