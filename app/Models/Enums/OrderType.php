<?php

namespace App\Models\Enums;

class OrderType
{
    use HasEnums;

    const ACCOUNT_CUSTOMER = 'account_customer';
}
