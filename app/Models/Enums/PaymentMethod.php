<?php

namespace App\Models\Enums;

class PaymentMethod
{
    use HasEnums;

    const ADJEMINPAY = 'adjeminpay';
    const CREDIT_CARD = 'credit_card';
}
