<?php

namespace App\Models\Enums;

class OrderStatus
{
    use HasEnums;

    const PENDING = 'pending';
    const PAID = 'paid';
    const REFUNDED = 'refunded';
    const CANCELLED = 'cancelled';
}
