<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Qualification
 * @package App\Models
 * @version July 30, 2020, 4:00 am UTC
 *
 * @property string $period
 * @property integer $qualifi_id
 * @property string $description
 */
class Qualification extends Model
{
    use SoftDeletes;

    public $table = 'qualifications';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'period',
        'qualifi_id',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'period' => 'string',
        'qualifi_id' => 'integer',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
