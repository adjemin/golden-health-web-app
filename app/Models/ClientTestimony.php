<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ClientTestimony
 * @package App\Models
 * @version October 20, 2020, 11:51 am UTC
 *
 * @property string $client_name
 * @property string $client_photo_url
 * @property string $client_feedback
 * @property integer $client_rating
 */
class ClientTestimony extends Model
{
    use SoftDeletes;

    public $table = 'client_testimonies';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'client_name',
        'client_photo_url',
        'client_feedback',
        'client_rating'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'client_name' => 'string',
        'client_photo_url' => 'string',
        'client_feedback' => 'string',
        'client_rating' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function stars(){
        $m = "";
        for($i = 0; $i < $this->client_rating; $i++){
            $m .= "<img src='".asset('customer/images/stars.png')."' class='icon'/>";
        }
        for($i = $this->client_rating; $i < 5; $i++){
            $m .= "<img src='".asset('customer/images/star.png')."' class='icon'/>";
        }
        $m .= "";


        return $m;
    }
}