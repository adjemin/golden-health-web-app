<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Advice
 * @package App\Models
 * @version July 30, 2020, 6:50 am UTC
 *
 * @property integer $customer_id
 * @property integer $coach_id
 * @property string $message
 * @property integer $rate
 */
class Advice extends Model
{
    use SoftDeletes;

    public $table = 'advice';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'customer_id',
        'coach_id',
        'message',
        'rate'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'coach_id' => 'integer',
        'message' => 'string',
        'rate' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
