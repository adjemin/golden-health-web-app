<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrderItem
 * @package App\Models
 * @version September 16, 2020, 5:57 am UTC
 *
 * @property integer $order_id
 * @property string $service_slug
 * @property integer $meta_data_id
 * @property string $meta_data
 * @property integer $quantity
 * @property string $unit_price
 * @property string $total_amount
 * @property string $currency_code
 * @property string $currency_name
 * @property string $days
 */
class OrderItem extends Model
{
    use SoftDeletes;

    public $table = 'order_items';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'order_id',
        'service_slug',
        'meta_data_id',
        'meta_data',
        'quantity',
        'unit_price',
        'total_amount',
        'currency_code',
        'currency_name',
        'color',
        'color_choice',
        'size',
        'size_choice'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'service_slug' => 'string',
        'meta_data_id' => 'integer',
        'meta_data' => 'string',
        'quantity' => 'integer',
        'unit_price' => 'string',
        'total_amount' => 'string',
        'currency_code' => 'string',
        'currency_name' => 'string',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'meta_data_id');
    }

    public function order()
    {
        return $this->hasOne(Order::class, 'id', 'order_id');
    }


}
