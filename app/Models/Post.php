<?php

namespace App\Models;

use App\User;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Post
 * @package App\Models
 * @version September 15, 2020, 6:51 pm UTC
 *
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property string $cover
 * @property string $status
 * @property boolean $is_published
 * @property integer $author_id
 * @property integer $category_id
 * @property string $shared_link
 */
class Post extends Model
{
    use SoftDeletes;

    public $table = 'posts';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'description',
        'slug',
        'cover',
        'status',
        'is_published',
        'author_id', // User Id
        'category_id',
        'shared_link'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'slug' => 'string',
        'cover' => 'string',
        'status' => 'string',
        'is_published' => 'boolean',
        'author_id' => 'integer',
        'category_id' => 'integer',
        'shared_link' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getCategoryAttribute() {
        return Category::find($this->category_id);
    }

    public function getAuthorAttribute() {
        return User::find($this->author_id);
    }

    public function category() {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
    
}
