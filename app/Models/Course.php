<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * Class Course
 * @package App\Models
 * @version July 29, 2020, 8:27 pm UTC
 *
 * @property integer $pack_id
 * @property string $title
 * @property string $person_number
 * @property string $description
 * @property string $price
 * @property string $type_course
 */
class Course extends Model
{
    use SoftDeletes;

    public $table = 'courses';


    protected $dates = ['deleted_at'];

    protected $appends = ['course_type_object', 'discipline', 'detail', 'coach', 'lieus', 'objectives', "availabilities","notes", ];



    public $fillable = [
        'coach_id',
        'discipline_id',
        'course_type',
        'price',
        'currency_code',
        'currency_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'coach_id' => 'integer',
        'discipline_id' => 'integer',
        'course_type' => 'string',
        'price' => 'string',
        'currency_code' => 'string',
        'currency_name' => 'string',
        'is_active' => 'boolean',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function discipline()
    {
        return $this->hasOne('App\Models\Discipline', 'id', 'discipline_id');
    }

    public function coach()
    {
        return $this->hasOne('App\Models\Coach', 'id', 'coach_id');
    }

    public function detail()
    {
        return $this->belongsTo('App\Models\CourseDetail', 'id', 'course_id');
    }

    public function lieu()
    {
        return $this->hasMany('App\Models\CourseLieu');
    }

    public function courseObjective()
    {
        return $this->hasMany('App\Models\CourseObjective');
    }

    public function getCourseTypeObjectAttribute(){

        return CourseType::where(['slug' => $this->course_type])->first();

    }

    public function getDisciplineAttribute(){
        return \App\Models\Discipline::where('id', $this->discipline_id)->first();
    }

    public function getCoachAttribute(){
        return \App\Models\Coach::where('id',$this->coach_id)->first();
    }


    public function getDetailAttribute(){
        return \App\Models\CourseDetail::where('course_id' , $this->id)->first();
    }

    public function getAvailabilitiesAttribute(){
        return \App\Models\Availability::where([
            'course_id' => $this->id,
            'is_active' => 1,
        ])->where('date_fin', '>=', \Carbon\Carbon::today()->format('Y-m-d'))->get();
    }

    public function getBooking(){
        return \App\Models\BookingCoach::where([
            'course_id' =>  $this->id,
            'coach_confirm' => 1
        ])->whereIn('status_name', ['paid', 'completed', 'confirmed']);
    }

    public function isPast(){
        return $this->getBooking()->where('status_name','completed')->count() == $this->getBooking()->count() && $this->getBooking()->count() > 0;
        // $countCompleted = collect($this->getAvailabilitiesAttribute())->filter(function($availability, $key){
        //     return $availability->status = 4;
        // })->count();

        // dd($countCompleted);
    }

    public function getLieusAttribute(){
        $result  =  \App\Models\CourseLieu::where('course_id' , $this->id)->get();

        $ids = collect($result)->map(function($user){
            return $user->lieu_course_id;
        });

        $courseLieuList = \App\Models\LieuCourse::whereIn('id', $ids)->get();

        return $courseLieuList;
    }

    public function getObjectivesAttribute(){
        $result  =  \App\Models\CourseObjective::where('course_id' , $this->id)->get();

        $ids = collect($result)->map(function($user){
            return $user->objective_id;
        });

        $objectiveList = \App\Models\Objective::whereIn('id', $ids)->get();

        return $objectiveList;
    }

    public function getNotesAttribute(){
        
        return \App\Models\Note::where('course_id' , $this->id)->get();
        
    }


    public function hasCurrentCustomerParticipated(){
        $customer = Auth::guard('customer')->user();
        if(!is_null($customer)){
            $bc = BookingCoach::where('course_id', $this->id)->where('customer_id', $customer->id)->where('coach_confirm', true)->first();

            return $bc ? true : false;
        }else{
            return false;
        }
    }


    public function rating(){
        $r = 0;
        $c = 0;
        $ns = Note::where('course_id', $this->id)->get();
        foreach ($ns as $n) {
            $r += $n->rate;
            $c ++;
        }
        //

        return $c == 0 ? 0 : $r/$c;
    }

    public function ratingView(){
        $m = "<div class='mb-4'>";
        for($i = 0; $i < $this->rating(); $i++){
            $m .= "<a><img src='".asset('customer/images/stars.png')."' class='icon' alt=''></a>";
        }
        for($i = 0; $i < 5-$this->rating(); $i++){
            $m .= "<a><img src='".asset('customer/images/star.png')."' class='icon' alt=''></a>";
        }
        $m .= "</div>";

        return $m;
    }

    public function ratingActiveView(){
        $m = "<div class='mb-4'>";
        for($i = 0; $i < $this->rating(); $i++){
            $m .= "<a href='/course/$this->id/rate/$i'><img src='".asset('customer/images/stars.png')."' class='icon' alt=''></a>";
        }
        for($i = $this->rating(); $i < 5; $i++){
            $m .= "<a href='/course/$this->id/rate/$i'><img src='".asset('customer/images/star.png')."' class='icon' alt=''></a>";
        }
        $m .= "</div>";
        return $m;
    }


    public function availability(){
        return $this->hasOne(Availability::class, 'course_id', 'id');
    }

    public function bookings(){
        return $this->hasMany(BookingCoach::class, 'course_id', 'id');
    }

    public function courseToReview(){
        $customer = Auth::guard('customer')->user();
        if(!$customer){
            return null;
        }

        $ids = collect($this->getReviewsAttribute())->map(function($review){
            return $review->source_id;
        })->unique()->toArray();

        if(!in_array($this->id,$ids ?? [])){
            return true;
        }

        return false;
    }

    public function getReviewsAttribute(){
        $customer = Auth::guard('customer')->user();
        return Review::where('customer_id', $customer->id)->where('source', 'course')->get();
    }
}
