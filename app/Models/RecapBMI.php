<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RecapBMI
 * @package App\Models
 * @version July 29, 2020, 7:33 pm UTC
 *
 * @property string $description
 * @property string $interval
 * @property string $color
 * @property number $weigth
 * @property number $height
 * @property integer $customer_id
 */
class RecapBMI extends Model
{
    use SoftDeletes;

    public $table = 'recap_b_m_is';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'description',
        'interval',
        'color',
        'weigth',
        'height',
        'customer_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'description' => 'string',
        'interval' => 'string',
        'color' => 'string',
        'weigth' => 'double',
        'height' => 'double',
        'customer_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
