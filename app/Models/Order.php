<?php

namespace App\Models;

use App\EventTicket;
// use Illuminate\Database\Eloquent\Model;
use App\OrderHistory;
use Eloquent as Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Order
 * @package App\Models
 * @version July 29, 2020, 7:02 pm UTC
 *
 * @property integer $customer_id
 * @property boolean $status
 * @property number $amount
 * @property string $currency_code
 * @property string $payment_method_slug
 * @property number $delivery_fees
 * @property boolean $is_waiting
 * @property string $delivery_date
 * @property boolean $is_delivery
 */
class Order extends Model
{
    use SoftDeletes;

    public $table = 'orders';


    const WAITING = "waiting";
    const CONFIRMED = "confirmed";
    const EDITED = "edited";
    const REFUSED = "refused";
    const DELIVERED = "delivered";
    const FAILED = "failed";
    const CANCELED = "canceled";
    const NOTED = "noted";
    const REPORTED = "reported";
    const WAITING_CUSTOMER_PICKUP = "waiting_customer_pickup";
    const WAITING_CUSTOMER_DELIVERY = "waiting_customer_delivery";
    const CUSTOMER_PAID = "customer_paid";
    const HAS_SELLER_PAID = "has_seller_paid";


    protected $dates = ['deleted_at'];

    protected $appends = ['items', 'histories'];



    public $fillable = [
        'customer_id',
        'status',
        'amount',
        'currency_code',
        'currency_name',
        'payement_method_slug',
        'delivery_fees',
        'is_waiting',
        'delivery_date',
        'is_delivery',
        'service_slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'status' => 'string',
        'amount' => 'string',
        'currency_code' => 'string',
        'currency_name' => 'string',
        'payment_method_slug' => 'string',
        'delivery_fees' => 'string',
        'is_waiting' => 'boolean',
        'is_delivery' => 'boolean',
        'service_slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function invoice(){
        return $this->hasOne(Invoice::class);
    }

    public function getCustomerAttribute(){
        //$this->hasOne(Customer::class, 'customer_id');
        return Customer::where('id', $this->customer_id)->first();
    }

    public function getPaymentMethod()
    {
        $taskStatus = Order::where('payment_method_slug', $this->payement_method_slug)->first();
        switch ($this->payement_method_slug) {
            case "cash":
                return "CASH";
                break;
            case "online":
                return "EN LIGNE";
                break;
            default:
                break;
        }
        return $this->payement_method_slug;
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }
    // Shop products from current order
    public function products(){

        $products = new Collection([]);
        if($this->service_slug == "shop"){
            foreach($this->orderItems as $orderItem){
                $products = $products->push($orderItem->product);
            }
        }
        return $products;
    }
    // Shop products with no reviews from current customer
    public function productsToReview(){
        $customer = Auth::guard('customer')->user();
        if(!$customer){
            return null;
        }
        $productsToReview = new Collection([]);

        foreach($this->products() as $product){
            if(!$product->isReviewed()){
                $productsToReview = $productsToReview->push($product);
            }
        }
        return $productsToReview;
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function getItemsAttribute(){
        return OrderItem::where('order_id', $this->id)->get();
    }
    public function getHistoriesAttribute(){
        return OrderHistory::where('order_id', $this->id)->get();
    }

    public function statusBadge(){
        $b = '';
        $m = "";
        switch($this->status){
            case 'waiting':
                $b = ' text-primary';
                $m = 'En Attente';
            break;
            case 'paid':
                $b = 'success';
                $m = 'Payé';
            break;
            case 'customer_paid':
                $b = 'success';
                $m = 'Payé';
            break;
            default:
                $b = 'info';
                $m = $this->status;
            break;
        }
        return "<span class='badge badge-$b p-3 font-weight-bold'>$m</span>";
    }

    // coupon functions
    public function coupons(){
        return $this->belongsToMany(Coupon::class, 'coupon_orders')->withPivot('amount_saved');
    }

    public function couponsTotalDiscount(){
        $total = 0;
        foreach($this->coupons as $coupon){
            $total += $coupon->amount_saved;
        }
        return $total;
    }

    public function tickets(){
        $tickets = new Collection([]);
        if($this->service_slug == "event"){

            foreach($this->orderItems as $orderItem){
                $ticket = EventTicket::find($orderItem->meta_data_id);
                if($ticket){
                    $tickets = $tickets->push($ticket);
                }
            }
        }
        return $tickets;
    }
}
