<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Customer
 * @package App\Models
 * @version July 29, 2020, 9:31 am UTC
 *
 * @property string $last_name
 * @property string $first_name
 * @property string $name
 * @property string $dial_code
 * @property string $phone_number
 * @property string $phone
 * @property boolean $is_phone_verifed
 * @property string $phone_verifed_at
 * @property boolean $is_active
 * @property string $activation_date
 * @property string $email
 * @property string $photo_url
 * @property string $gender
 * @property string $bio
 * @property string $birthday
 * @property string $birth_location
 * @property string $language
 * @property string $location_address
 * @property string $location_latitude
 * @property string $location_longitude
 * @property string $location_gps
 * @property string $password
 */
class Customer extends Model
{
    use SoftDeletes, Geographical;

    public $table = 'customers';


    protected $dates = ['deleted_at'];

    protected static $kilometers = true;

    const LATITUDE  = 'location_latitude';
    const LONGITUDE = 'location_longitude';

    public $fillable = [
        'last_name',
        'first_name',
        'name',
        'dial_code',
        'phone_number',
        'phone',
        'is_phone_verifed',
        'phone_verifed_at',
        'is_active',
        'activation_date',
        'email',
        'photo_url',
        'gender',
        'size',
        'weight',
        'bio',
        'birthday',
        'birth_location',
        'language',
        'location_address',
        'location_latitude',
        'location_longitude',
        'location_gps',
        'password',
        'is_cgu',
        'is_newsletter',
        'type_account',
        'facebook_id',
        'google_id',
        'email_verifed_at',
        'experience',
        'seance_price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'last_name' => 'string',
        'first_name' => 'string',
        'name' => 'string',
        'dial_code' => 'string',
        'phone_number' => 'string',
        'phone' => 'string',
        'is_phone_verifed' => 'boolean',
        'is_active' => 'boolean',
        'email' => 'string',
        'photo_url' => 'string',
        'gender' => 'string',
        'bio' => 'string',
        'birth_location' => 'string',
        'language' => 'string',
        'location_address' => 'string',
        'location_latitude' => 'string',
        'location_longitude' => 'string',
        'location_gps' => 'string',
        'password' => 'string',
        'size'  => 'double',
        'weight' => 'double',
        'type_account' => 'string',
        'facebook_id' => 'string',
        'google_id' => 'string',
        'experience' => 'string',
        'seance_price' => 'integer',
    ];

    protected $appends = [
        'coach'
    ];
    /**
     * Validation rules
     *
     * @var array
     */

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function notifications()
    {
        return $this->hasMany(CustomerNotifications::class, 'user_id', 'id')->latest();
    }

    public function booking()
    {
        return $this->hasMany(BookingCoach::class, 'customer_id', 'id')->latest();
    }


    public function getCoachAttribute(){
        return Coach::where('customer_id', $this->id)->first();
    }

    public function coaches()
    {
        $courses = collect($this->booking()->get())->map(function ($book) {
            if (!is_null($book)) {
                return $book->course;
            }
        })->reject(function ($name) {
            return empty($name);
        })->unique();

        $coaches = collect($courses)->map(function ($course) {
            if (!is_null($course)) {
                return $course->coach;
            }
        })->reject(function ($name) {
            return empty($name);
        })->unique();

        return $coaches;
    }

    public function coupons()
    {
        // return $this->hasMany(CouponCustomer::class, 'customer_id', 'id')->latest();

        // dd($this->invoices());

        $order_id =  collect($this->invoices()->get())->map(function ($invoice) {
            return $invoice->invoice->order->id;
        })->reject(function ($order_id) {
            return empty($order_id);
        })->unique();

        $coupons = \App\CouponOrder::whereIn('order_id', $order_id)->get();

        return $coupons;
    }

    public function packs()
    {
        $packs = collect($this->booking()->get())->map(function ($book) {
            if (!is_null($book)) {
                return $book->pack;
            }
        })->reject(function ($name) {
            return empty($name);
        })->unique();

        return $packs;
    }

    public function invoices()
    {
        return $this->hasMany(InvoicePayment::class, 'creator_id', 'id');
    }

    public static $rules = [];
}
