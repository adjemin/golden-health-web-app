<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="CustomerNotifications",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="title_en",
 *          description="title_en",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="subtitle",
 *          description="subtitle",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="subtitle_en",
 *          description="subtitle_en",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="action",
 *          description="action",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="action_by",
 *          description="action_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="meta_data",
 *          description="meta_data",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="type_notification",
 *          description="type_notification",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_read",
 *          description="is_read",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="is_received",
 *          description="is_received",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="data",
 *          description="data",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="data_id",
 *          description="data_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class CustomerNotifications extends Model
{
    use SoftDeletes;

    public const COACHING = "coaching";
    public const EVENT = "event";
    public const SHOP = "shop";

    public $table = 'customer_notifications';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'title_en',
        'subtitle',
        'subtitle_en',
        'action',
        'action_by',
        'meta_data',
        'type_notification',
        'is_read',
        'is_received',
        'data',
        'user_id',
        'data_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'title_en' => 'string',
        'subtitle' => 'string',
        'subtitle_en' => 'string',
        'action' => 'string',
        'action_by' => 'string',
        'meta_data' => 'string',
        'type_notification' => 'string',
        'is_read' => 'boolean',
        'is_received' => 'boolean',
        'data' => 'string',
        'user_id' => 'integer',
        'data_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}