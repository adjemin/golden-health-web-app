@extends("customer_frontOffice.layouts_customer.master_customer")


@section('css')

<style>
    body {
        background-image: linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, .6)), url("customer/images/bg2.jpg");
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        position: relative;
    }

    a {
        color: black;
    }

    .login-box .text-center{
        font-weight: bold;
    }

    .login-box p.text-center{
        font-size: 15px !important;
    }

    .p--divider {
    position: relative;
    text-align: center;
    margin: 13px 0 0 0;
    }

    .p--divider::after {
    position: absolute;
    content: "";
    width: 20%;
    height: 1.5px;
    background-color: #ccc;
    left: 25%;
    top: 50%;
    }

    .p--divider::before {
    position: absolute;
    content: "";
    width: 20%;
    top: 50%;
    right: 25%;
    height: 1.5px;
    background-color: #ccc;
    }

    .login-box form input::placeholder{
        color: black;
        font-weight: 500;
    }

</style>


@section('content')

<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="offset-md-3 col-lg-7 col-md-7 col-sm-12">
                <div class="login-box">
                    <h2 class="text-center"> <b>Me connecter</b> </h2>
                    <p class="text-center">Pas encore membre Golden health ? <span><a href="/customer/register/self-challenger<?php if(isset($_GET['action'])) { echo '?action='.$_GET['action']; } ?>" class="secondary-text-color">Inscrivez-vous
                                </a></span></p>
                    <div class="row mt-3">
                        {{-- <div class="col-md-6 mt-2">
                            <div class="login-google">
                                <a href="{!! url('auth/google') !!}"> <img src="{{asset ('customer/images/téléchargement.png') }}" width="15px" height="15px" alt="">
                                    &nbsp; &nbsp;
                                    Connexion avec
                                    google</a>
                            </div>
                        </div>
                        <div class="col-md-6 mt-2">
                            <div class="login-facebook">
                                <a href="{!! url('auth/facebook') !!}"> <img src="{{ asset('customer/images/facebook.png') }}" width="15px" height="15px" alt="">&nbsp; Connexion avec facebook</a>
                            </div>
                        </div> --}}
                    </div>
                    {{-- <div class="row mx-auto mt-4">
                        <div class="mini-divider mt-3"></div> &nbsp;&nbsp;
                        <p>ou</p>&nbsp;&nbsp;
                        <div class="mini-divider mt-3"></div>
                    </div> --}}

                    {{-- <div>
                        <p class="p--divider">Connectez-vous</p>
                    </div> --}}

                    <form role="form" method="POST" action="{{ url('/customer/login') }}">
                         {{ csrf_field() }}
                        <input type="hidden" name="action" value="<?php if(isset($_GET['action'])) { echo $_GET['action']; } ?>">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} mt-3">
                            <input type="email" name="email" class="form-control input-style " id="email" placeholder="Adresse email professionnelle" value="{{ old('email') }}"/>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-2">
                            <input type="password" name="password" class="form-control input-style " id="password" placeholder="Mot de passe">
                        </div>
                        <div class="form-group mt-2">
                            <input type="checkbox" name="remember" id="remember"> <strong>Se souvenir de moi</strong>
                        </div>
                        <a href="{{ url('/customer/password/reset') }}" class="password-forget ">J'ai perdu mon mot de passe</a>

                        <input type="submit" class="btn login-button col-lg-12 mt-4" value="Se connecter">
                    </form>

                </div>
            </div>
    </div>
        </div>
</section>

@endsection
