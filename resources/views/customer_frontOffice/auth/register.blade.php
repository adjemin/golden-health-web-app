@extends("customer_frontOffice.layouts_customer.master_customer")


@section('css')

    <style>
        body {
            background-image: linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, .6)), url("customer/images/bg2.jpg");
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            position: relative;
        }

        a {
            color: black;
        }

    </style>

    <link rel="stylesheet" href="https://unpkg.com/flickity@2.2.1/dist/flickity.css">
    <script src="https://unpkg.com/flickity@2.2.1/dist/flickity.pkgd.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

@section('content')

    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7 offset-md-3 col-sm-12">
                    <div class="login-box">
                        <center>
                            <h4> <b>Créer mon compte </b> </h4>
                        </center>
                        <br>
                        <center>
                            <small>Déjà inscrit sur Golden Health ? &nbsp; <span><a href="/customer/login"
                                        class="secondary-text-color">Connectez-vous</a></span></small>
                        </center>
                        <div class="row mt-3">
                            <div class="col-md-6 mt-2">
                                <div class="login-google">
                                    <a href="{!! url('auth/google') !!}"> <img
                                            src="{{ asset('customer/images/téléchargement.png') }}" width="15px"
                                            height="15px" alt="">
                                        &nbsp;
                                        &nbsp;
                                        Connexion avec
                                        google</a>
                                </div>
                            </div>
                            <div class="col-md-6 mt-2">
                                <div class="login-facebook">
                                    <a href="{!! url('auth/facebook') !!}"> <img
                                            src="{{ asset('customer/images/facebook.png') }}" width="15px" height="15px"
                                            alt="">&nbsp; Connexion avec facebook</a>
                                </div>
                            </div>
                        </div>
                        <div class="row mx-auto mt-4">
                            <div class="mini-divider mt-3"></div> &nbsp;&nbsp;
                            <p>ou</p>&nbsp;&nbsp;
                            <div class="mini-divider mt-3"></div>
                        </div>
                        <form role="form" method="POST" action="{{ url('/customer/register') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="action" value="<?php if (isset($_GET['action'])) {
    echo $_GET['action'];
} ?>">
                            @if (isset($data) && $data['facebook_id'])
                                <input type="hidden" name="facebook_id" value="{{ $data['facebook_id'] }}" />
                            @elseif(isset($data) && $data['google_id'])
                                <input type="hidden" name="google_id" value="{{ $data['google_id'] }}" />
                            @endif
                            <div class="row mb-4 ml-1">
                                <div class="form-check mr-4">
                                    <input class="form-check-input" type="radio" name="gender" id="genre-mr" value="mr"
                                        checked>
                                    <label class="form-check-label" for="genre-mr">
                                        M.
                                    </label>
                                </div>
                                <div class="form-check mr-4">
                                    <input class="form-check-input" type="radio" name="gender" id="genre-mme" value="mme">
                                    <label class="form-check-label" for="genre-mme">
                                        Mme
                                    </label>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }} mt-3">
                                <select name="type_account" id="type_account"
                                    class="form-control input-style placeholder-style" onchange="showDiv(this.value)">
                                    <option value="">Je souhaite être ?</option>
                                    <option value="challenger">Self Challenger</option>
                                    <option value="coach">Coach</option>
                                    <option value="partner">Partner</option>
                                </select>
                            </div>
                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }} mt-3">
                                <input type="text" name="first_name" class="form-control input-style placeholder-style"
                                    id="first_name" placeholder="Prénom" value="@if (isset($data) && $data['first_name']) {{ $data['first_name'] }} @else {{ old('first_name') }} @endif" />
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }} mt-3">
                                <input type="text" name="last_name" class="form-control input-style placeholder-style"
                                    id="last_name" placeholder="Nom" value="@if (isset($data) && $data['last_name']) {{ $data['last_name'] }} @else {{ old('last_name') }} @endif" />
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} mt-3">
                                <input type="email" name="email" class="form-control input-style placeholder-style"
                                    id="email" value="@if (isset($data) && $data['email']) {{ $data['email'] }} @else {{ old('email') }} @endif"
                                    placeholder="Adresse e-mail profesionnelle" />
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            {{-- Champs Facultatifs --}}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('commune') ? ' has-error' : '' }} mt-3">
                                        <!-- <label for="">Commune</label> -->
                                        <input type="text" name="commune" class="form-control input-style placeholder-style"
                                            id="commune" value="{{ old('commune') }}"
                                            placeholder="Entrez votre commune" />
                                        @if ($errors->has('commune'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('commune') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('quartier') ? ' has-error' : '' }} mt-3">
                                        <!-- <label for="">Quartier</label> -->
                                        <input type="text" name="quartier"
                                            class="form-control input-style placeholder-style" id="quartier" min="50"
                                            value="{{ old('quartier') }}" placeholder="Entrez votre quartier" />
                                        @if ($errors->has('quartier'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('quartier') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }} mt-3">
                                        <label for="">Date de naissance</label>
                                        <input type="date" name="birthday"
                                            class="form-control input-style placeholder-style" id="birthday"
                                            value="{{ old('birthday') }}" placeholder="Entrez votre taille" />
                                        @if ($errors->has('birthday'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('birthday') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                {{-- <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('target_weight') ? ' has-error' : '' }} mt-3">
                                        <label for="">Entrez votre poids cible</label>
                                        <input type="number" name="target_weight" class="form-control input-style placeholder-style" id="target_weight" value="{{ old('target_weight') }} "placeholder="Poids cible"/>
                                        @if ($errors->has('target_weight'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('target_weight') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div> --}}
                            </div>
                            {{-- / Champs Facultatifs --}}
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} mt-2">
                                <input type="password" name="password" class="form-control input-style placeholder-style"
                                    id="password" aria-describedby="password" placeholder="Mot de passe" />

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} mt-2">
                                <input type="password" name="password_confirmation"
                                    class="form-control input-style placeholder-style" id="password-confirm"
                                    placeholder="Confirmation du Mot de passe" />

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div id="coach" style="display:none">
                                <div class="form-group">
                                    <input type="text" name="birthday" class="form-control input-style placeholder-style"
                                        id="birthday" placeholder="Date de naissance">
                                </div>
                                <!-- Localisation -->
                                <div class="form-group">
                                    <div class="input-group">

                                        <input type="text" name="city" class="form-control" id="input_search"
                                            placeholder="Ville, commune, Quartier">

                                        <input type="hidden" name="lat" id="lat_input">
                                        <input type="hidden" name="lon" id="lon_input">

                                        <div id="match-list"
                                            style="margin: 10px auto; width: 50%;position:absolute; top:70px;right:0">
                                        </div>
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-secondary" type="button"
                                                id="submit_search">RECHERCHER</button>
                                        </div>
                                    </div>
                                </div>

                                <div
                                    class="input-group mb-3 {{ $errors->has('facebook_url_coach') ? ' has-error' : '' }}">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text placeholder-style " id="">https://facebook.com/</span>
                                    </div>
                                    <input type="text" name="facebook_url_coach"
                                        class="form-control input-style placeholder-style" id="facebook_url"
                                        aria-describedby="" placeholder="Page Facebook">
                                    @if ($errors->has('facebook_url_coach'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('facebook_url_coach') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div
                                    class="input-group mb-3 {{ $errors->has('instagram_url_coach') ? ' has-error' : '' }}">
                                    <div class="input-group-prepend ">
                                        <span class="input-group-text placeholder-style"
                                            id="">https://www.instagram.com/</span>
                                    </div>
                                    <input type="text" name="instagram_url_coach"
                                        class="form-control input-style placeholder-style" id="instagram_url"
                                        aria-describedby="" placeholder="Compte Instagram">
                                    @if ($errors->has('instagram_url_coach'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('instagram_url_coach') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div
                                    class="input-group mb-3 {{ $errors->has('youtube_url_coach') ? ' has-error' : '' }}">
                                    <div class="input-group-prepend ">
                                        <span class="input-group-text placeholder-style"
                                            id="">https://www.youtube.com/</span>
                                    </div>
                                    <input type="text" name="youtube_url_coach"
                                        class="form-control input-style placeholder-style" id="youtube_url"
                                        aria-describedby="" placeholder="Compte Youtube">
                                    @if ($errors->has('youtube_url_coach'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('youtube_url_coach') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="input-group mb-3 {{ $errors->has('site_web_coach') ? ' has-error' : '' }}">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text placeholder-style" id="">Site
                                            web</span>
                                    </div>
                                    <input type="text" name="site_web_coach"
                                        class="form-control input-style placeholder-style" id="site_web" aria-describedby=""
                                        placeholder="Site web">
                                    @if ($errors->has('site_web_coach'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('site_web_coach') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <label for="">Comment avez-vous connu Golden Health ? *</label>
                                <div class="input-group mb-3">
                                    <select name="discovery_source" class="custom-select input-style placeholder-style"
                                        id="discovery_source">
                                        <option value=""></option>
                                        <option value="search_engine"> Moteur de recherche (Bing, Google, Yahoo...)</option>
                                        <option value="website"> Site Internet autre qu'un moteur de recherche (Blog,
                                            presse...)</option>
                                        <option value="written-press"> Presse écrite</option>
                                        <option value="social-media"> Réseaux sociaux (Facebook, Instagram...)</option>
                                        <option value="word-of-mouth"> Bouche à oreilles (Amis, famille, collègue...)
                                        </option>
                                        <option value="corporate-lesson"> Cours en entreprise</option>
                                        <option value="subway-display"> Affichage métro</option>
                                        <option value="coach"> Par un coach</option>
                                        <option value="other"> Autre (Salon, évènement...)</option>
                                    </select>
                                </div>
                            </div>
                            <div id="partner" style="display:none">
                                <div class="form-group">
                                    <input type="text" class="form-control input-style placeholder-style"
                                        name="name_partner" id="name_partner" placeholder="Nom de l'entreprise">
                                </div>
                                @if ($errors->has('name_partner'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name_partner') }}</strong>
                                    </span>
                                @endif
                                <div class="form-row">
                                    <div
                                        class="form-group col-md-6 {{ $errors->has('email_entreprise') ? ' has-error' : '' }}">
                                        <input type="email" class="form-control input-style placeholder-style"
                                            name="email_entreprise" id="email_entreprise" placeholder="Email Entreprise">
                                        @if ($errors->has('email_entreprise'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email_entreprise') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div
                                        class="form-group col-md-6 {{ $errors->has('phone_entreprise') ? ' has-error' : '' }}">
                                        <input type="tel" class="form-control input-style placeholder-style"
                                            name="phone_entreprise" id="telephone " placeholder="telephone">
                                        @if ($errors->has('phone_entreprise'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone_entreprise') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="input-group mb-3">
                                    <select name="service_type" id="service_type"
                                        class="custom-select input-style placeholder-style">
                                        <option selected>Type de service</option>
                                        <option value="1">Publicité/Marketing</option>
                                        <option value="2">Vente de produits</option>
                                        <option value="3">Autres</option>
                                    </select>
                                </div>

                                <!-- <div id="div_other_service" style="display:none">
                                        <div class="input-group mb-3">
                                            <input type="text" name="service_type_other" class="form-control input-style placeholder-style" id="service_type_other"  placeholder="Autre type de service">
                                        </div>
                                    </div> -->



                                <div
                                    class="input-group mb-3 {{ $errors->has('facebook_url_partner') ? ' has-error' : '' }}">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text placeholder-style " id="">https://facebook.com/</span>
                                    </div>
                                    <input type="text" class="form-control input-style placeholder-style"
                                        name="facebook_url_partner" id="basic-url" aria-describedby=""
                                        placeholder="Page Facebook">
                                    @if ($errors->has('facebook_url_partner'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('facebook_url_partner') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div
                                    class="input-group mb-3 {{ $errors->has('instagram_url_partner') ? ' has-error' : '' }}">
                                    <div class="input-group-prepend ">
                                        <span class="input-group-text placeholder-style"
                                            id="">https://www.instagram.com/</span>
                                    </div>
                                    <input type="text" class="form-control input-style placeholder-style" id="basic-url"
                                        aria-describedby="" placeholder="Compte Instagram" name="instagram_url_partner">
                                    @if ($errors->has('instagram_url_partner'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('instagram_url_partner') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div
                                    class="input-group mb-3 {{ $errors->has('site_web_partner') ? ' has-error' : '' }}">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text placeholder-style" id="">Site
                                            web</span>
                                    </div>
                                    <input type="text" name="site_web_partner"
                                        class="form-control input-style placeholder-style" id="basic-url"
                                        aria-describedby="" placeholder="Site web">
                                    @if ($errors->has('site_web_partner'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('site_web_partner') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <label for="">Comment avez-vous connu Golden Health ? *</label>
                                <div class="input-group mb-3">
                                    <select class="custom-select input-style placeholder-style" id="inputGroupSelect01">
                                        <option selected>Selectionnez</option>
                                        <option value="1">Facebook</option>
                                        <option value="2">Instagram</option>
                                        <option value="3">Par quelqu'un</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <textarea name="some_text" class="form-control  placeholder-style"
                                        placeholder="Dites nous quelques choses " id="" cols="30" rows="10"></textarea>
                                </div>
                            </div>

                            <div class="form-check">
                                <input class="form-check-input" name="is_cgu" type="checkbox" id="inlineCheckbox1"
                                    value="1" required>
                                <label class="form-check-label" for="inlineCheckbox1">J'accepte les
                                    <a href="/terms-and-conditions" class="secondary-text-color">conditions générales
                                        d'utilisation</a></label>
                            </div>

                            <div class="form-check mt-1">
                                <input class="form-check-input" name="is_newsletter" type="checkbox" id="inlineCheckbox2"
                                    value="1">
                                <label class="form-check-label" for="inlineCheckbox2">J'accepte les
                                    S'inscrire à la newsletter</label>
                            </div>

                            <!--a href="/customer/password/reset" class="password-forget ">J'ai perdu mon mot de passe</a-->
                            {{-- <a href="{{ route('profile') }}" class="btn login-button col-lg-12 mt-4">S'inscrire </a> --}}

                            <input type="submit" class="btn login-button col-lg-12 mt-4" value="S'inscrire  ">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

@push('js')
    <script>
        function showDiv(val) {
            document.getElementById("partner").style.display = 'none'
            document.getElementById("coach").style.display = 'none'

            if (val != "" && val != null && val != "challenger") {
                document.getElementById(val).style.display = 'block'
            }
        }
    </script>
    <script>
        function getLocation(str) {
            value = str.match(/(\d+)(\.\d+)?/g); // trim
            return value;
        }

        //search = document.getElementById("input_search");
        const matchList = document.getElementById("match-list")

        $("#input_search").autocomplete({
            source: function(query, callback) {
                $.getJSON(
                    `https://nominatim.openstreetmap.org/search?q=${query.term}&countrycodes=CI&format=json&addressdetails=1&limit=100`,
                    function(data) {
                        var places = data;
                        places = places.map(place => {
                            console.log("==== place ===");
                            console.log(place);
                            return {
                                title: place.display_name.replace(/<br\/>/g, ", "),
                                value: place.display_name.replace(/<br\/>/g, ", "),
                                lat: place.lat,
                                lon: place.lon,
                                id: place.osm_id
                            };
                        });
                        return callback(places);
                    });
            },
            minLength: 2,
            select: function(event, ui) {
                console.log(ui);
                $('#lat_input').val(ui.item.lat);
                $('#lon_input').val(ui.item.lon);
                console.log("Selected: " + ui.item.title);
            },
            open: function() {
                $('.ui-autocomplete').css('width', '305px'); // HERE
            }
        });

        //Search Address to api
        /* const searchStates = async searchText =>{
             const response = await fetch(`https://nominatim.openstreetmap.org/search?q=${search.value}&countrycodes=CI&format=json&addressdetails=1&limit=100`);
             const states = await response.json();
             // console.log(states);
             let matches = states.filter(state =>{
                 const regex= new RegExp(`^${searchText}`, 'gi');
                 // console.log(state.display_name);
                 return state.display_name.match(regex);
             });
             if(searchText.length === 0){
                 matches = [];
                 matchList.innerHTML = "";
             }
             console.log(matches);
             outputHtml(matches);
         };
         const outputHtml = matches =>{
             if(matches.length > 0){
                 const html = matches.map(
                 (match, i)=>`
             <div class="d-flex justify-content-center card card-body w-150"
                 id='${match.place_id}' style='margin-top :-20px; cursor: pointer;' onmouseover="this.style.backgroundColor='rgba(250,240,230,0.95)';" onmouseout="this.style.backgroundColor='white';"  onclick="startSearch(${match.lat}, ${match.lon})"; >
                     <h6 class="text-primary">
                     <span>${match.display_name}</span>
                     ${(match.address.country_code).toUpperCase()}  <br/>
                     <small>Lat : ${match.lat} /Lon :${match.lon} <small/>
                 </h6>
             </div>`
                 ).join('');
                 console.log(html)
                 matchList.innerHTML = html;
             }
         }
         search.addEventListener('input', ()=>searchStates(search.value));
         function startSearch(lat, lon){
             console.log(lat)
             console.log(lon)
             document.getElementById('lat_input').value = lat
             document.getElementById('lon_input').value = lon
             document.getElementById("search_form").submit()
         }*/
    </script>
@endpush
