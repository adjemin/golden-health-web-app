@extends("customer_frontOffice.layouts_customer.master_customer")
@section('css')
<style>
    body {
        background-color: rgba(238, 238, 238, 0.336);
    }

</style>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>


    <link href="https://fonts.googleapis.com/css2?family=Merienda+One&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('css/intlTelInput.css')}}">

    <link rel="icon" href="{{asset('img/logo.png')}}">
    <link rel="stylesheet" href="{{asset('css/500.css')}}">


    <style>
        .text500{
            font-family: 'Merienda One', cursive;
            font-weight: bold;
            /* color: #2DC068; */
        }
        .text-link{
            text-decoration: underline;
            text-decoration-color: #2DC068;
        }
        input.is-invalid::placeholder{
            color: red;
        }

        input.is-invalid{
            color: red !important;
        }
    </style>
@endsection

@section('content')

<section class="py-5">
    <div class="container">
        <h1 class="font-weight-bold primary-text-color mb-3">Contactez nous </h1>
        <h3>Le renseignement est gratuit ! Allez, dites-nous ce
            que vous aimeriez savoir et notre service client se chargera de vous
            répondre dans un délai de 24h.</h3>
        <!-- <p class="">Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement
            du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé
            dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition
            et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page
        </p> -->
    </div>
</section>
<section>
    <div id="formarea-container" class="container-fluid">
                <div class="container-fluid mt-1" id="form-container">
                    <div class="d-flex justify-content-center">
                        @if (session()->has('success'))
                            <h2 class="text-secondary font-weight-bold text-center">
                                Merci de nous avoir contacté !
                            </h2>
                        @else
                            <h1 class="text-secondary font-weight-bold text-center">
                            </h1>
                        @endif
                    </div>
                    <div class="d-flex justify-content-center">
                        <img src="{{asset('img/logo.png')}}" alt="" class="mx-auto" width="100px" height="auto">
                    </div>
                    @if (session()->has('success'))


                    @else
                        <form action="{{route('contact')}}" method="POST" id="form500">
                            @csrf
                            <div class="container">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                    <input type="text" required class="form-control  @error('name') is-invalid @enderror" name="name" placeholder="Nom" value="{{old('name')}}">
                                        <span class="@error('name') text-danger @enderror">@error('name'){{ $message }}@enderror</span>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="email" required class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{old('email')}}">
                                        <span class="@error('email') text-danger @enderror">@error('email'){{ $message }}@enderror</span>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="text" required class="form-control @error('phone') is-invalid @enderror" name="phone" placeholder="Téléphone" value="{{old('phone')}}">
                                        <span class="@error('phone') text-danger @enderror">@error('phone'){{ $message }}@enderror</span>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="text" required class="form-control @error('subject') is-invalid @enderror" name="subject" placeholder="Sujet" value="{{old('subject')}}">
                                        <span class="@error('subject') text-danger @enderror">@error('subject'){{ $message }}@enderror</span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <textarea name="message" required class="form-control @error('subject') is-invalid @enderror" id="" cols="30" rows="10">{{old('message')}}</textarea>
                                        <span class="@error('message') text-danger @enderror">@error('message'){{ $message }}@enderror</span>
                                    </div>
                                </div>

                            </div>
                                <!-- Submit -->
                                <div class="text-center">
                                    <input type="submit" id="sumbmitform" onclick="" class="btn btn-secondary p-3 my-3 font-weight-bold text-uppercase mt-5" value="Envoyer">
                                </div>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
</section>
@endsection

@section('scripts')
    <script>
        //
        var $input = {
            first_name: $("#first_name"),
            last_name: $("#last_name"),
            email: $("#email"),
            phone_number: $("#phone_number"),
            whatsapp_number: $("#whatsapp_number"),
        }

        $input.phone_number.on('keyup', function(){

            // if($("#dial_code").val() == ''){

            $input.whatsapp_number.val($(this).val())
            // }else{
            //     $input.whatsapp_number.val($("#dial_code").val()+$(this).val())
            // }
        });


        function validateForm(){
            if($input.first_name == null){
                alert('');
            }
            if($input.first_name == null){
                alert('');
            }
            if($input.first_name == null){
                alert('');
            }
            if($input.first_name == null){
                alert('');
            }
        }
        //

    </script>
    <script src="{{asset('js/intlTelInput.js')}}"></script>
    <script>
        var input = document.querySelector("#phone_number");

        var errorMsg = document.querySelector("#error-msg"),
            validMsg = document.querySelector("#valid-msg"),
                form500 = document.querySelector("#form500");


        var errorMap = ["&#65794; Numéro Invalide !", "&#65794; Code de pays invalide !", "&#65794; Numéro de téléphone Trop Court", "&#65794; Numéro de téléphone Trop Long", "&#65794; Numéro Invalide"];
        var iti = window.intlTelInput(input, {
            dropdownContainer: document.body,
            separateDialCode: true,
            initialCountry: "CI",
            geoIpLookup: function(success, failure) {
                $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                success(countryCode);
                });
            },
            placeholderNumberType: "MOBILE",
            preferredCountries: ['ci'],
            utilsScript: "{{asset('js/utils.js?')}}",
        });
        var reset = function() {
            input.classList.remove("error");
            errorMsg.innerHTML = "";
            errorMsg.classList.add("hide");
            validMsg.classList.add("hide");
        };

        // on blur: validate
        input.addEventListener('blur', function() {
            reset();
            if (input.value.trim()) {
                if (iti.isValidNumber()) {
                    $("#dial_code").val(iti.getSelectedCountryData().dialCode);
                    // $("#phone_number").val(iti.);
                    $("#country_code").val(iti.getSelectedCountryData().iso2.toUpperCase());
                    // $()
                    // console
                    if($("#dial_code").val() == ''){
                        $('#whatsapp_number').val("225"+$(this).val())
                    }else{
                        $('#whatsapp_number').val($("#dial_code").val()+$("#phone_number").val())
                    }
                    form500.submit();

                    // $("#whatsapp_number").val($("#dial_code").value+$("#phone_number").value);
                    //console.log(iti.s.phone);
                    validMsg.classList.remove("hide");
                } else {
                    input.classList.add("error");
                    var errorCode = iti.getValidationError();
                    // console.log("Error from intInput");
                    // console.log(errorCode);
                    errorMsg.innerHTML = errorMap[errorCode];
                    errorMsg.classList.remove("hide");
                }
            }
        });

        // on keyup / change flag: reset
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);
    </script>

@endsection
