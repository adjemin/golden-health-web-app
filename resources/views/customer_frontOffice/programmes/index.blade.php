@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
<style>
    .qui-somme-nous {
        line-height: 2.1;
    }

</style>
@endsection
@section('content')

<section>
    <header class="become-partner-header">
        <div class="become-coach-header-des">
            <h3 class="font-weight-bold">Programmes </h3>
            <p>
                Retrouvez vos différents programmes ici
            </p>
        </div>
    </header>
</section>




<section class="py-3 mx-5">
    <div class="container ">
        <h1 class="font-weight-bold primary-text-color mb-5 mt-3">Programmes </h1>

        <br>
        <div class="row">
            @foreach($programs as $program)
            <div class="col-lg-4 col-md-4 pb-2">
                <a href="{{'/programme/'.$program->slug}}">
                    <div class="event-content">
                    <div class="bg-image" style="background-image: url({{$program->cover ?? asset('customer/images/IMAGE.png')}});"></div>
                        {{--<img src="{{$program->cover}}" width="100%" alt="" srcset="">--}}
                        <div class="mx-2 py-3">
                            <h5>{{ $program->title }}</h5>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
            <div class="d-flex justify-content-center mt-3">
                @if(count($programs) > 0)
                    {!! $programs->links() !!}
                @endif
            </div>
            {{--<div class="col-lg-4 col-md-4 pb-2">
                <a href="/programme/esprit-sain-corps-sain/Esprit Sain Corps Sain">
                    <div class="event-content">
                        <img src={{asset ('customer/images/IMAGE.png') }} width="100%" alt="" srcset="">
                        <div class="mx-2 py-3">
                            <h5>Esprit Sain Corps Sain</h5>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-4 col-md-4 pb-2">
                <a href="/programme/transformation-radicale/Transformation Radicale">
                    <div class="event-content">
                        <img src={{asset ('customer/images/IMAGE.png') }} width="100%" alt="" srcset="">
                        <div class="mx-2 py-3">
                            <h5>Transformation Radicale</h5>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-4 col-md-4 pb-2">
                <a href="/programme/sur-mesure/Sur-mesure">
                    <div class="event-content">
                        <img src={{asset ('customer/images/IMAGE.png') }} width="100%" alt="" srcset="">
                        <div class="mx-2 py-3">
                            <h5>Sur-mesure</h5>
                        </div>
                    </div>
                </a>
            </div>--}}

        </div>

    </div>
</section>

@endsection
