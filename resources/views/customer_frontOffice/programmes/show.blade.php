@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
    <style>
        body {
            background: #eee;
        }

    </style>
    <script src="https://cdn.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript"></script>
@endsection

@section('content')
    <main class="container">
        <!--  -->
        <section class="pt-3">
            <div class="">
                <div class="text-center p-md-3 p-2 r bg-white">
                    <h1 class="font-weight-bold text-center primary-text-color mb-3 text-uppercase">{{ $program->title ?? '' }}
                    </h1>
                    <div>
                    </div>
                </div>
            </div>
        </section>

        <section class="my-3">
            {{-- Breadcrumb --}}
            <ul class="breadcrumb">
                <li><a href="/">Accueil</a></li>
                <li><a href="/programmes">Programmes</a></li>
                <li><a>{{ $program->title }}</a></li>
            </ul>
            @csrf
        </section>

        <section class="py-5">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12">
                    <div>
                        <img src="{{ $program->cover ?? '' }}" class="img-fluid rdimg" alt="" srcset="">
                    </div>
                    <h3 class="mt-3 font-weight-bold  text-left secondary-text-color mb-3"></h3>
                    <div class="p-3 r bg-white">
                        <p class="" style="font-size: medium; line-height: 1.8;">
                            {!! $program->description ?? '' !!}
                        </p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 pb-2">
                    <div class="event-content">
                        <div class="p-2">
                            <h4 class="mb-3 font-weight-bold text-primary">Intéressez ?</h4>
                             <div>
                                <a href="{{url('/contact')}}" class="btn-article btn-secondary w-100 mt-3">Nous contacter</a>
                                {{--<img src="{{ asset('customer/images/icons8-calendar-16.png') }}" alt="" width="20px"
                                    height="20px" class="mr-2"><span class="font-weight-bold primary-text-color">Début :
                                </span>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

@endsection
