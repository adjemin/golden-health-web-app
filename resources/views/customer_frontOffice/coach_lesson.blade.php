@extends("customer_frontOffice.layouts_customer.master_customer")


@section('content')

@section('css')

<style>
    body
    {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }
    a
    {
        color: black;
    }

    .start-link-patner:hover {
        padding: 10px 30px;
        color: white;
        font-weight: bold;
        border: none;
        background-color: #edaa0d;
    }

    #wishlist_10136 .fa-toggle-on {
      color: #00B888;
    }
    #wishlist_10136 .fa-toggle-off {
      color: #E74C3C;
    }

</style>
@endsection
<section>
    <header class="profil-header">
        <div class="dashboard-header-des">
            <h3 class="font-weight-bold text-center">Disciplines </h3>
            <p class="text-center">
                Gérer mes disciplines
            </p>
        </div>
    </header>
</section>
<section>
    <div class="container">
        <div class="py-2">
            <div class="courses-makes">
                <div>
                    <p style="font-size: 16px;"> Ajouter les différentes disciplines que vous enseignez. Chaque cours ajouté vous apportera de la visibilité supplémentaire sur la plateforme. Il est conseillé dans les descriptions de cours de préciser toutes les disciplines enseignées afin de démontrer votre polyvalence.</p>
                </div>
            </div>
        </div>

    </div>
</section>


<!-- Heures de pratiques -->
<section class="mb-5">
    <div class="container">
        <div class="py-2">
            <div class="profil-box-content">
                <div class="mb-2">
                    <h5 class="font-weight-bold">Mes cours</h5>
                    @if(count($courses) == 0)
                      <p>Vous n'avez pas encore de cours</p>
                    @endif
                    @include('flash::message')
                </div>
                <div class="mb-3 mt-4">
                    <a href="/customer/lessons/manage" class="start-link-patner">
                        <i class="fa fa-plus"></i>
                        <span class="ml-2">Ajouter un cours</span>
                    </a>
                </div>
                @if($courses && count($courses) > 0)
                <div class="mt-4">
                    <table class="table table-bordered products-sortable">
                        <thead>
                            <tr>
                              {{-- <th>Priorité</th> --}}
                                <th>Etat</th>
                                <th class="name">Nom</th>
                                <th class="hidden-xs">Prix</th>
                                {{--<th class="hidden-xs">Mise à jour</th>--}}
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                           @if($courses)
                                @foreach($courses as $course)
                                    @if($course && $course->discipline)
                                    <tr id="wishlist_10136" data-product-id="{{$course->id}}">
                                        {{-- <td class="handle"> <i class="fa fa-arrows-alt"></i></td> --}}
                                        <td class="status"> @if($course->active == 1) <span class="badge badge-success">Actif</span> @else  <span class="badge badge-danger">Inactif</span> @endif </td>
                                        <td class="name">Coach sportif de {{$course->discipline->name}}</td>
                                        <td class="hidden-xs">{{$course->price}} FCFA</td>
                                        {{--<td class="hidden-xs">{{$course->updated_at}}</td>--}}
                                        <td>
                                            <div class="btn-group">
                                                <a href="/customer/lessons/manage?action=update&course_id={{$course->id}}" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"> </i> Modifier </a>
                                                <a href="#" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown"> <span class="caret"></span> </a>
                                                <ul class="dropdown-menu">
                                                  <li class="px-1">
                                                    {!! Form::open(['route' => ['courseCoaoch.destroy', $course->id], 'method' => 'delete']) !!}
                                                      {!! Form::button('<i class="fa fa-trash"></i> Supprimer', ['type' => 'submit', 'class' => 'btn
                                                      btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                    {!! Form::close() !!}
                                                  </li>
                                                    <li  class="px-1">
                                                        <a href="/customer/lessons/manage/show/{{$course->id}}"> <i class="fa fa-eye"></i> Voir mon cours</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                           @endif
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>

    </div>
</section>

@endsection
