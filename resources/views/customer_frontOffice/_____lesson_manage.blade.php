@extends("customer_frontOffice.layouts_customer.master_customer")

@section('content')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<link href="https://cdn.jsdelivr.net/npm/smartwizard@5/dist/css/smart_wizard_all.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{asset('css/evo-calendar.css')}}">
<link rel="stylesheet" href="{{asset('css/jquery.timepicker.min.css')}}">
<style>
    body
    {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }
    a
    {
        color: black;
    }

    .start-link-patner:hover {
        padding: 10px 30px;
        color: white;
        font-weight: bold;
        border: none;
        background-color: #edaa0d;
    }

    .bootstrap-select .btn-light {
    background-color:transparent ! important;
    border-color:transparent ! important;
    }
    .bootstrap-select .dropdown-toggle .filter-option-inner-inner{
        font-size: small ! important;
    }
    .bootstrap-select .dropdown-menu{
        border-radius: 0px ! important;
        left:-13px ! important;
    }
    .bootstrap-select.show-tick .dropdown-menu li a span.text{
        font-size:small ! important;
    }
</style>
@endsection
<section>
    <div class="container">
        <div class="py-2">
            <div class="courses-makes">
                <div>
                    <p>Les textes à remplir seront ceux affichés sur votre page de profil.</p>
                    <p>
                        Il est donc important de les rédiger de façon claire et en y précisant tous les éléments de plus-value d’une séance de sport avec vous. Exprimez votre professionnalisme et l’ambiance qui dégage de votre cours en étant vous même.
                    </p>
                </div>
            </div>
        </div>

    </div>
</section>
<section class="mb-5">
    <div class="container">
        <div class="py-2">
            <div class="profil-box-content">
                <div class="mb-5">
                        <div id="smartwizard">
                            <ul class="nav">
                               <li>
                                   <a class="nav-link" href="#step-1">
                                      Informations générales
                                   </a>
                               </li>
                               <li>
                                   <a class="nav-link" href="#step-2">
                                    Détail du cours
                                   </a>
                               </li>
                               <li>
                                   <a class="nav-link" href="#step-3">
                                  Calendrier
                                   </a>
                               </li>
                               <li>
                                   <a class="nav-link" href="#step-4">
                                      Prix
                                   </a>
                               </li>
                            </ul>

                            <div class="tab-content">
                               <div id="step-1" class="tab-pane" role="tabpanel">
                                  <div class="mt-4">
                                    <form role="form" id="CourseInfoForm" action="/customer/lessons/store/cart" method="post">
                                      {{ csrf_field() }}
                                      <input type="hidden" name="form_type" id="form_type" value="general">
                                      <div class="form-group mb-3 mt-4">
                                          <p for="">Discipline <span class="required">*</span> </p>
                                          @if (isset($disciplines) || is_null($disciplines))
                                              <select name="discipline_id"  class="custom-select input-style placeholder-style" id="discipline_id" required>
                                                  <option value="">Choisissez votre discipline...</option>
                                                  @foreach ($disciplines as $discipline)
                                                      <option value="{{ $discipline->id }}" <?php if((isset($course) && $course->discipline_id == $discipline->id)) { echo 'selected'; }elseif(isset($cartGeneral) && $cartGeneral['discipline_id'] && $cartGeneral['discipline_id'] == $discipline->id) { echo 'selected'; }?> label="{{ $discipline->name }}">{{ $discipline->name }}</option>
                                                  @endforeach
                                          </select>
                                          @endif
                                          <small class="form-text text-muted">Il permettra de trouver votre cours grâce au moteur de recherche</small >
                                      </div>
                                      <div class="form-group mb-3 mt-4">
                                          <p for="">Votre pratique du sport et votre expérience en tant que Coach</p>
                                          <p><small>
                                          Quelles ont été vos expériences dans ce sport? Avez-vous pratiqué en compétition ? Quels prix avez-vous gagné ? <br/>
                                          Quelle expérience avez-vous en tant qu'entraîneur? À quel niveau avez-vous entraîné ?<br/>
                                          Quels types de public avez vous déjà encadré ?
                                          </small></p>
                                          <textarea class="form-control" name="experience" id="experience" rows="5"><?php if(isset($course) && $course->detail && $course->detail->experience) { echo $course->detail->experience; } elseif(isset($cartGeneral) && $cartGeneral['experience']) { echo $cartGeneral['experience'];}?></textarea>
                                      </div>
                                      <div class="form-group mb-3 mt-4">
                                          <p for="">Ma séance type</p>
                                          <p><small>
                                          Description de ma séance type. <br/>
                                          Qu'est-ce qu'un client peut attendre d'une session typique avec vous? Avez-vous des techniques spécifiques ?<br/>
                                          Avez vous du petit matériel et lequel ? Quelles connaissances techniques vous utilisez ?<br/>
                                          Eviter les raccourcis, ex : Échauffement, corps de séance, retour au calme.
                                          </small></p>
                                          <textarea class="form-control" name="seance_type" id="seance_type" rows="5"><?php if(isset($course) && $course->detail && $course->detail->seance_type) { echo $course->detail->seance_type; } elseif(isset($cartGeneral) && $cartGeneral['seance_type']) { echo $cartGeneral['seance_type'];}?></textarea>
                                      </div>


                                      <div class="toolbar toolbar-bottom" style="text-align: right;">
                                        <button type="button" class="btn btn-info prevbtn" name="button">Précédent</button>
                                        <button type="submit" class="btn btn-info" name="button">Sauvegarder et continuer</button>
                                      </div>
                                    </form>
                                  </div>
                               </div>
                               <div id="step-2" class="tab-pane" role="tabpanel">
                                  <div class="mt-4">
                                    <form role="form" id="CourseDetailForm" action="/customer/lessons/store/cart" method="post">
                                      {{ csrf_field() }}
                                      <input type="hidden" name="form_type" id="form_type" value="detail">
                                      <div class="form-group mb-3 mt-4">
                                          <p for="">Diplôme ?</p>
                                          <div class="form-check form-check-inline">
                                              <input class="form-check-input" type="checkbox" name="diplome" id="diplome" value="1" <?php if(isset($course) && $course->detail && $course->detail->is_diplome == 1) { echo "checked"; }else if(isset($cartDetail) && isset($cartDetail['diplome']) && $cartDetail['diplome'] == 1) { echo "checked"; }?>>
                                              <label class="form-check-label" for="diplome">Diplôme / Certificat </label>
                                          </div>
                                      </div>
                                      <div class="form-group mb-3 mt-4">
                                          <p for="">Le cours a lieu : <span class="required">*</span></p>
                                          @if($lieu_courses)
                                            @foreach($lieu_courses as $key => $lieu)
                                               <?php if (isset($course)) {
                                                 $lieu_v = \App\Models\CourseLieu::where('course_id', $course->id)->where('lieu_course_id', $lieu->id)->first();
                                               }else if(isset($cartDetail) && isset($cartDetail['lieucourse']) && !empty($cartDetail['lieucourse']) && !empty($cartDetail['lieucourse'][$key])) {
                                                 if ($cartDetail['lieucourse'][$key] == $lieu->id) {
                                                   $lieu_v = $lieu;
                                                 }
                                               }else {
                                                 $lieu_v = null;
                                               }?>
                                              <div class="form-check form-check-inline">
                                                  <input class="form-check-input" name="lieucourse[]" type="checkbox" id="lieu-course{{$lieu->id}}" value="{{$lieu->id}}" <?php if(isset($lieu_v) && !empty($lieu_v)) { echo "checked"; }?>>
                                                  <label class="form-check-label" for="lieu-course{{$lieu->id}}">{{$lieu->name}} </label>
                                              </div>
                                            @endforeach
                                          @endif
                                      </div>
                                      <div class="form-group mb-3 mt-4">
                                          <p for="">Objectif principal du cours :</p>
                                          @if($objectives)
                                            @foreach($objectives as $key => $objective)
                                                <?php if (isset($course)) {
                                                  $object_v = \App\Models\CourseObjective::where('course_id', $course->id)->where('objective_id', $objective->id)->first();
                                                }else if(isset($cartDetail) && isset($cartDetail['objectif']) && !empty($cartDetail['objectif']) && !empty($cartDetail['objectif'][$key])) {
                                                  if ($cartDetail['objectif'][$key] == $objective->id) {
                                                    $object_v = $objective;
                                                  }
                                                }else {
                                                  $object_v = null;
                                                }?>
                                              <div class="form-check form-check-inline">
                                              <input class="form-check-input" type="checkbox" name="objectif[]" id="objectif{{$objective->id}}" value="{{$objective->id}}" <?php if(isset($object_v) && $object_v != null) { echo "checked"; }?>>
                                                  <label class="form-check-label" for="objectif{{$objective->id}}">{{$objective->name}}</label>
                                              </div>
                                              @endforeach
                                          @endif
                                      </div>
                                      <div class="form-group mb-3 mt-4">
                                          <p>Type de cours : <span class="required">*</span></p>
                                          @if($type_courses)
                                            @foreach($type_courses as $type)
                                              <div class="form-check form-check-inline">
                                              <input class="form-check-input" name="type_course" type="radio" value="{{$type->slug}}" id="type_course-{{$type->id}}" <?php if(isset($course) && $type->slug && $course->course_type == $type->slug) { echo "checked"; } else if(isset($cartDetail) && isset($cartDetail['type_course']) && $cartDetail['type_course'] == $type->slug) { echo "checked";}?>
                                              >
                                                  <label class="form-check-label" for="type_course-{{$type->id}}">{{$type->name}} </label>
                                              </div>
                                            @endforeach
                                          @endif
                                      </div>

                                      <div class="form-group mb-3 mt-4">
                                        <label for="">Nombre de participant <span class="required">*</span></label>
                                        <input type="number" min="1" class="form-control nb_person" name="nb_person" id="nb_person" value="<?php if(isset($course) && $course->detail && $course->detail->nb_person) { echo $course->detail->nb_person; } elseif(isset($cartDetail) && isset($cartDetail['nb_person'])) {
                                          echo $cartDetail['nb_person'];
                                        } else { echo "1"; }?>"/>
                                      </div>

                                      <div class="toolbar toolbar-bottom" style="text-align: right;">
                                        <button type="button" class="btn btn-info prevbtn" name="button">Précédent</button>
                                        <button type="submit" class="btn btn-info" name="button">Sauvegarder et continuer</button>
                                      </div>
                                    </form>
                                  </div>
                               </div>
                               <div  id="step-3" class="tab-pane" role="tabpanel">
                                    <div class="mt-4">
                                      <form role="form" id="CourseCalendarForm" action="/customer/lessons/store/cart" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="form_type" id="form_type" value="calendar">
                                        <div id="calendar-coach"></div>
                                        <div class="modal fade" id="add-event-popup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                         <div class="modal-dialog">
                                           <div class="modal-content">
                                             <div class="modal-header">
                                               <h5 class="modal-title" id="exampleModalLabel">Ajouter une nouvelle date</h5>
                                               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                 <span aria-hidden="true">&times;</span>
                                               </button>
                                             </div>
                                             <div class="modal-body">
                                                   <div class="form-row">
                                                     <div class="form-group col-md-6">
                                                       <label for="time_start">Heure de début</label>
                                                       <input type="text" class="form-control timePicker" name="time_start" id="time_start">
                                                     </div>
                                                     <div class="form-group col-md-6">
                                                       <label for="time_end">Heure de fin</label>
                                                       <input type="text" class="form-control timePicker" name="time_end" id="time_end">
                                                     </div>
                                                   </div>
                                             </div>
                                             <div class="modal-footer">
                                               <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                               <button type="button" class="btn btn-primary" id="showAddEventPopup">Valider</button>
                                             </div>
                                           </div>
                                         </div>
                                       </div>
                                        <div class="toolbar toolbar-bottom" style="text-align: right;">
                                          <button type="button" class="btn btn-info prevbtn" name="button">Précédent</button>
                                          <button type="submit" class="btn btn-info" name="button">Sauvegarder et continuer</button>
                                        </div>
                                      </form>
                                    </div>
                               </div>
                               <div id="step-4" class="tab-pane" role="tabpanel">
                                  <div class="mt-4">
                                    <form action="{{ url('/customer/lessons/save') }}" method="POST" id="CourseFinishForm">
                                      {{ csrf_field() }}
                                      <?php if(isset($course)) { ?>
                                          <input type="hidden" name="course_id" value="{{$course->id}}">
                                      <?php }?>
                                      <input type="hidden" name="current_coach_id" id="current_coach_id" value="{{$customer->is_coach->id}}">
                                      <input type="hidden" name="form_type" id="form_type" value="fin">
                                      <div class="form-group mt-4">
                                          <p>Prix du cours</p>
                                          <span>Sur une base d'une heure de cours</span>
                                      </div>
                                      <div class="form-group mt-4">
                                          <input id="price" class="form-control" name="price" type="number" min="0" step="1" value="<?php if(isset($course)) { echo $course->price; } elseif(isset($cart) && isset($cart['price'])) { echo $cart['price']; } else { echo '5000';}?>" style="font-weight: bold; border: 1px solid green; color: green;">
                                      </div>
                                      <div class="toolbar toolbar-bottom" style="text-align: right;">
                                        <button type="button" class="btn btn-info prevbtn" name="button">Précédent</button>
                                        <button type="submit" class="btn btn-info" name="submit">Enregistrer</button>
                                      </div>
                                    </form>
                                  </div>
                               </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/smartwizard@5/dist/js/jquery.smartWizard.min.js" type="text/javascript"></script>
<script src="{{asset('js/evo-calendar.js')}}"></script>
<script src="{{asset('js/jquery.timepicker.min.js')}}"></script>
<script src="{{asset('js/calendar-coach.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){

// SmartWizard initialize
$('#smartwizard').smartWizard({
    toolbarSettings: {
      showNextButton: false, // show/hide a Next button
      showPreviousButton: false, // show/hide a Previous button
      toolbarExtraButtons: [] // Extra buttons to show on toolbar, array of jQuery input/buttons elements
    }
});

$(".prevbtn").on("click", function() {
   // Navigate previous
   $('#smartwizard').smartWizard("prev");
   return true;
});

/*$(".nextbtn").on("click", function() {
   // Navigate previous
   $('#smartwizard').smartWizard("next");
   return true;
});*/

$('.nb_person').keyup(function(){
  var radiocheck = $("input[name='type_course']:checked").val();
  if (radiocheck == "semi-collectif") {
    if ($(this).val() > 5) {
      $(this).val(5);
    }
  } else if(radiocheck == "exercice-de-groupe"){
    if ($(this).val() > 10) {
      $(this).val(10);
    }
  }
});

$("input[name=type_course]").change(function () {
        if ($("input[name='type_course']:checked").val() == "semi-collectif") {
          $('#nb_person').val(5);
          $('#nb_person').attr('max', 5);
        } else if($("input[name='type_course']:checked").val() == "exercice-de-groupe"){
          $('#nb_person').val(10);
          $('#nb_person').attr('max', 10);
        } else {
          $('#nb_person').val(1);
        }
    });



    $("#CourseInfoForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url = form.attr('action');
        $('#smartwizard').smartWizard("loader", "show");

        $.ajax({
               type: "POST",
               url: url,
               data: form.serialize(), // serializes the form's elements.
               success: function(data)
               {
                 console.log(data);
                  $('#smartwizard').smartWizard("loader", "hide");
                  // alert(data); // show response from the php script.
                   $('#smartwizard').smartWizard("next");
                   return true;
               }
      });
    });

    $("#CourseDetailForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url = form.attr('action');
        $('#smartwizard').smartWizard("loader", "show");

        $.ajax({
               type: "POST",
               url: url,
               data: form.serialize(), // serializes the form's elements.
               success: function(data)
               {
                 console.log(data);
                  $('#smartwizard').smartWizard("loader", "hide");
                  // alert(data); // show response from the php script.
                   $('#smartwizard').smartWizard("next");
                   return true;
               }
      });
    });


});
</script>
@endsection
