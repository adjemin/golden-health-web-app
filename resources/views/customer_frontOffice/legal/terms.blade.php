@extends("customer_frontOffice.layouts_customer.master_customer")
@section('css')
<style>
    body {
        background-color: rgba(238, 238, 238, 0.336);
    }

</style>
@endsection

@section('content')

<section class="py-5">
    <div class="container">
        <h1 class="font-weight-bold primary-text-color mb-3">Conditions générales d'utilisation de la plateforme GoldenHealth</h1>

        @if(App\Entreprise::Instance()->terms)
        {!!App\Entreprise::Instance()->terms!!}
        @else
            <p class="">Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement
                du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé
                dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition
                et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page
            </p>
        @endif
    </div>
</section>

@endsection
