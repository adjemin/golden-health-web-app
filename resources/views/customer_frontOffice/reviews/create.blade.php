@extends("customer_frontOffice.layouts_customer.master_customer")

@section('content')

@section('css')
<style>
        :root {
            --background: #eee;
            /* --foreground: #fff; */
            --foreground: #d3d3d3;
            --foreground: #fff;
            --foreground-l: #e6e6e6;
            --primary: #1c6a49;
            --primary-dark: #144e36;
            --secondary: #edaa0d;
            --secondary-dark: #cf940a;
            --br: 4px;
        }

        * {
            box-sizing: border-box;
        }

        body {
            /* font-size: 13px; */
            /* font-size: .875rem; */
            font-size: .625rem;
            /* background-color: rgba(247, 247, 247, 0.973); */
            background-color: var(--background);
            font-weight: 500;
        }

        .b-fg {
            background-color: var(--foreground);
        }

        .b-fgl {
            background-color: var(--foreground-l);
        }

        .b-bg {
            background-color: var(--background);
        }

        .breadcrumb {

            background-color: var(--foreground) !important;
        }

        .r {
            border-radius: var(--br);
        }

        .fs-xs {
            font-size: .625rem !important;
        }

        .fs-s {
            font-size: .75rem !important;
        }

        .fs-m,
        p {
            font-size: .875rem !important;
        }

        .fs-l {
            font-size: 1rem !important;
        }

        a {
            font-size: .75rem;
            color: black;
        }

        ,
        a:hover {
            color: currentColor !important;
        }

        main {
            /* padding: 0 15px; */
        }

        .aside-left {
            padding-top: 0;
            padding-right: 0;
        }

        .aside-right {
            padding-top: 0;
            padding-left: 0;
        }

        .container {
            width: 100%;
            padding: 5px 10px;
            margin-right: auto;
            margin-left: auto;
        }

        @media (min-width: 576px) {
            .container {
                max-width: 540px;
            }
        }

        @media (max-width: 576px) {
            .btn-article.invisible {
                visibility: visible;
            }
        }

        @media (min-width: 768px) {
            .container {
                max-width: 720px;
            }
        }

        @media (min-width: 992px) {
            .container {
                max-width: 960px;
                /* max-width: 825px; */
            }
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 1140px;
                /* max-width: 900px; */
            }
        }


        .container-fluid {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        .article-box:nth-child(3n+1) {
            padding-right: 0 !important;
        }

        .article-box:nth-child(3n+2) {
            padding-right: 5px !important;
            padding-left: 5px !important;
        }

        .article-box:nth-child(3n+0) {
            padding-left: 0 !important;
        }

        /* .article-box:nth-child(even){
                background: blue !important;
            } */
        .article-card {
            border-radius: var(--br);
            position: relative;
            padding-bottom: 5px;
            max-height: 500px;
            box-shadow: none;
            border: .5px solid #eee;
        }

        .article-card:hover {
            border: 1px solid #ccc;
            box-shadow: 3px 3px 3px #bbb;
        }

        .article-card:hover .btn-article.invisible {
            visibility: visible !important;
        }

        .article-card a:hover {
            color: currentColor;
        }

        .article-image {
            border-radius: var(--br) var(--br) 0 0;
        }

        .article-price {
            background-color: var(--secondary);
            color: #000;
            font-weight: bold;
            padding: 7px 15px;
            width: fit-content;
            color: white;
            position: absolute;
            border-radius: 0 var(--br) 0 var(--br);
            right: 0;
        }

        .circle {
            width: 20px;
            height: 20px;
            border-radius: 50%;
            /* border-style: solid; */
            border: 1px solid;
        }

    </style>

    @include('layouts.inc.review.review_css')
@endsection

@section('content')
<!-- header -->
<section>
    <header class="become-coach-header">
        <div class="become-coach-header-des">
            <h3 class="font-weight-bold text-center">Tableau de bord </h3>
            <p class="text-center">
                Gérez toutes vos informations
            </p>
        </div>
    </header>
</section>
<section class="pt-5 pb-3">
    <div class="container">
        {{-- Breadcrumb --}}
        <ul class="breadcrumb">
            <li><a href="/">Accueil</a></li>
            <li><a href="/customer/dashboard">Dashboard</a></li>
            <li><a>Evaluer produit</a></li>
        </ul>
        @csrf
    </div>
</section>


<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-12">
                <div class="profil-box-content mb-2">
                    <div>
                        <div class="mb-2">
                            <img src="{{$customer->photo_url}}" class="rounded-circle" height="60" width="60" alt="">
                        </div>
                        <p class="font-weight-bold">{{$customer->name}}</p>
                        <p>Statut : <span class="self-challenger-box">@if($customer->is_coach) Coach @else Self Challenger @endif</span></p>
                        <?php $date_start = Carbon\Carbon::parse($customer->created_at)->locale('fr_FR'); ?>
                        <p>Inscrit depuis le {{$date_start->isoFormat('LL')}}</p>
                        {!! $customer->profileCompletionProgress() !!}

                        <a href="{{ route('customer.manage_profil') }}" class="primary-text-color update-profil-link">Mon
                            profil </a>
                    </div>
                </div>
                @include('customer_frontOffice.layouts_customer.dashboard_panel')
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12">
                @include('flash::message')
                <div class="profil-box-content p-3 mb-2 r">
                    <h4>Evaluation produit</h4>

                    <form action="{{route('customer.reviews.store')}}" id="reviewForm" method="POST" class="mt-3">
                        @csrf
                        <div class="row">
                            <div class="col-3">
                                <img class="img-fluid" src="{{$product->getImageUrl()}}" alt="">
                            </div>
                            <div class="col">
                                <div class="row">
                                    <h3>
                                        {{$product->title}}
                                    </h3>
                                </div>
                                @include('layouts.inc.review.review_input')
                                <div class="row">
                                    <div id="message" class="text-danger font-weight-bold"></div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="product_id" value="{{$product->id}}">
                        <!-- Actual form -->
                        <div class="row mt-3">
                            {{--<div class="form-group col-md-6">
                                <label for="title">Titre</label>
                                <input id="title" class="form-control" type="text" name="title" placeholder="J'aime / Je n'aime pas">
                            </div>--}}
                            <div class="form-group col-md-6">
                                <label for="title">Votre nom</label>
                                <input id="title" class="form-control" type="text" name="title" placeholder="Votre nom" value="{{$customer->name}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-12">
                                <label for="content">Commentaire</label>
                                <textarea id="content" class="form-control" name="content" rows="3" placeholder="Dites-nous plus sur ce produit!"></textarea>
                            </div>
                        </div>
                        <div class="row col-12">
                            <a id="submitForm" class="btn-article btn-secondary w-100">Enregistrer</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection

@section('scripts')
    @include('layouts.inc.review.review_js')

    <script>


        $("#submitForm").click(function(){
            if(!$("input[name='rating']").is(':checked')){
                $("#message").html("Veuillez sélectionner les étoiles");
                return false;
            }
            $("#reviewForm").submit();
        });
</script>
@endsection
