@extends("customer_frontOffice.layouts_customer.master_customer")

@section('content')

@section('css')
<style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }

    a {
        color: black;
    }

    .wrap-resultats .resultats {
        font-size: 64px;
        font-weight: 900;
        color: #099;
        line-height: 100%;
        margin: 25px 0 7px;
        font-family: "Arial Black", "Arial Bold", Gadget, sans-serif;
    }

    .wrap-resultats .txt-resultats {
        text-transform: uppercase;
        font-size: 18px;
        font-weight: 700;
        line-height: 120%;
        color: #099;
    }

    #progressbar {
    margin-bottom: 3vh;
    overflow: hidden;
    color: rgb(252, 103, 49);
    padding-left: 0px;
    margin-top: 3vh
    }

    #progressbar li {
        list-style-type: none;
        font-size: x-small;
        width: 25%;
        float: left;
        position: relative;
        font-weight: 400;
        color: rgb(160, 159, 159)
    }

    #progressbar #step1:before {
        content: "";
        color: rgb(252, 103, 49);
        width: 5px;
        height: 5px;
        margin-left: 0px !important
    }

    #progressbar #step2:before {
        content: "";
        color: #fff;
        width: 5px;
        height: 5px;
        margin-left: 32%
    }

    #progressbar #step3:before {
        content: "";
        color: #fff;
        width: 5px;
        height: 5px;
        margin-right: 32%
    }

    #progressbar #step4:before {
        content: "";
        color: #fff;
        width: 5px;
        height: 5px;
        margin-right: 0px !important
    }

    #progressbar li:before {
        line-height: 29px;
        display: block;
        font-size: 12px;
        background: #ddd;
        border-radius: 50%;
        margin: auto;
        z-index: -1;
        margin-bottom: 1vh
    }

    #progressbar li:after {
        content: '';
        height: 2px;
        background: #ddd;
        position: absolute;
        left: 0%;
        right: 0%;
        margin-bottom: 2vh;
        top: 1px;
        z-index: 1
    }

    .progress-track {
        padding: 0 8%
    }

    #progressbar li:nth-child(2):after {
        margin-right: auto
    }

    #progressbar li:nth-child(1):after {
        margin: auto
    }

    #progressbar li:nth-child(3):after {
        float: left;
        width: 68%
    }

    #progressbar li:nth-child(4):after {
        margin-left: auto;
        width: 132%
    }

    #progressbar li.active {
        color: black
    }

    #progressbar li.active:before,
    #progressbar li.active:after {
        background: rgb(252, 103, 49)
    }

    #details {
    font-weight: 400
    }

    .info .col-5 {
        padding: 0
    }

    #heading {
        color: grey;
        line-height: 6vh
    }

    .pricing {
        background-color: #ddd3;
        padding: 2vh 8%;
        font-weight: 400;
        line-height: 2.5
    }

    .item_deat {
        font-weight: 400;
        line-height: 2.5
    }

    .pricing .col-3 {
        padding: 0
    }

    .total {
        padding: 2vh 8%;
        color: rgb(252, 103, 49);
        font-weight: bold
    }

    .total .col-3 {
        padding: 0
    }

    .modal-body {
      padding: 0 2rem;
    }

</style>
@endsection

@section('content')
<!-- header -->
<section>
    <header class="become-coach-header">
        <div class="become-coach-header-des">
            <h3 class="font-weight-bold text-center">Tableau de bord </h3>
            <p class="text-center">
                Gérez toutes vos informations
            </p>
        </div>
    </header>
</section>
<section class="pt-5 pb-3">
    <div class="container">
        {{-- Breadcrumb --}}
        <ul class="breadcrumb">
            <li><a href="/">Accueil</a></li>
            <li><a href="/customer/dashboard">Dashboard</a></li>
            <li><a>Avis en attente</a></li>
        </ul>
        @csrf
    </div>
</section>


<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-12">
                <div class="profil-box-content mb-2">
                    <div>
                        <div class="mb-2">
                            <img src="{{$customer->photo_url}}" class="rounded-circle" height="60" width="60" alt="">
                        </div>
                        <p class="font-weight-bold">{{$customer->name}}</p>
                        <p>Statut : <span class="self-challenger-box">@if($customer->is_coach) Coach @else Self Challenger @endif</span></p>
                        <?php $date_start = Carbon\Carbon::parse($customer->created_at)->locale('fr_FR'); ?>
                        <p>Inscrit depuis le {{$date_start->isoFormat('LL')}}</p>
                        {!! $customer->profileCompletionProgress() !!}

                        <a href="{{ route('customer.manage_profil') }}" class="primary-text-color update-profil-link">Mon
                            profil </a>
                    </div>
                </div>
                
                @include('customer_frontOffice.layouts_customer.dashboard_panel')
                
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12">
                @include('flash::message')
                <div class="profil-box-content p-3 mb-2 r">
                    <h4>Mes Avis en attente</h4>

                    <div>
                       <h1>Produits</h1>
                        @php
                            $count_order = 0;
                        @endphp 
                        @foreach($deliveredOrders as $order)
                            @foreach($order->productsToReview() as $product)
                                @php
                                    $count_order++;
                                @endphp
                                <div class="card b-fg r ">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img class="img-fluid" src="{{$product->getImageUrl()}}" alt="">
                                        </div>
                                        <div class="col">
                                            <div class="row">
                                                <h3>
                                                    {{$product->title}}
                                                </h3>
                                            </div>
                                            <div class="row">
                                                <h6 class="fw-500">
                                                    Commande n°:
                                                    {{$order->id}}
                                                </h6>
                                            </div>
                                            <div class="row">
                                                <p class="text-primary">
                                                    Livré le {{$order->delivery_date}}
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-4 d-flex align-items-center">
                                        <a href="/customer/reviews/create/{{$order->id}}/{{$product->id}}" class="btn-article btn-secondary">
                                                Evaluer
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                        @if($count_order == 0)
                            <div class="mt-5">
                                <div class="card-body text-center">
                                    <h5 class="card-title">Vous n'avez aucun produit à évaluer pour l'instant</h5>
                                    <a href="/shop" class="btn btn-article btn-secondary mt-2 px-2">Voir la boutique</a>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="mt-5">
                       <h1>Réservation</h1>
                        @php
                            $count = 0;
                        @endphp
                        @foreach($pastCourses as $course)
                            @if($course->courseToReview() && $course->coach->customer)
                                @php
                                    $count++;
                                @endphp
                                <div class="col-lg-4 col-md-4 col-sm-12 col-12 mb-3">
                                    <a href="/customer/reviews/course/{{$course->id}}">
                                    <div class="coach-card">
                                        <div class="price-position">
                                            <img src="{{$course->coach->customer->photo_url}}" class="card-img-top img-fluid mb-2">
                                            <div class="price-content">{{ $course->price }} XOF</div>
                                        </div>
                                        <div class="col ">
                                            <h5><b>{{$course->coach->customer->first_name}}</b> </h5>
                                            @if($course->course_type_object)
                                              <p class="text-primary font-weight-bold">{{$course->course_type_object->name}}</p>
                                            @endif
                                            <div class="mb-2">
                                            @php
                                                $rating = (int) $course->coach->rating;
                                            @endphp
                                            @for ($i = 0; $i < 5; $i++)
                                                @if($i+ 1 <= $rating)
                                                    <img src="{{ asset('customer/images/stars.png')}}" class="icon" alt="">
                                                @else
                                                    <img src="{{ asset('customer/images/star.png')}}" class="icon" alt="">
                                                @endif
                                            @endfor
                                            </div>
                                            <p class="text-secondary font-weight-bold mb-4 mt-2">Coach {{$course->discipline->name ?? ''}}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endif  
                        @endforeach
                        @if($count == 0)
                            <div class="mt-5">
                                <div class="card-body text-center">
                                    <h5 class="card-title">Vous n'avez aucun cours à évaluer pour l'instant</h5>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                @if($customer->is_coach)
                    <div class="profil-box-content p-3 mb-2 r">
                        <h4>Retour des clients sur mes cours</h4>
                    <div>
                       <h1>Avis des clients sur mes cours</h1>
                        @php
                            $count_review = 0;
                        @endphp 
                        @if(!is_null($reviews))
                        @foreach($reviews as $reviews)
                            @if($review && isset($review->customer))
                                @php
                                    $count_review++
                                @endphp
                                <div class="row mt-5 ">
                                    <div class="col-lg-2 col-md-2 col-sm-12">
                                        <img src="{{$review->customer->photo_url}}" height="80" width="80" class="rounded-circle "
                                            alt="">
                                    </div>

                                    <div class="col-lg-10 col-md-10 col-sm-12">
                                        <p class="font-weight-bold">{{$review->customer->name}} le {{$review->created_at}} </p>
                                        <div class="mb-3">
                                        @for ($i = 0; $i < $review->rating; $i++)
                                            <img src="{{ asset('customer/images/stars.png')}}" class="icon" alt="">
                                        @endfor
                                            {{--<a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
                                            <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
                                            <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
                                            <a href="#"><img src="{{ asset('customer/images/star.png') }}" class="icon" alt=""></a>
                                            <a href="#"><img src="{{ asset('customer/images/star.png') }}" class="icon" alt=""></a>--}}
                                        </div>
                                        <div>
                                            <p>
                                            {{$review->content}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if($count_review == 0)
                                <div class="mt-5">
                                    <div class="card-body text-center">
                                        <h5 class="card-title">Vous n'avez aucun cours qui a été évalué pour l'instant</h5>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        @endif
                </div>
                @endif

            </div>
        </div>
    </div>

<div class="modal fade" id="bookingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Détail réservation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="booking-detail" class="mb-4">
          <input type="hidden" name="booking_id" id="input-booking" />
          <div class="info mb-2">
              <div class="row">
                  <div class="col-7"></div>
                  <div  class="col-5 pull-right"> <span id="heading">Order No.</span><br> <span id="ref"></span> </div>
              </div>
          </div>
          <div class="item_deat" id="customer_detail">
            <div class="title"><strong>Information du client</strong> </div>
            <div class="row">
                <div class="col-9"> <span>Nom du client</span> </div>
                <div class="col-3"> <span id="customer_name"></span> </div>
            </div>
          </div>
          <div>

            <div class="item_deat">
                <div class="title"><strong>Information Réservation</strong> </div>
                <div class="row">
                    <div class="col-9"> <span id="lieu">Type Lieu</span> </div>
                    <div class="col-3"> <span></span> </div>
                </div>

                <div class="row">
                    <div class="col-9"> <span>Date début</span> </div>
                    <div class="col-3"> <span id="dateStart"></span> </div>
                </div>
                <div class="row">
                    <div class="col-9"> <span>Date fin</span> </div>
                    <div class="col-3"> <span id="dateEnd"></span> </div>
                </div>
                <div class="row">
                    <div class="col-9"> <span>Description du lieu</span> </div>
                    <div class="col-3"> <span></span> </div>
                </div>
            </div>
            <div class="pricing mt-2">
                <div class="row">
                    <div class="col-9"> <span id="name-booking"></span> </div>
                    <div class="col-3"> <span id="price">0</span> </div>
                </div>
                <div class="row">
                    <div class="col-9"> <span>Quantité</span> </div>
                    <div class="col-3"> <span id="quantity">0</span> </div>
                </div>
            </div>
            <div class="total">
                <div class="row">
                    <div class="col-9"></div>
                    <div class="col-3"><big id="total"></big></div>
                </div>
            </div>
          </div>

          <div class="tracking">
              <div class="title">Tracking Réservation</div>
          </div>
          <div class="progress-track">
              <ul id="progressbar">
                  <li class="step0" id="step1">Confirmation Coach</li>
                  <li class="step0  text-right" id="step2">Début de la séance</li>
                  <li class="step0 text-right" id="step3">Terminée</li>
              </ul>
          </div>
          <div class="row">
            <div class="col-4">
                <div id="button-div" style="display:none">
                    <button type="button" class="btn btn-primary" id="confirm_reservation" onclick="return confirm('Confirmer la réservation?')">Confirmer la réservation</button>
                </div>
            </div>
            <div class="col-4">
                <div id="button-reject" style="display:none">
                    <button type="button" class="btn btn-danger" id="reject_reservation" onclick="return confirm('Rejeter la réservation?')">Rejeter la réservation</button>
                </div>
            </div>
            <div class="col-4">
              <div id="button-begin" style="display:none">
                <button type="button" class="btn btn-primary" id="begin_reservation" onclick="return confirm('Démarrer la séance ?')">Démarrer la séance</button>
              </div>
            </div>
            <div class="col-4">
              <div id="button-terminer" style="display:none">
                <button type="button" class="btn btn-primary" id="fin_reservation" onclick="return confirm('Terminer la séance ?')">Terminer la séance</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="reviewModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Noter la séance</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="mt-4" action="/customer/save/review" method="POST">
          {{ csrf_field() }}
          <input type="hidden" id="booking_id_rev" name="booking_id">
          <div class="form-group">
            <label for="rate">Évaluez votre expérience récente</label>
            <select class="form-control" name="rate" required>
              <option value="">Sélectionner une note</option>
              <option value="1">1/5</option>
              <option value="2">2/5</option>
              <option value="3">3/5</option>
              <option value="4">4/5</option>
              <option value="5">5/5</option>
            </select>
          </div>
          <div class="form-group">
            <label for="description">Racontez-nous votre expérience</label>
            <textarea class="form-control" id="message" name="message" rows="3" required></textarea>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" name="button">Enregistrer</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>
</section>
@endsection
