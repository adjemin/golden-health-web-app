                    <div class="profil-box-content">
                        <div class="row">
                        @if($customer->is_coach == null)
                            <div class="col @if(isset($packs)) r py-2 b-bg  @endif">
                                <a href="{{url('/customer/packs')}}">
                                    <center><img src="{{asset ('customer/images/contract.png') }}" class="text-center" width="30" height="30" alt=""></center>

                                    <center><small>Abonnements</small></center>
                                </a>
                            </div>
                        @endif
                            <div class="col @if(isset($notifications)) r py-2 b-bg  @endif">
                                <a href="{{url('/customer/notifications')}}">
                                    <center>
                                        <small><img src="{{ asset('customer/images/mail.png') }}" class="text-center" width="30" height="30" alt=""></small>
                                    </center>
                                    <center>
                                        <small>Messages</small>
                                    </center>
                                </a>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col @if(isset($coupons)) r py-2 b-bg  @endif">
                                <a href="/customer/coupons">
                                    <center><img src="{{asset ('customer/images/coupon.png') }}" class="text-center" width="30" height="30" alt="">
                                    </center>
                                    <center><small>Bon de reductions </small></center>
                                </a>
                            </div>

                            <div class="col @if(isset($deliveredOrders) || isset($pastCourses)) r py-2 b-bg  @endif">
                                <a href="/customer/reviews">
                                    <center>
                                        <small><img src="{{asset ('customer/images/review.png') }}" class="text-center" width="30" height="30" alt=""></small>
                                    </center>
                                    <center>
                                        <small>Mes notes</small>
                                    </center>
                                </a>
                            </div>
                            @if($customer->is_coach == null)
                            <div class="col @if(isset($coaches)) r py-2 b-bg  @endif">
                                <a href="/customer/coaches">
                                    <center>
                                        <small><img src="{{asset ('customer/images/ch.png') }}" class="text-center" width="30" height="30" alt=""></small>
                                    </center>
                                    <center>
                                        <small>Mes coachs</small>
                                    </center>
                                </a>
                            </div>
                            @endif
                        </div>
                        <div class="row mt-4">
                            <div class="col @if(isset($favorites)) r py-2 b-bg  @endif">
                                <a href="/customer/wishlist">
                                    <center><img src="{{asset ('customer/images/heart.png') }}" class="text-center" width="30" height="30" alt="">
                                    </center>
                                    <center><small>Ma liste de souhaits</small></center>
                                </a>
                            </div>
                            <div class="col @if(isset($orders)) r py-2 b-bg  @endif">
                                <a href="/customer/orders/shop">
                                    <center><img src="{{asset ('customer/images/coupon.png') }}" class="text-center" width="30" height="30" alt="">
                                    </center>
                                    <center>
                                        <small>Mes Commandes</small>
                                    </center>
                                </a>
                            </div>
                        {{-- </div>
                        <div class="row mt-4"> --}}
                            <div class="col @if(isset($tickets)) r py-2 b-bg  @endif">
                                <a href="/customer/tickets">
                                    <center><img src="{{asset ('customer/images/ticket.png') }}" class="text-center" width="30" height="30" alt="">
                                    </center>
                                    <center><small>Mes Tickets Event </small></center>
                                </a>
                            </div>
                        </div>
                    </div>
