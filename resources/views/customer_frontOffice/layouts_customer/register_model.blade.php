<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div>
                        <center>
                            <h4> <b>Créer mon compte </b> </h4>
                        </center>
                        <br>
                        <center>
                            <small>Déjà inscrit sur Golden Health ? &nbsp; <span><a href="#"
                                        class="secondary-text-color">Connectez-vous</a></span></small>
                        </center>
                        <div class="row mt-3">
                            <div class="col-md-6 mt-2">
                                <div class="login-google">
                                    <a href="#"> <img src="images/téléchargement.png" width="15px"
                                            height="15px" alt=""> &nbsp; &nbsp;
                                        Connexion avec
                                        Google</a>
                                </div>
                            </div>
                            <div class="col-md-6 mt-2">
                                <div class="login-facebook">
                                    <a href="#"> <img src="images/facebook.png" width="15px"
                                            height="15px" alt="">
                                        &nbsp; &nbsp; Connexion avec Google</a>
                                </div>
                            </div>
                        </div>
                        <div class="row mx-auto mt-4">
                            <div class="mini-divider mt-3"></div> &nbsp;&nbsp;
                            <p>ou</p>&nbsp;&nbsp;
                            <div class="mini-divider mt-3"></div>
                        </div>

                        <form action="" class="">

                            <div class="row mb-4 ml-1">
                                <div class="form-check mr-4">
                                    <input class="form-check-input" type="radio" name="gridRadios"
                                        id="gridRadios1" value="option1" checked>
                                    <label class="form-check-label" for="gridRadios1">
                                        Mr
                                    </label>
                                </div>

                                <div class="form-check mr-4">
                                    <input class="form-check-input" type="radio" name="gridRadios"
                                        id="gridRadios2" value="option2">
                                    <label class="form-check-label" for="gridRadios2">
                                        Mme
                                    </label>
                                </div>
                            </div>
                            <div class="form-group mt-3 ">
                                <input type="text"
                                    class="form-control input-style placeholder-style" id="prenom"
                                    aria-describedby="prenom" placeholder="Prénom">
                            </div>

                            <div class="form-group mt-3 ">
                                <input type="text"
                                    class="form-control input-style placeholder-style" id="nom"
                                    aria-describedby="nom" placeholder="Nom">
                            </div>

                            <div class="form-group mt-3 ">
                                <input type="email"
                                    class="form-control input-style placeholder-style" id="email"
                                    aria-describedby="email"
                                    placeholder="Adresse e-mail profesionnelle">
                            </div>

                            <div class="form-group mt-2">
                                <input type="password"
                                    class="form-control input-style placeholder-style" id="password"
                                    aria-describedby="password" placeholder="Mot de passe">
                            </div>

                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox1"
                                    value="option1">
                                <label class="form-check-label" for="inlineCheckbox1">J'accepte les
                                    <span class="secondary-text-color">conditions générales
                                        d'utilisation</span></label>
                            </div>

                            <div class="form-check mt-1">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox2"
                                    value="option2">
                                <label class="form-check-label" for="inlineCheckbox2">J'accepte les
                                    S'inscrire à la newsletter</label>
                            </div>
                            <a href="#" class="password-forget mt-3">J'ai perdu mon mot de passe</a>
                            <input type="submit" class="btn btn-secondary col-lg-12 mt-4" value="S'inscrire
                                ">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
