<div id="imagesModal" class="img-modal">
    <span class="close">&times;</span>
    <div class="img-modal-content">
        <div class="img-container">
            @foreach($imageData as $image)
                <div class="img-slides">
                    <div class="numbertext">
                        <span id="imageIndex"></span> {{$loop->index+1}} / {{$imageData->count()}}
                    </div>
                    <img class="img-img" src="{{$image}}" style="width:100%">
                </div>
            @endforeach

            <a class="prev" onclick="plusSlides(-1)">❮</a>
            <a class="next" onclick="plusSlides(1)">❯</a>

            <div class="caption-container">
                <p id="caption"></p>
            </div>

            <div class="img-row">
                @foreach($imageData as $image)
                    <div class="img-column">
                    <img class="demo cursor" src="{{ $image }}" style="width:100%" onclick="currentSlide('{{$loop->index+1}}');" alt="{{ $hotel->name }}">
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
