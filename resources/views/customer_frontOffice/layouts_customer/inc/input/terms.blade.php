
                            <div class="form-check">
                                <input class="form-check-input" name="is_cgu" type="checkbox" id="inlineCheckbox1" value="1" required>
                                <label class="form-check-label" for="inlineCheckbox1">J'accepte les
                                    <a href="{{route('terms')}}" class="secondary-text-color" target="_blank">conditions générales d'utilisation</a></label>
                            </div>

                            <div class="form-check mt-1">
                                <input class="form-check-input" name="is_newsletter" type="checkbox" id="inlineCheckbox2" value="1">
                                <label class="form-check-label" for="inlineCheckbox2">M'inscrire à la newsletter</label>
                            </div>
