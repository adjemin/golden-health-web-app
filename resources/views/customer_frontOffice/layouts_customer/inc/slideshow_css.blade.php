    {{-- Image Modal  --}}
    <style>
        /* The Modal (background) */
        .img-modal {

            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 999999; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
        }

        /* Modal Content (image) */
        .img-modal-content {
            position: relative;
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        }

        /* Caption of Modal Image */


        /* Add Animation */
        .img-modal-content {
        -webkit-animation-name: zoom;
        -webkit-animation-duration: 0.6s;
        animation-name: zoom;
        animation-duration: 0.6s;
        }

        @-webkit-keyframes zoom {
        from {-webkit-transform:scale(0)}
        to {-webkit-transform:scale(1)}
        }

        @keyframes zoom {
        from {transform:scale(0)}
        to {transform:scale(1)}
        }

        /* The Close Button */
        .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: #f1f1f1;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
        }

        .close:hover,
        .close:focus {
        color: #bbb;
        text-decoration: none;
        cursor: pointer;
        }

        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 700px){
        .modal-content {
            width: 100%;
        }
        }
    </style>
        {{-- Image Slideshow --}}
    <style>
        .img-img {
        vertical-align: middle;
        }

        /* Position the image container (needed to position the left and right arrows) */
        .img-container {
        position: relative;
        }

        /* Hide the images by default */
        .img-slides {
        display: none;
        }

        /* Add a pointer when hovering over the thumbnail images */
        .cursor {
        cursor: pointer;
        }

        /* Next & previous buttons */
        .prev,
        .next {
        cursor: pointer;
        position: absolute;
        top: 40%;
        width: auto;
        padding: 16px;
        margin-top: -50px;
        color: white !important;
        font-weight: bold;
        font-size: 20px;
        border-radius: 0 3px 3px 0;
        user-select: none;
        -webkit-user-select: none;
        }

        /* Position the "next button" to the right */
        .next {
            right: -50px;
            border-radius: 3px 0 0 3px;
        }
        .prev {
            left: -50px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .prev:hover,
        .next:hover {
        /* background-color: rgba(0, 0, 0, 0.8); */
        background-color: rgba(255, 255, 255, 0.8);
        }

        /* Number text (1/3 etc) */
        .numbertext {
        color: #f2f2f2;
        font-size: 12px;
        padding: 8px 12px;
        position: absolute;
        top: 0;
        }

        /* Container for image text */
        .caption-container {
        text-align: center;
        background-color: #222;
        padding: 2px 16px;
        color: white;
        }

        /* .img-row:after {
        content: "";
        display: table;
        clear: both;
        } */
        .img-row{
            display: flex;
            justify-content: start;
            max-height: 100px;
        }
        .img-row .demo{
            
        }
        
        /* Six columns side by side */
        .img-column {
            position: relative;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            height: 100px !important;
            
        /* width: 16.66%; */
        }

        /* Add a transparency effect for thumnbail images */
        .demo {
            position: relative;
            opacity: 0.6;
            display:inline-block;
            height: 100%;
            width: auto;
            /* height: auto;
            width: 100%; */
        }

        .active,
        .demo:hover {
        opacity: 1;
        }

    </style>
