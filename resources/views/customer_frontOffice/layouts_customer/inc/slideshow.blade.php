<div class="img-container r">
    @foreach($imageData as $image)
        <div class="img-slides">
            <div class="numbertext">
                <span id="imageIndex"></span> {{$loop->index+1}} / {{$imageData->count()}}
            </div>
            <img class="img-img" src="{{$image}}" style="width:100%">
        </div>
    @endforeach

    <a class="prev" onclick="plusSlides(-1)">❮</a>
    <a class="next" onclick="plusSlides(1)">❯</a>

    <div class="caption-container">
        <p id="caption"></p>
    </div>

    <div class="img-row">
        @foreach($imageData as $image)
            <div class="img-column cursor" onclick="currentSlide('{{$loop->index+1}}');" style="width={{(100/$imageData->count())}}%!important; background-image:url({{$image}})">
                <img class="demo cursor" src="{{ $image }}"  alt="{{ $product->title }}" style="flex-basis:{{(100/$imageData->count())}}%!important;">
            </div>
        @endforeach
    </div>
</div>
