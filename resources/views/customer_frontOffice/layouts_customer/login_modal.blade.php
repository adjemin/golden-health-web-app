<div class="modal fade" id="exampleModalLongs" tabindex="-1" role="dialog"
aria-labelledby="exampleModalLongTitles" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div>
                <center>
                    <h4> <b>Me connecter</b> </h4>
                </center>
                <br>
                <center>
                    <small>Pas encore membre Golden health ? <span><a href="#"
                                class="secondary-text-color">Inscrivez-vous
                            </a></span></small>
                </center>
                <div class="row mt-3">
                    <div class="col-md-6 mt-2">
                        <div class="login-google">
                            <a href="#"> <img src="images/téléchargement.png" width="15px"
                                    height="15px" alt=""> &nbsp; &nbsp;
                                Connexion avec
                                Google</a>
                        </div>
                    </div>
                    <div class="col-md-6 mt-2">
                        <div class="login-facebook">
                            <a href="#"> <img src="images/facebook.png" width="15px"
                                    height="15px" alt="">
                                &nbsp; &nbsp; Connexion avec Google</a>
                        </div>
                    </div>
                </div>
                <div class="row mx-auto mt-4">
                    <div class="mini-divider mt-3"></div> &nbsp;&nbsp;
                    <p>ou</p>&nbsp;&nbsp;
                    <div class="mini-divider mt-3"></div>
                </div>

                <form action="" class="">
                    <div class="form-group mt-3 ">
                        <input type="email"
                            class="form-control input-style placeholder-style" id="email"
                            aria-describedby="username"
                            placeholder="Adresse email professionnelle">
                    </div>
                    <div class="form-group mt-2">
                        <input type="password"
                            class="form-control input-style placeholder-style" id="password"
                            aria-describedby="password" placeholder="Mot de passe">
                    </div>
                    <a href="#" class="password-forget ">J'ai perdu mon mot de passe</a>
                    <input type="submit" class="btn btn-secondary col-lg-12 mt-4"
                        value="Se connecter">
                </form>

            </div>
        </div>
    </div>
</div>
</div>
