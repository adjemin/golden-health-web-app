<section class="dedicated-application">
    <div class="container p-0">
        <div class="">
            <div class="row ">
                <div class="col-lg-8 col-md-8 col-sm-12">
                    <div class="mb-3">
                        <h4 class="font-weight-bold">Une application dédiée</h4>
                    </div>
                    <div>
                        <p>
                            Nous avons développé une application dédiée pour les professionnels disponible sur
                            IOs et Android.
                        </p>
                    </div><br>

                    <div>
                        <p>
                            Une meilleure visibilité
                            Des réservations uniquement sur vos disponibilités
                            Pas de frais d'inscription
                            Pas d'engagement, vous partez quand vous voulez
                            Un service téléphonique pour vos clients (9h à 22h)
                            Une application dédiée aux coachs et gratuite
                        </p>
                    </div><br>
                    <div class="d-flex flex-sm-row flex-column align-items-center justify-content-around">
                        <a href="#" target="_blank" class="btn btn-black mx-auto mt-2">
                            <span class="d-flex align-items-center justify-content-start">
                                <img src="{{ asset('customer/images/google-play.png') }}" class="icon mr-2" width="24px" alt="">
                                Téléchargez sur <br>
                                Google Play
                            </span>
                        </a>
                        <a href="#" target="_blank" class="btn btn-black mx-auto mt-2">
                            <span class="d-flex align-items-center justify-content-start">
                                <i class="fa fa-apple mr-2" aria-hidden="true"></i>
                                Téléchargez sur <br>
                                Apple Store
                            </span>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="d-flex align-items-center justify-content-center">
                        <img src="{{ asset('customer/images/jshdshd.png') }}" width="230" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
