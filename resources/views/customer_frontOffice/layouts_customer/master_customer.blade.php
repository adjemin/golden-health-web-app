<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    {{-- <link rel="icon" href="{{ asset('customer/images/gh1.png') }}"> --}}
    <link rel="icon" href="{{ asset('customer/images/logo_golden-removebg-preview.png') }}">
    {{-- <link rel="icon" href="{{ asset('customer/images/logo_gold.png') }}"> --}}
    <!-- Montserrat Font -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!--  -->
    <link rel="stylesheet" href="{{ asset('customer/css/bootstrap.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('customer/css/responsive.css') }} ">
    <link rel="stylesheet" href="{{ asset('customer/css/style.css') }} ">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="https://unpkg.com/flickity@2.2.1/dist/flickity.css">
    <script src="https://unpkg.com/flickity@2.2.1/dist/flickity.pkgd.min.js"></script>
    <script src="{{ asset('js/pace.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/fullcalendar.css') }} ">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('css/fileinput.css') }}">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-3RBY15TSCX"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-3RBY15TSCX');
    </script>
    @yield('css')

    <style>
        body {
            font-size: 13px;
            /* background-color: rgba(247, 247, 247, 0.973); */
        }

    </style>
    <script>
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;
    </script>

    

<body>
    <div id="snackbar"></div>
    {{-- navbar --}}
    <section class="bg-white">

        <div class="container">
            @auth('customer')
                @include('customer_frontOffice.profile.partials.header')
                {{-- @if (!is_null(Auth::guard('customer')->user()->email_verifed_at))
                    @include('customer_frontOffice.profile.partials.header')
                @else
                    @include('customer_frontOffice.layouts_customer.navbar')
                @endif --}}
            @endauth
            @guest('customer')
                @include('customer_frontOffice.layouts_customer.navbar')
            @endguest
        </div>

    </section>
    {{-- end navbar --}}

    {{-- content --}}
    <section class="page-content">
        @yield('content')
    </section>
    {{-- end content --}}


    @if (Request::is('become_coach') || Request::is('become_partner'))
        @include('customer_frontOffice.layouts_customer.pubapp')
    @endif

    @include('customer_frontOffice.layouts_customer.footer')

    <script>
        function showSnackbar(content = "", colorClass = "bg-success", duration = 3000) {
            console.log("Showing snackbar")
            var x = document.getElementById("snackbar");
            x.innerHTML = content;
            x.className = "show";
            x.classList.add(colorClass);
            setTimeout(function() {
                x.className = x.className.replace("show", "");
            }, duration);
        }
    </script>

    <script src="{{ asset('customer/js/jquery.min.js') }}"></script>
    <script src="{{ asset('customer/js/popper.min.js') }}"></script>
    <script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
    {{-- JS Moment for date format --}}
    <script type="text/javascript" src="{{ asset('customer/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('customer/moment/moment-with-locales.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.blockui.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script type="text/javascript" src="{{ asset('js/fileinput.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.1.3/js/locales/fr.js"></script>
    {{-- API recaptcha --}}
    <script src="https://www.google.com/recaptcha/api.js"></script>
    
    <!-- GetButton.io widget -->
    <script type="text/javascript">
        (function() {
            var options = {
                whatsapp: "+2250777720507", // WhatsApp number
                call_to_action: "Nous contactez", // Call to action
                position: "right", // Position may be 'right' or 'left'
            };
            var proto = document.location.protocol,
                host = "getbutton.io",
                url = proto + "//static." + host;
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = url + '/widget-send-button/js/init.js';
            s.onload = function() {
                WhWidgetSendButton.init(host, proto, options);
            };
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        })();
    </script>
    <script type="text/javascript">
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;


        var e = document.getElementsByClassName("loading-btn");
        var i;

        for (i = 0; i < e.length; i++) {
            e[i].addEventListener("click", function() {

                this.innerHTML = _cuteLoader;
            });
        }
    </script>
    <!-- /GetButton.io widget -->
    <script src="{{ asset('js/script.js') }}"></script>
    @stack('js')
    @yield('scripts')

</body>
<!-- / Footer -->

</html>
