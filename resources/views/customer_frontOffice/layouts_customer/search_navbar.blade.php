<style>
    .dropdown-content {
        display: none;
        position: absolute;
        right: 0;

        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        margin-top: 10px;
        z-index: 1;
    }

    .dropdown-content:hover {
        display: block;
    }

    .dropdown-content a:hover {
        background: #dedede;
    }

    .dropdown-content a {
        float: none;
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        text-align: left;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }

</style>
<section class="white-bg">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light ml-auto">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('customer/images/gh1.png') }}" alt="Logo" width="100px">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <div class="">

                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <div class="">
                                <form action="/search_coach" method="GET" id="search_form">
                                    <div class="input-group search-form1">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text" for="discipline-id"><i
                                                    class="fa fa-search"></i></label>
                                        </div>
                                        <select name="discipline" class="custom-select input-height" id="discipline-id"
                                            required>
                                            <option value="">Choisissez une discipline </option>
                                            @if (isset($all_disciplines) && $all_disciplines)
                                                @foreach ($all_disciplines as $discipline)
                                                    <option value="{{ base64_encode($discipline->id) }}">
                                                        {{ $discipline->name }}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div class="vertical-divider"></div>
                                        <select name="city" id="" class="custom-select input-height">
                                            @php
                                                $villes = \App\Models\ville::all();
                                            @endphp
                                            @foreach ($villes as $ville)
                                                <option value="{{ $ville->name }}">{{ $ville->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-secondary" type="button"
                                                id="submit_search">GO</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>
                        {{-- <li class="nav-item">
                            <a class="nav-link {{ Request::is('/*') ? 'nav-link__active' : '' }}"
                                href="{{ url('/') }}"> Accueil</a>
                        </li> --}}
                        {{-- <li class="nav-item dropdown">
                            <a href="#"
                                class="nav-link {{ Request::is('qui-sommes-nous*') || Request::is('faq*') ? 'nav-link__active' : '' }} dropbtn">
                                </i>
                                A Propos
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <div class="dropdown-content">
                                <a class="dropdown-item {{ Request::is('qui-sommes-nous*') ? 'nav-link__active' : '' }}"
                                    href="{{ route('qui-sommes-nous') }}">Qui sommes nous ?</a>
                                <a class="dropdown-item {{ Request::is('faq*') ? 'nav-link__active' : '' }}"
                                    href="{{ route('faq') }}">FAQ</a>
                            </div>
                        </li> --}}
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('customer/disciplines*') ? 'nav-link__active' : '' }}"
                                href="/customer/disciplines">Disciplines</a>
                        </li>
                        {{-- <li class="nav-item">
                            <a class="nav-link {{ Request::is('programmes*') ? 'nav-link__active' : '' }}"
                                href="{{ route('programmes') }}"> Programmes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('all_events*') ? 'nav-link__active' : '' }}"
                                href="{{ route('events.all') }}"> Events</a>
                        </li> --}}

                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('shop*') ? 'nav-link__active' : '' }}"
                                href="{{ route('shop') }} "> Shop</a>
                        </li>


                        {{-- <a href="{{ route('customer.login') }}" class=" ml-auto login-button  dropbtn"> --}}
                        {{-- <li class="nav-item dropdown">
                            <a href="#"
                                class="nav-link {{ Request::is('blog*') || Request::is('videotheque*') ? 'nav-link__active' : '' }} dropbtn">
                                </i>
                                Médias
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <div class="dropdown-content">
                                <a class="dropdown-item {{ Request::is('blog*') ? 'nav-link__active' : '' }}"
                                    href="{{ route('blog') }}">Blog</a>
                                <a class="dropdown-item {{ Request::is('videotheque*') ? 'nav-link__active' : '' }}"
                                    href="{{ url('/videotheque') }}">Vidéothèque</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('contact*') ? 'nav-link__active' : '' }}"
                                href="{{ route('contact') }}">Contacts</a>
                        </li> --}}
                        <!--li class="nav-item" style="position: relative;">
                            <a class="nav-link btn nav-item  " href="#" data-toggle="dropdown"><small>Services</small><span class="sr-only">(current)</span></a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item " href="{{ route('self-challenger') }}">Self Challenger
                                    @if (Request::is('self-challenger'))
                                    <span class="sr-only">
                                        (current)
                                    </span>
                                    @endif
                                </a>
                                    {{-- {{ route('coachs') }} --}}
                                <a class="dropdown-item " href="{{ route('coach-description') }}">Coach
                                    @if (Request::is('coach-description'))
                                    <span class="sr-only">
                                        (current)
                                    </span>
                                    @endif
                                </a>



                                <a class="dropdown-item " href="{{ route('become.partner') }}">Partner
                                    @if (Request::is('become_partner'))
                                    <span class="sr-only">
                                        (current)
                                    </span>
                                    @endif
                                </a>
                            </div>
                        </li-->



                    </ul>
                </div>
                <div class="ml-auto">
                    <!-- Se connecter -->
                    <span class="nav-item dropdown">
                        {{-- <a href="{{ route('customer.login') }}" class=" ml-auto login-button  dropbtn"> --}}
                        <a href="#" class=" ml-auto register-button  dropbtn">
                            <i class="fa fa-sign-in" aria-hidden="true"></i>
                            S'inscrire
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <div class="dropdown-content">
                            <a class="dropdown-item" href="{{ route('customer.register.self-challenger') }}">Self
                                Challenger</a>
                            <a class="dropdown-item" href="{{ route('customer.register.coach') }}">Coach</a>
                            <a class="dropdown-item" href="{{ route('customer.register.partner') }}">Partner</a>
                        </div>
                    </span>

                    <a href="{{ route('customer.login') }} " class="login-button ml-2">
                        <i class="fa fa-user mr-2" aria-hidden="true"></i>
                        Se Connecter
                    </a>

                    <a href="/basket" class="ml-2 cart-icon">
                        <span class="badge cart-badge">
                            {{ \App\ShoppingCart::ShoppingCart()->items->count() }}
                        </span>
                        <i class="fa fa-shopping-cart mr-2" style="font-size: 20px; color:black"></i>
                    </a>


                </div>
                {{-- <div class="">
                    <a href="#" class="register-button mr-2">
                        <i class="fa fa-shopping-car mr-2" aria-hidden="true"></i> Panier
                    </a>
                </div> --}}
            </div>
        </nav>
    </div>
</section>
