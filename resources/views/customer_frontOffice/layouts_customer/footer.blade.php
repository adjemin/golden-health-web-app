    <footer class="footer">

        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 mb-4">
                    <div>
                        <img src="{{ asset('customer/images/gh2.png') }}" alt="" width="150px">
                    </div>
                    <div>
                        <span style="width: 200px;">
                            @if (App\Entreprise::Instance()) {{ App\Entreprise::Instance()->footer_description }} @endif
                        </span>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="links mb-4 mb-md-0">
                        <h4 class="title pb-2">A propos</h4>
                        <a href="/qui-sommes-nous" class="text-link">Qui sommes-nous ? </a>
                        <a href="/customer/disciplines" class="text-link">Nos disciplines</a>
                        <a href="/contact" class="text-link">Nous contacter</a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="links mb-4 mb-md-0">
                        <h4 class="title pb-2">Contact</h4>
                        <a href="tel:@if (App\Entreprise::Instance()){{ App\Entreprise::Instance()->phone }}@endif" class="text-link">@if (App\Entreprise::Instance()) {{ App\Entreprise::Instance()->phone ?? 'Numéro de téléphone' }} @endif</a>
                        <a href="
                            mailto:{{ App\Entreprise::Instance() && App\Entreprise::Instance()->email ?? App\Entreprise::Instance()->email }}"
                            class="text-link">@if (App\Entreprise::Instance()){{ App\Entreprise::Instance()->email ?? 'Adresse email' }}@endif </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="links mb-4 mb-md-0">
                        <h4 class="title pb-2">Ressources</h4>
                        <a href="/legal/privacy" class="text-link">Politique de confidentialité</a>
                        <a href="/blog" class="text-link">Blog</a>
                        <a href="/faq" class="text-link">FAQ</a>
                    </div>
                </div>

            </div>
            <div class="">
                <center>
                    <a href="{{ env('APP_FACEBOOK_URL') }}" target="_blank"><img
                            src="{{ asset('customer/images/facebook-circular-logo.png') }}" alt="" width="30"
                            height="30" class="mr-2"></a>
                    <a href="{{ env('APP_INSTAGRAM_URL') }}" target="_blank"><img
                            src="{{ asset('customer/images/instagram.png') }}" alt="" class="mr-2"
                            width="30" height="30"></a>
                    <a href="{{ env('APP_TWITTER_URL') }}" target="_blank"><img
                            src="{{ asset('customer/images/twitter.png') }}" alt="" class="mr-2" width="30"
                            height="30"></a>
                </center>
            </div>

            <div class="text-center mt-4">
                <small>© 2020 <a href="{{ env('APP_URL') }}" class="text-secondary">{{ env('APP_NAME') }}</a> .
                    Tous
                    Droits Réservés.</small>
                <span>Powered by <a href="{{ env('ADJEMIN_URL') }}" class="text-secondary fw-500"
                        target="_blank">Adjemin</a></span>
            </div>
        </div>
    </footer>
