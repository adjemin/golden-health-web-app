@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
<link href="{{asset('css/jquery-steps.css')}}" rel="stylesheet"/>
<style>
    body {
        font-size: 13px;
        background-color: rgba(238, 238, 238, 0.336);

    }

    a {
        color: black;
    }

    a:hover {
        color: var(--primary)
    }

    

</style>
@endsection

@section('content')
<div class="container">
<div id="booking-process" class="mt-4">
    <h3>Choisissez un créneau</h3>
    <wizard>
        <p>Try the keyboard navigation by clicking arrow left or right!</p>
    </wizard>
    <h3>Formule</h3>
    <wizard>
    <!--section>
        <div class="container py-3">
            <div class="coach-identity">
                <div class="row" style="border: 1px solid #eee; padding:5px">
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div>
                            <img src="{{$coach->customer->photo_url}}" class="img-fluid" alt="">
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 py-3">
                        <div class="">
                            <div class="">
                                <h4 class="font-weight-bold">{{$coach->customer->name}}</h4>
                            </div>
                            <div class="">
                                <p>Coach {{$course->discipline->name}}</p>
                            </div>
                            <div class="mb-4">
                                <a href="#"><img src="{{asset('customer/images/stars.png')}}" class="icon" alt=""></a>
                                <a href="#"><img src="{{asset('customer/images/stars.png')}}" class="icon" alt=""></a>
                                <a href="#"><img src="{{asset('customer/images/stars.png')}}" class="icon" alt=""></a>
                                <a href="#"><img src="{{asset('customer/images/star.png')}}" class="icon" alt=""></a>
                                <a href="#"><img src="{{asset('customer/images/star.png')}}" class="icon" alt=""></a>
                            </div>
                            <div>
                                <button class="btn-outline-secondary">
                                    <span class="">Tarif Indicatif</span> <br>
                                    <span class="secondary-text-color font-weight-bold h5">{{$course->price}} XOF</span>
                                </button>
                                <input type="hidden" name="course_price" id="course_price" value="{{$course->price}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 py-3">
                        <div class="mb-4">
                            <p class="text-right">Date du cours : samedi 4 juillet
                                Heure : 10h00</p>
                        </div>
                        <div class="text-right mt-5 ">
                            <button class="ask-queestion"><img src="{{ asset('customer/images/conversation (1).') }}" height="30" width="30" alt=""> Posez votre
                                question</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section-->

    <!--  -->
    <section>
        <div class="container mb-3">
            <div class="row">
                <ul class="nav nav-pills mb-4 mt-3  justify-content-center" id="rounded-pills-icon-tab" role="tablist">
                    <li class="nav-item ml-2 mr-2">

                        <a class="nav-link mb-2 active text-center" id="rounded-pills-icon-home-tab " data-toggle="pill" href="#rounded-pills-icon-home" role="tab" aria-controls="rounded-pills-icon-home" aria-selected="true">
                            <div class="tab-content-box">
                                <span class="font-weight-bold ">Cours Unique</span>
                                <p class="mt-1">
                                    Réserver uniquement
                                    ce cours.
                                </p>
                                <center>
                                    <div class=" mt-1 mini-circle-tabs">

                                    </div>
                                </center>
                            </div>
                        </a>
                    </li>

                    <li class="nav-item ml-2 mr-2">
                        <a class="nav-link mb-2 text-center" id="rounded-pills-icon-profile-tab" data-toggle="pill" href="#rounded-pills-icon-profile" role="tab" aria-controls="rounded-pills-icon-profile" aria-selected="false" style="">
                            <div class="tab-content-box">
                                <span class="font-weight-bold ">Pack de 5 cours </span>
                                <p class="mt-1">
                                    <small class="font-weight-bold">Bénéficiez de -5.0%</small>

                                    <small>Vous recevrez alors un code qui vous permettra de commander les 4 autres
                                        cours.</small>
                                </p>
                                <center>
                                    <div class=" mt-1 mini-circle-tabs">

                                    </div>
                                </center>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item ml-2 mr-2">
                        <a class="nav-link mb-2 text-center" id="rounded-pills-icon-contact-tab" data-toggle="pill" href="#rounded-pills-icon-contact" role="tab" aria-controls="rounded-pills-icon-contact" aria-selected="false">
                            <div class="tab-content-box">
                                <span class="font-weight-bold ">Pack de 10 cours </span>
                                <p class="mt-1">
                                    <small class="font-weight-bold">Bénéficiez de -10% </small>

                                    <small>
                                        Vous recevrez alors un code qui vous permettra de commander les 9 autres cours.
                                    </small>
                                </p>
                                <center>
                                    <div class=" mt-1 mini-circle-tabs">

                                    </div>
                                </center>
                            </div>
                        </a>
                    </li>

                    <li class="nav-item ml-2 mr-2">
                        <a class="nav-link mb-2 text-center" id="rounded-pills-icon-settings-tab" data-toggle="pill" href="#rounded-pills-icon-settings" role="tab" aria-controls="rounded-pills-icon-settings" aria-selected="false">
                            <div class="tab-content-box">
                                <span class="font-weight-bold ">Pack de 10 cours </span>
                                <p class="mt-1">
                                    <small class="font-weight-bold">Bénéficiez de -17% </small>

                                    <small>
                                        Vous recevrez alors un code qui vous permettra de commander les 29 autres cours.
                                    </small>
                                </p>
                                <center>
                                    <div class=" mt-1 mini-circle-tabs">

                                    </div>
                                </center>
                            </div>
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="rounded-pills-icon-tabContent">
                    <div class="tab-pane fade show active" id="rounded-pills-icon-home" role="tabpanel" aria-labelledby="rounded-pills-icon-home-tab">
                        <div class="row px-2">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <h6 class="font-weight-bold">Bon de réduction ou pack</h6>
                                <p>Si vous avez un pack connectez-vous pour l'utiliser</p>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control input-style">
                                    <div class="input-group-append">
                                        <button class="login-button" type="submit">OK</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="text-right">
                                    <h5 class="font-weight-bold text-primary">Total TTC {{$course->price}} XOF</h5>
                                    <div class="">
                                        <button type="submit" class="login-button ">Continuer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grey-bg mt-5">
                            <h4 class="font-weight-bold text-primary">Informations</h4>
                            <p>
                                Paiement : l'argent est versé au coach 24h après le cours, si celui-ci s'est bien
                                déroulé
                                Annulation : tous les cours sont annulables gratuitement jusqu'a 24h avant le début du
                                cours. Il suffit
                                d'aller dans votre espace réservations et de l'annuler. Vous pourrez ensuite
                                reprogrammer
                                votre seance selon vos disponibilités.
                                Utilisation des packs : pour les packs, vous reçevrez un code par mail. Il sera
                                également
                                disponible dans vos "Bons et Packs" dans votre espace personnel. Pour l'utiliser, il
                                suffit
                                de rentrer ce code sur la page panier. Les packs sont valables 1 an à partir de la date
                                d'achat. Enfin, si votre coach ne vous convenait pas, nous nous engageons à trouver un
                                autre
                                coach sans aucun frais.
                            </p>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="rounded-pills-icon-profile" role="tabpanel" aria-labelledby="rounded-pills-icon-profile-tab">
                        <div class="row px-2">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <h6 class="font-weight-bold">Bon de réduction ou pack</h6>
                                <p>Si vous avez un pack connectez-vous pour l'utiliser</p>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control input-style">
                                    <div class="input-group-append">
                                        <button class="login-button" type="submit">OK</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="text-right">
                                    <h5 class="font-weight-bold text-primary">Total TTC {{$course->price}} XOF</h5>
                                    <div class="">
                                        <button type="submit" class="login-button ">Continuer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grey-bg mt-5">
                            <h4 class="font-weight-bold text-primary">Informations</h4>
                            <p>
                                Paiement : l'argent est versé au coach 24h après le cours, si celui-ci s'est bien
                                déroulé
                                Annulation : tous les cours sont annulables gratuitement jusqu'a 24h avant le début du
                                cours. Il suffit
                                d'aller dans votre espace réservations et de l'annuler. Vous pourrez ensuite
                                reprogrammer
                                votre seance selon vos disponibilités.
                                Utilisation des packs : pour les packs, vous reçevrez un code par mail. Il sera
                                également
                                disponible dans vos "Bons et Packs" dans votre espace personnel. Pour l'utiliser, il
                                suffit
                                de rentrer ce code sur la page panier. Les packs sont valables 1 an à partir de la date
                                d'achat. Enfin, si votre coach ne vous convenait pas, nous nous engageons à trouver un
                                autre
                                coach sans aucun frais.
                            </p>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="rounded-pills-icon-contact" role="tabpanel" aria-labelledby="rounded-pills-icon-contact-tab">
                        <div class="row px-2">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <h6 class="font-weight-bold">Bon de réduction ou pack</h6>
                                <p>Si vous avez un pack connectez-vous pour l'utiliser</p>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control input-style">
                                    <div class="input-group-append">
                                        <button class="login-button" type="submit">OK</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="text-right">
                                    <h5 class="font-weight-bold text-primary">Total TTC {{$course->price}} XOF</h5>
                                    <div class="">
                                        <button type="submit" class="login-button ">Continuer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grey-bg mt-5">
                            <h4 class="font-weight-bold text-primary">Informations</h4>
                            <p>
                                Paiement : l'argent est versé au coach 24h après le cours, si celui-ci s'est bien
                                déroulé
                                Annulation : tous les cours sont annulables gratuitement jusqu'a 24h avant le début du
                                cours. Il suffit
                                d'aller dans votre espace réservations et de l'annuler. Vous pourrez ensuite
                                reprogrammer
                                votre seance selon vos disponibilités.
                                Utilisation des packs : pour les packs, vous reçevrez un code par mail. Il sera
                                également
                                disponible dans vos "Bons et Packs" dans votre espace personnel. Pour l'utiliser, il
                                suffit
                                de rentrer ce code sur la page panier. Les packs sont valables 1 an à partir de la date
                                d'achat. Enfin, si votre coach ne vous convenait pas, nous nous engageons à trouver un
                                autre
                                coach sans aucun frais.
                            </p>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="rounded-pills-icon-settings" role="tabpanel" aria-labelledby="rounded-pills-icon-settings-tab">
                        <div class="row px-2">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <h6 class="font-weight-bold">Bon de réduction ou pack</h6>
                                <p>Si vous avez un pack connectez-vous pour l'utiliser</p>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control input-style">
                                    <div class="input-group-append">
                                        <button class="login-button" type="submit">OK</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="text-right">
                                    <h5 class="font-weight-bold text-primary">Total TTC {{$course->price}} XOF</h5>
                                    <div class="">
                                        <button type="submit" class="login-button ">Continuer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grey-bg mt-5">
                            <h4 class="font-weight-bold text-primary">Informations</h4>
                            <p>
                                Paiement : l'argent est versé au coach 24h après le cours, si celui-ci s'est bien
                                déroulé
                                Annulation : tous les cours sont annulables gratuitement jusqu'a 24h avant le début du
                                cours. Il suffit
                                d'aller dans votre espace réservations et de l'annuler. Vous pourrez ensuite
                                reprogrammer
                                votre seance selon vos disponibilités.
                                Utilisation des packs : pour les packs, vous reçevrez un code par mail. Il sera
                                également
                                disponible dans vos "Bons et Packs" dans votre espace personnel. Pour l'utiliser, il
                                suffit
                                de rentrer ce code sur la page panier. Les packs sont valables 1 an à partir de la date
                                d'achat. Enfin, si votre coach ne vous convenait pas, nous nous engageons à trouver un
                                autre
                                coach sans aucun frais.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </wizard>
    <h3>Pager</h3>
    <wizard>
        <p>The next and previous buttons help you to navigate through your content.</p>
    </wizard>
</div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{asset('js/jquery.steps.min.js')}}"></script>
<script>
$(document).ready(function(){
    $("#booking-process").steps({
        headerTag: "h3",
        bodyTag: "wizard",
        transitionEffect: "slideLeft",
        autoFocus: true
    });
});
</script>
@endpush