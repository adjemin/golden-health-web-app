@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
    <style>
        a {
            color: black;
            font-weight: bold;
        }

        a:hover {
            color: var(--secondary);
        }

    </style>

@endsection


@section('content')
    <section class="py-5">
        <div class="container-fluid">
            <div class="row">
                <div class="article-header">
                    <div class="description">
                        {{ $post->title }}
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid ">
            <div class="row main-container">
                <div class="col-lg-7 col-md-7 col-sm-12  offset-md-1">
                    <h5 class="mt-5 font-weight-bold">{{ $post->title }}</h5>
                    {{-- <p class="mt-4" style="line-height: 2;">
                        {!! $post->description !!}

                    </p> --}}


                    <img src="{{ $post->cover }}" class="img-fluid" alt="" srcset="">


                    <p class="mt-3">
                        {!! $post->description !!}
                    </p>
                    <div class="mt-5 text-left">
                        <p style="text-decoration: underline;"> <b>Partager</b></p>
                        <iframe src="https://www.facebook.com/plugins/share_button.php?href={{ Request::url() }}&layout=button_count&size=large&width=130&height=28&appId" width="130" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                        {{-- <div style="display: inline; margin-right: 10px" class="fb-share-button" data-href="{{ Request::url() }}" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">
                            <img src="{{ asset('customer/images/icons8-facebook-48 (1).png') }}"
                                height="20px" width="20px" alt="" srcset="">
                        </a></div> --}}
                        {{-- <a href="#" class="mr-3"><img src="{{ asset('customer/images/icons8-facebook-48 (1).png') }}"
                                height="20px" width="20px" alt="" srcset=""></a> --}}
                        {{-- <a href="#"><img src="{{ asset('customer/images/icons8-instagram-52.png') }}" height="20px"
                                width="20px" alt="" srcset=""></a> --}}
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-12 ">
                    {{--
                    <!-- Anciens articles -->

                    <h5 class="mt-5 font-weight-bold">Anciens articles</h5>
                    <div class="secondary-dividers mb-5"></div>
                    <!--  -->
                    @if ($oldPosts != null)
                        @foreach ($oldPosts as $oldPost)
                            <div class="white-bg d-flex mb-3">
                                <div>
                                    <img src="{{ $oldPost->cover }}" class="img-height" alt="">
                                </div>

                                <div class="px-2 py-1">
                                    <h6 class="secondary-text-color"><b>{{ $oldPost->getCategoryAttribute()->name }}</b>
                                    </h6>
                                    <p>{{ $oldPost->title }}</p>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="white-bg d-flex">
                            Aucun article récent disponible !
                        </div>
                    @endif

                    <!-- Archives -->

                    <h5 class="mt-5 font-weight-bold">Archives</h5>
                    <div class="small-secondary-dividers mb-4"></div>

                    <p class="mt-5"> <a href="#">20 Janvierv 2020</a></p>
                    <hr>
                    <p> <a href="#">20 Janvierv 2020</a></p>
                    <hr>
                    <p> <a href="#">20 Janvierv 2020</a></p>
                    <hr>--}}

                    <!-- Categories -->

                    <h5 class="mt-5 font-weight-bold">Catégories</h5>
                    <div class="small-secondary-dividers mb-4"></div>
                    @foreach($post_grouped as $key => $value)
                        @php
                            $categorie = \App\Models\Category::find($key);
                        @endphp
                    <div class="d-flex justify-content-between">
                        <div>
                            <a href="/blog?categorie={{$categorie->name}}">{{$categorie->name}}</a>
                        </div>
                        <div>
                            <a href="/blog?categorie={{$categorie->name}}" class="cat">{{count($value)}}</a>
                        </div>
                    </div>
                        @if(!$loop->last)
                            <hr>
                        @endif
                    @endforeach
                    {{--<div class="d-flex justify-content-between">
                        <div>
                            <a href="#">Handball</a>
                        </div>
                        <div>
                            <a href="#" class="cat">5</a>
                        </div>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-between">
                        <div>
                            <a href="#">Volleyball</a>
                        </div>
                        <div>
                            <a href="#" class="cat">3</a>
                        </div>
                    </div>
                    <hr>--}}

                </div>
            </div>
        </div>
    </section>

@endsection
