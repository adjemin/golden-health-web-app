@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
<style>
    body {
        background-color: rgba(238, 238, 238, 0.336);
    }


    .qui-somme-nous {
        /* line-height: 2.3; */
        font-size: 16px;
    }
    .card {
        position: relative;
        /* width: 300px; */
        width: auto;
        height: 320px;
        background: #000;
        overflow: hidden;
        border: none;
        box-shadow: 2px 2px 3px #666;
    }

    .card .image {
        width: 100%;
        height: 100%;
        overflow: hidden;
    }
    .card .image img {
        width: 100%;
        /* height: 100%; */
        display: block;
        height: auto;
        transition: .5s;
    }
    .card:hover .image img {
        opacity: .2;
        /* transform: translateX(30%); */
        transform: translateY(-30%);
    }
    .card .title{
        position: absolute;
        left: 50%;
        transform: translateX(-50%);
        text-align: center;
        /* top: 10px; */
        top: 50px;
        opacity: 0;
        transition: all .3s;
    }
    .card:hover .title{
        top: 10px;
        opacity: 1;
    }
    .card .details {
        position: absolute;
        bottom: -130px;
        width: 100%;
        height: 70%;
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
        /* background: #FFC107; */
        transition: .5s;
        /* transform-origin: left; */
        /* transform: perspective(2000px) rotateY(-90deg); */
        /* transform: translateY(100px); */
    }
    .card:hover .details {
        transform: translateY(-130px);
    }
    .card .details .center {
        padding: 10px;
        text-align: center;
        height: 100%;
        /* background: #e9ecef; */
        background: rgba(237, 170, 13, 0.769);
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
    }
    .card .details .center h1 {
        margin: 0;
        padding: 0;
        color: #1c6a49;
        opacity: 1;
        font-weight: 700;

        line-height: 20px;
        font-size: 22px;
        text-transform: uppercase;
        transition: all .3s;
    }
    .card:hover .details .center h1{
        opacity: 0;
        margin-bottom: 20px;
    }
    .card .details .center h1 span {
        font-size: 14px;
        color: #262626;
    }
    .card .details .center p {
        margin: 10px 0;
        padding: 0;
        font-size: 18px;
        letter-spacing: 1px;
        color: transparent;
        transition: color .4s;
    }
    .card:hover .details .center p {
        /* color: #000; */
        color: #fff;
    }


</style>
@endsection
@section('content')

<section class="py-3">
    <div class="container">
        <div class="text-center p-md-5 p-2" style="border-radius: 5px; background:lightgrey;">
            <h1 class="font-weight-bold text-center primary-text-color mb-3 text-capitalize">Qui Sommes nous ? </h1>
        </div>
    </div>
</section>

<section class="py-3">
    <div class="container">
        {{-- <h1 class="font-weight-bold primary-text-color mb-4">Qui sommes nous ? </h1> --}}
        <p class="jumbotron qui-somme-nous">
            {{$entreprise->bio}}
            {{-- La plateforme de Golden Health a pour but de promouvoir et d’encourager une pratique régulière et responsable du sport dans les ménages ivoiriens. Elle vise également à augmenter le nombre d’adepte à moyen et long terme afin d’assister à un recul des maladies chroniques (Hypertension artérielle, Diabètes etc…) qui ont gagnées du terrain en Côte d’Ivoire au cours de ces dernières années.
            Elle est portée par l’innovation, la qualité du service, l’esprit d'équipe, la rigueur dans le travail, l'intégrité, l’empathie et surtout la satisfaction complète du client. --}}
        </p>
        <br>
        <div class="container">
            <h1 class="mb-5 text-center text-primary font-weight-bold text-capitalize">
                Nos valeurs
            </h1>
        </div>
        <ul class="qui-somme-nous">
        </ul>
    </div>
    {{-- <div class="container">
        <div class="card">
            <div class="image">
                <img src="{{asset('customer/images/passion.jpg')}}"/>
            </div>
            <div class="title">
                <h1 class="text-uppercase font-weight-bold text-secondary">
                    Passion
                </h1>
            </div>
            <div class="details">
                <div class="center">
                    <h1 class="text-uppercase">
                        Passion
                    </h1>
                    <p class="font-weight-bold">
                        Nous sommes guidés par l’amour du sport et du bien-être
                    </p>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="container mb-5 pb-5">
        <div class="row my-2">
            @if($entreprise->values)
                @foreach ($entreprise->values as $value)

                    <div class="@if($entreprise->values->count()%2!=0 && $loop->last) col-md-12 col-lg-8 @else col-lg-4 col-md-6 @endif mb-3">
                        <div class="card">
                            <div class="image">
                                <img src="{{ $value->cover_url ?? asset('customer/images/passion.jpg')}}"/>
                            </div>
                            <div class="title">
                                <h3 class="text-uppercase font-weight-bold text-secondary">
                                    {{$value->title ?? ''}}
                                </h3>
                            </div>
                            <div class="details" style="background-image:url({{$value->cover_url ?? asset('customer/images/passion.jpg')}});">
                                <div class="center">
                                    <h1 class="text-uppercase">
                                        {{$value->title ?? ''}}
                                    </h1>
                                    <p class="font-weight-bold">
                                        {{$value->description ?? ''}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</section>

@endsection
