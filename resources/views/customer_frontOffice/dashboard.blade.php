@extends("customer_frontOffice.layouts_customer.master_customer")

@section('content')

@section('css')
<style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }

    a {
        color: black;
    }

    .wrap-resultats .resultats {
        font-size: 64px;
        font-weight: 900;
        color: #099;
        line-height: 100%;
        margin: 25px 0 7px;
        font-family: "Arial Black", "Arial Bold", Gadget, sans-serif;
    }

    .wrap-resultats .txt-resultats {
        text-transform: uppercase;
        font-size: 18px;
        font-weight: 700;
        line-height: 120%;
        color: #099;
    }

    #progressbar {
    margin-bottom: 3vh;
    overflow: hidden;
    color: rgb(252, 103, 49);
    padding-left: 0px;
    margin-top: 3vh
    }

    #progressbar li {
        list-style-type: none;
        font-size: x-small;
        width: 25%;
        float: left;
        position: relative;
        font-weight: 400;
        color: rgb(160, 159, 159)
    }

    #progressbar #step1:before {
        content: "";
        color: rgb(252, 103, 49);
        width: 5px;
        height: 5px;
        margin-left: 0px !important
    }

    #progressbar #step2:before {
        content: "";
        color: #fff;
        width: 5px;
        height: 5px;
        margin-left: 32%
    }

    #progressbar #step3:before {
        content: "";
        color: #fff;
        width: 5px;
        height: 5px;
        margin-right: 32%
    }

    #progressbar #step4:before {
        content: "";
        color: #fff;
        width: 5px;
        height: 5px;
        margin-right: 0px !important
    }

    #progressbar li:before {
        line-height: 29px;
        display: block;
        font-size: 12px;
        background: #ddd;
        border-radius: 50%;
        margin: auto;
        z-index: -1;
        margin-bottom: 1vh
    }

    #progressbar li:after {
        content: '';
        height: 2px;
        background: #ddd;
        position: absolute;
        left: 0%;
        right: 0%;
        margin-bottom: 2vh;
        top: 1px;
        z-index: 1
    }

    .progress-track {
        padding: 0 8%
    }

    #progressbar li:nth-child(2):after {
        margin-right: auto
    }

    #progressbar li:nth-child(1):after {
        margin: auto
    }

    #progressbar li:nth-child(3):after {
        float: left;
        width: 68%
    }

    #progressbar li:nth-child(4):after {
        margin-left: auto;
        width: 132%
    }

    #progressbar li.active {
        color: black
    }

    #progressbar li.active:before,
    #progressbar li.active:after {
        background: rgb(252, 103, 49)
    }

    #details {
    font-weight: 400
    }

    .info .col-5 {
        padding: 0
    }

    #heading {
        color: grey;
        line-height: 6vh
    }

    .pricing {
        background-color: #ddd3;
        padding: 2vh 8%;
        font-weight: 400;
        line-height: 2.5
    }

    .item_deat {
        font-weight: 400;
        line-height: 2.5
    }

    .pricing .col-3 {
        padding: 0
    }

    .total {
        padding: 2vh 8%;
        color: rgb(252, 103, 49);
        font-weight: bold
    }

    .total .col-3 {
        padding: 0
    }

    .modal-body {
      padding: 0 2rem;
    }

</style>
@endsection

@section('content')
<!-- header -->
<section>
    <header class="become-coach-header">
        <div class="become-coach-header-des">
            <h3 class="font-weight-bold text-center">Tableau de bord </h3>
            <p class="text-center">
                Gérez toutes vos informations
            </p>
        </div>
    </header>
</section>
<section>
    <div class="container py-5">
        <div class="row">
            @if($customer->is_coach == null)
            <!-- Devenez Coach Golden Health -->
            {{--<div class="col-lg-6 col-md-6 col-sm-12">
                <div class=" alert dismissible-coach-box alert-dismissible fade show" role="alert">
                    <div>
                        <p class="font-weight-bold">Devenez Coach Golden Health </p>
                        <p>Voulez-vous bénéficier de plus de visibilité et de vivre de votre passion ?
                            N'attendez plus, rejoignez-nous !
                        </p>
                        <a href="{{ route('become.coach') }}" class="login-button button-position-right">
                            Commencer
                        </a>
                    </div>

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>--}}
            @endif



        </div>
        {{-- <div class="py-2">
            <p class="h4 mb-4 font-weight-bold primary-text-color">Inscrivez vous à un cour</p>

            <a href="{{ route('register-course') }}" class="primary-button">Inscription à cour</a>
        </div> --}}
    </div>
</section>




<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-12">
                <div class="profil-box-content mb-2">
                    <div>
                        <div class="mb-2">
                            <img src="{{$customer->photo_url}}" class="rounded-circle" height="60" width="60" alt="">
                        </div>
                        <p class="font-weight-bold">{{$customer->name}}</p>
                        <p>Statut : <span class="self-challenger-box">@if($customer->is_coach) Coach @else Self Challenger @endif</span></p>
                        <?php $date_start = Carbon\Carbon::parse($customer->created_at)->locale('fr_FR'); ?>
                        <p>Inscrit depuis le {{$date_start->isoFormat('LL')}}</p>
                        {!! $customer->profileCompletionProgress() !!}

                        <div class="mt-4">
                            <a href="/customer/profile/settings" class="grey-button">Modifier profil</a>
                        </div>
                        {{--<a href="{{ route('customer.manage_profil') }}" class="primary-text-color update-profil-link">Mon
                            profil </a>--}}
                    </div>
                </div>
                @if($customer->is_coach == null)
                {{--<div class="profil-box-content">
                    <div class="row">
                        <div class="col">
                          <a href="{{url('/customer/packs')}}">
                            <center><img src="{{asset ('customer/images/contract.png') }}" class="text-center" width="30" height="30" alt=""></center>
                            <center><small>Abonnements</small></center>
                          </a>
                        </div>

                        <div class="col">
                            <a href="{{url('/customer/notifications')}}">
                              <center>
                                  <small><img src="{{ asset('customer/images/mail.png') }}" class="text-center" width="30" height="30" alt=""></small>
                              </center>
                              <center>
                                  <small>Messages</small>
                              </center>
                            </a>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col">
                            <a href="{{url('/customer/coupons')}}">
                              <center><img src="{{asset ('customer/images/coupon.png') }}" class="text-center" width="30" height="30" alt="">
                              </center>
                              <center><small>Bon de reductions </small></center>
                            </a>
                        </div>

                        <div class="col">
                          <a href="{{url('/customer/coaches')}}">
                            <center>
                                <small><img src="{{asset ('customer/images/ch.png') }}" class="text-center" width="30" height="30" alt=""></small>
                            </center>
                            <center>
                                <small>Mes coachs</small>
                            </center>
                          </a>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <!--div class="col">
                            <center><img src="{{asset ('customer/images/heart.png') }}" class="text-center" width="30" height="30" alt="">
                            </center>
                            <center><small>Bon de reductions </small></center>
                        </div-->
                        <div class="col">
                            <a href="/customer/tickets">
                                <center><img src="{{asset ('customer/images/ticket.png') }}" class="text-center" width="30" height="30" alt="">
                                </center>
                                <center>
                                    <small>Mes Tickets</small>
                                </center>
                            </a>
                        </div>
                        <div class="col">
                            <a href="/customer/orders/shop">
                                <center><img src="{{asset ('customer/images/orders.png') }}" class="text-center" width="30" height="30" alt="">
                                </center>
                                <center>
                                    <small>Mes Commandes</small>
                                </center>
                            </a>
                        </div>
                    </div>
                </div>--}}
                @endif
                @include('customer_frontOffice.layouts_customer.dashboard_panel')
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12">
              @include('flash::message')
              @if($customer->is_coach == null)
                    {{--@php
                    $booking = $customer->booking()->get();
                    $sum = 0;
                    foreach($booking as $item){
                        $sum += $item->pastBooking();
                    }
                    $pastBooking = $booking->count() - $sum;
                    @endphp--}}
                    @php
                        $bookingCompletedCount = $customer->booking()->where('status', 4)->count();
                        $bookingCount = $customer->booking()->where('coach_confirm', 0)->whereIn('status_name', ['paid', 'confirmed'])->count();
                    @endphp

                <section>
                    <div class="container">
                        <div class="py-2">
                            <div class="profil-box-content">
                                <h5 class="font-weight-bold">Statistiques </h5>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                                        <h3 class="font-weight-bold primary-text-color">{{ $bookingCompletedCount }}</h3>
                                        <p>Cours effectués</p>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                                        <h3 class="font-weight-bold primary-text-color">{{ $bookingCount }}</h3>
                                        <p>Cours réservés</p>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                                        <h3 class="font-weight-bold primary-text-color ">{{ $bookingCompletedCount * 2 }}</h3>
                                        <p>Points Goden Health</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
              @endif
              @if($customer->is_coach)

                <section>
                    <div class="container">
                        <div class="py-2">
                            <div class="profil-box-content">
                                <h5 class="font-weight-bold">Statistiques </h5>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                                        <h3 class="font-weight-bold primary-text-color">{{ $customer->is_coach->pastCourseCount() }}</h3>
                                        <p>Cours effectués</p>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                                        <h3 class="font-weight-bold primary-text-color">{{ $customer->is_coach->reservationLeftCount() }}</h3>
                                        <p>Cours réservés</p>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                                        <h3 class="font-weight-bold primary-text-color ">{{ $customer->is_coach->goldenPoint() }}</h3>
                                        <p>Points Goden Health</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                @endif

                <!-- Cours effectutés -->
                @if(isset($availabilities) && !is_null($availabilities) && $customer->is_coach)
                <section>
                    <div class="container">
                        <div class="py-2">
                            <div>
                            <!-- <div class="courses-makes"> -->
                                <div>
                                    <h5 class="font-weight-bold">Cours éffectués</h5>
                                    {{--@php
                                        $courses = collect($availabilities)->filter(function($availability){
                                            return $availability->isPast();
                                        })->map(function($availability){
                                            return $availability->course;
                                        });

                                    @endphp--}}
                                    @php
                                        $courses = \App\Models\Course::where('coach_id', $customer->is_coach->id)->get();
                                        $pastCourse = collect($courses)->filter(function($course){
                                            return $course->isPast();
                                        });
                                    @endphp
                                    @if($pastCourse->count() == 0)
                                        <p>Vous n'avez pas encore effectué de cours</p>
                                    @else
                                        <table class="table table-striped mt-3">
                                            <thead>
                                                <tr>
                                                    <th>Discipline</th>
                                                    <th>Type de cours</th>
                                                    <th>Prix</th>
                                                    <th>Nombre de personne</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($pastCourse as $course)
                                                    <tr>
                                                        <th>{{$course->discipline->name ?? ''}}</th>
                                                        <th>{{$course->course_type}}</th>
                                                        <th>{{$course->price}}</th>
                                                        <th>{{$course->detail->nb_person ?? ''}}</th>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <!-- Heures de pratiques -->
                <section>
                    <div class="container">
                        <div class="py-2">
                            <div class="profil-box-content">
                                <div>
                                    <h5 class="font-weight-bold">Heures de pratiques</h5>
                                    @php
                                        $left_pratice_time = collect($availabilities)->filter(function($availability){
                                            return $availability->isPast() == false;
                                        });
                                    @endphp
                                    @if(count($left_pratice_time) == 0)
                                        <p>Vous n'avez pas effectué de cours</p>
                                    @else
                                        <table class="table table-striped mt-3">
                                            <thead>
                                                <tr>
                                                    <th>Course</th>
                                                    <th>Date de debut</th>
                                                    <th>Date de fin</th>
                                                    <th>Heure de depart</th>
                                                    <th>Heure de fin</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($left_pratice_time as $availability)
                                                @if($availability->course && $availability->course->discipline)
                                                <tr>
                                                    <td>{{$availability->course->discipline->name ?? ''}}</td>
                                                    <td>{{ $availability->date_debut }}</td>
                                                    <td>{{ $availability->date_fin }}</td>
                                                    <td>{{ $availability->start_time }}</td>
                                                    <td>{{ $availability->end_time }}</td>
                                                    <td>
                                                        <a href="/customer/lessons/manage?action=update&course_id={{$availability->course->id}}" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"> </i> Modifier </a>
                                                        <a href="#" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown"> <span class="caret"></span> </a>
                                                        <ul class="dropdown-menu">
                                                            <li class="px-1">
                                                            {!! Form::open(['route' => ['courseCoaoch.destroy', $availability->course->id], 'method' => 'delete']) !!}
                                                                {!! Form::button('<i class="fa fa-trash"></i> Supprimer', ['type' => 'submit', 'class' => 'btn
                                                                btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                            {!! Form::close() !!}
                                                            </li>
                                                            <li  class="px-1">
                                                                <a href="/customer/lessons/manage/show/{{$availability->course->id}}"> <i class="fa fa-eye"></i> Voir mon cours</a>
                                                             </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                @endif
                @php
                    $conf_count = 5;
                    $wait_count = 5;
                    $pending_count = 5;
                @endphp
              {{--@if($customer->is_coach)--}}
                  <div class="profil-box-content p-0 mb-2">
                    <div class="courses-entete">
                        Réservation en attente
                    </div>
                    <div class="courses-content">
                      @if($coach_booking_wait != null && count($coach_booking_wait) > 0)
                      <div class="mt-4">
                          <table class="table">
                              <thead>
                                  <tr>
                                      <th>Status</th>
                                      <th>Cours</th>
                                      <th>Pack</th>
                                      <th>Date début</th>
                                      <th>Date fin</th>
                                      <th>Montant</th>
                                      {{-- <th>Status</th> --}}
                                      <th>Action</th>
                                  </tr>
                              </thead>
                              <tbody>
                                 @if($coach_booking_wait)
                                      @foreach($coach_booking_wait as $booking_c)
                                          @if(!is_null($booking_c) && !is_null($booking_c->course))
                                          @php
                                            if($wait_count < $coach_booking_wait->count()){
                                                $wait_count += 5;
                                            }
                                            //$wait_count++;
                                          @endphp
                                          <tr id="wishlist_10136" data-product-id="{{$booking_c->id}}">
                                            @if(in_array($booking_c->status_name,["paid", "confirmed", "completed"]))
                                                <td> <span class="badge badge-success">{{$booking_c->status_name}}</span> </td>
                                            @else
                                                <td> <span class="badge badge-warning">UnPaid</span> </td>
                                            @endif
                                            <td>Coach {!! !is_null($booking_c->course->discipline) ? $booking_c->course->discipline->name : ''!!}</td>
                                            <td>{{$booking_c->pack->name ?? ''}}</td>
                                            <td>{{$booking_c->start_date}}</td>
                                            <td>{{$booking_c->end_date}}</td>
                                            {{--
                                              <td>{{$booking_c->course->availability->date_debut}}</td>
                                              <td>{{$booking_c->course->availability->start_time}} - {{$booking_c->course->availability->end_time}}</td>
                                              --}}
                                            <td>{{$booking_c->amount}} CFA</td>
                                            {{-- <td> <span class="badge badge-warning" data-toggle="tooltip" data-placement="top" title="Paiement non effectué par le client">Unpaid</span> </td> --}}
                                            <td>
                                                @if($customer->is_coach)
                                                    <button type="button" class="btn btn-success booking_detail" data-modal="coach" data-id="{{$booking_c->id}}" name="button" style="font-size:10px" title="Voir plus de détails">Voir plus de détails</button>
                                                @else
                                                    <button type="button" class="btn btn-success booking_detail" data-modal="customer" data-id="{{$booking_c->id}}" name="button" style="font-size:10px" title="Voir plus de détails">Voir plus de détails</button>
                                                @endif
                                            </td>
                                          </tr>
                                          @endif
                                      @endforeach
                                 @endif
                              </tbody>
                          </table>
                          {{-- @if($wait_count > 5)
                          <div class="d-flex justify-content-center">
                            <a href="#" class="btn btn-light">Voir plus</a>
                          </div>
                          @endif --}}
                      </div>
                      @else
                        <p>Aucune réservation en attente</p>
                      @endif
                    </div>
                  </div>
                  <div class="profil-box-content p-0 mb-2">
                    <div class="seance-entete">
                        Séance à venir
                    </div>
                    <div class="courses-content">
                      @if($coach_booking_conf != null && $coach_booking_conf->count() > 0)
                      <div class="mt-4">
                          <table class="table">
                              <thead>
                                  <tr>
                                      <th>Status</th>
                                      <th>Cours</th>
                                      <th>Pack</th>
                                      <th>Date début</th>
                                      <th>Date fin</th>
                                      <th>Montant</th>
                                      <th>Action</th>
                                  </tr>
                              </thead>
                              <tbody>
                                 @if($coach_booking_conf)
                                    @foreach($coach_booking_conf as $booking_c)
                                        @if(!is_null($booking_c) && !is_null($booking_c->course) && $customer->is_coach && $booking_c->status == 2)
                                          @php
                                            $conf_count++;
                                          @endphp
                                          <tr id="wishlist_10136" data-product-id="{{$booking_c->id}}">
                                            @if(in_array($booking_c->status_name,["paid", "confirmed", "completed"]))
                                                <td> <span class="badge badge-success">{{$booking_c->status_name}}</span> </td>
                                            @else
                                                <td> <span class="badge badge-warning">UnPaid</span> </td>
                                            @endif
                                            <td>Coach {!! !is_null($booking_c->course->discipline) ? $booking_c->course->discipline->name : ''!!}</td>
                                            <td>{{$booking_c->pack->name ?? '!is_null($booking_c) && !is_null($booking_c->course)'}}</td>
                                            <td>{{$booking_c->start_date}}</td>
                                            <td>{{$booking_c->end_date}}</td>
                                            {{--
                                              <td>{{$booking_c->course->availability->date_debut}}</td>
                                              <td>{{$booking_c->course->availability->start_time}} - {{$booking_c->course->availability->end_time}}</td>
                                              --}}
                                            <td>{{$booking_c->amount}} CFA</td>
                                            <td>
                                                @if($customer->is_coach)
                                                    <button type="button" class="btn btn-success booking_detail" data-modal="coach" data-id="{{$booking_c->id}}" name="button" style="font-size:10px" title="Voir plus de détails">Voir plus de détails</button>
                                                @else
                                                    <button type="button" class="btn btn-success booking_detail" data-modal="customer" data-id="{{$booking_c->id}}" name="button" style="font-size:10px" title="Voir plus de détails">Voir plus de détails</button>
                                                @endif
                                            </td>
                                          </tr>
                                        @endif
                                        @if(!is_null($booking_c) && !is_null($booking_c->course)  && $booking_c->status == 2 && !$customer->is_coach)
                                          @php
                                            $conf_count++;
                                          @endphp
                                          <tr id="wishlist_10136" data-product-id="{{$booking_c->id}}">
                                            @if(in_array($booking_c->status_name,["paid", "confirmed"]))
                                                <td> <span class="badge badge-success">{{$booking_c->status_name}}</span> </td>
                                            @else
                                                <td> <span class="badge badge-warning">UnPaid</span> </td>
                                            @endif
                                            <td>Coach {!! !is_null($booking_c->course->discipline) ? $booking_c->course->discipline->name : ''!!}</td>
                                            <td>{{$booking_c->pack->name ?? '!is_null($booking_c) && !is_null($booking_c->course)'}}</td>
                                            <td>{{$booking_c->start_date}}</td>
                                            <td>{{$booking_c->end_date}}</td>
                                            {{--
                                              <td>{{$booking_c->course->availability->date_debut}}</td>
                                              <td>{{$booking_c->course->availability->start_time}} - {{$booking_c->course->availability->end_time}}</td>
                                              --}}
                                            <td>{{$booking_c->amount}} CFA</td>
                                            <td>
                                                @if($customer->is_coach)
                                                    <button type="button" class="btn btn-success booking_detail" data-modal="coach" data-id="{{$booking_c->id}}" name="button" style="font-size:10px" title="Voir plus de détails">Voir plus de détails</button>
                                                @else
                                                    <button type="button" class="btn btn-success booking_detail" data-modal="customer" data-id="{{$booking_c->id}}" name="button" style="font-size:10px" title="Voir plus de détails">Voir plus de détails</button>
                                                @endif
                                            </td>
                                          </tr>
                                        @endif
                                      @endforeach
                                 @endif
                              </tbody>
                          </table>
                          @if($conf_count > 5)
                          <div class="d-flex justify-content-center">
                            <a href="#" class="btn btn-light">Voir plus</a>
                          </div>
                          @endif
                      </div>
                      @else
                        <p>Aucune séance à venir</p>
                      @endif
                    </div>
                  </div>
              {{--@endif--}}
              {{--@if($customer->is_coach == null)--}}
                <div class="profil-box-content p-0 mb-2">
                    <div class="courses-entete">
                        Activité en cours
                    </div>
                    <div class="courses-content">
                        <!-- <p>Activité en cours</p> -->
                        @if($coach_booking_conf->count() > 0)
                        <div class="mt-4">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Status</th>
                                        <th>Cours</th>
                                        <th>Date début</th>
                                        <th>Date de fin</th>
                                        <th>Montant</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @if($bookings)
                                        @foreach($coach_booking_conf as $booking_c)
                                            @if(!is_null($booking_c) && !is_null($booking_c->course) && $booking_c->status == 3)
                                            @php
                                                $pending_count++;
                                            @endphp
                                            <tr id="wishlist_10136" data-product-id="{{$booking_c->id}}">
                                              @if(in_array($booking_c->status_name,["paid", "confirmed"]))
                                                <td> <span class="badge badge-success">{{$booking_c->status_name}}</span> </td>
                                              @else
                                                <td> <span class="badge badge-warning">UnPaid</span> </td>
                                              @endif
                                              <td>Coach {!! !is_null($booking_c->course->discipline) ? $booking_c->course->discipline->name : ''!!}</td>
                                              <td>{{$booking_c->start_date}}</td>
                                              <td>{{$booking_c->end_date}}</td>
                                              <td>{{$booking_c->amount}} CFA</td>
                                              <td>
                                                @if($customer->is_coach)
                                                    <button type="button" class="btn btn-success booking_detail" data-modal="coach" data-id="{{$booking_c->id}}" name="button" style="font-size:10px" title="Voir plus de détails">Voir plus de détails</button>
                                                @else
                                                    <button type="button" class="btn btn-success booking_detail" data-modal="customer" data-id="{{$booking_c->id}}" name="button" style="font-size:10px" title="Voir plus de détails">Voir plus de détails</button>
                                                @endif
                                                {{--@if($booking_c->status == 4)
                                                  <button type="button" class="btn btn-secondary mt-2 add_new_review" data-modal="customer" data-id="{{$booking_c->id}}" name="button" style="font-size:10px"  title="Ajouter un note">Laisser une note</button>
                                                @endif--}}
                                              </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                   @endif
                                </tbody>
                            </table>
                            {{-- @if($pending_count > 5)
                            <div class="d-flex justify-content-center">
                                <a href="#" class="btn btn-light">Voir plus</a>
                            </div>
                            @endif --}}
                        </div>
                        @else
                          <hr>
                          <p>Retrouvez ici les notifications concernant vos activités (cours accepté ou réfusé …)</p>
                          <p class="mt-3">Activité terminée</p>
                          <hr>
                          <p>Retrouvez ici les notifications concernant vos activités terminées </p>
                        @endif
                    </div>
                </div>

                @if($customer->is_coach == null)
                <div class="profil-box-content mb-2">
                    <div>
                        <h6 class="font-weight-bold primary-text-color">Calculez votre indice de Masse Corporelle
                            (IMC) homme ou femme</h6>
                        <p>Ces différentes informations nous permettrons de vous envoyer gratuitement vos indices de
                            masse corporelle et quelques conseils. </p>
                    </div>

                    <div class="row py-2">
                        <div class="col-lg-6 col-md-6 col-sm-12 mt-3">
                            <form role id="calcul-imc-form">
                                @csrf
                                <div class="col">
                                    <img src="{{asset ('customer/images/height.png') }}" width="40" height="40" alt="">
                                    <p>Taille en cm</p>
                                    <input type="number" name="customer_size" placeholder="{{$customer->size}}" value="{{$customer->size}}" class="calcul-input" required>
                                </div>
                                <div class="col mt-3">
                                    <img src="{{asset ('customer/images/weight.png') }}" width="40" height="40" alt="">
                                    <p>Poids actuelle en kg</p>
                                    <input type="number" name="customer_weight" placeholder="{{$customer->weight}}" value="{{$customer->weight}}" class="calcul-input" required>
                                </div>
                                <div class="col mt-3">
                                    <button type="submit" class="login-button mt-3">Calculer</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 mt-3 ">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="dev_resultats" class="wrap-resultats" style="display: none;">
                                        <div id="dev_resultat_imc" class="resultats"></div>
                                        <div class="txt-resultats">Votre IMC</div>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-4">
                                    <!--  -->
                                    <div class="d-flex mb-2">
                                        <div class="proportion-circle mr-2"></div> <span class="font-weight-bold small-text"> moins de 18,5</span> <small> =
                                            MAIGREUR</small>
                                    </div>
                                    <!--  -->
                                    <div class="d-flex mb-2">
                                        <div class="green-proportion-circle mr-2"></div> <span class="font-weight-bold small-text"> 18,5 à 25</span> <small> = CORPULANCE
                                            NORMALE </small>
                                    </div>
                                    <!--  -->
                                    <div class="d-flex mb-2">
                                        <div class="yellow-proportion-circle mr-2"></div> <span class="font-weight-bold small-text"> 25 à 30</span> <small> =
                                            SURPOIDS</small>
                                    </div>
                                    <!--  -->
                                    <div class="d-flex mb-2">
                                        <div class="orange-proportion-circle mr-2"></div> <span class="font-weight-bold small-text"> 30 à 35</span><small> = OBESITE MODERE
                                            (CLASSE I) </small>
                                    </div>
                                    <!--  -->
                                    <div class="d-flex mb-2">
                                        <div class="orangered-proportion-circle mr-2"></div> <span class="font-weight-bold small-text"> 35 à 40</span><small> = OBESITE SEVERE
                                            (CLASSE II)</small>
                                    </div>
                                    <!--  -->
                                    <div class="d-flex">
                                        <div class="red-proportion-circle mr-2"></div> <span class="font-weight-bold small-text"> plus de 40</span> <small>= OBESITE
                                            MORBIDE (CLASSE III)</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @php
                    $invoices = \App\Models\Customer::find(\Auth::guard('customer')->user()->id)->invoices()->latest()->get();
                    $payed = collect($invoices)->filter(function($invoice){
                        return $invoice->invoice->status == 'paid';
                    });
                    $unpayed = collect($invoices)->filter(function($invoice){
                        return $invoice->invoice->status == 'unpaid';
                    });
                @endphp
                @if($invoices->count() > 0)
                <div class="profil-box-content p-0 mb-3">
                    <div class="payment-entete">Mes Paiements </div>
                    <div class="courses-content">
                        @if($payed->count() > 0)
                        <p>Paiements récents ({{$payed->count()}})</p>
                        <table class="table table-striped" id="invoices-table-paid">
                            <thead>
                                <tr>
                                    <th>Reference</th>
                                    <th>Service</th>
                                    <th>Total</th>
                                    <!-- <th>Status</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($payed as $invoice)
                                <tr>
                                    <td>{{ $invoice->invoice->reference ?? '' }}</td>
                                    <td>{{ $invoice->invoice->service  ?? ''}}</td>
                                    <td>{{ $invoice->invoice->total  ?? ''}}</td>
                                    {{--<td>{!! $invoice->invoice->status == "paid" ? '<span class="badge badge-success p-1"> Payé</span>' : '<span class="badge badge-success p-1">Non Payé</span>' !!}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif
                        @if($payed->count() > 0 && $unpayed->count() > 0)
                        <hr>
                        @endif
                        {{--<p>Retrouvez ici les notifications concernant vos paiements </p>--}}
                        @if($unpayed->count() > 0)
                        <p>Paiements écouhés ({{$unpayed->count()}})</p>
                        <table class="table table-striped" id="invoices-table-unpaid">
                            <thead>
                                <tr>
                                    <th>Reference</th>
                                    <th>Service</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                    <!-- <th>Status</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($unpayed as $invoice)
                                <tr>
                                    <td><a href="#">{{ $invoice->invoice->reference  ?? ''}}</a> </td>
                                    <td>{{ $invoice->invoice->service  ?? ''}}</td>
                                    <td>{{ $invoice->invoice->total  ?? ''}}</td>
                                    <td> <a class="btn btn-info" href="/customer/viewinvoice/{{$invoice->invoice->id}}">Voir la facture</a> </td>
                                    {{--<td>{!! $invoice->invoice->status == "paid" ? '<span class="badge badge-success p-1"> Payé</span>' : '<span class="badge badge-success p-1">Non Payé</span>' !!}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif
                        {{--<hr>
                        <p>Retrouvez ici les notifications concernant vos paiements échoué</p>--}}
                    </div>
                </div>
                @endif
            @endif

                @if($customer->type_account == 'challenger')
                <!-- Devenez Partner Golden Health  -->
                {{--<div class=" alert dismissible-partner-box alert-dismissible fade show" role="alert">
                    <div>
                        <p class="font-weight-bold">Devenez Partner Golden Health </p>
                        <p>Voulez-vous faire connaitre vos produits et services ?
                            Bénéficiez d'une large communauté de sportifs à travers la plateforme Golden Health
                        </p>
                        <a href="{{ route('become.partner') }}" class="primary-button button-position-right">
                            Commencer
                        </a>
                    </div>

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>--}}
                @endif
            </div>
        </div>
    </div>

<div class="modal fade" id="bookingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Détail réservation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="booking-detail" class="mb-4">
          <input type="hidden" name="booking_id" id="input-booking" />
          <div class="info mb-2">
              <div class="row">
                  <div class="col-7"></div>
                  <div  class="col-5 pull-right"> <span id="heading">Order No.</span><br> <span id="ref"></span> </div>
              </div>
          </div>
          {{-- <div class="item_deat" id="coach_detail">
            <div class="title"><strong>Information du client</strong> </div>
            <div class="row">
                <div class="col-9"> <span>Nom du coach</span> </div>
                <div class="col-3"> <span id="coach_name"></span> </div>
            </div>
          </div> --}}
          <div class="item_deat" id="customer_detail">
            <div class="title"><strong>Information du client</strong> </div>
            <div class="row">
                <div class="col-9"> <span>Nom du client</span> </div>
                <div class="col-3"> <span id="customer_name"></span> </div>

                <div class="col-9"> <span>Numéro de téléphone</span> </div>
                <div class="col-3"> <span id="phone"></span> </div>

                <div class="col-9"> <span>Email </span> </div>
                <div class="col-3"> <span id="email"></span> </div>

                <div class="col-9"> <span>Commune</span> </div>
                <div class="col-3"> <span id="commune"></span> </div>

                <div class="col-9"> <span>Quartier</span> </div>
                <div class="col-3"> <span id="quartier"></span> </div>
            </div>
          </div>
          <div>

            <div class="item_deat">
                <div class="title"><strong>Information Réservation</strong> </div>
                <div class="row">
                    <div class="col-9"> <span>Type Lieu</span> </div>
                    <div class="col-3"> <span id="lieu"></span> </div>
                </div>

                <div class="row">
                    <div class="col-9"> <span>Date début</span> </div>
                    <div class="col-3"> <span id="dateStart"></span> </div>
                </div>
                <div class="row">
                    <div class="col-9"> <span>Date fin</span> </div>
                    <div class="col-3"> <span id="dateEnd"></span> </div>
                </div>
                <!-- <div class="row">
                    <div class="col-9"> <span>Description du lieu</span> </div>
                    <div class="col-3"> <span id="description_lieu"></span> </div>
                </div> -->
            </div>
            <div class="pricing mt-2">
                <div class="row">
                    <div class="col-9"> <span id="name-booking"></span> </div>
                    <div class="col-3"> <span id="price">0</span> </div>
                </div>
                <div class="row">
                    <div class="col-9"> <span>Quantité</span> </div>
                    <div class="col-3"> <span id="quantity">0</span> </div>
                </div>
            </div>
            <div class="total">
                <div class="row">
                    <div class="col-9"></div>
                    <div class="col-3"><big id="total"></big></div>
                </div>
            </div>
          </div>

          <div class="tracking">
              <div class="title">Tracking Réservation</div>
          </div>
          <div class="progress-track">
              <ul id="progressbar">
                  <li class="step0" id="step1">Confirmation Coach</li>
                  <li class="step0  text-right" id="step2">Début de la séance</li>
                  <li class="step0 text-right" id="step3">Terminée</li>
              </ul>
          </div>
          @if($customer->is_coach)
          <div class="row">
            <div class="col-4">
                <div id="button-div" style="display:none">
                    <button type="button" class="btn btn-primary" id="confirm_reservation" onclick="return confirm('Confirmer la réservation?')">Confirmer la réservation</button>
                </div>
            </div>
            <div class="col-4">
                <div id="button-reject" style="display:none">
                    <button type="button" class="btn btn-danger" id="reject_reservation" onclick="return confirm('Rejeter la réservation?')">Rejeter la réservation</button>
                </div>
            </div>
            <div class="col-4">
              <div id="button-begin" style="display:none">
                <button type="button" class="btn btn-primary" id="begin_reservation" onclick="return confirm('Démarrer la séance ?')">Démarrer la séance</button>
              </div>
            </div>
            <div class="col-4">
              <div id="button-terminer" style="display:none">
                <button type="button" class="btn btn-primary" id="fin_reservation" onclick="return confirm('Terminer la séance ?')">Terminer la séance</button>
              </div>
            </div>
          </div>
          @endif
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="reviewModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Noter la séance</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="mt-4" action="/customer/save/review" method="POST">
          {{ csrf_field() }}
          <input type="hidden" id="booking_id_rev" name="booking_id">
          <div class="form-group">
            <label for="rate">Évaluez votre expérience récente</label>
            <select class="form-control" name="rate" required>
              <option value="">Sélectionner une note</option>
              <option value="1">1/5</option>
              <option value="2">2/5</option>
              <option value="3">3/5</option>
              <option value="4">4/5</option>
              <option value="5">5/5</option>
            </select>
          </div>
          <div class="form-group">
            <label for="description">Racontez-nous votre expérience</label>
            <textarea class="form-control" id="message" name="message" rows="3" required></textarea>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" name="button">Enregistrer</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>
</section>
@endsection
@push('js')
<script type="text/javascript" src="{{asset('assets/js/jquery.blockui.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
    /*
     * Block / Unblock UI
     */
    function blockUI() {
        $.blockUI({
            message: "<img src='{{asset('assets/img/loading.gif')}}' alt='Loading ...' />"
            , overlayCSS: {
                backgroundColor: '#1B2024'
                , opacity: 0.55
                , cursor: 'wait'
            }
            , css: {
                border: '4px #999 solid'
                , padding: 15
                , backgroundColor: '#fff'
                , color: 'inherit'
            }
        });
    }

    function unblockUI() {
        $.unblockUI();
    }

    $(document).ready(function() {
        function processForm(e) {
            blockUI();
            $.ajax({
                url: '/customer/calcul/imc'
                , dataType: 'json'
                , type: 'POST'
                , data: $(this).serialize()
                , success: function(data) {
                    unblockUI();
                    console.log(data);
                    if (data.data) {
                        $('#dev_resultat_imc').text(data.data.value_BMI);
                        $('#dev_resultats').show();
                        Swal.fire({
                            icon: 'success'
                            , title: 'Félication !'
                            , text: 'Calcul effectué avec succès'
                        , });
                    } else {
                        $('#dev_resultat_imc').text(data.value_BMI);
                        $('#dev_resultats').show();
                    }
                }
                , error: function(error) {
                    unblockUI();
                    console.log(error);
                    Swal.fire({
                        icon: 'warning'
                        , title: 'Oops...'
                        , text: 'Une erreur s\'est produite veuillez réessayer'
                    , });
                }
            });
            e.preventDefault();
        }
        $('#calcul-imc-form').submit(processForm);


        $('#confirm_reservation').on('click', function(e){
          $('#bookingModal').modal('hide');
          blockUI();
          var booking_id = $('#input-booking').val();
          console.log(booking_id);
          $.ajax({
              url: '/api/booking_coaches/'+booking_id
              , dataType: 'json'
              , type: 'PUT'
              , data: {
                  "coach_confirm": 1,
                  "status":2,
                  "status_name": "paid"
                },
                success: function(data) {
                  unblockUI();
                  console.log(data);
                      Swal.fire({
                          icon: 'success'
                          , title: 'Félication !'
                          , text: 'Réservation confirmée avec succès'
                      , }).then((result) => {
                            if (result.isConfirmed) {
                                location.reload();
                            }
                        });
              }
              , error: function(error) {
                  unblockUI();
                  console.log(error);
                  Swal.fire({
                      icon: 'warning'
                      , title: 'Oops...'
                      , text: 'Une erreur s\'est produite veuillez réessayer'
                  , });
              }
          });
          e.preventDefault();
        });

        $('#reject_reservation').on('click', function(e){
          $('#bookingModal').modal('hide');
          blockUI();
          var booking_id = $('#input-booking').val();
          console.log(booking_id);
          $.ajax({
              url: '/api/booking_coaches/'+booking_id
              , dataType: 'json'
              , type: 'PUT'
              , data: {
                  "coach_confirm": 2,
                  "status":5,
                  "status_name": "canceled"
                }
              , success: function(data) {
                  unblockUI();
                  console.log(data);
                      Swal.fire({
                          icon: 'success'
                          , title: 'Ohoh !'
                          , text: 'Réservation rejetée avec succès'
                      , }).then((result) => {
                            if (result.isConfirmed) {
                                location.reload();
                            }
                        });
              }
              , error: function(error) {
                  unblockUI();
                  console.log(error);
                  Swal.fire({
                      icon: 'warning'
                      , title: 'Oops...'
                      , text: 'Une erreur s\'est produite veuillez réessayer'
                  , });
              }
          });
          e.preventDefault();
        });

        $('#begin_reservation').on('click', function(e){
          $('#bookingModal').modal('hide');
          blockUI();
          var booking_id = $('#input-booking').val();
          console.log(booking_id);
          $.ajax({
              url: '/api/booking_coaches/'+booking_id
              , dataType: 'json'
              , type: 'PUT'
              , data: {
                  "status": 3,
                  "status_name": "confirmed",
                  "coach_confirm": 1
                }
              , success: function(data) {
                  unblockUI();
                  console.log(data);
                      Swal.fire({
                          icon: 'success'
                          , title: 'coool !'
                          , text: 'Enregistrement effectué avec succès'
                      , }).then((result) => {
                            if (result.isConfirmed) {
                                location.reload();
                            }
                        });
              }
              , error: function(error) {
                  unblockUI();
                  console.log(error);
                  Swal.fire({
                      icon: 'warning'
                      , title: 'Oops...'
                      , text: 'Une erreur s\'est produite veuillez réessayer'
                  , });
              }
          });
          e.preventDefault();
        });


        $('#fin_reservation').on('click', function(e){
          $('#bookingModal').modal('hide');
          blockUI();
          var booking_id = $('#input-booking').val();
          console.log(booking_id);
          $.ajax({
              url: '/api/booking_coaches/'+booking_id
              , dataType: 'json'
              , type: 'PUT'
              , data: {
                "status_name": "completed",
                "coach_confirm": 1,
                "status": 4
                }
              , success: function(data) {
                  unblockUI();
                  console.log(data);
                      Swal.fire({
                          icon: 'success'
                          , title: 'coool !'
                          , text: 'Enregistrement effectué avec succès'
                      , }).then((result) => {
                            if (result.isConfirmed) {
                                location.reload();
                            }
                        });
              }
              , error: function(error) {
                  unblockUI();
                  console.log(error);
                  Swal.fire({
                      icon: 'warning'
                      , title: 'Oops...'
                      , text: 'Une erreur s\'est produite veuillez réessayer'
                  , });
              }
          });
          e.preventDefault();
        });

        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })

        $('.add_new_review').on('click', function(e){
            var bookin_id = $(this).attr('data-id');
            var type_modal = $(this).attr('data-modal');
            $('#booking_id_rev').val(bookin_id);
            $('#reviewModal').modal('show');
        })

        $('.booking_detail').on('click', function(e){
          var bookin_id = $(this).attr('data-id');
          var type_modal = $(this).attr('data-modal');

          blockUI();
          $.ajax({
              url: '/api/booking_coaches/'+bookin_id
              , dataType: 'json'
              , type: 'GET'
              , success: function(data) {
                  unblockUI();
                  console.log(data);
                  var resp = data.data;
                  discipline = resp.course.discipline != null ? resp.course.discipline.name : ''
                  /////////// ITEM BOOKING APPEND DATA MODAL

                  $('#booking-detail #input-booking').val(resp.id);
                  $('#booking-detail #name-booking').text( discipline+" - "+ resp.pack.name);
                  $('#booking-detail #price').text(resp.course.price+ " CFA");
                  $('#booking-detail #quantity').text(resp.quantity);
                  $('#booking-detail #ref').text(resp.booking_ref);
                  $('#booking-detail #total').text(resp.amount+ " CFA");
                  $('#booking-detail #dateStart').text(resp.start_date);
                  $('#booking-detail #dateEnd').text(resp.end_date);

                  var response_lieu

                  $.ajax({
                    url: '/api/lieu_courses/'+resp.course_lieu_id,
                    dataType: 'json',
                    type: 'GET',
                    success: function(datax) {
                        console.log(datax);
                        response_lieu = datax.data

                        $('#booking-detail #lieu').text(datax.data.name);
                        $('#booking-detail #description_lieu').text(datax.data.description);
                    }
                  })


                  /////////// CUSTOMER APPEND DATA MODAL
                  $('#booking-detail #customer_name').text(resp.customer.gender.charAt(0).toUpperCase()+resp.customer.gender.slice(1).toLowerCase()+" "+resp.customer.first_name+ " "+resp.customer.last_name);
                  $('#booking-detail #phone').text("+"+resp.course.coach.customer.phone);
                  $('#booking-detail #email').text(resp.course.coach.customer.email);
                  $('#booking-detail #commune').text(resp.course.coach.customer.commune);
                  $('#booking-detail #quartier').text(resp.course.coach.customer.quartier);
                  ////////// UPDATE PROGRESS BAR STATE Order
                    if (resp.status == 4) {
                        $('#booking-detail #step1').addClass('active');
                        $('#booking-detail #step2').addClass('active');
                        $('#booking-detail #step3').addClass('active');
                    } else {
                        $('#booking-detail #step1').removeClass('active');
                        $('#booking-detail #step2').removeClass('active');
                        $('#booking-detail #step3').removeClass('active');
                    }

                    if (resp.status == 3) {
                        $('#booking-detail #step1').addClass('active');
                        $('#booking-detail #step2').addClass('active');
                        $('#booking-detail #button-terminer').show();
                        $('#booking-detail #button-begin').hide();
                    }else {
                        $('#booking-detail #step1').removeClass('active');
                        $('#booking-detail #step2').removeClass('active');
                        $('#booking-detail #button-terminer').hide();
                        $('#booking-detail #button-begin').show();
                    }

                    if (resp.coach_confirm && resp.status <= 2) {
                        $('#booking-detail #step1').addClass('active');
                        $('#booking-detail #button-begin').show();
                        $('#booking-detail #button-terminer').hide();
                    } else {
                        $('#booking-detail #step1').removeClass('active');
                        $('#booking-detail #button-begin').hide();
                    }

                    if (resp.coach_confirm == false){
                        $('#booking-detail #button-div').show();
                    } else {
                        $('#booking-detail #button-div').hide();
                    }
                    if(resp.coach_confirm == false) {
                        $('#booking-detail #button-reject').show();
                    } else {
                        $('#booking-detail #button-reject').hide();
                    }

                    $('#bookingModal').modal('show');
                //   if (resp.coach_confirm == 1 && resp.status < 2) {
                //     $('#booking-detail #step1').addClass('active');
                //     $('#booking-detail #button-begin').show();
                //     $('#booking-detail #button-terminer').hide();
                //   } else {
                //     $('#booking-detail #step1').removeClass('active');
                //     $('#booking-detail #button-begin').hide();
                //   }
                //   if (resp.status == 3) {
                //     $('#booking-detail #step2').addClass('active');
                //     $('#booking-detail #button-terminer').show();
                //     $('#booking-detail #button-begin').hide();
                //   }else {
                //     $('#booking-detail #step2').removeClass('active');
                //     $('#booking-detail #step1').removeClass('active');
                //     $('#booking-detail #button-terminer').hide();
                //   }
                //   if (resp.status == 4) {
                //     $('#booking-detail #step1').addClass('active');
                //     $('#booking-detail #step2').addClass('active');
                //     $('#booking-detail #step3').addClass('active');
                //   }
                //    else {
                //     $('#booking-detail #step3').removeClass('active');
                //   }
                //   if (resp.coach_confirm == false){
                //     $('#booking-detail #button-div').show();
                //   } else {
                //     $('#booking-detail #button-div').hide();
                //   }
                //   if(resp.coach_confirm == false) {
                //       $('#booking-detail #button-reject').show();
                //   } else {
                //     $('#booking-detail #button-reject').hide();
                //   }
                  if (type_modal == "customer") {
                    $('#booking-detail #button-reject').hide();
                    $('#booking-detail #button-div').hide();
                    $('#booking-detail #customer_detail').hide();
                    $('#booking-detail #button-begin').hide();
                    $('#booking-detail #button-terminer').hide();
                    $('#booking-detail #coach_detail').hide();
                  }else{
                    $('#booking-detail #customer_detail').show();
                    $('#booking-detail #coach_detail').show();
                  }
                //   $('#bookingModal').modal('show');
              }
              , error: function(error) {
                  unblockUI();
                  console.log(error);
              }
          });
          e.preventDefault();
        });
    });

</script>
@endpush
