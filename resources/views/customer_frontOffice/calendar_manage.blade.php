@extends("customer_frontOffice.layouts_customer.master_customer")


@section('content')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
    <style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }

    a {
        color: black;
    }
    .datepicker {
    background-color:#edaa0d !important;
    color: #fff !important;
    border: none;
    padding: 10px !important
}

.datepicker-dropdown:after {
    border-bottom: 6px solid #edaa0d
}

thead tr:nth-child(3) th {
    color: #fff !important;
    font-weight: bold;
    padding-top: 20px;
    padding-bottom: 10px
}

.dow,
.old-day,
.day,
.new-day {
    width: 40px !important;
    height: 40px !important;
    border-radius: 0px !important
}

.old-day:hover,
.day:hover,
.new-day:hover,
.month:hover,
.year:hover,
.decade:hover,
.century:hover {
    border-radius: 6px !important;
    background-color: #eee;
    color: #edaa0d;
}

.active {
    border-radius: 6px !important;
    background-image: linear-gradient(#90CAF9, #64B5F6) !important;
    color: #edaa0d !important
}

.disabled {
    color: #616161 !important
}

.prev,
.next,
.datepicker-switch {
    border-radius: 0 !important;
    padding: 20px 10px !important;
    text-transform: uppercase;
    font-size: 20px;
    color: #fff !important;
    opacity: 0.8
}

.prev:hover,
.next:hover,
.datepicker-switch:hover {
    background-color: inherit !important;
    opacity: 1
}

.cell {
    border: 1px solid #BDBDBD;
    border-radius: 5px;
    margin: 2px;
    cursor: pointer
}

.cell:hover {
    border: 1px solid #1c6a49;
}

.cell.select {
    background-color: #edaa0d;
    color: #fff
}

.fa-calendar {
    color: #fff;
    font-size: 30px;
    padding-top: 8px;
    padding-left: 5px;
    cursor: pointer
}

</style>
@endsection
<section>
    <header class="profil-header">
        <div class="dashboard-header-des">
            <h3 class="font-weight-bold text-center">Calendier </h3>
            <p class="text-center">
                Gérez vos disponibilités et vos réservations en quelques clics
            </p>
        </div>
    </header>
</section>
<section class="mb-5">
    @include('coreui-templates::common.errors')
    @include('flash::message')
    @if(count($availabilities) > 0)
        <div class="card card-body">
            <div class="mx-2">
                <h1 class="text-center">Mes disponibilités</h1>
                <div class="container">
                    <div class="mt-2 py-2">

                        @foreach($availabilities as $key => $value)
                            <h2>{{$key}}</h2>
                            <div class="row text-center my-4">
                                @foreach($value as $el)
                                    <div class="col-md-2 col-4 my-2 px-2">
                                        <div class="cell py-1" data-id="{{$el['id']}}">{{$el['start_time']}} - {{$el['end_time']}}</div>
                                    </div>
                                @endforeach
                            </div>
                            @foreach($value as $el)
                                @php
                                    $availability = \App\Models\Availability::find($el['id']);
                                    // dd($availability);
                                @endphp
                                @if(count($availability->availabity) > 0)
                                    <div class="my-2">
                                        <h3> Personnes inscritent pour le créneau {{$el['start_time']}} - {{$el['end_time']}} <span class="badge badge-info"></span> </h3>
                                        <table class="table my-3" id="customers-table">
                                            <thead class="text-primary">
                                                <tr>
                                                    <th>Nom</th>
                                                    <th>Contact</th>
                                                    <th>Email</th>
                                                    <th>Civilité</th>
                                                    <th>Reference Reservation</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($availability->availabity as $data)
                                                    <tr>
                                                        <td>{{$data->challenger->name}}</td>
                                                        <td>{{$data->challenger->phone}}</td>
                                                        <td>{{$data->challenger->email}}</td>
                                                        <td>{{$data->challenger->gender}}</td>
                                                        <td>{{$data->booking->booking_ref}}</td>
                                                        <td>{!! $data->booking->status == 1 && $data->booking->coach_confirm == 1 ? '<a class="btn btn-primary">Reservation Confirmer</a>' : '<a class="btn btn-danger">Reservation En attente</a>' !!}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                            @endforeach
                        @endforeach
                    </div>
                <div>
            </div>
        </div>
    @endif
    <div class="container">
        <hr>
        <div class="py-2">
            <h1 class="text-center">Gérer mes disponibilités</h1>
            <form action="{{ route('coach.calendar.post') }}" method="post">
                @csrf
                <input type="hidden" name="coach_id" value="{{Auth::guard('customer')->user()->is_coach->id }}">
                <div class="profil-box-content">
                    <div class="mb-2">
                        <div class="mx-0 mb-0 row justify-content-sm-center justify-content-start px-1">
                            <input type="text" id="datepicker-booking" class="datepicker" name="date" placeholder="selectionner des dates" required readonly>
                        <span class="fa fa-calendar"></span>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="form-group col-sm-6 my-2 ">
                        <label for="start_hour">Heure de début</label>
                        <input class="form-control" type="time" id="start_hour" name="start_hour">
                    </div>
                    <div class="form-group col-sm-6  my-2">
                        <label for="end_hour">Heure de fin</label>
                        <input class="form-control" type="time" id="end_hour" name="end_hour">
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-sm-9"></div>
                    <div class="col-sm-3">
                        <input type="submit" value="Enregistrer" class="btn btn-primary right">
                    </div>
                </div>
            </form>
                <!-- <input type="hidden" name="input-time" id="input-time">
                <div class="mt-2" id="show-time-content">
                    <div class="row text-center mx-0">
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="9:00AM">9:00AM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="9:30AM">9:30AM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="9:45AM">9:45AM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="10:00AM">10:00AM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="10:30AM">10:30AM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="10:45AM">10:45AM</div>
                        </div>
                    </div>
                    <div class="row text-center mx-0">
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="11:00AM">11:00AM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="11:30AM">11:30AM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="11:45AM">11:45AM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="12:00PM">12:00PM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="12:30PM">12:30PM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="12:45PM">12:45PM</div>
                        </div>
                    </div>
                    <div class="row text-center mx-0">
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="1:00PM">1:00PM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="1:30PM">1:30PM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="1:45PM">1:45PM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="2:00PM">2:00PM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="2:30PM">2:30PM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="2:45PM">2:45PM</div>
                        </div>
                    </div>
                    <div class="row text-center mx-0">
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="3:00PM">3:00PM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="3:30PM">3:30PM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="4:15PM">4:15PM</div>
                        </div>
                        <div class="col-md-2 col-4 my-1 px-2">
                            <div class="cell py-1 timepicker" time="5:00PM">5:00PM</div>
                        </div>
                    </div>
                </div> -->
                <!-- </div> -->
            <!-- </div> -->
        </div>
    </div>
</section>
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
<script>
$(document).ready(function(){

    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        // autoclose: true,
        startDate: '0d',
        multidate: true,
        minDate: moment(),
        todayHighlight: true
    });

    $('.cell').click(function(){
        $('.cell').removeClass('select');
        $(this).addClass('select');
    });

});
</script>
@endpush
