@extends("customer_frontOffice.layouts_customer.master_customer")


@section('content')

@section('css')

<style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }

    a {
            color: black;
    }

</style>
@endsection
{{-- <!-- nav -->
<section class="nav-section">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light ">
            <a class="navbar-brand" href="index.html">
                <img src="images/gh1.png" alt="Logo" width="100px">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <div class="">
                    <ul class="navbar-nav">
                        <li class="nav-item" style="position: relative;">
                            <a class="nav-link btn nav-item  " href="#" data-toggle="dropdown">Services<span class="sr-only">(current)</span></a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item " href="coachs.html">Self Challenger</a>
                                <a class="dropdown-item " href="become_a_coach.html">Coach</a>
                                <a class="dropdown-item " href="become_patner.html">Partner</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mr-1" href="event.html"> Events</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mr-3" href="shop.html"> Shop</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link mr-3" href="#"> Blog</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link mr-3" href="#"> A Propos</a>
                        </li>
                    </ul>
                </div>
                <div class="ml-auto">
                    <!-- Tableau de bord -->

                    <a href="/dashboard" class="dashboard-button mr-1">
                        <small> Tableau de bord</small>
                    </a>

                    <a href="#" class="profil-button">

                        <i class="fa fa-user mr-2" aria-hidden="true"></i>
                        Jean Michel
                    </a>


                </div>
            </div>
        </nav>
    </div>
</section> --}}

<!-- header -->

<section>
    <header class="profil-header">
        <div class="dashboard-header-des">
            <h3 class="font-weight-bold text-center">Mon Profil </h3>
            <p class="text-center">
                Gérez mes informations
            </p>
        </div>
    </header>
</section>
<!-- Profil details -->

<section>
    <div class="container">
        <div class="py-2">
            <div class="profil-box-content">
                <div class="row">
                    <div class="col-lg-7 col-md-7 col-sm-12 d-flex">

                        <div class="mr-4">
                            <img src="{{$customer->photo_url}}" class="rounded-circle" height="110" width="110" alt="">
                        </div>

                        <div class="mb-4">
                            <div>
                                <h4 class="font-weight-bold">{{$customer->first_name}} {{$customer->last_name}}</h4>
                                <?php $date_start = Carbon\Carbon::parse($customer->created_at)->locale('fr_FR'); ?>
                                <p>Inscrit depuis le {{$date_start->isoFormat('LL')}}</p>
                                <p>Statut : <span class="self-challenger-box">@if($customer->is_coach) Coach @else Self Challenger @endif</span></p>

                            </div>

                            <div class="mt-4">
                              <a href="/customer/profile/settings" class="grey-button">Modifier profil</a>
                            </div>
                        </div>
                    </div>

                    <!-- divider -->

                    <!-- <div class="text-center">
                            <div class="verticals-divider "></div>
                        </div> -->

                    {{-- <div class="col-lg-4 col-md-3 col-sm-12  py-3 ">
                        <form role id="calcul-imc-form" method="POST" action="/customer/update/profile">
                                @csrf
                            <div class="d-flex">
                                <div class="text-center">
                                    <img src="images/height.png" width="40" height="40" alt="">
                                    <p><small>Taille en cm</small></p>
                                    <input type="number" name="size" placeholder="0" value="{{$customer->size}}" class="calcul-input" required>
                                </div>
                                <div class="text-center">
                                    <img src="images/weight.png" width="40" height="40" alt="">
                                    <p><small>Poids actuelle en kg</small></p>
                                    <input type="number"  name="weight" placeholder="0" value="{{$customer->weight}}" class="calcul-input" required>
                                </div>
                            </div>
                            <button type="submit" class="grey-pale-button mt-2 col-lg-10">Enregistrer</button>
                        </form>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Statistiques -->
{{--@if($customer->is_coach)

<section>
    <div class="container">
        <div class="py-2">
            <div class="profil-box-content">
                <h5 class="font-weight-bold">Statistiques </h5>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                        <h3 class="font-weight-bold primary-text-color">{{ $customer->is_coach->pastCourseCount() }}</h3>
                        <p>Cours effectués</p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                        <h3 class="font-weight-bold primary-text-color">{{ $customer->is_coach->reservationLeftCount() }}</h3>
                        <p>Cours réservés</p>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                        <h3 class="font-weight-bold primary-text-color ">{{ $customer->is_coach->goldenPoint() }}</h3>
                        <p>Points Goden Health</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endif

<!-- Cours effectutés -->
@if(isset($availabilities) && !is_null($availabilities))
<section>
    <div class="container">
        <div class="py-2">
            <div class="courses-makes">
            <!-- <div class="courses-makes"> -->
                <div>
                    <h5 class="font-weight-bold">Cours éffectués</h5>
                    @php
                        $courses = collect($availabilities)->filter(function($availability){
                            return $availability->isPast();
                        })->map(function($availability){
                            return $availability->course;
                        });
                    @endphp
                    @if(count($courses) == 0)
                        <p>Vous n'avez pas encore effectué de cours</p>
                    @else
                        <table class="table table-striped mt-3">
                            <thead>
                                <tr>
                                    <th>Discipline</th>
                                    <th>Type de cours</th>
                                    <th>Prix</th>
                                    <th>Nombre de personne</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($courses as $course)
                                    <tr>
                                        <th>{{$course->discipline->name ?? ''}}</th>
                                        <th>{{$course->course_type}}</th>
                                        <th>{{$course->price}}</th>
                                        <th>{{$course->detail->nb_person ?? ''}}</th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Heures de pratiques -->
<section>
    <div class="container">
        <div class="py-2">
            <div class="profil-box-content">
                <div>
                    <h5 class="font-weight-bold">Heures de pratiques</h5>
                    @php
                        $left_pratice_time = collect($availabilities)->filter(function($availability){
                            return $availability->isPast() == false;
                        });
                    @endphp
                    @if(count($left_pratice_time) == 0)
                        <p>Vous n'avez pas effectué de cours</p>
                    @else
                        <table class="table table-striped mt-3">
                            <thead>
                                <tr>
                                    <th>Course</th>
                                    <th>Date de debut</th>
                                    <th>Date de fin</th>
                                    <th>Heure de depart</th>
                                    <th>Heure de fin</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($left_pratice_time as $availability)
                                  @if($availability->course && $availability->course->discipline)
                                  <tr>
                                      <td>{{$availability->course->discipline->name ?? ''}}</td>
                                      <td>{{ $availability->date_debut }}</td>
                                      <td>{{ $availability->date_fin }}</td>
                                      <td>{{ $availability->start_time }}</td>
                                      <td>{{ $availability->end_time }}</td>
                                      <td>
                                          <a href="/customer/lessons/manage?action=update&course_id={{$availability->course->id}}" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"> </i> Modifier </a>
                                          <a href="#" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown"> <span class="caret"></span> </a>
                                          <ul class="dropdown-menu">
                                              <li class="px-1">
                                              {!! Form::open(['route' => ['courseCoaoch.destroy', $availability->course->id], 'method' => 'delete']) !!}
                                                  {!! Form::button('<i class="fa fa-trash"></i> Supprimer', ['type' => 'submit', 'class' => 'btn
                                                  btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                              {!! Form::close() !!}
                                              </li>
                                          </ul>
                                      </td>
                                  </tr>
                                  @endif
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>

    </div>
</section>
@endif--}}
@endsection
