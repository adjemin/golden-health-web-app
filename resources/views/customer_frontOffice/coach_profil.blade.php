@extends("customer_frontOffice.layouts_customer.search_master_customer")
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
    <style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }

    a {
        color: black;
    }
    .datepicker {
    background-color:#edaa0d !important;
    color: #fff !important;
    border: none;
    padding: 10px !important
}

.datepicker-dropdown:after {
    border-bottom: 6px solid #edaa0d
}

thead tr:nth-child(3) th {
    color: #fff !important;
    font-weight: bold;
    padding-top: 20px;
    padding-bottom: 10px
}

.dow,
.old-day,
.day,
.new-day {
    width: 40px !important;
    height: 40px !important;
    border-radius: 0px !important
}

.old-day:hover,
.day:hover,
.new-day:hover,
.month:hover,
.year:hover,
.decade:hover,
.century:hover {
    border-radius: 6px !important;
    background-color: #eee;
    color: #edaa0d;
}

.active {
    border-radius: 6px !important;
    background-image: linear-gradient(#90CAF9, #64B5F6) !important;
    color: #edaa0d !important
}

.disabled {
    color: #616161 !important
}

.prev,
.next,
.datepicker-switch {
    border-radius: 0 !important;
    padding: 20px 10px !important;
    text-transform: uppercase;
    font-size: 20px;
    color: #fff !important;
    opacity: 0.8
}

.prev:hover,
.next:hover,
.datepicker-switch:hover {
    background-color: inherit !important;
    opacity: 1
}

.cell {
    border: 1px solid #BDBDBD;
    border-radius: 5px;
    margin: 2px;
    cursor: pointer
}

.cell:hover {
    border: 1px solid #1c6a49;
}

.cell.select {
    background-color: #edaa0d;
    color: #fff
}

.fa-calendar {
    color: #fff;
    font-size: 30px;
    padding-top: 8px;
    padding-left: 5px;
    cursor: pointer
}

#price_total {
    display: block;
    font-weight: bold;
    margin: 5px;
}


.fc-header-toolbar {
  /*
  the calendar will be butting up against the edges,
  but let's scoot in the header's buttons
  */
  padding-top: 1em;
  padding-left: 1em;
  padding-right: 1em;
}
@media (min-width: 576px) {
  .modal-dialog {
      max-width: 1024px;
  }
}

#DateModal{
  display:none;
}
.hide{
  display: none;
}
.closepopover{
  margin-right: 10px;
}
</style>
@endsection
@section('content')
<section>
    <div class="container py-3">
        <div class="coach-identity">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div>
                        <img src="{{$coach->customer->photo_url}}" class="img-fluid" alt="">
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 py-3">
                    <div class="">
                        <div class="">
                            <h4 class="font-weight-bold">{{$coach->customer->name}}</h4>
                        </div>
                        <div class="">
                            <p>Coach {{$course->discipline->name}}</p>
                        </div>
                        <!--div class="">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc erat augue, dapibus
                                sed felis at, imperdiet pretium .</p>
                        </div-->
                        {{-- {!! $coach->ratingView() !!} --}}
                        <div class="mb-4">
                            @php
                                $rating = (int) $coach->rating;
                            @endphp
                            @for ($i = 0; $i < 5; $i++)
                                @if($i+ 1 <= $rating)
                                    <img src="{{ asset('customer/images/stars.png')}}" class="icon" alt="">
                                @else
                                    <img src="{{ asset('customer/images/star.png')}}" class="icon" alt="">
                                @endif
                            @endfor
                        </div>
                        <div>
                            <a href="#">
                                <div class="tarifs-button">
                                    Tarif Indicatif <br>
                                    <span class="secondary-text-color font-weight-bold h5" id="price_span">{{$course->price}} XOF</span>
                                </div>
                                <span id="price_total" style="display:none">Total à régler : <span id="span_price_content"></span>  </span>
                                <input type="hidden" name="course_price" id="course_price" value="{{$course->price}}">
                                <input type="hidden" name="final_price" id="final_price">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 py-3">
                    <div class="col-md-1O">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Je souhaite que</label>
                            <select id="packs" name="packs" class="form-control packs">
                                @if($packs)
                                    @foreach($packs as $pack)
                                        @if ($pack->slug == "cours-unique")
                                            <option value="{{$pack->id}}" discount="{{$pack->percentage_discount}}">{{$pack->name}}</option>
                                        @else
                                            <option value="{{$pack->id}}" discount="{{$pack->percentage_discount}}">{{$pack->name}} (- {{($pack->percentage_discount)}} %)</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                          <label for="nb_personnes">Nombre de personnes</label>
                            <select id="nb_personnes" name="nb_personnes" class="form-control nb_personnes">
                                @if($course->course_type == null || $course->course_type == "cour-particulier")
                                <!-- <option value="1">Cours particulier</option> -->
                                <option value="1">1 pers</option>
                                @elseif(isset($course->course_type_object) && !empty($course->course_type_object))
                                    <optgroup label="{{$course->course_type_object->name}}">
                                        @if(isset($course->detail) && !empty($course->detail))
                                          @for ($i = 1; $i <= $course->detail->nb_person; $i++) {
                                            <option value="{{$i}}">{{$i}} (pers)</option>
                                          @endfor
                                        @endif
                                    </optgroup>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="mb-3">
                        @if(!empty($availabilities))
                            <button type="button" id="go-booking" class="secondary-button btn-block text-center">Réserver</button>
                        @else
                            <p>Ce cours ne dispose plus de disponibilité valide</p>
                        @endif
                    </div>

                    {{--<div class="mb-4">
                        <button type="button" id="go-available" class="primary-button btn-block">
                            <img src="{{asset('customer/images/calendar.png')}}" class="mr-2" width="20" height="20" alt="">
                            Disponibilité
                        </button>
                    </div>

                    <div class="ask-queestion mt-5">
                        <img src="{{asset('customer/images/conversation (1).png')}}" height="30" width="30" alt=""> Posez votre
                        question
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</section>

{{-- Details --}}

<section>
    <div class="container py-3">
        <div class="coach-identity">
            <div class="row">
                <div>
                    <h4 class="primary-text-color font-weight-bold mb-4">Son expérience de coach</h4>

                    <p>
                       @if ($course && $course->detail ) {!! $course->detail->experience !!} @endif
                    </p>
                </div>
            </div>
            <div class="row mt-3">
                <div>
                    <h4 class="primary-text-color font-weight-bold mb-2">Sa séance type</h4>

                    <p>
                      @if ($course && $course->detail )  {!! $course->detail->seance_type !!} @endif
                    </p>
                </div>
            </div>

            <!-- Caractéristiques -->
            <div>
                <div class="row mt-3 mb-4">
                    <div>
                        <h4 class="primary-text-color font-weight-bold mb-2"> Caractéristiques</h4>
                    </div>
                </div>

                <div class="row">
                    @if($course)
                      <div class="col-lg-3 col-md-3 col-sm-12">
                          <div class="text-center">
                              <img src="{{ asset('customer/images/university.png') }}" width="50" height="50" alt="">
                          </div>
                          <div class="text-center font-weight-bold mt-3">Diplôme</div>
                          <div class="text-center mt-3">
                            @if($course->detail && $course->detail->diplome_url)
                                @if($course->detail->diplome_url && $course->detail->diplome_name)
                                    <a href="{{$course->detail->diplome_url}}" target="_blank">{{$course->detail->diplome_name}}</a>
                                @else
                                    <a href="{{$course->detail->diplome_url}}" target="_blank">Diplomé</a>
                                @endif
                            @endif
                          </div>
                      </div>
                    @endif
                    @if($course)
                      <div class="col-lg-3 col-md-3 col-sm-12">
                          <div class="text-center">
                              <img src="{{ asset('customer/images/pin.png') }}" width="50" height="50" alt="">
                          </div>
                          <div class="text-center font-weight-bold mt-3">Le cours a lieu </div>
                          <div class="text-center mt-3">
                            @php
                                $lieux = collect($course->lieu)->map(function($lieu){
                                    return $lieu->lieucourse->name ?? null;
                                })->reject(function($lieu){
                                    return is_null($lieu) || empty($lieu);
                                })->unique();
                            @endphp
                            @foreach($lieux as $lieu)
                                <p>{{$lieu}}</p>
                            @endforeach
                          </div>
                      </div>
                    @endif
                    @if($course)
                      <div class="col-lg-3 col-md-3 col-sm-12">
                          <div class="text-center">
                              <img src="{{ asset('customer/images/religion (1).png') }}" width="50" height="50" alt="">
                          </div>
                          <div class="text-center font-weight-bold mt-3">Objectif principal du cours</div>
                          <div class="text-center mt-3">
                            @php
                                $courseObjectives = collect($course->courseObjective)->map(function($courseObjective){
                                    return $courseObjective->objective->name ?? null;
                                })->reject(function($courseObjective){
                                    return is_null($courseObjective) || empty($courseObjective);
                                })->unique();
                            @endphp
                            @foreach($courseObjectives as $courseObjective)
                                <p>{{$courseObjective}}</p>
                            @endforeach
                          </div>
                      </div>
                    @endif
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="text-center">
                            <img src="{{ asset('customer/images/team.png') }}" width="50" height="50" alt="">
                        </div>
                        <div class="text-center font-weight-bold mt-3">Nombre de personnes</div>
                        <div class="text-center mt-3">
                            @if($course && $course->detail)
                              <p>{{$course->detail->nb_person}}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <!--  Portfolio -->
                <div class="row mt-5 mb-2">
                    <div>
                        <h4 class="primary-text-color font-weight-bold mb-2"> Portfolio</h4>
                    </div>
                </div>
                @if($portfolios->count() != 0)
                    @foreach($portfolios as $portfolio)
                    <div class="row">
                        <!-- Display the first Portfolio -->
                        @if($loop->first)
                            <div class="col-6">
                                <div>
                                    @if($portfolio->media_type == "youtube")
                                        <div class="embed-responsive embed-responsive-21by9 embed-responsive-16by9 embed-responsive-4by3 embed-responsive-1by1">
                                            <iframe class="embed-responsive-item" width="560" height="315" src="{{$portfolio->video_url }}" title="YouTube video player" frameborder="0"  allowfullscreen></iframe>
                                        </div>
                                    @elseif($portfolio->media_type == "facebook")
                                    <iframe src="{{$portfolio->video_url }}" width="560" height="314" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allowFullScreen="true"></iframe>
                                    @else
                                        <img src="{{ $portfolio->images_url }}" alt="{{ $portfolio->images_url }}" type="">
                                    @endif
                                    {{--<img src="{{ asset('customer/images/401-felix-6175.png') }}" class="img-fluid" alt="">--}}
                                    <h5 class="text-center h5">
                                        {{$portfolio->title}}
                                    </h5>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="row">
                        @elseif($loop->iteration <= 5)
                        {{-- class="col-lg-{{intval(12/$portfolios->count())}} col-md-{{intval(12/$portfolios->count())}} col-sm-12 mt-2" --}}
                            <div class="col-6">
                                <div>
                                    @if($portfolio->media_type == "youtube")
                                        <div class="embed-responsive embed-responsive-21by9 embed-responsive-16by9 embed-responsive-4by3 embed-responsive-1by1">
                                            <iframe class="embed-responsive-item" width="560" height="315" src="{{$portfolio->video_url }}" title="YouTube video player" frameborder="0"  allowfullscreen></iframe>
                                        </div>
                                    @elseif($portfolio->media_type == "facebook")
                                        <iframe src="{{$portfolio->video_url }}" width="560" height="314" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allowFullScreen="true"></iframe>
                                    @else
                                        <img src="{{ $portfolio->images_url }}" alt="{{ $portfolio->title }}" type="">
                                    @endif
                                    {{--<img src="{{ asset('customer/images/401-felix-6175.png') }}" class="img-fluid" alt="">--}}
                                    <div class="text-center">
                                        {{$portfolio->title}}
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    @endforeach
                @else
                <div class="margin:20px 0; text-align:center">
                    Ce catch n'a pas encore ajouté de portfolio.
                </div>
                @endif

                <!--  Avis clients -->
                <div class="row mt-5 mb-2">
                    <div>
                        <h4 class="primary-text-color font-weight-bold mb-2"> Avis clients</h4>
                    </div>
                </div>
                @if($reviews)
                  @foreach($reviews as $review)
                    @if($review && isset($review->customer))
                    <div class="row mt-5 ">
                        <div class="col-lg-2 col-md-2 col-sm-12">
                            <img src="{{$review->customer->photo_url}}" height="80" width="80" class="rounded-circle "
                                alt="">
                        </div>

                        <div class="col-lg-10 col-md-10 col-sm-12">
                            <p class="font-weight-bold">{{$review->customer->name ?? ""}} le {{$review->created_at}} </p>
                            <div class="mb-3">
                              @for ($i = 0; $i < $review->rating; $i++)
                                <img src="{{ asset('customer/images/stars.png')}}" class="icon" alt="">
                              @endfor
                                {{--<a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
                                <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
                                <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
                                <a href="#"><img src="{{ asset('customer/images/star.png') }}" class="icon" alt=""></a>
                                <a href="#"><img src="{{ asset('customer/images/star.png') }}" class="icon" alt=""></a>--}}
                            </div>
                            <div>
                                <p>
                                  {{$review->content ?? ''}}
                                </p>
                            </div>
                        </div>
                    </div>
                    @endif
                  @endforeach
                @endif
                <hr>
                <!--div class="row mt-5 ">
                    <div class="col-lg-2 col-md-2 col-sm-12">
                        <img src="{{ asset('customer/images/401-ted3467-jj-a_2.png') }}" height="80" width="80" class="rounded-circle "
                            alt="">
                    </div>

                    <div class="col-lg-10 col-md-10 col-sm-12">
                        <p class="font-weight-bold">Sandrine Tanoh le 09/11/2019</p>
                        <div class="mb-3">
                            <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
                            <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
                            <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
                            <a href="#"><img src="{{ asset('customer/images/star.png') }}" class="icon" alt=""></a>
                            <a href="#"><img src="{{ asset('customer/images/star.png') }}" class="icon" alt=""></a>
                        </div>
                        <div>
                            <p>
                                Attentif - Merveilleux - Sympathique - Bon conseils - Séance adaptée - Efficace
                                -
                                Contact facile - Pedagogue - Dynamique - Patient
                            </p>
                        </div>
                    </div>
                </div-->
            </div>

        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="DateModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Choisissez un créneau</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id='calendar-container'>
          <div id='calendarModal'></div>
          <input type="hidden" id="input-slot" name="input-slot"/>
          <input type="hidden" id="input-datestart" name="datestart"/>
          <input type="hidden" id="input-dateend" name="dateend"/>
        </div>
      </div>
      {{--
        <div class="modal-footer">
          <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
          <button type="button" id="go-date-booking" class="btn btn-secondary">Réserver</button>
        </div>
        --}}
    </div>
  </div>
</div>
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepickeRéserver ce créneau ?r/1.9.0/js/bootstrap-datepicker.js"></script>
<script src="{{asset('js/fullcalendar.js')}}"></script>
<script src="{{asset('js/locales/fr.js')}}"></script>
<script>

var date;

$(document).ready(function(){

    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        startDate: '0d'
    });

    $('.cell').click(function(){
        $('.cell').removeClass('select');
        $(this).addClass('select');
        // $('#datepicker-booking').val($(this).data('id'))
        date = $(this).data('id')
    });
});

      var EventData = '<?php echo json_encode($availabilities); ?>';
      console.log(EventData);
      var popoverElement;

      var popTemplate = [
          '<div class="popover" style="max-width:600px;" >',
          '<div class="arrow"></div>',
          '<div class="popover-header">',
          '<button id="closepopover" type="button" class="close" aria-hidden="true">&times;</button>',
          '<h3 class="popover-title"></h3>',
          '</div>',
          '<div class="popover-content"></div>',
          '</div>'].join('');

    $('#DateModal').on('shown.bs.modal', function () {

      var calendarEl = document.getElementById('calendarModal');

      var calendar = new FullCalendar.Calendar(calendarEl, {
        height: 'auto',
        contentHeight: 'auto',
        expandRows: true,
        allDaySlot: false,
        slotMinTime: '06:00',
        slotMaxTime: '23:00',
        headerToolbar: {
          left: 'prev,next today',
          center: 'title',
          right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
        },
        initialView: 'timeGridWeek',
        locale: 'fr',
        initialDate: new Date(),
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        selectable: false,
        nowIndicator: true,
        dayMaxEvents: true, // allow "more" link when too many events
        events: JSON.parse(EventData),
        eventClick: function(info) {
           console.log(info);
           console.log(info.event._def.publicId);
           console.log('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
           var event_id = info.event._def.publicId;
           var start = info.event.start;
           var end = info.event.end;
           console.log(start);
           console.log(end);
           closePopovers();
           popoverElement = $(info.el);
            popoverElement.popover({
                title: "Réserver ce créneau ?",
                content: function() {
                  $('#input-slot').val(event_id);
                  $('#input-datestart').val(start);
                  $('#input-dateend').val(end);
                   return  "<div>"+
                            "<a class='btn btn-light btn-xs closepopover'>Annuler</a>"+
                            "<a class='btn btn-primary btn-xs goToBooking' data-id='"+event_id+"' data-start='"+start+"' data-end='"+end+"'>Réserver</a>"+
                           "</div>";

                },
                placement: 'top',
                html: true,
                trigger: 'click',
                animation: true,
                container: 'body'
            }).popover('show');
         }
      });

      calendar.render();
    });


    function closePopovers() {
        $('.popover').not(this).popover('hide');
    }

    $('body').on('click', function (e) {
        if (popoverElement && ((!popoverElement.is(e.target) && popoverElement.has(e.target).length === 0 && $('.popover').has(e.target).length === 0) || (popoverElement.has(e.target) && e.target.id === 'closepopover'))) {
            closePopovers();
        }
    });

    $(document).on("click", ".closepopover", function(e) {
      closePopovers();
    });

    $('#go-available').on('click', function(){
        $('#availableModal').modal('toggle');
    });


    /*$('#DateModal').on('shown.bs.modal', function () {
      $("#calendarModal").fullCalendar('render');
    });*/


    $('#go-booking').on('click', function(){
        $('#DateModal').modal('toggle');
        //alert('ok');
    });

    $('#datepicker-booking-available').change(function(){
        $('#show-content-avalable').hide();
        $('#show-content-avalable').show();
    });

    $('#datepicker-booking').change(function(){
        $('#show-time-content').hide();
        $('#show-time-content').show();
    });

    $('.timepicker').on('click', function(){
        var time = $(this).attr('time');
        var date = $('#datepicker-booking').val();
        $('#input-time').val(time);
        console.log(time);
        console.log(date);
    });

    $('#packs').on('change', function(e){
        var pack = $(this).val();
        var discount = $('option:selected', this).attr('discount');
        var price = $('#course_price').val();
        console.log(pack);
        console.log(discount);
        if (parseInt(discount) > 0) {
            var discount_price  = (parseInt(price) * discount) / 100;
            var final_price =  (parseInt(price) - discount_price);
            $('#price_span').text(final_price +' XOF');
            $('#final_price').val(final_price);
            if (pack == 2) {
                var $res_price = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'XOF' }).format(final_price * 5);
                $('#span_price_content').text($res_price);
            } else if(pack == 3) {
                var $res_price = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'XOF' }).format(final_price * 10);
                $('#span_price_content').text($res_price);
            } else if(pack == 4) {
                var $res_price = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'XOF' }).format(final_price * 30);
                $('#span_price_content').text($res_price);
            }
            $('#price_total').show();
        }else {
            $('#final_price').val(price);
            $('#price_span').text(price +' XOF');
            $('#price_total').hide();
        }
    });
      $(document).on("click", ".goToBooking", function(e) {
        // var time = $('#input-time').val();
        var slot = $("#input-slot").val();
        var startDate = $("#input-datestart").val();
        var endDate = $("#input-dateend").val();
        console.log(slot);
        // alert(date)
        var pack = $("#packs").val();
        var nbperson = $("#nb_personnes").val();

        // if (time && date) {
        if (slot != null && slot != "" && slot != undefined && startDate && endDate) {
            var courseID = "{{$course->id}}";
            window.location.href = "/course/reserver/"+btoa(courseID)+"?slot="+btoa(slot)+"&pack="+btoa(pack)+"&nbperson="+nbperson+"&start="+startDate+"&end="+endDate;
            // window.location.href = "/course/reserver/"+btoa(courseID)+"?date="+btoa(date)+"&time="+btoa(time)+"&pack="+btoa(pack)+"&nbperson="+nbperson;
        } else {
            Swal.fire({
                icon: 'info',
                title: 'Oops...',
                text: 'Veuillez obligatoirement choisir une date et une heure !' ,
            });
        }
    });
</script>
@endpush
