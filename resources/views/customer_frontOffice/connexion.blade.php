@extends("customer_frontOffice.layouts_customer.master_customer")


@section('css')

<style>
    body {
        background-image: linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, .6)), url("customer/images/bg2.jpg");
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        position: relative;
    }

    a {
        color: black;
    }

</style>


@section('content')

<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="offset-md-3 col-lg-7 col-md-7 col-sm-12">
                <div class="login-box">
                    <center>
                        <h4> <b>Me connecter</b> </h4>
                    </center>
                    <br>
                    <center>
                        <small>Pas encore membre Golden health ? <span><a href="#" class="secondary-text-color">Inscrivez-vous
                                </a></span></small>
                    </center>
                    <div class="row mt-3">
                        <div class="col-md-6 mt-2">
                            <div class="login-google">
                                <a href="#"> <img src="{{asset ('customer/images/téléchargement.png') }}" width="15px" height="15px" alt="">
                                    &nbsp; &nbsp;
                                    Connexion avec
                                    google</a>
                            </div>
                        </div>
                        <div class="col-md-6 mt-2">
                            <div class="login-facebook">
                                <a href="#"> <img src="{{ asset('customer/images/facebook.png') }}" width="15px" height="15px" alt="">
                                    &nbsp; &nbsp; Connexion avec facebook</a>
                            </div>
                        </div>
                    </div>
                    <div class="row mx-auto mt-4">
                        <div class="mini-divider mt-3"></div> &nbsp;&nbsp;
                        <p>ou</p>&nbsp;&nbsp;
                        <div class="mini-divider mt-3"></div>
                    </div>

                    <form action="" class="">
                        <div class="form-group mt-3 ">
                            <input type="email" class="form-control input-style placeholder-style" id="email" aria-describedby="username" placeholder="Adresse email professionnelle">
                        </div>
                        <div class="form-group mt-2">
                            <input type="password" class="form-control input-style placeholder-style" id="password" aria-describedby="password" placeholder="Mot de passe">
                        </div>
                        <a href="#" class="password-forget ">J'ai perdu mon mot de passe</a>
                        <a href="{{ route('profile') }}" class="btn login-button col-lg-12 mt-4">Se connecter</a>
                        
                        {{-- <input type="submit" class="btn login-button col-lg-12 mt-4" value="Se connecter"> --}}
                    </form>

                </div>
            </div>
    </div>
        </div>
</section>

@endsection
