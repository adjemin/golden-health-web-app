@extends("customer_frontOffice.layouts_customer.master_customer")

@section('content')

@section('css')

<style>
   
</style>
@endsection


@section('content')
        
        <section>
            <header class="become-coach-header">
                <div class="become-coach-header-des">
                    <h3 class="font-weight-bold">S'inscrire à un cours </h3>
                    <p>
                        De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par </p>
                </div>
            </header>
        </section>

        <section>
            <div class=" py-5">
                <div class="container">
                    <div class="row ">
                        <h4 class="font-weight-bold text-primary">M'inscrire</h4>
                    </div>
                    <div class="row py-5">
                        <div class="col-lg-10 col-md-10 col-sm-12">
                            <form>
                                <div class="row mb-4 ml-1">
                                    <div class="form-check mr-4">
                                        <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
                                        <label class="form-check-label" for="gridRadios1">
                                            Mr
                                        </label>
                                    </div>

                                    <div class="form-check mr-4">
                                        <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2">
                                        <label class="form-check-label" for="gridRadios2">
                                            Mme
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="option3">
                                        <label class="form-check-label" for="gridRadios3">
                                            Mlle
                                        </label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <input type="text" class="form-control input-style placeholder-style" id="name" placeholder="Nom">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="text" class="form-control input-style placeholder-style" id="username" placeholder="Prénom">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="email" class="form-control input-style placeholder-style" id="email" placeholder="email">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="tel" class="form-control input-style placeholder-style" id="telephone " placeholder="telephone ">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="password" class="form-control input-style placeholder-style" id="password " placeholder="Mot de passe * ">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="password" class="form-control input-style placeholder-style" id="password1 " placeholder="Confirmer Mot de passe *">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="date" class="form-control input-style placeholder-style" id="date" placeholder="Date de naissance">
                                </div>

                                <div class="input-group mb-3">
                                    <select class="custom-select input-style placeholder-style" id="inputGroupSelect01">
                                        <option selected>Disciplines</option>
                                        <option value="1">Yoga</option>
                                        <option value="2">Danse</option>
                                        <option value="3">Football</option>
                                    </select>
                                </div>

                                <div class="input-group ">
                                    <div class="custom-file ">
                                        <input type="file" class="custom-file-input" id="inputGroupFile04">
                                        <label class="custom-file-label" for="inputGroupFile04">Ajouter une pièce
                                            d'identité , Diplôme ou certificats
                                        </label>
                                    </div>
                                </div>

                                <p class="mt-3"><span class="secondary-text-color">Pièces requises :</span> Pièce d'identité , Diplôme ou certificats ( Facultatif )</p>



                                <div class="mt-4">
                                    <a href="{{ route('profile') }}" type="submit" class="login-button col-lg-12 text-center">S'inscrire</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="dedicated-application">
            <div class="container p-0">
                <div class="">
                    <div class="row ">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <div class="mb-3">
                                <h4 class="font-weight-bold">Une application dédiée</h4>
                            </div>
                            <div>
                                <p>
                                    Nous avons développé une application dédiée pour les professionnels disponible sur IOs et Android.
                                </p>
                            </div><br>

                            <div>
                                <p>
                                    Une meilleure visibilité Des réservations uniquement sur vos disponibilités Pas de frais d'inscription Pas d'engagement, vous partez quand vous voulez Un service téléphonique pour vos clients (9h à 22h) Une application dédiée aux coachs et gratuite
                                </p>
                            </div><br>
                            <div class="d-flex flex-sm-row flex-column align-items-center justify-content-between">
                                <a href="#" target="_blank" class="btn btn-black mx-auto mt-2">
                                    <span class="d-flex align-items-center justify-content-start">
                                        <img src="images/google-play.png" class="icon mr-2" width="24px" alt="">
                                        Téléchargez sur <br>
                                        Google Play
                                    </span>
                                </a>&nbsp;&nbsp;
                                <a href="#" target="_blank" class="btn btn-black mx-auto mt-2">
                                    <span class="d-flex align-items-center justify-content-start">
                                        <i class="fa fa-apple mr-2" aria-hidden="true"></i>
                                        Téléchargez sur <br>
                                        Apple Store
                                    </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="d-flex align-items-center justify-content-center">
                                <img src="{{asset ('customer/images/jshdshd.png') }}" width="230" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
