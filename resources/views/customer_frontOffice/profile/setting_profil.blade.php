@extends("customer_frontOffice.layouts_customer.master_customer")
@section('css')
    <link rel="stylesheet" href="{{ asset('css/main.min.css') }}">
    <style>
        body {
            background-color: rgba(238, 238, 238, 0.336);
        }

    </style>
    {{-- <link rel="stylesheet" href="https://unpkg.com/flickity@2.2.1/dist/flickity.css"> --}}
    {{-- <script src="https://unpkg.com/flickity@2.2.1/dist/flickity.pkgd.min.js"></script> --}}
    {{-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> --}}
    <link rel="stylesheet" href="{{asset('css/intlTelInput.css')}}">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.15/css/intlTelInput.css"> --}}
@endsection
@section('content')
    <section>
        <header class="profil-header">
            <div class="dashboard-header-des">
                <h3 class="font-weight-bold text-center">Modifier mon profil </h3>
                <p class="text-center">
                    Gérez mes informations
                </p>
            </div>
        </header>
    </section>
    <!-- Profil details -->

    <section>
        <div class="container">
            <div class="row">
                <!-- .page-section -->
                <div class="page-section mt-4">
                    <!-- grid row -->
                    <div class="row">
                        <!-- grid column -->
                        <div class="col-lg-10 offset-md-2">
                            <!-- .card -->
                            <div class="card card-fluid">
                                <h6 class="card-header"> Modifier mes informations</h6>
                                <!-- .card-body -->
                                <div class="card-body">
                                    @foreach ($errors->all() as $error)
                                        <div class="alert alert-danger">
                                            {{ $error }}
                                        </div>
                                    @endforeach
                                    @if (session()->has('message'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message') }}
                                        </div>
                                    @endif

                                    <!-- .media -->
                                    <div class="media mb-3">
                                        <!-- avatar -->
                                        <div class="user-avatar user-avatar-xl fileinput-button">
                                            <div class="fileinput-button-label"> Change photo </div>
                                            <img src="@if (!$customer->photo_url){{ asset('customer/images/397-mckinsey-21a3074-jir_1.png') }} @else {{ $customer->photo_url }} @endif" alt="User Avatar" />
                                            <input id="fileupload-avatar" type="file" name="avatar">
                                        </div>
                                        <!-- /avatar -->
                                        <!-- .media-body -->
                                        <div class="media-body pl-3">
                                            <h3 class="card-title"> Public avatar </h3>
                                            <h6 class="card-subtitle text-muted"> Cliquez sur l'avatar actuel pour modifier
                                                votre photo.. </h6>
                                            <p class="card-text">
                                                <small>JPG, GIF or PNG 400x400, &lt; 2 MB.</small>
                                            </p>
                                            <!-- The avatar upload progress bar -->
                                            <div id="progress-avatar" class="progress progress-xs fade">
                                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                                                    role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <!-- /avatar upload progress bar -->
                                        </div>
                                        <!-- /.media-body -->
                                    </div>
                                    <!-- /.media -->
                                    <!-- form -->
                                    <form method="post" action="/customer/update/settings">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="lat" id="lat_input">
                                        <input type="hidden" name="lon" id="lon_input">
                                        <!-- form row -->
                                        <div class="form-row">
                                            <!-- form column -->
                                            <div class="col-md-6 mb-3">
                                                <label for="input01">Prénom <span class="required">*</span> </label>
                                                <input type="text" name="first_name" class="form-control" id="first_name"
                                                    value="{{ $customer->first_name }}" required="">
                                            </div>
                                            <!-- /form column -->
                                            <!-- form column -->
                                            <div class="col-md-6 mb-3">
                                                <label for="input02">Nom <span class="required">*</span></label>
                                                <input type="text" name="last_name" class="form-control" id="last_name"
                                                    value="{{ $customer->last_name }}" required="">
                                            </div>
                                            <!-- /form column -->
                                        </div>
                                        <!-- /form row -->
                                        <!-- .form-group -->
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label for="email">Adresse e-mail <span class="required">*</span></label>
                                                <input type="email" name="email" class="form-control" id="email"
                                                    value="{{ $customer->email }}" required="">
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="phone_number">Téléphone <span class="required">*</span></label>
                                                <input type="text" name="phone_number" class="form-control" id="phone_number" placeholder="Numéro de telephone *" data-msg="x" style="display: inline-block;" required value="{{$customer->phone_number}}" />
                                                <input type="hidden" name="dial_code" id="dial_code" value="{{ $customer->dial_code }}">
                                                <input type="hidden" name="country_code" id="country_code" value="{{ $customer->country_code }}" />
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="size">Taille (cm) Ex: 1m50 = 150 </label>
                                            <input type="text" name="size" class="form-control" id="size"
                                                value="{{ $customer->size }}" >
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label for="email">Poids actuel (Kg) </label>
                                                <input type="text" name="weight" class="form-control" id="weight"
                                                    value="{{ $customer->weight }}">
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="target_weight">Poids que vous comptez atteindre (Kg)</label>
                                                <input type="text" name="target_weight" class="form-control" id="target_weight" placeholder="" value="{{$customer->target_weight }}" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="input03">Date de naissance <span
                                                    class="required">*</span></label>
                                            <input type="date" name="birthday" class="form-control" id="email"
                                                value="{{ \Carbon\Carbon::parse($customer->birthday)->format('Y-m-d') }}"
                                                required="">
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col">
                                                    <label for="available">Disponibilité</label>
                                                    <select name="available" id="available"
                                                        class="form-control input-style placeholder-style" required>
                                                        <option value="flexible">Je suis flexible</option>
                                                        <option value="as_soon_as_possible">Dès que possible</option>
                                                        <option value="specific_date">A une date précise</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label for="input01">Commune <span class="required">*</span> </label>
                                                <input type="text" name="commune" class="form-control" id="commune"
                                                    value="{{ $customer->commune }}" required="">
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="input01">Quartier <span class="required">*</span> </label>
                                                <input type="text" name="quartier" class="form-control" id="quartier"
                                                    value="{{ $customer->quartier }}" required="">
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="bio">Biographie </label>
                                            <textarea type="text" rows="7" name="bio" class="form-control" id="bio"
                                                 >{{ $customer->bio }}</textarea>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="required form-group mt-4"> <label for="old_passwd"> <i
                                                    class="fa fa-info-circle"></i> Uniquement si vous souhaitez changer
                                                votre mot de passe actuel </label></div>
                                        <!-- .form-group -->
                                        <div class="form-group">
                                            <label for="current_password"> Mot de passe actuel </label>
                                            <input type="password" name="current_password" class="form-control"
                                                id="current_password">
                                        </div>
                                        <!-- /.form-group -->
                                        <!-- .form-group -->
                                        <div class="form-group">
                                            <label for="new_password"> Nouveau mot de passe </label>
                                            <input type="password" name="new_password" class="form-control"
                                                id="new_password">
                                        </div>
                                        <!-- /.form-group -->
                                        <!-- .form-group -->
                                        <div class="form-group">
                                            <label for="new_confirm_password"> Confirmation </label>
                                            <input type="password" name="new_confirm_password" class="form-control"
                                                id="new_confirm_password">
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-check mt-1">
                                            @if($customer->is_newsletter =='1')
                                                <input checked class="form-check-input" name="is_newsletter" type="checkbox" id="inlineCheckbox2" value="1">
                                            @else
                                                <input class="form-check-input" name="is_newsletter" type="checkbox" id="inlineCheckbox2" value="1">
                                            @endif
                                            <label class="form-check-label" for="inlineCheckbox2">M'inscrire à la newsletter</label>
                                        </div>
                                        <hr>
                                        <!-- .form-actions -->
                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-primary">Enregistrer</button>
                                            {{-- <a href="{{url('/customer/dashboard')}}" class="btn btn-info"> Tableau de bord</a> --}}
                                        </div>
                                        <!-- /.form-actions -->
                                    </form>
                                    <!-- /form -->
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /grid column -->
                    </div>
                    <!-- /grid row -->
                </div>
                <!-- /.page-section -->
            </div>
        </div>
    </section>

@endsection
@push('js')
    <script src="{{ asset('js/blueimp-file-upload/jquery.ui.widget.js') }}"></script>
    <script src="{{ asset('js/blueimp-file-upload/load-image.all.min.js') }}"></script>
    <script src="{{ asset('js/blueimp-file-upload/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('js/blueimp-file-upload/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('js/blueimp-file-upload/jquery.fileupload-process.js') }}"></script>
    <script src="{{ asset('js/blueimp-file-upload/jquery.fileupload-image.js') }}"></script>
    <script src="{{ asset('js/blueimp-file-upload/jquery.fileupload-validate.js') }}"></script>
    <script src="{{ asset('js/user-settings.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $("#commune").autocomplete({
            source: function(query, callback) {
                $.getJSON(
                    `https://nominatim.openstreetmap.org/search?q=${query.term}&countrycodes=CI&format=json&addressdetails=1&limit=100`,
                    function(data) {
                        var places = data;
                        places = places.map(place => {
                            console.log("==== place ===");
                            console.log(place);
                            return {
                                title: place.display_name.replace(/<br\/>/g, ", "),
                                value: place.display_name.replace(/<br\/>/g, ", "),
                                lat: place.lat,
                                lon: place.lon,
                                id: place.osm_id
                            };
                        });
                        return callback(places);
                    }
                );
            },
            minLength: 2,
            select: function(event, ui) {
                console.log(ui);
                $('#lat_input').val(ui.item.lat);
                $('#lon_input').val(ui.item.lon);
                console.log("Selected: " + ui.item.title);
            },
            open: function() {
                $('.ui-autocomplete').css('background-color', '#808080').css('width', '305px'); // HERE
            }
        });
    </script>
    <script src="{{asset('js/intlTelInput.js')}}"></script>
    {{-- <script scr="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.15/js/intlTelInput.min.js"></script> --}}
    <script>
        var input = document.querySelector("#phone_number");
    
        var errorMsg = document.querySelector("#error-msg"),
            validMsg = document.querySelector("#valid-msg"),
                form500 = document.querySelector("#form500");
    
    
        var errorMap = ["&#65794; Numéro Invalide !", "&#65794; Code de pays invalide !", "&#65794; Numéro de téléphone Trop Court", "&#65794; Numéro de téléphone Trop Long", "&#65794; Numéro Invalide"];
        var iti = window.intlTelInput(input, {
            dropdownContainer: document.body,
            separateDialCode: true,
            initialCountry: "CI",
            geoIpLookup: function(success, failure) {
                $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                success(countryCode);
                });
            },
            placeholderNumberType: "MOBILE",
            preferredCountries: ['ci'],
            utilsScript: "{{asset('js/utils.js?')}}",
        });
        var reset = function() {
            input.classList.remove("error");
            errorMsg.innerHTML = "";
            errorMsg.classList.add("hide");
            validMsg.classList.add("hide");
        };
    
        // on blur: validate
        input.addEventListener('blur', function() {
            reset();
            if (input.value.trim()) {
                if (iti.isValidNumber()) {
                    $("#dial_code").val(iti.getSelectedCountryData().dialCode);
                    // $("#phone_number").val(iti.);
                    $("#country_code").val(iti.getSelectedCountryData().iso2.toUpperCase());
                    // $()
                    // console
                    if($("#dial_code").val() == ''){
                        $('#whatsapp_number').val("225"+$(this).val())
                    }else{
                        $('#whatsapp_number').val($("#dial_code").val()+$("#phone_number").val())
                    }
                    form500.submit();
    
                    // $("#whatsapp_number").val($("#dial_code").value+$("#phone_number").value);
                    //console.log(iti.s.phone);
                    validMsg.classList.remove("hide");
                } else {
                    input.classList.add("error");
                    var errorCode = iti.getValidationError();
                    // console.log("Error from intInput");
                    // console.log(errorCode);
                    errorMsg.innerHTML = errorMap[errorCode];
                    errorMsg.classList.remove("hide");
                }
            }
        });
    
        // on keyup / change flag: reset
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);
    </script>
@endpush
