@extends("customer_frontOffice.layouts_customer.master_customer")
@section('css')
<style>
    body {
        background-color: rgba(238, 238, 238, 0.336);
    }

</style>
@endsection
@section('content')
<section>
    <header class="profil-header">
        <div class="dashboard-header-des">
            <h3 class="font-weight-bold text-center">Mon Profil </h3>
            <p class="text-center">
                Gérez mes informations
            </p>
        </div>
    </header>
</section>
<!-- Profil details -->

<section>
    <div class="container">
        <div class="py-2">
            <div class="profil-box-content">
                <div class="row">
                    <div class="col-lg-7 col-md-7 col-sm-12 d-flex">

                        <div class="mr-4">
                            <img src="{{ asset('customer/images/397-mckinsey-21a3074-jir_1.png') }}" class="rounded-circle" height="110"
                                width="110" alt="">
                        </div>

                        <div>
                            <div>
                                <h4 class="font-weight-bold">Jean Michel Bagui</h4>
                                <p>Inscrit depuis le 14 juillet 2020</p>
                                <p>Statut : <span class="self-challenger-box">Self Challenger</span></p>

                            </div>

                            <button type="submit" class="grey-button">Modifier photo de profil</button>
                        </div>
                    </div>

                    <!-- divider -->

                    <!-- <div class="text-center">
                        <div class="verticals-divider "></div>
                    </div> -->

                    <div class="col-lg-4 col-md-3 col-sm-12  py-3 ">
                        <div class="d-flex">
                            <div class="text-center">
                                <img src="{{ asset('customer/images/height.png') }}" width="40" height="40" alt="">
                                <p><small>Taille en cm</small></p>
                                <input type="number" placeholder="170" value="170" class="calcul-input">
                            </div>
                            <div class="text-center">
                                <img src="{{ asset('customer/images/weight.png') }}" width="40" height="40" alt="">
                                <p><small>Poids actuelle en kg</small></p>
                                <input type="number" placeholder="60" value="60" class="calcul-input">
                            </div>
                        </div>
                        <button type="submit" class="grey-pale-button mt-2 col-lg-10">Enregistrer</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Statistiques -->

<section>
    <div class="container">
        <div class="py-2">
            <div class="profil-box-content">
                <h5 class="font-weight-bold">Statistiques </h5>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                        <h3 class="font-weight-bold primary-text-color">0</h3>
                        <p>Cours effectués</p>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                        <h3 class="font-weight-bold primary-text-color">0</h3>
                        <p>Cours à réserver</p>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                        <h3 class="font-weight-bold primary-text-color ">0</h3>
                        <p>Points Goden Health</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Cours effectutés -->

<section>
    <div class="container">
        <div class="py-2">
            <div class="courses-makes">
                <div>
                    <h5 class="font-weight-bold">Cours éffectués</h5>
                    <p>Vous n'avez pas encore effectué de cours</p>
                </div>
            </div>
        </div>

    </div>
</section>


<!-- Heures de pratiques -->
<section>
    <div class="container">
        <div class="py-2">
            <div class="profil-box-content">
                <div>
                    <h5 class="font-weight-bold">Heures de pratiques</h5>
                    <p>Vous n'avez pas encore effectué de cours</p>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection
