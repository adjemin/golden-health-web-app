    <style>
    body {
        font-size: 13px;
        /* background-color: rgba(247, 247, 247, 0.973); */
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        margin-top: 10px;
        z-index: 1;
    }

    .dropdown-content a {
        float: none;
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        text-align: left;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }

</style>
<section class="nav-section">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light ">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('customer/images/gh1.png') }}" alt="Logo" width="100px">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <div class="">
                    <ul class="navbar-nav">
                        <!--li class="nav-item" style="position: relative;">
                        <a class="nav-link btn nav-item  " href="#" data-toggle="dropdown"><small>Services</small><span class="sr-only">(current)</span></a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item " href="{{ route('self-challenger') }}">Self Challenger
                                @if(Request::is('self-challenger'))
                                <span class="sr-only">
                                    (current)
                                </span>
                                @endif
                            </a>
                                {{-- {{ route('coachs') }} --}}
                            <a class="dropdown-item " href="{{ route('coach-description') }}">Coach
                                @if(Request::is('coach-description'))
                                <span class="sr-only">
                                    (current)
                                </span>
                                @endif
                            </a>



                            <a class="dropdown-item " href="{{ route('become.partner') }}">Partner
                                @if(Request::is('become_partner'))
                                <span class="sr-only">
                                    (current)
                                </span>
                                @endif
                            </a>
                        </div>
                    </li-->
                        <li class="nav-item">
                            <a class="nav-link" href="/customer/disciplines">Nos disciplines</a>
                            @if(Request::is('/customer/disciplines'))
                            <span class="sr-only">
                                (current)
                            </span>
                            @endif
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('programmes') }}"> Programmes</a>
                            @if(Request::is('/programmes'))
                            <span class="sr-only">
                                (current)
                            </span>
                            @endif
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('events.all') }}"> Events</a>
                            @if(Request::is('/events'))
                            <span class="sr-only">
                                (current)
                            </span>
                            @endif
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('shop') }} "> Shop</a>
                            @if(Request::is('/shop'))
                            <span class="sr-only">
                                (current)
                            </span>
                            @endif
                        </li>

                        <li class="nav-item" style="position: relative;">
                            <a class="nav-link" href="#" data-toggle="dropdown">Médias</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item " href="{{ route('blog') }}">Blog</a>
                                <a class="dropdown-item " href="{{url('/videotheque')}}">Vidéothèque</a>
                            </div>
                        </li>

                        <li class="nav-item" style="position: relative;">
                            <a class="nav-link" href="#" data-toggle="dropdown">A Propos</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item " href="{{ route('qui-sommes-nous') }}">Qui sommes nous ?</a>
                                <a class="dropdown-item " href="{{ route('contact') }}">Contactez-nous</a>
                                <a class="dropdown-item " href="{{ route('faq') }}">FAQ</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="ml-auto">
                    <!-- Tableau de bord -->

                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="{{ route('customer.dashboard') }}" class="dashboard-button mr-1">
                                <small> Tableau de bord</small>
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="profil-button dropbtn">
                                <i class="fa fa-user mr-2" aria-hidden="true"></i>
                                @if(Auth::guard('customer')->user()) {{ Auth::guard('customer')->user()->first_name }} @endif<i class="fa fa-caret-down"></i>
                            </a>
                            <div class="dropdown-content">
                                <a class="dropdown-item " href="{{url('/customer/profile')}}">Profil</a>
                                @if(Auth::guard('customer')->user() && Auth::guard('customer')->user()->is_coach && Auth::guard('customer')->user()->is_active == true && !is_null(Auth::guard('customer')->user()->email_verifed_at))
                                
                                <a class="dropdown-item " href="{{url('/customer/coach/diplome')}}">Diplômes</a>

                                <a class="dropdown-item " href="{{url('/customer/coach/lessons')}}">
                                    {{-- Disciplines --}}
                                    Mes Cours
                                </a>
                                {{--   <a class="dropdown-item " href="{{url('/customer/coach/calendar')}}">Calendrier</a> --}}
                                
                                <a class="dropdown-item " href="{{url('/customer/coach/portfolio')}}">Portfolio</a>
                                @endif
                                <a class="dropdown-item " href="{{ url('/customer/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Se Déconnecter</a>
                                <form id="logout-form" action="{{ url('/customer/logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                    <!--a href="{{ url('/logout') }}" class="dropdown-item btn btn-default btn-flat"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fa fa-lock"></i>Logout
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    @csrf
                </form-->
                </div>
                <a href="/basket" class="ml-2 cart-icon">
                    <span class="badge cart-badge">
                        {{App\ShoppingCart::ShoppingCart()->items->count()}}
                    </span>
                    <i class="fa fa-shopping-cart mr-2" style="font-size: 20px; color:black"></i>
                </a>
            </div>
        </nav>
    </div>
</section>
