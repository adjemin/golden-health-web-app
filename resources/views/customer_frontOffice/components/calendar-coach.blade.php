<div mbsc-page class="demo-add-delete-event">
  <div style="height:100%">
      <div mbsc-form>
        <div class="mbsc-grid md-add-event-demo">
            <div class="mbsc-row mbsc-no-padding">
                <div class="mbsc-col-md-4 mbsc-col-12">
                    <div id="demo-add-event-month"></div>
                    <div class="mbsc-btn-group-block">
                        <button mbsc-button id="showAddEventPopup">Add New Event</button>
                    </div>
                </div>
                <div class="mbsc-col-md-8 mbsc-col-12 md-col-right">
                    <div id="demo-add-event-day"></div>
                </div>
            </div>
        </div>
    </div>

    <div id="demo-add-event-popup" class="md-add-event-popup">
        <div mbsc-form>
            <div class="mbsc-form-group">
                <label>
                    Title
                    <input mbsc-input id="eventText">
                </label>
                <label>
                    Description
                    <textarea mbsc-textarea id="eventDesc"></textarea>
                </label>
            </div>
            <div class="mbsc-form-group">
                <label for="allDay">
                    All-day
                    <input mbsc-switch id="allDay" type="checkbox">
                </label>
                <label for="eventDate">
                    Starts
                    <input mbsc-input id="eventDate">
                </label>
                <label>
                    Ends
                    <input mbsc-input id="endInput">
                </label>
                <label>
                    Show as busy
                    <input mbsc-segmented type="radio" name="free" value="false" checked>
                </label>
                <label>
                    Show as free
                    <input mbsc-segmented type="radio" name="free" value="true">
                </label>
            </div>
        </div>
    </div>
  </div>
</div>
