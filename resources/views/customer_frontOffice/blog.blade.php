@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
    <style>
        body {
            background-color: #eee;
        }

        a {
            color: black;
        }

    </style>

@endsection
@section('content')

    <!-- Barners -->

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="blog-barner">
                    <div class="des">
                        Bienvenue sur le blog !
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section>
        <div class="container-fluid mt-5 ">
            <h3 class="font-weight-bold mx-5">Découvrez nos différents articles</h3>
            <div class="secondary-dividers mb-2 mx-5"></div>
        </div>
    </section>


    <section class="py-5">
        <div class="container-fluid">
            <div class="row main-container">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                    <div class="row">
                        @if ($posts != null && count($posts)>0)
                            @foreach ($posts as $post)
                            <div class="col-lg-4 col-md-4 col-sm-12 mb-4">
                                <div class="article-card">
                                    <div class="bg-image" style="background-image: url({{  $post->cover ?? asset('customer/images/IMAGE.png') }});">
                                        {{--<img src="{{ $post->cover }}" width="100%"
                                            class="img-fluid" alt="" srcset="">--}}
                                    </div>
                                    <div class="py-3 px-3">
                                        <div class="">
                                            <h5 class="font-weight-bold">{{ $post->getCategoryAttribute()->name }}</h5>
                                            <p class="mb-4">
                                                {{ $post->title }}
                                            </p>
                                            <div class="text-right">
                                                <a href="{{ URL::to('blog-article/'.$post->slug ) }}" class="view-more text-center">Voir
                                                    plus</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
                                Aucun article disponible !
                            </div>
                        @endif



                        {{-- <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="article-card">
                                <div>
                                    <img src="{{ asset('customer/images/mobilite_0urbaine.jpg') }}" width="100%"
                                        class="img-fluid" alt="" srcset="">
                                </div>
                                <div class="py-3 px-3">
                                    <div class="">
                                        <h5 class="font-weight-bold">FootballZ</h5>
                                        <p class="mb-4">
                                            Le Lorem Ipsum est simplement du faux texte employé dans la composition et la
                                            mise
                                            en page avant impression.
                                        </p>
                                        <div class="text-right">
                                            <a href="{{ route('blog-article') }}" class="view-more text-center">Voir
                                                plus</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="article-card">
                                <div>
                                    <img src="{{ asset('customer/images/small test buy.jpg') }}" width="100%"
                                        class="img-fluid" alt="" srcset="">
                                </div>
                                <div class="py-3 px-3">
                                    <div class="">
                                        <h5 class="font-weight-bold">Football</h5>
                                        <p class="mb-4">
                                            Le Lorem Ipsum est simplement du faux texte employé dans la composition et la
                                            mise
                                            en page avant impression.
                                        </p>
                                        <div class="text-right">
                                            <a href="{{ route('blog-article') }}" class="view-more text-center">Voir
                                                plus</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}

                        {{-- <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="article-card">
                                <div>
                                    <img src="{{ asset('customer/images/mobilite_0urbaine.jpg') }}" width="100%"
                                        class="img-fluid" alt="" srcset="">
                                </div>
                                <div class="py-3 px-3">
                                    <div class="">
                                        <h5 class="font-weight-bold">Football</h5>
                                        <p class="mb-4">
                                            Le Lorem Ipsum est simplement du faux texte employé dans la composition et la
                                            mise
                                            en page avant impression.
                                        </p>
                                        <div class="text-right">
                                            <a href="{{ route('blog-article') }}" class="view-more text-center">Voir
                                                plus</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="article-card mt-4">
                                <div>
                                    <img src="{{ asset('customer/images/mobilite_0urbaine.jpg') }}" width="100%"
                                        class="img-fluid" alt="" srcset="">
                                </div>
                                <div class="py-3 px-3">
                                    <div class="">
                                        <h5 class="font-weight-bold">Football</h5>
                                        <p class="mb-4">
                                            Le Lorem Ipsum est simplement du faux texte employé dans la composition et la
                                            mise
                                            en page avant impression.
                                        </p>
                                        <div class="text-right">
                                            <a href="#" class="view-more text-center">Voir plus</a>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div> --}}
                    </div>
                </div>

                {{--<div class="col-lg-4">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 ">

                            <!-- Dernier articles -->
                            <h5 class="font-weight-bold ">Récents articles</h5>
                            <div class="secondary-dividers mb-4"></div>
                            <!--  -->
                            @if ($recentPosts != null)
                                @foreach ($recentPosts as $recentPost)
                                    <div class="white-bg d-flex mb-3">
                                        <div>
                                            <img src="{{ $recentPost->cover }}" class="img-height" alt="">
                                        </div>

                                        <div class="px-2 py-1">
                                            <h6 class="secondary-text-color"><b>{{ $recentPost->getCategoryAttribute()->name }}</b></h6>
                                            <p>{{ $recentPost->title }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="white-bg d-flex">
                                    Aucun article récent disponible !
                                </div>
                            @endif--}}


                            {{-- <!--  -->
                            <div class="white-bg d-flex mt-3">
                                <div>
                                    <img src="{{ asset('customer/images/78e60a1.jpg') }}" class="img-height" alt="">
                                </div>

                                <div class="px-2 py-1">
                                    <h6 class="secondary-text-color"><b>Fitness</b></h6>
                                    <p>Testez des activités sportives à la...</p>
                                </div>
                            </div>
                            <!--  -->
                            <div class="white-bg d-flex mt-3">
                                <div>
                                    <img src="{{ asset('customer/images/78e60a1.jpg') }}" class="img-height" alt="">
                                </div>

                                <div class="px-2 py-1 ">
                                    <h6 class="secondary-text-color"><b>Fitness</b></h6>
                                    <p>Testez des activités sportives à la...</p>
                                </div>
                            </div> --}}


                            <!-- Anciens articles -->

                            {{--<h5 class="mt-5 font-weight-bold">Anciens articles</h5>
                            <div class="secondary-dividers mb-4"></div>
                            @if ($oldPosts != null)
                                @foreach ($oldPosts as $oldPost)
                                    <div class="white-bg d-flex mb-3">
                                        <div>
                                            <img src="{{ $oldPost->cover }}" class="img-height" alt="">
                                        </div>

                                        <div class="px-2 py-1">
                                            <h6 class="secondary-text-color"><b>{{ $oldPost->getCategoryAttribute()->name }}</b></h6>
                                            <p>{{ $oldPost->title }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="white-bg d-flex">
                                    Aucun article récent disponible !
                                </div>
                            @endif
                        </div>
                    </div>
                </div>--}}


            </div>

            {{-- <center><a href="#" class="view-more"> Charger plus</a></center> --}}
            <div class="mt-3 d-flex justify-content-center">
                {{ $posts->links() }}
            </div>

        </div>

    </section>

@endsection
