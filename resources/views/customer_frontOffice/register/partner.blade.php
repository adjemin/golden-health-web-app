@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')

<link rel="stylesheet" href="{{asset('css/intlTelInput.css')}}">
<script src="https://www.google.com/recaptcha/api.js?render=6LcWRaQdAAAAAMRicMpnxAL1V1eMRDQWC6kONe9Q"></script>
@endsection


@section('content')
<section>
    <header class="become-partner-header">
        <div class="become-coach-header-des">
            <h3 class="font-weight-bold">Devenir Partner </h3>
            <p>
                Voulez-vous collaborer avec Golden Health sur un projet sportif que vous avez pour votre entreprise ou organisation ? / Voulez-vous faire connaitre vos produits et services ? N’attendez pas !!! inscrivez-vous et dites-nous tout. Bénéficier vous aussi de cette large communauté de sportifs.
            </p>
        </div>
    </header>
</section>


<section class="py-5">
    <div class="container">
        <div class="row ">
            <h1 class="font-weight-bold primary-text-color">Partner </h1>
        </div>

        <!--<p>la description des avantages disponibles pour un coach qui souhaite collaborer avec GH. <br> <br> Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte
            standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté
            à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans
            des applications de mise en page de texte, comme Aldus PageMaker.</p> -->
    </div>
</section>

@if(session()->has('message'))
<div class=" py-5">
    <div class="container">
        <div class="row">
          <div class="cold-md-12">
              <div class="alert alert-success">
                  {{ session()->get('message') }}
              </div>
            </div>
        </div>

@else
    <div class=" py-5">
        <div class="container">
            <div class="row">
            <div class="cold-md-12">
                @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
                else
                @endif
            </div>
            </div>
            <div class="row">
                @if (session()->has('success'))
                    <h4 class="text-secondary font-weight-bold text-center">
                        Merci de nous avoir contacté !
                    </h4>
                @else
                    <h4 class="font-weight-bold text-primary">M'inscrire en tant que Partner Golden Health </h4>
                @endif
            </div>
            <div class="row py-5 ">
                @if (session()->has('success'))
                    Votre requête a été prise en compte, nous vous contacterons dans un délai de 48H.
                @else
                    <div class="col-lg-10 col-md-10 col-sm-12">
                        <form role="form" method="POST" id="my-form" action="{{ route('partners.store') }}">
                        {{ csrf_field() }}
                            <div class="row mb-4 ml-1">
                                <div class="form-check mr-4">
                                    <input class="form-check-input" type="radio" name="gender" id="gender" value="MR" checked >
                                    <label class="form-check-label" for="gridRadios1">
                                        Mr
                                    </label>
                                </div>

                                <div class="form-check mr-4">
                                    <input class="form-check-input" type="radio" name="gender" id="gender" value="MME" >
                                    <label class="form-check-label" for="gridRadios2">
                                        Mme
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gender" id="gender" value="MLLE" >
                                    <label class="form-check-label" for="gridRadios3">
                                        Mlle
                                    </label>
                                </div>
                                <div class="form-check ml-2">
                                    <input class="form-check-input" type="radio" name="gender" id="gender" value="ENTREPRISE" >
                                    <label class="form-check-label" for="gridRadios4">
                                        Entreprise
                                    </label>
                                </div>
                            </div>
                            <input type="hidden" name="type_account" value="partner">
                            <div class="form-group">
                                <label for="name">Nom complet <span style="color:red;">*</span></label>
                                <input type="text" name="name" class="form-control input-style placeholder-style" id="name" placeholder="Nom Complet"  value="{{old('name')}}">
                                <span class="@error('name') text-danger @enderror">@error('name'){{ $message }}@enderror</span>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="email">Email <span style="color:red;">*</span></label>
                                    <input type="email" name="email" class="form-control input-style placeholder-style" id="email" placeholder="email*"  value="{{old('email')}}">
                                    <span class="@error('email') text-danger @enderror">@error('email'){{ $message }}@enderror</span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="phone_number">Numéro de téléphone <span style="color:red;">*</span></label>
                                    <input type="text" name="phone_number" class="form-control" id="phone_number" placeholder="Numéro de telephone" data-msg="x" style="display: inline-block;" value="{{old('phone_number')}}" />
                                    <input type="hidden" name="dial_code" id="dial_code">
                                    <input type="hidden" name="country_code" id="country_code" value="" />
                                    <div>
                                        <span id="valid-msg" class="hide"></span>
                                            <span id="error-msg" class="hide" style="color:red;"></span>
                                        <div class="validation"></div>

                                        <span class="@error('phone_number') text-danger @enderror">@error('phone_number'){{ $message }}@enderror</span>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <select name="service_type" id="service_type" class="custom-select input-style placeholder-style" >
                                    <option value="">Type de service</option>
                                    <option value="1">Publicité/Marketing</option>
                                    <option value="2">Vente de produits</option>
                                    <option value="3">Autres</option>
                                </select>
                            </div>
                            <div id="div_other_service" style="display: none @error('service_type_other') block @enderror">
                                <div class="input-group mb-3">
                                    <input type="text" name="service_type_other" class="form-control input-style placeholder-style @error('service_type_other') is-invalid @enderror" id="service_type_other"  placeholder="Autre type de service">
                                    <span class="@error('service_type_other') text-danger @enderror">@error('service_type_other'){{ $message }}@enderror</span>
                                </div>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text placeholder-style " id="basic-addon3">Facebook</span>
                                </div>
                                <input type="text" name="facebook_url" class="form-control input-style placeholder-style" id="facebook_url" aria-describedby="basic-addon3" placeholder="Page Facebook" value="{{old('facebook_url')}}">
                                <span class="@error('facebook_url') text-danger @enderror">@error('facebook_url'){{ $message }}@enderror</span>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend ">
                                    <span class="input-group-text placeholder-style" id="basic-addon3">Instagram</span>
                                </div>
                                <input type="text" name="instagram_url" class="form-control input-style placeholder-style" id="instagram_url" aria-describedby="basic-addon3" placeholder="Compte Instagram" value="{{old('instagram_url')}}">
                                <span class="@error('instagram_url') text-danger @enderror">@error('instagram_url'){{ $message }}@enderror</span>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend ">
                                    <span class="input-group-text placeholder-style" id="basic-addon3">Youtube link</span>
                                </div>
                                <input type="text" name="youtube_url" class="form-control input-style placeholder-style" id="instagram_url" aria-describedby="basic-addon3" placeholder="Compte Youtube" value="{{old('youtube_url')}}">
                                <span class="@error('youtube_url') text-danger @enderror">@error('youtube_url'){{ $message }}@enderror</span>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text placeholder-style" id="basic-addon3">Twitter</span>
                                </div>
                                <input type="text" name="twitter_id" class="form-control input-style placeholder-style" id="twitter_id" aria-describedby="basic-addon3" placeholder="Twitter" value="{{old('twitter_id')}}">
                                <span class="@error('twitter_id') text-danger @enderror">@error('twitter_id'){{ $message }}@enderror</span>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text placeholder-style" id="basic-addon3">Site
                                        web</span>
                                </div>
                                <input type="text" name="site_web_partner" class="form-control input-style placeholder-style" id="site_web_partner" aria-describedby="basic-addon3" placeholder="Site web" value="{{old('site_web_coach')}}">
                                <span class="@error('site_web_partner') text-danger @enderror">@error('site_web_partner'){{ $message }}@enderror</span>
                            </div>

                            <label for="discovery_source">Comment avez-vous connu Golden Health ? </label>
                            <div class="input-group mb-3">
                                <select name="discovery_source" class="custom-select input-style placeholder-style" id="discovery_source" >
                                    <option value=""></option>
                                    <option value="search_engine"> Moteur de recherche (Bing, Google, Yahoo...)</option>
                                    <option value="website"> Site Internet autre qu'un moteur de recherche (Blog, presse...)</option>
                                    <option value="written-press"> Presse écrite</option>
                                    <option value="social-media"> Réseaux sociaux (Facebook, Instagram...)</option>
                                    <option value="word-of-mouth"> Bouche à oreilles (Amis, famille, collègue...)</option>
                                    <option value="corporate-lesson"> Cours en entreprise</option>
                                    <option value="subway-display"> Affichage métro</option>
                                    <option value="coach"> Par un coach</option>
                                    <option value="other"> Autre (Salon, évènement...)</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <textarea name="description" class="form-control  placeholder-style" placeholder="Parlez-nous un peu de vous" id="" cols="30" rows="10" >{{old('description')}}</textarea>
                            </div>

                            <div class="">
                                @include('customer_frontOffice.layouts_customer.inc.input.terms')

                                {{-- <div class="col-lg-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="is_newsletter" name="is_newsletter" value="1">
                                        <label class="form-check-label" for="is_newsletter">S'inscrire à la
                                            newsletter</label>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-check">
                                        <input class="form-check-input" name="is_cgu" type="checkbox" id="is_cgu" value="1" >
                                        <label class="form-check-label" for="is_cgu">J'accepte les
                                            Conditions
                                            générales d'utilisation</label>
                                    </div>
                                </div> --}}
                            </div>
                            <div style="margin-top: 15px" class="g-recaptcha brochure__form__captcha" data-sitekey="6LcWRaQdAAAAAMRicMpnxAL1V1eMRDQWC6kONe9Q"></div>
                            @if ($errors->has('g-recaptcha-response'))
                                <span class="help-block">
                                    <strong style="color:red">{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                            @endif
                            <div class="mt-4">
                                <button type="submit" name="submit" class="login-button col-lg-12 text-center">S'inscrire</button>
                            </div>
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endif
@endsection
@push('js')
<script>
$(document).ready(function() {
    $('#service_type').on('change', function(){
        if ($(this).val() == 3){
            $('#div_other_service').show();
        } else {
            $('#div_other_service').hide();
        }
    });
});
</script>

<script src="{{asset('js/intlTelInput.js')}}"></script>
<script>
    var input = document.querySelector("#phone_number");

    var errorMsg = document.querySelector("#error-msg"),
        validMsg = document.querySelector("#valid-msg"),
            form500 = document.querySelector("#form500");


    var errorMap = ["&#65794; Numéro Invalide !", "&#65794; Code de pays invalide !", "&#65794; Numéro de téléphone Trop Court", "&#65794; Numéro de téléphone Trop Long", "&#65794; Numéro Invalide"];
    var iti = window.intlTelInput(input, {
        dropdownContainer: document.body,
        separateDialCode: true,
        initialCountry: "CI",
        geoIpLookup: function(success, failure) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
            var countryCode = (resp && resp.country) ? resp.country : "";
            success(countryCode);
            });
        },
        placeholderNumberType: "MOBILE",
        preferredCountries: ['ci'],
        utilsScript: "{{asset('js/utils.js?')}}",
    });
    var reset = function() {
        input.classList.remove("error");
        errorMsg.innerHTML = "";
        errorMsg.classList.add("hide");
        validMsg.classList.add("hide");
    };

    // on blur: validate
    input.addEventListener('blur', function() {
        reset();
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
                $("#dial_code").val(iti.getSelectedCountryData().dialCode);
                // $("#phone_number").val(iti.);
                $("#country_code").val(iti.getSelectedCountryData().iso2.toUpperCase());
                // $()
                // console
                if($("#dial_code").val() == ''){
                    $('#whatsapp_number').val("225"+$(this).val())
                }else{
                    $('#whatsapp_number').val($("#dial_code").val()+$("#phone_number").val())
                }
                form500.submit();

                // $("#whatsapp_number").val($("#dial_code").value+$("#phone_number").value);
                //console.log(iti.s.phone);
                validMsg.classList.remove("hide");
            } else {
                input.classList.add("error");
                var errorCode = iti.getValidationError();
                // console.log("Error from intInput");
                // console.log(errorCode);
                errorMsg.innerHTML = errorMap[errorCode];
                errorMsg.classList.remove("hide");
            }
        }
    });

    // on keyup / change flag: reset
    input.addEventListener('change', reset);
    input.addEventListener('keyup', reset);
</script>
<script type="text/javascript">
        /*
      * Block / Unblock UI
      */
        function blockUI() {
            $.blockUI({
              message: "<img src='{{asset('assets/img/loading.gif')}}' alt='Loading ...' />",
              overlayCSS: {
                  backgroundColor: '#1B2024',
                  opacity: 0.55,
                  cursor: 'wait'
              },
              css: {
                  border: '4px #999 solid',
                  padding: 15,
                  backgroundColor: '#fff',
                  color: 'inherit'
              }
            });
        }

        function unblockUI() {
            $.unblockUI();
        }

    $(document).ready(function(){
        function successregister(){
            Swal.fire({
                icon: 'success',
                title: 'Félication !',
                text: 'Enregistrement effectué avec succès',
            });
            // setTimeout(function() {
            //     window.location.href = "/"
            // }, 5000);
        }
        if({{ $partner??false }} != false){
            successregister()           
        }
    })

    $(document).ready(function(){
        function processForm( e ){
             var messages_errors = '';
            blockUI();
            $.ajax({
                url: '/register/partner',
                dataType: 'json',
                type: 'POST',
                data: $(this).serialize(),
                success: function(data){
                    unblockUI();
                    console.log(data);
                    if (data.data){
                        Swal.fire({
                          icon: 'success',
                          title: 'Félication !',
                          text: 'Enregistrement effectué avec succès',
                        });
                        setTimeout(function() {
                          window.location.href = "/"
                        }, 500);
                    } else {
                        if (data.email) {
                            messages_errors = data.email[0];
                        }
                        if (data.password) {
                            messages_errors = data.password[0];
                        }
                        Swal.fire({
                          icon: 'error',
                          title: 'Oops',
                          text: messages_errors,
                        });
                    }
                },
                error: function(error){
                    unblockUI();
                    console.log(error);
                    Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Une erreur s\'est lors de l\'enregistrement veuillez réessayer' ,
                    });
                }
            });

            e.preventDefault();
        }

        //$('#my-form').submit( processForm );
        

        $('.selectpicker').selectpicker();

        $('#birthday').datetimepicker({
            format: 'DD/MM/YYYY'
        });
    });
</script>
@endpush
