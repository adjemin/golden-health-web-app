@extends("customer_frontOffice.layouts_customer.master_customer")


@section('css')

<style>
    body {
        background-image: linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, .6)), url("customer/images/bg2.jpg");
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        position: relative;
    }

    a {
        color: black;
    }

</style>

<link rel="stylesheet" href="https://unpkg.com/flickity@2.2.1/dist/flickity.css">
<script src="https://unpkg.com/flickity@2.2.1/dist/flickity.pkgd.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{asset('css/intlTelInput.css')}}">
<script src="https://www.google.com/recaptcha/api.js?render=6LcWRaQdAAAAAMRicMpnxAL1V1eMRDQWC6kONe9Q"></script>
@section('content')

    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7 offset-md-3 col-sm-12">
                    <div class="login-box">
                        <center>
                            <h4> <b>Créer mon compte </b> </h4>

                            <div class="mt-3">
                                <p>
                                    Veux-tu sortir de ta zone de confort et faire le choix d’une vie plus saine? Une vie où on est en harmonie avec son corps et on apprend à se transcender ?
                                    <br/>
                                    Une vie où l’on devient tout simplement une meilleure version de « soi-même » : alors tu es un self-challenger !
                                    <br/>
                                    <br/>
                                    Golden Health accompagne le self-challenger dans cette aventure en mettant à sa disposition des coachs qualifiés pour un suivi professionnel, des diététiciens pour un suivi nutritionnel, en plus d’un tableau de bord personnel retraçant ses performances.
                                    <br/>
                                    {{-- https://www.youtube.com/watch?v=O8ElQBIenN0 --}}
                                    Etes-vous prêt à relever vos défis sportifs ?
                                    A vos marques, prêt, c’est parti !
                                </p>
                            </div>
                        </center>
                        <br>
                        <center>
                            <p>Déjà inscrit sur Golden Health ? &nbsp; <span><a href="/customer/login" class="secondary-text-color">Connectez-vous</a></span></p>
                        </center>
                        {{-- <div class="row mt-3">
                            <div class="col-md-6 mt-2">
                                <div class="login-google">
                                    <a href="{!! url('auth/google') !!}"> <img src="{{asset('customer/images/téléchargement.png')}}" width="15px" height="15px" alt="">
                                        &nbsp;
                                        &nbsp;
                                        Connexion avec
                                        google</a>
                                </div>
                            </div>
                            <div class="col-md-6 mt-2">
                                <div class="login-facebook">
                                    <a href="{!! url('auth/facebook') !!}"> <img src="{{asset('customer/images/facebook.png')}}" width="15px" height="15px" alt="">&nbsp; Connexion avec facebook</a>
                                </div>
                            </div>
                        </div> --}}
                        <div class="row mx-auto mt-4">
                            <div class="mini-divider mt-3"></div> &nbsp;&nbsp;
                            <p>Inscription</p>&nbsp;&nbsp;
                            <div class="mini-divider mt-3"></div>
                        </div>
                        <form role="form" method="POST" action="{{ url('/customer/register') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="action" value="<?php if(isset($_GET['action'])) { echo $_GET['action']; } ?>">
                            @if(isset($data) && $data['facebook_id'])
                              <input type="hidden" name="facebook_id" value="{{$data['facebook_id']}}" />
                            @elseif(isset($data) && $data['google_id'])
                              <input type="hidden" name="google_id" value="{{$data['google_id']}}" />
                            @endif
                            <div class="row mb-4 ml-1">
                                <div class="form-check mr-4">
                                    <input class="form-check-input" type="radio" name="gender" id="genre-mr" value="mr" checked>
                                    <label class="form-check-label" for="genre-mr">
                                        M.
                                    </label>
                                </div>
                                <div class="form-check mr-4">
                                    <input class="form-check-input" type="radio" name="gender" id="genre-mme" value="mme">
                                    <label class="form-check-label" for="genre-mme">
                                        Mme
                                    </label>
                                </div>
                                <div class="form-check mr-4">
                                    <input class="form-check-input" type="radio" name="gender" id="genre-mlle" value="mlle">
                                    <label class="form-check-label" for="genre-mlle">
                                        Mlle
                                    </label>
                                </div>
                            </div>
                            <div class="form-group mt-3 d-none">
                                <input type="text" name="type_account" value="challenger" hidden >
                                {{-- <select name="type_account" id="type_account" class="form-control input-style placeholder-style" onchange="showDiv(this.value)">
                                    <option value="">Je souhaite être ?</option>
                                    <option value="challenger">Self Challenger</option>
                                    <option value="coach">Coach</option>
                                    <option value="partner">Partner</option>
                                </select> --}}
                            </div>
                            <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }} mt-3">
                                <label for="first_name">Prénom(s) <span style="color:red">*</label>
                                <input type="text" name="first_name" class="form-control input-style placeholder-style" id="first_name"  placeholder="Prénom *" value="{!! isset($data) && $data['first_name'] ? $data['first_name'] : old('first_name') !!}"/>
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong style="color:red">{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }} mt-3">
                                <label for="last_name">Nom <span style="color:red">*</label>
                                <input type="text" name="last_name"  class="form-control input-style placeholder-style" id="last_name" placeholder="Nom * " value="{!! isset($data) && $data['last_name'] ? $data['last_name'] : old('last_name') !!}"/>
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong style="color:red">{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} mt-3">
                                <label for="email">Email <span style="color:red">*</label>
                                <input type="email" name="email"  class="form-control input-style placeholder-style" id="email" value="@if(isset($data) && $data['email']) {{$data['email']}} @else {{ old('email') }} @endif" placeholder="Adresse e-mail profesionnelle *"/>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong style="color:red">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="phone_number">Numéro de téléphone <span style="color:red">*</label>
                                <input type="text" name="phone_number" class="form-control" id="phone_number" placeholder="Numéro de telephone *" data-msg="x" style="display: inline-block;" value="{{old('phone_number')}}" />
                                <input type="hidden" name="dial_code" id="dial_code">
                                <input type="hidden" name="country_code" id="country_code" value="" />
                                <div>
                                    <span id="valid-msg" class="hide"></span>
                                        <span id="error-msg" class="hide" style="color:red;"></span>
                                    <div class="validation"></div>

                                    <span class="@error('phone_number') text-danger @enderror">@error('phone_number'){{ $message }}@enderror</span>
                                </div>
                            </div>
                            {{-- Champs Facultatifs --}}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('commune') ? ' has-error' : '' }} mt-3">
                                        <!-- <label for="">Commune</label> -->
                                        @php
                                            $villes = \App\Models\ville::all();
                                        @endphp
                                        <label for="commune">Commune</label>
                                        <select name="commune" class="form-control input-style placeholder-style"  id="commune">
                                            <option value="">Selectionner ...</option>
                                            @foreach($villes as $ville)
                                                <option value="{{$ville->name}}">{{$ville->name}}</option>
                                            @endforeach
                                        </select>
                                        {{--<input type="text" name="commune" class="form-control input-style placeholder-style" id="commune" value="{{ old('commune') }}"placeholder="Entrez votre commune"/>--}}
                                        @if ($errors->has('commune'))
                                            <span class="help-block">
                                                <strong style="color:red">{{ $errors->first('commune') }}</strong>
                                            </span>
                                        @endif
                                        <input type="hidden" name="lat" id="lat_input">
                                        <input type="hidden" name="lon" id="lon_input">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('quartier') ? ' has-error' : '' }} mt-3">
                                        <!-- <label for="">Quartier</label> -->
                                        <label for="quartier">Quartier</label>
                                        <input type="text"  name="quartier" class="form-control input-style placeholder-style" id="quartier" min="50" value="{{ old('quartier') }}"placeholder="Entrez votre quartier"/>
                                        @if ($errors->has('quartier'))
                                            <span class="help-block">
                                                <strong style="color:red">{{ $errors->first('quartier') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('birthday') ? ' has-error' : '' }} mt-3">
                                        <label for="">Date de naissance</label>
                                        <input type="date" name="birthday" class="form-control input-style placeholder-style" id="birthday" value="{{ old('birthday') }}"placeholder="Entrez votre taille"/>
                                        @if ($errors->has('birthday'))
                                            <span class="help-block">
                                                <strong style="color:red">{{ $errors->first('birthday') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                {{-- <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('target_weight') ? ' has-error' : '' }} mt-3">
                                        <label for="">Entrez votre poids cible</label>
                                        <input type="number" name="target_weight" class="form-control input-style placeholder-style" id="target_weight" value="{{ old('target_weight') }} "placeholder="Poids cible"/>
                                        @if ($errors->has('target_weight'))
                                            <span class="help-block">
                                                <strong style="color:red">{{ $errors->first('target_weight') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>--}}
                            </div>
                            {{-- / Champs Facultatifs --}}
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} mt-2">
                                <label for="password">Mot de passe <span style="color:red">*</span></label>
                                <input required type="password" name="password" class="form-control input-style placeholder-style" id="password" aria-describedby="password" placeholder="Mot de passe *"/>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong style="color:red">{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }} mt-2">
                                <label for="password_confirmation">Confirmation du mot de passe <span style="color:red">*</span></label>
                                <input required type="password" name="password_confirmation" class="form-control input-style placeholder-style" id="password-confirm" placeholder="Confirmation du Mot de passe *"/>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong style="color:red">{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <label for="">Comment avez-vous connu Golden Health ? </label>
                            <div class="input-group mb-3">
                                <select name="discovery_source" class="custom-select input-style placeholder-style" id="discovery_source" >
                                    <option value="">Choisir ...</option>
                                    <option value="search_engine"> Moteur de recherche (Bing, Google, Yahoo...)</option>
                                    <option value="website"> Site Internet autre qu'un moteur de recherche (Blog, presse...)</option>
                                    <option value="written-press"> Presse écrite</option>
                                    <option value="social-media"> Réseaux sociaux (Facebook, Instagram...)</option>
                                    <option value="word-of-mouth"> Bouche à oreilles (Amis, famille, collègue...)</option>
                                    <option value="corporate-lesson"> Cours en entreprise</option>
                                    <option value="subway-display"> Affichage métro</option>
                                    <option value="coach"> Par un coach</option>
                                    <option value="other"> Autre (Salon, évènement...)</option>
                                </select>
                            </div>

                            @include('customer_frontOffice.layouts_customer.inc.input.terms')
                            <div style="margin-top: 15px" class="g-recaptcha brochure__form__captcha" data-sitekey="6LcWRaQdAAAAAMRicMpnxAL1V1eMRDQWC6kONe9Q"></div>
                            @if ($errors->has('g-recaptcha-response'))
                                <span class="help-block">
                                    <strong style="color:red">{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                            @endif
                            <button type="submit" class="btn login-button col-lg-12 mt-4">S'inscrire</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

{{-- 
    CAPTACHA 
    @online: 
    CLE DU SITE: 6LcWRaQdAAAAAMRicMpnxAL1V1eMRDQWC6kONe9Q
    CLE SECRETE : 6LcWRaQdAAAAAK6a4zYaJVW6l_lq0M_mlJE3baG8

    
    --}}

@endsection

@push('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        function showDiv(val){
            document.getElementById("partner").style.display = 'none'
            document.getElementById("coach").style.display = 'none'

            if(val != "" && val != null && val != "challenger"){
                document.getElementById(val).style.display = 'block'
            }
        }
    </script>
    <script>

        function getLocation(str) {
            value = str.match(/(\d+)(\.\d+)?/g); // trim
            return value;
        }

        //search = document.getElementById("input_search");
        const matchList = document.getElementById("match-list")

        $("#commune").autocomplete({
            source: function(query, callback) {
                $.getJSON(
                    `https://nominatim.openstreetmap.org/search?q=${query.term}&countrycodes=CI&format=json&addressdetails=1&limit=100`,
                    function(data) {
                        var places = data;
                        places = places.map(place => {
                            console.log("==== place ===");
                            console.log(place);
                            return {
                                title: place.display_name.replace(/<br\/>/g, ", "),
                                value: place.display_name.replace(/<br\/>/g, ", "),
                                lat: place.lat,
                                lon: place.lon,
                                id: place.osm_id
                            };
                        });
                        return callback(places);
                    });
            },
            minLength: 2,
            select: function(event, ui) {
                console.log(ui);
                $('#lat_input').val(ui.item.lat);
                $('#lon_input').val(ui.item.lon);
                console.log("Selected: " + ui.item.title);
            },
            open: function() {
                $('.ui-autocomplete').css('background-color', '#808080').css('width', '305px'); // HERE
            }
        });

        //Search Address to api
        /* const searchStates = async searchText =>{
             const response = await fetch(`https://nominatim.openstreetmap.org/search?q=${search.value}&countrycodes=CI&format=json&addressdetails=1&limit=100`);
             const states = await response.json();
             // console.log(states);
             let matches = states.filter(state =>{
                 const regex= new RegExp(`^${searchText}`, 'gi');
                 // console.log(state.display_name);
                 return state.display_name.match(regex);
             });
             if(searchText.length === 0){
                 matches = [];
                 matchList.innerHTML = "";
             }
             console.log(matches);
             outputHtml(matches);
         };
         const outputHtml = matches =>{
             if(matches.length > 0){
                 const html = matches.map(
                 (match, i)=>`
                 <div class="d-flex justify-content-center card card-body w-150"
                     id='${match.place_id}' style='margin-top :-20px; cursor: pointer;' onmouseover="this.style.backgroundColor='rgba(250,240,230,0.95)';" onmouseout="this.style.backgroundColor='white';"  onclick="startSearch(${match.lat}, ${match.lon})"; >
                         <h6 class="text-primary">
                         <span>${match.display_name}</span>
                         ${(match.address.country_code).toUpperCase()}  <br/>
                         <small>Lat : ${match.lat} /Lon :${match.lon} <small/>
                     </h6>
                 </div>`
                 ).join('');
                 console.log(html)
                 matchList.innerHTML = html;
             }
         }
         search.addEventListener('input', ()=>searchStates(search.value));
         function startSearch(lat, lon){
             console.log(lat)
             console.log(lon)
             document.getElementById('lat_input').value = lat
             document.getElementById('lon_input').value = lon
             document.getElementById("search_form").submit()
         }*/

    </script>

<script src="{{asset('js/intlTelInput.js')}}"></script>
<script>
    var input = document.querySelector("#phone_number");

    var errorMsg = document.querySelector("#error-msg"),
        validMsg = document.querySelector("#valid-msg"),
            form500 = document.querySelector("#form500");


    var errorMap = ["&#65794; Numéro Invalide !", "&#65794; Code de pays invalide !", "&#65794; Numéro de téléphone Trop Court", "&#65794; Numéro de téléphone Trop Long", "&#65794; Numéro Invalide"];
    var iti = window.intlTelInput(input, {
        dropdownContainer: document.body,
        separateDialCode: true,
        initialCountry: "CI",
        geoIpLookup: function(success, failure) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
            var countryCode = (resp && resp.country) ? resp.country : "";
            success(countryCode);
            });
        },
        placeholderNumberType: "MOBILE",
        preferredCountries: ['ci'],
        utilsScript: "{{asset('js/utils.js?')}}",
    });
    var reset = function() {
        input.classList.remove("error");
        errorMsg.innerHTML = "";
        errorMsg.classList.add("hide");
        validMsg.classList.add("hide");
    };

    // on blur: validate
    input.addEventListener('blur', function() {
        reset();
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
                $("#dial_code").val(iti.getSelectedCountryData().dialCode);
                // $("#phone_number").val(iti.);
                $("#country_code").val(iti.getSelectedCountryData().iso2.toUpperCase());
                // $()
                // console
                if($("#dial_code").val() == ''){
                    $('#whatsapp_number').val("225"+$(this).val())
                }else{
                    $('#whatsapp_number').val($("#dial_code").val()+$("#phone_number").val())
                }
                form500.submit();

                // $("#whatsapp_number").val($("#dial_code").value+$("#phone_number").value);
                //console.log(iti.s.phone);
                validMsg.classList.remove("hide");
            } else {
                input.classList.add("error");
                var errorCode = iti.getValidationError();
                // console.log("Error from intInput");
                // console.log(errorCode);
                errorMsg.innerHTML = errorMap[errorCode];
                errorMsg.classList.remove("hide");
            }
        }
    });

    // on keyup / change flag: reset
    input.addEventListener('change', reset);
    input.addEventListener('keyup', reset);
</script>
@endpush
