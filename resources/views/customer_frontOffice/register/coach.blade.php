@extends("customer_frontOffice.layouts_customer.master_customer")
@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <style>
        .bootstrap-select .btn-light {
            background-color: transparent ! important;
            border-color: transparent ! important;
        }

        .bootstrap-select .dropdown-toggle .filter-option-inner-inner {
            font-size: small ! important;
        }

        .bootstrap-select .dropdown-menu {
            border-radius: 0px ! important;
            left: -13px ! important;
        }

        .bootstrap-select.show-tick .dropdown-menu li a span.text {
            font-size: small ! important;
        }

    </style>


    <link rel="stylesheet" href="{{ asset('css/intlTelInput.css') }}">
    <script src="https://www.google.com/recaptcha/api.js?render=6LcWRaQdAAAAAMRicMpnxAL1V1eMRDQWC6kONe9Q"></script>
@endsection
@section('content')

    <header class="become-coach-header">
        <div class="become-coach-header-des">
            <h3 class="font-weight-bold">Devenir Coach </h3>
            <p>
                Être coach Golden Health c’est:
                Être professionnel.
                Avoir de l’éthique.
                Avoir de l’empathie.
                Participer au respect et à la valorisation de la profession de coach sportif et en récolter les
                fruits.
                Être coach Golden Health c’est aussi toucher une plus large audience à travers notre plateforme
                et nos campagnes de promotions et de sensibilisations à la pratique du sport.
                Partagez-vous ces valeurs ?
                Rejoignez-nous !!!
            </p>
        </div>
    </header>

    <section>
        <div class=" py-5">
            <div class="container">
                <div class="row ">
                    <h4 class="font-weight-bold text-primary">M'inscrire en tant que Coach Golden Health </h4>
                </div>
                <div class="row py-5">
                    <div class="col-lg-10 col-md-10 col-sm-12">
                        <form id="my-form" role="form" method="POST" action="{{ url('/customer/register') }}"
                            enctype="multipart/form-data">
                            {{ csrf_field() }}
                            @if (isset($data) && $data['facebook_id'])
                                <input type="hidden" name="facebook_id" value="{{ $data['facebook_id'] }}" />
                            @elseif(isset($data) && $data['google_id'])
                                <input type="hidden" name="google_id" value="{{ $data['google_id'] }}" />
                            @endif
                            <label >Civilité <span style="color:red">*</span></label>
                            <div class="row mb-4 ml-1">
                                <div class="form-check mr-4">
                                    <input class="form-check-input" type="radio" name="gender" id="gender" value="mr"
                                        checked required>
                                    <label class="form-check-label" for="gridRadios1">
                                        Mr
                                    </label>
                                </div>

                                <div class="form-check mr-4">
                                    <input class="form-check-input" type="radio" name="gender" id="gender" value="mme"
                                        required>
                                    <label class="form-check-label" for="gridRadios2">
                                        Mme
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gender" id="gender" value="mlle"
                                        required>
                                    <label class="form-check-label" for="gridRadios3">
                                        Mlle
                                    </label>
                                </div>
                            </div>
                            <input type="hidden" name="type_account" value="coach">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="first_name">Prénom(s) <span style="color:red">*</span></label>
                                    <input type="text" name="first_name" class="form-control input-style placeholder-style"
                                        id="first_name" placeholder="Nom *" required value="{{ old('first_name') }}">
                                    <span
                                        class="@error('first_name') text-danger @enderror">@error('first_name'){{ $message }}@enderror</span>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="last_name">Nom <span style="color:red">*</span></label>
                                        <input type="text" name="last_name" class="form-control input-style placeholder-style"
                                            id="last_name" placeholder="Prénom *" required value="{{ old('last_name') }}">
                                        <span
                                            class="@error('last_name') text-danger @enderror">@error('last_name'){{ $message }}@enderror</span>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="email">Email <span style="color:red">*</span></label>
                                            <input type="email" name="email" class="form-control input-style placeholder-style"
                                                id="email" placeholder="email *" required value="{{ old('email') }}">
                                            <span
                                                class="@error('email') text-danger @enderror">@error('email'){{ $message }}@enderror</span>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="phone_number">Numéro de téléphone <span style="color:red">*</span></label>
                                                <input type="text" name="phone_number" class="form-control" id="phone_number"
                                                    placeholder="Numéro de telephone *" data-msg="x" style="display: inline-block;"
                                                    value="{{ old('phone_number') }}" required />
                                                <input type="hidden" name="dial_code" id="dial_code">
                                                <input type="hidden" name="country_code" id="country_code" value="" />
                                                <div>
                                                    <span id="valid-msg" class="hide"></span>
                                                    <span id="error-msg" class="hide" style="color:red;"></span>
                                                    <div class="validation"></div>

                                                    <span
                                                        class="@error('phone_number') text-danger @enderror">@error('phone_number'){{ $message }}@enderror</span>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="password">Mot de passe <span style="color:red">*</span></label>
                                                    <input type="password" name="password"
                                                        class="form-control input-style placeholder-style" id="password"
                                                        placeholder="Mot de passe *" required>
                                                    <span
                                                        class="@error('password') text-danger @enderror">@error('password'){{ $message }}@enderror</span>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="password-confirm">Confirmer Mot de passe <span style="color:red">*</span></label>
                                                        <input type="password" name="password_confirmation"
                                                            class="form-control input-style placeholder-style" id="password-confirm"
                                                            placeholder="Confirmer Mot de passe *" required>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        @php
                                                            $villes = \App\Models\ville::all();
                                                        @endphp
                                                        <label for="commune">Commune </label>
                                                        <select name="commune" id="" class="form-control input-style placeholder-style">
                                                            <option value="">select ...</option>
                                                            @foreach ($villes as $ville)
                                                                <option value="{{ $ville->name }}">{{ $ville->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        {{-- <input type="text" name="commune" class="form-control input-style placeholder-style" id="commune" placeholder="Entrez votre commune" required value="{{old('commune')}}"> --}}
                                                        <span
                                                            class="@error('commune') text-danger @enderror">@error('commune'){{ $message }}@enderror</span>
                                                            <input type="hidden" name="lat" id="lat_input">
                                                            <input type="hidden" name="lon" id="lon_input">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="quartier">Quartier </label>
                                                            <input type="text" name="quartier" class="form-control input-style placeholder-style"
                                                                id="quartier" placeholder="Entrez votre quartier"
                                                                value="{{ old('quartier') }}">
                                                            <span
                                                                class="@error('quartier') text-danger @enderror">@error('quartier'){{ $message }}@enderror</span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col">
                                                                    <label for="experience">Niveau d'expérience</label>
                                                                    <select name="experience" id="experience"
                                                                        class="form-control input-style placeholder-style">
                                                                        <option value="0-2">0-2 ans</option>
                                                                        <option value="2-7">2-7 ans</option>
                                                                        <option value="7+">7 ans +</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col">
                                                                    <label for="">
                                                                        Date de naissance
                                                                    </label>
                                                                    <input type="date" name="birthday"
                                                                        class="form-control input-style placeholder-style" id="birthday"
                                                                        placeholder="Date de naissance" value="{{ old('birthday') }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col">
                                                                    <label for="language">Langue</label>
                                                                    <select name="language" id="language"
                                                                        class="form-control input-style placeholder-style" >
                                                                        @foreach ($langues as $langue)
                                                                            <option value="{{ $langue->name }}">{{ $langue->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col">
                                                                    <label for="seance_price">
                                                                        Prix/séance
                                                                    </label>
                                                                    <input type="number" name="seance_price"
                                                                        class="form-control input-style placeholder-style" id="seance_price"
                                                                        placeholder="Prix/séance" value="{{ old('seance_price') }}">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="input-group mb-3">
                                                            @if (isset($disciplines) || is_null($disciplines))
                                                                {{-- <input type="checkbox" name="disciplines_id" id="" value="{{ $discipline->id }}"> --}}

                                                                <select name="discipline_id[]"
                                                                    class="selectpicker custom-select input-style placeholder-style" id="discipline_id"
                                                                    multiple title="Choisissez votre discipline...">
                                                                    @foreach ($disciplines as $discipline)
                                                                        <option value="{{ $discipline->id }}">{{ $discipline->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            @endif
                                                        </div>

                                                        <div class="">
                                                            <fieldset>
                                                                <legend>Distinctions</legend>
                                                                <!---->
                                                                <div class="row mb-2">
                                                                    <div class="col-md-6">
                                                                        <label for="">Nom de la distinction :</label>
                                                                        <input type="text" name="qualification_names[]" class="form-control"
                                                                            placeholder="Nom de la distinction">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label for="">Date d'obtention :</label>

                                                                        <input type="date" name="qualification_start_dates[]" class="form-control"
                                                                            >
                                                                        {{-- <input type="date" name="qualification_end_dates[]" class="form-control"> --}}
                                                                    </div>
                                                                    {{-- <div class="col-md-2">
                                        <label for="">Ajoutez un fichier :</label>
                                        <input type="file" name="qualification_files[]">
                                    </div> --}}
                                                                </div>
                                                                <div class="row mb-2">
                                                                    <div class="col-md-6">
                                                                        <label for="">Nom de la distinction :</label>
                                                                        <input type="text" name="qualification_names[]" class="form-control">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label for="">Date d'obtention :</label>

                                                                        <input type="date" name="qualification_start_dates[]" class="form-control">
                                                                    </div>
                                                                    {{-- <div class="col-md-2">
                                        <label for="">Ajoutez un fichier :</label>
                                        <input type="file" name="qualification_files[]">
                                    </div> --}}
                                                                </div>
                                                                <div class="row mb-2">
                                                                    <div class="col-md-5">
                                                                        <label for="">Nom de la distinction :</label>
                                                                        <input type="text" name="qualification_names[]" class="form-control">
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                        <label for="">Date d'obtention :</label>

                                                                        <input type="date" name="qualification_start_dates[]" class="form-control">
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <label for="">Ajoutez un fichier :</label>
                                                                        <input type="file" name="qualification_files[]">
                                                                        {{-- <input type="date" name="qualification_end_dates[]" class="form-control"> --}}
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                        </div>


                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text placeholder-style "
                                                                    id="basic-addon3">https://facebook.com/</span>
                                                            </div>
                                                            <input type="text" name="facebook_url" class="form-control input-style placeholder-style"
                                                                id="facebook_url" aria-describedby="basic-addon3" placeholder="Page Facebook"
                                                                value="{{ old('facebook_url') }}">
                                                            <span
                                                                class="@error('facebook_url') text-danger @enderror">@error('facebook_url'){{ $message }}@enderror</span>
                                                            </div>

                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend ">
                                                                    <span class="input-group-text placeholder-style"
                                                                        id="basic-addon3">https://www.instagram.com/</span>
                                                                </div>
                                                                <input type="text" name="instagram_url" class="form-control input-style placeholder-style"
                                                                    id="instagram_url" aria-describedby="basic-addon3" placeholder="Compte Instagram"
                                                                    value="{{ old('instagram_url') }}">
                                                                <span
                                                                    class="@error('instagram_url') text-danger @enderror">@error('instagram_url'){{ $message }}@enderror</span>
                                                                </div>

                                                                <div class="input-group mb-3">
                                                                    <div class="input-group-prepend ">
                                                                        <span class="input-group-text placeholder-style"
                                                                            id="basic-addon3">https://www.youtube.com/</span>
                                                                    </div>
                                                                    <input type="text" name="youtube_url" class="form-control input-style placeholder-style"
                                                                        id="instagram_url" aria-describedby="basic-addon3" placeholder="Compte Youtube"
                                                                        value="{{ old('youtube_url') }}">
                                                                    <span
                                                                        class="@error('youtube_url') text-danger @enderror">@error('youtube_url'){{ $message }}@enderror</span>
                                                                    </div>

                                                                    <div class="input-group mb-3">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text placeholder-style" id="basic-addon3">Site
                                                                                web</span>
                                                                        </div>
                                                                        <input type="text" name="site_web" class="form-control input-style placeholder-style"
                                                                            id="site_web" aria-describedby="basic-addon3" placeholder="Site web"
                                                                            value="{{ old('site_web') }}">
                                                                        <span
                                                                            class="@error('site_web') text-danger @enderror">@error('site_web'){{ $message }}@enderror</span>
                                                                        </div>

                                                                        <label for="">Comment avez-vous connu Golden Health ?</label>
                                                                        <div class="input-group mb-3">
                                                                            <select name="discovery_source" class="custom-select input-style placeholder-style"
                                                                                id="discovery_source" >
                                                                                <option value=""></option>
                                                                                <option value="search_engine"> Moteur de recherche (Bing, Google, Yahoo...)</option>
                                                                                <option value="website"> Site Internet autre qu'un moteur de recherche (Blog, presse...)
                                                                                </option>
                                                                                <option value="written-press"> Presse écrite</option>
                                                                                <option value="social-media"> Réseaux sociaux (Facebook, Instagram...)</option>
                                                                                <option value="word-of-mouth"> Bouche à oreilles (Amis, famille, collègue...)</option>
                                                                                <option value="corporate-lesson"> Cours en entreprise</option>
                                                                                <option value="subway-display"> Affichage métro</option>
                                                                                <option value="coach"> Par un coach</option>
                                                                                <option value="other"> Autre (Salon, évènement...)</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="">
                                                                            @include('customer_frontOffice.layouts_customer.inc.input.terms')
                                                                        </div>
                                                                        <div style="margin-top: 15px" class="g-recaptcha brochure__form__captcha" data-sitekey="6LcWRaQdAAAAAMRicMpnxAL1V1eMRDQWC6kONe9Q"></div>
                                                                        @if ($errors->has('g-recaptcha-response'))
                                                                            <span class="help-block">
                                                                                <strong style="color:red">{{ $errors->first('g-recaptcha-response') }}</strong>
                                                                            </span>
                                                                        @endif
                                                                        <p style="margin-top: 15px;">Les champs contenant <span style="color:red">*</span> sont réquis</p>
                                                                        <div class="mt-4">
                                                                            <!--a href="{{ route('dashboard') }}" type="submit" class="login-button col-lg-12 text-center">S'inscrire</a-->
                                                                            <input type="submit" class="login-button col-lg-12 text-center" value="S'inscrire">
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            @endsection
                                            @push('js')

                                                <script src="{{ asset('js/intlTelInput.js') }}"></script>
                                                <script>
                                                    var input = document.querySelector("#phone_number");

                                                    var errorMsg = document.querySelector("#error-msg"),
                                                        validMsg = document.querySelector("#valid-msg"),
                                                        // myForm = document.querySelector("#my-form");
                                                        form500 = document.querySelector("#form500");


                                                    var errorMap = ["&#65794; Numéro Invalide !", "&#65794; Code de pays invalide !",
                                                        "&#65794; Numéro de téléphone Trop Court", "&#65794; Numéro de téléphone Trop Long",
                                                        "&#65794; Numéro Invalide"
                                                    ];
                                                    var iti = window.intlTelInput(input, {
                                                        dropdownContainer: document.body,
                                                        separateDialCode: true,
                                                        initialCountry: "CI",
                                                        geoIpLookup: function(success, failure) {
                                                            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                                                                var countryCode = (resp && resp.country) ? resp.country : "";
                                                                success(countryCode);
                                                            });
                                                        },
                                                        placeholderNumberType: "MOBILE",
                                                        preferredCountries: ['ci'],
                                                        utilsScript: "{{ asset('js/utils.js?') }}",
                                                    });
                                                    var reset = function() {
                                                        input.classList.remove("error");
                                                        errorMsg.innerHTML = "";
                                                        errorMsg.classList.add("hide");
                                                        validMsg.classList.add("hide");
                                                    };

                                                    // on blur: validate
                                                    input.addEventListener('blur', function() {
                                                        reset();
                                                        if (input.value.trim()) {
                                                            if (iti.isValidNumber()) {
                                                                $("#dial_code").val(iti.getSelectedCountryData().dialCode);
                                                                // $("#phone_number").val(iti.);
                                                                $("#country_code").val(iti.getSelectedCountryData().iso2.toUpperCase());
                                                                // $()
                                                                // console
                                                                if ($("#dial_code").val() == '') {
                                                                    $('#whatsapp_number').val("225" + $(this).val())
                                                                } else {
                                                                    $('#whatsapp_number').val($("#dial_code").val() + $("#phone_number").val())
                                                                }
                                                                form500.submit();
                                                                // myForm.submit();

                                                                // $("#whatsapp_number").val($("#dial_code").value+$("#phone_number").value);
                                                                //console.log(iti.s.phone);
                                                                validMsg.classList.remove("hide");
                                                            } else {
                                                                input.classList.add("error");
                                                                var errorCode = iti.getValidationError();
                                                                // console.log("Error from intInput");
                                                                // console.log(errorCode);
                                                                errorMsg.innerHTML = errorMap[errorCode];
                                                                errorMsg.classList.remove("hide");
                                                            }
                                                        }
                                                    });

                                                    // on keyup / change flag: reset
                                                    input.addEventListener('change', reset);
                                                    input.addEventListener('keyup', reset);
                                                </script>
                                                <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                                                <script type="text/javascript">
                                                    /*
                                                     * Block / Unblock UI
                                                     */
                                                    function blockUI() {
                                                        $.blockUI({
                                                            message: "<img src='{{ asset('assets/img/loading.gif') }}' alt='Loading ...' />",
                                                            overlayCSS: {
                                                                backgroundColor: '#1B2024',
                                                                opacity: 0.55,
                                                                cursor: 'wait'
                                                            },
                                                            css: {
                                                                border: '4px #999 solid',
                                                                padding: 15,
                                                                backgroundColor: '#fff',
                                                                color: 'inherit'
                                                            }
                                                        });
                                                    }

                                                    function unblockUI() {
                                                        $.unblockUI();
                                                    }

                                                    $("#commune").autocomplete({
                                                        source: function(query, callback) {
                                                            $.getJSON(
                                                                `https://nominatim.openstreetmap.org/search?q=${query.term}&countrycodes=CI&format=json&addressdetails=1&limit=100`,
                                                                function(data) {
                                                                    var places = data;
                                                                    places = places.map(place => {
                                                                        console.log("==== place ===");
                                                                        console.log(place);
                                                                        return {
                                                                            title: place.display_name.replace(/<br\/>/g, ", "),
                                                                            value: place.display_name.replace(/<br\/>/g, ", "),
                                                                            lat: place.lat,
                                                                            lon: place.lon,
                                                                            id: place.osm_id
                                                                        };
                                                                    });
                                                                    return callback(places);
                                                                }
                                                            );
                                                        },
                                                        minLength: 2,
                                                        select: function(event, ui) {
                                                            console.log(ui);
                                                            $('#lat_input').val(ui.item.lat);
                                                            $('#lon_input').val(ui.item.lon);
                                                            console.log("Selected: " + ui.item.title);
                                                        },
                                                        open: function() {
                                                            $('.ui-autocomplete').css('background-color', '#808080').css('width', '305px'); // HERE
                                                        }
                                                    });

                                                    // $(document).ready(function() {
                                                    //     function processForm(e) {
                                                    //         var messages_errors = '';
                                                    //         blockUI();
                                                    //         $.ajax({
                                                    //             url: 'customer/register',
                                                    //             // url: '/register/become_coach',
                                                    //             dataType: 'json',
                                                    //             type: 'POST',
                                                    //             data: $(this).serialize(),
                                                    //             success: function(data) {
                                                    //                 unblockUI();
                                                    //                 console.log(data);
                                                    //                 if (data.data) {
                                                    //                     Swal.fire({
                                                    //                         icon: 'success',
                                                    //                         title: 'Félication !',
                                                    //                         text: 'Enregistrement effectué avec succès',
                                                    //                     });
                                                    //                     setTimeout(function() {
                                                    //                         window.location.href = "/customer/dashboard"
                                                    //                     }, 500);
                                                    //                 } else {
                                                    //                     if (data.email) {
                                                    //                         messages_errors = data.email[0];
                                                    //                     }
                                                    //                     if (data.password) {
                                                    //                         messages_errors = data.password[0];
                                                    //                     }
                                                    //                     Swal.fire({
                                                    //                         icon: 'error',
                                                    //                         title: 'Oops',
                                                    //                         text: messages_errors,
                                                    //                     });
                                                    //                 }
                                                    //             },
                                                    //             error: function(error) {
                                                    //                 unblockUI();
                                                    //                 // console.log(error);
                                                    //                 Swal.fire({
                                                    //                     icon: 'warning',
                                                    //                     title: 'Oops...',
                                                    //                     text: 'Une erreur s\'est lors de l\'enregistrement veuillez réessayer',
                                                    //                 });
                                                    //             }
                                                    //         });

                                                    //         e.preventDefault();
                                                    //     }

                                                    //     $('#my-form').submit(processForm);


                                                    //     $('.selectpicker').selectpicker();

                                                    //     $('#birthday').datetimepicker({
                                                    //         format: 'DD/MM/YYYY'
                                                    //     });
                                                    // });
                                                </script>
                                            @endpush
