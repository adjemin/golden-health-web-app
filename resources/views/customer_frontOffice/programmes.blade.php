@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
<style>
    .qui-somme-nous {
        line-height: 2.1;
    }

</style>
@endsection
@section('content')

<section>
    <header class="become-partner-header">
        <div class="become-coach-header-des">
            <h3 class="font-weight-bold">Programmes </h3>
            <p>
                Retrouvez vos différents programmes ici
            </p>
        </div>
    </header>
</section>




<section class="py-3 mx-5">
    <div class="container ">
        <h1 class="font-weight-bold primary-text-color mb-5 mt-3">Programmes </h1>

        <br>
        <div class="row">
            <div class="col-lg-4 col-md-4 pb-2">
                <a href="#">
                    <div class="event-content">
                        <img src={{asset ('customer/images/IMAGE.png') }} width="100%" alt="" srcset="">
                        <div class="mx-2 py-3">
                            <h5>Esprit Sain Corps Sain</h5>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-4 col-md-4 pb-2">
                <a href="#">
                    <div class="event-content">
                        <img src={{asset ('customer/images/IMAGE.png') }} width="100%" alt="" srcset="">
                        <div class="mx-2 py-3">
                            <h5>Transformation Radicale</h5>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-4 col-md-4 pb-2">
                <a href="#">
                    <div class="event-content">
                        <img src={{asset ('customer/images/IMAGE.png') }} width="100%" alt="" srcset="">
                        <div class="mx-2 py-3">
                            <h5>Sur-mesure</h5>
                        </div>
                    </div>
                </a>
            </div>

        </div>

    </div>
</section>


<section class="py-4">
    <div class="container">
        <p class="qui-somme-nous">Le Self-challenger (Le client) nous fait part de ses objectifs et contraintes, puis nous nous occupons du reste à travers l’élaboration d’un programme d’entrainement, la sélection des coachs adaptés, en plus des conseillers diététiques dédiés pour un bien-être complet.
        </p>

        <h6 class="font-weight-bold mt-5 mb-3"><u>NB:</u></h6>
        <p>Esprit Sain Corps Sain / Transformation Radicale / Sur-mesure, constituent les 3 titres de nos 3 programmes d’entrainements conçus par Golden Health</p>
    </div>
</section>

@endsection
