@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
<style>
    body {
        font-size: 13px;
        background-color: rgba(238, 238, 238, 0.336);

    }

    a {
        color: black;
    }

    a:hover {
        color: var(--primary)
    }

    #order .shadow {
        /* box-shadow: 1px 1px 5px 3px rgba(0, 0, 0, 0.08) !important; */
        /* min-height: 60px !important; */
        height:200px ;
        /* max-height: 300px !important; */
        text-align: center;
        border-radius: 10px !important;

    }

    @media only screen and (max-width: 576px) {

        #order .shadow {
            /* box-shadow: 1px 1px 5px 3px rgba(0, 0, 0, 0.08) !important; */
            min-height: 60px !important;
            /* height:200px !important; */
            /* max-height: 300px !important; */
            /* text-align: center; */

        }
    }

    #order .shadow p{
        font-size: 12px;
    }

    #order .shadow h5{
        font-size: 14px;
    }

    #order .packs .radio {
        margin: auto;
        width: 10px;
    }

    /* #order .packs .radio input {
        height: 20px;
        width: 20px;
        border-radius: 100px;
        border: 2px solid rgb(160, 158, 158);
    } */

    #order .packs .radio input {
    height: 20px;
    width: 20px;
    position: absolute;
    bottom: 20px;
    border-radius: 100px;
    /* border: 2px solid rgb(160, 158, 158); */
}

    /* .checked [type="radio"]:checked  {
        background: green !important;
        box-shadow: 0 0 0 0.25em #000;
    } */

    .packs .card {
        background-color:#1c6a49;
        color:#fff;
        font-size:16px
    }

    .packs .cardx{
        background-color:#eee;
        color:#000;
        font-size:14px
    }

    #order .clear {
        clear: both;
    }

    .radio, .checkbox {
        display: block;
        min-height: 21px;
        margin-top: 10px;
        margin-bottom: 10px;
        vertical-align: middle;
    }

    .card .card-body{
        transition: all 0.3s;
        border-radius: 10px !important;
    }
    .card .card-body:hover{
        background-color: #edaa0d !important;
    }
    .card .card-body.active{
        background-color: #edaa0d !important;
        color: white !important;
    }

    .packs .card {
    background-color: #fff !important;
    color: #fff;
    /* font-size: 16px; */
    }
</style>
@endsection

@section('content')
<section>
    <div class="container py-3">
        <div class="coach-identity">
            <div class="row" style="border: 1px solid #eee; padding:5px">
                <input type="hidden" name="availabilityId" id="availabilityId" value="{{$availability->id}}">
                <input type="hidden" name="startDate" id="startDate" value="{{ \Carbon\Carbon::parse($dates[0])->format('d/m/Y - H:i') }}">
                <input type="hidden" name="endDate" id="endDate" value="{{ \Carbon\Carbon::parse($dates[1])->format('d/m/Y - H:i')}}">
                <input type="hidden" name="course_id"    id="course-id" value="{{$course->id}}">
                <input type="hidden" name="nbperson" id="nbperson" value="{{$nbperson}}">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div>
                        <img src="{{ $coach->customer->photo_url }}" class="img-fluid" alt="">
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 py-3">
                    <div class="">
                        <div class="">
                            <h4 class="font-weight-bold">{{$coach->customer->name}}</h4>
                        </div>
                        <div class="">
                            <p>Coach {{$course->discipline->name}}</p>
                        </div>
                        <!--div class="">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc erat augue, dapibus
                                sed
                                felis at, imperdiet pretium .</p>
                        </div-->
                        @if(!$course->hasCurrentCustomerParticipated())
                        {{-- <div class="mb-4"> --}}
                            {!!$course->ratingView()!!}
                            {{-- <a href="#"><img src="{{asset('customer/images/stars.png')}}" class="icon" alt=""></a>
                            <a href="#"><img src="{{asset('customer/images/stars.png')}}" class="icon" alt=""></a>
                            <a href="#"><img src="{{asset('customer/images/stars.png')}}" class="icon" alt=""></a>
                            <a href="#"><img src="{{asset('customer/images/star.png')}}" class="icon" alt=""></a>
                            <a href="#"><img src="{{asset('customer/images/star.png')}}" class="icon" alt=""></a> --}}
                        {{-- </div> --}}
                        @endif
                        <div>
                            <button class="btn-outline-secondary">
                                <span class="">Tarif Indicatif</span> <br>
                                <?php $discount = ($course->price * $pack->percentage_discount / 100); ?>
                                <span class="secondary-text-color font-weight-bold h5" id="price_span">{{$course->price - $discount}} XOF</span>
                                <input type="hidden" name="course_price" id="course_price" value="{{$course->price}}">
                                <input type="hidden" name="final_price" id="final_price">
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 py-3">
                    <!--div class="mb-5 text-right">
                        <button class=" secondary-button">Réserver</button>
                    </div-->
                    <script type="text/javascript" src="{{ asset('customer/moment/moment.min.js') }}"></script>
                    <script type="text/javascript" src="{{ asset('customer/moment/moment-with-locales.js') }}"></script>
                    <div class="mb-4">
                        <p class="text-right"><b>Date du cours:</b>
                            {{-- {{ \Carbon\Carbon::parse($dates[0])->format('l j F Y') }}</p> --}}
                            <script>
                                moment.locale('fr')
                                document.write(moment('{{ \Carbon\Carbon::parse($dates[0]) }}').format('LL'))
                            </script>
                        <p>



                         </p>
                        <p class="text-right"><b>Heure: </b>
                            {{-- {{ \Carbon\Carbon::parse($dates[0])->format('H:i') }} --}}
                            <script>
                                    moment.locale('fr')
                                    document.write(moment('{{ \Carbon\Carbon::parse($dates[0]) }}').format('LT'))
                                </script>
                        </p>
                    </div>

                    <div class="text-right mt-5 ">
                        {{--<button class="ask-queestion"><img src="{{ asset('customer/images/conversation (1).png') }}" height="30" width="30" alt=""> Posez votre
                            question</button>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--  -->
<section id="order">
    <div class="container mb-3">
        <div class="row packs">
            @if ($packs)
                @foreach($packs as $pack_v)
                    @if($pack_v->slug == "cours-unique")
                        <div class="col-xs-12 col-sm-6  col-md-6 col-lg-3 mt-2">
                            <div class="card shadow" for="pack{{$pack_v->id}}">
                                <!-- <div class="card-header" style="background-color: #f5f5f5; border-color: #f5f5f5; color: #333">Cours unique</div> -->
                                <div class="card-body <?php if($pack->id == $pack_v->id) { echo "active"; } ?>"  style="background-color: #fff; color: #333;"#1c6a49 for="pack{{$pack_v->id}}">
                                    <div>
                                        <h5>Cours unique</h5>
                                        <p>
                                            Réserver uniquement ce cours.
                                        </p>
                                    </div>


                                    <div class="clear"></div>
                                    <div class="radio mt-3" for="pack{{$pack_v->id}}">
                                        <div class="radio">
                                            <span class="checked"><input id="pack{{$pack_v->id}}" class="pack_radio" type="radio" discount="{{$pack_v->percentage_discount}}" nblesson="{{$pack_v->nb_lesson}}" name="pack" <?php if($pack->id == $pack_v->id) { echo "checked"; } ?> value="{{$pack_v->id}}"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {{--<div class="col-xs-12 col-sm-6 mt-2">
                        <div class="card cardx" for="pack{{$pack_v->id}}">
                            <div class="card-header" style="background-color: #f5f5f5; border-color: #f5f5f5; color: #333;height:320px;">Cours unique</div>
                            <div class="card-body" for="pack{{$pack_v->id}}"> Réserver uniquement ce cours.<div class="clear"></div>
                                <div class="radio mt-3" for="pack{{$pack_v->id}}">
                                    <div class="radio">
                                        <span class="checked"><input id="pack{{$pack_v->id}}" class="pack_radio" type="radio" discount="{{$pack_v->percentage_discount}}" nblesson="{{$pack_v->nb_lesson}}" name="pack" <?php if($pack->id == $pack_v->id) { echo "checked"; } ?> value="{{$pack_v->id}}"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--}}
                    @else
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 mt-2">
                        <div class="card shadow" for="pack{{$pack_v->id}}" >
                            <!-- <div class="card-header" style="background-color: #f5f5f5; border-color: #f5f5f5; color: #333">PACK DE {{$pack_v->nb_lesson}} COURS</div> -->
                            <div class="card-body"  style="background-color: #fff; color: #333" for="pack{{$pack_v->id}}">
                                <h5 class="">Pack de {{$pack_v->nb_lesson}} cours</h5>
                                <h5><strong>Bénéficiez de <span class="price-percent-reduction small">-{{$pack_v->percentage_discount}}.0%</span> en commandant ce pack.</strong> </h5>
                                <p> Vous recevrez alors un <b>code</b> qui vous permettra de commander les <b>4 autres cours</b>.</p>
                                <div class="clear"></div>
                                <div class="radio mt-3" for="pack{{$pack_v->id}}">
                                    <div class="radio">
                                        <span class="checked"><input id="pack{{$pack_v->id}}" class="pack_radio" type="radio" discount="{{$pack_v->percentage_discount}}" nblesson="{{$pack_v->nb_lesson}}" name="pack" value="{{$pack_v->id}}" <?php if($pack->id == $pack_v->id) { echo "checked"; } ?>></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-xs-12 col-sm-6 mt-2">
                        <div class="card shadow" for="pack{{$pack_v->id}}">
                            <div class="card-header" style="background-color: #f5f5f5; border-color: #f5f5f5; color: #333; font-size:14px">PACK DE {{$pack_v->nb_lesson}} COURS</div>
                            <div class="card-body" for="pack{{$pack_v->id}}">
                                <p for="pack{{$pack_v->id}}"> Bénéficiez de <span class="price-percent-reduction small">-{{$pack_v->percentage_discount}}.0%</span> en commandant ce pack.</p>
                                <p> Vous recevrez alors un <b>code</b> qui vous permettra de commander les <b>{{$pack_v->nb_lesson}} autres cours</b>.</p>
                                <div class="clear"></div>
                                <div class="radio mt-3" for="pack{{$pack_v->id}}">
                                    <div class="radio">
                                        <span class="checked"><input id="pack{{$pack_v->id}}" class="pack_radio" type="radio" discount="{{$pack_v->percentage_discount}}" nblesson="{{$pack_v->nb_lesson}}" name="pack" value="{{$pack_v->id}}" <?php if($pack->id == $pack_v->id) { echo "checked"; } ?>></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--}}
                    @endif
                @endforeach
            @endif
        </div>
        <div class="row mt-5">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    @if(!auth()->guard('customer'))
                    <h6 class="font-weight-bold">Pack</h6>
                    <p>Si vous avez un pack connectez-vous pour l'utiliser</p>
                    <div class="input-group mb-3">
                        <a href="/customer/login" class="login-button">OK</a>
                    </div>
                    @endif
                    @if(auth()->guard('customer'))
                    <h6 class="font-weight-bold">Coupon</h6>
                    <p>Si vous avez un pack connectez-vous pour l'utiliser</p>
                    <div class="input-group mb-3">
                        <input type="text" id="coupon_code" name="coupon_code" placeholder="Entrez votre code" class="form-control input-style">
                        <div class="input-group-append">
                            <button type="button" class="login-button" id="submitCoupon">Appliquer</button>
                        </div>
                    </div>
                    <p id="coupon_error" class="text-danger font-weight-bold mt-1"></p>
                        @forelse($coupons as $coupon)
                            <div class="text-primary font-weight-bold text-uppercase my-2 text-right">
                                coupon
                                {{$coupon->code."  (-".$coupon->pourcentage."%) "}} Ajouté
                            </div>
                        @empty
                        @endforelse
                    @endif

                    {{--@if(auth()->guard('customer'))
                        <div class="input-group mb-3">

                        </div>
                    @endif--}}
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 mt-1">
                    <div class="text-right">
                    @php
                        $total_price = ($course->price - $discount) * $pack->nb_lesson;
                        foreach($coupons as $coupon){
                            $total_price *= (1- ((int) $coupon->pourcentage/100));
                        }
                    @endphp
                        <h5 class="font-weight-bold text-primary">Total TTC <span id="total_price">{{$total_price}} XOF</span></h5>
                        <div class="">
                            <button type="button" data-toggle="modal" data-target="#lieuModal" id="booking-next" class="login-button">Continuer</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grey-bg mt-5">
                <h4 class="font-weight-bold text-primary">Informations</h4>
                <p>
                Paiement : l'argent est versé au coach 72h après le cours, si celui-
                ci s'est bien déroulé.
                Annulation : tous les cours sont annulables gratuitement jusqu'à
                24h avant le début du cours. Il suffit d'aller dans votre espace
                réservations et de l'annuler. Vous pourrez ensuite reprogrammer
                votre séance selon vos disponibilités.
                Utilisation des packs : pour les packs, vous recevrez un code par
                mail. Il sera également disponible dans vos "Bons et Packs" dans
                votre espace personnel. Pour l'utiliser, il suffit de rentrer le code
                envoyé sur la page panier. Les packs sont valables 1 an à partir de
                la date d'achat.
                Enfin, si votre coach ne vous convenait pas, nous nous engageons
                à trouver un autre coach sans aucun frais.
                </p>
            </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="lieuModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Lieu du cours</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="select_delivery">Lieu souhaité du cours</label>
                        <select class="form-control" name="select_delivery" id="select_delivery">
                            @php
                                $lieux = collect($lieus)->map(function($lieu){
                                    return $lieu->lieucourse ?? null;
                                })->reject(function($lieu){
                                    return is_null($lieu) || empty($lieu);
                                })->unique();
                            @endphp
                            @if($lieux)
                                @foreach($lieux as $lieu)
                                    <option value="{{$lieu->id}}">{{$lieu->name}}</option>
                                @endforeach
                            @endif
                            {{--
                            <option value="5">Je souhaite que Golden Health me propose un lieu</option>
                            <option value="0">Dans le lieu suivant...</option>
                            --}}
                        </select>
                    </div>
                    <div id="lieu-1" style="display:none">
                        <div class="form-group">
                            <label for="address_autocomplete">Entrez votre lieu</label>
                            <input type="text" class="form-control" name="address_autocomplete" id="address_autocomplete">
                            <!--div class="details">
                                <input name="lat" type="text" value="">
                                <input name="lng" type="text" value="">
                                <input name="formatted_address" type="text" value="">
                            </div-->
                        </div>
                        <div class="form-group">
                            <label for="address_other_new" class="control-label">Informations complémentaires</label>
                            <textarea class="form-control" name="address_other_new" id="address_other_new" rows="4" placeholder="Essayez d'être le plus précis possible"></textarea>
                        </div>
                    </div>
                    <div id="lieu-2" style="display:none">
                        <div class="form-group">
                            <label for="message">Description</label>
                            <textarea class="form-control" name="message" rows="4" placeholder="Décrivez ce que vous souhaitez. Type de lieux, extétieur, intérieur..."></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                <button type="button" id="booking-save" class="btn btn-primary">Continuer</button>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection
@push('js')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDi_rXXj8hi3dT9qZlJjnB5ay3gCVVln_U&libraries=places"></script>
<script src="{{asset('js/jquery.geocomplete.js')}}"></script>
<script>
    $token = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });

    $("#submitCoupon").click(function name(params) {
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;

        var coupon_code = $("input[name='coupon_code']").val();

        $(this).html(_cuteLoader);
        var $btn = $(this);
        var $messageDiv = $("#coupon_error");

        if(coupon_code == ''){
            $("#coupon_error").html("Vous devez entrez un coupon");
            $("#submitCoupon").html("Vérifier")
            return false;
        }

        $.ajax({
                type: "POST",
                url: "{{ route('ajax.coupon.addStorage') }}",
                data: {
                    "coupon_code": coupon_code,
                },
                dataType: "json",
                success: function (response) {
                    //
                    console.log(">> success");
                    console.log(response);
                    if(response.status == null){
                        $btn.html("Veuillez réessayer plus tard");
                        $messageDiv.html("Une erreur est survenu");
                        return false;
                    }
                    // managing status codes
                    if(response.status != "OK"){
                        $btn.html("OUPS");
                        if(response.error != null){
                            if(response.error.message != null){
                                $messageDiv.html(response.error.message);
                                setTimeout(() => {
                                    $btn.html("Réessayer");
                                }, 1000);
                                return false;
                            }
                            $btn.html("Réessayer")
                        }

                    }
                    $btn.html("Coupon Ajouté");

                    window.location.reload();

                },
                error: function(response){
                    console.log(">> erreur");
                    $btn.html("Veuillez réessayer plus tard");
                    $messageDiv.html("Une erreur est survenue");
                    console.log(response);
                }
          });
      });
    /*
      * Block / Unblock UI
    */
    function blockUI() {
        $.blockUI({
            message: "<img src='{{asset('assets/img/loading.gif')}}' alt='Loading ...' />",
            overlayCSS: {
                backgroundColor: '#1B2024',
                opacity: 0.55,
                cursor: 'wait'
            },
            css: {
                border: '4px #999 solid',
                padding: 15,
                backgroundColor: '#fff',
                color: 'inherit'
            }
        });
    }

    function unblockUI() {
        $.unblockUI();
    }

    selectDelivery();

    function selectDelivery() {
        if ($('#select_delivery').val() == 0) {
            $('#lieu-1').show();
            $('#lieu-2').hide();
        } else if($('#select_delivery').val() == 5 || $('#select_delivery').val() == 6) {
            $('#lieu-1').hide();
            $('#lieu-2').show();
        }else{
            $('#lieu-1').hide();
            $('#lieu-2').hide();
        }
    }

    $(document).ready(function(){

        $('.pack_radio').on('change', function() {
            var check_pack = $('input[name=pack]:checked').val();
            var discount = $('input[name=pack]:checked').attr('discount');
            var nblesson = $('input[name=pack]:checked').attr('nblesson');
            var price = $('#course_price').val();
            var coupons = @json($coupons);
            console.log(coupons);
            console.log(discount);
            console.log(price);
            //console.log(check_pack);
            if (parseInt(discount) > 0) {
                var discount_price  = (parseInt(price) * discount) / 100;
                var final_price =  (parseInt(price) - discount_price);
                coupons.forEach(function(coupon){
                    final_price *= (1-(parseInt(coupon.pourcentage)/100));
                })
                $('#price_span').text(final_price +' XOF');
                var $res_price = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'XOF' }).format(final_price * nblesson);
                $('#total_price').text($res_price);
            }else {
                var $res_price = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'XOF' }).format(price);
                coupons.forEach(function(coupon){
                    price = parseInt(price) * (1-(parseInt(coupon.pourcentage)/100));
                })
                $('#final_price').val(price);
                $('#price_span').text(price +' XOF');
                $('#total_price').text($res_price);
            }
        });

        $('#select_delivery').on('change', function(){
            console.log($(this).val());
            selectDelivery();
        });

        $("#address_autocomplete").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo"
        });

        $('#booking-save').on('click', function(e){
            e.preventDefault();
           var pack = $("input[name='pack']:checked").val();
           var availability_id = $('#availabilityId').val();
           var startDate = $('#startDate').val();
           console.log('startDate :', startDate)
           var endDate = $('#endDate').val();
           console.log('endDate :', endDate)
           var course_id = $('#course-id').val();
           var select_delivery = $('#select_delivery').val();
           var address_autocomplete = $('#address_autocomplete').val();
           var address_other_new = $('#address_other_new').val();
           var nbperson = $('#nbperson').val();
           var message = $('#message').val();
           var _token = $("input[name='_token']").val();
           var fd = new FormData();
           fd.append('availability_id', availability_id);
           fd.append('start', startDate);
           fd.append('end', endDate);
           fd.append('course_id', course_id);
           fd.append('pack', pack);
           fd.append('select_delivery', select_delivery);
           fd.append('address_autocomplete', address_autocomplete);
           fd.append('address_other_new', address_other_new);
           fd.append('message', message);
           fd.append('nbperson', nbperson);
           fd.append('_token', _token);
           $('#lieuModal').modal('toggle');
           blockUI();
            console.log('ok');
            console.log(pack);
            $.ajax({
                url: '/booking/course',
                type: 'POST',
                data: fd,
                contentType:false,
                cache:false,
                processData:false,
                success: function(data){
                    unblockUI();
                    console.log(data);
                    if (data.data){
                        setTimeout(function() {
                          window.location.href = "/final/checkout"
                        }, 500);
                    }
                },
                error: function(error){
                    unblockUI();
                    console.log(error);
                    Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Une erreur s\'est produite veuillez réessayer' ,
                    });
                }
            });
        });
    });

    const RadioInput = document.querySelectorAll('.card-body input[type=radio]');
    for (let i = 0; i < RadioInput.length; i++) {
        RadioInput[i].addEventListener('click', function(e){
            let cardBodyActive = document.querySelectorAll('.card-body.active');
            cardBodyActive[0] = cardBodyActive[0].classList.remove("active");
            this.parentNode.parentNode.parentNode.parentElement = this.parentNode.parentNode.parentNode.parentElement.classList.add("active");
        });
    }
</script>
@endpush
