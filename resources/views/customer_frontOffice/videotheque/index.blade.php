@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
    <style>
        body {
            background-color: #eee;
        }

        a {
            color: black;
        }

    </style>

@endsection
@section('content')

    <!-- Barners -->

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="blog-barner">
                    <div class="des">
                        Bienvenue dans la vidéotheque !
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section>
        <div class="container mt-5 ">
            <h3 class="font-weight-bold ">Découvrez nos différentes vidéos</h3>
            <div class="secondary-dividers mb-2"></div>
        </div>
    </section>


    <section class="py-5">
        <div class="container">
            <div class="row">
                @if ($videos != null && count($videos)>0)
                    @foreach ($videos as $video)
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 mb-4">

                            <div class="py-3">
                                <div class="">

                                    <div class="embed-responsive embed-responsive-21by9 embed-responsive-16by9 embed-responsive-4by3 embed-responsive-1by1">
                                        @if($video->link_youtube != null)
                                            {{-- <iframe class="embed-responsive-item" src="{{$video->video_url }}" frameborder="0" allowfullscreen></iframe> --}}
                                            <iframe class="embed-responsive-item"  width="560" height="315" src="{{ $video->link_youtube }}" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
                                        @elseif($video->link_facebook != null)
                                            <iframe src="{{  $video->link_facebook }}" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"></iframe>
                                        @else
                                            <video class="embed-responsive-item" src="{{ $video->video }}" controls></video>
                                        @endif
                                    </div>
                                    <h5 class="font-weight-bold">{{ $video->titre }}</h5>
                                    {{--<p class="mb-4">
                                        {{ $video->title }}
                                    </p>--}}
                                    {{--<div class="text-right">
                                        <a href="{{ URL::to('blog-article/'.$video->slug ) }}" class="view-more text-center">Voir
                                            plus</a>
                                    </div>--}}
                                </div>
                            </div>
                        <!-- </div> -->
                    </div>
                    @endforeach

                @else
                    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
                        Aucune video disponible !
                    </div>
                @endif
            </div>


            {{-- <div class="row"> --}}
                {{-- <div class="col-lg-12">

                </div> --}}

                {{--<div class="col-lg-4">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 ">

                            <!-- Dernier articles -->
                            <h5 class="font-weight-bold ">Récents articles</h5>
                            <div class="secondary-dividers mb-4"></div>
                            <!--  -->
                            @if ($recentVideos != null)
                                @foreach ($recentVideos as $recentVideo)
                                    <div class="white-bg d-flex mb-3">
                                        <div>
                                            <iframe width="560" height="315" src="{{$recentVideo->url ?? 'https://www.youtube.com/embed/T8Lt8c43wVY' }}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        </div>

                                        <div class="px-2 py-1">
                                            <h6 class="secondary-text-color"><b>{{ $recentVideo->getCategoryAttribute()->name }}</b></h6>
                                            <p>{{ $recentVideo->title }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="white-bg d-flex">
                                    Aucun article récent disponible !
                                </div>
                            @endif


                            <!--  -->
                            <div class="white-bg d-flex mt-3">
                                <div>
                                    <img src="{{ asset('customer/images/78e60a1.jpg') }}" class="img-height" alt="">
                                </div>

                                <div class="px-2 py-1">
                                    <h6 class="secondary-text-color"><b>Fitness</b></h6>
                                    <p>Testez des activités sportives à la...</p>
                                </div>
                            </div>
                            <!--  -->
                            <div class="white-bg d-flex mt-3">
                                <div>
                                    <img src="{{ asset('customer/images/78e60a1.jpg') }}" class="img-height" alt="">
                                </div>

                                <div class="px-2 py-1 ">
                                    <h6 class="secondary-text-color"><b>Fitness</b></h6>
                                    <p>Testez des activités sportives à la...</p>
                                </div>
                            </div>


                            <!-- Anciens articles -->

                            <h5 class="mt-5 font-weight-bold">Anciens articles</h5>
                            <div class="secondary-dividers mb-4"></div>
                            @if ($oldVideos != null)
                                @foreach ($oldVideos as $oldVideo)
                                    <div class="white-bg d-flex mb-3">
                                        <div>
                                            <img src="{{ $oldVideo->cover }}" class="img-height" alt="">
                                        </div>

                                        <div class="px-2 py-1">
                                            <h6 class="secondary-text-color"><b>{{ $oldVideo->getCategoryAttribute()->name }}</b></h6>
                                            <p>{{ $oldVideo->title }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="white-bg d-flex">
                                    Aucun article récent disponible !
                                </div>
                            @endif
                        </div>
                    </div>
                </div>--}}


            {{-- </div> --}}

            {{-- <center><a href="#" class="view-more"> Charger plus</a></center> --}}
            <div class="mt-3 d-flex justify-content-center">
                {{-- {{ $videos->links() }} --}}
            </div>

        </div>

    </section>

@endsection
