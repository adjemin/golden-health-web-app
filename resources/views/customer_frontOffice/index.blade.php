<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GOLDEN HEALTH</title>
    <link rel="icon" href="{{ asset('customer/images/gh1.png') }}">
    <!-- Montserrat Font -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap"
        rel="stylesheet">
    <!--  -->
    <link rel="stylesheet" href="{{ asset('customer/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('customer/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('customer/css/style.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://unpkg.com/flickity@2.2.1/dist/flickity.css">
    <script src="https://unpkg.com/flickity@2.2.1/dist/flickity.pkgd.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-3RBY15TSCX"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-3RBY15TSCX');
    </script>
    <style>
        .card {
            position: relative;
            /* width: 300px; */
            width: auto;
            height: 200px;
            /* background: #000; */
            overflow: hidden;
            border: none;
            box-shadow: 0px 0px 9px 1px rgba(0, 0, 0, 0.08);
            /* box-shadow: 2px 2px 3px #666; */
        }

        .card .image {
            /* background-size: auto; */
            width: 100%;
            height: 100%;
            overflow: hidden;
        }

        .card .image img {
            width: 100%;
            /* height: 100%; */
            display: block;
            height: auto;
            transition: .5s;
        }

        .card:hover .image img {
            opacity: .2;
            /* transform: translateX(30%); */
            transform: translateY(-30%);
        }

        .card .title {
            position: absolute;
            left: 50%;
            transform: translateX(-50%);
            text-align: center;
            /* top: 10px; */
            top: 50px;
            opacity: 0;
            transition: all .3s;
        }

        .card:hover .title {
            top: 10px;
            opacity: 1;
        }

        .card .details {
            position: absolute;
            bottom: -130px;
            width: 100%;
            height: 70%;
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
            /* background: #FFC107; */
            transition: .5s;
            /* transform-origin: left; */
            /* transform: perspective(2000px) rotateY(-90deg); */
            /* transform: translateY(100px); */
        }

        .card:hover .details {
            transform: translateY(-130px);
        }

        .card .details .center {
            padding: 10px;
            text-align: center;
            height: 100%;
            /* background: #e9ecef; */
            background: linear-gradient(rgba(0, 0, 0, .5), rgba(0, 0, 0, .5));
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
        }

        .card .details .center h1 {
            margin: 0;
            padding: 0;
            color: #1c6a49;
            opacity: 1;
            font-weight: 700;

            line-height: 20px;
            font-size: 22px;
            text-transform: uppercase;
            transition: all .3s;
        }

        .card:hover .details .center h1 {
            opacity: 0;
            margin-bottom: 20px;
        }

        .card .details .center h1 span {
            font-size: 14px;
            color: #262626;
        }

        .card .details .center p {
            margin: 10px 0;
            padding: 0;
            font-size: 12px;
            letter-spacing: 1px;
            color: transparent;
            transition: color .4s;
        }

        .card:hover .details .center p {
            /* color: #000; */
            color: #fff;
        }

        .card-discipline {
        height: 200px;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        color: #fff;
        border-radius: 9px;
        margin-bottom: 20px;
        background-size: cover;
        background-position: center;
        padding: 10px;
        text-align: center;
        box-shadow: 0px 0px 9px 1px rgba(0, 0, 0, 0.08);
        }


        .card-discipline h3{
            font-size: 16px
        }

        .card-discipline1 {
        height: 200px;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        color: #fff;
        border-radius: 9px;
        margin-bottom: 20px;
        background-color: #ccc;
        }

        .card-discipline1 button {
        border: 1px solid #1C6A49;
        padding: 10px;
        transition: all .3s ease-in-out;
        }

        .card-discipline1 button:hover {
        background-color: #1C6A49;
        color: #fff;
        }


        @media screen and (max-width: 767px) {
            .slide-item .img-small{
                height: 120px;
                width: 120px;
                object-fit: cover;
                border-radius: 50%;
            }
        }

    </style>

</head>

<body>
    <!-- nav -->
    <section>
        <div class="container-fluid">
            {{-- {{ dd(Auth::guard('customer')->user()->is_active == true) }} --}}
            @auth('customer')
                @if (!is_null(Auth::guard('customer')->user()->email_verifed_at))
                    @include('customer_frontOffice.profile.partials.header')
                @else
                    @include('customer_frontOffice.layouts_customer.navbar')
                @endif

            @endauth
            @guest('customer')
                @include('customer_frontOffice.layouts_customer.navbar')
            @endguest
        </div>
    </section>
    <!-- carousel section -->
    <section>
        <div class="container-fluid">
            <!-- <div class="row" style="background: linear-gradient(rgba(0,0,0,.8), rgba(0,0,0,.4)) !important;"> -->
            <div class="row">
                <div class="col-lg-12 col-md-12 mb-5 p-0">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            @foreach ($slides as $key => $slide)
                                <li data-target="#carouselExampleIndicators" data-slide-to="{{ $key }}"
                                    class="{!! $loop->first ? 'active' : '' !!}"></li>
                            @endforeach
                            {{-- <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li> --}}
                        </ol>
                        <div class="carousel-inner">
                            @foreach ($slides as $slide)
                                {{-- <div class="carousel-item {!! $loop->first ? 'active' : '' !!}" style="background-image:linear-gradient(rgba(0,0,0,.4), rgba(0,0,0,.1)),url('{{$slide->image}}') center center cover no-repeat border-box content-box !important; height:95vh;"> --}}
                                <div class="carousel-item {!! $loop->first ? 'active' : '' !!}"
                                    style="background-image:linear-gradient(rgba(0,0,0,.8), rgba(0,0,0,.4)),url('{{ $slide->image }}') !important; height:95vh;background-size: cover;">
                                    <div class="carousel-item {!! $loop->first ? 'active' : '' !!}">
                                        {{-- <img class="d-block w-100" src="{{$slide->image}}"
                                        alt="{{$slide->titre}}"> --}}
                                    </div>
                            @endforeach
                            {{-- <div class="carousel-item active">
                                <img class="d-block w-100" src="{{ asset('customer/images/IMAGE.png') }}"
                                    alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset('customer/images/IMAGE.png') }}"
                                    alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset('customer/images/IMAGE.png') }}"
                                    alt="Third slide">
                            </div> --}}
                        </div>
                        <div class="carousel-caption">
                            <div class=" d-md-block">
                                <h2>
                                    {{-- <b>Coach bien-être</b> --}}
                                    <b>GoldenHealth, le bien être à vos portes</b>
                                    <!-- <b>Golden Health, votre bien-être, notre mission</b> -->
                                </h2>
                                <p class="carousel-caption-des mt-3">Les bienfaits du sport pour votre santé
                                    ne sont
                                    plus à démontrer ! Que vous
                                    souhaitiez combattre le stress, perdre du poids ou mieux vous nourrir,
                                    nos
                                    coachs spécialisés sont là pour vous.
                                </p>
                            </div>
                            <center>
                                <div class=" mt-md-5">
                                    <!-- <form action="/coachs" method="GET"> -->
                                    <form action="/search_coach" method="GET" id="search_form">
                                        <div class="input-group mx-auto">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="discipline-id"><i
                                                        class="fa fa-search"></i></label>
                                            </div>
                                            <select name="discipline" class="custom-select input-height"
                                                id="discipline-id" required>
                                                <option value="">Choisissez une discipline </option>
                                                @if (isset($all_disciplines) && $all_disciplines)
                                                    @foreach ($all_disciplines as $discipline)
                                                        <option value="{{ base64_encode($discipline->id) }}">
                                                            {{ $discipline->name }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <div class="vertical-divider"></div>
                                            <select name="city" id="" class="custom-select input-height">
                                                @php
                                                    $villes = \App\Models\ville::all();
                                                @endphp
                                                @foreach ($villes as $ville)
                                                    <option value="{{ $ville->name }}">{{ $ville->name }}</option>
                                                @endforeach
                                            </select>
                                            {{-- <input type="text" name="city" class="form-control input-height"
                                                id="input_search" placeholder="Ville, Commune">
                                            <input type="hidden" name="lat" id="lat_input">
                                            <input type="hidden" name="lon" id="lon_input">
                                            <div id="match-list"
                                                style="margin: 10px auto; width: 50%;position:absolute; top:70px;right:0">
                                            </div> --}}
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-secondary" type="button"
                                                    id="submit_search">RECHERCHER</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </center>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="py-5">
        <div class="container mb-5">
            <h2 class="text-center how-it-works mb-5">Comment ça marche ?</h2>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 mt-2">
                    <div class="mb-4">
                        <center><img src="{{ asset('customer/images/Groupe 87.png') }}" alt="" width="120"
                                height="120">
                        </center>
                    </div>
                    <h5 class="primary-text-color teasers text-center">Choisissez votre discipline </h5>
                    <div class="text-center teasers mt-3">
                        choisissez votre discipline et votre lieu d'habitation pour que nous puissions trouver le
                        coach qui vous correspond.
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 mt-2">
                    <div class="mb-4">
                        <center><img src="{{ asset('customer/images/Groupe 88.png') }}" alt="" width="120"
                                height="120">
                        </center>
                    </div>
                    <h5 class="primary-text-color teasers text-center">Choisissez votre Coach </h5>
                    <div class="text-center teasers mt-3">
                        Une liste de Coachs qualifiés vous est proposée (avis, prix, tout y est !)
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 mt-2">
                    <div class="mb-4">
                        <center><img src="{{ asset('customer/images/Groupe 92.png') }}" alt="" width="120"
                                height="120">
                        </center>
                    </div>
                    <h5 class="primary-text-color teasers text-center">Démarrer la séance</h5>
                    <div class="text-center teasers mt-3">
                        Démarrer la séance avec les Coachs qui vous plaisent!
                    </div>
                </div>


            </div>
        </div>
    </section>

    <!-- discipline -->

    <section>
        <div class="container py-2">
            <div class="d-flex justify-content-center">
                <!-- <h2 class="text-center how-it-works mb-5">Nos Disciplines</h2> -->
                <h2 class="text-center how-it-works mb-5">Nos Disciplines</h2>
                <!-- <a href="/customer/disciplines" class="pull-right mr-1">Voir plus de disciplines</a> -->
            </div>

            <div class="row ">
                @foreach ( $disciplines as $discipline)
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 ">
                    <a href="/search_coach?discipline={{ base64_encode($discipline->id) }}&city=">
                        <div class="card-discipline"  style="background-image: linear-gradient(45deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url({{  $discipline->image }});                        ">
                            <h3>{{  $discipline->name }}</h3>
                        </div>
                    </a>
                </div>
                @endforeach
    

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 ">
                    <div class=" card-discipline1" >
                       <a href="/customer/disciplines">
                           <button>Voir plus</button>
                       </a>
                    </div>
                </div>
            </div>
            <div class="row">

                {{-- @if (isset($disciplines) and !empty($disciplines))
                    @foreach ($disciplines as $discipline)
                        @if ($discipline)
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 ">
                            <div class="card-discipline" >
                                <h3>Coach Sportif</h3>
                            </div>
                        </div> --}}
                            {{-- <div class="col-lg-3 col-md-3 col-sm-12 mt-2">
                                <a href="/search_coach?discipline={{ base64_encode($discipline->id) }}&city="> --}}
                                    {{-- <div class="bg-image hoverDiscipline"
                                        style="background-image: linear-gradient(rgba(0, 0, 0, .5), rgba(0, 0, 0, .5)), url({{ $discipline->image }}) !important;">
                                        <p class="discipline-content-des">{{ $discipline->name }}</p>
                                        <div class="disciplineBlock">{{ $discipline->description }}</div>
                                    </div> --}}
                                    {{-- <div class="mb-3">
                                        <div class="card">
                                            <div class="image" style="background-position: center; background-image: url(https://i.imgur.com/FHZzZIy.jpg) !important; background-repeat: no-repeat; background-size: cover; filter: brightness(0.8);"> --}}
                                                {{-- style="background-position: center; background-image: linear-gradient(rgba(0, 0, 0, .5), rgba(0, 0, 0, .5)), url({{ $discipline->image }}) !important;"> --}}

                                                {{-- <img
                                                    src="{{ $discipline->image }}" /> --}}
                                            </div>
                                            {{-- <div class="title">
                                                <h3 class="text-uppercase font-weight-bold text-secondary">
                                                    {{ $discipline->name ?? '' }}
                                                </h3>
                                            </div>
                                            <div class="details" style="">
                                                <div class="center">
                                                    <h1 class="text-uppercase">
                                                        {{ $discipline->name ?? '' }}
                                                    </h1>
                                                    <p class="" style="font-size: 10px !important;">
                                                        {!! $discipline->description ?? '' !!}
                                                    </p>
                                                </div>
                                            </div> --}}
                                        {{-- </div>
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endforeach --}}
                    {{-- <div class="col-lg-3 col-md-3 col-sm-12 mt-2">
                        <a href="/customer/disciplines">
                            <div class="mb-3">
                                <div class="card" style="background-color:#fff !important">
                                    <div style="position:relative !important ;top:45% !important;">
                                        <h3 class="text-uppercase font-weight-bold text-center"
                                            style="border:2px black solid !important;padding:3px 5px;">
                                            Voir plus
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endif --}}

            </div>
        </div>
    </section>

    <!-- Coachs -->
    @guest('customer')
        <section class="container">
            <div class="container  mt-5">
                <div class="row coach-section">
                    <div>
                        <div class="row ">
                            <div class="col-lg-9 col-md-9 col-sm-12 ">
                                <h2 class="coach">Coach</h2>
                                <h6 class="mini-coach-content-des">Devenez Coach Golden Health </h6>
                                <p class="coach-content-des">
                                    Voulez-vous bénéficier de plus de visibilité et partager votre passion ? N'attendez
                                    plus, rejoignez-nous ! Golden Health vous donne la possibilité d’entrer en contact avec
                                    des personnes intéressées par votre discipline à travers sa plateforme.
                                </p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 d-flex align-items-center">
                                {{-- <a href="{{ route('become.coach') }}"
                                    class="start-link-coach">Commencer</a> --}}
                                <a href="{{ url('customer/register/coach') }}" class="start-link-coach">Commencer</a>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    @endguest

    <!-- Patner -->
    @guest('customer')
        <section class="container">
            <div class="container mt-5">
                <div class="row patner-section">
                    <div>
                        <div class="row ">
                            <div class="col-lg-9 col-md-9 col-sm-12 ">
                                <h2 class="coach">Partner</h2>
                                <h6 class="mini-coach-content-des">Devenez Partenaire Golden Health </h6>
                                <p class="coach-content-des">
                                    Voulez-vous faire connaitre vos produits et services ?
                                    Bénéficiez d'une large communauté de sportifs à travers la plateforme Golden Health
                                </p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 d-flex align-items-center">
                                <a href="{{ url('customer/register/partner') }}" class="start-link-patner">Commencer</a>
                                {{-- <a href="{{ route('become.partner') }}"
                                    class="start-link-patner">Commencer</a> --}}
                            </div>
                        </div>

                    </div>
                </div>
        </section>
    @endguest

    <!-- Advertissment -->

    <section class="py-4 container">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <h2><b>Gardez Votre Santé en poche</b> </h2>
                    <p>Télécharger l'application mobile</p>

                    <div class="">
                        <a href="#" target="_blank" class="btn btn-black mx-auto mt-2">
                            <span class="">
                                <img src="{{ asset('customer/images/google-play.png') }}" class="icon mr-2"
                                    width="24px" alt="">
                                Téléchargez sur <br>
                                Google Play
                            </span>
                        </a>&nbsp;&nbsp;
                        <a href="#" target="_blank" class="btn btn-black mx-auto mt-2">
                            <span class="">
                                <i class="fa fa-apple mr-2" aria-hidden="true"></i>
                                Téléchargez sur <br>
                                Apple Store
                            </span>
                        </a>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4  d-flex align-items-center justify-content-center">
                    <img src="{{ asset('customer/images/jshdshd.png') }}" alt="" width="210">
                </div>
            </div>
        </div>
    </section>
    <!-- <section>
        <div class="container">
            <div class="row d-flex justify-content-around">
                <div class="d-flex ml-5">
                    <div class="col-lg-6 col-md-6 col-sm-12 d-flex align-content-center flex-wrap">
                        <h2><b>Gardez Votre Santé en poche</b> </h2>
                        <p>Télécharger l'application mobile</p>

                        <div class="d-flex flex-sm-row flex-column align-items-center justify-content-between">
                            <a href="#" target="_blank" class="btn btn-black mx-auto mt-2">
                                <span class="d-flex align-items-center justify-content-starst">
                                    <img src="images/google-play.png" class="icon mr-2" width="24px" alt="">
                                    Téléchargez sur <br>
                                    Google Play
                                </span>
                            </a>&nbsp;&nbsp;
                            <a href="#" target="_blank" class="btn btn-black mx-auto mt-2">
                                <span class="d-flex align-items-center justify-content-starst">
                                    <i class="fa fa-apple mr-2" aria-hidden="true"></i>
                                    Téléchargez sur <br>
                                    Apple Store
                                </span>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <img src="images/jshdshd.png" alt="" width="230">
                    </div>
                </div>
            </div>
        </div>
    </section> -->


    <!-- vegan protéine -->
    @if (isset($priorityAd))
        <section class="py-5">
            @if ($priorityAd != null)
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="bg-image vegan-image"
                                style="background-image:  linear-gradient(rgba(0, 0, 0, .3), rgba(0, 0, 0, .3)), url({{ $priorityAd->cover_url }});">
                                <div class="container">
                                    <h2 class="coach mb-4">{{ $priorityAd->name ?? 'VEGAN PROTEIN' }}</h2>
                                    <div>
                                        <p class="vegan-des mb-5">
                                            {!! $priorityAd->description ??
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc erat augue,
                                            dapibus
                                            sed felis at, imperdiet pretium .' !!}
                                        </p>
                                        <a href="{{ $priorityAd->redirect_to ?? '#' }}" target="__blank"
                                            class="start-link-patner"><small>En savoir plus</small></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </section>
    @endif

    <!-- Testimonials -->
    <!-- <section id="testimonials" class="py-4 text-center mt-5" style="min-height: 300px;"> -->
    <section id="testimonials" class="py-4 mt-5" style="min-height: 300px;">
        <div class="container mb-5 pb-5">
            @if (isset($testimonies))
                @if ($testimonies->count() > 0)
                    <h2 class="title text-center text-primary mb-5">
                        <b>Avis clients</b>
                    </h2>

                    <div id="testimony-slider" class="row">
                        <div class="slides-container border shadow p-5">

                            {{-- @if (isset($advices) and !empty($advices))
                                @foreach ($advices as $advice)
                                    <div class="slide-item">
                                        <img src="{{ asset($advice->image) }}" class="img-fluid img-circle img-small">
                                        <h5 class="mt-2 text-primary font-weight-bold">
                                            Cliente Satisfaite
                                        </h5>
                                        <p class="quote">{{ $advice->message }}</p>
                                        <div class="row justify-content-center my-2 stars_rating">
                                            <img src="{{ asset('customer/images/starss.png') }}" class="icon" alt="">
                                            <img src="{{ asset('customer/images/starss.png') }}" class="icon" alt="">
                                            <img src="{{ asset('customer/images/starss.png') }}" class="icon" alt="">
                                            <img src="{{ asset('customer/images/starss.png') }}" class="icon" alt="">
                                            <img src="{{ asset('customer/images/starss.png') }}" class="icon" alt="">
                                        </div>
                                    </div>
                                @endforeach
                            @endif --}}
                            @foreach ($testimonies as $testimony)
                                <div class="slide-item">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <img src="{{ $testimony->client_photo_url ?? asset('customer/images/401-ted3467-jj-a_2.png') }}"
                                                alt="{{ $testimony->client_name }}" class="img-fluid img-small">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                            <h5 class="mt-2 text-primary font-weight-bold">
                                                {{ $testimony->client_name }}
                                            </h5>
                                            <p class="quote">
                                                {!! $testimony->client_feedback !!}
                                            </p>
                                            <div class="row my-2 stars_rating">
                                                {!! $testimony->stars() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="slide-item">
                                    <img src="{{ $testimony->client_photo_url ?? asset('customer/images/401-ted3467-jj-a_2.png') }}" alt="{{$testimony->client_name}}" class="img-fluid img-circle img-small">
                                    <h5 class="mt-2 text-primary font-weight-bold">
                                        {{$testimony->client_name}}
                                    </h5>
                                    <p class="quote">
                                        {!! $testimony->client_feedback !!}
                                    </p>
                                    <div class="row justify-content-center my-2 stars_rating">
                                        {!! $testimony->stars() !!}
                                    </div>
                                </div> --}}
                            @endforeach
                            {{-- <div class="slide-item">
                                <img src="{{ asset('customer/images/401-ted3467-jj-a_2.png') }}" alt="Aristide Kone"
                                    class="img-fluid img-circle img-small">
                                <h5 class="mt-2 text-primary font-weight-bold">
                                    Amane Hosanna
                                </h5>
                                <p class="quote">"I appreciate this service" </p>
                                <div class="row justify-content-center my-2 stars_rating">
                                    <img src="{{ asset('customer/images/stars.png') }}" class="icon" alt="">
                                    <img src="{{ asset('customer/images/stars.png') }}" class="icon" alt="">
                                    <img src="{{ asset('customer/images/stars.png') }}" class="icon" alt="">
                                    <img src="{{ asset('customer/images/stars.png') }}" class="icon" alt="">
                                    <img src="{{ asset('customer/images/stars.png') }}" class="icon" alt="">
                                </div>
                            </div> --}}

                        </div>
                        <div class="slide-indicators">
                            <div class="circle-indicator active" onclick="currentSlide(1)"></div>
                            <div class="circle-indicator" onclick="currentSlide(2)"></div>
                            <div class="circle-indicator" onclick="currentSlide(3)"></div>
                        </div>

                        <a href="#" class="left-button slide-control shadow shadow-paler"
                            onclick="event.preventDefault();plusSlides(-1)">
                            <img src="{{ asset('customer/images/left.png') }}" alt="" width=""
                                class="img-fluid">
                        </a>
                        <a href="#" class="right-button slide-control shadow shadow-paler"
                            onclick="event.preventDefault();plusSlides(1)">
                            <img src="{{ asset('customer/images/right.png') }}" alt="" width=""
                                class="img-fluid">
                        </a>
    </section>
    @endif
    @endif
    </div>
    </div>

    <!-- footer -->
    @include('customer_frontOffice.layouts_customer.footer')
    <!-- / Footer -->

    <script src="{{ asset('customer/js/jquery.min.js') }} "></script>
    <script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('customer/js/popper.min.js') }} "></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- GetButton.io widget -->
    <script type="text/javascript">
        (function() {
            var options = {
                whatsapp: "+2250777720507", // WhatsApp number
                call_to_action: "Nous contactez", // Call to action
                position: "right", // Position may be 'right' or 'left'
            };
            var proto = document.location.protocol,
                host = "getbutton.io",
                url = proto + "//static." + host;
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = url + '/widget-send-button/js/init.js';
            s.onload = function() {
                WhWidgetSendButton.init(host, proto, options);
            };
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        })();
    </script>
    <!-- /GetButton.io widget -->
    <script>
        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("slide-item");
            var dots = document.getElementsByClassName("circle-indicator");
            if (n > slides.length) {
                slideIndex = 1
            }
            if (n < 1) {
                slideIndex = slides.length
            }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " active";

            setTimeout(() => {
                plusSlides(1);
            }, 20000);
        }

        function getLocation(str) {
            value = str.match(/(\d+)(\.\d+)?/g); // trim
            return value;
        }

        //search = document.getElementById("input_search");
        const matchList = document.getElementById("match-list")

        $("#input_search").autocomplete({
            source: function(query, callback) {
                $.getJSON(
                    `https://nominatim.openstreetmap.org/search?q=${query.term}&countrycodes=CI&format=json&addressdetails=1&limit=100`,
                    function(data) {
                        var places = data;
                        places = places.map(place => {
                            console.log("==== place ===");
                            console.log(place);
                            return {
                                title: place.display_name.replace(/<br\/>/g, ", "),
                                value: place.display_name.replace(/<br\/>/g, ", "),
                                lat: place.lat,
                                lon: place.lon,
                                id: place.osm_id
                            };
                        });
                        return callback(places);
                    });
            },
            minLength: 2,
            select: function(event, ui) {
                console.log(ui);
                $('#lat_input').val(ui.item.lat);
                $('#lon_input').val(ui.item.lon);
                console.log("Selected: " + ui.item.title);
            },
            open: function() {
                $('.ui-autocomplete').css('width', '305px'); // HERE
            }
        });

        //Search Address to api
        /* const searchStates = async searchText =>{
                     const response = await fetch(`https://nominatim.openstreetmap.org/search?q=${search.value}&countrycodes=CI&format=json&addressdetails=1&limit=100`);
                     const states = await response.json();
                     // console.log(states);
                     let matches = states.filter(state =>{
                         const regex= new RegExp(`^${searchText}`, 'gi');
                         // console.log(state.display_name);
                         return state.display_name.match(regex);
                     });
                     if(searchText.length === 0){
                         matches = [];
                         matchList.innerHTML = "";
                     }
                     console.log(matches);
                     outputHtml(matches);
                 };
                 const outputHtml = matches =>{
                     if(matches.length > 0){
                         const html = matches.map(
                         (match, i)=>`
     <div class="d-flex justify-content-center card card-body w-150"
         id='${match.place_id}' style='margin-top :-20px; cursor: pointer;' onmouseover="this.style.backgroundColor='rgba(250,240,230,0.95)';" onmouseout="this.style.backgroundColor='white';"  onclick="startSearch(${match.lat}, ${match.lon})"; >
             <h6 class="text-primary">
             <span>${match.display_name}</span>
             ${(match.address.country_code).toUpperCase()}  <br/>
             <small>Lat : ${match.lat} /Lon :${match.lon} <small/>
         </h6>
     </div>`
                         ).join('');
                         console.log(html)
                         matchList.innerHTML = html;
                     }
                 }
                 search.addEventListener('input', ()=>searchStates(search.value));
                 function startSearch(lat, lon){
                     console.log(lat)
                     console.log(lon)
                     document.getElementById('lat_input').value = lat
                     document.getElementById('lon_input').value = lon
                     document.getElementById("search_form").submit()
                 }*/
    </script>
</body>

</html>
