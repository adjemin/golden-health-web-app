@extends("customer_frontOffice.layouts_customer.master_customer")

@section('content')
<section>
    <header class="become-partner-header">
        <div class="become-coach-header-des">
            <h3 class="font-weight-bold">Devenir Partner </h3>
            <p>
                Voulez-vous collaborer avec Golden Health sur un projet sportif que vous avez pour votre entreprise ou organisation ? / Voulez-vous faire connaitre vos produits et services ? N’attendez pas !!! inscrivez-vous et dites-nous tout. Bénéficier vous aussi de cette large communauté de sportifs.
            </p>
        </div>
    </header>
</section>


<section class="py-5">
    <div class="container">
        <div class="row ">
            <h1 class="font-weight-bold primary-text-color">Partner </h1>
        </div>

        <!--<p>la description des avantages disponibles pour un coach qui souhaite collaborer avec GH. <br> <br> Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte
            standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté
            à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans
            des applications de mise en page de texte, comme Aldus PageMaker.</p> -->
    </div>
</section>

<div class=" py-5">
    <div class="container">
        <div class="row">
          <div class="cold-md-12">
            @if(session()->has('message'))
              <div class="alert alert-success">
                  {{ session()->get('message') }}
              </div>
            @endif
          </div>
        </div>
        <div class="row">
            <h4 class="font-weight-bold text-primary">M'inscrire en tant que Partner Golden Health </h4>
        </div>
        <div class="row py-5 ">
            <div class="col-lg-10 col-md-10 col-sm-12">
                <form id="my-form-partner" method="POST" action="/register/partner">
                  {{ csrf_field() }}
                  <div class="row mb-4 ml-1">
                      <div class="form-check mr-4">
                          <input class="form-check-input" type="radio" name="gender" id="gender" value="MR" checked required>
                          <label class="form-check-label" for="gridRadios1">
                              Mr
                          </label>
                      </div>

                      <div class="form-check mr-4">
                          <input class="form-check-input" type="radio" name="gender" id="gender" value="MME" required>
                          <label class="form-check-label" for="gridRadios2">
                              Mme
                          </label>
                      </div>

                      <div class="form-check">
                          <input class="form-check-input" type="radio" name="gender" id="gender" value="MLLE" required>
                          <label class="form-check-label" for="gridRadios3">
                              Mlle
                          </label>
                      </div>
                      <div class="form-check">
                          <input class="form-check-input" type="radio" name="gender" id="gender" value="ENTREPRISE" required>
                          <label class="form-check-label" for="gridRadios4">
                            Entreprise
                          </label>
                      </div>
                  </div>
                    <div class="form-group">
                        <input type="text" name="name" class="form-control input-style placeholder-style" id="name" placeholder="Nom Complet " required>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="email" name="email" class="form-control input-style placeholder-style" id="email" placeholder="Adresse email" required>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="tel" name="telephone" class="form-control input-style placeholder-style" id="telephone " placeholder="telephone " required>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <select name="service_type" id="service_type" class="custom-select input-style placeholder-style" required>
                            <option selected>Type de service</option>
                            <option value="1">Publicité/Marketing</option>
                            <option value="2">Vente de produits</option>
                            <option value="3">Autres</option>
                        </select>
                    </div>
                    <div id="div_other_service" style="display:none">
                        <div class="input-group mb-3">
                            <input type="text" name="service_type_other" class="form-control input-style placeholder-style" id="service_type_other"  placeholder="Autre type de service">
                        </div>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text placeholder-style " id="basic-addon3">https://facebook.com/</span>
                        </div>
                        <input type="text" name="facebook_url" class="form-control input-style placeholder-style" id="facebook_url" aria-describedby="basic-addon3" placeholder="Page Facebook">
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend ">
                            <span class="input-group-text placeholder-style" id="basic-addon3">https://www.instagram.com/</span>
                        </div>
                        <input type="text" name="instagram_url" class="form-control input-style placeholder-style" id="instagram_url" aria-describedby="basic-addon3" placeholder="Compte Instagram">
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend ">
                            <span class="input-group-text placeholder-style" id="basic-addon3">https://www.youtube.com/</span>
                        </div>
                        <input type="text" name="youtube_url" class="form-control input-style placeholder-style" id="instagram_url" aria-describedby="basic-addon3" placeholder="Compte Youtube">
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text placeholder-style" id="basic-addon3">Site
                                web</span>
                        </div>
                        <input type="text" name="site_web" class="form-control input-style placeholder-style" id="site_web" aria-describedby="basic-addon3" placeholder="Site web">
                    </div>

                    <label for="">Comment avez-vous connu Golden Health ? *</label>
                    <div class="input-group mb-3">
                        <select name="discovery_source" class="custom-select input-style placeholder-style" id="discovery_source" required>
                            <option value=""></option>
                            <option value="search_engine"> Moteur de recherche (Bing, Google, Yahoo...)</option>
                            <option value="website"> Site Internet autre qu'un moteur de recherche (Blog, presse...)</option>
                            <option value="written-press"> Presse écrite</option>
                            <option value="social-media"> Réseaux sociaux (Facebook, Instagram...)</option>
                            <option value="word-of-mouth"> Bouche à oreilles (Amis, famille, collègue...)</option>
                            <option value="corporate-lesson"> Cours en entreprise</option>
                            <option value="subway-display"> Affichage métro</option>
                            <option value="coach"> Par un coach</option>
                            <option value="other"> Autre (Salon, évènement...)</option>
                        </select>

                    </div>

                    <div class="form-group">
                        <textarea name="description" class="form-control  placeholder-style" placeholder="Dites nous quelques choses " id="" cols="30" rows="10"></textarea>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="is_newsletter" name="is_newsletter" value="1">
                                <label class="form-check-label" for="is_newsletter">S'inscrire à la
                                    newsletter</label>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-check">
                                <input class="form-check-input" name="is_cgu" type="checkbox" id="is_cgu" value="1" required>
                                <label class="form-check-label" for="is_cgu">J'accepte les
                                    Conditions
                                    générales d'utilisation</label>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4">
                        <button type="submit" name="submit" class="login-button col-lg-12 text-center">S'inscrire</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
$(document).ready(function() {

    $('#service_type').on('change', function(){
        if ($(this).val() == 3){
            $('#div_other_service').show();
        } else {
            $('#div_other_service').hide();
        }
    });
});
</script>
@endpush
