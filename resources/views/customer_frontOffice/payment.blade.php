<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Payer Via AdjeminPay</title>
</head>

<body>
    <script src="https://www.cdn.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript">
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

    <div id="result">
        <h1 id="result-title"></h1>
        <p id="result-message"></p>
        <p id="result-status"></p>
        <a href="/customer/dashboard">Retour</a>
    </div>


    <input type="hidden" id="amount" value="{{$payment['amount']}}">
    <input type="hidden" id="currency" value="CFA">
    <input type="hidden" id="designation" value="{{$payment['transaction_designation']}}">
    <input type="hidden" id="transaction_id" value="{{$payment['transaction_id']}}">
    <input type="hidden" id="custom_field" value="{{$payment['booking_id']}}">

    <script>
        var AdjeminPay = AdjeminPay();

        AdjeminPay.on('init', function (e) {
            console.log(e);
        });

        AdjeminPay.init({
            apikey: "eyJpdiI6ImhUQUZkMkV0YjBFZXg2OW9zT",
            application_id: "482cdf",
            notify_url: "https://4e4b545fa4f2.ngrok.io/customer/payment/notify"
            // TO USE:
            // notify_url: "https://goldenhealth.adjemincloud.com/payments/notify"
        });

        AdjeminPay.on('error', function (e) {
          console.log(e);
          $("#result-title").html(e.title);
          $("#result-message").html(e.message);
          $("#result-status").html(e.status);
          setTimeout(()=> {
              window.location.reload();
              location.assign("/final/checkout");
              //
          }, 3000);
        });

        // Lancer la procédure de paiement au click
        AdjeminPay.preparePayment({
            amount: parseInt($('#amount').val()),
            transaction_id: $('#transaction_id').val(),
            currency: $('#currency').val(),
            designation: $('#designation').val(),
            custom: $('#custom_field').val()
        });
        AdjeminPay.renderPaymentView();

        // Payment terminé
        AdjeminPay.on('paymentTerminated', function (e) {
            console.log('<<<<<<< Terminated !');
            console.log(e);
            $("#result-title").html(e.title);
            $("#result-message").html(e.message);
            $("#result-status").html(e.status);
        });
          // Payment réussi
        AdjeminPay.on('paymentSuccessful', function (e) {
            console.log('<<<<<<< Successful !');
            console.log('>>>>>>> Paiement réussi !');

            $("#result-title").html(e.title);
            $("#result-message").html(e.message);
            $("#result-status").html(e.status);
            setTimeout(()=> {
                window.location.reload();
                location.assign("/customer/dashboard");
                //
            }, 3000);
        });
        // Payment échoué
        AdjeminPay.on('paymentFailed', function (e) {
            console.log('<<<<<<< Echec !');
            console.log('>>>>>>> Paiement echoué !');

            $("#result-title").html(e.title);
            $("#result-message").html(e.message);
            $("#result-status").html(e.status);
            setTimeout(()=> {
                window.location.reload();
                location.assign("/final/checkout");
                //
            }, 3000);
        });

        // Payment annulé
        AdjeminPay.on('paymentCancelled', function (e) {
            console.log('<<<<<<< Echec !');
            console.log('>>>>>>> Paiement annulé !');

            $("#result-title").html(e.title);
            $("#result-message").html(e.message);
            $("#result-status").html(e.status);
            setTimeout(()=> {
                window.location.reload();
                location.assign("/final/checkout");
                //
            }, 3000);
        });
    </script>

</body>

</html>
