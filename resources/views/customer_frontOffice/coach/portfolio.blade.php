@extends("customer_frontOffice.layouts_customer.master_customer")

@section('content')
<section>
    <header class="profil-header">
        <div class="dashboard-header-des">
            <h3 class="font-weight-bold text-center">Portfolios </h3>
            <p class="text-center">
              Ajouter ici votre portfolio. Vous pouvez ajouter un maximum de 5 medias
            </p>
        </div>
    </header>
</section>
<section class="mb-5">
  @include('coreui-templates::common.errors')
  @include('flash::message')
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="mx-2 mt-4">
              @if(count($portfolios) <= 5)
                <button type="button" class="float-right btn btn-primary" data-toggle="modal" data-target="#exampleModal" name="button">Ajouter portfolio</button>
              @endif
              <div class="mt-4">
                <div class="row">
                @if ($portfolios != null && count($portfolios)>0)
                  @foreach ($portfolios as $portfolio)
                    <div class="col-lg-4 col-md-4 col-sm-6 mb-4">
                      <div class="py-3 px-3">
                        <div class="">
                            @if($portfolio->media_type == "youtube")
                              <div class="embed-responsive embed-responsive-21by9 embed-responsive-16by9 embed-responsive-4by3 embed-responsive-1by1">
                              {{-- <iframe class="embed-responsive-item" src="{{$portfolio->video_url }}" frameborder="0" allowfullscreen></iframe> --}}
                              <iframe width="560" height="315" src="{{$portfolio->video_url }}" frameborder="0"  allowfullscreen></iframe>
                              </div>
                            @elseif($portfolio->media_type == "facebook")
                            <iframe src="{{$portfolio->video_url }}" width="560" height="314" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allowFullScreen="true"></iframe>
                            @else
                              <img class="img-fluid" src="{{ $portfolio->images_url }}" alt="{{ $portfolio->title }}" type="">
                            @endif
                            <h5 class="font-weight-bold">{{ $portfolio->title }}</h5>
                        </div>
                        <div class="d-flex justify-content-center">
                          <button class="btn btn-sm btn-danger deleteProduct" onclick="return confirm('Voulez-vous vraiment supprimer ce portfolio ?')" data-id="{{$portfolio->id}}" >Supprimer</button>
                        </div>
                      </div>
                    </div>
                  @endforeach
                @else
                  <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
                      Aucun Portfolio Disponible !
                  </div>
                @endif
                </div>
              </div>
          </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
          <form action="/customer/coach/portfolio/save" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ajouter un portfolio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                @csrf
                <div class="form-group mb-4">
                  <label for="">Titre</label>
                  <input type="text" class="form-control" name="title" id="title" required/>
                </div>
                <div class="form-group mb-4">
                  <label for="">Type de fichier</label>
                  <div class="row d-flex justify-content-around">
                    <span>
                      <input type="radio" name="file_type" value="youtube" id="youtube_input">&nbsp;&nbsp; Video Youtube
                    </span>
                    <span>
                      <input type="radio" name="file_type" value="facebook" id="facebook_input">&nbsp;&nbsp; Video Facebook
                    </span>
                    <span>
                      <input type="radio" name="file_type" value="image" id="image_input">&nbsp;&nbsp; Image
                    </span>
                  </div>
                </div>
                <div id="youtube_div" style="display:none;">
                  <input id="youtube_input" name="youtube_input" type="text" class="form-control my-2">
                  <small>Exemple: pour le lien suivant https://www.youtube.com/watch?v=mmq5zZfmIws vous devrez enregistrer l'id de la video qui est <strong>"mmq5zZfmIws"</strong></small>
                </div>
                <div id="facebook_div" style="display:none;">
                  <input id="facebook_input" name="facebook_input" type="text" class="form-control my-2">
                  <small>Sous votre video Facebook,
                    <ol>
                      <li>cliquez sur le boutton <b>partager</b>,</li>
                      <li>puis <b>Integrer a un site</b>.</li>
                      <li>Enfin cliquer sur le bootton bleu <b>copier le code </b> en coller ici.</li>
                    </ol>
                </div>
                <div id="image_div" style="display:none;">
                  <div class="file-loading mt-4">
                    <input id="input-b9" name="input-b9" type="file">
                  </div>
                  <div id="kartik-file-errors"></div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                <input type="submit" class="btn btn-primary" style="display:none" id="save_button" value="Enregistrer">
                <!-- <button type="button"></button> -->
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection

@push('js')
<script>
$(document).ready(function() {
    $("#input-b9").fileinput({
        language : 'fr',
        showPreview: true,
        showUpload: true,
        dropZoneEnabled:true,
        maxFileSize:2000,
        maxFilesNum: 10,
        elErrorContainer: '#kartik-file-errors',
        allowedFileExtensions: ["jpg", "png", 'jpeg'],
        uploadUrl: '/customer/coach/portfolio/save',
        uploadExtraData: function() {
          return {
            _token: $("input[name='_token']").val(),
            title: $("input[name='title']").val(),
            media_type: $("input[name='file_type']").val()
          };
        },
        slugCallback: function (filename) {
          return filename.replace('(', '_').replace(']', '_');
        }
    });
    $("#input-b9").on('filebatchuploadcomplete', function(event, preview, config, tags, extraData) {
        setTimeout(function() {
          window.location.href = '/customer/coach/portfolio';
        }, 2000);
    });


    $(".deleteProduct").on('click', function(){
        var id = $(this).data("id");
        var token = $("input[name='_token']").val();
        $.ajax(
        {
            url: "/customer/coach/portfolio/delete/"+id,
            type: 'post',
            dataType: "JSON",
            data: {
                "id": id,
                "_token": token,
            },
            success: function ()
            {
              setTimeout(function() {
                window.location.href = '/customer/coach/portfolio';
              }, 500);
                console.log("it Work");
            }
        });

        console.log("It failed");
    });

    $("input[name='file_type']").on('change', function(){
      if($(this).val() == "youtube"){
        $("#youtube_div").show();
        $("#facebook_div").hide();
        $("#save_button").show();
        $("#image_div").hide();
      }
      if($(this).val() == "facebook"){
        $("#youtube_div").hide();
        $("#facebook_div").show();
        $("#save_button").show();
        $("#image_div").hide();
      }
      if($(this).val() == 'image'){
        $("#youtube_div").hide();
        $("#facebook_div").hide();
        $("#save_button").hide();
        $("#image_div").show();
      }
    });
});
</script>
@endpush
