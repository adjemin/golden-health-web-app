@extends("customer_frontOffice.layouts_customer.master_customer")

@section('content')
<section>
    <header class="profil-header">
        <div class="dashboard-header-des">
            <h3 class="font-weight-bold text-center">Diplômes </h3>
            <p class="text-center">
              Ajouter ici vos diplômes et votre carte professionnelle d'éducateur sportif
            </p>
        </div>
    </header>
</section>
<section class="mb-5">
  @include('coreui-templates::common.errors')
  @include('flash::message')
    <div class="container">
      <div class="row">
        <div class="col-md-10">
          <div class="mx-2 mt-4">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" name="button">Ajouter un fichier</button>
              <div class="mt-4">
                <table class="table table-bordered" style="">
                  <thead>
                    <tr>
                      <th>Titre</th>
                      <th>Document</th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @if($diplomes)
                      @foreach($diplomes as $diplome)
                        <tr>
                          <td>{{$diplome->diplome_name}}</td>
                          <td class="2">{{$diplome->name}}</td>
                          <td><a class="btn btn-success btn-sm" href="{{url($diplome->image_url)}}" download="">Télécharger</a></td>
                          <td><button class="btn btn-sm btn-danger deleteProduct" onclick="return confirm('Are you sure?')" data-id="{{$diplome->id}}" >Supprimer</button></td>
                        </tr>
                      @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
          </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ajouter vos fichiers (.pdf .jpg ou .png)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                @csrf
                <div class="form-group mb-4">
                  <label for="">Nom du diplome <span style="color:red">*</span></label>
                  <input type="text" class="form-control" name="diplome_name" id="diplome_name" required/>
                </div>
                <div class="file-loading mt-4">
                  <input id="input-b9" name="input-b9[]" multiple type="file">
                </div>
                <div id="kartik-file-errors"></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                {{--   <button type="button" class="btn btn-primary">Save changes</button> --}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection
@push('js')
<script>
$(document).ready(function() {
    $("#input-b9").fileinput({
        language: "fr",
        showPreview: true,
        showUpload: true,
        dropZoneEnabled:true,
        maxFileSize:2000,
        maxFilesNum: 10,
        elErrorContainer: '#kartik-file-errors',
        allowedFileExtensions: ["jpg", "png", "pdf"],
        uploadUrl: '/customer/coach/diplome/save',
        uploadExtraData: function() {
            return {
                _token: $("input[name='_token']").val(),
                diplome_name: $("input[name='diplome_name']").val()
            };
        },
        slugCallback: function (filename) {
          return filename.replace('(', '_').replace(']', '_');
        }
    });
    $("#input-b9").on('filebatchuploadcomplete', function(event, preview, config, tags, extraData) {
        setTimeout(function() {
          window.location.href = '/customer/coach/diplome';
        }, 2000);
    });


    $(".deleteProduct").on('click', function(){
        var id = $(this).data("id");
        var token = $("input[name='_token']").val();
        $.ajax(
        {
            url: "/customer/coach/diplome/delete/"+id,
            type: 'post',
            dataType: "JSON",
            data: {
                "id": id,
                "_token": token,
            },
            success: function ()
            {
              setTimeout(function() {
                window.location.href = '/customer/coach/diplome';
              }, 500);
                console.log("it Work");
            }
        });

        console.log("It failed");
    });
});
</script>
@endpush
