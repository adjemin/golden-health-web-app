@extends("customer_frontOffice.layouts_customer.master_customer")
@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <style>
        .bootstrap-select .btn-light {
            background-color: transparent ! important;
            border-color: transparent ! important;
        }

        .bootstrap-select .dropdown-toggle .filter-option-inner-inner {
            font-size: small ! important;
        }

        .bootstrap-select .dropdown-menu {
            border-radius: 0px ! important;
            left: -13px ! important;
        }

        .bootstrap-select.show-tick .dropdown-menu li a span.text {
            font-size: small ! important;
        }

    </style>
@endsection
@section('content')

    <header class="become-coach-header">
        <div class="become-coach-header-des">
            <h3 class="font-weight-bold">Devenir Coach</h3>
            <!--p>
                        Voulez-vous bénéficier de plus de visibilité etet partager votre passion ?
                        N'attendez plus, rejoignez-nous ! Golden Health vous donne la possibilité de
                        rentrer en contact avec des personnes intéressées, par votre discipline à
                        travers sa plateforme.
                    </p-->
            <p>
                Être coach Golden Health c’est:
                Être professionnel.
                Avoir de l’éthique.
                Avoir de l’empathie.
                Participer au respect et à la valorisation de la profession de coach sportif et en récolter les
                fruits.
                Être coach Golden Health c’est aussi toucher une plus large audience à travers notre plateforme
                et nos campagnes de promotions et de sensibilisations à la pratique du sport.
                Partagez-vous ces valeurs ?
                Rejoignez-nous !!!
            </p>
        </div>
    </header>

    <section>
        <div class=" py-5">
            <div class="container">
                <div class="row ">
                    <h4 class="font-weight-bold text-primary">M'inscrire en tant que Coach Golden Health </h4>
                </div>
                <div class="row py-5">
                    <div class="col-lg-10 col-md-10 col-sm-12">
                        <form id="my-form" method="POST">
                            @guest('customer')
                                <div class="row mb-4 ml-1">
                                    <div class="form-check mr-4">
                                        <input class="form-check-input" type="radio" name="gender" id="gender" value="mr"
                                            checked required>
                                        <label class="form-check-label" for="gridRadios1">
                                            Mr
                                        </label>
                                    </div>

                                    <div class="form-check mr-4">
                                        <input class="form-check-input" type="radio" name="gender" id="gender" value="mme"
                                            required>
                                        <label class="form-check-label" for="gridRadios2">
                                            Mme
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="gender" id="gender" value="mlle"
                                            required>
                                        <label class="form-check-label" for="gridRadios3">
                                            Mlle
                                        </label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <input type="text" name="first_name" class="form-control input-style placeholder-style"
                                            id="first_name" placeholder="Nom" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="text" name="last_name" class="form-control input-style placeholder-style"
                                            id="last_name" placeholder="Prénom" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="email" name="email" class="form-control input-style placeholder-style"
                                            id="email" placeholder="email" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="date" name="birthday" class="form-control input-style placeholder-style"
                                            id="birthday" placeholder="Date de naissance" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="text" name="commune" class="form-control input-style placeholder-style"
                                            id="commune" placeholder="commune" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="text" name="quartier" class="form-control input-style placeholder-style"
                                            id="quartier" placeholder="quartier" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="tel" name="phone_number" class="form-control input-style placeholder-style"
                                            id="phone_number " placeholder="telephone" required>
                                    </div><br>
                                    <div class="form-group col-md-6">
                                        <input type="password" name="password"
                                            class="form-control input-style placeholder-style" id="password "
                                            placeholder="Mot de passe *" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="password" name="password_confirmation"
                                            class="form-control input-style placeholder-style" id="password-confirm"
                                            placeholder="Confirmer Mot de passe*" required>
                                    </div>
                                </div>
                            @endguest
                            @auth('customer')
                                <input type="hidden" id="customer_id" name="customer_id"
                                    value="{{ Auth::guard('customer')->user()->id }}" required />
                            @endauth

                            <div class="form-group">
                                <input type="text" name="birthday" class="form-control input-style placeholder-style"
                                    id="birthday" placeholder="Date de naissance" required>
                            </div>

                            <!--div class="input-group mb-3">
                                        @if (isset($disciplines) || is_null($disciplines))
                                            <select name="discipline_id[]"  class="selectpicker custom-select input-style placeholder-style" id="discipline_id" multiple title="Choisissez votre discipline...">
                                                @foreach ($disciplines as $discipline)
                                                    <option value="{{ $discipline->id }}">{{ $discipline->name }}</option>
                                                @endforeach
                                        </select>
                                        @endif
                                    </div-->

                            <!--div class="input-group ">
                                        <div class="custom-file ">
                                            <input type="file" name="file" class="custom-file-input" id="inputGroupFile04">
                                            <label class="custom-file-label" for="inputGroupFile04">Ajouter une pièce
                                                d'identité , Diplôme ou certificats
                                            </label>
                                        </div>
                                    </div>

                                    <p class="mt-3"><span class="secondary-text-color">Pièces requises :</span> Pièce
                                        d'identité , Diplôme ou certificats ( Facultatif )</p>
                                        -->

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text placeholder-style "
                                        id="basic-addon3">https://facebook.com/</span>
                                </div>
                                <input type="text" name="facebook_url" class="form-control input-style placeholder-style"
                                    id="facebook_url" aria-describedby="basic-addon3" placeholder="Page Facebook">
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend ">
                                    <span class="input-group-text placeholder-style"
                                        id="basic-addon3">https://www.instagram.com/</span>
                                </div>
                                <input type="text" name="instagram_url" class="form-control input-style placeholder-style"
                                    id="instagram_url" aria-describedby="basic-addon3" placeholder="Compte Instagram">
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend ">
                                    <span class="input-group-text placeholder-style"
                                        id="basic-addon3">https://www.youtube.com/</span>
                                </div>
                                <input type="text" name="youtube_url" class="form-control input-style placeholder-style"
                                    id="instagram_url" aria-describedby="basic-addon3" placeholder="Compte Youtube">
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text placeholder-style" id="basic-addon3">Site
                                        web</span>
                                </div>
                                <input type="text" name="site_web" class="form-control input-style placeholder-style"
                                    id="site_web" aria-describedby="basic-addon3" placeholder="Site web">
                            </div>

                            <label for="">Comment avez-vous connu Golden Health ? *</label>
                            <div class="input-group mb-3">
                                <select name="discovery_source" class="custom-select input-style placeholder-style"
                                    id="discovery_source" required>
                                    <option value=""></option>
                                    <option value="search_engine"> Moteur de recherche (Bing, Google, Yahoo...)</option>
                                    <option value="website"> Site Internet autre qu'un moteur de recherche (Blog, presse...)
                                    </option>
                                    <option value="written-press"> Presse écrite</option>
                                    <option value="social-media"> Réseaux sociaux (Facebook, Instagram...)</option>
                                    <option value="word-of-mouth"> Bouche à oreilles (Amis, famille, collègue...)</option>
                                    <option value="corporate-lesson"> Cours en entreprise</option>
                                    <option value="subway-display"> Affichage métro</option>
                                    <option value="coach"> Par un coach</option>
                                    <option value="other"> Autre (Salon, évènement...)</option>
                                </select>

                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="is_newsletter"
                                            name="is_newsletter" value="1">
                                        <label class="form-check-label" for="is_newsletter">S'inscrire à la
                                            newsletter</label>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-check">
                                        <input class="form-check-input" name="is_cgu" type="checkbox" id="is_cgu" value="1"
                                            required>
                                        <label class="form-check-label" for="is_cgu">J'accepte les
                                            Conditions
                                            générales d'utilisation</label>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-4">
                                <!--a href="{{ route('dashboard') }}" type="submit" class="login-button col-lg-12 text-center">S'inscrire</a-->
                                <input type="submit" class="login-button col-lg-12 text-center" value="S'inscrire">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('js')
    <script type="text/javascript">
        /*
         * Block / Unblock UI
         */
        function blockUI() {
            $.blockUI({
                message: "<img src='{{ asset('assets/img/loading.gif') }}' alt='Loading ...' />",
                overlayCSS: {
                    backgroundColor: '#1B2024',
                    opacity: 0.55,
                    cursor: 'wait'
                },
                css: {
                    border: '4px #999 solid',
                    padding: 15,
                    backgroundColor: '#fff',
                    color: 'inherit'
                }
            });
        }

        function unblockUI() {
            $.unblockUI();
        }

        $(document).ready(function() {
            function processForm(e) {
                var messages_errors = '';
                blockUI();
                $.ajax({
                    url: '/register/become_coach',
                    dataType: 'json',
                    type: 'POST',
                    data: $(this).serialize(),
                    success: function(data) {
                        unblockUI();
                        console.log(data);
                        if (data.data) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Félication !',
                                text: 'Enregistrement effectué avec succès',
                            });
                            setTimeout(function() {
                                window.location.href = "/customer/dashboard"
                            }, 500);
                        } else {
                            if (data.email) {
                                messages_errors = data.email[0];
                            }
                            if (data.password) {
                                messages_errors = data.password[0];
                            }
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops',
                                text: messages_errors,
                            });
                        }
                    },
                    error: function(error) {
                        unblockUI();
                        console.log(error);
                        Swal.fire({
                            icon: 'warning',
                            title: 'Oops...',
                            text: 'Une erreur s\'est lors de l\'enregistrement veuillez réessayer',
                        });
                    }
                });

                e.preventDefault();
            }

            $('#my-form').submit(processForm);


            $('.selectpicker').selectpicker();

            $('#birthday').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>
@endpush
