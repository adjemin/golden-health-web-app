@extends("customer_frontOffice.layouts_customer.master_customer")

@section('content')

@section('css')
<style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }

    a {
        color: black;
    }

    .wrap-resultats .resultats {
        font-size: 64px;
        font-weight: 900;
        color: #099;
        line-height: 100%;
        margin: 25px 0 7px;
        font-family: "Arial Black", "Arial Bold", Gadget, sans-serif;
    }

    .wrap-resultats .txt-resultats {
        text-transform: uppercase;
        font-size: 18px;
        font-weight: 700;
        line-height: 120%;
        color: #099;
    }

    #progressbar {
    margin-bottom: 3vh;
    overflow: hidden;
    color: rgb(252, 103, 49);
    padding-left: 0px;
    margin-top: 3vh
    }

    #progressbar li {
        list-style-type: none;
        font-size: x-small;
        width: 25%;
        float: left;
        position: relative;
        font-weight: 400;
        color: rgb(160, 159, 159)
    }

    #progressbar #step1:before {
        content: "";
        color: rgb(252, 103, 49);
        width: 5px;
        height: 5px;
        margin-left: 0px !important
    }

    #progressbar #step2:before {
        content: "";
        color: #fff;
        width: 5px;
        height: 5px;
        margin-left: 32%
    }

    #progressbar #step3:before {
        content: "";
        color: #fff;
        width: 5px;
        height: 5px;
        margin-right: 32%
    }

    #progressbar #step4:before {
        content: "";
        color: #fff;
        width: 5px;
        height: 5px;
        margin-right: 0px !important
    }

    #progressbar li:before {
        line-height: 29px;
        display: block;
        font-size: 12px;
        background: #ddd;
        border-radius: 50%;
        margin: auto;
        z-index: -1;
        margin-bottom: 1vh
    }

    #progressbar li:after {
        content: '';
        height: 2px;
        background: #ddd;
        position: absolute;
        left: 0%;
        right: 0%;
        margin-bottom: 2vh;
        top: 1px;
        z-index: 1
    }

    .progress-track {
        padding: 0 8%
    }

    #progressbar li:nth-child(2):after {
        margin-right: auto
    }

    #progressbar li:nth-child(1):after {
        margin: auto
    }

    #progressbar li:nth-child(3):after {
        float: left;
        width: 68%
    }

    #progressbar li:nth-child(4):after {
        margin-left: auto;
        width: 132%
    }

    #progressbar li.active {
        color: black
    }

    #progressbar li.active:before,
    #progressbar li.active:after {
        background: rgb(252, 103, 49)
    }

    #details {
    font-weight: 400
    }

    .info .col-5 {
        padding: 0
    }

    #heading {
        color: grey;
        line-height: 6vh
    }

    .pricing {
        background-color: #ddd3;
        padding: 2vh 8%;
        font-weight: 400;
        line-height: 2.5
    }

    .item_deat {
        font-weight: 400;
        line-height: 2.5
    }

    .pricing .col-3 {
        padding: 0
    }

    .total {
        padding: 2vh 8%;
        color: rgb(252, 103, 49);
        font-weight: bold
    }

    .total .col-3 {
        padding: 0
    }

    .modal-body {
      padding: 0 2rem;
    }

</style>
@endsection

@section('content')
<!-- header -->
<section>
    <header class="become-coach-header">
        <div class="become-coach-header-des">
            <h3 class="font-weight-bold text-center">Tableau de bord </h3>
            <p class="text-center">
                Gérez toutes vos informations
            </p>
        </div>
    </header>
</section>

<section class="py-5">
    <div class="container py-5">
        <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-12">
                <div class="profil-box-content mb-2">
                    <div>
                        <div class="mb-2">
                            <img src="{{$customer->photo_url}}" class="rounded-circle" height="60" width="60" alt="">
                        </div>
                        <p class="font-weight-bold">{{$customer->name}}</p>
                        <p>Statut : <span class="self-challenger-box">@if($customer->is_coach) Coach @else Self Challenger @endif</span></p>
                        <?php $date_start = Carbon\Carbon::parse($customer->created_at)->locale('fr_FR'); ?>
                        <p>Inscrit depuis le {{$date_start->isoFormat('LL')}}</p>
                        {!! $customer->profileCompletionProgress() !!}
                        <a href="{{ route('customer.manage_profil') }}" class="primary-text-color update-profil-link">Modifier mon
                            profil </a>
                    </div>
                </div>
                @if($customer->is_coach == null)
                <div class="profil-box-content">
                    <div class="row">
                        <div class="col">
                            <center><img src="{{asset ('customer/images/contract.png') }}" class="text-center" width="30" height="30" alt=""></center>
                            <center><small>Abonnements</small></center>
                        </div>

                        <div class="col">
                            <center>
                                <small><img src="{{ asset('customer/images/mail.png') }}" class="text-center" width="30" height="30" alt=""></small>
                            </center>
                            <center>
                                <small>Messages</small>
                            </center>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col">
                            <center><img src="{{asset ('customer/images/coupon.png') }}" class="text-center" width="30" height="30" alt="">
                            </center>
                            <center><small>Bon de reductions </small></center>
                        </div>

                        <div class="col">
                            <center>
                                <small><img src="{{asset ('customer/images/ch.png') }}" class="text-center" width="30" height="30" alt=""></small>
                            </center>
                            <center>
                                <small>Mes coachs</small>
                            </center>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col">
                            <center><img src="{{asset ('customer/images/heart.png') }}" class="text-center" width="30" height="30" alt="">
                            </center>
                            <center><small>Bon de reductions </small></center>
                        </div>
                        <div class="col">
                            <a href="/customer/tickets">
                                <center><img src="{{asset ('customer/images/ticket.png') }}" class="text-center" width="30" height="30" alt="">
                                </center>
                                <center>
                                    <small>Mes Tickets</small>
                                </center>
                            </a>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12">

              @if($order != null)
                <div class="profil-box-content p-0 mb-2 r">
                    <div class="courses-entete r">
                        Mon ticket
                    </div>
                    <div class="courses-content">
                        <div class="mt-4">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Numéro commande</th>
                                        <th>Produits</th>
                                        <th>Montant</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($order)
                                        <tr id="wishlist_10136" data-product-id="{{$order->id}}">
                                            <td>{{$order->id}}</td>

                                            {{-- <td> <span class="badge  {{$order->isValid() ? 'badge-success' : 'badge-danger'}} p-3">{{$order->is_used ? 'Utilisé' : 'Pas encore utilisé'}}</span> </td> --}}
                                            <td>{{$order->orderItems->count()}}</td>

                                            <td>{{$order->amount}} CFA</td>
                                            <td>{{$order->created_at}}</td>
                                            <td>
                                                {!!$order->statusBadge() !!}
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        @else
                            <p class="text-center fs-l fw-500 pt-3">
                                Aucun ticket
                            </p>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>

<div class="modal fade" id="bookingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Détail réservation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="booking-detail" class="mb-4">
          <input type="hidden" name="booking_id" id="input-booking" />
          <div class="info mb-2">
              <div class="row">
                  <div class="col-7"></div>
                  <div  class="col-5 pull-right"> <span id="heading">Order No.</span><br> <span id="ref"></span> </div>
              </div>
          </div>
          <div class="item_deat" id="customer_detail">
            <div class="title"><strong>Information du client</strong> </div>
            <div class="row">
                <div class="col-9"> <span>Nom du client</span> </div>
                <div class="col-3"> <span id="customer_name"></span> </div>
            </div>
          </div>
          <div>

            <div class="item_deat">
                <div class="title"><strong>Information Réservation</strong> </div>
                <div class="row">
                    <div class="col-9"> <span id="lieu">Type Lieu</span> </div>
                    <div class="col-3"> <span></span> </div>
                </div>

                <div class="row">
                    <div class="col-9"> <span>Date et heure</span> </div>
                    <div class="col-3"> <span id="date"></span> </div>
                </div>
                <div class="row">
                    <div class="col-9"> <span>Description du lieu</span> </div>
                    <div class="col-3"> <span></span> </div>
                </div>
            </div>
            <div class="pricing mt-2">
                <div class="row">
                    <div class="col-9"> <span id="name-booking"></span> </div>
                    <div class="col-3"> <span id="price">0</span> </div>
                </div>
                <div class="row">
                    <div class="col-9"> <span>Quantité</span> </div>
                    <div class="col-3"> <span id="quantity">0</span> </div>
                </div>
            </div>
            <div class="total">
                <div class="row">
                    <div class="col-9"></div>
                    <div class="col-3"><big id="total"></big></div>
                </div>
            </div>
          </div>

          <div class="tracking">
              <div class="title">Tracking Réservation</div>
          </div>
          <div class="progress-track">
              <ul id="progressbar">
                  <li class="step0 active " id="step1">Confirmation Coach</li>
                  <li class="step0  text-center" id="step2">Paiement</li>
                  <li class="step0  text-right" id="step3">Début de la séance</li>
                  <li class="step0 text-right" id="step4">Terminée</li>
              </ul>
          </div>
          <div class="row">
            <div class="col-4">
                <div id="button-div" style="display:none">
                    <button type="button" class="btn btn-primary" id="confirm_reservation" onclick="return confirm('Confirmer la réservation?')">Confirmer la réservation</button>
                </div>
            </div>
            <div class="col-4">
                <div id="button-reject" style="display:none">
                    <button type="button" class="btn btn-danger" id="reject_reservation" onclick="return confirm('Rejeter la réservation?')">Rejeter la réservation</button>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>
</section>
@endsection
@push('js')
<script type="text/javascript" src="{{asset('assets/js/jquery.blockui.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
    /*
     * Block / Unblock UI
     */
    function blockUI() {
        $.blockUI({
            message: "<img src='{{asset('assets/img/loading.gif')}}' alt='Loading ...' />"
            , overlayCSS: {
                backgroundColor: '#1B2024'
                , opacity: 0.55
                , cursor: 'wait'
            }
            , css: {
                border: '4px #999 solid'
                , padding: 15
                , backgroundColor: '#fff'
                , color: 'inherit'
            }
        });
    }

    function unblockUI() {
        $.unblockUI();
    }

    $(document).ready(function() {
        function processForm(e) {
            blockUI();
            $.ajax({
                url: '/customer/calcul/imc'
                , dataType: 'json'
                , type: 'POST'
                , data: $(this).serialize()
                , success: function(data) {
                    unblockUI();
                    console.log(data);
                    if (data.data) {
                        $('#dev_resultat_imc').text(data.data.value_BMI);
                        $('#dev_resultats').show();
                        Swal.fire({
                            icon: 'success'
                            , title: 'Félication !'
                            , text: 'Calcul effectué avec succès'
                        , });
                    } else {
                        $('#dev_resultat_imc').text(data.value_BMI);
                        $('#dev_resultats').show();
                    }
                }
                , error: function(error) {
                    unblockUI();
                    console.log(error);
                    Swal.fire({
                        icon: 'warning'
                        , title: 'Oops...'
                        , text: 'Une erreur s\'est produite veuillez réessayer'
                    , });
                }
            });
            e.preventDefault();
        }
        $('#calcul-imc-form').submit(processForm);


        $('#confirm_reservation').on('click', function(e){
          $('#bookingModal').modal('hide');
          blockUI();
          var booking_id = $('#input-booking').val();
          console.log(booking_id);
          $.ajax({
              url: '/api/booking_coaches/'+booking_id
              , dataType: 'json'
              , type: 'PUT'
              , data: {"coach_confirm": 1}
              , success: function(data) {
                  unblockUI();
                  console.log(data);
                      Swal.fire({
                          icon: 'success'
                          , title: 'Félication !'
                          , text: 'Réservation confirmée avec succès'
                      , }).then((result) => {
                            if (result.isConfirmed) {
                                location.reload();
                            }
                        });
              }
              , error: function(error) {
                  unblockUI();
                  console.log(error);
                  Swal.fire({
                      icon: 'warning'
                      , title: 'Oops...'
                      , text: 'Une erreur s\'est produite veuillez réessayer'
                  , });
              }
          });
          e.preventDefault();
        });

        $('#reject_reservation').on('click', function(e){
          $('#bookingModal').modal('hide');
          blockUI();
          var booking_id = $('#input-booking').val();
          console.log(booking_id);
          $.ajax({
              url: '/api/booking_coaches/'+booking_id
              , dataType: 'json'
              , type: 'PUT'
              , data: {"coach_confirm": 2}
              , success: function(data) {
                  unblockUI();
                  console.log(data);
                      Swal.fire({
                          icon: 'success'
                          , title: 'Ohoh !'
                          , text: 'Réservation rejetée avec succès'
                      , }).then((result) => {
                            if (result.isConfirmed) {
                                location.reload();
                            }
                        });
              }
              , error: function(error) {
                  unblockUI();
                  console.log(error);
                  Swal.fire({
                      icon: 'warning'
                      , title: 'Oops...'
                      , text: 'Une erreur s\'est produite veuillez réessayer'
                  , });
              }
          });
          e.preventDefault();
        });

        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })

        $('.booking_detail').on('click', function(e){
          var bookin_id = $(this).attr('data-id');
          var type_modal = $(this).attr('data-modal');
          blockUI();
          $.ajax({
              url: '/api/booking_coaches/'+bookin_id
              , dataType: 'json'
              , type: 'GET'
              , success: function(data) {
                  unblockUI();
                  console.log(data);
                  var resp = data.data;
                  /////////// ITEM BOOKING APPEND DATA MODAL
                  $('#booking-detail #input-booking').val(resp.id);
                  $('#booking-detail #name-booking').text(resp.course.discipline.name +" - "+ resp.pack.name);
                  $('#booking-detail #price').text(resp.course.price+ " CFA");
                  $('#booking-detail #quantity').text(resp.quantity);
                  $('#booking-detail #ref').text(resp.booking_ref);
                  $('#booking-detail #total').text(resp.amount+ " CFA");
                  $('#booking-detail #date').text(resp.booking_date+ " "+ resp.booking_time);

                  /////////// CUSTOMER APPEND DATA MODAL
                  $('#booking-detail #customer_name').text(resp.customer.first_name+ " "+resp.customer.last_name);
                  ////////// UPDATE PROGRESS BAR STATE Order
                  if (resp.status == 2) {
                    $('#booking-detail #step2').addClass('active');
                  } else {
                    $('#booking-detail #step2').removeClass('active');
                  }
                  if (resp.status == 3) {
                    $('#booking-detail #step3').addClass('active');
                  }else {
                    $('#booking-detail #step3').removeClass('active');
                  }
                  if (resp.status == 4) {
                    $('#booking-detail #step4').addClass('active');
                  } else {
                    $('#booking-detail #step4').removeClass('active');
                  }
                  if (resp.coach_confirm == false){
                    $('#booking-detail #button-div').show();
                  } else {
                    $('#booking-detail #button-div').hide();
                  }
                  if(resp.status == 1) {
                      $('#booking-detail #button-reject').show();
                  } else {
                    $('#booking-detail #button-reject').hide();
                  }
                  if (type_modal == "customer") {
                    $('#booking-detail #button-reject').hide();
                    $('#booking-detail #button-div').hide();
                    $('#booking-detail #customer_detail').hide();
                  }
                  $('#bookingModal').modal('show');
              }
              , error: function(error) {
                  unblockUI();
                  console.log(error);
              }
          });
          e.preventDefault();
        });
    });

</script>
@endpush
