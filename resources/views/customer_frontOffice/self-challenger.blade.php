@extends("customer_frontOffice.layouts_customer.master_customer")
@section('css')
<style>
    body {
        background-color: rgba(238, 238, 238, 0.336);
    }

    .qui-somme-nous {
        line-height: 2;
    }

</style>
@endsection

@section('content')

<section class="py-5">
    <div class="container">
        <h1 class="font-weight-bold primary-text-color mb-3">Self Challenger</h1>
        <p class="qui-somme-nous">
            Veux-tu sortir de ta zone de confort et faire le choix d’une vie plus saine? Une vie où on est en harmonie avec son corps et on apprend à se transcender ? Une vie où l’on devient tout simplement une meilleure version de « soi-même » : alors tu es un self-challenger !
        </p>

        <p class="qui-somme-nous">
            Golden Health accompagne le self-challenger dans cette aventure en mettant à sa disposition des coachs qualifiés pour un suivi professionnel, des diététiciens pour un suivi nutritionnel, en plus d’un tableau de bord personnel retraçant ses performances.
        </p>

        <p class="mb-5">Etes-vous prêt à relever vos défis sportifs ?<br><br>A vos marques, prêt, c’est parti !</p>

        <a href="{{ route('inscription') }}" class="primary-button">
            S'inscrire
        </a>


    </div>
</section>

@endsection
