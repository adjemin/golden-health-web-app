{{-- <h1>Coach</h1> --}}
@extends("customer_frontOffice.layouts_customer.search_master_customer")

@section('css')

    <style>
        body {
            background-color: #eee;
        }


        a {
            color: black;
        }

        .slider {
            -webkit-appearance: none;
            width: 100%;
            height: 15px;
            border-radius: 5px;  
            background: #d3d3d3;
            outline: none;
            opacity: 0.7;
            -webkit-transition: .2s;
            transition: opacity .2s;
        }

        .slider::-webkit-slider-thumb {
            -webkit-appearance: none;
            appearance: none;
            width: 25px;
            height: 25px;
            border-radius: 50%; 
            background: #edaa0d;
            cursor: pointer;
        }

        .slider::-moz-range-thumb {
            width: 25px;
            height: 25px;
            border-radius: 50%;
            background: #edaa0d;
            cursor: pointer;
        }

        .badges--coach{
            display: flex;
            flex-wrap: wrap;
        }

    </style>
@endsection

@section('content')
    <!-- Body -->
    <main class="container-fluid">
        <section>
            <a id="button"></a>
        </section>
        <section id="products-page-body" class="row">

            <aside id="products-page-aside" class="col-md-3 py-3 px-3">
                <section class="px-3">
                    <div class="d-flex justify-content-between mb-2">
                        <span class="text-secondary h3">Filtres</span>
                        <span>Réinitialiser</span>
                    </div>
                    <div>
                        <p>
                            Cocody Angre 8e tranche
                        </p>

                    </div>
                </section>
                <!--  -->
                <!-- Filtre -->


                <section class="px-3 my-3">
                    <form role="form" id="filter-form">
                        {{-- <Tarif/Séance range slider --}}
                        <span class="text-secondary h6">Tarif / Séance</span>
                        
                        <form class="multi-range-field my-2">
                            <input id="multiple" class="slider filter-option" value="{{  $maxprice }}" step='1000' type="range" min="1000" max="100000" />
                        </form>
                        <div>Prix Max. : <span id="montant_max"></span> FCFA</div>
                        
                        <hr>

                        <!-- Disciplines -->
                        <section filter="discipline">
                            <span class="text-secondary h6">Disciplines</span>
                            <div class="px-3 my-2">
                                @php
                                    $selected = explode(',', $_GET['discipline']);
                                    $decode_selected = collect($selected)
                                        ->map(function ($id) {
                                            return base64_decode($id);
                                        })
                                        ->toArray();
                                    //dd($decode_selected);
                                @endphp
                                @if (!empty($disciplines) && !empty($decode_selected))
                                    @foreach ($disciplines as $discipline)
                                        <div class="form-check">
                                            <input class="filter-option form-check-input filled-in" type="checkbox"
                                                id="discipline-{{ $discipline->id }}" discipline="{{ $discipline->id }}"
                                                name="discipline[]" value="{{ $discipline->id }}" {!! in_array($discipline->id, $decode_selected) ? 'checked' : '' !!}>
                                            <label class="form-check-label"
                                                for="discipline-{{ $discipline->id }}">{{ $discipline->name }}</label>
                                        </div>
                                    @endforeach
                                @else
                                    @foreach ($disciplines as $discipline)
                                        <div class="form-check">
                                            <input class="filter-option form-check-input filled-in" type="checkbox"
                                                id="discipline-{{ $discipline->id }}" discipline="{{ $discipline->id }}"
                                                name="discipline[]" value="{{ $discipline->id }}">
                                            <label class="form-check-label"
                                                for="discipline-{{ $discipline->id }}">{{ $discipline->name }}</label>
                                        </div>
                                    @endforeach
                                @endif
                            </div>

                            <hr>
                        </section>

                        {{-- Niveau d'expérience --}}
                        <section filter="experience">
                            <span class="text-secondary h6">Niveau d'expérience </span>
                            <div class="px-3 my-2">
                                <div class="form-check">
                                    <input class="filter-option form-check-input filled-in" type="radio" id="experience-1"
                                        experience="0-2" name="experience" value="0-2" {!! isset($_GET['experience']) && $_GET['experience'] == '0-2' ? 'checked' : '' !!}>
                                    <label class="form-check-label" for="experience-1">0-2</label>
                                </div>
                                <div class="form-check">
                                    <input class="filter-option form-check-input filled-in" type="radio" id="experience-2"
                                        experience="2-7" name="experience" value="2-7" {!! isset($_GET['experience']) && $_GET['experience'] == '2-7' ? 'checked' : '' !!}>
                                    <label class="form-check-label" for="experience-2">2-7</label>
                                </div>
                                <div class="form-check">
                                    <input class="filter-option form-check-input filled-in" type="radio" id="experience-3"
                                        experience="7plus" name="experience" value="7plus" {!! isset($_GET['experience']) && $_GET['experience'] == '7plus' ? 'checked' : '' !!}>
                                    <label class="form-check-label" for="experience-3">7 ans +</label>
                                </div>
                            </div>
                            <hr>
                            <input type="hidden" name="city" value="{!! isset($_GET['city']) ? $_GET['city'] : '' !!}">
                            <input type="hidden" name="lat" value="{!! isset($_GET['lat']) ? $_GET['lat'] : '' !!}">
                            <input type="hidden" name="lon" value="{!! isset($_GET['lon']) ? $_GET['lon'] : '' !!}">
                        </section>
                        <!-- Sexe -->
                        <section filter="genrer">
                            <span class="text-secondary h6">Sexe</span>
                            <div class="px-3 my-2">
                                <div class="form-check">
                                    <input class="filter-option form-check-input filled-in" type="radio" id="gender-1"
                                        genrer="MR" name="gender" value="mr" {!! isset($_GET['gender']) && $_GET['gender'] == 'mr' ? 'checked' : '' !!}>
                                    <label class="form-check-label" for="gender-1">Homme</label>
                                </div>
                                <div class="form-check">
                                    <input class="filter-option form-check-input filled-in" type="radio" id="gender-2"
                                        genrer="MME" name="gender" value="mme" {!! isset($_GET['gender']) && $_GET['gender'] == 'mme' ? 'checked' : '' !!}>
                                    <label class="form-check-label" for="gender-2">Femme</label>
                                </div>
                            </div>
                            <hr>
                            <input type="hidden" name="city" value="{!! isset($_GET['city']) ? $_GET['city'] : '' !!}">
                            <input type="hidden" name="lat" value="{!! isset($_GET['lat']) ? $_GET['lat'] : '' !!}">
                            <input type="hidden" name="lon" value="{!! isset($_GET['lon']) ? $_GET['lon'] : '' !!}">
                        </section>

                        {{-- Disponibilités --}}
                        <section filter="available">
                            <span class="text-secondary h6">Disponibilités</span>
                            <div class="px-3 my-2">
                                <div class="form-check">
                                    <input class="filter-option form-check-input filled-in" type="radio" id="available-1"
                                        available="flexible" name="available" value="flexible" {!! isset($_GET['available']) && $_GET['available'] == 'flexible' ? 'checked' : '' !!}>
                                    <label class="form-check-label" for="available-1">Je suis flexible</label>
                                </div>
                                <div class="form-check">
                                    <input class="filter-option form-check-input filled-in" type="radio" id="available-2"
                                        available="as_soon_as_possible" name="available" value="as_soon_as_possible"
                                        {!! isset($_GET['available']) && $_GET['available'] == 'as_soon_as_possible' ? 'checked' : '' !!}>
                                    <label class="form-check-label" for="available-2">Dès que possible</label>
                                </div>
                                <div class="form-check">
                                    <input class="filter-option form-check-input filled-in" type="radio" id="available-3"
                                        available="specific_date" name="available" value="specific_date"
                                        {!! isset($_GET['available']) && $_GET['available'] == 'specific_date' ? 'checked' : '' !!}>
                                    <label class="form-check-label" for="available-3">A une date précise</label>
                                </div>
                            </div>
                            <hr>
                            {{-- <input type="hidden" name="city" value="{!! isset($_GET['city']) ? $_GET['city'] : '' !!}">
                            <input type="hidden" name="lat" value="{!! isset($_GET['lat']) ? $_GET['lat'] : '' !!}">
                            <input type="hidden" name="lon" value="{!! isset($_GET['lon']) ? $_GET['lon'] : '' !!}"> --}}
                        </section>
                        <section filter="course_lieu">
                            <span class="text-secondary h6">Lieu d'entrainement</span>
                            <div class="px-3 my-2">
                                @foreach ($course_lieus as $course_lieu)
                                    <div class="form-check">
                                        <input class="filter-option form-check-input filled-in" type="radio"
                                            name="course_lieu" id="course_lieu_{{ $course_lieu->id }}"
                                            value="{{ $course_lieu->slug }}" {!! isset($_GET['course_lieu']) && !empty($_GET['course_lieu']) && $_GET['course_lieu'] == $course_lieu->slug ? 'checked' : '' !!}>
                                        <label class="form-check-label"
                                            for="course_lieu_{{ $course_lieu->id }}">{{ $course_lieu->name }}</label>
                                    </div>
                                @endforeach
                            </div>
                        </section>


                        <section filter="course_type">
                            <span class="text-secondary h6">Type de cours</span>
                            <div class="px-3 my-2">
                                @foreach ($courseTypes as $type)
                                    <div class="form-check">
                                        <input class="filter-option form-check-input filled-in" type="radio"
                                            name="course_type" id="course_type_{{ $type->id }}"
                                            value="{{ $type->slug }}" {!! isset($_GET['course_type']) && !empty($_GET['course_type']) && $_GET['course_type'] == $type->slug ? 'checked' : '' !!}>
                                        <label class="form-check-label"
                                            for="course_type_{{ $type->id }}">{{ $type->name }}</label>
                                    </div>
                                @endforeach
                            </div>
                        </section>
                        <section filter="language">
                            <span class="text-secondary h6">Langues</span>
                            <div class="px-3 my-2">
                                @foreach ($languages as $language)
                                    <div class="form-check">
                                        <input class="filter-option form-check-input filled-in" name="language" type="radio"
                                            id="language_{{ $language->id }}" value="{{ $language->name }}"
                                            {!! isset($_GET['language']) && !empty($_GET['language']) && $_GET['language'] == $language->name ? 'checked' : '' !!}>
                                        <label class="form-check-label"
                                            for="language_{{ $language->id }}">{{ $language->name }} </label>
                                    </div>
                                @endforeach
                                {{-- <div class="form-check">
                                    <input class="form-check-input" name="anglaise" type="checkbox" id="language-2" language="2">
                                    <label class="form-check-label" for="language-2">Français </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" name="anglaise" type="checkbox" id="language-3" language="3">
                                    <label class="form-check-label" for="language-3">Espagnole </label>
                                </div> --}}
                            </div>

                            <hr>
                        </section>

                        <!-- Niveau d'expérience --
                                                                                                                                <section filter="experience">
                                                                                                                                    <span class="text-secondary h6">Niveau d'expérience</span>
                                                                                                                                    <div class="px-3 my-2">
                                                                                                                                        <div class="form-check">
                                                                                                                                            <input class="filter-option form-check-input filled-in" type="checkbox" id="experience-1" experience="1">
                                                                                                                                            <label class="form-check-label" for="experience-1">0-2 ans</label>
                                                                                                                                        </div>
                                                                                                                                        <div class="form-check">
                                                                                                                                            <input class="filter-option form-check-input filled-in" type="checkbox" id="experience-2" experience="2">
                                                                                                                                            <label class="form-check-label" for="experience-2">2-5 ans</label>
                                                                                                                                        </div>
                                                                                                                                        <div class="form-check">
                                                                                                                                            <input class="filter-option form-check-input filled-in" type="checkbox" id="experience-3" experience="3">
                                                                                                                                            <label class="form-check-label" for="experience-3">5 ans et +</label>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <hr>
                                                                                                                                </section>
                                                                                                                                <!-- Disponibiltés --
                                                                                                                                <section filter="availability">
                                                                                                                                    <span class="text-secondary h6">Disponibiltés</span>
                                                                                                                                    <div class="px-3 my-2">
                                                                                                                                        <div class="form-check">
                                                                                                                                            <input class="form-check-input" type="checkbox" id="availability-2" availability="2">
                                                                                                                                            <label class="form-check-label" for="availability-2">Je suis flexible</label>
                                                                                                                                        </div>
                                                                                                                                        <div class="form-check">
                                                                                                                                            <input class="form-check-input" type="checkbox" id="availability-3" availability="3">
                                                                                                                                            <label class="form-check-label" for="availability-3">Dès que possible</label>
                                                                                                                                        </div>
                                                                                                                                        <div class="form-check">
                                                                                                                                            <input class="form-check-input" type="checkbox" id="availability-4" availability="4">
                                                                                                                                            <label class="form-check-label" for="availability-4">A une date précise</label>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <hr>
                                                                                                                                </section>
                                                                                                                                <!-- Lieu d'entrainement --
                                                                                                                                <section filter="location">
                                                                                                                                    <span class="text-secondary h6">Lieu d'entrainement</span>
                                                                                                                                    <div class="px-3 my-2">
                                                                                                                                        <div class="form-check">
                                                                                                                                            <input class="form-check-input" type="checkbox" id="location-2" location="2">
                                                                                                                                            <label class="form-check-label" for="location-2">Gymnasme de l'entraineur</label>
                                                                                                                                        </div>
                                                                                                                                        <div class="form-check">
                                                                                                                                            <input class="form-check-input" type="checkbox" id="location-3" location="3">
                                                                                                                                            <label class="form-check-label" for="location-3">Emplacement public extérieur</label>
                                                                                                                                        </div>
                                                                                                                                        <div class="form-check">
                                                                                                                                            <input class="form-check-input" type="checkbox" id="location-4" location="4">
                                                                                                                                            <label class="form-check-label" for="location-4">Ma maison ou mon appartement</label>
                                                                                                                                        </div>
                                                                                                                                        <div class="form-check">
                                                                                                                                            <input class="form-check-input" type="checkbox" id="location-5" location="5">
                                                                                                                                            <label class="form-check-label" for="location-5">Un lieu recommandé par
                                                                                                                                                l’entraîneur</label>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <hr>
                                                                                                                                </section>
                                                                                                                                <!-- Langues --
                                                                                                                                <section filter="langue">
                                                                                                                                    <span class="text-secondary h6">Langues</span>
                                                                                                                                    <div class="px-3 my-2">
                                                                                                                                        <div class="form-check">
                                                                                                                                            <input class="form-check-input" type="checkbox" id="langue-1" langue="1">
                                                                                                                                            <label class="form-check-label" for="langue-1">Anglais </label>
                                                                                                                                        </div>
                                                                                                                                        <div class="form-check">
                                                                                                                                            <input class="form-check-input" type="checkbox" id="langue-2" langue="2">
                                                                                                                                            <label class="form-check-label" for="langue-2">Français </label>
                                                                                                                                        </div>
                                                                                                                                        <div class="form-check">
                                                                                                                                            <input class="form-check-input" type="checkbox" id="langue-3" langue="3">
                                                                                                                                            <label class="form-check-label" for="langue-3">Espagnole </label>
                                                                                                                                        </div>
                                                                                                                                                                       <hr>
                                                                                                                                </section> -->
                    </form>
                </section>
            </aside>

            <main id="products-page-main" class="col-md-9 bg-pale py-3">

                <!--div class="row mx-auto">
                                                                                                                                <h2 class="font-weight-bold primary-text-color"> Le fitness</h2>
                                                                                                                                <div class="small-secondary-dividers"></div>
                                                                                                                                <div class="">
                                                                                                                                    <p class="mt-4 mb-3 ml-3 discipline-des-line-height">
                                                                                                                                        Le fitness (abréviation de l'expression anglaise physical fitness, « forme physique »), aussi
                                                                                                                                        appelé la gymnastique de forme1 ou l'entraînement physique2, désigne un ensemble d'activités
                                                                                                                                        physiques permettant au pratiquant d'améliorer sa condition physique et son hygiène de vie, dans
                                                                                                                                        un souci de bien-être, Le fitness (abréviation de l'expression anglaise physical fitness, «
                                                                                                                                        forme physique »), aussi appelé la gymnastique de forme1 ou l'entraînement physique2, désigne un
                                                                                                                                        ensemble d'activités physiques permettant au pratiquant d'améliorer sa condition physique et son
                                                                                                                                        hygiène de vie, dans un souci de bien-être section. Le fitness (abréviation de l'expression
                                                                                                                                        anglaise physical fitness, « forme physique »), <br> aussi appelé la gymnastique de forme1 ou
                                                                                                                                        l'entraînement physique2, désigne un ensemble d'activités physiques permettant au pratiquant
                                                                                                                                        d'améliorer sa condition physique et son hygiène de vie, dans un souci de bien-être.                                                                                             </div-->

                <div class="row mb-3 mt-5">
                    <h2 class="font-weight-bold primary-text-color">Les différents coachs @if (isset($_GET['city'])){{ $_GET['city'] }}@endif</h2>
                </div>

                <div class="row flex-wrap" id="resultats">
                    {{-- @forelse($coaches as $coach)
                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 mb-3">
                        <a href="/coach_profil/{{base64_encode($coach->id)}}">
                            <div class="coach-card">
                                <div class="price-position">
                                    <img src="{{$coach->customer->photo_url}}" class="card-img-top img-fluid mb-2">
                                    @php
                                        $min = collect($coach->courses)->map(function($course){
                                            return $course->price;
                                        })->min();
                                    @endphp
                                    <div class="price-content">{{$min}} {{$coach->currency_name}}</div>
                                </div>
                                <div class="col ">
                                    <h5><b>{{$coach->customer->first_name}}</b> </h5>
                                    <h5><b>{{$coach->customer->location_address ??  $coach->customer->commune ?? $coach->customer->quartier}}</b> </h5>
                                    <p class="text-primary font-weight-bold">
                                        @foreach ($coach->disciplines as $discipline)
                                            Coach {{$discipline->name}} @if (!$loop->last) , @endif
                                        @endforeach
                                    </p>
                                    <div>
                                        <a href="#">
                                            @php
                                                $rating = (int) $coach->rating;
                                            @endphp
                                            @for ($i = 0; $i < 5; $i++)
                                                @if ($i + 1 <= $rating)
                                                    <img src="{{ asset('customer/images/stars.png')}}" class="icon" alt="">
                                                @else
                                                    <img src="{{ asset('customer/images/star.png')}}" class="icon" alt="">
                                                @endif
                                            @endfor
                                        </a>
                                    </div>
                                    <div class="d-flex">
                                        @foreach ($coach->caracteristiques as $caracteristique)
                                            {!! $loop->last ? '<h6 class="chips">' :  '<h6 class="chips mr-2">' !!}
                                                <small class="mr-3 py-4">{{$caracteristique->name}}</small><i class="fa fa-check-circle" style="color: gray;"></i>
                                            </h6>
                                        @endforeach
                                    </div>
                                    <div class="d-flex">
                                        @foreach ($coach->disciplines as $discipline)
                                            {!! $loop->last ? '<h6 class="px-1 py-2" style="color: blue; border-radius:12px;">' :  '<h6 class="px-1 py-2 mr-1" style="color: blue; border-radius:12px;">' !!}
                                                <small class="mr-1">{{$discipline->name}}</small><i class="fa fa-check-circle" style="color: gray;"></i>
                                            </h6>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @empty
                    <div style="position:relative; top: 50px; left:75px">
                        <div class="d-flex justify-content-center align-items-center">
                            <div class="card card-body" style="width:800px;">
                                <center>
                                    <img src="{{ asset('customer/images/gh1.png') }}" alt="Logo" width="500px">
                                </center>
                                <h4 class="text-center"> Désolé Aucun resultat ne correspond à votre recherche</h4>
                            </div>
                        </div>
                    </div>
                @endforelse --}}
                    @php
                        $count = 0;
                    @endphp
                    @foreach ($courses as $course)
                        @if ($course && $course->coach && $course->discipline)
                            @php
                                $count++;
                            @endphp
                            <div class="col-lg-3 col-md-3 col-sm-12 col-12 mb-3">
                                <a href="/coach_profil/{{ base64_encode($course->id) }}">
                                    <div class="coach-card">
                                        <div class="price-position">
                                            <img src="{{ $course->coach->customer->photo_url }}"
                                                class="card-img-top img-fluid mb-2">
                                            <div class="price-content">{{ $course->price }} XOF</div>
                                        </div>
                                        <div class="col ">
                                            <h5><b>{{ $course->coach->customer->first_name }}</b> </h5>
                                            <h6><b>{{ $course->coach->customer->location_address ?? ($course->coach->customer->commune ?? $course->coach->customer->quartier) }}</b>
                                            </h6>
                                            @if ($course->course_type_object)
                                                <p class="text-primary font-weight-bold">
                                                    {{ $course->course_type_object->name }}</p>
                                            @endif
                                            <div class="mb-2">
                                                @php
                                                    $rating = (int) $course->coach->rating;
                                                @endphp
                                                @for ($i = 0; $i < 5; $i++)
                                                    @if ($i + 1 <= $rating)
                                                        <img src="{{ asset('customer/images/stars.png') }}"
                                                            class="icon" alt="">
                                                    @else
                                                        <img src="{{ asset('customer/images/star.png') }}"
                                                            class="icon" alt="">
                                                    @endif
                                                @endfor
                                            </div>
                                            <p class="text-secondary font-weight-bold mb-2 mt-2">Coach
                                                {{ $course->discipline->name }}</p>

                                            {{-- @dd( $course->discipline->name) --}}

                                            {{-- <div class="d-flex">
                                                @foreach ($course->coach->caracteristiques as $caracteristique)
                                                    {!! $loop->last ? '<h6 class="chips">' : '<h6 class="chips mr-2">' !!}
                                                    <small class="mr-3 py-4">{{ $caracteristique->name }}</small><i
                                                        class="fa fa-check-circle" style="color: gray;"></i>
                                                    </h6>
                                                @endforeach
                                            </div> --}}
                                            <div class="badges--coach">
                                                @foreach ($course->coach->disciplines as $discipline)
                                                    {!! $loop->last ? '<h6  style="color: blue; border-radius:12px;">' : '<h6 class=" mr-2" style="color: blue; border-radius:12px;">' !!}
                                                    <small class="mr-1">{{ $discipline->name }}</small><i
                                                        class="fa fa-check-circle" style="color: gray;"></i>
                                                    </h6>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endif
                        {{-- @empty --}}
                    @endforeach
                    @if ($count == 0)
                        <div style="position:relative; top: 50px; left:75px">
                            <div class="d-flex justify-content-center align-items-center">
                                <div class="card card-body" style="width:800px;">
                                    <center>
                                        <img src="{{ asset('customer/images/gh1.png') }}" alt="Logo" width="180px">
                                    </center>
                                    <h6 class="text-center" style="color:#AAA"> Désolé Aucun resultat ne correspond à
                                        votre recherche</h6>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div id="loader"></div>
                </div>
            </main>

        </section>

        <section>
        </section>
    </main>
@endsection
@push('js')
    <script type="text/javascript">
        $('#multiple').change(function(){
            console.log($('#multiple').val())
        })
        var slider = document.getElementById("multiple");
        var output = document.getElementById("montant_max");
        output.innerHTML = slider.value;
        slider.oninput = function() {
            output.innerHTML = this.value;
        }

        $(".filter-option").change(function() {
            disciplines = []
            $('input[name="discipline[]"]:checked').each(function() {
                disciplines.push($(this).val());
            });
            console.log(disciplines);

            course_type = $('input[name="course_type"]:checked').val() != undefined ? $(
                'input[name="course_type"]:checked').val() : "";
            course_lieu = $('input[name="course_lieu"]:checked').val() != undefined ? $(
                'input[name="course_lieu"]:checked').val() : "";
            language = $('input[name="language"]:checked').val() != undefined ? $(
                'input[name="language"]:checked').val() : "";
            available = $('input[name="available"]:checked').val() != undefined ? $(
                'input[name="available"]:checked').val() : "";
            gender = $('input[name="gender"]:checked').val();
            experience = $('input[name="experience"]:checked').val();
            city = $('input[name="city"]').val();
            lat = $('input[name="lat"]').val();
            lon = $('input[name="lon"]').val();
            var price = $('#montant_max').text()
            

            if (gender != null) {
                window.location.href = "/search_coach?discipline=" + disciplines.map(function(id) {
                        return btoa(id)
                    }).join(',') + "&gender=" + gender + "&city=" + city + "&lat=" +
                    lat + "&lon=" + lon +
                    "&course_type=" + course_type + "&course_lieu=" + course_lieu + "&experience=" + experience +
                    "&language=" + language + "&available=" + available+ "&maxprice=" + price
            } else {
                window.location.href = "/search_coach?discipline=" + disciplines.map(function(id) {
                        return btoa(id)
                    }).join(',') + "&city=" + city + "&lat=" + lat + "&lon=" + lon + "&course_type=" + course_type +
                    "&course_lieu=" + course_lieu + "&experience=" + experience + "&language=" + language +
                    "&available=" + available+ "&maxprice=" + price
            }
            if (experience != null) {
                window.location.href = "/search_coach?discipline=" + disciplines.map(function(id) {
                        return btoa(id)
                    }).join(',') + "&gender=" + gender + "&city=" + city + "&lat=" +
                    lat + "&lon=" + lon +
                    "&course_type=" + course_type + "&course_lieu=" + course_lieu + "&experience=" + experience +
                    "&language=" + language + "&available=" + available + "&maxprice=" + price
            } else {
                window.location.href = "/search_coach?discipline=" + disciplines.map(function(id) {
                        return btoa(id)
                    }).join(',') + "&city=" + city + "&lat=" + lat + "&lon=" + lon + "&course_type=" + course_type +
                    "&course_lieu=" + course_lieu + "&language=" + language + "&available=" + available+ "&maxprice=" + price
            }
        })
        // $(document).ready(() => {
        //     let filters = getFilters();

        //     console.log(filters);

        //     $(".filter-option").change((e) => {
        //         e.stopImmediatePropagation();
        //         for (let filter of Object.keys(filters)) {
        //             console.log(e.target.attributes[filter]);
        //             if (e.target.attributes[filter]) {
        //                 filters = updateFilters(
        //                 filters,
        //                 filter,
        //                 e.target.attributes[filter].value,
        //                 e.target.checked
        //                 );
        //             }
        //         }
        //         getCoachs(filters);
        //     });
        // });
        // function getFilters() {
        //     const filters = Array.from($("[filter]")).map(
        //         (el) => el.attributes.filter.value
        //     );
        //     const dict = {};

        //     filters.forEach((filter) => {
        //         dict[filter] = [];
        //     });
        //     return dict;
        // }
        // function updateFilters(filters, filter, option, value) {
        //     const dict = { ...filters };
        //     if (filter === "price") {
        //         dict.price = [option];
        //     } else if (filter === "rating") {
        //         dict.rating = [option];
        //     } else if (value && filters[filter].indexOf(option) === -1)
        //         dict[filter].push(option);
        //     else if (!value)
        //         dict[filter] = dict[filter].filter((entry) => entry !== option);

        //     console.log(dict);
        //     return dict;
        // }

        // function getCoachs(filters, property = "rating", increase = false) {
        //     $('#loader').css("display", "flex")
        //     $.get(
        //         "/filter/coachs",
        //         { filters, sort: { property, increase } },
        //         (responses) => {
        //             renderResponse(responses);
        //         }
        //     ).fail(() => {
        //         renderResponse([])
        //     })
        // }

        // function createTemplate(data) {
        //   if(data) {
        //     return `
    //     <div class="col-lg-3 col-md-3 col-sm-12 col-12 mb-3">
    //         <a href="/coach_profil/${btoa(data.course_id)}">
    //             <div class="coach-card">
    //                 <div class="price-position">
    //                     <img src="${data.photo_url}" class="card-img-top img-fluid mb-2">
    //                     <div class="price-content">${data.course_price} XOF</div>
    //                 </div>
    //                 <div class="col">
    //                     <h5><b>${data.name}</b> </h5>
    //                     <p class="text-primary font-weight-bold">Coach ${data.discipline_name}</p>
    //                     <div class="mb-4">
    //                         <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
    //                         <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
    //                         <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
    //                         <a href="#"><img src="{{ asset('customer/images/star.png') }}" class="icon" alt=""></a>
    //                         <a href="#"><img src="{{ asset('customer/images/star.png') }}" class="icon" alt=""></a>
    //                     </div>
    //                 </div>
    //             </div>
    //         </a>
    //     </div>
    //     `;
        //   }
        // }
        // function renderResponse(responses) {
        //     console.log(responses);
        //     $('#loader').css('display', 'none');

        //     const template =
        //     responses.data.length === 0
        //         ? `<div style="position:relative; top: 50px; left:75px">
    //                 <div class="d-flex justify-content-center align-items-center">
    //                     <div class="card card-body" style="width:800px;">
    //                         <center>
    //                             <img src="{{ asset('customer/images/gh1.png') }}" alt="Logo" width="180px">
    //                         </center>
    //                         <h6 class="text-center" style="color:#AAA"> Désolé Aucun resultat ne correspond à votre recherche</h6>
    //                     </div>
    //                 </div>
    //             </div>`
        //         : responses.data.map((response) => createTemplate(response)).join("\n");
        //     $("#resultats").html(template);
        // }
    </script>
@endpush
