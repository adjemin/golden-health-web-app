{{--
<div class="row d-flex justify-content-between align-items-center">
    <div class="btn-article">
        <i class="material-icons">add</i>
    </div>
    <span>
        1
    </span>
    <div class="btn-article">
        <i class="material-icons">remove</i>
    </div>
</div> --}}

<style>

/* The Modal (background) */
.w3-modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 99; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.w3-modal-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 420px;
    max-width: 50%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

/* The Close Button */
.w3-close {
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.w3-close:hover,
.w3-close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.w3-modal-header {
    padding: 2px 16px;
    /* background-color: #5cb85c; */
    background-color:var(--secondary);
    color: white;
}

.w3-modal-body {
    padding: 2px 16px;
    min-height: 300px;
}

.w3-modal-footer {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: white;
}
</style>
<style>
    /* Color radio inputs */

    input[type=radio] {
    display: none;
    }
    input[type=radio]:checked + label span {
    transform: scale(1.25);
    border: 2px solid var(--secondary);
    }
    input[type=radio]:checked + label .red {
    border: 2px solid #fff;
    }
    input[type=radio]:checked + label .orange {
    border: 2px solid #873a08;
    }
    input[type=radio]:checked + label .yellow {
    border: 2px solid #816102;
    }
    input[type=radio]:checked + label .olive {
    border: 2px solid #505a0b;
    }
    input[type=radio]:checked + label .green {
    border: 2px solid #0e4e1d;
    }
    input[type=radio]:checked + label .teal {
    border: 2px solid #003633;
    }
    input[type=radio]:checked + label .blue {
    border: 2px solid #103f62;
    }
    input[type=radio]:checked + label .violet {
    border: 2px solid #321a64;
    }
    input[type=radio]:checked + label .purple {
    border: 2px solid #501962;
    }
    input[type=radio]:checked + label .pink {
    border: 2px solid #851554;
    }

    label {
        display: inline-block;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        margin-right: 10px;
        cursor: pointer;
    }
    label:hover span {
        transform: scale(1.25);
    }
    label span {
        display: block;
        border-radius: 50%;
        width: 100%;
        height: 100%;
        transition: transform 0.2s ease-in-out;
    }
</style>

<div id="product-{{$product->id}}-cartModal" class="w3-modal">

    <!-- w3-Modal content -->
    <div class="w3-modal-content">
        <div class="w3-modal-header">
        <span class="w3-close" id="product-{{$product->id}}-closeModal" onclick="closeModal('{{$product->id}}');">&times;</span>
        <h4 class="mt-1">Veuillez sélectionner une taille et une couleur</h4>
        </div>
        <div class="w3-modal-body d-flex flex-column justify-content-between">
            <div class="container">
                {{-- If product has sizes --}}
                @if($product->sizes()!= null && $product->sizes()->count() > 0)
                    @foreach($product->sizes() as $size)
                        <div class="container">
                            {{-- Size choice line + color --}}
                            <div class="row d-flex justify-content-between align-items-center">
                                <span class="fs-xxl fw-500">
                                    {{$size}}
                                    {{--
                                        <p class="fs-xxl fw-500">
                                        </p>
                                        {{$product->price}}
                                        --}}
                                </span>
                                {{-- Color and quantity --}}
                                <div class="d-flex align-items-center">
                                    {{-- If it has Sizes & Color --}}
                                    @if($product->getProductColor())
                                        <div class="d-flex align-items-center mr-3">
                                            @foreach($product->getProductColor() as $color)
                                                <div class="mr-1">
                                                    <input class="radio-color" type="radio" name="product_{{$size}}_color" value="{{$color->name}}" id="{{$product->id.$size.$color->name}}" />

                                                    <label for="{{$product->id.$size.$color->name}}">
                                                        <span class="span-color" data-color="{{$color->name}}"></span>
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                    {{-- Quantity / size --}}
                                    <div class="mb-2 d-flex justify-content-between align-items-center" data-product-id="{{$product->id}}">
                                        <a onclick="decrementUniqueCartItem('{{$product->id}}', '{{$product->title}}', '{{$product->price}}', '{{$size}}');" class="cartQtyDecrement btn-article ml-0 py-1 px-3 btn-secondary btn-icon fs-xxl">
                                            -
                                        </a>
                                        <span id="cartQtyValue-{{$product->id}}" class="fs-l fw-500 mx-3">
                                            {{$product->uniqueCartItem($size, $color)->quantity ?? 0 }}

                                        </span>
                                        <a onclick="incrementUniqueCartItem('${pId}', '{{$product->title}}', '{{$product->price}}', '{{$size}}');" class="cartQtyIncrement btn-article mr-0 py-1 px-3 btn-secondary btn-icon fs-xxl">
                                            +
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    {{-- If not sizes check for colors --}}
                    @if($product->getProductColor())
                        <div class="d-flex align-items-center mr-3">
                            @foreach($product->getProductColor() as $color)
                                <div class="mr-1">
                                    <input class="radio-color" type="radio" name="product_{{$size}}_color" value="{{$color->name}}" id="{{$product->id.$size.$color->name}}" />

                                    <label for="{{$product->id.$size.$color->name}}">
                                        <span class="span-color" data-color="{{$color->name}}"></span>
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    @endif
                @endif
            </div>
            <div class="w3-modal-foot">
                <div class="row">
                {{-- <span>Total : <span id="product-{{$product->id}}-total">{{$product->uniqueCartItem()}}</span></span> --}}
                </div>
                <div class="row p-2 d-flex justify-content-between">
                    <div class="col">
                        <a class="btn-article btn-primary w-100" href="/basket">
                            Poursuivre vos achats
                        </a>
                    </div>
                    <div class="col">
                        <a class="btn-article btn-secondary w-100" href="/basket">
                            Finaliser la commande
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>

    function openModal(id){
        var modal = document.getElementById("product-"+id+"-cartModal");
        modal.style.display = "block";

    }
    function closeModal(id){
        var modal = document.getElementById("product-"+id+"-cartModal");
        modal.style.display = "none";
    }

</script>
<script>
    // Radio color inputs
    var colorSpans = document.getElementsByClassName('span-color');
    for(var c = 0; c < colorSpans.length; c++){
        //
        colorSpans[c].style.background = colorSpans[c].dataset.color;
    }
</script>

<script>


    function decrementUniqueCartItem(id, price) {
        console.log("dec");

        qty = parseInt($("#cartQtyValue-" + id).html());

        if (qty > 0) {
            qty--;
            $("#cartQtyValue-" + id).html(qty);
            $("#cartItemAmount-" + id).html((qty) * parseInt(price));

            ajaxCart({
                pId: id,
                pName: name,
                pQuantity: qty,
                pPrice: price,
            });

            if (qty == 0) {
                $("#qtyController-" + id).empty();
                $("#btnCart-" + id).html('Ajouter au panier');
                $("#btnCart-" + id).addClass('btn-article btn-secondary w-100  btnAddToCart');

                showSnackbar("Produit Retiré", "bg-danger");

                setTimeout(() => {
                    $("#btnCart-" + id).addClass('invisible');
                }, 3000);
                return false;
            }
            // showSnackbar("Produit Modifi", "bg-danger");

        }


    }

    function increment(id, name, price) {
        console.log("inc");

        qty = parseInt($("#cartQtyValue-" + id).html()) + 1;
        $("#cartQtyValue-" + id).html(qty);
        $("#cartItemAmount-" + id).html(qty * parseInt(price));

        ajaxCart({
            pId: id,
            pName: name,
            pQuantity: qty,
            pPrice: price,
        });
    }

    function ajaxCart(cartData, action = "manage") {
        if (cartData == null) {
            console.log("=== Invalid data");
            return 0;
        }
        console.log("=== Saving to cart");
        console.log(cartData);

        $.ajax({
            type: "POST",
            url: "/ajax/cart/" + action,
            data: {
                'product_id': cartData.pId,
                'product_name': cartData.pName,
                'product_quantity': cartData.pQuantity,
                'product_price': cartData.pPrice,
            },
            dataType: "json",
            success: function(response) {
                if (response.status != "OK") {

                    if (response.status == "REMOVED") {
                        showSnackbar("Produit Retiré", "bg-danger");
                        $(".cart-badge").html(response.data.itemCount);
                        return false;
                    }
                    console.log(">>>> errerur");
                    showSnackbar("Erreur, Veuillez réessayer", "bg-danger", 4000);
                    console.log(response);
                    return false;
                }

                // showSnackbar("Erreur, Veuillez réessayer", "bg-danger", 4000);
                console.log(">>>> SUCCESS  ");
                console.log(response);
                $(".cart-badge").html(response.data.itemCount);
                // window.location.reload();
            },
            error: function(response) {
                console.log("Error :");
                console.log(response);
            },
        });
    }
</script>
