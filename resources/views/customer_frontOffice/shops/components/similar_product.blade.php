<!-- carousel item -->
<div class="swiper-slide">
  <div class="item">
    <figure>
      <div class="intro-img">
        @if($product->isPromoted())
          <div class="new">
            <span>{{"-".$product->promo_percentage."%"}}</span>
          </div>
        @endif
        <a href="{{ route('shop.product_details', $product->slug) }}">
          <img src="{{ $product->getImageUrl() ?? asset('customer/images/8513471.jpg') }}" alt="img dis" class="fill" heigth="200">
        </a>
        <div class="action">
            <ul>
                <li>
                    <a href="{{ route('shop.product_details', $product->slug) }}"><i class="flaticon-search"></i></a>
                </li>
                <!--li>
                    <a href="#"><i class="flaticon-heart"></i></a>
                </li-->
                <li>
                  <a id="btnCart-{{$product->id}}"
                  data-p-id="{{$product->id}}" data-p-name="{{$product->title}}"
                  data-p-price="{{$product->getPrice()}}" class="btnAddToCart"><i class="flaticon-shopping-cart"></i></a>
                </li>
            </ul>
        </div>
      </div>
      <figcaption>
        <i class="ratings__{{$product->getRating()}}"></i>
        <h4>
          <a href="{{ route('shop.product_details', $product->slug) }}">{{ Str::limit($product->title, 50) }}</a>
        </h4>
        <div class="product-meta">
          <div class="price">
            <span class="normal">{{$product->getPrice()}} FCFA</span>
            @if($product->isPromoted())
              <span class="strike">{{$product->original_price}} FCFA</span>
              {{--<span class="strike">{{$product->price}} FCFA</span>--}}
            @endif
          </div>
          <div class="size-n-color">
            <?php if($product->sizes != null) { $sizeProduct = explode(',', $product->sizes);} ?>
            @if(isset($sizeProduct) && !empty($sizeProduct))
              <div class="size">
                  <ul>
                    @foreach($sizeProduct as $size)
                      <li><span>{{$size}}</span></li>
                    @endforeach
                  </ul>
              </div>
              @endif
              @if($product->getProductColor())
              <div class="colors">
                  <ul>
                    @foreach ($product->getProductColor() as $color)
                        <li><span data-color="{{ $color->code_color }}"></span></li>
                    @endforeach
                  </ul>
              </div>
              @endif
          </div>
        </div>
      </figcaption>
    </figure>
  </div>
</div>
