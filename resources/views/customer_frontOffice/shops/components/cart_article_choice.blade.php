


<div>
    @if($product->hasSizes())
        <p>Choisissez une taille</p>
        @foreach($product->sizes() as $size)
            <div class="container">
                {{-- Size choice line + color --}}
                <div class="row d-flex justify-content-between align-items-center">
                <input class="radio-size" type="radio" data-detail-name="taille" name="{{ 'product_'.$product->id.'_size'}}" data-item-id="{{$item->id}}" value="{{$size}}" id="{{$product->id.$size}}" @if($item->size == $size) checked @endif />

                        <label for="{{$product->id.$size}}" class="chooseDetail">
                            <span class="span-size" data-color="{{$size}}">
                                {{$size}}
                            </span>
                        </label>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
    @if($product->hasColors())
        <p>Choisissez une couleur</p>
        @if($product->getProductColor())
            <div class="d-flex align-items-center mr-3">
                @foreach($product->getProductColor() as $color)
                    <div class="mr-1">
                    <input class="radio-color" type="radio" data-detail-name="couleur" data-item-id="{{$item->id}}" name="{{ 'product_'.$product->id.'_color'}}" value="{{$color->name}}" id="{{$product->id.$color->name}}"  @if($item->color == $color->name) checked @endif/>

                        <label for="{{$product->id.$color->name}}" class="chooseDetail">
                            <span class="span-color" data-color="{{$color->name}}"></span>
                        </label>
                    </div>
                @endforeach
            </div>
        @endif
        {{-- <a class="btn btn-primary">
            Choisir une couleur
        </a> --}}
        {{-- {{$product->getProductColor() }} --}}
        {{-- @include('customer_frontOffice.shops.components.cart_article_choice', ['product' => $product]) --}}
    @endif
</div>
<script>
    // Radio color inputs
    var colorSpans = document.getElementsByClassName('span-color');
    for(var c = 0; c < colorSpans.length; c++){

        colorSpans[c].style.background = colorSpans[c].dataset.color;
    }
</script>
