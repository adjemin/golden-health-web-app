<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-12">
    <div class="white-bg card mb-2">
        <a href="{{ route('shop.product_details', $product->slug) }}">
            <div class="img-container">
                <img src="{{ $product->getImageUrl() ?? asset('customer/images/8513471.jpg') }}" class="img-fluid article-image" width="100%" alt="" srcset="">
            </div>
            <div class="px-2 py-1">
                <h6 class="secondary-text-color">
                    <b>{{ Str::limit($product->title, 50) }}</b>
                </h6>
                <div class="p-2">
                    <h3 class="mb-3 text-primary fw-600">
                        @if($product->isPromoted())
                            <span class="mr-3 r p-1 bg-secondary">
                                {{"-".$product->promo_percentage."%"}}
                            </span>
                            <br/>
                        @endif
                        @if($product->isPromoted())
                            <del class="mr-3">
                                {{$product->price}} FCFA
                            </del>
                            <br/>
                        @endif
                        {{$product->getPrice()}} FCFA
                    </h3>
                </div>
                <p>{!! Str::limit($product->description, 50) !!}</p>
            </div>

        </a>
    </div>
</div>

{{-- <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-12">
    <div class="article-card r">
        <a href="{{ route('shop.product_details', $product->slug) }}">
            <div class="article-price">
                {{$product->getPrice()}} FCFA
            </div>
            <div>
                <img src="{{ $product->getImageUrl() ?? asset('customer/images/8513471.jpg') }}" class="img-fluid article-image" width="100%" alt="" srcset="">
            </div>
            <div class="container">
                <p class="font-weight-bold article-title text-primary mb-1 text-capitalize">
                    {{ Str::limit($product->title, 50) }}
                </p>
                <div class="row container">
                    <div class="col-md">
                        <p class="font-weight-bold article-title text-primary mb-1 text-capitalize">
                            {{ $product->sizes }}
                        </p>
                    </div>

                    <div class="col-md">
                        <div class="row">
                            @foreach ($product->getProductColor() as $color)

                                <div class="circle m-1" style="background:{{ $color->code_color }}"></div>
                            @endforeach
                        </div>

                    </div>
                </div>
                <p class="article-description fs-s">
                    {!! Str::limit($product->description, 55) !!}
                </p>
            </div>
            <div class="d-none row justify-content-center my-2 star_rating star-rating-sm">
                <img src="{{ asset('customer/images/stars.png') }}" class="icon mr-2" alt="">
                <img src="{{ asset('customer/images/stars.png') }}" class="icon mr-2" alt="">
                <img src="{{ asset('customer/images/stars.png') }}" class="icon mr-2" alt="">
                <img src="{{ asset('customer/images/star.png') }}" class="icon mr-2" alt="">
                <img src="{{ asset('customer/images/star.png') }}" class="icon mr-2" alt="">
            </div>

            <div class="d-none container font-weight-bold m-0 text-primary">
                Disponible
            </div>
            @if ($product->currentCartItem())
                <div id="qtyController-{{$product->id}}" class="container">
                    <div class="text-center text-secondary fs-l fw-500">
                        <span id="cartItemAmount-{{$product->id}}">
                            {{$product->currentCartItem()->amount}}
                        </span>
                        <span>FCFA</span>
                    </div>
                    <div class="mb-2 d-flex justify-content-between align-items-center" data-product-id="{{$product->id}}">

                        <a onclick="decrement('${pId}', '${pName}', '${pPrice}');" class="cartQtyDecrement btn-article ml-0 py-1 px-3 btn-secondary btn-icon fs-xxl">
                            -
                        </a>
                        <span id="cartQtyValue-{{$product->id}}" class="fs-l fw-500 ">
                            {{$product->currentCartItem()->quantity}}
                        </span>
                        <a onclick="increment('{{$product->id}}', '{{$product->title}}', '{{$product->getPrice()}}');" class="cartQtyIncrement btn-article mr-0 py-1 px-3 btn-secondary btn-icon fs-xxl w-100">
                            +
                        </a>
                    </div>
                    <div class="">
                        <a class="btn-article btn-primary w-100" href="/basket">
                            Finaliser
                        </a>
                    </div>
                </div>
            @else
                <div class="container">
                    <a id="btnCart-{{$product->id}}"
                    data-p-id="{{$product->id}}" data-p-name="{{$product->title}}"
                    data-p-price="{{$product->getPrice()}}" class="btn-article btn-secondary w-100  btnAddToCart invisible">
                        Ajouter au panier
                    </a>
                    <div></div>
                </div>
            @endif
        </a>
    </div>
</div> --}}
