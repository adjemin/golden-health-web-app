<div class="container r b-fg">
    <div class="">
        <h5 class="text-uppercase">Produits</h5>
        <div class="small-secondary-dividers mb-3"></div>
    </div>
    <div class="row">
        @forelse ($products as $product)
            <div class="article-box col-lg-4 col-md-4 col-6 mb-3 r">
                <div class="article-card r">
                    <a href="{{ route('shop.product_details', $product->slug) }}">
                        @if($product->isPromoted())
                        <div class="article-promo">
                            {{"-".$product->promo_percentage."%"}}
                        </div>
                        @endif
                        <div class="article-price">
                            @if($product->isPromoted())
                                <del>
                                    {{$product->price}} FCFA
                                </del>
                                <br>
                            @endif
                            {{$product->getPrice()}} FCFA
                        </div>
                        <div>
                            <img src="{{ $product->getImageUrl() ?? asset('customer/images/8513471.jpg') }}" class="img-fluid article-image" width="100%" alt="" srcset="">
                        </div>
                        <div class="container">
                            <p class="font-weight-bold article-title text-primary mb-1 text-capitalize">
                                {{ Str::limit($product->title, 50) }}
                            </p>
                            <div class="row container">
                                <div class="col-md">
                                    <p class="font-weight-bold article-title text-primary mb-1 text-capitalize">
                                        {{ $product->sizes }}
                                    </p>
                                </div>

                                <div class="col-md">
                                    <div class="row">
                                        @foreach ($product->getProductColor() as $color)
                                            {{-- <p>{{ $color->name }}</p> --}}
                                            <div class="circle m-1" style="background:{{ $color->code_color }}"></div>
                                        @endforeach
                                    </div>
                                </div>

                                {{-- @include('customer_frontOffice.shops.components.cart_quantity', ['product' => $product]) --}}

                            </div>

                            <p class="article-description fs-s">
                                {!! Str::limit($product->description, 55) !!}
                            </p>
                        </div>
                        <div class="col-12 text-center">
                            {!!$product->starRatingWidget()!!}
                        </div>


                        <div class="">
                            @if ($product->currentCartItem())
                                <div id="qtyController-{{$product->id}}" class="container">
                                    <div class="text-center text-secondary fs-l fw-500">
                                        <span id="cartItemAmount-{{$product->id}}">
                                            {{$product->currentCartItem()->amount}}
                                        </span>
                                        <span>FCFA</span>
                                    </div>
                                    <div class="mb-2 d-flex justify-content-between align-items-center" data-product-id="{{$product->id}}">
                                        <a onclick="decrement('{{$product->id}}', '{{$product->title}}', '{{$product->getPrice()}}');" class="cartQtyDecrement btn-article ml-0 py-1 px-3 btn-secondary btn-icon fs-xxl">
                                            -
                                        </a>
                                        <span id="cartQtyValue-{{$product->id}}" class="fs-l fw-500 ">
                                            {{$product->currentCartItem()->quantity}}
                                        </span>
                                        <a onclick="increment('{{$product->id}}', '{{$product->title}}', '{{$product->getPrice()}}');" class="cartQtyIncrement btn-article mr-0 py-1 px-3 btn-secondary btn-icon fs-xxl">
                                            +
                                        </a>
                                    </div>
                                    <div class="">
                                        <a class="btn-article btn-primary w-100" href="/basket">
                                            Finaliser
                                        </a>
                                    </div>
                                </div>
                            @else
                                <div class="container">
                                    <a id="btnCart-{{$product->id}}"
                                    data-p-id="{{$product->id}}" data-p-name="{{$product->title}}"
                                    data-p-price="{{$product->getPrice()}}" class="btn-article btn-secondary w-100  btnAddToCart invisible">
                                        Ajouter au panier
                                    </a>
                                    <div></div>
                                </div>
                            @endif
                        </div>
                    </a>
                </div>
            </div>
            {{-- @if($loop->count == ) --}}
        @empty
            <div style="position:relative; top: 10px; left:75px">
                <div class="d-flex justify-content-center align-items-center">
                    <div class="card card-body" style="width:500px;">
                        <center>
                            <img src="{{ asset('customer/images/gh1.png') }}" alt="Logo" width="180px">
                        </center>
                        <h6 class="text-center" style="color:#AAA"> Désolé Aucun resultat ne correspond à votre recherche</h6>
                    </div>
                </div>
            </div>
        @endforelse
    </div>
    <div class="d-flex justify-content-center mt-3">
        @if(count($products) > 0)
            {!! $products->appends($_GET)->links() !!}
        @endif
    </div>
</div>
