<!-- Product Listing grid -->
<div class="product-layot__grid">
    <ul class="row">
        <!-- product item 01 -->
        @forelse ($products as $product)
        <li class="col-md-6 col-lg-4">
            <div class="item">
                <figure>
                    <div class="intro-img">
                      @if($product->isPromoted())
                        <div class="new">
                          <span>{{"-".$product->promo_percentage."%"}}</span>
                        </div>
                      @endif
                        @if(!is_null($product->slug))
                        <a href="{{ route('shop.product_details', $product->slug) }}">
                            <img src="{{ $product->getImageUrl() ?? asset('customer/images/8513471.jpg') }}" alt="img dis" height="200" class="fill">
                        </a>
                        @else
                            <img src="{{ $product->getImageUrl() ?? asset('customer/images/8513471.jpg') }}" alt="img dis" height="200" class="fill">
                        @endif
                        <div class="action">
                            <ul>
                                <li>
                                    @if(!is_null($product->slug))
                                        <a href="{{ route('shop.product_details', $product->slug) }}"><i class="flaticon-search"></i></a>
                                    @else
                                        <i class="flaticon-search"></i>
                                    @endif
                                </li>
                                <!--li>
                                    <a href="#"><i class="flaticon-heart"></i></a>
                                </li-->
                                @if(is_null($product->stock))
                                <li>
                                  <a id="btnCart-{{$product->id}}"
                                  data-p-id="{{$product->id}}" data-p-name="{{$product->title}}"
                                  data-p-price="{{$product->getPrice()}}" class="btnAddToCart"><i class="flaticon-shopping-cart"></i></a>
                                </li>
                                @elseif($product->stock > 0)
                                    <li>
                                        <a id="btnCart-{{$product->id}}"
                                        data-p-id="{{$product->id}}" data-p-name="{{$product->title}}"
                                        data-p-price="{{$product->getPrice()}}" class="btnAddToCart"><i class="flaticon-shopping-cart"></i></a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <figcaption>
                        <i class="ratings__{{$product->getRating()}}"></i>
                        <h4>
                            @if(!is_null($product->slug))
                                <a href="{{ route('shop.product_details', $product->slug) }}">{{ Str::limit($product->title, 50) }}</a>
                            @else
                                {{ Str::limit($product->title, 50) }}
                            @endif
                        </h4>
                        <div>
                            @if(!is_null($product->stock) && $product->stock == 0)
                            <span class="badge bg-danger" >En pupture de stock</span>
                            @endif
                        </div>
                        <div class="product-meta">
                            <div class="price">
                                <span class="normal">{{$product->getPrice()}} FCFA</span>
                                @if($product->isPromoted())
                                    <span class="strike">{{$product->original_price}} FCFA</span>
                                    {{--<span class="strike">{{$product->price}} FCFA</span>--}}
                                @endif
                            </div>
                            <div class="size-n-color">
                              <?php if($product->sizes != null) { $sizeProduct = explode(',', $product->sizes);} ?>
                              @if(isset($sizeProduct) && !empty($sizeProduct))
                                <div class="size">
                                    <ul>
                                      @foreach($sizeProduct as $size)
                                        <li><span>{{$size}}</span></li>
                                      @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if($product->getProductColor())
                                <div class="colors">
                                    <ul>
                                      @foreach ($product->getProductColor() as $color)
                                          <li><span data-color="{{ $color->code_color }}"></span></li>
                                      @endforeach
                                    </ul>
                                </div>
                                @endif
                            </div>
                        </div>
                    </figcaption>
                </figure>
            </div>
        </li>

        @empty
            <div class="col-md-12" style="position:relative; top: 10px; left:10px">
                <div class="d-flex justify-content-center align-items-center">
                    <div class="card card-body" style="width:300px;">
                        <center>
                            <img src="{{ asset('customer/images/gh1.png') }}" alt="Logo" width="180px">
                        </center>
                        <h6 class="text-center" style="color:#AAA"> Désolé Aucun resultat ne correspond à votre recherche</h6>
                    </div>
                </div>
            </div>
        @endforelse
    </ul>

</div>
