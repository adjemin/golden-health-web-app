@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
<link media="all" rel="stylesheet" href="{{asset('css/plugins.css')}}">
<!-- Custom css -->
<link media="all" rel="stylesheet" href="{{asset('css/shop.css')}}">
@endsection

@section('content')

<main class="container-fluid py-5">
    <section class="">
        {{-- Breadcrumb --}}
        <ul class="breadcrumb">
            <li><a href="/">Accueil</a></li>
            <li><a href="/shop">Boutique</a></li>
            {{--<li><a href="/tous_les_articles/">Catégorie</a></li>--}}
            <li><a>{{$product->title}}</a></li>
        </ul>
        @csrf
    </section>
    <!-- Content -->
    <div class="content common-box py-3">
      <div class="container">
        @include('flash::message')
        <div class="row" id="main-content">
          <div class="col-lg-6">
            <div class="sticky-sidebar">
               {{ csrf_field() }}
              <div class="product-banner-module common-module sticky-sidebar-inner">
                @if($product->hasMultipleImages())
                <div class="swiper-carousel-outer">
                  <div class="product-banner">
                    <div class="swiper-wrapper">
                      <!-- carousel item 01 -->
                      @foreach($product->imageData() as $image)
                          <div class="swiper-slide">
                            <div class="item">
                              <figure class="zoom">
                                <img src="{{$image}}" alt="">
                              </figure>
                            </div>
                          </div>
                      @endforeach
                    </div>
                  </div>
                </div>

                <div class="swiper-carousel-outer">
                  <div class="product-thumbs">
                    <div class="swiper-wrapper">
                      @foreach($product->imageData() as $image)
                          <div class="swiper-slide">
                            <div class="item">
                              <figure>
                                <img src="{{$image}}" alt="" height="100">
                              </figure>
                            </div>
                          </div>
                      @endforeach
                    </div>
                  </div>
                </div>
                @else
                <div class="swiper-carousel-outer">
                  <div class="product-banner">
                    <div class="swiper-wrapper">
                      <!-- carousel item 01 -->
                      <div class="swiper-slide">
                        <div class="item">
                          <figure class="zoom">
                            <img src="{{$product->getImageUrl() }}" alt="" height="100">
                          </figure>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @endif

              </div>


            </div>
          </div>
          <div class="col-lg-6">
            <div class="product-content pl-lg-3">
              <div class="product-detail-module common-module">
                <div class="slot product-title">
                  <h1>{{$product->title}}</h1>
                  <span class="review"><i class="ratings__{{$product->getRating()}}"></i></span>
                </div>
                <div class="slot price">
                  <span class="normal">{{$product->getPrice()}} FCFA</span>
                  @if($product->isPromoted())
                      <span class="strike">
                          {{--$product->price--}} {{$product->original_price}} FCFA
                      </span>
                  @endif
                </div>
                <div class="slot">
                  @if($product->hasColors())
                  <div class="slot-row">
                    <div class="slot-col">
                      <label>Couleur</label>
                    </div>
                    <div class="slot-col colors">
                      @if($product->getProductColor())
                        <ul>
                          @foreach($product->getProductColor() as $key => $color)
                            <li class="myColor {{$key == 0 ? 'active' : ''}}" data-id="{{$color->name}}" data-action="checked"><span data-color="{{$color->name}}"></span></li>
                          @endforeach
                        </ul>
                      @endif
                      <input type="hidden" name="color_input" id="color_input" value="{{$product->getProductColor()[0]->name}}" />
                    </div>
                  </div>
                  @endif
                  @if($product->hasSizes())
                  <div class="slot-row">
                    <div class="slot-col">
                      <label>Taille</label>
                    </div>
                    <div class="slot-col">
                      <div class="custom_select">
                        <select class="form-control" name="taille" id="taille" style="padding-right:30px;">
                          <option value="">Sélectionnez la taille</option>
                          @foreach($product->sizes() as $size)
                          <option value="{{$size}}">{{$size}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                  @endif
                  <div class="slot-row">
                    <div class="slot-col">
                      <label>Quantité</label>
                    </div>
                    <div class="slot-col">
                      <input type="number" name="quantity" class="form-control" value="1" min="1" max="{!! $product->stock == 0 || $product->stock == null ? ($product->initial_count == 0 || $product->initial_count == null ? 0 : $product->initial_count ) : $product->stock  !!}"
                        data-input-type="incrementer" id="people" readonly />
                    </div>
                  </div>
                </div>
                <div class="slot">
                  @if(!is_null($product->stock) && $product->stock == 0)
                  <p style="color:red;">Ce produit est en rupture de stock, vous pouvez entrer vos coordonnées et vous serez contacté dès que le réapprovisionnement sera effectué.</p>
                  @endif
                  <div class="slot-row">
                    <p  id="getMessage" class="text-danger" style="" >Veuillez choisir votre taille avant l'ajout du produit au panier svp !</p>
                    @if(is_null($product->stock) || $product->stock > 0 )
                    <div class="slot-col">
                      <button  class="btn btn-secondary btn-md rounded-pill addToCart" data-p-id="{{$product->id}}" data-p-name="{{$product->title}}" data-p-price="{{$product->getPrice()}}" {!! !$product->hasSizes() ? "" : "disabled" !!}><i class="flaticon-shopping-cart"></i> Ajouter au panier</button>
                    </div>
                    @else
                    <div class="slot-col">
                      <button class="btn btn-secondary btn-md rounded-pill " data-toggle="modal" data-target="#exampleModal" ><i class="flaticon-contact"></i> Passer votre réservation</button>
                    </div>
                    @endif
                    <div class="slot-col">
                      {!! $product->wishlistWidget() !!}
                      <!-- <button class="btn btn-link"><i class="flaticon-heart"></i> Ajouter aux favoris</button> -->
                    </div>
                    <div class="slot-col mt-3">
                      <a href="{{url('/commentaires/'.$product->slug)}}" class="btn btn-primary btn-md rounded-pill"><i class="fa fa-star"></i> Les avis clients</a>
                    </div>
                  </div>

                </div>
              </div>

              <article class="common-module">
                <div class="module-title">
                  <h2>Description</h2>
                </div>
                <p>{!!$product->description!!}</p>
              </article>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--//-End Content -->

    <!-- Best Selling Products -->
    <section class="common-box product-carousel-section pt-0">
      <div class="container-fluid">
        <div class="title text-center">
          <h2>Articles Similaires</h2>
        </div>

        <div class="swiper-carousel-outer" data-carousel="swiper" data-items="5" data-space-between="30"
          data-autoplay="5000" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-lg="4">
          <div class="product-layot__grid" data-swiper="container">
             <div class="swiper-wrapper">
               @foreach($product->getSimilarProducts() as $similar_product)
                @include('customer_frontOffice.shops.components.similar_product', ['product' => $similar_product])
               @endforeach
             </div>
          </div>
        </div>
      </div>
    </section>

</main>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Passer votre réservation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ url('/product-reservation') }}">
          @csrf
          <div class="form-group  mt-3">
            <label for="name">Nom & Prénom(s) <span style="color:red">*</span></label>
            <input type="text" name="customer_name" required class="form-control input-style placeholder-style" id="name"  placeholder="Entrer votre nom et prénom" value="{!!  Auth::guard('customer')->user() !== null ? Auth::guard('customer')->user()->name : '' !!}"/>
            
        </div>
        <div class="form-group mt-3">
            <label for="email">Email <span style="color:red">*</span></label>
            <input type="tel" name="customer_email" required class="form-control input-style placeholder-style" id="email" placeholder="Entrer email" value="{!!  Auth::guard('customer')->user() !== null ? Auth::guard('customer')->user()->email : '' !!}"/>
        </div>
        <div class="form-group mt-3">
          <label for="telephone">Numero de téléphone <span style="color:red">*</span></label>
          <input type="text" name="customer_phone" required  class="form-control input-style placeholder-style" id="telephone" placeholder="Entrer numero de telephone" value="{!!  Auth::guard('customer')->user() !== null ? Auth::guard('customer')->user()->phone : '' !!}"/>
      </div>
      <div class="form-group mt-3">
          <label for="quantite">Quantité <span style="color:red">*</span></label>
          <input type="number" name="product_quantity" required  class="form-control input-style placeholder-style" id="quantite" min='1' placeholder="Entrer la quantité" value="1"/>
      </div>
      <input type="hidden" value="{{ $product->id }}" name="product_id" >
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-primary">Réserver</button>
      </div>
      </form>
      </div>
      
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/plugins.js')}}"></script>
<script src="{{asset('js/shop-init.js')}}"></script>

<script>
    // Radio color inputs
    var colorSpans = document.getElementsByClassName('span-color');
    for(var c = 0; c < colorSpans.length; c++){

        colorSpans[c].style.background = colorSpans[c].dataset.color;
    }
</script>
@if($product->hasSizes())
<script>
  $('#taille').on('change', function(){
    if ($(this).val() != '') {
      $('.addToCart').removeAttr('disabled');
      console.log($('#getMessage'))
      note = document.querySelector('#getMessage');
      note.style.display="none"
      // $('#getMessage').addStyle.display="block";
      // $('#getMessage').addStyle.color="red";
    } else {
      $('.addToCart').attr('disabled', 'disabled');
      $('#getMessage').removeAttr('style');
      console.log("Good moning")
    }
  });
</script>
@endif
<script>
      $(document).on('click', "#AddToCard", function(){
        console.log("Ohhhhh ok")
        $('#getMessage').style.display = 'block'
        $('#getMessage').style.color = 'red'
      })
    function ajaxCart(cartData, action = "manage") {
        if (cartData == null) {
            console.log("=== Invalid data");
            return 0;
        }
        console.log("=== Saving to cart");
        console.log(cartData);

        $.ajax({
            type: "POST",
            url: "/ajax/cart/" + action,
            data: {
                '_token': $("input[name='_token']").val(),
                'product_id': cartData.pId,
                'product_name': cartData.pName,
                'product_quantity': cartData.pQuantity,
                'product_price': cartData.pPrice,
                'product_size': cartData.size,
                'product_color': cartData.detail
            },
            dataType: "json",
            success: function(response) {
                if (response.status != "OK") {

                    if (response.status == "REMOVED") {
                        showSnackbar("Produit Retiré", "bg-danger");
                        $(".cart-badge").html(response.data.itemCount);
                        return false;
                    }
                    console.log(">>>> errerur");
                    showSnackbar("Erreur, Veuillez réessayer", "bg-danger", 4000);
                    console.log(response);
                    return false;
                }

                // showSnackbar("Erreur, Veuillez réessayer", "bg-danger", 4000);
                console.log(">>>> SUCCESS  ");
                console.log(response);
                $(".cart-badge").html(response.data.itemCount);
                // window.location.reload();
            },
            error: function(response) {
                console.log("Error :");
                console.log(response);
            },
        });
    }


    function checkedColor(){
      var checkedColor = $(this);
      var color = checkedColor.data('id');
      $('#color_input').val(color);
      //$(".selectedColor").removeClass('class');
      $('body').find('.myColor').removeClass('active');
      checkedColor.addClass('active');
      showSnackbar('couleur '+ color)
    }

    $(function(){
      $(document).on('click', ".btnAddToCart", function() {

      var pId = $(this).data('p-id');
      var pName = $(this).data('p-name');
      var pPrice = $(this).data('p-price');

      $(this).removeClass('invisible');

      // $(this).load('/ajax/x/s/c/cart_quantity');

      var qqty = `
          <div id="qtyController-${pId}">
              <div class="text-center text-secondary fs-l fw-500">
                  <span id="cartItemAmount-${pId}">
                      ${pPrice}
                  </span>
                  <span>FCFA</span>
              </div>
              <div class="mb-2 d-flex justify-content-between align-items-center" data-product-id="${pId}">
                  <a onclick="decrement('${pId}', '${pName}', '${pPrice}');" class="cartQtyDecrement btn-article ml-0 py-1 px-3 btn-secondary btn-icon fs-xxl">
                      -
                  </a>
                  <span id="cartQtyValue-${pId}" class="fs-l fw-500">
                      1
                  </span>
                  <a onclick="increment('${pId}', '${pName}', '${pPrice}');" class="cartQtyIncrement btn-article mr-0 py-1 px-3 btn-secondary btn-icon fs-xxl">
                      +
                  </a>
              </div>
              <div class="">
                  <a class="btn-article btn-primary w-100" href="/basket">
                      Finaliser
                  </a>
              </div>
          </div>
      `;
      $(this).next().html(qqty);

      $(this).removeClass('btn-article');
      $(this).removeClass('btnAddToCart');
      $(this).empty();
      ajaxCart({
          pId: pId,
          pName: pName,
          pQuantity: 1,
          pPrice: pPrice,
      });
      showSnackbar('Produit Ajouté');

      });

      $('[data-action="checked"]').click(checkedColor);
      $('#people').on('change',function(){
        var min = $(this).attr('min'),
        max = $(this).attr('max'),
        current = $(this).val()
        if(current > max){
          showSnackbar("Le stock disponible est "+ max, 'bg-danger');
        }else if(current < min){
          showSnackbar("Quantité inférieure à "+ min, 'bg-danger');
        }
      });

      var color = $('#color_input').val();
      if(color == "" || color == undefined){
        showSnackbar("Selectionner une couleur", 'bg-danger');
      }else{
      $('.addToCart').click(function(){
        var detailValue = $('#color_input').val();
        var size = $('#taille').val();
        var quantity = $('#people').val();
        var pId = $(this).data('p-id');
        var pName = $(this).data('p-name');
        var pPrice = $(this).data('p-price');

        ajaxCart({
            pId: pId,
            pName: pName,
            pQuantity: parseInt(quantity),
            size: size,
            detail: detailValue,
            pPrice: pPrice
        });
        showSnackbar('Produit ajouté au panier');
      });
      }
    });
    // Add size to article
</script>
@endsection
