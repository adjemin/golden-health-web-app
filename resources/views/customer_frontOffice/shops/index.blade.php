

@extends("customer_frontOffice.layouts_customer.master_customer")
@section('css')
    <link media="all" rel="stylesheet" href="{{asset('css/plugins.css')}}">
    <!-- Custom css -->
    <link media="all" rel="stylesheet" href="{{asset('css/shop.css')}}">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap"
        rel="stylesheet">
    <style>
        :root {
            --background: #eee;
            /* --foreground: #fff; */
            --foreground: #d3d3d3;
            --foreground: #fff;
            --foreground-l: #e6e6e6;
            --primary: #1c6a49;
            --primary-dark: #144e36;
            --secondary: #edaa0d;
            --secondary-dark: #cf940a;
            --br: 4px;
        }

        * {
            box-sizing: border-box;
        }

        body {
            /* font-size: 13px; */
            /* font-size: .875rem; */
            font-size: .625rem;
            /* background-color: rgba(247, 247, 247, 0.973); */
            /*background-color: var(--background);*/
            font-weight: 500;
        }

        .b-fg {
            background-color: var(--foreground);
        }

        .b-fgl {
            background-color: var(--foreground-l);
        }

        .b-bg {
            background-color: var(--background);
        }

        .breadcrumb {

            background-color: var(--foreground) !important;
        }

        .r {
            border-radius: var(--br);
        }

        .fs-xs {
            font-size: .625rem !important;
        }

        .fs-s {
            font-size: .75rem !important;
        }

        .fs-m,
        p {
            font-size: .875rem !important;
        }

        .fs-l {
            font-size: 1rem !important;
        }

        a {
            font-size: .75rem;
            color: black;
        }

        ,
        a:hover {
            color: currentColor !important;
        }

        .item .intro-img .new {
            position: absolute;
            top: 20px;
            right: 20px;
            -webkit-transition: var(--transition);
            transition: var(--transition);
        }
        .item .intro-img .new span {
            padding: 5px 10px;
            border: 1px solid var(--secondary);
            font-family: var(--optionalfontFamily);
            color: #fff;
            -webkit-transition: var(--transition);
            transition: var(--transition);
            background-color: var(--secondary);
            font-size: 12px;
        }

        /* Start Range price css */
        .slider {
            -webkit-appearance: none;
            width: 100%;
            height: 15px;
            border-radius: 5px;  
            background: #d3d3d3;
            outline: none;
            opacity: 0.7;
            -webkit-transition: .2s;
            transition: opacity .2s;
        }

        .slider::-webkit-slider-thumb {
            -webkit-appearance: none;
            appearance: none;
            width: 25px;
            height: 25px;
            border-radius: 50%; 
            background: #edaa0d;
            cursor: pointer;
        }

        .slider::-moz-range-thumb {
            width: 25px;
            height: 25px;
            border-radius: 50%;
            background: #edaa0d;
            cursor: pointer;
        }

        /* End Range price css */

    </style>
@endsection

@section('content')
    <main class="container-fluid">
        <section class="pt-3">
                <div class="text-center p-md-5 p-2 r b-fg">
                    <h1 class="font-weight-bold text-center primary-text-color mb-3 text-uppercase">Boutique</h1>
                    <div>
                        <p class="font-weight-bold shop-name text-center p-0">Bien s'équiper, c'est aussi mieux s'exercer
                        </p>
                    </div>
                </div>
        </section>
        <section>
                {{-- Breadcrumb --}}
                <ul class="breadcrumb">

                    <li><a href="/">Accueil</a></li>
                    <li><a href="/shop">Boutique</a></li>
                    {{-- <li><a href="/tous_les_articles/">Tous les articles</a></li>
                    --}}
                </ul>
                @csrf
        </section>
        <!-- contents -->
        <hr>
        <!-- Content -->
        <div class="content common-box" role="main">
            <div class="container-fluid">
                <div class="row flex-lg-row-reverse">
                  @if(isset($ads))
                      @if($ads->count() > 0)
                          <aside class="col-lg-2">
                              <div class="sidebar sidebar-right">
                                <h4 class="module-title">Publicités</h4>
                                @foreach ($ads as $ad)
                                    <div class="mb-2">
                                        @if($ad->file_type == "image")
                                        <a target="_blank" href="{{ $ad->redirect_to }}">
                                            <div class="img-container">
                                                <img src="{{ $ad->cover_url ?? asset('customer/images/860b5c3.jpg') }}"
                                                    class="img-fluid" alt="">
                                            </div>
                                            <div class="px-2 py-1">
                                                <h6 class="secondary-text-color">
                                                    <b>{{ $ad->name }}</b>
                                                </h6>
                                                <p>{!! Str::limit($ad->description, 50) !!}</p>
                                            </div>
                                        </a>
                                        @else
                                            <div class="embed-responsive embed-responsive-21by9 embed-responsive-16by9 embed-responsive-4by3 embed-responsive-1by1">
                                                <iframe class="embed-responsive-item" src="{{$ad->cover_url ?? asset('customer/images/860b5c3.jpg') }}"></iframe>
                                            </div>
                                            <a target="_blank" href="{{ $ad->redirect_to ?? '#' }}">
                                                <div class="px-2 py-1">
                                                    <h6 class="secondary-text-color">
                                                        <b>{{ $ad->name }}</b>
                                                    </h6>
                                                    <p>{!! Str::limit($ad->description, 50) !!}</p>
                                                </div>
                                            </a>
                                        @endif
                                        <hr>
                                    </div>
                                @endforeach
                              </div>
                          </aside>
                          @endif
                      @endif
                    <div class="@if(isset($ads)) @if($ads->count() > 0) col-xl-7 @endif @endif  col-md-9 col-sm-12">
                        {{--
                          <div class="common-module sorting-module">
                              <div class="module-content">
                                  <div class="content-left">
                                      <h3><span class="font-weight-bold">1800</span> items found</h3>
                                  </div>
                                  <div class="content-right">
                                      <div class="sort-by">
                                          <label for="sort-by">Sort By:</label>
                                          <select class="form-control">
                                              <option value="popularity">Popularity</option>
                                              <option value="">Price Low to High</option>
                                              <option value="">Price High to Low</option>
                                          </select>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          --}}

                        @include('customer_frontOffice.shops.components.products', ['products' => $products])

                        <nav class="custom-pagination mt-4">
                          @if(count($products) > 0)
                              {!! $products->appends($_GET)->links() !!}
                          @endif
                        </nav>
                    </div>

                    <aside class="col-lg-3">
                        <div class="sidebar sidebar-left">
                            <div class="sidebar-module sidebar-nav">
                                <h4 class="module-title">Trier par prix</h4>
                            
                                    <form class="multi-range-field my-2">
                                        <input id="multiple" class="slider filter-option" value="{{ $max_price }}" step='1000' type="range" min="1000" max="300000" />
                                    </form>
                                    <div>Prix Max. : <span id="montant_max"></span> FCFA</div>
                            </div>
                            <div class="sidebar-module sidebar-nav">
                                <h4 class="module-title">Catégories</h4>
                                @php
                                if(array_key_exists('categories', $_GET)){
                                $selected = explode(',', $_GET['categories']) ?? [];
                                }else{
                                $selected = [];
                                }
                                @endphp

                                @foreach ($categories as $category)
                                  <div class="custom-control custom-checkbox">
                                      <input type="checkbox" class="custom-control-input filter" name="filter[]" id="{{ $category->name }}" value="{{ $category->name }}" {!! in_array($category->name, $selected ?? []) ? 'checked' : '' !!}>
                                      <label class="custom-control-label" for="{{ $category->name }}">{{ $category->name }}</label>
                                  </div>
                                @endforeach
                            </div>
                            <div class="sidebar-module sidebar-nav">
                                <h4 class="module-title">Sexe</h4>
                                <div class="custom-control custom-checkbox">
                                    <input type="radio" class="custom-control-input filter" name="gender" id="Unisexe" value="Unisexe" {!! isset($_GET['gender']) && !empty($_GET['gender']) && $_GET['gender'] == 'Unisexe' ? 'checked' : '' !!}>
                                    <label class="custom-control-label" for="Unisexe">Unisexe</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="radio" class="custom-control-input filter" name="gender" id="Homme" value="Homme" {!! isset($_GET['gender']) && !empty($_GET['gender']) && $_GET['gender'] == 'Homme' ? 'checked' : '' !!}>
                                    <label class="custom-control-label" for="Homme">Homme</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="radio" class="custom-control-input filter" name="gender" id="Femme" value="Femme" {!! isset($_GET['gender']) && !empty($_GET['gender']) && $_GET['gender'] == 'Femme' ? 'checked' : '' !!}>
                                    <label class="custom-control-label" for="Femme">Femme</label>
                                </div>
                            </div>
                            {{--
                              <div class="sidebar-module">
                                  <h4 class="module-title">Prix</h4>
                                  <div class="range-slider primary-skin">
                                      <input id="budget_range" type="text" value="" />
                                  </div>
                              </div>
                              --}}

                            {{--
                              <div class="sidebar-module sidebar-nav">
                                  <h4 class="module-title">Rating</h4>
                                  <ul>
                                      <li>
                                          <a href="#"><i class="ratings__5" data-width="100%"></i>Excellent
                                              <span>(10)</span></a>
                                          </i>
                                      <li>
                                          <a href="#"><i class="ratings__4" data-width="80%"></i>Good
                                              <span>(09)</span></a>
                                      </li>
                                      <li>
                                          <a href="#"><i class="ratings__3" data-width="60%"></i>Average
                                              <span>(07)</span></a>
                                      </li>
                                      <li>
                                          <a href="#"><i class="ratings__2" data-width="40%"></i>Rather Poor
                                              <span>(04)</span></a>
                                      </li>
                                      <li>
                                          <a href="#"><i class="ratings__1" data-width="20%"></i>Bad
                                              <span>(20)</span></a>
                                      </li>
                                  </ul>
                              </div>
                              --}}

                        </div>
                    </aside>

                </div>
            </div>
        </div>
        <!-- //-End Content -->
    </main>
@endsection


@section('scripts')
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/shop-init.js')}}"></script>

    <script>
        $token = $("input[name='_token']").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $token
            }
        });

    </script>
    {{-- --}}
    <script>
        big = $('#product_views')
        
        $(".filter").click(function() {
            categories = []
            $('input[name="filter[]"]:checked').each(function() {
                categories.push($(this).val());
            });
            var price = $('#montant_max').text()
            gender = $('input[name="gender"]:checked').val() != undefined ? $('input[name="gender"]:checked').val() : "";
            console.log(categories);
            window.location.href = "/shop?max-price="+price+"&categories=" + categories.join(',')+"&gender="+gender
            
        });
        $(".filter-option").change(function() {
            categories = []
            $('input[name="filter[]"]:checked').each(function() {
                categories.push($(this).val());
            });
            var price = $('#montant_max').text()
            gender = $('input[name="gender"]:checked').val() != undefined ? $('input[name="gender"]:checked').val() : "";
            console.log(categories);
            window.location.href = "/shop?max-price="+price+"&categories=" + categories.join(',')+"&gender="+gender
            
        });




        // Just add item to cart then can modify it in the basket
        $(document).on('click', ".btnAddToCart", function() {

            var pId = $(this).data('p-id');
            var pName = $(this).data('p-name');
            var pPrice = $(this).data('p-price');

            $(this).removeClass('invisible');

            // $(this).load('/ajax/x/s/c/cart_quantity');

            var qqty = `
                <div id="qtyController-${pId}">
                    <div class="text-center text-secondary fs-l fw-500">
                        <span id="cartItemAmount-${pId}">
                            ${pPrice}
                        </span>
                        <span>FCFA</span>
                    </div>
                    <div class="mb-2 d-flex justify-content-between align-items-center" data-product-id="${pId}">
                        <a onclick="decrement('${pId}', '${pName}', '${pPrice}');" class="cartQtyDecrement btn-article ml-0 py-1 px-3 btn-secondary btn-icon fs-xxl">
                            -
                        </a>
                        <span id="cartQtyValue-${pId}" class="fs-l fw-500">
                            1
                        </span>
                        <a onclick="increment('${pId}', '${pName}', '${pPrice}');" class="cartQtyIncrement btn-article mr-0 py-1 px-3 btn-secondary btn-icon fs-xxl">
                            +
                        </a>
                    </div>
                    <div class="">
                        <a class="btn-article btn-primary w-100" href="/basket">
                            Finaliser
                        </a>
                    </div>
                </div>
            `;
            $(this).next().html(qqty);

            $(this).removeClass('btn-article');
            $(this).removeClass('btnAddToCart');
            $(this).empty();
            ajaxCart({
                pId: pId,
                pName: pName,
                pQuantity: 1,
                pPrice: pPrice,
            });
            showSnackbar('Produit Ajouté');

        });

        // Modal to choose size and color
        // $(document).on('click', ".btnAddToCart", function() {
        //     var pId = $(this).data('p-id');
        //     var pName = $(this).data('p-name');
        //     var pPrice = $(this).data('p-price');

        //     openModal(pId);
        //     console.log(">>opening" +pId);
        //     return false;
        // });

        function decrement(id, name, price) {
            console.log("dec");

            qty = parseInt($("#cartQtyValue-" + id).html());

            if (qty > 0) {
                qty--;
                $("#cartQtyValue-" + id).html(qty);
                $("#cartItemAmount-" + id).html((qty) * parseInt(price));

                ajaxCart({
                    pId: id,
                    pName: name,
                    pQuantity: qty,
                    pPrice: price,
                });

                if (qty == 0) {
                    $("#qtyController-" + id).empty();
                    $("#btnCart-" + id).html('Ajouter au panier');
                    $("#btnCart-" + id).addClass('btn-article btn-secondary w-100  btnAddToCart');

                    showSnackbar("Produit Retiré", "bg-danger");

                    setTimeout(() => {
                        $("#btnCart-" + id).addClass('invisible');
                    }, 3000);
                    return false;
                }
                // showSnackbar("Produit Modifi", "bg-danger");

            }


        }

        function increment(id, name, price) {
            console.log("inc");

            qty = parseInt($("#cartQtyValue-" + id).html()) + 1;
            $("#cartQtyValue-" + id).html(qty);
            $("#cartItemAmount-" + id).html(qty * parseInt(price));

            ajaxCart({
                pId: id,
                pName: name,
                pQuantity: qty,
                pPrice: price,
            });
        }

        function ajaxCart(cartData, action = "manage") {
            if (cartData == null) {
                console.log("=== Invalid data");
                return 0;
            }
            console.log("=== Saving to cart");
            console.log(cartData);

            $.ajax({
                type: "POST",
                url: "/ajax/cart/" + action,
                data: {
                    'product_id': cartData.pId,
                    'product_name': cartData.pName,
                    'product_quantity': cartData.pQuantity,
                    'product_price': cartData.pPrice,
                },
                dataType: "json",
                success: function(response) {
                    if (response.status != "OK") {

                        if (response.status == "REMOVED") {
                            showSnackbar("Produit Retiré", "bg-danger");
                            $(".cart-badge").html(response.data.itemCount);
                            return false;
                        }
                        console.log(">>>> errerur");
                        showSnackbar("Erreur, Veuillez réessayer", "bg-danger", 4000);
                        console.log(response);
                        return false;
                    }

                    // showSnackbar("Erreur, Veuillez réessayer", "bg-danger", 4000);
                    console.log(">>>> SUCCESS  ");
                    console.log(response);
                    $(".cart-badge").html(response.data.itemCount);
                    // window.location.reload();
                },
                error: function(response) {
                    console.log("Error :");
                    console.log(response);
                },
            });
        }

    </script>

    {{-- Price range js --}}
    <script>
        $('#multiple').change(function(){
            console.log($('#multiple').val())
        })
        var slider = document.getElementById("multiple");
        var output = document.getElementById("montant_max");
        output.innerHTML = slider.value;
        slider.oninput = function() {
            output.innerHTML = this.value;
        }
    </script>
@endsection
