@extends("customer_frontOffice.layouts_customer.master_customer")
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
    <style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }

    a {
        color: black;
    }
    .datepicker {
    background-color:#edaa0d !important;
    color: #fff !important;
    border: none;
    padding: 10px !important
}

.datepicker-dropdown:after {
    border-bottom: 6px solid #edaa0d
}

thead tr:nth-child(3) th {
    color: #fff !important;
    font-weight: bold;
    padding-top: 20px;
    padding-bottom: 10px
}

.dow,
.old-day,
.day,
.new-day {
    width: 40px !important;
    height: 40px !important;
    border-radius: 0px !important
}

.old-day:hover,
.day:hover,
.new-day:hover,
.month:hover,
.year:hover,
.decade:hover,
.century:hover {
    border-radius: 6px !important;
    background-color: #eee;
    color: #edaa0d;
}

.active {
    border-radius: 6px !important;
    background-image: linear-gradient(#90CAF9, #64B5F6) !important;
    color: #edaa0d !important
}

.disabled {
    color: #616161 !important
}

.prev,
.next,
.datepicker-switch {
    border-radius: 0 !important;
    padding: 20px 10px !important;
    text-transform: uppercase;
    font-size: 20px;
    color: #fff !important;
    opacity: 0.8
}

.prev:hover,
.next:hover,
.datepicker-switch:hover {
    background-color: inherit !important;
    opacity: 1
}

.cell {
    border: 1px solid #BDBDBD;
    border-radius: 5px;
    margin: 2px;
    cursor: pointer
}

.cell:hover {
    border: 1px solid #1c6a49;
}

.cell.select {
    background-color: #edaa0d;
    color: #fff
}

.fa-calendar {
    color: #fff;
    font-size: 30px;
    padding-top: 8px;
    padding-left: 5px;
    cursor: pointer
}

#price_total {
    display: block;
    font-weight: bold;
    margin: 5px;
}


.fc-header-toolbar {
  /*
  the calendar will be butting up against the edges,
  but let's scoot in the header's buttons
  */
  padding-top: 1em;
  padding-left: 1em;
  padding-right: 1em;
}
@media (min-width: 576px) {
  .modal-dialog {
      max-width: 1024px;
  }
}

#DateModal{
  display:none;
}
.hide{
  display: none;
}
.closepopover{
  margin-right: 10px;
}
</style>
<script src="{{ asset('customer/moment/moment.min.js') }}"></script>
<script src="{{ asset('customer/moment/moment-with-locales.js') }}"></script>
@endsection
@section('content')
<section>
    <div class="container py-3">
        <div class="coach-identity">
            <div>
                <!--  Avis clients -->
                <div class="row mt-5 mb-2">
                    <div>
                        <h4 class="primary-text-color font-weight-bold mb-2"> Avis clients</h4>
                    </div>
                    </div class="float-right;">
                        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">ajouter un avis sur ce produit</button>
                    </div>
                    
                </div>
                @php
                    $i = 0;
                @endphp
                
                @if($reviews)
                  @foreach($reviews as $review)
                    @if($review && isset($review->customer))
                    @php
                        $i++;
                    @endphp
                
                    <div class="row mt-5 ">
                        <div class="col-lg-2 col-md-2 col-sm-12">
                            <img src="{{$review->customer->photo_url}}" height="80" width="80" class="rounded-circle "
                                alt="">
                        </div>

                        <div class="col-lg-10 col-md-10 col-sm-12">
                            <p class="font-weight-bold">{{$review->customer->name ?? "Inconnu"}} , 
                                <script>
                                    moment.locale('fr')
                                    var newmoment = moment("{{$review->created_at}}", "YYYY-MM-DD hh:mm:ss").fromNow();
                                    document.write(newmoment)
                                </script> 
                                {{-- {{$review->created_at}}  --}}
                                </p>
                            <div class="mb-3">
                              @for ($i = 0; $i < $review->rating; $i++)
                                <img src="{{ asset('customer/images/stars.png')}}" class="icon" alt="">
                              @endfor
                            </div>
                            <div>
                                <p>
                                  {{$review->content ?? ''}}
                                </p>
                            </div>
                        </div>
                    </div>
                    @endif
                  @endforeach
                @endif
                @if($i == 0)
                    <h1>Aucun avis pour ce produit</h1>
                @endif
                <hr>
            </div>
        </div>
    </div>
    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ajouter un nouveau avis</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <form action="{{ route('customer.reviews.store') }}" method="POST">
            @csrf
            <div class="form-group  mt-3">
                <label for="content">Votre avis sur le produit <b>{{ $product->title }}</b></label>
                <textarea name="content" required id="content" class="form-control"  ></textarea>
            </div>
            <div class="form-group  mt-3">
                <label for="rating">Nombre d'étoiles (1-4) </label>
                <input type="number" required min=1 max=4 name="rating" id="rating" class="form-control input-style placeholder-style"  placeholder="1-4" value='1' />
            </div>
            <input type="text" name="service" value="shop" hidden>
            <input type="text" name="source" value="product" hidden>
            <input type="text" name="title" value="{{ Auth::guard('customer')->user()->name }}" hidden>
            <input type="text" name="product_id" value="{{ $product->id }}" hidden>

            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button type="submit" class="btn btn-primary">Enregistrer</button>
         </form>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection