@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
<style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }
    a {
        color: black;
    }
    .no-display-input{
        /*  */
        border: none !important;
        display: inline-block;
    }
    .address{
        padding: 10px;
    }
    .address:hover{
        background-color: white;
    }
</style>
<style>
    /* Choose color and size */
    /* Color radio inputs */

    input[type=radio] {
    display: none;
    }
    input[type=radio]:checked + label span {
    transform: scale(1.25);
    border: 2px solid var(--secondary);
    }
    input[type=radio]:checked + label .red {
    border: 2px solid #fff;
    }
    input[type=radio]:checked + label .orange {
    border: 2px solid #873a08;
    }
    input[type=radio]:checked + label .yellow {
    border: 2px solid #816102;
    }
    input[type=radio]:checked + label .olive {
    border: 2px solid #505a0b;
    }
    input[type=radio]:checked + label .green {
    border: 2px solid #0e4e1d;
    }
    input[type=radio]:checked + label .teal {
    border: 2px solid #003633;
    }
    input[type=radio]:checked + label .blue {
    border: 2px solid #103f62;
    }
    input[type=radio]:checked + label .violet {
    border: 2px solid #321a64;
    }
    input[type=radio]:checked + label .purple {
    border: 2px solid #501962;
    }
    input[type=radio]:checked + label .pink {
    border: 2px solid #851554;
    }

    label {
        display: inline-block;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        margin-right: 10px;
        cursor: pointer;
    }
    label:hover span {
        transform: scale(1.25);
    }
    label span {
        display: block;
        border-radius: 50%;
        width: 100%;
        height: 100%;
        transition: transform 0.2s ease-in-out;
    }
</style>
<script src="https://cdn.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"/>
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>

@endsection

@section('content')
<main class="container">
      <section class="my-3">
        {{-- Breadcrumb --}}
        <ul class="breadcrumb">
            <li><a href="/">Accueil</a></li>
            <li><a href="/shop">Boutique</a></li>
            <li><a>Checkout</a></li>
        </ul>
        @csrf
    </section>

    <section class="py-5">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 bg-white r">
                <div class="p-3 m-3">
                    <h2 class="fs-title">Résumé commande</h2>
                    <div class="table-responsive table-full-width">
                        <table class="table table-hover table-striped table-adjemin">
                            <tbody class="text-left">
                                <tr>
                                    <th>Articles</th>
                                    <th>Quantité</th>
                                    <th>Prix Unitaire</th>
                                    <th>Montant</th>
                                </tr>
                                @foreach (App\ShoppingCart::ShoppingCart()->items as $item)
                                    <tr>
                                        <td class="" id="orderReviewItemTitle">
                                            {{$item->product->title}}
                                        </td>
                                        <td class="" id="orderReviewQuantity">
                                            {{$item->quantity}}
                                        </td>
                                        <td class="" id="orderReviewPrice">
                                            {{$item->price}}
                                        </td>
                                        <td class="" id="orderReviewAmount">
                                            {{$item->amount}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            {{-- Services info from $form_data --}}
                            <tfoot>
                                <tr >
                                    <td colspan="6" style="padding-top:50px;">
                                        <strong>
                                            Taxes
                                        </strong>
                                        <span id="orderReviewTotal" class="float-right">{{$order->delivery_fees ?? 0 }} Fcfa
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                    <strong>
                                        Moyen de paiment
                                    </strong>
                                        <span id="orderReviewTotal" class="float-right">{{$order->payment_method_slug}}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <strong>
                                            Total à payer
                                        </strong>
                                        <span id="orderReviewTotal" class="float-right">{{App\ShoppingCart::ShoppingCart()->getTotal()}}  Fcfa</span>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-left">
                            <p>
                            L'article sera livré à : <i id="orderReviewDeliveryAddress" style="text-decoration: underline">{{$order->location_name}}</i>
                            </p>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="transaction" data-method ="{{$transactionData['paymentMethod']}}" data-id ="{{$transactionData['id']}}" data-designation ="{{$transactionData['designation']}}" data-amount ="{{$transactionData['amount']}}" data-currency ="{{$transactionData['currency']}}" data-status ="{{$transactionData['status']}}" data-custom="{{isset($transactionData['customField']) ? $transactionData['customField'] : $transactionData['status'] }}">

                <div class="container" id="buttonHolder">
                    <h2 class="text-danger" id="result-message"></h2>
                    <div class="px-2 py-5">
                        @if(Auth::guard('customer')->check())
                            <a id="BtnAdjeminPay" class="btn-article w-100 text-center btn-secondary px-2">
                                <i class="fa fa-money mr-2" style="font-size: 20px;"></i> Payer {{$order->amount}} Fcfa
                            </a>
                        @else
                            <a href="{{route('customer.redirectTo', ['url' => Request::url()])}}" class="btn-article m-w-150 btn-secondary px-2">
                                <i class="fa fa-money mr-2" style="font-size: 20px;"></i> Inscrivez-vous pour finaliser la commande
                            </a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="p-3 mx-3 r bg-white">
                    <h2 class="fs-title">Vous avez un coupon ?</h2>
                    <div class="form-group">
                        <label for="my-input"></label>
                        <input id="my-input" class="form-control" type="text" name="coupon_code" placeholder="Entrez votre code">

                        <a id="submitCoupon" class="btn btn-article btn-primary w-100 mt-3">Vérifier</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection

@section('scripts')
<script>
    $token = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });
    var AdjeminPay = AdjeminPay();

    AdjeminPay.on('init', function (e) {
        // retourne une erreur au cas où votre API_KEY ou APPLICATION_ID est incorrecte
        console.log(e);
    });

    // Lance une requete ajax pour vérifier votre API_KEY et APPLICATION_ID et initie le paiement
    AdjeminPay.init({
        apikey: "eyJpdiI6Ik5ObTNmMTFtMFwvV2ZkU3RJS",
        application_id: "2f699e",
        notify_url: "https://goldenhealth.adjemincloud.com/api/invoice_payments_notify"
    });
</script>
<script>
    var $btn = $("#BtnAdjeminPay");
    // ***** Adjemin Pay
    $("#BtnAdjeminPay").click(function(){
        console.log(">>> clicked");
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;


        var btn = $(this);
        btn.html(_cuteLoader);

        // payWithAdjeminPay({
        //     amount: response.data.transaction.amount,
        //     transactionId: response.data.transaction.id,
        //     currency: response.data.transaction.currency,
        //     designation: response.data.transaction.designation,
        //     customField: response.data.status
        // });

        var transData = $("input[name='transaction']");

        var transaction = {
            amount: transData.data('amount'),
            transactionId: transData.data('id'),
            currency: transData.data('currency'),
            designation: transData.data('designation'),
            customField: transData.data('custom')
        }

        console.log(">>< trans data");
        console.log(transaction);

        payWithAdjeminPay({
            amount: transaction.amount,
            transactionId: transaction.transactionId,
            currency: transaction.currency,
            designation: transaction.designation,
            customField: transaction.customField
            // customField: "mtn-only"
        });

    });

    function payWithAdjeminPay(transactionData){
        if(!transactionData){
            alert(">>>> erreur paiement, veuillez réessayer");
            console.log("<< clicked");
        }
        console.log(">>> clicked");

        // Ecoute le feedback sur les erreurs
        AdjeminPay.on('error', function (e) {
            // la fonction que vous définirez ici sera exécutée en cas d'erreur
            console.log(e);
            $("#result-title").html(e.title);
            $("#result-message").html(e.data);
            $("#result-status").html(e.status);
            $("#BtnAdjeminPay").html("Vous allez être redirigé vers le panier");
            console.log(">>> Error");

            return false;
        });

        // Lancer la procédure de paiement au click
        AdjeminPay.preparePayment({
            amount: transactionData.amount,
            transaction_id: transactionData.transactionId,
            currency: transactionData.currency,
            designation: transactionData.designation,
            custom: transactionData.customField
        });

        // Si l'étape précédante n'a pas d'erreur,
        // cette ligne génère et affiche l'interface de paiement AdjeminPay
        AdjeminPay.renderPaymentView();


        // Payment terminé
        AdjeminPay.on('paymentTerminated', function (e) {
            console.log('<<<<<<< Terminated !');
            console.log(e);
            //
            // Ref and status
            adjeminPaymentNotify(transactionData.transactionId, e);
        });
    }

    //
</script>
{{-- AdjeminPay notification workaround --}}
<script>

    $token = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });
    //
    function adjeminPaymentNotify(reference, data){
        //
        if(reference == null){
            console.log("<<< REf null");
            return false;
        }
        if(data == null){
            console.log("<<< Data null");
            return false;
        }
        if(data.status == null){
            console.log("<<< Status null");
            return false;
        }
        var trans = {
            'id': reference,
            'status': data.status
        }

        console.log(">>> transData");
        console.log(trans);

        $.ajax({
                type: "POST",
                url: "{{ route('api.payments.notify') }}",
                data: {
                    "transaction_id": trans.id,
                    "status": trans.status,
                },
                dataType: "json",
                success: function (response) {
                    console.log("Response succes <<<");
                    console.log(response);
                    if(response.error != null){
                        $btn.html(response.error.message);
                    }

                    if(response.status != null){
                        if(response.status == "OK"){
                            // Correctly notified
                            // now redirect according to status
                            console.log(">>>>> PAYMENT SUCCESS");
                            window.location.replace('/customer/shop/checkout_finish/'+trans.id);
                        }
                    }
                    //
                    //
                    // btn.html("J'y participe <i class='material-icons ml-2'>check</i>");
                    // btn.removeClass('btn-secondary');
                    // btn.addClass('btn-primary');

                    // if(response.status != "ALREADY_EXISTS"){
                    //     $("#place_number_count").html($("#place_number_count").html()-1);
                    // }
                    // alert(document.URL+'#toReload');

                    // $('#toReload').load(document.URL+'#toReload');
                    // window.location.reload();
                    // this.classList.remove("markCompletedBtn");
                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                    $btn.html("Erreur");
                    setTimeout(() => {
                        $btn.html("Réessayer");
                    }, 2000);
                },
            });
    }
</script>

{{-- Color inputs css --}}
<script>
    // Radio color inputs
    var colorSpans = document.getElementsByClassName('span-color');
    for(var c = 0; c < colorSpans.length; c++){
        //
        colorSpans[c].style.background = colorSpans[c].dataset.color;
    }
</script>


@endsection
