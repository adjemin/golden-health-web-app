<div id="cartScreenModal" class="cart-modal">
    <span id="spanClose" class="cart-modal-close">&times;</span>
    {{-- <img class="cart-modal-content" id="cart-modal-img">
    <div id="cart-modal-caption"></div> --}}
    {{-- <div class="col-xl-6 col-lg-8 col-md-10 col-sm-12 col-12 mx-auto"> --}}

        @include('customer_frontOffice.shops.cart_screen_form')
    {{-- </div> --}}
</div>
<script>
    
    var modalScreen = document.getElementById("cartScreenModal");

// Get the <span> element that closes the modal
    // var span = document.getElementsByClassName("cart-screen-modal-close")[0];
    var span = document.getElementById("spanClose");
    // $(".cart-screen-modal-close").on('click', function({

    //     modalScreen.style.display = "none";
    // }));
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modalScreen.style.display = "none";
    }
</script>
