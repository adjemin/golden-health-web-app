@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
<style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
        font-family: 'Montserrat', sans-serif !important;
    }

    a {
        color: black;
    }
    .no-display-input{
        /*  */
        border: none !important;
        display: inline-block;
    }
    .address{
        padding: 10px;
    }
    .address:hover{
        background-color: white;
    }
</style>


<style>

    /* The Modal (background) */
    .w3-modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 99; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .w3-modal-content {
        position: relative;
        background-color: #fefefe;
        margin: auto;
        padding: 0;
        border: 1px solid #888;
        width: 420px;
        max-width: 50%;
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
        -webkit-animation-name: animatetop;
        -webkit-animation-duration: 0.4s;
        animation-name: animatetop;
        animation-duration: 0.4s
    }

    /* Add Animation */
    @-webkit-keyframes animatetop {
        from {top:-300px; opacity:0}
        to {top:0; opacity:1}
    }

    @keyframes animatetop {
        from {top:-300px; opacity:0}
        to {top:0; opacity:1}
    }

    /* The Close Button */
    .w3-close {
        color: white;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .w3-close:hover,
    .w3-close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    .w3-modal-header {
        padding: 2px 16px;
        /* background-color: #5cb85c; */
        background-color:var(--secondary);
        color: white;
    }

    .w3-modal-body {
        padding: 2px 16px;
        min-height: 300px;
    }

    .w3-modal-footer {
        padding: 2px 16px;
        background-color: #5cb85c;
        color: white;
    }
</style>
<style>
    /* Color radio inputs */

    input[type=radio] {
    display: none;
    }
    input[type=radio]:checked + label span {
    transform: scale(1.25);
    border: 2px solid var(--secondary);
    border: 2px solid #edaa0d;
    }

    label {
        display: inline-block;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        margin-right: 10px;
        cursor: pointer;
    }
    label:hover span {
        transform: scale(1.25);
    }
    label span {
        display: block;
        border-radius: 50%;
        width: 100%;
        height: 100%;
        transition: transform 0.2s ease-in-out;
    }
    label span.span-size {
        display: block;
        border-radius: 0%;
        width: 100%;
        height: 100%;
        transition: transform 0.2s ease-in-out;
    }
</style>

<script src="https://cdn.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"/>
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>

@endsection

@section('content')
<main class="container">
    <section class="mt-5">
        {{-- Breadcrumb --}}
        <ul class="breadcrumb">

            <li><a href="/">Accueil</a></li>
            <li><a href="/shop">Boutique</a></li>
            <li><a href="/tous_les_articles/">Panier</a></li>
        </ul>
        @csrf
    </section>
    <section class="container py-2 r bg-white my-2">
        <div class="d-flex justify-content-between align-items-center">

            <span class="h5 font-weight-bold">Panier
                <span class="font-weight-bold">({{App\ShoppingCart::ShoppingCart()->items->count()}} Articles)</span>
            </span>
            @if (App\ShoppingCart::ShoppingCart()->count() > 0)
                <a id="btnCartClear" class="text-danger text-uppercase fw-600 fs-l btn-icon">
                    Tout supprimer
                    <i class="material-icons ml-2">delete</i>
                </a>
            @endif
        </div>
    </section>
    @if (App\ShoppingCart::ShoppingCart()->items->count() > 0)
        <section class="py-3 r">
            <div class="container">
                @foreach (App\ShoppingCart::ShoppingCart()->items as $item)
                    <div class="row mb-3 b-fg r d-flex align-items-center">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{ $item->product->getImageUrl() ?? asset('customer/images/pair.jpg') }}" height="120" width="100%" class="img-fluid" alt="" srcset="">
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <a href="/product_details/{{$item->product->slug}}">
                                        <p class="primary-text-color fs-xxl fw-600">{{$item->product->title}}</p>
                                    </a>
                                </div>

                                @if($item->product->isPromoted())
                                    <h5 class="font-weight-bold text-secondary">
                                        {{"-".$item->product->promo_percentage."%"}}
                                    </h5>
                                @endif
                                <div class="row">
                                    <div class="d-flex align-items-center justify-content-end">
                                        <div class="fw-500 h4">
                                            @if($item->product->isPromoted())
                                                <del>
                                                    {{$item->product->price}} FCFA
                                                </del>
                                                <br>
                                            @endif
                                            {{$item->product->getPrice()}} FCFA
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="my-md-0">
                                                <div id="qtyController-{{$item->product->id}}" class="container">
                                                    <div class="mb-2 d-flex justify-content-between align-items-center">
                                                        <a href="/ajax/cart/decrement/{{$item->id}}" class="cartQtyDecrement btn-article py-1 px-3 btn-secondary btn-icon fs-xxl">
                                                            -
                                                        </a>
                                                        <span class="fs-xxl fw-500 mx-5 mx-md-3">
                                                            {{$item->product->currentCartItem()->quantity}}
                                                        </span>
                                                        <a  href="/ajax/cart/increment/{{$item->id}}"  class="cartQtyIncrement btn-article py-1 px-3 btn-secondary btn-icon fs-xxl">
                                                            +
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <p class="font-weight-bold h5 ">{{$item->amount}} FCFA </p>
                                                <a href="/ajax/cart/remove/{{$item->id}}" class="mx-md-4">
                                                    <i class="material-icons text-danger">delete</i>
                                                </a>
                                            </div>
                                        </div>
                                        <div>
                                            @include('customer_frontOffice.shops.components.cart_article_choice', ['product' => $item->product])
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>

        <section>
            <div class="container">
                <div class="text-right mt-3">
                    <h5 class="font-weight-bold text-right">Total: <span class="secondary-text-color font-weight-bold ml-5">{{App\ShoppingCart::ShoppingCart()->getTotal()}} FCFA</span>
                    </h5>
                </div>
                <br>
        </section>
        {{-- Delivery / Livraison --}}
        <section class="mb-2">
            <fieldset id="fieldset_delivery">
                <div class="form-card">
                    <h2 class="fs-title">Livraison</h2>
                    <div class="container mt-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                @if(Auth::guard('customer')->user())
                                    @if(!is_null(Auth::guard('customer')->user()->location_latitude) && !is_null(Auth::guard('customer')->user()->location_longitude) && !is_null(Auth::guard('customer')->user()->location_address))
                                        <input type="text" id="locat_delivery" name="address_delivery_name" class="form-control no-display-input mb-3" placeholder="Adresse de livraison " value="{{old('address_delivery_name') ?? Auth::guard('customer')->user()->location_address ?? null}}" readonly>
                                    @else
                                        <input type="text" id="locat_delivery" name="address_delivery_name" class="form-control no-display-input mb-3" placeholder="Adresse de livraison " readonly>
                                    @endif
                                @else
                                    <input type="text" id="locat_delivery" name="address_delivery_name" class="form-control no-display-input mb-3" placeholder="Adresse de livraison " readonly>
                                @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="input-group">
                                @if(Auth::guard('customer')->user())
                                    @if(!is_null(Auth::guard('customer')->user()->location_latitude) && !is_null(Auth::guard('customer')->user()->location_longitude) && !is_null(Auth::guard('customer')->user()->location_address))
                                        <input type="text" id="addr_delivery" name="addr_delivery" required class="form-control" placeholder="Lieu de livraison" data-toggle="modal_delivery" data-target="#Modal_delivery" onkeyup="start_search();" value="{{old('addr_delivery') ?? Auth::guard('customer')->user()->location_address ?? null}}"/>
                                    @else
                                        <input type="text" id="addr_delivery" name="addr_delivery" required class="form-control" placeholder="Lieu de livraison" data-toggle="modal_delivery" data-target="#Modal_delivery" onkeyup="start_search();"/>
                                    @endif
                                @else
                                    <input type="text" id="addr_delivery" name="addr_delivery" required class="form-control" placeholder="Lieu de livraison" data-toggle="modal_delivery" data-target="#Modal_delivery" onkeyup="start_search();"/>
                                @endif

                                    <div class="input-group-append">
                                        <button type="button" onclick="addr_delivery_search();" class="btn btn-secondary">Rechercher</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="error-msg" class="text-danger font-weight-bold"></div>
                                <div class="">
                                    <div id="results_delivery"></div>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @if(Auth::guard('customer')->user())
                                @if(!is_null(Auth::guard('customer')->user()->location_latitude))
                                    <input type="hidden" id="lat_delivery" name="location_delivery_lat" value="{{old('location_delivery_lat') ?? Auth::guard('customer')->user()->location_latitude ?? 5.40911790}}"/>
                                @else
                                    <input type="hidden" id="lat_delivery" name="location_delivery_lat" value="{{old('location_delivery_lat') ?? 5.40911790}}"/>
                                @endif

                                @if(!is_null(Auth::guard('customer')->user()->location_longitude))
                                    <input type="hidden" id="lon_delivery" name="location_delivery_lng" value="{{old('location_delivery_lng') ?? Auth::guard('customer')->user()->location_longitude ?? -4.04220990}}"/>
                                @else
                                    <input type="hidden" id="lon_delivery" name="location_delivery_lng" value="{{old('location_delivery_lng') ?? -4.04220990}}"/>
                                @endif
                            @else
                                <input type="hidden" id="lat_delivery" name="location_delivery_lat" value="{{old('location_delivery_lat') ?? 5.40911790}}"/>
                                <input type="hidden" id="lon_delivery" name="location_delivery_lng" value="{{old('location_delivery_lng') ?? -4.04220990}}"/>
                            @endif
                        </div>
                        <div class="row">

                            <div class="container">
                                <div class="b-fg">
                                    <div style="height:200px; width: 100%" id="map_delivery"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </section>

        {{-- <section>
            <input type="text" name="city" class="form-control input-height" id="input_search" placeholder="Ville, commune, Quartier">
            <input type="hidden" name="lat" id="lat_input" >
            <input type="hidden" name="lon" id="lon_input">
                <div id="match-list" style="margin: 10px auto; width: 50%;position:absolute; top:70px;right:0">
                </div>
            <div class="input-group-append">
                <button type="submit" class="btn btn-secondary" type="button" id="submit_search">RECHERCHER</button>
            </div>
        </section> --}}
        <section class="mt-3">
                <div class="text-right">
                    <!-- Button trigger modal -->
                    @if(Auth::guard('customer')->check())
                        <a id="BtnAdjeminPay" class="btn-article m-w-150 btn-secondary px-2">
                            Passer au paiement
                        </a>
                        {{-- <a href="/customer/shop/checkout" class="btn-article m-w-150 btn-secondary px-2">
                            <i class="fa fa-money mr-2" style="font-size: 20px;"></i>
                            Valider la commande
                        </a> --}}
                    @else
                        <a href="{{route('customer.redirectTo', ['url' => Request::url()])}}" class="btn-article m-w-150 btn-secondary px-2">
                            Inscrivez-vous pour finaliser la commande
                        </a>
                    @endif

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="modal-body">
                                    <center>
                                        <img src="{{ asset('customer/images/icons8-instagram-check-mark-50.png') }}" height="70" width="70" alt="">
                                    </center>
                                    <p class="text-center mt-4 h6">Votre Commande a été validée</p>
                                </div>
                                <!-- <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <br><br>
            </div>
        </section>
    @else
        <section class="py-3 bg-white r">
            <div class="container">
                <div class="row product-card">
                        <div class="col-lg-5 col-md-5 mx-auto col-sm-12">
                            <img src="{{asset('customer/images/logo.png')}}" style="" class="img-fluid" alt="">
                        </div>
                        <div class="container text-center">
                            <h3>Vous n'avez pas d'article dans votre panier</h3>
                        </div>
                        <div class="container">
                            <div class="row">
                                <a href="/shop" class="btn-article btn-secondary mx-auto px-2">
                                    Ajouter des produits
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
</main>

@endsection

@section('scripts')
<script>
    $token = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });
</script>
<script>

    // delete all from cart
    $("#btnCartClear").click(function(){
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;

        var btn = $(this);
        btn.html(_cuteLoader);

            $.ajax({
                type: "POST",
                url: "{{ route('ajax.cart.clear') }}",

                dataType: "json",
                success: function (response) {
                    if(response.status != "OK"){

                        btn.html("Erreur");

                        setTimeout(() => {
                            btn.html("Tout supprimer<i class='material-icons ml-1'>delete</i>");
                        }, 1000);
                        return false;
                    }

                    btn.html("Tout supprimer<i class='material-icons ml-1'>delete</i>");
                    window.location.reload();
                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                    btn.html("Erreur");
                    setTimeout(() => {
                        btn.html("Réessayer");
                    }, 2000);
                },
            });
    });


    // ***** Adjemin Pay
    $("#BtnAdjeminPay").click(function(){
        console.log(">>> clicked");
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;



        var deliveryAddress = $("input[name='address_delivery_name']").val();
        var deliveryAddressLat = $("input[name='location_delivery_lat']").val();
        var deliveryAddressLng = $("input[name='location_delivery_lng']").val();

        if(deliveryAddress == '' || deliveryAddress == null){
            //
            $("#error-msg").html("Veuillez choisir une adresse de livraison");
            return false;
        }
        if(deliveryAddressLat == '' || deliveryAddressLat == null){
            //
            $("#error-msg").html("Veuillez choisir une adresse de livraison");
            return false;
        }
        if(deliveryAddressLng == '' || deliveryAddressLng == null){
            //
            $("#error-msg").html("Veuillez choisir une adresse de livraison");
            return false;
        }

        var btn = $(this);
        btn.html(_cuteLoader);

            $.ajax({
                type: "POST",
                url: "{{ route('ajax.shop.createOrderOnly') }}",
                data: {
                    "delivery_name": deliveryAddress,
                    "delivery_lat": deliveryAddressLat,
                    "delivery_lng": deliveryAddressLng,
                },
                dataType: "json",
                success: function (response) {
                    console.log("Ressponse succes");
                    console.log(response);
                    if(response.status != "OK"){
                        if(response.status == "ALREADY_EXISTS"){
                            btn.html("J'y participe <i class='material-icons ml-2'>check</i>");
                            return false;
                        }
                        btn.html("Erreur");

                        setTimeout(() => {
                            btn.html("Réessayer");
                        }, 1000);
                        return false;
                    }
                    //
                    // **
                    if(response.data.checkout_url != null){
                        window.location.assign(response.data.checkout_url);
                        return true;
                    }
                    // if(response.data.transaction != null){
                    //     // pay with adjeminpay
                    //     payWithAdjeminPay({
                    //         amount: response.data.transaction.amount,
                    //         transactionId: response.data.transaction.id,
                    //         currency: response.data.transaction.currency,
                    //         designation: response.data.transaction.designation,
                    //         customField: response.data.status
                    //     });
                    // }
                    //
                    // btn.html("J'y participe <i class='material-icons ml-2'>check</i>");
                    // btn.removeClass('btn-secondary');
                    // btn.addClass('btn-primary');

                    // if(response.status != "ALREADY_EXISTS"){
                    //     $("#place_number_count").html($("#place_number_count").html()-1);
                    // }
                    // alert(document.URL+'#toReload');

                    // $('#toReload').load(document.URL+'#toReload');
                    // window.location.reload();
                    // this.classList.remove("markCompletedBtn");
                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                    btn.html("Erreur");
                    setTimeout(() => {
                        btn.html("Réessayer");
                    }, 2000);
                },
            });
    });

    //

    function decrement(id, name, price){
        console.log("dec");

        qty = parseInt($("#cartQtyValue-"+id).html());

        if(qty > 0){
            qty--;
            $("#cartQtyValue-"+id).html(qty);
            $("#cartItemAmount-"+id).html((qty)*parseInt(price));

            ajaxCart({
                    pId: id,
                    pName: name,
                    pQuantity: qty,
                    pPrice: price,
                });

            if(qty == 0){
                $("#qtyController-"+id).empty();
                $("#btnCart-"+id).html('Ajouter au panier');
                $("#btnCart-"+id).addClass('btn-article btn-secondary w-100  btnAddToCart');

                showSnackbar("Produit Retiré", "bg-danger");

                setTimeout(() => {
                    $("#btnCart-"+id).addClass('invisible');
                }, 3000);
                return false;
            }
            // showSnackbar("Produit Modifi", "bg-danger");

        }


    }
    function increment(id, name, price){
        console.log("inc");

        qty = parseInt($("#cartQtyValue-"+id).html())+1;
        $("#cartQtyValue-"+id).html(qty);
        $("#cartItemAmount-"+id).html(qty*parseInt(price));

        ajaxCart({
            pId: id,
            pName: name,
            pQuantity: qty,
            pPrice: price,
        });
    }

    function ajaxCart(cartData, action="manage"){
        if(cartData == null)
        {
            console.log("=== Invalid data");
            return 0;
        }
        console.log("=== Saving to cart");
        console.log(cartData);

        $.ajax({
            type: "POST",
            url: "/ajax/cart/"+action,
            data: {
                'product_id': cartData.pId,
                'product_name': cartData.pName,
                'product_quantity': cartData.pQuantity,
                'product_price': cartData.pPrice,
            },
            dataType: "json",
            success: function (response) {
                if(response.status != "OK"){
                    if(response.status == "REMOVED"){
                        showSnackbar("Produit Retiré", "bg-danger");
                        return false;
                    }
                    console.log(">>>> errerur");
                    showSnackbar("Erreur, Veuillez réessayer", "bg-danger", 4000);
                    console.log(response);
                    return false;
                }
                // showSnackbar("Erreur, Veuillez réessayer", "bg-danger", 4000);
                console.log(">>>> SUCCESS  ");
                console.log(response);
                // window.location.reload();
            },
            error: function (response) {
                console.log("Error :");
                console.log(response);
            },
        });
    }

    function start_search(){
        //
        var val = $("input[name='addr_delivery']").val();
        if(val.length > 4){
            addr_delivery_search();
        }
    }

</script>
<script>
    // Add color
    $(".chooseDetail").click(function (){
        console.log(">> choosing detail");
        thisBtn = $(this);
        itemId = $(this).prev().data('item-id');
        detailName = $(this).prev().data('detail-name');
        detailContent = $(this).prev().val();
        console.log(detailName);
        console.log(detailContent);
        console.log(itemId);

        // return false;

            $.ajax({
                type: "POST",
                url: "{{ route('ajax.cart.addDetail') }}",
                data: {
                    "item_id": itemId,
                    "detail_name": detailName,
                    "detail_content": detailContent,
                },
                dataType: "json",
                success: function (response) {
                    console.log("Ressponse succes");
                    console.log(response);
                    if(response.status != "OK"){

                        showSnackbar("Erreur", "bg-danger");
                        thisBtn.prev().prop("checked", false);
                        return false;
                    }

                    showSnackbar(detailName+" "+detailContent+" ajouté");
                    thisBtn.prev().prop("checked", true);
                    return true;
                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                    btn.html("Erreur");
                    setTimeout(() => {
                        btn.html("Réessayer");
                    }, 2000);
                },
            });
    });

    // Add size to article
</script>


{{-- Gestion livraison --}}

<script type="text/javascript">
    // *** Delivery address map init
    // Abidjan
    var startlat_delivery =  document.getElementById('lat_delivery').value != null ? document.getElementById('lat_delivery').value : 5.40911790;
    var startlon_delivery = document.getElementById('lon_delivery').value != null ? document.getElementById('lon_delivery').value : -4.04220990;
    console.log(startlat_delivery)
    console.log(startlon_delivery)

    var options_delivery = {
        center: [startlat_delivery, startlon_delivery],
        // zoom: 10
        zoom: 12
    }

    document.getElementById('lat_delivery').value = startlat_delivery;
    document.getElementById('lon_delivery').value = startlon_delivery;

    var map_delivery = L.map('map_delivery', options_delivery);
    var nzoom = 12;

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: 'OSM'}).addTo(map_delivery);
    // L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: 'OSM'}).addTo(map_delivery);

    var my_delivery_Marker = L.marker([startlat_delivery, startlon_delivery], {
        title: "Coordinates",
        alt: "Coordinates",
        draggable: true
    }).addTo(map_delivery).on('dragend', function () {
        var lat_delivery = my_delivery_Marker.getLatLng().lat_delivery.toFixed(8);
        var lon_delivery = my_delivery_Marker.getLatLng().lng.toFixed(8);
        var czoom = map_delivery.getZoom();

        if (czoom < 18) {
            nzoom = czoom + 2;
        }
        if (nzoom > 18) {
            nzoom = 18;
        }
        if (czoom != 18) {
            map_delivery.setView([lat_delivery, lon_delivery], nzoom);
        } else {
            map_delivery.setView([lat_delivery, lon_delivery]);
        }
        document.getElementById('lat_delivery').value = lat_delivery;
        document.getElementById('lon_delivery').value = lon_delivery;
        my_delivery_Marker.bindPopup("Lat " + lat_delivery + "<br />Lon " + lon_delivery).openPopup();
    });

    function choose_delivery_Addr(lat1, lng1) {
        my_delivery_Marker.closePopup();
        map_delivery.setView([lat1, lng1], 18);
        my_delivery_Marker.setLatLng([lat1, lng1]);
        lat = lat1.toFixed(8);
        lon = lng1.toFixed(8);
        document.getElementById('lat_delivery').value = lat;
        document.getElementById('lon_delivery').value = lon;
        //document.getElementById('locat').value = addrn;
        //alert(addrn);
        my_delivery_Marker.bindPopup("Lat " + lat + "<br />Lon " + lon).openPopup();

        $.get('https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=' + lat + '&lon=' + lon, function (data) {
            //console.log(data);
            // document.getElementById('locat').value = data.address.city + ',' + data.address.state + ',' + data.address.country;
            document.getElementById('locat_delivery').value = data.display_name;
        });
        //alert(i);
        document.getElementById('results_delivery').innerHTML = '';
    //document.getElementById('location').submit();
    }

    function lolp(ii) {
        alert(ii);
    }

    function loca(i) {
        alert(i);
    }

    function my_delivery_Function(arr) {
        var out = "<br />";
        var i;

        if (arr.length > 0) {
            for (i = 0; i < arr.length; i++) {
                out += "<div class='address' title='Show Location and Coordinates' style='cursor:pointer' value='" + arr[i].display_name + "' onclick='choose_delivery_Addr(" + arr[i].lat + ", " + arr[i].lon + ");return false;'>" + arr[i].display_name + "</div>";
            }
            document.getElementById('results_delivery').innerHTML = out;
        } else {
            document.getElementById('results_delivery').innerHTML = "<div class='address'>Aucune adresse trouvée...</div>";
        }

    }

    function addr_delivery_search() {
        var inp = document.getElementById("addr_delivery");
        var xmlhttp = new XMLHttpRequest();
        var url = "https://nominatim.openstreetmap.org/search?format=json&limit=3&q=" + inp.value;
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var my_delivery_Arr = JSON.parse(this.responseText);
                my_delivery_Function(my_delivery_Arr);
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }
</script>

{{-- Color inputs css --}}
<script>
    // Radio color inputs
    var colorSpans = document.getElementsByClassName('span-color');
    for(var c = 0; c < colorSpans.length; c++){
        //
        colorSpans[c].style.background = colorSpans[c].dataset.color;
    }
</script>


@endsection
