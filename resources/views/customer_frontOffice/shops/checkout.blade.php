@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
<style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }

    a {
        color: black;
    }
    .no-display-input{
        /*  */
        border: none !important;
        display: inline-block;
    }
    .address{
        padding: 10px;
    }
    .address:hover{
        background-color: white;
    }
</style>
<style>
    /* Choose color and size */
    /* Color radio inputs */

    input[type=radio] {
    display: none;
    }
    input[type=radio]:checked + label span {
    transform: scale(1.25);
    border: 2px solid var(--secondary);
    }
    input[type=radio]:checked + label .red {
    border: 2px solid #fff;
    }
    input[type=radio]:checked + label .orange {
    border: 2px solid #873a08;
    }
    input[type=radio]:checked + label .yellow {
    border: 2px solid #816102;
    }
    input[type=radio]:checked + label .olive {
    border: 2px solid #505a0b;
    }
    input[type=radio]:checked + label .green {
    border: 2px solid #0e4e1d;
    }
    input[type=radio]:checked + label .teal {
    border: 2px solid #003633;
    }
    input[type=radio]:checked + label .blue {
    border: 2px solid #103f62;
    }
    input[type=radio]:checked + label .violet {
    border: 2px solid #321a64;
    }
    input[type=radio]:checked + label .purple {
    border: 2px solid #501962;
    }
    input[type=radio]:checked + label .pink {
    border: 2px solid #851554;
    }

    label {
        display: inline-block;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        margin-right: 10px;
        cursor: pointer;
    }
    label:hover span {
        transform: scale(1.25);
    }
    label span {
        display: block;
        border-radius: 50%;
        width: 100%;
        height: 100%;
        transition: transform 0.2s ease-in-out;
    }
</style>
<script src="https://cdn.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"/>
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>

@endsection

@section('content')
<main class="container">
      <section class="my-3">
        {{-- Breadcrumb --}}
        <ul class="breadcrumb">
            <li><a href="/">Accueil</a></li>
            <li><a href="/shop">Boutique</a></li>
            <li><a>Checkout</a></li>
        </ul>
        @csrf
    </section>

    <section class="py-5">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 bg-white r">
                <div class="p-3 m-3">
                    <h2 class="fs-title">Résumé commande</h2>
                    <div class="table-responsive table-full-width">
                        <table class="table table-hover table-striped table-adjemin">
                            <tbody class="text-left">
                                <tr>
                                    <th>Articles</th>
                                    <th>Quantité</th>
                                    <th>Prix Unitaire</th>
                                    <th>Couleur</th>
                                    <th>Taille</th>
                                    <th>Montant</th>
                                </tr>
                                @foreach (App\ShoppingCart::ShoppingCart()->items as $item)
                                    <tr>
                                        <td class="" id="orderReviewItemTitle">
                                            {{$item->product->title}}
                                        </td>
                                        <td class="" id="orderReviewQuantity">
                                            {{$item->quantity}}
                                        </td>
                                        <td class="" id="orderReviewPrice">
                                            {{$item->price}}
                                        </td>
                                        <td class="" id="orderReviewColor">
                                            <span style="width:10px;height:10px;background-color:{{$item->getColor()->code_color ?? ""}};"></span>
                                            {{$item->color}}
                                        </td>
                                        <td class="" id="orderReviewSize">
                                            {{$item->size}}
                                        </td>
                                        <td class="" id="orderReviewAmount">
                                            {{$item->amount}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            {{-- Services info from $form_data --}}
                            <tfoot>
                                <tr >
                                    <td colspan="6" style="padding-top:50px;">
                                        <strong>
                                            Taxes
                                        </strong>
                                        <span id="orderReviewTotal" class="float-right">{{$order->delivery_fees ?? 0 }} Fcfa
                                    </td>
                                </tr>
                                @forelse($order->coupons as $coupon)
                                    <tr >
                                        <td colspan="6">
                                            <strong>
                                                Bon de réduction {{$coupon->code."  "}} -{{$coupon->pourcentage}}%
                                            </strong>
                                            <span id="orderReviewTotal" class="float-right">
                                                -{{$coupon->pivot->amount_saved}} Fcfa
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                                <tr>
                                    <td colspan="6">
                                    <strong>
                                        Moyen de paiment
                                    </strong>
                                        <span id="orderReviewTotal" class="float-right">{{$order->payment_method_slug}}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <strong>
                                            Total à payer
                                        </strong>
                                        <span id="orderReviewTotal" class="float-right">{{$order->amount}}  Fcfa</span>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-left">
                            <p>
                            L'article sera livré à : <i id="orderReviewDeliveryAddress" style="text-decoration: underline">{{$order->location_name}}</i>
                            </p>
                        </div>
                    </div>
                </div>
            {{--
                <input type="hidden" name="transaction" data-method ="{{$transactionData['paymentMethod']}}" data-id ="{{$transactionData['id']}}" data-designation ="{{$transactionData['designation']}}" data-amount ="{{$transactionData['amount']}}" data-currency ="{{$transactionData['currency']}}" data-status ="{{$transactionData['status']}}" data-custom="{{isset($transactionData['customField']) ? $transactionData['customField'] : $transactionData['status'] }}">
            --}}

                <div class="container" id="buttonHolder">
                    <h2 class="text-danger" id="result-message"></h2>
                    <div class="px-2 py-5">
                        @if(Auth::guard('customer')->check())
                            <form method="POST" action="/payment/fromInvoice">
                                @csrf
                                <input hidden name="invoice_id" value="{{$invoice->id}}">
                                <input hidden name="redirect_to" value="shop">
                                <button type="submit" class="btn-article w-100 text-center btn-secondary px-2">
                                    <i class="fa fa-money mr-2" style="font-size: 20px;"></i> Payer {{$order->amount}} Fcfa
                                </button>
                            </form>
                        @else
                            <a href="{{route('customer.redirectTo', ['url' => Request::url()])}}" class="btn-article m-w-150 btn-secondary px-2">
                                <i class="fa fa-money mr-2" style="font-size: 20px;"></i> Inscrivez-vous pour finaliser la commande
                            </a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="p-3 mx-3 r bg-white">
                    <h2 class="fs-title mb-5">Vous avez un coupon ?</h2>
                    <p>
                        Entrez votre coupon ci-dessous et bénéficiez d'une réduction
                    </p>
                    <div class="form-group">
                        @forelse($order->coupons as $coupon)
                            <div class="text-primary font-weight-bold text-uppercase my-2 text-right">
                                coupon
                                {{$coupon->code."  (-".$coupon->pourcentage."%) "}} Ajouté
                            </div>
                        @empty
                        @endforelse
                            <input class="form-control" type="text" name="coupon_code" placeholder="Entrez votre code">

                            <span id="coupon_error" class="text-danger font-weight-bold"></span>
                            <a id="submitCoupon" class="btn btn-article btn-primary w-100 mt-3">Vérifier</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection

@section('scripts')
<script>
    $token = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });
</script>
{{-- Coupon adding Script --}}
<script>
    $("#submitCoupon").click(function name(params) {
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;

        var coupon_code = $("input[name='coupon_code']").val();
        var invoice_id = $("input[name='invoice_id'").val();

        $(this).html(_cuteLoader);
        var $btn = $(this);
        var $messageDiv = $("#coupon_error");

        if(coupon_code == ''){
            $("#coupon_error").html("Vous devez entrez un coupon");
            $("#submitCoupon").html("Vérifier")
            return false;
        }
        if(invoice_id == ''){
            $("#coupon_error").html("Erreur survenue");
            $("#submitCoupon").html("Veuillez réessayer plus tard")
            return false;
        }


        $.ajax({
                type: "POST",
                url: "{{ route('ajax.coupon.add') }}",
                data: {
                    "invoice_id": invoice_id,
                    "coupon_code": coupon_code,
                    '_token': "{{ csrf_token() }}"
                },
                dataType: "json",
                success: function (response) {
                    //
                    console.log(">> success");
                    console.log(response);
                    if(response.status == null){
                        $btn.html("Veuillez réessayer plus tard");
                        $messageDiv.html("Une erreur est survenu");
                        return false;
                    }
                    // managing status codes
                    if(response.status != "OK"){
                        $btn.html("OUPS");
                        if(response.error != null){
                            if(response.error.message != null){
                                $messageDiv.html(response.error.message);
                                setTimeout(() => {
                                    $btn.html("Réessayer");
                                }, 1000);
                                return false;
                            }
                            $btn.html("Réessayer")
                        }

                    }
                    $btn.html("Coupon Ajouté");

                    window.location.reload();

                },
                error: function(response){
                    console.log(">> erreur");
                    $btn.html("Veuillez réessayer plus tard");
                    $messageDiv.html("Une erreur est survenue");
                    console.log(response);
                }
        });
    });
</script>

{{-- Color inputs css --}}
<script>
    // Radio color inputs
    var colorSpans = document.getElementsByClassName('span-color');
    for(var c = 0; c < colorSpans.length; c++){
        //
        colorSpans[c].style.background = colorSpans[c].dataset.color;
    }
</script>


@endsection
