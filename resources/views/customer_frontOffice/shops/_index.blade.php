

@extends("customer_frontOffice.layouts_customer.master_customer")
@section('css')

    <style>
        :root {
            --background: #eee;
            /* --foreground: #fff; */
            --foreground: #d3d3d3;
            --foreground: #fff;
            --foreground-l: #e6e6e6;
            --primary: #1c6a49;
            --primary-dark: #144e36;
            --secondary: #edaa0d;
            --secondary-dark: #cf940a;
            --br: 4px;
        }

        * {
            box-sizing: border-box;
        }

        body {
            /* font-size: 13px; */
            /* font-size: .875rem; */
            font-size: .625rem;
            /* background-color: rgba(247, 247, 247, 0.973); */
            background-color: var(--background);
            font-weight: 500;
        }

        .b-fg {
            background-color: var(--foreground);
        }

        .b-fgl {
            background-color: var(--foreground-l);
        }

        .b-bg {
            background-color: var(--background);
        }

        .breadcrumb {

            background-color: var(--foreground) !important;
        }

        .r {
            border-radius: var(--br);
        }

        .fs-xs {
            font-size: .625rem !important;
        }

        .fs-s {
            font-size: .75rem !important;
        }

        .fs-m,
        p {
            font-size: .875rem !important;
        }

        .fs-l {
            font-size: 1rem !important;
        }

        a {
            font-size: .75rem;
            color: black;
        }

        ,
        a:hover {
            color: currentColor !important;
        }

        main {
            /* padding: 0 15px; */
        }

        .aside-left {
            padding-top: 0;
            padding-right: 0;
        }

        .aside-right {
            padding-top: 0;
            padding-left: 0;
        }

        .container {
            width: 100%;
            padding: 5px 10px;
            margin-right: auto;
            margin-left: auto;
        }

        @media (min-width: 576px) {
            .container {
                max-width: 540px;
            }
        }

        @media (max-width: 576px) {
            .btn-article.invisible {
                visibility: visible;
            }
        }

        @media (min-width: 768px) {
            .container {
                max-width: 720px;
            }
        }

        @media (min-width: 992px) {
            .container {
                max-width: 960px;
                /* max-width: 825px; */
            }
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 1140px;
                /* max-width: 900px; */
            }
        }


        .container-fluid {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        .article-box:nth-child(3n+1) {
            padding-right: 0 !important;
        }

        .article-box:nth-child(3n+2) {
            padding-right: 5px !important;
            padding-left: 5px !important;
        }

        .article-box:nth-child(3n+0) {
            padding-left: 0 !important;
        }

        /* .article-box:nth-child(even){
                background: blue !important;
            } */
        .article-card {
            border-radius: var(--br);
            position: relative;
            padding-bottom: 5px;
            max-height: 500px;
            box-shadow: none;
            border: .5px solid #eee;
        }

        .article-card:hover {
            border: 1px solid #ccc;
            box-shadow: 3px 3px 3px #bbb;
        }

        .article-card:hover .btn-article.invisible {
            visibility: visible !important;
        }

        .article-card a:hover {
            color: currentColor;
        }

        .article-image {
            border-radius: var(--br) var(--br) 0 0;
        }

        .article-price {
            background-color: var(--secondary);
            color: #000;
            font-weight: bold;
            padding: 7px 15px;
            width: fit-content;
            color: white;
            position: absolute;
            border-radius: 0 var(--br) 0 var(--br);
            right: 0;
        }

        .circle {
            width: 20px;
            height: 20px;
            border-radius: 50%;
            /* border-style: solid; */
            border: 1px solid;
        }

    </style>
@endsection

@section('content')
    <main class="container-fluid">
        <section class="pt-3">
            <div class="container-fluid">
                <div class="text-center p-md-5 p-2 r b-fg">
                    <h1 class="font-weight-bold text-center primary-text-color mb-3 text-uppercase">Boutique</h1>
                    <div>
                        <p class="font-weight-bold shop-name text-center p-0">Bien s'équiper, c'est aussi mieux s'exercer
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="">
            <div class="container-fluid">
                {{-- Breadcrumb --}}
                <ul class="breadcrumb">

                    <li><a href="/">Accueil</a></li>
                    <li><a href="/shop">Boutique</a></li>
                    {{-- <li><a href="/tous_les_articles/">Tous les articles</a></li>
                    --}}
                </ul>
                @csrf
            </div>
        </section>
        {{-- Flash Info --}}
        {{-- <section class="">
            <div class="container-fluid">
                <div class="container flash-info r py-2 bg-white d-flex justify-content-between">
                    Container
                    <span class="flash-info-close">X</span>
                </div>
            </div>
        </section> --}}

        <!-- contents -->
        <section class="container-fluid">
            {{-- <div class=""> --}}
                <div class="row">
                    <!--  -->
                    {{-- Sidebar Left --}}
                    <aside id="aside-left" class="aside-left @if(isset($ads)) @if($ads->count() > 0) col-xl-2 @endif @endif col-md-3 mb-2">
                        <section class="container r b-fg">
                            <!-- Disciplines -->
                            <h5 class="text-uppercase">Catégories</h5>
                            <div class="small-secondary-dividers mb-3"></div>
                            <div class="px-3 my-2">
                                @php
                                if(array_key_exists('categories', $_GET)){
                                $selected = explode(',', $_GET['categories']) ?? [];
                                }else{
                                $selected = [];
                                }
                                @endphp
                                @foreach ($categories as $category)
                                    <div class="form-check py-2">
                                        <input class="form-check-input filter" type="checkbox" name="filter[]"
                                            id="{{ $category->name }}" value="{{ $category->name }}" {!! in_array($category->name, $selected ?? []) ? 'checked' : '' !!}>
                                        <label class="form-check-label" for="{{ $category->name }}">{{ $category->name }}
                                        </label>
                                    </div>
                                @endforeach
                                {{--<div class="form-check py-2">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                    <label class="form-check-label" for="inlineCheckbox1">Fitness </label>
                                </div>

                                <div class="form-check py-2">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                    <label class="form-check-label" for="inlineCheckbox2">Yoga </label>
                                </div>
                                <div class="form-check py-2">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                    <label class="form-check-label" for="inlineCheckbox2">Pilates </label>
                                </div>
                                <div class="form-check py-2">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                    <label class="form-check-label" for="inlineCheckbox2">Boxe et Auto-défense </label>
                                </div>
                                <div class="form-check py-2">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                    <label class="form-check-label" for="inlineCheckbox2">Sports Loisirs </label>
                                </div>
                                <div class="form-check py-2">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                    <label class="form-check-label" for="inlineCheckbox2">Danse </label>
                                </div>--}}
                            </div>

                            <hr>

                        </section>
                    </aside>

                    <main class="@if(isset($ads)) @if($ads->count() > 0) col-xl-7 @endif @endif  col-md-9 col-sm-12" id="render-products">
                        @include('customer_frontOffice.shops.components.products', ['products' => $products])
                    </main>
                    @if(isset($ads))
                        @if($ads->count() > 0)
                            <aside id="aside-right" class="aside-right col-xl-3 d-md-none d-none d-xl-block">
                                <section class="r b-fg">
                                    <div class="container">

                                        <h5 class="text-uppercase">Publicités</h5>
                                        <div class="small-secondary-dividers mb-3"></div>
                                        <!--  -->
                                        @foreach ($ads as $ad)
                                            <div class="white-bg card mb-2">
                                                @if($ad->file_type == "image")
                                                <a target="_blank" href="{{ $ad->redirect_to }}">
                                                    <div class="img-container">
                                                        <img src="{{ $ad->cover_url ?? asset('customer/images/860b5c3.jpg') }}"
                                                            class="img-fluid" alt="">
                                                    </div>
                                                    <div class="px-2 py-1">
                                                        <h6 class="secondary-text-color">
                                                            <b>{{ $ad->name }}</b>
                                                        </h6>
                                                        <p>{!! Str::limit($ad->description, 50) !!}</p>
                                                    </div>
                                                </a>
                                                @else
                                                    <div class="embed-responsive embed-responsive-21by9 embed-responsive-16by9 embed-responsive-4by3 embed-responsive-1by1">
                                                        <iframe class="embed-responsive-item" src="{{$ad->cover_url ?? asset('customer/images/860b5c3.jpg') }}"></iframe>
                                                    </div>
                                                    <a target="_blank" href="{{ $ad->redirect_to ?? '#' }}">
                                                        <div class="px-2 py-1">
                                                            <h6 class="secondary-text-color">
                                                                <b>{{ $ad->name }}</b>
                                                            </h6>
                                                            <p>{!! Str::limit($ad->description, 50) !!}</p>
                                                        </div>
                                                    </a>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                </section>
                            </aside>
                        @endif
                    @endif

                </div>
                {{--
            </div> --}}
        </section>
    </main>

@endsection


@section('scripts')
    <script>
        $token = $("input[name='_token']").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $token
            }
        });

    </script>
    {{-- --}}
    <script>
        big = $('#product_views')
        $(".filter").click(function() {
            categories = []
            $('input[name="filter[]"]:checked').each(function() {
                categories.push($(this).val());
            });
            console.log(categories);
            window.location.href = "/shop?categories=" + categories.join(',')
            // $.ajax({
            //     type: "GET",
            //     url: "{{ url('/shop') }}",
            //     data: {
            //         'categories': categories
            //         // '_token': "{{ csrf_token() }}"
            //     },
            //     dataType: "json",
            //     success: function (response) {
            //         console.log(response);
            //         if (response.data) {
            //             // $('#render-products').empty()
            //             $(document).empty()
            //             $(document).append(response.data);
            //             // $('#render-products').append(response.data);
            //         }
            //     },
            //     error: function(error){
            //         big.append(
            //             `
            //                 <div style="position:relative; top: 10px; left:75px">
            //                     <div class="d-flex justify-content-center align-items-center">
            //                         <div class="card card-body" style="width:500px;">
            //                             <center>
            //                                 <img src="{{ asset('customer/images/gh1.png') }}" alt="Logo" width="180px">
            //                             </center>
            //                             <h6 class="text-center" style="color:#AAA"> Désolé Aucun resultat ne correspond à votre recherche</h6>
            //                         </div>
            //                     </div>
            //                 </div>
            //             `
            //         )
            //     }
            // })
        });



        // Just add item to cart then can modify it in the basket
        $(document).on('click', ".btnAddToCart", function() {

            var pId = $(this).data('p-id');
            var pName = $(this).data('p-name');
            var pPrice = $(this).data('p-price');

            $(this).removeClass('invisible');

            // $(this).load('/ajax/x/s/c/cart_quantity');

            var qqty = `
                <div id="qtyController-${pId}">
                    <div class="text-center text-secondary fs-l fw-500">
                        <span id="cartItemAmount-${pId}">
                            ${pPrice}
                        </span>
                        <span>FCFA</span>
                    </div>
                    <div class="mb-2 d-flex justify-content-between align-items-center" data-product-id="${pId}">
                        <a onclick="decrement('${pId}', '${pName}', '${pPrice}');" class="cartQtyDecrement btn-article ml-0 py-1 px-3 btn-secondary btn-icon fs-xxl">
                            -
                        </a>
                        <span id="cartQtyValue-${pId}" class="fs-l fw-500">
                            1
                        </span>
                        <a onclick="increment('${pId}', '${pName}', '${pPrice}');" class="cartQtyIncrement btn-article mr-0 py-1 px-3 btn-secondary btn-icon fs-xxl">
                            +
                        </a>
                    </div>
                    <div class="">
                        <a class="btn-article btn-primary w-100" href="/basket">
                            Finaliser
                        </a>
                    </div>
                </div>
            `;
            $(this).next().html(qqty);

            $(this).removeClass('btn-article');
            $(this).removeClass('btnAddToCart');
            $(this).empty();
            ajaxCart({
                pId: pId,
                pName: pName,
                pQuantity: 1,
                pPrice: pPrice,
            });
            showSnackbar('Produit Ajouté');

        });

        // Modal to choose size and color
        // $(document).on('click', ".btnAddToCart", function() {
        //     var pId = $(this).data('p-id');
        //     var pName = $(this).data('p-name');
        //     var pPrice = $(this).data('p-price');

        //     openModal(pId);
        //     console.log(">>opening" +pId);
        //     return false;
        // });

        function decrement(id, name, price) {
            console.log("dec");

            qty = parseInt($("#cartQtyValue-" + id).html());

            if (qty > 0) {
                qty--;
                $("#cartQtyValue-" + id).html(qty);
                $("#cartItemAmount-" + id).html((qty) * parseInt(price));

                ajaxCart({
                    pId: id,
                    pName: name,
                    pQuantity: qty,
                    pPrice: price,
                });

                if (qty == 0) {
                    $("#qtyController-" + id).empty();
                    $("#btnCart-" + id).html('Ajouter au panier');
                    $("#btnCart-" + id).addClass('btn-article btn-secondary w-100  btnAddToCart');

                    showSnackbar("Produit Retiré", "bg-danger");

                    setTimeout(() => {
                        $("#btnCart-" + id).addClass('invisible');
                    }, 3000);
                    return false;
                }
                // showSnackbar("Produit Modifi", "bg-danger");

            }


        }

        function increment(id, name, price) {
            console.log("inc");

            qty = parseInt($("#cartQtyValue-" + id).html()) + 1;
            $("#cartQtyValue-" + id).html(qty);
            $("#cartItemAmount-" + id).html(qty * parseInt(price));

            ajaxCart({
                pId: id,
                pName: name,
                pQuantity: qty,
                pPrice: price,
            });
        }

        function ajaxCart(cartData, action = "manage") {
            if (cartData == null) {
                console.log("=== Invalid data");
                return 0;
            }
            console.log("=== Saving to cart");
            console.log(cartData);

            $.ajax({
                type: "POST",
                url: "/ajax/cart/" + action,
                data: {
                    'product_id': cartData.pId,
                    'product_name': cartData.pName,
                    'product_quantity': cartData.pQuantity,
                    'product_price': cartData.pPrice,
                },
                dataType: "json",
                success: function(response) {
                    if (response.status != "OK") {

                        if (response.status == "REMOVED") {
                            showSnackbar("Produit Retiré", "bg-danger");
                            $(".cart-badge").html(response.data.itemCount);
                            return false;
                        }
                        console.log(">>>> errerur");
                        showSnackbar("Erreur, Veuillez réessayer", "bg-danger", 4000);
                        console.log(response);
                        return false;
                    }

                    // showSnackbar("Erreur, Veuillez réessayer", "bg-danger", 4000);
                    console.log(">>>> SUCCESS  ");
                    console.log(response);
                    $(".cart-badge").html(response.data.itemCount);
                    // window.location.reload();
                },
                error: function(response) {
                    console.log("Error :");
                    console.log(response);
                },
            });
        }

    </script>
@endsection
