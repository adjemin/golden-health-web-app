@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
<link media="all" rel="stylesheet" href="{{asset('css/plugins.css')}}">
<!-- Custom css -->
<link media="all" rel="stylesheet" href="{{asset('css/shop.css')}}">
<style media="screen">
@media (min-width: 992px) {
  .common-box {
    padding-bottom: 10px ! important;
  }
}

</style>
<script src="https://cdn.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"/>
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
@endsection

@section('content')
<main class="container-fluid py-5">
  <!-- Page Banner -->
      <div class="page-banner">
          <div class="banner-caption common-box">
              <div class="container">

                  <h1>Checkout</h1>
                  @if(Auth::guard('customer')->check())
                  @else
                    <p class="mt-3 font-weight-500 opacity-9"> Êtes-vous un client ? <a href="{{route('customer.redirectTo', ['url' => Request::url()])}}"
                        class="btn btn-primary rounded btn-sm mx-2 shadow">Se connecter</a> pour continuer
                    </p>
                    @endif
              </div>
          </div>
      </div>
      <!-- //-End Page Banner -->

        <!-- Content -->
        <div class="content common-box pt-0">
            <div class="container">
                <div class="row flex-lg-row-reverse">
                  @if (App\ShoppingCart::ShoppingCart()->items->count() > 0)
                    <div class="col-lg-5">
                        <div class="common-module order-preview-module mb-0">
                            <h4 class="module-title text-uppercase">Résumé commande</h4>
                            <ul>
                              @foreach (App\ShoppingCart::ShoppingCart()->items as $item)
                                <li>
                                    <div class="item">
                                        <div class="intro-image"><img src="{{ $item->product->getImageUrl() ?? asset('customer/images/pair.jpg') }}" alt="img dis">
                                        </div>
                                        <div class="intro-text">
                                            <h4><a href="/product_details/{{$item->product->slug}}">{{$item->product->title}}</a></h4>

                                            <span class="qnty">Quantité: {{$item->product->currentCartItem()->quantity}}</span>
                                            <span class="price">{{$item->amount}} CFA</span>
                                            <span style="width:10px;height:10px;background-color:{{$item->getColor()->code_color ?? ''}};"></span>
                                            <span>{{$item->size}}</span>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                            <div class="grand-total">
                                <ul>
                                    <li>
                                        <span class="content-title">Sous-total</span>
                                        <span class="value text-primary">{{App\ShoppingCart::ShoppingCart()->getSousTotal()}} CFA</span>
                                    </li>
                                    <li>
                                        <span class="content-title">Frais de livraison</span>
                                        <span class="value">{{App\ShoppingCart::ShoppingCart()->getDeliveryFeeTotal()}} CFA</span>
                                    </li>
                                    <!--li>
                                        <span class="content-title">20% Discount</span>
                                        <span class="value text-primary">- $ 24</span>
                                    </li-->
                                    <li>
                                        <span class="content-title">Total:</span>
                                        <span class="value text-primary">{{App\ShoppingCart::ShoppingCart()->getTotal()}} CFA</span>
                                    </li>
                                </ul>
                            </div>
                            <a href="/basket"
                                class="btn btn-secondary btn-block shadow  btn-md  rounded font-weight-600 mt-6 text-uppercase mt-2">Modifier
                                mon panier</a>
                        </div>
                    </div>
                    @endif
                    <div class="col-lg-7">
                        <form action="#" class="checkout-module">
                          <div class="common-module bg-light">
                            <h4 class="module-title">Information de livraison</h4>
                            <div class="form-row">
                                <div class="col-12">
                                    <label for="email_address">
                                        <h5>Adresse de livraison</h5>
                                    </label>
                                </div>
                                <div class="col-md-12">
                                    <div class="input-group">
                                    @if(Auth::guard('customer')->user())
                                        @if(!is_null(Auth::guard('customer')->user()->location_latitude) && !is_null(Auth::guard('customer')->user()->location_longitude) && !is_null(Auth::guard('customer')->user()->location_address))
                                            <input type="text" id="locat_delivery" name="address_delivery_name" class="form-control no-display-input mb-3" placeholder="Adresse de livraison " value="{{old('address_delivery_name') ?? Auth::guard('customer')->user()->location_address ?? null}}" readonly>
                                        @else
                                            <input type="text" id="locat_delivery" name="address_delivery_name" class="form-control no-display-input mb-3" placeholder="Adresse de livraison " readonly>
                                        @endif
                                    @else
                                        <input type="text" id="locat_delivery" name="address_delivery_name" class="form-control no-display-input mb-3" placeholder="Adresse de livraison " readonly>
                                    @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="detail">
                                        <h5>Informations complementaires sur l'addresse de livraison</h5>
                                    </label>
                                </div>
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <textarea style="margin-bottom: 15px;" class="form-control" name="detail" id="detail"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="input-group">
                                          @if(Auth::guard('customer')->user())
                                              @if(!is_null(Auth::guard('customer')->user()->location_latitude) && !is_null(Auth::guard('customer')->user()->location_longitude) && !is_null(Auth::guard('customer')->user()->location_address))
                                                  <input type="text" id="addr_delivery" name="addr_delivery" required class="form-control" placeholder="Lieu de livraison" data-toggle="modal_delivery" data-target="#Modal_delivery" onkeyup="start_search();" value="{{old('addr_delivery') ?? Auth::guard('customer')->user()->location_address ?? null}}"/>
                                              @else
                                                  <input type="text" id="addr_delivery" name="addr_delivery" required class="form-control" placeholder="Lieu de livraison" data-toggle="modal_delivery" data-target="#Modal_delivery" onkeyup="start_search();"/>
                                              @endif
                                          @else
                                              <input type="text" id="addr_delivery" name="addr_delivery" required class="form-control" placeholder="Lieu de livraison" data-toggle="modal_delivery" data-target="#Modal_delivery" onkeyup="start_search();"/>
                                          @endif
                                        <div class="input-group-append">
                                            <button type="button" onclick="addr_delivery_search();" class="btn btn-secondary">Rechercher</button>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <div id="error-msg" class="text-danger font-weight-bold"></div>
                                    <div class="">
                                        <div id="results_delivery"></div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                @if(Auth::guard('customer')->user())
                                    @if(!is_null(Auth::guard('customer')->user()->location_latitude))
                                        <input type="hidden" id="lat_delivery" name="location_delivery_lat" value="{{old('location_delivery_lat') ?? Auth::guard('customer')->user()->location_latitude ?? 5.40911790}}"/>
                                    @else
                                        <input type="hidden" id="lat_delivery" name="location_delivery_lat" value="{{old('location_delivery_lat') ?? 5.40911790}}"/>
                                    @endif

                                    @if(!is_null(Auth::guard('customer')->user()->location_longitude))
                                        <input type="hidden" id="lon_delivery" name="location_delivery_lng" value="{{old('location_delivery_lng') ?? Auth::guard('customer')->user()->location_longitude ?? -4.04220990}}"/>
                                    @else
                                        <input type="hidden" id="lon_delivery" name="location_delivery_lng" value="{{old('location_delivery_lng') ?? -4.04220990}}"/>
                                    @endif
                                @else
                                    <input type="hidden" id="lat_delivery" name="location_delivery_lat" value="{{old('location_delivery_lat') ?? 5.40911790}}"/>
                                    <input type="hidden" id="lon_delivery" name="location_delivery_lng" value="{{old('location_delivery_lng') ?? -4.04220990}}"/>
                                @endif
                            </div>

                            <div class="row mt-4">

                                <div class="container">
                                    <div class="b-fg">
                                        <div style="height:200px; width: 100%" id="map_delivery"></div>
                                    </div>
                                </div>
                            </div>
                            
                          </div>
                          
                            <!--div class="common-module bg-light">
                                <h4 class="module-title">Information de livraison</h4>
                                <div class="form-row">
                                    <div class="col-12">
                                        <label for="full_name">
                                            <h5>Full Name*</h5>
                                        </label>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <select class="form-control" id="full_name" required>
                                            <option value="">Title*</option>
                                            <option value="Mr">Mr </option>
                                            <option value="Mrs"> Mrs </option>
                                            <option value="Ms"> Ms </option>
                                        </select>
                                    </div>
                                    <div class="col-md-5 form-group">
                                        <input type="text" class="form-control" placeholder="First Name*">
                                    </div>
                                    <div class="col-md-5 form-group">
                                        <input type="text" class="form-control" placeholder="Last Name*">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-12">
                                        <label for="email_address">
                                            <h5>Email Address*</h5>
                                        </label>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <input type="email" class="form-control" placeholder="Your email*"
                                            id="email_address">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-12">
                                        <label for="contact_number">
                                            <h5>Contact Number*</h5>
                                        </label>
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <input type="number" class="form-control" placeholder="Country Code*"
                                            id="contact_number">

                                    </div>
                                    <div class="col-md-9 form-group">
                                        <input type="tel" class="form-control" placeholder="Mobile Number*">
                                    </div>
                                </div>

                                <div class="form-row">

                                    <div class="col-12">
                                        <label for="message">
                                            <h5>Message</h5>
                                        </label>

                                        <textarea class="form-control" rows="7" id="message"
                                            placeholder="Max characters length only 500 "></textarea>
                                    </div>
                                </div>
                            </div -->

                            <!--div class="from-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="terms">
                                    <label class="custom-control-label" for="terms">I accept the <a href="#">terms and
                                            Conditions</a></label>
                                </div>
                            </div-->
                            
                        </form>
                        <div class="row my-4">
                            <p>
                                Entrez votre coupon ci-dessous et bénéficiez d'une réduction
                            </p>
                            <form class="form-inline coupne-form">
                                <input type="text" class="form-control" id="coupon_code" name="coupon_code"  placeholder="Entrez votre code"/>
                                <button type="button" class="btn btn-article shadow border-0 btn-md text-uppercase font-weight-600 rounded" id="submitCoupon">Appliquer</button>
                            </form>
                            <p id="coupon_error" class="text-danger font-weight-bold mt-1"></p>
                            @forelse($coupons as $coupon)
                                <div class="text-primary font-weight-bold text-uppercase my-2 text-right">
                                    coupon
                                    {{$coupon->code."  (-".$coupon->pourcentage."%) "}} Ajouté
                                </div>
                            @empty
                            @endforelse
                          </div>
                        </div>
                        <div class="row mt-4">
                            @if(Auth::guard('customer')->check())
                                <a id="payementCash" class="btn btn-primary btn-md rounded font-weight-600 mt-6 text-uppercase mx-2" style="color:#fff">
                                    Payer en espèce
                                </a>
                                <a id="BtnAdjeminPay" class="btn btn-secondary btn-md rounded font-weight-600 mt-6 text-uppercase" style="color:#fff">
                                    Payer en ligne
                                </a>
                            @else
                                <a href="{{route('customer.redirectTo', ['url' => Request::url()])}}" class="btn btn-secondary btn-md btn-block rounded font-weight-600 mt-6 text-uppercase" style="color:#fff">
                                    Inscrivez-vous pour finaliser la commande
                                </a>
                            @endif
                        </div>

                </div>
            </div>
        </div>
        <!-- //- End Content -->

</main>

@endsection

@section('scripts')
<script src="{{asset('js/plugins.js')}}"></script>
<script src="{{asset('js/shop-init.js')}}"></script>
<script>
    $token = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });

    $('#payementCash').on('click', function(e){
        console.log("Hellloooooo")
        var deliveryAddress = $("input[name='address_delivery_name']").val();
        var deliveryAddressLat = $("input[name='location_delivery_lat']").val();
        var deliveryAddressLng = $("input[name='location_delivery_lng']").val();
        var deliveryDetail= $("input[name='detail']").val();
        
        if(deliveryAddress == '' || deliveryAddress == null){
            
            $("#error-msg").html("Veuillez choisir une adresse de livraison");
            Swal.fire({
                icon: 'error',
                title: 'Adresse de livraison',
                text: 'Veuillez veuillez choisir votre adresse de livraison avant de finaliser votre commande svp.',
                showConfirmButton: false
            });
            return false;
        }
        else if(deliveryAddressLat == '' || deliveryAddressLat == null){
            //
            $("#error-msg").html("Veuillez choisir une adresse de livraison");
            Swal.fire({
                icon: 'error',
                title: 'Adresse de livraison',
                text: 'Veuillez veuillez choisir votre adresse de livraison avant de finaliser votre commande svp.',
                showConfirmButton: false
            });
            return false;
        }
        else if(deliveryAddressLng == '' || deliveryAddressLng == null){
            //
            $("#error-msg").html("Veuillez choisir une adresse de livraison");
            return false;
        }

        $.ajax({
                type: "POST",
                url: "{{ route('ajax.shop.createOrder') }}",
                data: {
                    "payment_method" : "cash",
                    "delivery_name": deliveryAddress,
                    "delivery_lat": deliveryAddressLat,
                    "delivery_lng": deliveryAddressLng,
                    "delivery_detail": deliveryDetail
                },
                dataType: "json",
                success: function (response) {
                    console.log("Ressponse succes");
                    console.log(response);
                    if(response.status != "OK"){
                        if(response.status == "ALREADY_EXISTS"){
                            Swal.fire({
                                icon: 'error',
                                title: 'Commande',
                                text: 'Désolé, cette commande existe déja.',
                                showConfirmButton: false
                            });
                            
                        }
                        // btn.html("Erreur");

                        setTimeout(() => {
                            btn.html("Réessayer");
                        }, 1000);
                        return false;
                    }else{
                        Swal.fire({
                            icon: 'success',
                            title: 'Enregistrement effectué avec succès!',
                            text: 'Votre commande a été prise en compte, vous récevrez un mail de confirmation.',
                            showConfirmButton: false
                        });
                        setTimeout(function() {
                            window.location.replace('/customer/shop/checkout_finish/'+response.data.transaction.id);
                        }, 500);
                        
                    }
                    
    

                   
                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                    btn.html("Erreur");
                    setTimeout(() => {
                        btn.html("Réessayer");
                    }, 2000);
                },
        });
    })

    //var AdjeminPay = AdjeminPay();

    // AdjeminPay.on('init', function (e) {
    //     // retourne une erreur au cas où votre API_KEY ou APPLICATION_ID est incorrecte
    //     console.log(e);
    // });

    // Lance une requete ajax pour vérifier votre API_KEY et APPLICATION_ID et initie le paiement
    // AdjeminPay.init({
    //     apikey: "eyJpdiI6Ik5ObTNmMTFtMFwvV2ZkU3RJS",
    //     application_id: "2f699e",
    //     notify_url: "https://goldenhealth.adjemincloud.com/api/invoice_payments_notify"
    // });

    function start_search(){
        //
        var val = $("input[name='addr_delivery']").val();
        if(val.length > 4){
            addr_delivery_search();
        }
    }

    
    $("#submitCoupon").click(function name(params) {
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;

        var coupon_code = $("input[name='coupon_code']").val();

        $(this).html(_cuteLoader);
        var $btn = $(this);
        var $messageDiv = $("#coupon_error");

        if(coupon_code == ''){
            $("#coupon_error").html("Vous devez entrez un coupon");
            $("#submitCoupon").html("Vérifier")
            return false;
        }

        $.ajax({
                type: "POST",
                url: "{{ route('ajax.coupon.addStorage') }}",
                data: {
                    "coupon_code": coupon_code,
                },
                dataType: "json",
                success: function (response) {
                    //
                    console.log(">> success");
                    console.log(response);
                    if(response.status == null){
                        $btn.html("Veuillez réessayer plus tard");
                        $messageDiv.html("Une erreur est survenu");
                        return false;
                    }
                    // managing status codes
                    if(response.status != "OK"){
                        $btn.html("OUPS");
                        if(response.error != null){
                            if(response.error.message != null){
                                $messageDiv.html(response.error.message);
                                setTimeout(() => {
                                    $btn.html("Réessayer");
                                }, 1000);
                                return false;
                            }
                            $btn.html("Réessayer")
                        }

                    }
                    $btn.html("Coupon Ajouté");

                    window.location.reload();

                },
                error: function(response){
                    console.log(">> erreur");
                    $btn.html("Veuillez réessayer plus tard");
                    $messageDiv.html("Une erreur est survenue");
                    console.log(response);
                }
        });
    });  

    // ***** Adjemin Pay
    $("#BtnAdjeminPay").click(function(){
        console.log(">>> clicked");
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;



        var deliveryAddress = $("input[name='address_delivery_name']").val();
        var deliveryAddressLat = $("input[name='location_delivery_lat']").val();
        var deliveryAddressLng = $("input[name='location_delivery_lng']").val();

        if(deliveryAddress == '' || deliveryAddress == null){
            //
            $("#error-msg").html("Veuillez choisir une adresse de livraison");
            return false;
        }
        if(deliveryAddressLat == '' || deliveryAddressLat == null){
            //
            $("#error-msg").html("Veuillez choisir une adresse de livraison");
            return false;
        }
        if(deliveryAddressLng == '' || deliveryAddressLng == null){
            //
            $("#error-msg").html("Veuillez choisir une adresse de livraison");
            return false;
        }

        var btn = $(this);
        btn.html(_cuteLoader);

            $.ajax({
                type: "POST",
                url: "{{ route('ajax.shop.createOrder') }}",
                data: {
                    "payment_method" : "online",
                    "delivery_name": deliveryAddress,
                    "delivery_lat": deliveryAddressLat,
                    "delivery_lng": deliveryAddressLng
                },
                dataType: "json",
                success: function (response) {
                    console.log("Ressponse succes");
                    console.log(response);
                    if(response.status != "OK"){
                        if(response.status == "ALREADY_EXISTS"){
                            btn.html("Acheter <i class='material-icons ml-2'>check</i>");
                            return false;
                        }
                        // btn.html("Erreur");

                        setTimeout(() => {
                            btn.html("Réessayer");
                        }, 1000);
                        return false;
                    }

                    if(response.data.transaction != null){
                         // pay with adjeminpay
                         let amount = 0;
                         if(parseInt(response.data.transaction.discount) != 0 && response.data.transaction.discount != undefined){
                            amount = response.data.transaction.amount - parseInt(response.data.transaction.discount)
                         }else{
                            amount = response.data.transaction.amount;
                         }

                         payWithAdjeminPay({
                             amount: amount,
                             transactionId: response.data.transaction.id,
                             currency: response.data.transaction.currency,
                             designation: response.data.transaction.designation,
                             customField: response.data.status
                         });
                     }
                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                    btn.html("Erreur");
                    setTimeout(() => {
                        btn.html("Réessayer");
                    }, 2000);
                },
            });
    });

    function payWithAdjeminPay(transactionData){
        if(!transactionData){
            alert(">>>> erreur paiement, veuillez réessayer");
            console.log("<< clicked");
        }
        console.log(">>> clicked");

        // Ecoute le feedback sur les erreurs
        AdjeminPay.on('error', function (e) {
            // la fonction que vous définirez ici sera exécutée en cas d'erreur
            console.log(e);
            $("#result-title").html(e.title);
            $("#result-message").html(e.data);
            $("#result-status").html(e.status);
            $("#BtnAdjeminPay").html("Vous allez être redirigé vers le panier");
            console.log(">>> Error");

            return false;
        });

        // Lancer la procédure de paiement au click
        AdjeminPay.preparePayment({
            amount: transactionData.amount,
            transaction_id: transactionData.transactionId,
            currency: transactionData.currency,
            designation: transactionData.designation,
            custom: transactionData.customField
        });

        // Si l'étape précédante n'a pas d'erreur,
        // cette ligne génère et affiche l'interface de paiement AdjeminPay
        AdjeminPay.renderPaymentView();


        // Payment terminé
        AdjeminPay.on('paymentTerminated', function (e) {
            console.log('<<<<<<< Terminated !');
            console.log(e);
            //
            // Ref and status
            adjeminPaymentNotify(transactionData.transactionId, e);
        });
    }

    function adjeminPaymentNotify(reference, data){
        //
        if(reference == null){
            console.log("<<< REf null");
            return false;
        }
        if(data == null){
            console.log("<<< Data null");
            return false;
        }
        if(data.status == null){
            console.log("<<< Status null");
            return false;
        }
        var trans = {
            'id': reference,
            'status': data.status
        }

        console.log(">>> transData");
        console.log(trans);

        $.ajax({
                type: "POST",
                url: "{{ route('api.payments.notify') }}",
                data: {
                    "transaction_id": trans.id,
                    "status": trans.status,
                },
                dataType: "json",
                success: function (response) {
                    console.log("Response succes <<<");
                    console.log(response);
                    if(response.error != null){
                        $btn.html(response.error.message);
                    }
                    if(response.status != null){
                        if(response.status == "OK"){
                            // Correctly notified
                            // now redirect according to status
                            console.log(">>>>> PAYMENT SUCCESS");
                            window.location.replace('/customer/shop/checkout_finish/'+trans.id);
                        }
                    }
                    window.location.replace('/customer/shop/checkout_finish/'+trans.id);
                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                    $btn.html("Erreur");
                    setTimeout(() => {
                        $btn.html("Réessayer");
                    }, 2000);
                },
            });
    }
</script>

{{-- Gestion livraison --}}
<script type="text/javascript">
    // *** Delivery address map init
    // Abidjan
    var startlat_delivery =  document.getElementById('lat_delivery').value != null ? document.getElementById('lat_delivery').value : 5.40911790;
    var startlon_delivery = document.getElementById('lon_delivery').value != null ? document.getElementById('lon_delivery').value : -4.04220990;
    console.log(startlat_delivery)
    console.log(startlon_delivery)

    var options_delivery = {
        center: [startlat_delivery, startlon_delivery],
        // zoom: 10
        zoom: 12
    }

    document.getElementById('lat_delivery').value = startlat_delivery;
    document.getElementById('lon_delivery').value = startlon_delivery;

    var map_delivery = L.map('map_delivery', options_delivery);
    var nzoom = 12;

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: 'OSM'}).addTo(map_delivery);
    // L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: 'OSM'}).addTo(map_delivery);

    var my_delivery_Marker = L.marker([startlat_delivery, startlon_delivery], {
        title: "Coordinates",
        alt: "Coordinates",
        draggable: true
    }).addTo(map_delivery).on('dragend', function () {
        var lat_delivery = my_delivery_Marker.getLatLng().lat_delivery.toFixed(8);
        var lon_delivery = my_delivery_Marker.getLatLng().lng.toFixed(8);
        var czoom = map_delivery.getZoom();

        if (czoom < 18) {
            nzoom = czoom + 2;
        }
        if (nzoom > 18) {
            nzoom = 18;
        }
        if (czoom != 18) {
            map_delivery.setView([lat_delivery, lon_delivery], nzoom);
        } else {
            map_delivery.setView([lat_delivery, lon_delivery]);
        }
        document.getElementById('lat_delivery').value = lat_delivery;
        document.getElementById('lon_delivery').value = lon_delivery;
        my_delivery_Marker.bindPopup("Lat " + lat_delivery + "<br />Lon " + lon_delivery).openPopup();
    });

    function choose_delivery_Addr(lat1, lng1) {
        my_delivery_Marker.closePopup();
        map_delivery.setView([lat1, lng1], 18);
        my_delivery_Marker.setLatLng([lat1, lng1]);
        lat = lat1.toFixed(8);
        lon = lng1.toFixed(8);
        document.getElementById('lat_delivery').value = lat;
        document.getElementById('lon_delivery').value = lon;
        //document.getElementById('locat').value = addrn;
        //alert(addrn);
        my_delivery_Marker.bindPopup("Lat " + lat + "<br />Lon " + lon).openPopup();

        $.get('https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=' + lat + '&lon=' + lon, function (data) {
            console.log(data);
            // document.getElementById('locat').value = data.address.city + ',' + data.address.state + ',' + data.address.country;
            document.getElementById('locat_delivery').value = data.display_name;
        });
        //alert(i);
        document.getElementById('results_delivery').innerHTML = '';
    //document.getElementById('location').submit();
    }

    function lolp(ii) {
        alert(ii);
    }

    function loca(i) {
        alert(i);
    }

    function my_delivery_Function(arr) {
        var out = "<br />";
        var i;

        if (arr.length > 0) {
            for (i = 0; i < arr.length; i++) {
                out += "<div class='address' title='Show Location and Coordinates' style='cursor:pointer' value='" + arr[i].display_name + "' onclick='choose_delivery_Addr(" + arr[i].lat + ", " + arr[i].lon + ");return false;'>" + arr[i].display_name + "</div>";
            }
            document.getElementById('results_delivery').innerHTML = out;
        } else {
            document.getElementById('results_delivery').innerHTML = "<div class='address'>Aucune adresse trouvée...</div>";
        }

    }

    function addr_delivery_search() {
        var inp = document.getElementById("addr_delivery");
        var xmlhttp = new XMLHttpRequest();
        var url = "https://nominatim.openstreetmap.org/search?format=json&limit=3&q=" + inp.value;
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var my_delivery_Arr = JSON.parse(this.responseText);
                my_delivery_Function(my_delivery_Arr);
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }
</script>
{{-- Coupon adding Script --}}
{{--<script>
    $("#submitCoupon").click(function name(params) {
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;

        var coupon_code = $("input[name='coupon_code']").val();
        var invoice_id = $("input[name='invoice_id'").val();

        $(this).html(_cuteLoader);
        var $btn = $(this);
        var $messageDiv = $("#coupon_error");

        if(coupon_code == ''){
            $("#coupon_error").html("Vous devez entrez un coupon");
            $("#submitCoupon").html("Vérifier")
            return false;
        }
        if(invoice_id == ''){
            $("#coupon_error").html("Erreur survenue");
            $("#submitCoupon").html("Veuillez réessayer plus tard")
            return false;
        }


        $.ajax({
                type: "POST",
                url: "{{ route('ajax.coupon.add') }}",
                data: {
                    "invoice_id": invoice_id,
                    "coupon_code": coupon_code,
                    '_token': "{{ csrf_token() }}"
                },
                dataType: "json",
                success: function (response) {
                    //
                    console.log(">> success");
                    console.log(response);
                    if(response.status == null){
                        $btn.html("Veuillez réessayer plus tard");
                        $messageDiv.html("Une erreur est survenu");
                        return false;
                    }
                    // managing status codes
                    if(response.status != "OK"){
                        $btn.html("OUPS");
                        if(response.error != null){
                            if(response.error.message != null){
                                $messageDiv.html(response.error.message);
                                setTimeout(() => {
                                    $btn.html("Réessayer");
                                }, 1000);
                                return false;
                            }
                            $btn.html("Réessayer")
                        }

                    }
                    $btn.html("Coupon Ajouté");

                    window.location.reload();

                },
                error: function(response){
                    console.log(">> erreur");
                    $btn.html("Veuillez réessayer plus tard");
                    $messageDiv.html("Une erreur est survenue");
                    console.log(response);
                }
        });
    });
</script>--}}

{{-- Color inputs css --}}
<script>
    // Radio color inputs
    var colorSpans = document.getElementsByClassName('span-color');
    for(var c = 0; c < colorSpans.length; c++){
        //
        colorSpans[c].style.background = colorSpans[c].dataset.color;
    }
</script>


@endsection
