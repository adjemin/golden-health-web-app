@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
<style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }
    a {
        color: black;
    }

    .circle {
            width: 50px;
            height: 50px;
            border-radius: 50%;
            border: 1px solid;
        }

</style>
<style>
    /* Color radio inputs */

    input[type=radio] {
    display: none;
    }
    input[type=radio]:checked + label span {
    transform: scale(1.25);
    border: 2px solid var(--secondary);
    border: 2px solid #edaa0d;
    }

    label {
        display: inline-block;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        margin-right: 10px;
        cursor: pointer;
    }
    label:hover span {
        transform: scale(1.25);
    }
    label span {
        display: block;
        border-radius: 50%;
        width: 100%;
        height: 100%;
        transition: transform 0.2s ease-in-out;
    }
    label span.span-size {
        display: block;
        border-radius: 0%;
        width: 100%;
        height: 100%;
        transition: transform 0.2s ease-in-out;
    }
</style>
@include('customer_frontOffice.layouts_customer.inc.slideshow_css')
@endsection

@section('content')

<main class="container py-5">
    <section class="">
        {{-- Breadcrumb --}}
        <ul class="breadcrumb">
            <li><a href="/">Accueil</a></li>
            <li><a href="/shop">Boutique</a></li>
            {{--<li><a href="/tous_les_articles/">Catégorie</a></li>--}}
            <li><a>{{$product->title}}</a></li>
        </ul>
        @csrf
    </section>

    <section class="">
        <div class="container">
            <div class="row product-card r">
                <div class="col-lg-5 col-md-5 col-sm-12 d-flex flex-column justify-content-between">
                    <div class="">
                        @if($product->hasMultipleImages())
                            @include('customer_frontOffice.layouts_customer.inc.slideshow', ['imageData' => $product->imageData()])
                        @else
                            <div class="">
                                <img class="img-fluid r" src="{{$product->getImageUrl() }}" alt=""/>
                            </div>
                        @endif
                    </div>
                    <div class="">
                        <div class="mt-5 p-2 r b-bg">
                            <div class="d-flex justify-content-between align-items-center">
                                <span class="fs-l fw-500">
                                    PARTAGER
                                </span>
                                <div>
                                    <a href="#"><img src="{{ asset('customer/images/icons8-facebook-48 (1).png') }}" height="30" width="30" alt="" class="mr-3"></a>
                                    <a href="#"><img src="{{ asset('customer/images/icons8-instagram-52.png') }}" height="30" width="30" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-7 col-md-7 col-sm-12 d-flex flex-column justify-content-between">
                    <div class="">
                        <p class="product-name text-uppercase fw-500 d-flex justify-content-between">
                            {{$product->title}}
                        </p>
                        <div class="my-2">
                            <div class="d-flex pl-5">
                                {!!$product->starRatingWidget()!!}
                            </div>
                            {!!$product->wishlistWidget()!!}
                        </div>
                        <div class="row container">
                            <div class="col-md">
                                <div class="form-group">
                                    {{-- {!! Form::label('Tailles:') !!}
                                    <p class="font-weight-bold article-title text-primary mb-1 text-capitalize">
                                        {{ $product->sizes }}
                                    </p> --}}
                                    @if($product->hasSizes())
                                        <p>Choisissez une taille</p>
                                        @foreach($product->sizes() as $size)
                                            <div class="container">
                                                {{-- Size choice line + color --}}
                                                <div class="row d-flex justify-content-between align-items-center">
                                                <input class="radio-size" type="radio" data-detail-name="taille" name="{{ 'product_'.$product->id.'_size'}}" data-product-id="{{$product->id}}" value="{{$size}}" id="{{$product->id.$size}}" />

                                                        <label for="{{$product->id.$size}}" class="chooseDetail">
                                                            <span class="span-size" data-color="{{$size}}">
                                                                {{$size}}
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>

                            <div class="col-md">
                                <div class="form-group">
                                    {{-- {!! Form::label('Couleurs:') !!}
                                    <div class="row">
                                        @foreach ($product->getProductColor() as $color)
                                            <div class="circle m-1" style="background:{{ $color->code_color }}"></div>
                                        @endforeach
                                    </div> --}}
                                    @if($product->hasColors())
                                        <p>Couleurs</p>
                                        @if($product->getProductColor())
                                            <div class="d-flex align-items-center mr-3">
                                                @foreach($product->getProductColor() as $color)
                                                    <div class="mr-1">
                                                    <input class="radio-color" type="radio" data-detail-name="couleur" data-product-id="{{$product->id}}" name="{{ 'product_'.$product->id.'_color'}}" value="{{$color->name}}" id="{{$product->id.$color->name}}"
                                                    />

                                                        <label for="{{$product->id.$color->name}}" class="chooseDetail">
                                                            <span class="span-color" data-color="{{$color->name}}"></span>
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        <h3 class="mb-3 text-primary fw-600">
                            @if($product->isPromoted())
                                <span class="mr-3 r p-1 bg-secondary">
                                    {{"-".$product->promo_percentage."%"}}
                                </span>
                            @endif
                            @if($product->isPromoted())
                                <del class="mr-3">
                                    {{$product->price}} FCFA
                                </del>
                            @endif
                            {{$product->getPrice()}} FCFA

                        </h3>
                        <p class="fs-l">
                            {!!$product->description!!}
                        </p>
                    </div>
                    <div class="">
                        <hr>
                        <div class="d-flex justify-content-between">
                            <a onclick="history.go(-1);" class="btn-article btn-primary col-lg-5 ml-0">
                                Continuer le shopping
                            </a>
                            <a href="/basket/add/{{$product->id}}" class="btn-article btn-secondary col-lg-5 mr-0">
                                Passer la commande
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- Articles Similaires --}}
    <div class="section py-5">
        <div class="container r b-fg p-3">
            <h5 class="text-uppercase">Articles Similaires</h5>
            <div class="small-secondary-dividers mb-3"></div>
            <div class="row">
                @foreach($product->getSimilarProducts() as $similar_product)
                    @include('customer_frontOffice.shops.components.similar_product', ['product' => $similar_product])
                @endforeach
            </div>
        </div>
    </div>
</main>

<script>
    // Radio color inputs
    var colorSpans = document.getElementsByClassName('span-color');
    for(var c = 0; c < colorSpans.length; c++){

        colorSpans[c].style.background = colorSpans[c].dataset.color;
    }
</script>
<script>
    // Add color
    $(".chooseDetail").click(function (){
        console.log(">> choosing detail");
        thisBtn = $(this);
        itemId = $(this).prev().data('item-id');
        detailName = $(this).prev().data('detail-name');
        detailContent = $(this).prev().val();
        console.log(detailName);
        console.log(detailContent);
        console.log(itemId);

        // return false;

            $.ajax({
                type: "POST",
                url: "{{ route('ajax.cart.addDetail') }}",
                data: {
                    "item_id": itemId,
                    "detail_name": detailName,
                    "detail_content": detailContent,
                },
                dataType: "json",
                success: function (response) {
                    console.log("Ressponse succes");
                    console.log(response);
                    if(response.status != "OK"){

                        showSnackbar("Erreur", "bg-danger");
                        thisBtn.prev().prop("checked", false);
                        return false;
                    }

                    showSnackbar(detailName+" "+detailContent+" ajouté");
                    thisBtn.prev().prop("checked", true);
                    return true;
                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                    btn.html("Erreur");
                    setTimeout(() => {
                        btn.html("Réessayer");
                    }, 2000);
                },
            });
    });

    // Add size to article
</script>


@include('customer_frontOffice.layouts_customer.inc.slideshow_js')
@endsection
