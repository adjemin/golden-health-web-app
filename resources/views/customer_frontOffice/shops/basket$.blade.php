@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
<style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }

    a {
        color: black;
    }
    .no-display-input{
        /*  */
        border: none !important;
        display: inline-block;
    }
    .address{
        padding: 10px;
    }
    .address:hover{
        background-color: white;
    }
</style>
<style>
    /* Choose color and size */
    /* Color radio inputs */

    input[type=radio] {
    display: none;
    }
    input[type=radio]:checked + label span {
    transform: scale(1.25);
    border: 2px solid var(--secondary);
    }
    input[type=radio]:checked + label .red {
    border: 2px solid #fff;
    }
    input[type=radio]:checked + label .orange {
    border: 2px solid #873a08;
    }
    input[type=radio]:checked + label .yellow {
    border: 2px solid #816102;
    }
    input[type=radio]:checked + label .olive {
    border: 2px solid #505a0b;
    }
    input[type=radio]:checked + label .green {
    border: 2px solid #0e4e1d;
    }
    input[type=radio]:checked + label .teal {
    border: 2px solid #003633;
    }
    input[type=radio]:checked + label .blue {
    border: 2px solid #103f62;
    }
    input[type=radio]:checked + label .violet {
    border: 2px solid #321a64;
    }
    input[type=radio]:checked + label .purple {
    border: 2px solid #501962;
    }
    input[type=radio]:checked + label .pink {
    border: 2px solid #851554;
    }

    label {
        display: inline-block;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        margin-right: 10px;
        cursor: pointer;
    }
    label:hover span {
        transform: scale(1.25);
    }
    label span {
        display: block;
        border-radius: 50%;
        width: 100%;
        height: 100%;
        transition: transform 0.2s ease-in-out;
    }
</style>
<script src="https://cdn.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"/>
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>

@endsection

@section('content')
<main class="container">
    <section class="mt-5">
        {{-- Breadcrumb --}}
        <ul class="breadcrumb">

            <li><a href="/">Accueil</a></li>
            <li><a href="/shop">Boutique</a></li>
            <li><a href="/tous_les_articles/">Panier</a></li>
        </ul>
        @csrf
    </section>
    <section class="container py-2 r bg-white my-2">
        <div class="d-flex justify-content-between align-items-center">

            <span class="h5 font-weight-bold">Panier
                <span class="font-weight-bold">({{App\ShoppingCart::ShoppingCart()->items->count()}} Articles)</span>
            </span>
            @if (App\ShoppingCart::ShoppingCart()->count() > 0)
                <a id="btnCartClear" class="text-danger text-uppercase fw-600 fs-l btn-icon">
                    Tout supprimer
                    <i class="material-icons ml-2">delete</i>
                </a>
            @endif
        </div>
    </section>
    @if (App\ShoppingCart::ShoppingCart()->items->count() > 0)
        <section class="py-3 r">
            <div class="container">
                @foreach (App\ShoppingCart::ShoppingCart()->items as $item)

                    @if($item->product->sizes() && $item->product->sizes()->count() > 0)
                        {{-- @for($i = 0; $i< $item->quantity; $i++) --}}
                        @foreach($item->product->sizes() as $size)
                            <div class="row mb-3 b-fg r d-flex align-items-center">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="{{ $item->product->getImageUrl() ?? asset('customer/images/pair.jpg') }}" height="120" width="100%" alt="" srcset="">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <p class="primary-text-color fs-xxl fw-600">{{$item->product->title}}</p>
                                        </div>
                                        <div class="row">
                                            <div class="ml-2">
                                                Taille :
                                                <span class="ml-2 font-weight-bold">{{$size}}</span>
                                            </div>
                                            <div class="ml-2">
                                                Couleur :
                                                @if($item->product->getProductColor())
                                                    <div class="d-flex align-items-center ml-2">
                                                        @foreach($item->product->getProductColor() as $color)
                                                            <div class="mr-1">
                                                                <input class="radio-color" type="radio" name="product_{{$size}}_color" value="{{$color->name}}" id="{{$item->product->id.$size.$color->name}}" />

                                                                <label for="{{$item->product->id.$size.$color->name}}">
                                                                    <span class="span-color" data-color="{{$color->name}}"></span>
                                                                </label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="d-flex align-items-center justify-content-end">
                                                <div class="fw-500 h4">
                                                    {{$item->product->price}} FCFA
                                                </div>
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <div class="my-md-0">
                                                        <div id="qtyController-{{$item->product->id}}" class="container">
                                                            <div class="mb-2 d-flex justify-content-between align-items-center">
                                                                <a href="/ajax/cart/decrement/{{$item->id}}" class="cartQtyDecrement btn-article py-1 px-3 btn-secondary btn-icon fs-xxl">
                                                                    -
                                                                </a>
                                                                <span class="fs-xxl fw-500 mx-5 mx-md-3">
                                                                    {{$item->product->currentCartItem()->quantity}}
                                                                </span>
                                                                <a  href="/ajax/cart/increment/{{$item->id}}"  class="cartQtyIncrement btn-article py-1 px-3 btn-secondary btn-icon fs-xxl">
                                                                    +
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between">
                                                        <p class="font-weight-bold h5 ">{{$item->amount}} FCFA </p>
                                                        <a href="/ajax/cart/remove/{{$item->id}}" class="mx-md-4">
                                                            <i class="material-icons text-danger">delete</i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {{-- @endfor --}}
                        @endforeach
                    @else
                        <div class="row mb-3 b-fg r d-flex align-items-center">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="{{ $item->product->getImageUrl() ?? asset('customer/images/pair.jpg') }}" height="120" width="100%" alt="" srcset="">
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <p class="primary-text-color fs-xxl fw-600">{{$item->product->title}}</p>
                                    </div>

                                    <div class="row">
                                        <div class="d-flex align-items-center justify-content-end">
                                            <div class="fw-500 h4">
                                                {{$item->product->price}} FCFA
                                            </div>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="my-md-0">
                                                    <div id="qtyController-{{$item->product->id}}" class="container">
                                                        <div class="mb-2 d-flex justify-content-between align-items-center">
                                                            <a href="/ajax/cart/decrement/{{$item->id}}" class="cartQtyDecrement btn-article py-1 px-3 btn-secondary btn-icon fs-xxl">
                                                                -
                                                            </a>
                                                            <span class="fs-xxl fw-500 mx-5 mx-md-3">
                                                                {{$item->product->currentCartItem()->quantity}}
                                                            </span>
                                                            <a  href="/ajax/cart/increment/{{$item->id}}"  class="cartQtyIncrement btn-article py-1 px-3 btn-secondary btn-icon fs-xxl">
                                                                +
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <p class="font-weight-bold h5 ">{{$item->amount}} FCFA </p>
                                                    <a href="/ajax/cart/remove/{{$item->id}}" class="mx-md-4">
                                                        <i class="material-icons text-danger">delete</i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </section>

        <section>
            <div class="container">
                <div class="text-right mt-3">
                    <h5 class="font-weight-bold text-right">Total: <span class="secondary-text-color font-weight-bold ml-5">{{App\ShoppingCart::ShoppingCart()->getTotal()}} FCFA</span>
                    </h5>
                </div>
                <br>
        </section>
        {{-- Delivery / Livraison --}}
        <section class="mb-2">
            <fieldset id="fieldset_delivery">
                <div class="form-card">
                    <h2 class="fs-title">Livraison</h2>
                    <div class="container mt-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">

                                    <input type="text" id="locat_delivery" name="address_delivery_name" class="form-control no-display-input mb-3" placeholder="Adresse de livraison " readonly>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="text" id="addr_delivery" name="addr_delivery" required class="form-control" placeholder="Lieu de livraison" data-toggle="modal_delivery" data-target="#Modal_delivery" onkeyup="start_search();"/>

                                    <div class="input-group-append">
                                        <button type="button" onclick="addr_delivery_search();" class="btn btn-secondary">Rechercher</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="error-msg" class="text-danger font-weight-bold"></div>
                                <div class="">
                                    <div id="results_delivery"></div>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <input type="hidden" id="lat_delivery" name="location_delivery_lat"/>
                            <input type="hidden" id="lon_delivery" name="location_delivery_lng"/>
                        </div>
                        <div class="row">

                            <div class="container">
                                <div class="b-fg">
                                    <div style="height:200px; width: 100%" id="map_delivery"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </section>

        {{-- <section>
            <input type="text" name="city" class="form-control input-height" id="input_search" placeholder="Ville, commune, Quartier">
            <input type="hidden" name="lat" id="lat_input" >
            <input type="hidden" name="lon" id="lon_input">
                <div id="match-list" style="margin: 10px auto; width: 50%;position:absolute; top:70px;right:0">
                </div>
            <div class="input-group-append">
                <button type="submit" class="btn btn-secondary" type="button" id="submit_search">RECHERCHER</button>
            </div>
        </section> --}}
        <section class="mt-3">
                <div class="text-right">
                    <!-- Button trigger modal -->
                    @if(Auth::guard('customer')->check())
                        <a id="BtnAdjeminPay" class="btn-article m-w-150 btn-secondary px-2">
                            <i class="fa fa-money mr-2" style="font-size: 20px;"></i> Passer au paiement
                        </a>
                    @else
                        <a href="{{route('customer.redirectTo', ['url' => Request::url()])}}" class="btn-article m-w-150 btn-secondary px-2">
                            <i class="fa fa-money mr-2" style="font-size: 20px;"></i> Inscrivez-vous pour finaliser la commande
                        </a>
                    @endif

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="modal-body">
                                    <center>
                                        <img src="{{ asset('customer/images/icons8-instagram-check-mark-50.png') }}" height="70" width="70" alt="">
                                    </center>
                                    <p class="text-center mt-4 h6">Votre Commande a été validée</p>
                                </div>
                                <!-- <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <br><br>
            </div>
        </section>
    @else
        <section class="py-3 bg-white r">
            <div class="container">
                <div class="row product-card">
                        <div class="col-lg-5 col-md-5 mx-auto col-sm-12">

                            <img src="{{asset('customer/images/logo.png')}}" style="" class="img-fluid" alt="">
                        </div>
                        <div class="container text-center">
                            <h3>Vous n'avez pas d'article dans votre panier</h3>
                        </div>
                        <div class="container">
                            <div class="row">
                                <a href="/shop" class="btn-article btn-secondary mx-auto px-2">
                                    Ajouter des produits
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    @endif
</main>

@endsection

@section('scripts')
<script>
    $token = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });
    var AdjeminPay = AdjeminPay();

    AdjeminPay.on('init', function (e) {
        // retourne une erreur au cas où votre API_KEY ou APPLICATION_ID est incorrecte
        console.log(e);
    });

    // Lance une requete ajax pour vérifier votre API_KEY et APPLICATION_ID et initie le paiement
    AdjeminPay.init({
        apikey: "eyJpdiI6Ik5ObTNmMTFtMFwvV2ZkU3RJS",
        application_id: "2f699e",
        notify_url: "https://goldenhealth.adjemincloud.com/payments/notify"
    });
</script>
<script>

    // delete all from cart
    $("#btnCartClear").click(function(){
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;

        var btn = $(this);
        btn.html(_cuteLoader);

            $.ajax({
                type: "POST",
                url: "{{ route('ajax.cart.clear') }}",

                dataType: "json",
                success: function (response) {
                    if(response.status != "OK"){

                        btn.html("Erreur");

                        setTimeout(() => {
                            btn.html("Tout supprimer<i class='material-icons ml-1'>delete</i>");
                        }, 1000);
                        return false;
                    }

                    btn.html("Tout supprimer<i class='material-icons ml-1'>delete</i>");
                    window.location.reload();
                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                    btn.html("Erreur");
                    setTimeout(() => {
                        btn.html("Réessayer");
                    }, 2000);
                },
            });

    });


    // ***** Adjemin Pay
    $("#BtnAdjeminPay").click(function(){
        console.log(">>> clicked");
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;



        var deliveryAddress = $("input[name='address_delivery_name']").val();
        var deliveryAddressLat = $("input[name='location_delivery_lat']").val();
        var deliveryAddressLng = $("input[name='location_delivery_lng']").val();

        if(deliveryAddress == '' || deliveryAddress == null){
            //
            $("#error-msg").html("Veuillez choisir une adresse de livraison");
            return false;
        }
        if(deliveryAddressLat == '' || deliveryAddressLat == null){
            //
            $("#error-msg").html("Veuillez choisir une adresse de livraison");
            return false;
        }
        if(deliveryAddressLng == '' || deliveryAddressLng == null){
            //
            $("#error-msg").html("Veuillez choisir une adresse de livraison");
            return false;
        }

        var btn = $(this);
        btn.html(_cuteLoader);
            // payWithAdjeminPay({

            //     amount: 100,
            //     transactionId: "{{Carbon\Carbon::now()->toString()}}",
            //     currency: "XOF",
            //     designation: "Buying Products from shop golden health",
            //     customField: "Buying stuff"
            // });
            // btn.html = "payé";


            $.ajax({
                type: "POST",
                url: "{{ route('ajax.shop.createOrder') }}",
                data: {
                    "delivery_name": deliveryAddress,
                    "delivery_lat": deliveryAddressLat,
                    "delivery_lng": deliveryAddressLng,
                },
                dataType: "json",
                success: function (response) {
                    console.log("Ressponse succes");
                    console.log(response);
                    if(response.status != "OK"){
                        if(response.status == "ALREADY_EXISTS"){
                            btn.html("J'y participe <i class='material-icons ml-2'>check</i>");
                            return false;
                        }
                        btn.html("Erreur");

                        setTimeout(() => {
                            btn.html("Réessayer");
                        }, 1000);
                        return false;
                    }
                    //
                    // **
                    if(response.data.transaction != null){
                        // pay with adjeminpay
                        payWithAdjeminPay({
                            amount: response.data.transaction.amount,
                            transactionId: response.data.transaction.id,
                            currency: response.data.transaction.currency,
                            designation: response.data.transaction.designation,
                            customField: response.data.status
                        });
                    }
                    //
                    // btn.html("J'y participe <i class='material-icons ml-2'>check</i>");
                    // btn.removeClass('btn-secondary');
                    // btn.addClass('btn-primary');

                    // if(response.status != "ALREADY_EXISTS"){
                    //     $("#place_number_count").html($("#place_number_count").html()-1);
                    // }
                    // alert(document.URL+'#toReload');

                    // $('#toReload').load(document.URL+'#toReload');
                    // window.location.reload();
                    // this.classList.remove("markCompletedBtn");
                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                    btn.html("Erreur");
                    setTimeout(() => {
                        btn.html("Réessayer");
                    }, 2000);
                },
            });
    });

    function payWithAdjeminPay(transactionData){
        if(!transactionData){
            alert(">>>> erreur paiement, veuillez réessayer");
            console.log("<< clicked");
        }
        console.log(">>> clicked");

        // Ecoute le feedback sur les erreurs
        AdjeminPay.on('error', function (e) {
            // la fonction que vous définirez ici sera exécutée en cas d'erreur
            console.log(e);
            $("#result-title").html(e.title);
            $("#result-message").html(e.message);
            $("#result-status").html(e.status);
            $("#BtnAdjeminPay").html(e.status);
            console.log(">>> clicked");

        });

        // Lancer la procédure de paiement au click
        AdjeminPay.preparePayment({
            amount: transactionData.amount,
            transaction_id: transactionData.transactionId,
            currency: transactionData.currency,
            designation: transactionData.designation,
            custom: transactionData.customField
        });

        // Si l'étape précédante n'a pas d'erreur,
        // cette ligne génère et affiche l'interface de paiement AdjeminPay
        AdjeminPay.renderPaymentView();


        // Payment terminé
        AdjeminPay.on('paymentTerminated', function (e) {
            console.log('<<<<<<< Terminated !');
            console.log(e);

        });
        // Payment réussi
        AdjeminPay.on('paymentSuccessful', function (e) {
            console.log('<<<<<<< Successful !');
            console.log(e);
            btn = $("#BtnAdjeminPay");
            btn.html(e.status);
            setTimeout(()=> {
                window.location.reload();
                location.assign("/customer/orders/shop");
                //
            }, 3000);
        });
        // Payment échoué
        AdjeminPay.on('paymentFailed', function (e) {
            console.log('<<<<<<< Failed !');
            console.log(e);

            btn = $("#BtnAdjeminPay");
            btn.html(e.status);
        });
        // Payment annulé
        AdjeminPay.on('paymentCancelled', function (e) {
            console.log('<<<<<<< Cancelled !');
            console.log(e);
            $("#BtnAdjeminPay").html(e.status);
        });
    }

    //

    function decrement(id, name, price){
        console.log("dec");

        qty = parseInt($("#cartQtyValue-"+id).html());

        if(qty > 0){
            qty--;
            $("#cartQtyValue-"+id).html(qty);
            $("#cartItemAmount-"+id).html((qty)*parseInt(price));

            ajaxCart({
                    pId: id,
                    pName: name,
                    pQuantity: qty,
                    pPrice: price,
                });

            if(qty == 0){
                $("#qtyController-"+id).empty();
                $("#btnCart-"+id).html('Ajouter au panier');
                $("#btnCart-"+id).addClass('btn-article btn-secondary w-100  btnAddToCart');

                showSnackbar("Produit Retiré", "bg-danger");

                setTimeout(() => {
                    $("#btnCart-"+id).addClass('invisible');
                }, 3000);
                return false;
            }
            // showSnackbar("Produit Modifi", "bg-danger");

        }


    }
    function increment(id, name, price){
        console.log("inc");

        qty = parseInt($("#cartQtyValue-"+id).html())+1;
        $("#cartQtyValue-"+id).html(qty);
        $("#cartItemAmount-"+id).html(qty*parseInt(price));

        ajaxCart({
            pId: id,
            pName: name,
            pQuantity: qty,
            pPrice: price,
        });
    }

    function ajaxCart(cartData, action="manage"){
        if(cartData == null)
        {
            console.log("=== Invalid data");
            return 0;
        }
        console.log("=== Saving to cart");
        console.log(cartData);

        $.ajax({
            type: "POST",
            url: "/ajax/cart/"+action,
            data: {
                'product_id': cartData.pId,
                'product_name': cartData.pName,
                'product_quantity': cartData.pQuantity,
                'product_price': cartData.pPrice,
            },
            dataType: "json",
            success: function (response) {
                if(response.status != "OK"){
                    if(response.status == "REMOVED"){
                        showSnackbar("Produit Retiré", "bg-danger");
                        return false;
                    }
                    console.log(">>>> errerur");
                    showSnackbar("Erreur, Veuillez réessayer", "bg-danger", 4000);
                    console.log(response);
                    return false;
                }
                // showSnackbar("Erreur, Veuillez réessayer", "bg-danger", 4000);
                console.log(">>>> SUCCESS  ");
                console.log(response);
                // window.location.reload();
            },
            error: function (response) {
                console.log("Error :");
                console.log(response);
            },
        });
    }

    function start_search(){
        //
        var val = $("input[name='addr_delivery']").val();
        if(val.length > 4){
            addr_delivery_search();
        }
    }
</script>


{{-- Gestion livraison --}}

<script>
    // *** Delivery address map init
    // Abidjan
    var startlat_delivery = 5.40911790;
    var startlon_delivery = -4.04220990;

    var options_delivery = {
        center: [startlat_delivery, startlon_delivery],
        // zoom: 10
        zoom: 12
    }

    document.getElementById('lat_delivery').value = startlat_delivery;
    document.getElementById('lon_delivery').value = startlon_delivery;

    var map_delivery = L.map('map_delivery', options_delivery);
    var nzoom = 12;

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: 'OSM'}).addTo(map_delivery);
    // L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: 'OSM'}).addTo(map_delivery);

    var my_delivery_Marker = L.marker([startlat_delivery, startlon_delivery], {
        title: "Coordinates",
        alt: "Coordinates",
        draggable: true
    }).addTo(map_delivery).on('dragend', function () {
        var lat_delivery = my_delivery_Marker.getLatLng().lat_delivery.toFixed(8);
        var lon_delivery = my_delivery_Marker.getLatLng().lng.toFixed(8);
        var czoom = map_delivery.getZoom();

        if (czoom < 18) {
            nzoom = czoom + 2;
        }
        if (nzoom > 18) {
            nzoom = 18;
        }
        if (czoom != 18) {
            map_delivery.setView([lat_delivery, lon_delivery], nzoom);
        } else {
            map_delivery.setView([lat_delivery, lon_delivery]);
        }
        document.getElementById('lat_delivery').value = lat_delivery;
        document.getElementById('lon_delivery').value = lon_delivery;
        my_delivery_Marker.bindPopup("Lat " + lat_delivery + "<br />Lon " + lon_delivery).openPopup();
    });

    function choose_delivery_Addr(lat1, lng1) {
        my_delivery_Marker.closePopup();
        map_delivery.setView([lat1, lng1], 18);
        my_delivery_Marker.setLatLng([lat1, lng1]);
        lat = lat1.toFixed(8);
        lon = lng1.toFixed(8);
        document.getElementById('lat_delivery').value = lat;
        document.getElementById('lon_delivery').value = lon;
        //document.getElementById('locat').value = addrn;
        //alert(addrn);
        my_delivery_Marker.bindPopup("Lat " + lat + "<br />Lon " + lon).openPopup();

        $.get('https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=' + lat + '&lon=' + lon, function (data) {
            //console.log(data);
            // document.getElementById('locat').value = data.address.city + ',' + data.address.state + ',' + data.address.country;
            document.getElementById('locat_delivery').value = data.display_name;
        });
        //alert(i);
        document.getElementById('results_delivery').innerHTML = '';
    //document.getElementById('location').submit();
    }

    function lolp(ii) {
        alert(ii);
    }

    function loca(i) {
        alert(i);
    }

    function my_delivery_Function(arr) {
        var out = "<br />";
        var i;

        if (arr.length > 0) {
            for (i = 0; i < arr.length; i++) {
                out += "<div class='address' title='Show Location and Coordinates' style='cursor:pointer' value='" + arr[i].display_name + "' onclick='choose_delivery_Addr(" + arr[i].lat + ", " + arr[i].lon + ");return false;'>" + arr[i].display_name + "</div>";
            }
            document.getElementById('results_delivery').innerHTML = out;
        } else {
            document.getElementById('results_delivery').innerHTML = "<div class='address'>Aucune adresse trouvée...</div>";
        }

    }

    function addr_delivery_search() {
        var inp = document.getElementById("addr_delivery");
        var xmlhttp = new XMLHttpRequest();
        var url = "https://nominatim.openstreetmap.org/search?format=json&limit=3&q=" + inp.value;
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var my_delivery_Arr = JSON.parse(this.responseText);
                my_delivery_Function(my_delivery_Arr);
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }
</script>

{{-- Color inputs css --}}
<script>
    // Radio color inputs
    var colorSpans = document.getElementsByClassName('span-color');
    for(var c = 0; c < colorSpans.length; c++){
        //
        colorSpans[c].style.background = colorSpans[c].dataset.color;
    }
</script>


@endsection
