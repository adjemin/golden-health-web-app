@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
<link media="all" rel="stylesheet" href="{{asset('css/plugins.css')}}">
<!-- Custom css -->
<link media="all" rel="stylesheet" href="{{asset('css/shop.css')}}">
@endsection

@section('content')
<main class="container-fluid py-5">
  <!-- Page Banner -->
  <div class="page-banner">
      <div class="banner-caption common-box">
          <div class="container">

              <div class="page-nav skin_dark">
                  <ol class="breadcrumb">
                      <li>
                          <a href="/shop">Shop</a>
                      </li>
                      <li class="active">Panier</li>
                  </ol>
              </div>

              <h1>Panier <span class="font-weight-300"> - {{App\ShoppingCart::ShoppingCart()->items->count()}} Articles</span> </h1>
          </div>
      </div>
  </div>
  <!-- //-End Page Banner -->

  <!-- Content -->
 <div class="common-box pt-0" role="main">
     <div class="container">
       @if (App\ShoppingCart::ShoppingCart()->items->count() > 0)
         <div class="cart-module">
             <div class="table-responsive common-module">
                 <table class="table m-0 table-borderless">
                     <thead>
                         <tr>
                             <th>Article</th>
                             <th>Prix</th>
                             <th>Quantité</th>
                             <th>Couleur</th>
                             <th>Taille</th>
                             <th>Total</th>
                             <th>&nbsp;</th>
                         </tr>
                     </thead>
                     <tbody>
                       @foreach (App\ShoppingCart::ShoppingCart()->items as $item)
                         
                         <tr>
                             <td>
                                 <div class="item">
                                     <div class="intro-img">
                                        @if(!is_null($item->product))
                                         <img src="{{ $item->product->getImageUrl() ?? asset('customer/images/pair.jpg') }}" alt="img dis">
                                        @endif
                                     </div>
                                     <div class="intro-text">
                                        @if(!is_null($item->product))
                                         <h4><a href="/product_details/{{$item->product->slug}}">{{$item->product->title}}</a></h4>
                                        @endif
                                     </div>
                                 </div>
                             </td>
                             <td>
                                @if(!is_null($item->product))
                                    <span class="price">  {{$item->product->getPrice()}} CFA</span>
                                @endif
                             </td>
                             <td>
                                @if(!is_null($item->product))
                                    <input type="number" class="form-control" value="{{$item->product->currentCartItem()->quantity}}" min="1" max="{!! $item->product->stock == 0 || $item->product->stock == null ? ($item->product->initial_count == 0 || $item->product->initial_count == null ? 0 : $item->product->initial_count ) : $item->product->stock  !!}"
                                            data-p-id="{{$item->product->id}}" data-p-name="{{$item->product->title}}"
                                            data-p-price="{{$item->product->getPrice()}}"
                                            data-input-type="incrementerCart" id="quantityCart" readonly />
                                @endif
                             </td>
                             <td>
                                @if(!is_null($item->product))
                                    <span class="price">{{$item->color}}</span></td>
                                @endif
                             <td>
                                <span class="price">{{$item->size}}</span>
                            </td>
                             <td><span class="price">{{$item->amount}} CFA</span></td>
                             <td><a href="/ajax/cart/remove/{{$item->id}}" class="delete"><i class="flaticon-trash"></i></a></td>
                         </tr>
                         @endforeach
                     </tbody>
                 </table>
             </div>

             <div class="common-module">
                 <div class="row align-items-end">
                     <div class="col-md-7 col-lg-5">
                       {{--<p>
                           Entrez votre coupon ci-dessous et bénéficiez d'une réduction
                       </p>
                         <form class="form-inline coupne-form">
                             <input type="text" class="form-control" id="coupon_code" name="coupon_code"  placeholder="Entrez votre code"/>
                             <button type="button" class="btn btn-article shadow border-0 btn-md text-uppercase font-weight-600 rounded" id="submitCoupon">Appliquer</button>
                         </form>
                         <p id="coupon_error" class="text-danger font-weight-bold mt-1"></p>--}}
                     </div>
                     <div class="col-md-5 col-lg-4 ml-lg-auto">
                         <div class="price-detail-slot">
                             <ul>
                                 <li>
                                     <span class="content-title">Sous-total</span>
                                     <span class="value">{{App\ShoppingCart::ShoppingCart()->getSousTotal()}} CFA</span>
                                 </li>
                                 <li>
                                     <span class="content-title">Frais de livraison</span>
                                     <span class="value">{{App\ShoppingCart::ShoppingCart()->getDeliveryFeeTotal()}} CFA</span>
                                 </li>
                                 <li>
                                     <span class="content-title">Total</span>
                                     <span class="value text-primary font-lg">{{App\ShoppingCart::ShoppingCart()->getTotal()}} CFA</span>
                                 </li>
                             </ul>
                             <a
                                 href="/checkout"
                                 class="btn btn-secondary btn-block btn-md text-uppercase font-weight-600 mt-6">Passer au paiement<i class="flaticon-arrow-right"></i> </a>
                         </div>
                     </div>
                 </div>
             </div>

         </div>
         @else
         <section>
             <div class="container">
                 <div class="row product-card">
                         <div class="col-lg-5 col-md-5 mx-auto col-sm-12">
                             <img src="{{asset('customer/images/logo.png')}}" class="img-fluid" style="width:180px;" alt="">
                         </div>
                         <div class="container text-center">
                             <h3>Vous n'avez pas d'article dans votre panier</h3>
                         </div>
                         <div class="container">
                             <div class="row">
                                 <a href="/shop" class="btn-article btn-secondary mx-auto px-2 mt-3">
                                     Ajouter des produits
                                 </a>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </section>
         @endif
     </div>
 </div>
 <!-- //-End Content -->

</main>
@endsection

@section('scripts')
<script src="{{asset('js/plugins.js')}}"></script>
<script src="{{asset('js/shop-init.js')}}"></script>
<script>
    $token = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });
    $(function(){
      //input tyle number
      $('input[data-input-type="incrementerCart"]').wrap('<div class="increment_decrement">');
      $('<div class="handler"><span class="increment"><i class="fa fa-plus"></i></span><span class="decrement"><i class="fa fa-minus"></i></span></div>').insertAfter('.increment_decrement input');
      $('.increment_decrement').each(function () {
          var spinner = $(this),
              input = spinner.find('input[type="number"]'),
              btnUp = spinner.find('.increment'),
              btnDown = spinner.find('.decrement'),
              min = input.attr('min'),
              max = input.attr('max');
          var pId = spinner.find("input").data('p-id');
          var pName = spinner.find("input").data('p-name');
          var pPrice = spinner.find("input").data('p-price');

          btnUp.click(function () {
              var oldValue = parseFloat(input.val());
              if (oldValue >= max) {
                var newVal = oldValue;
              } else {
                var newVal = oldValue + 1;
              }
              spinner.find("input").val(newVal);
              spinner.find("input").trigger("change");
              ajaxCart({
                  pId: pId,
                  pName: pName,
                  pQuantity: newVal,
                  pPrice: pPrice,
              });
          });

          btnDown.click(function () {
              var oldValue = parseFloat(input.val());
              if (oldValue <= min) {
                var newVal = oldValue;
              } else {
                var newVal = oldValue - 1;
              }
              spinner.find("input").val(newVal);
              spinner.find("input").trigger("change");
              ajaxCart({
                  pId: pId,
                  pName: pName,
                  pQuantity: newVal,
                  pPrice: pPrice,
              });
          });
      });

      function ajaxCart(cartData, action="manage") {
        if(cartData == null)
        {
            console.log("=== Invalid data");
            return 0;
        }
        console.log("=== Saving to cart");
        console.log(cartData);

        $.ajax({
            type: "POST",
            url: "/ajax/cart/"+action,
            data: {
                'product_id': cartData.pId,
                'product_name': cartData.pName,
                'product_quantity': cartData.pQuantity,
                'product_price': cartData.pPrice,
            },
            dataType: "json",
            success: function (response) {
                if(response.status != "OK"){
                    if(response.status == "REMOVED"){
                        showSnackbar("Produit Retiré", "bg-danger");
                        return false;
                    }
                    console.log(">>>> errerur");
                    showSnackbar("Erreur, Veuillez réessayer", "bg-danger", 4000);
                    console.log(response);
                    return false;
                }
                // showSnackbar("Erreur, Veuillez réessayer", "bg-danger", 4000);
                console.log(">>>> SUCCESS  ");
                console.log(response);
                window.location.reload();
            },
            error: function (response) {
                console.log("Error :");
                console.log(response);
            },
        });
      }



      $("#submitCoupon").click(function name(params) {
          var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
          var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;

          var coupon_code = $("input[name='coupon_code']").val();

          $(this).html(_cuteLoader);
          var $btn = $(this);
          var $messageDiv = $("#coupon_error");

          if(coupon_code == ''){
              $("#coupon_error").html("Vous devez entrez un coupon");
              $("#submitCoupon").html("Vérifier")
              return false;
          }

          $.ajax({
                  type: "POST",
                  url: "{{ route('ajax.coupon.add') }}",
                  data: {
                      "coupon_code": coupon_code,
                  },
                  dataType: "json",
                  success: function (response) {
                      //
                      console.log(">> success");
                      console.log(response);
                      if(response.status == null){
                          $btn.html("Veuillez réessayer plus tard");
                          $messageDiv.html("Une erreur est survenu");
                          return false;
                      }
                      // managing status codes
                      if(response.status != "OK"){
                          $btn.html("OUPS");
                          if(response.error != null){
                              if(response.error.message != null){
                                  $messageDiv.html(response.error.message);
                                  setTimeout(() => {
                                      $btn.html("Réessayer");
                                  }, 1000);
                                  return false;
                              }
                              $btn.html("Réessayer")
                          }

                      }
                      $btn.html("Coupon Ajouté");

                      window.location.reload();

                  },
                  error: function(response){
                      console.log(">> erreur");
                      $btn.html("Veuillez réessayer plus tard");
                      $messageDiv.html("Une erreur est survenue");
                      console.log(response);
                  }
          });
      });
    });
</script>


@endsection
