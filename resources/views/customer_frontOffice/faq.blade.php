@extends("customer_frontOffice.layouts_customer.master_customer")
@section('css')
<style>
    body {
        background-color: rgba(238, 238, 238, 0.336);
    }
    a.accordion:hover{
        color: currentColor !important;
    }
    .accordion {
        display: inline-block;
        background-color: #eee;
        color: #444;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        transition: 0.4s;
    }
    .accordion:first-child{
        border-radius: 5px 5px 0 0;
    }
    .accordion:last-child{
        border-radius: 0 0 5px 5px;
    }


    .active, .accordion:hover {
        background-color: #ccc;
    }

    .accordion:after {
        content: '\002B';
        color: #777;
        font-weight: bold;
        float: right;
        margin-left: 5px;
    }

    .active:after {
        content: "\2212";
    }

    .panel {
        padding: 0 18px;
        background-color: white;
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.2s ease-out;
    }
    .panel p{
        padding: 15px;
        font-size: 17px;
    }

</style>

@endsection
@section('content')

<section class="py-md-5 p-y2">
    <div class="container">
        <div class="text-center p-md-5 p-2" style="border-radius: 5px; background:lightgrey;">
            <h1 class="font-weight-bold text-center primary-text-color mb-3">Foire Aux Questions </h1>
        </div>
    </div>
    <div class="container mt-3 mb-5">

        @if($faqs)
            @forelse($faqs as $faq)
                <a class="accordion text-primary font-weight-bold @if($loop->last) last @endif">
                    <span class="h5 font-weight-bold">Q : </span>
                    {{$faq->question}}</a>
                <div class="panel">
                    <p>
                        <span class="h5 font-weight-bold text-secondary">A : </span>
                        {!!$faq->answer!!}
                    </p>
                </div>
            @empty
            @endforelse
        @else
            <div class="container mt-md-5 mt-3 p-4 text-center">
                <p class="h5">
                    Aucune Question disponible pour l'instant
                </p>
            </div>
        @endif

        <h3 class="text-center">Pour toutes autres questions, prière de contacter le service client de
            GoldenHealth <a href="mailto:ny.goldenhealth@gmail.com">ny.goldenhealth@gmail.com</a>
            <br>
            Merci.
        </h3>
    </div>
</section>

@endsection

@section('scripts')

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;

    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}
</script>

@endsection
