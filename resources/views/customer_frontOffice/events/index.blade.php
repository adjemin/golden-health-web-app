@extends("customer_frontOffice.layouts_customer.master_customer")

@section('title', 'Events')

@section('css')

<style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }

    a {
        color: black;
    }

    a:hover{
        color:var(--primary)
    }
     #typeahead_events span{
        width: 100%;
    }
</style>
    {{-- Loading Spinner --}}
    <style>
        .loader {
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        }
        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
    {{-- Loader Spinner --}}
    <style>
        .cute-loader {
            display: inline-block;
            position: relative;
            top: 50%;
            /* transform: translate(-50%, -50%); */
            width: 30px;
            height: 30px;
            margin: 0 auto;
        }
        .cute-loader div {
            /* box-sizing: border-box; */
            display: block;
            position: absolute;
            width: 25px;
            height: 25px;
            /* margin: 4px; */
            margin: auto;
            border: 4px solid #edaa0d;
            border-radius: 50%;
            animation: cute-loader 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #edaa0d transparent transparent transparent;
            background: white;
        }
        .cute-loader.dark div{
            border: 4px solid #fff;
            border-color: #fff transparent transparent transparent;

        }
        .cute-loader div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .cute-loader div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .cute-loader div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes cute-loader {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
    {{-- Selected events style --}}
    <style>
        #selected-users-list{
            display: flex;
            justify-content: start;
            align-items: baseline;
            flex-wrap: wrap;
            padding: 5px;
            background: #ececec;
            border-radius: 3px;
            margin: 10px 0;
        }
        .select-result{
            box-shadow: 2px 2px 2px #ccc;
            margin-top: 5px;
            max-height: 200px;
            overflow-x: hidden;
            overflow-y: scroll;

            /* border: 1px solid #bbb; */
        }
        #select-result-users{
            position:absolute;
            z-index: 90;
            display: inline-flex;
            flex-direction: column;
            align-items: center;
            border-radius: 4px;
            background: white;
        }
        #select-result-users .cute-loader{
            /* margin: auto; */
            margin: 10px;
        }
        .select-result .cute-loader{
            /* margin: auto; */
            margin: 10px;
        }

        .result-item{
            display: flex;
            align-items: stretch;
            justify-content: start;
            width: 100%;
            padding: 5px 10px;
            /* border-top: 1px solid #bbb; */
            background: #fff;
            transition: .1s;
            border-radius: 4px;
        }
        .result-item *{
            border-radius: 4px;
        }

        .result-item:hover{
            background: #edaa0d;
            color: #fff !important;
            cursor: pointer;
        }
        .search-icon:hover{
            cursor: pointer;
            background: #efefef;
        }
    </style>
    {{-- animated searchbar --}}
    <style>
        .search-container{
            position: relative;
        }
        .input-slide{
            border: none;
            transition: .3s ease-in;
            width: 0;
            opacity: 0;
            /* position: absolute; */
            /* right: -20%; */
        }
        .search-placeholder{

            transition: .3s ease-in;
        }
        .slide-out{
            width: 100%;
            opacity: 0;
            display:none;
        }
        .input-slide.slide{
            width: 100%;
            opacity: 1;
        }
    </style>

    @foreach($slides as $key => $slide)
    <style>
        .event-slide-image-{{$key}} {
            position: relative;
            width: 100%;
            height: 65vh;
            background-position: center;
            background-size: cover;
            background-image: linear-gradient(rgba(0, 0, 0, .6), rgba(0, 0, 0, .6)), url("{{asset($slide->image)}}");
        }
    </style>
    @endforeach
@endsection

@section('content')

<section>
    <div class="container-fluid d-none d-md-block">
        <div class="row">
            <div class="col-lg-12 col-md-12 mb-5 p-0">
                <div id="style1" class="carousel slide style-custom-1" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach($slides as $key => $slide)
                            <li data-target="#style1" data-slide-to="{{$key}}"  class="{!! $loop->first ? 'active' : '' !!}"></li>
                        @endforeach
                        {{--<li data-target="#style1" data-slide-to="0" class="active"></li>
                        <li data-target="#style1" data-slide-to="1"></li>
                        <li data-target="#style1" data-slide-to="2"></li>--}}
                    </ol>
                    <div class="carousel-inner">
                        @foreach($slides as $slide)
                        <div class="carousel-item {!! $loop->first ? 'active' : '' !!}">
                            <div class="event-slide-image-{{$key}}"></div>
                            <div class="carousel-caption">
                                <div class="container">
                                    <h2 class="slide-des-content">
                                        <b>{{ $slide->titre ?? '' }}</b>
                                    </h2>
                                    {!! $slide->subtitle ?? '' !!}
                                </div>
                            </div>
                        </div> 
                        @endforeach
                        {{--<div class="carousel-item active">
                            <div class="event-slide-image-one"></div>
                            <div class="carousel-caption">
                                <div class="container">
                                    <h2 class="slide-des-content">
                                        <b>Sud Ouest
                                            Basket-ball : l'Euro 2021 féminin sera organisé en France</b>
                                    </h2>
                                </div>
                            </div>
                        </div> 
                        <div class="carousel-item">
                            <div class="event-slide-image-two"></div>
                            <!-- <img class="d-block w-100 slide-image-two" alt="Second slide"> -->
                            <div class="carousel-caption">
                                <div class="container">
                                    <h2>
                                        <b>
                                            Sud Ouest
                                            Basket-ball : l'Euro 2021 féminin sera organisé en France</b>
                                    </h2>

                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="event-slide-image-three"></div>
                            <!-- <img class="d-block w-100 slide-image-three" src="" alt="Third slide"> -->
                            <div class="carousel-caption">
                                <div class="container">
                                    <h2>
                                        <b>
                                            Mairie de Grande-Synthe
                                            Sport'ouvertes le 1er Sept </b>
                                    </h2>
                                </div>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @csrf
</section>


<section class="container mt-5 mt-md-3">
        {{-- Breadcrumb --}}
        <div class="row">
            {{-- <div class="col-md-11 transition"> --}}
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="/">Accueil</a></li>
                    <li><a href="/all_events">Events</a></li>
                </ul>
            </div>
            {{-- <div class="col-md-1 ml-auto search-container pr-0"> --}}
            <div class="col-md-6 ml-auto search-container pr-0">
                <div class="d-flex align-items-center justify-content-end">
                    <input type="text" name="" class="EventTypeahead form-control input-slide" id="eventSearchInput" placeholder="Recherchez un évent..">
                    <span class="search-placeholder search-icon r b-fg p-2 fw-500 fs-l">Rechercher</span>
                    <i class="material-icons ml-2 b-fg r p-2 search-icon">search</i>
                </div>
                <div class="select-result invisible" id="select-result-users"></div>
            </div>
        </div>

</section>

<section class="py-3 mx--md-5">
    <div class="container ">
        <div class="">
            <h3 class="font-weight-bold text-center mb-3 ">Top <span class="secondary-text-color">Evénements</span> </h3>

        </div>

        <div class="secondary-divider mb-3"> </div>
        <br>

        @if($events->count() > 0)
            <h2 class="mb-5">Evénements à venir</h2>
            <div class="row">
                @if(isset($upcoming_events) && !is_null($upcoming_events) )

                @forelse ($upcoming_events as $event)
                    <a href="/show_event/{{$event->slug}}" class="event-card" >
                        <div class="col-lg-4 col-md-4 pb-4">
                            <div class="event-content">
                                <div class="bg-image" style="background-image: url({{  $event->cover_url ?? asset('customer/images/IMAGE.png') }});">
                                </div>

                                <div class="mx-2 py-2">
                                    <h5 class="text-primary">{{$event->title }} </h5>
                                    <div class="py-2 d-md-flex flex-md-row justify-content-md-between align-items-md-center d-flex flex-column">
                                        {!! $event->showEntryFees()!!}

                                        @if ($event->type != null)
                                            <a class="event-type">
                                                {{$event->type->name ?? '' }}
                                            </a>
                                        @endif
                                    </div>
                                    <div>
                                        <img src="{{asset('customer/images/icons8-next-location-30.png')}}" alt="" width="25px" height="25px" class="mr-2">
                                        {{ $event->venue }}
                                    </div>
                                    <div>
                                        <img src="{{asset('customer/images/icons8-clock-64.png')}}" alt="" width="25px" height="25px" class="mr-2">

                                        {{-- {{ Carbon\Carbon::parse($event->date_event)->diffForHumans() }} le --}}
                                        Le {{ Carbon\Carbon::parse($event->date_event)
                                        ->isoFormat('dddd Do MMMM YYYY') }} @if(!is_null($event->end_time) && !is_null($event->start_time)) de {{$event->start_time}} à {{$event->end_time}} @elseif(!is_null($event->start_time)) à {{$event->start_time}} @endif
                                        {{-- ->isoFormat('dddd D M Y à H:m') }} --}}
                                    </div>
                                    <div>
                                        @if ($event->place_number != null || $event->place_number > 0 )

                                            <span class="d-inline-flex align-items-center font-weight-bold  @if($event->isFull()) text-danger @else text-primary @endif">
                                            <i class="material-icons secondary mr-2">person</i>
                                                {{$event->availableSeatsCount()}}
                                                <span class="ml-2">
                                                    {{$event->availableSeatsText()}}
                                                </span>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        @if($event->isAttendable())
                                            @if($event->isCurrentCustomerParticipating())
                                                <a href="show_event/{{$event->slug}}" class="btn-article  btn-primary w-100 d-flex align-items-center justify-content-center">
                                                    J'y participe
                                                    <i class="material-icons ml-2">check</i>
                                                </a>
                                            @else
                                                @if(!$event->isFull())
                                                    <a href="show_event/{{$event->slug}}" class="btn-article btn-secondary w-100">
                                                        Participer
                                                    </a>
                                                @else
                                                    <a href="show_event/{{$event->slug}}" class="btn btn-danger w-100 py-2 font-weight-medium">
                                                        Aucune place disponible
                                                    </a>
                                                @endif
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                @empty
                    <div class="text-center">
                        <h3 class="text-center text-secondary">Aucun event en vue pour l'instant</h3>
                    </div>
                @endforelse
                @endif
            </div>

            <!--  -->
            <div class="secondary-divider mb-3 mt-5"> </div>


            @if(isset($past_events) && !is_null($past_events))
                <h2 class="mb-5 mt-4">Evénements passés</h2>

                <div class="row">
                    @foreach ($past_events as $event)
                        <a href="/show_event/{{$event->slug}}" class="event-card past-event">
                            <div class="col-lg-4 col-md-4 pb-2">
                                <div class="event-content past-event">
                                    <div class="bg-image" style="background-image: url({{  $event->cover_url ?? asset('customer/images/IMAGE.png') }});">
                                    </div>

                                    <div class="mx-2 py-2">
                                        <h5 class="text-primary">{{$event->title }} </h5>
                                        <div class="py-2 d-md-flex flex-md-row justify-content-md-between align-items-md-center d-flex flex-column">
                                            {!! $event->showEntryFees()!!}

                                            @if ($event->type != null)
                                                <a class="event-type">
                                                    {{$event->type->name ?? '' }}
                                                </a>
                                            @endif
                                        </div>
                                        <div>
                                            <img src="{{asset('customer/images/icons8-next-location-30.png')}}" alt="" width="25px" height="25px" class="mr-2">
                                            {{ $event->venue }}
                                        </div>
                                        <div>
                                            <img src="{{asset('customer/images/icons8-clock-64.png')}}" alt="" width="25px" height="25px" class="mr-2">

                                            {{-- {{ Carbon\Carbon::parse($event->date_event)->diffForHumans() }} le --}}
                                            Le {{ Carbon\Carbon::parse($event->date_event)
                                            ->isoFormat('dddd Do MMMM YYYY') }} @if(!is_null($event->end_time) && !is_null($event->start_time)) de {{$event->start_time}} à {{$event->end_time}} @elseif(!is_null($event->start_time)) à {{$event->start_time}} @endif
                                            {{-- ->isoFormat('dddd D M Y à H:m') }} --}}
                                        </div>
                                        <div>
                                            @if ($event->place_number != null || $event->place_number > 0 )

                                                <span class="d-inline-flex align-items-center">
                                                <i class="material-icons secondary mr-2">person</i>
                                                    {{$event->place_number - $event->participants->count()}} Places restantes
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            @endif
        @else
            <div class="text-center">
                <h3 class="text-center text-secondary">Aucun event n'est prévu pour l'instant</h3>
            </div>
        @endif
    </div>
</section>

@endsection

@section('scripts')

<script src="{{asset('js/typeahead.bundle.js')}}"></script>
    {{-- Fetch get data --}}
<script>
    $token = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });
</script>
<script>
        var eventList = []

        eventList = getEvents();
        function getEvents(){

            $.ajax({
                url : "{{route('ajax.event.list') }}",
                method : "POST",
                data: "pleaaaase cutie gimmie ya customer events nan ><"
                }).done((response) => {
                    for(var x = 0; x < response.data.length ; x++){
                        var event = response.data[x].event;
                        console.log(event);
                        eventList.push(event);
                    }
                    console.log("Success");
                    console.log(response);
                }).fail((response) =>{

                    console.log("failed, here's why :")
                    console.log(response);
                });
            return eventList;
        }
    </script>
    <script>
        // *** Type Ahead
        Type ahead for eventList
        var substringMatcher = function(strs) {
            return function findMatches(q, cb) {
                var matches, substringRegex;

                // an array that will be populated with substring matches
                matches = [];

                // regex used to determine if a string contains the substring `q`
                substrRegex = new RegExp(q, 'i');

                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(strs, function(i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
                });

                cb(matches);
            };
        };
        // console.log("eventList typeahead");
        // console.log(eventList);
        $('#EventTypeahead .typeahead').typeahead({
                hint: true,
                highlight: false,
                minLength: 3
            },
            {
            name: 'eventList',
            source: substringMatcher(eventList),
            afterSelect: function (item) {
                alert(item);
                console.log(item);
            },
            updater: function(item){
                alert(item);
            }
        });
        // Select first on enter
        $('#cities-prefetch .typeahead').on('keyup', function(e) {
            if(e.which == 13) {
                $(".tt-suggestion:first-child", this).trigger('click');
            }
        });
    </script>
    {{-- Invite modal members suggestion input --}}
    <script>
        // var $dEvents = [];
        var $suggestedEvents = [];
        var $selectedEvents = [];
        // //
        var suggestedDiv = $("#select-result-users");
        var _cuteLoader = `
        <div class="container w-100">
            <div class="cute-loader"><div></div><div></div><div></div><div></div></div>
        </div>
        `;
        // var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;



        // ** Fetching user typed events for suggestion
        $('#eventSearchInput').on('keyup',  function(e){
            console.log(">>>>");
            suggestedDiv.html("");
            inputdata = $(this).val();
            console.log(inputdata);
            suggestedDiv.html(_cuteLoader);

            if(inputdata.length < 3){
                return false;
            }

            $.ajax({
                type: "POST",
                url: "{{route('ajax.event.search')}}",
                data: {
                    value: inputdata,
                    // exclude: eventsToExclude
                },
                dataType: "json",
                success: function (response) {
                    console.log("<<<");
                    console.log(response);
                    if(response.code = 11){
                        suggestedDiv.html("");
                        if(response.data.length == 0){
                            suggestedDiv.html(`
                                <a class='result-item'>
                                    <span>Aucun event trouvé </span>
                                </a>`);
                            return false;
                        }
                        response.data.forEach(suggestion => {
                            suggestedDiv.toggleClass('invisible');
                            suggestedDiv.append(`
                            <a id='${suggestion.id}' href='/show_event/${suggestion.slug}' class='result-item visible' >
                                <img src="${suggestion.cover_url ?? ''}" class="img-fluid d-none d-md-block" width='100px'/>
                                <div class="d-flex flex-column justify-content-between ml-2">

                                    <span class="fs-m fw-500"> ${suggestion.title}</span>
                                    <div class="d-flex justify-content-between ">
                                        <span> ${suggestion.date_event} </span>
                                        <span> ${suggestion.entry_fee == null ? "Gratuit" : suggestion.entry_fee+" XOF"} </span>
                                    </div>
                                </div>
                            </a>`);
                        });
                    } else{

                    }
                    console.log("<<< data");
                    console.log(response.data);
                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                },
            });
        });
        $(window).click(function(){
            suggestedDiv.html("");
        });
        $(".search-icon").click(function(){
            $('.input-slide').toggleClass('slide');
            $('.search-placeholder').toggleClass('slide-out');
           // $(this).parent().parent().toggleClass('col-md-6');
            //$(this).parent().parent().toggleClass('col-md-1');
            // $(".breadcrumb").parent().toggleClass('col-md-11');
            // $(".breadcrumb").parent().toggleClass('col-md-6');
        });
    </script>
@endsection
