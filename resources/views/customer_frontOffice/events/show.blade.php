@extends("customer_frontOffice.layouts_customer.master_customer")

@section('title', 'Event - '.$event->title)


@section('css')
<style>
    body{
        background: #eee;
    }
</style>
<script src="https://cdn.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript"></script>
@endsection

@section('content')
<main class="container">
    <!--  -->
    <section class="pt-3">
        <div class="">
            <div class="text-center p-md-3 p-2 r bg-white">
                <h1 class="font-weight-bold text-center primary-text-color mb-3 text-uppercase">{{$event->title}}</h1>
                <div>
                </div>
            </div>
        </div>
    </section>

    <section class="my-3">
        {{-- Breadcrumb --}}
        <ul class="breadcrumb">
            <li><a href="/">Accueil</a></li>
            <li><a href="/all_events">Events</a></li>
            <li><a>{{$event->title}}</a></li>
        </ul>
        @csrf
    </section>

    <section class="py-5">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                @if(!$event->cover_url)
                    <img src="{{ asset('customer/images/IMAGE.png') }}" class="img-fluid rdimg" alt="" srcset="">
                @else
                    <div class="r w-100">
                        <img src="{{  $event->cover_url ?? asset('customer/images/IMAGE.png') }}" class="r" style="width:100%;height:auto;border-radius:5px;" alt="" srcset="">
                    </div>
                    <div class="bg-image d-none" style="background-image: url({{  $event->cover_url ?? asset('customer/images/IMAGE.png') }});">
                        <img src="{{  $event->cover_url ?? asset('customer/images/IMAGE.png') }}" class="img-fluid rdimg d-none" alt="" srcset="">
                    </div>
                @endif
                <h3 class="mt-3 font-weight-bold  text-left secondary-text-color mb-3"></h3>
                <div class="p-3 r bg-white">
                    <p class="" style="font-size: medium; line-height: 1.8;">
                        {!!$event->description!!}
                    </p>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 pb-2">
                <div class="event-content">
                    <div class="p-2">
                        <h4 class="mb-3 font-weight-bold text-primary">A propos de cet Event</h4>

                        <div> <img src="{{asset('customer/images/icons8-calendar-16.png') }}" alt="" width="20px" height="20px"
                            class="mr-2"><span class="font-weight-bold primary-text-color">Début : </span>
                            {{ Carbon\Carbon::parse($event->date_event)
                            ->isoFormat('dddd Do MMMM YYYY') }} @if(!is_null($event->start_time)) à {{$event->start_time}} @endif
                        </div>

                        @if($event->date_event_end)
                        <div>
                            <img src="{{asset('customer/images/icons8-calendar-16.png') }}" alt="" width="20px" height="20px"
                                class="mr-2"><span class="font-weight-bold primary-text-color">Fin : </span>
                            {{ Carbon\Carbon::parse($event->date_event_end)
                            ->isoFormat('dddd Do MMMM YYYY') }} @if(!is_null($event->end_time)) à {{$event->end_time}} @endif
                        </div>
                        @endif
                        <div>
                            <img src="{{asset('customer/images/icons8-clock-64.png') }}" alt="" width="20px" height="20px"
                                class="mr-2"><span class="font-weight-bold primary-text-color">Heure : </span>

                                {{-- Carbon\Carbon::parse($event->date_event)
                                ->isoFormat('HH:mm') --}}
                                @if(!is_null($event->end_time) && !is_null($event->start_time)) de {{$event->start_time}} à {{$event->end_time}} @elseif(!is_null($event->start_time)) {{$event->start_time}} @endif
                        </div>

                        <div>
                            <img src="{{asset('customer/images/icons8-next-location-30.png') }}" alt="" width="20px" height="20px"
                                class="mr-2"><span class="font-weight-bold primary-text-color">Lieu : </span>
                                {{$event->venue}}
                        </div>

                        @if($event->country)
                            <div>
                                <img src="{{ asset('customer/images/icons8-country-64.png') }}" alt="" width="20px" height="20px"
                                    class="mr-2"><span class="font-weight-bold primary-text-color">Pays : </span>
                                    {{$event->country}}
                            </div>
                        @endif

                        @if($event->type)
                            <div>
                                <img src="{{ asset('customer/images/icons8-price-tag-pound-30.png') }}" alt="" width="20px" height="20px"
                                    class="mr-2"><span class="font-weight-bold primary-text-color">Catégorie :
                                </span> {{$event->type->name}}
                            </div>
                        @endif
                        @if($event->organizers)
                            <p>
                                <img src="{{ asset('customer/images/icons8-person-female-48.png') }}" alt="" width="20px" height="20px"
                                    class="mr-2"><span class="font-weight-bold primary-text-color">
                                        Organisateur :
                                    </span>
                                    {{$event->organizers}}
                            </p>
                        @endif
                        <div id="toReload">
                            @if($event->isAttendable())
                                @if($event->isUpcoming())
                                    <div class="mt-2">
                                        <img src="{{ asset('customer/images/ticket.png') }}" alt="" width="20px" height="20px"
                                            class="mr-2">
                                        @if($event->isFree())
                                            <span class="font-weight-bold primary-text-color">
                                                Gratuit
                                            </span>
                                        @else
                                            <span class="font-weight-bold primary-text-color">
                                                Tarif d'entrée :
                                            </span>
                                            {{ ($event->entry_fee ?? "0")." FCFA"}}
                                        @endif
                                    </div>
                                    <div>
                                        @if ($event->place_number != null || $event->place_number > 0 )
                                        <span class="d-inline-flex align-items-center font-weight-bold @if($event->isFull()) text-danger  @else text-primary @endif">
                                            <i class="material-icons secondary mr-2">person</i>
                                            <span id="place_number_count" class="mr-2">{{$event->availableSeatsCount()}}</span>  Places restantes
                                        </span>
                                        @endif
                                        @if($event->isFull())
                                            <a class="w-100 font-weight-bold text-danger d-flex justify-content-start align-items-center">
                                                <i class="material-icons mr-3">error</i>
                                                Plus de place
                                            </a>
                                        @endif
                                    </div>

                                    @if(!$event->isFull())
                                        @auth('customer')
                                            @if($event->isCurrentCustomerParticipating())
                                                <a href="/customer/tickets" class="text-underline font-weight-bold text-center">
                                                    Voir mon ticket
                                                </a>
                                                <a class="btn-article btn-primary w-100 d-flex justify-content-center align-items-center mt-3" class="loading-btn">
                                                    J'y participe
                                                    <i class="material-icons ml-2">check</i>
                                                </a>
                                            @else
                                                @if($event->isFree())
                                                    <input type="number" id="nb_places" name="nb_places" placeholder="nombre de places" class="form-control" min="1" max=1>
                                                @else
                                                    <input type="number" id="nb_places" name="nb_places" placeholder="nombre de places" class="form-control" min="1" max={{$event->availableSeatsCount()}}>
                                                @endif

                                                <div id="message" class="text-danger font-weight-bold"></div>
                                                <a class="btn-article btn-secondary w-100 mt-3 d-flex justify-content-center align-items-center" data-event-id="{{$event->id}}" id="btnParticipate" data-event-entry-fee="{{$event->entry_fee}}" class="loading-btn">
                                                    Participer
                                                </a>
                                            @endif
                                        @endauth
                                        @guest('customer')
                                            <a href="{{route('customer.redirectTo', ['url' => Request::url()])}}" class="btn-article btn-secondary w-100 mt-3" data-event-id="{{$event->id}}"  class="loading-btn">
                                                Inscrivez-vous pour participer
                                            </a>
                                        @endauth
                                    @else
                                        @if($event->isCurrentCustomerParticipating())
                                            <a class="btn-article btn-primary d-flex justify-content-center align-items-center mt-3">
                                                J'y participe

                                                <i class="material-icons ml-2">check</i>
                                            </a>

                                        @endif
                                    @endif
                                    {{-- @if($event->isCurrentCustomerParticipating())
                                        <div class="text-center mt-2">
                                            <a id="btnCancelParticipation" class="mt-2 text-danger w-100 text-center font-weight-bold" data-event-id="{{$event->id}}">Annuler ma participation</a>
                                        </div>
                                    @endif --}}
                                @else
                                    <a class="btn-article btn-danger w-100 mt-3">Expiré</a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection


@section('scripts')
<script>
    $token = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });
</script>
<script>
    // AJAX participate
    $('#nb_places').on('keyup',function(e){
        e.preventDefault()
        var max = $(this).attr('max')
        var value = $(this).val()
        if(value > max){
            $(this).val(max)
        }
    })
    $("#btnParticipate").on('click', function(e){
        e.preventDefault();
        $("#message").empty();
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;

        var btn = $(this);
        btn.html(_cuteLoader);
        var id = btn.data('event-id');
        var entry_fees = btn.data('entry-fee');

        seatsCount = $("input[name='nb_places']").val();
        availableSeatsCount = parseInt($("#place_number_count").html());

        if(seatsCount == null || seatsCount == ''){
            $("#message").html("Nombre de places requis");
            btn.html("Participer");
            return false;
        }

        if(seatsCount > availableSeatsCount){

            $("#message").html("Nombre de places trop grand");
            btn.html("Participer");
            return false;
        }
        if(seatsCount < 1){

            $("#message").html("Nombre de places trop petit");
            btn.html("Participer");

            return false;
        }

        $.ajax({
                type: "POST",
                url: "{{ route('ajax.event.prepareCheckout') }}",
                data: {
                    event_id: id,
                    seats_count: seatsCount,
                },
                dataType: "json",
                success: function (response) {
                    console.log("Ressponse succes");
                    console.log(response);

                        //
                    if(response.status != "OK"){

                        if(response.status == "ALREADY_EXISTS"){
                            btn.html("J'y participe <i class='material-icons ml-2'>check</i>");
                            return false;
                        }

                        btn.html("Erreur");

                        setTimeout(() => {
                            btn.html("Réessayer");
                        }, 1000);
                        return false;
                    }

                    window.location.assign(response.data.checkout_url);
                    return true;

                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                    btn.html("Erreur");
                    setTimeout(() => {
                        btn.html("Réessayer");
                    }, 2000);
                },
            });
    });



    $("#btnCancelParticipation").click(function(){

        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;

        var btn = $(this);
        btn.html(_cuteLoader);
        var id = btn.data('event-id');

        $.ajax({
                type: "POST",
                url: "{{ route('ajax.event.participate.cancel') }}",
                data: {
                    event_id: id
                },
                dataType: "json",
                success: function (response) {
                    console.log("Ressponse succes");
                    console.log(response);
                    if(response.status != "OK"){
                        btn.html("Erreur");

                        setTimeout(() => {
                            btn.html("Réessayer");
                        }, 1000);
                        return false;
                    }
                    btn.html("");

                    $("#place_number_count").html(parseInt($("#place_number_count").html())+1);

                    $("#btnParticipate").html("Participer");
                    $("#btnParticipate").removeClass("btn-primary");
                    $("#btnParticipate").addClass("btn-secondary");

                    // $('#toReload').load(document.URL+'#toReload');

                    window.location.reload();
                    // this.classList.remove("markCompletedBtn");
                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                    btn.html("Erreur");
                    setTimeout(() => {
                        btn.html("Réessayer");
                    }, 2000);
                },
            });
    });
</script>
@endsection
