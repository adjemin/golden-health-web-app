@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
<style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }

    a {
        color: black;
    }
    .no-display-input{
        /*  */
        border: none !important;
        display: inline-block;
    }
    .address{
        padding: 10px;
    }
    .address:hover{
        background-color: white;
    }
</style>
<style>
    /* Choose color and size */
    /* Color radio inputs */

    input[type=radio] {
    display: none;
    }
    input[type=radio]:checked + label span {
    transform: scale(1.25);
    border: 2px solid var(--secondary);
    }
    input[type=radio]:checked + label .red {
    border: 2px solid #fff;
    }
    input[type=radio]:checked + label .orange {
    border: 2px solid #873a08;
    }
    input[type=radio]:checked + label .yellow {
    border: 2px solid #816102;
    }
    input[type=radio]:checked + label .olive {
    border: 2px solid #505a0b;
    }
    input[type=radio]:checked + label .green {
    border: 2px solid #0e4e1d;
    }
    input[type=radio]:checked + label .teal {
    border: 2px solid #003633;
    }
    input[type=radio]:checked + label .blue {
    border: 2px solid #103f62;
    }
    input[type=radio]:checked + label .violet {
    border: 2px solid #321a64;
    }
    input[type=radio]:checked + label .purple {
    border: 2px solid #501962;
    }
    input[type=radio]:checked + label .pink {
    border: 2px solid #851554;
    }

    label {
        display: inline-block;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        margin-right: 10px;
        cursor: pointer;
    }
    label:hover span {
        transform: scale(1.25);
    }
    label span {
        display: block;
        border-radius: 50%;
        width: 100%;
        height: 100%;
        transition: transform 0.2s ease-in-out;
    }
</style>
<script src="https://cdn.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"/>
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>

@endsection

@section('content')
<main class="container">
      <section class="my-3">
        {{-- Breadcrumb --}}
        <ul class="breadcrumb">
            <li><a href="/">Accueil</a></li>
            <li><a href="/shop">Boutique</a></li>
            <li><a>Checkout</a></li>
        </ul>
        @csrf
    </section>

    <section class="py-5">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 bg-white r">
                <div class="p-3 m-3">
                    <h2 class="fs-title">Résumé commande</h2>
                    <div class="table-responsive table-full-width">
                        <table class="table table-hover table-striped table-adjemin">
                            <tbody class="text-left">
                                <tr>
                                    <th>Article</th>
                                    <td class="text-right" id="orderReviewItemTitle">
                                        Ticket Event: {{$event->title}}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Quantité</th>
                                    <td class="text-right" id="orderReviewQuantity">
                                        {{$order->items->count()}}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Prix</th>
                                    <td class="text-right" id="orderReviewPrice">
                                        {{$event->entry_fee}} Fcfa
                                    </td>
                                </tr>
                                <tr>
                                    <th>Montant</th>
                                    <td class="text-right" id="orderReviewAmount">
                                        {{$order->amount}}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Taxes</th>
                                    <td class="text-right" id="orderReviewTaxes">
                                        0 Fcfa
                                    </td>
                                </tr>
                                <tr>
                                    <th>Moyen de paiment</th>
                                    <td class="text-right" id="orderReviewPaymentMethod">
                                        En ligne
                                    </td>
                                </tr>
                                @forelse($order->coupons as $coupon)
                                    <tr >
                                        <td colspan="6">
                                            <strong>
                                                Bon de réduction {{$coupon->code."  "}} -{{$coupon->pourcentage}}%
                                            </strong>
                                            <span id="orderReviewTotal" class="float-right">
                                                -{{$coupon->pivot->amount_saved}} Fcfa
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                            {{-- Services info from $form_data --}}
                            <tfoot>
                                <tr>
                                    <th colspan="6" style="text-align: right;">
                                        Total à payer <span id="orderReviewTotal">{{$order->amount}}</span> Frcs
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="row d-none">
                        <div class="col-md-12 text-left">
                            <p>
                                L'article sera livré à : <i id="orderReviewDeliveryAddress" style="text-decoration: underline"></i>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 pb-2">
                <div class="r b-fg py-3">
                    <div class="container my-3">
                        <div class="row d-flex justify-content-center">
                            @if($transaction->status == "SUCCESSFUL")
                                <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="fit-image"> </div>
                            @else
                                <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/cancel--v1.png">
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="container" id="buttonHolder">
                        <h2 class="text-danger" id="result-message"></h2>
                            <div class="px-2 py-5">
                                @if($transaction->status == "SUCCESSFUL")
                                    <a class="btn-article w-100 text-center btn-success px-2 text-white">
                                        <i class="fa fa-money mr-2" style="font-size: 20px;"></i> Commande Payée
                                    </a>
                                @endif
                                @if($transaction->status == "FAILED")
                                    <a class="btn-article w-100 text-center btn-danger px-2">
                                        <i class="fa fa-money mr-2" style="font-size: 20px;"></i> Commande Echouée
                                    </a>
                                @endif
                                @if($transaction->status == "EXPIRED")
                                    <a class="btn-article w-100 text-center btn-success px-2">
                                        <i class="fa fa-money mr-2" style="font-size: 20px;"></i> Commande Echouée
                                    </a>
                                @endif
                                @if($transaction->status == "CANCELLED")
                                    <a class="btn-article w-100 text-center btn-danger text-white px-2">
                                        <i class="fa fa-money mr-2" style="font-size: 20px;"></i> Commande Annulée
                                    </a>
                                @endif
                            </div>

                            <div class="row p-2 mt d-md-flex justify-content-between">
                                <div class="col-12 mt-2">
                                    <a class="btn-article btn-primary w-100" href="/all_events">
                                        Voir d'autres events
                                    </a>
                                </div>
                                <div class="col-12 mt-2">
                                    <a class="btn-article btn-secondary w-100" href="/customer/tickets">
                                        Voir mes tickets
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection
