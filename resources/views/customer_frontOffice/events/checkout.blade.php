




@extends("customer_frontOffice.layouts_customer.master_customer")

@section('title', 'Event - '.$event->title)


@section('css')
<style>
    body{
        background: #eee;
    }
</style>
<script src="https://cdn.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript"></script>
@endsection

@section('content')
<main class="container">
    <!--  -->
    <section class="pt-3">
        <div class="">
            <div class="text-center p-md-3 p-2 r bg-white">
                <h1 class="font-weight-bold text-center primary-text-color mb-3 text-uppercase">{{$event->title}}</h1>
                <div>
                </div>
            </div>
        </div>
    </section>

    <section class="my-3">
        {{-- Breadcrumb --}}
        <ul class="breadcrumb">
            <li><a href="/">Accueil</a></li>
            <li><a href="/all_events">Events</a></li>
            <li><a>{{$event->title}}</a></li>
        </ul>
        @csrf
    </section>

    <section class="py-5">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 bg-white r py-3">
                <div class="p-3 m-3">
                    <h2 class="fs-title">Résumé commande</h2>
                    <div class="table-responsive table-full-width">
                        <table class="table table-hover table-striped table-adjemin">
                            <tbody class="text-left">
                                <tr>
                                    <th>Article</th>
                                    <td class="text-right" id="orderReviewItemTitle">
                                        Ticket Event: {{$event->title}}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Quantité</th>
                                    <td class="text-right" id="orderReviewQuantity">
                                        {{$seatsCount}}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Prix</th>
                                    <td class="text-right" id="orderReviewPrice">
                                        @if($event->isFree())
                                            Gratuit
                                        @else
                                            {{$event->entry_fee}}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Montant</th>
                                    <td class="text-right" id="orderReviewAmount">
                                        {{$order->amount}}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Taxes</th>
                                    <td class="text-right" id="orderReviewTaxes">
                                        0 Fcfa
                                    </td>
                                </tr>
                                <tr>
                                    <th>Moyen de paiement</th>
                                    <td class="text-right" id="orderReviewPaymentMethod">
                                        En ligne
                                    </td>
                                </tr>
                                @forelse($order->coupons as $coupon)
                                    <tr >
                                        <td colspan="6">
                                            <strong>
                                                Bon de réduction {{$coupon->code."  "}} -{{$coupon->pourcentage}}%
                                            </strong>
                                            <span id="orderReviewTotal" class="float-right">
                                                -{{$coupon->pivot->amount_saved}} Fcfa
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                            {{-- Services info from $form_data --}}
                            <tfoot>
                                <tr>
                                    <th colspan="6" style="text-align: right;">
                                        Total à payer <span id="orderReviewTotal">{{$order->amount}}</span> Fcfa
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>

                <input type="number" hidden name="seats_count" placeholder="nombre de places" class="form-control" min="1" value="{{$seatsCount}}">
                <input hidden name="invoice_id" value="{{$invoice->id}}">
                <span class="text-danger font-weight-bold" id="error_message"></span>
                <div class="" id="buttonHolder">
                    <div class="p-2">
                        @if($event->isFree())
                            <a class="btn-article btn-secondary w-100 mt-3 d-flex justify-content-center align-items-center" data-event-id="{{$event->id}}" id="btnParticipate" data-event-entry-fee="{{$event->entry_fee}}" class="loading-btn">

                                Participer
                            </a>
                        @else
                            <form method="POST" action="/payment/fromInvoice">
                                @csrf
                                <input hidden name="invoice_id" value="{{$invoice->id}}">
                                <input hidden name="redirect_to" value="event">

                                <button type="submit" class="btn-article btn-secondary w-100 mt-3 d-flex justify-content-center align-items-center loading-btn">

                                        Payer {{$order->amount}} Fcfa
                                </button>
                            </form>
                        @endif


                    </div>
                </div>
            </div>
            @if(!$event->isFree())
                <div class="col-md-4">
                    <div class="p-3 mx-3 r bg-white">
                        <h2 class="fs-title mb-5">Vous avez un coupon ?</h2>
                        <p>
                            Entrez votre coupon ci-dessous et bénéficiez d'une réduction
                        </p>
                        <div class="form-group">
                            @forelse($order->coupons as $coupon)
                                <div class="text-primary font-weight-bold text-uppercase my-2 text-right">
                                    coupon
                                    {{$coupon->code."  (-".$coupon->pourcentage."%) "}} Ajouté
                                </div>
                            @empty
                            @endforelse
                                <input class="form-control" type="text" name="coupon_code" placeholder="Entrez votre code">
                                <input hidden name="invoice_id" value="{{$invoice->id}}">
                                <span id="coupon_error" class="text-danger font-weight-bold"></span>
                                <a id="submitCoupon" class="btn btn-article btn-primary w-100 mt-3">Vérifier</a>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
</main>

@endsection


@section('scripts')
<script>
    $token = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });
</script>
{{-- Coupon adding Script --}}
<script>
    $("#submitCoupon").click(function name(params) {
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;

        var coupon_code = $("input[name='coupon_code']").val();
        var invoice_id = $("input[name='invoice_id'").val();

        $(this).html(_cuteLoader);
        var $btn = $(this);
        var $messageDiv = $("#coupon_error");

        if(coupon_code == ''){
            $("#coupon_error").html("Vous devez entrez un coupon");
            $("#submitCoupon").html("Vérifier")
            return false;
        }
        if(invoice_id == ''){
            $("#coupon_error").html("Erreur survenue");
            $("#submitCoupon").html("Veuillez réessayer plus tard")
            return false;
        }


        $.ajax({
                type: "POST",
                url: "{{ route('ajax.coupon.add') }}",
                data: {
                    "invoice_id": invoice_id,
                    "coupon_code": coupon_code,
                },
                dataType: "json",
                success: function (response) {
                    //
                    console.log(">> success");
                    console.log(response);
                    if(response.status == null){
                        $btn.html("Veuillez réessayer plus tard");
                        $messageDiv.html("Une erreur est survenu");
                        return false;
                    }
                    // managing status codes
                    if(response.status != "OK"){
                        $btn.html("OUPS");
                        if(response.error != null){
                            if(response.error.message != null){
                                $messageDiv.html(response.error.message);
                                setTimeout(() => {
                                    $btn.html("Réessayer");
                                }, 1000);
                                return false;
                            }
                            $btn.html("Réessayer")
                        }

                    }
                    $btn.html("Coupon Ajouté");

                    window.location.reload();

                },
                error: function(response){
                    console.log(">> erreur");
                    $btn.html("Veuillez réessayer plus tard");
                    $messageDiv.html("Une erreur est survenue");
                    console.log(response);
                }
        });
    });
</script>
<script>
    var $btn = $("#btnParticipate");

    // AJAX participate
    $("#btnParticipate").click(function(){

        $("#message").empty();
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;

        var btn = $(this);
        btn.html(_cuteLoader);
        var id = btn.data('event-id');
        var invoice_id = $("input[name='invoice_id']").val();
        var seats_count = $("input[name='seats_count']").val();

        $.ajax({
                type: "POST",
                url: "{{ route('ajax.event.participate') }}",
                data: {
                    event_id: id,
                    invoice_id: invoice_id,
                    seats_count: seats_count,
                },
                dataType: "json",
                success: function (response) {
                    console.log("Ressponse succes");
                    console.log(response);
                    if(response.status == "OK"){
                        // * For a free event no transaction needed

                            btn.html("J'y participe <i class='material-icons ml-2'>check</i>");
                            btn.removeClass('btn-secondary');
                            btn.addClass('btn-primary');

                            // moving to checkout_finished
                            // window.location.replace('/customer/event/checkout_finish/'+response.order.id);
                            window.location.replace('/customer/event/checkout_finish/'+response.transaction.payment_reference);
                            return true;

                    }
                        //
                    $("#error_message").html(response.message);
                    btn.html("Une erreur est survenue");
                    setTimeout(() => {
                        btn.html("Veuillez réessayez")
                    }, 3000);
                        // if(response.status == "ALREADY_EXISTS"){
                        //     btn.html("J'y participe <i class='material-icons ml-2'>check</i>");
                        //     return false;
                        // }
                        // btn.html("Erreur");

                        // setTimeout(() => {
                        //     btn.html("Réessayer");
                        // }, 1000);
                        // return false;
                    //
                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                    btn.html("Erreur");
                    setTimeout(() => {
                        btn.html("Réessayer");
                    }, 2000);
                },
            });
    });
</script>

@endsection
