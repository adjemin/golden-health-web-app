@extends("customer_frontOffice.layouts_customer.master_customer")

@section('content')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<link href="https://cdn.jsdelivr.net/npm/smartwizard@5/dist/css/smart_wizard_all.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{asset('css/evo-calendar.css')}}">
<link rel="stylesheet" href="{{asset('css/jquery.timepicker.min.css')}}">
<style>
    body
    {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }
    a
    {
        color: black;
    }

    .start-link-patner:hover {
        padding: 10px 30px;
        color: white;
        font-weight: bold;
        border: none;
        background-color: #edaa0d;
    }

    .bootstrap-select .btn-light {
    background-color:transparent ! important;
    border-color:transparent ! important;
    }
    .bootstrap-select .dropdown-toggle .filter-option-inner-inner{
        font-size: small ! important;
    }
    .bootstrap-select .dropdown-menu{
        border-radius: 0px ! important;
        left:-13px ! important;
    }
    .bootstrap-select.show-tick .dropdown-menu li a span.text{
        font-size:small ! important;
    }
</style>
@endsection
<section>
    <div class="container">
        <div class="py-2">
            <div class="courses-makes">
                <div>
                    <p>Recapitulatif d'un cours</p>
                    <p>
                      Dans cette section vous pourriez voir en detail le récapitulatif de votre cours.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mb-5">
    <div class="container">
        <div class="py-2">
            <div class="profil-box-content">
                <div class="mb-5">
                        <div id="smartwizard">
                            <ul class="nav">
                               <li>
                                   <a class="nav-link" href="#step-1">
                                      Informations générales
                                   </a>
                               </li>
                               <li>
                                   <a class="nav-link" href="#step-2">
                                    Détail du cours
                                   </a>
                               </li>
                               <li>
                                  <a class="nav-link" href="#step-3">
                                    Calendrier
                                  </a>
                               </li>
                               <li>
                                  <a class="nav-link" href="#step-4">
                                    Prix
                                  </a>
                               </li>
                            </ul>

                            <div class="tab-content">
                               <div id="step-1" class="tab-pane" role="tabpanel">
                                  <div class="mt-4">
                                      <div class="form-group mb-3 mt-4">
                                          <p for="">Votre Discipline <span class="required"></span> </p>
                                          <p>{{$course->discipline->name ?? ""}}</p>
                                          <small class="form-text text-muted">Il permettra de trouver votre cours grâce au moteur de recherche</small >
                                      </div>
                                      <div class="form-group mb-3 mt-4">
                                          <p for="">Votre pratique du sport et votre expérience en tant que Coach</p>
                                          <p><small>
                                          Quelles ont été vos expériences dans ce sport? Avez-vous pratiqué en compétition ? Quels prix avez-vous gagné ? <br/>
                                          Quelle expérience avez-vous en tant qu'entraîneur? À quel niveau avez-vous entraîné ?<br/>
                                          Quels types de public avez vous déjà encadré ?
                                          </small></p>
                                          <textarea class="form-control" name="experience" id="experience" rows="5">{{ $course->detail->experience ?? ""}}</textarea>
                                      </div>
                                      <div class="form-group mb-3 mt-4">
                                          <p for="">Ma séance type</p>
                                          <p><small>
                                          Description de ma séance type. <br/>
                                          Qu'est-ce qu'un client peut attendre d'une session typique avec vous? Avez-vous des techniques spécifiques ?<br/>
                                          Avez vous du petit matériel et lequel ? Quelles connaissances techniques vous utilisez ?<br/>
                                          Eviter les raccourcis, ex : Échauffement, corps de séance, retour au calme.
                                          </small></p>
                                          <textarea class="form-control" name="seance_type" id="seance_type" rows="5">{{$course->detail->seance_type ?? ""}}</textarea>
                                      </div>


                                      <div class="toolbar toolbar-bottom" style="text-align: right;">
                                        <button type="button" class="btn btn-info prevbtn" name="button">Précédent</button>
                                        <button type="button" class="btn btn-success nextbtn" name="button">continuer</button>
                                      </div>
                                  </div>
                               </div>
                               <div id="step-2" class="tab-pane" role="tabpanel">
                                  <div class="mt-4">
                                    <div class="form-group mt-4 col-md-6">
                                        <label for="">Diplôme</label>
                                        @if(!is_null($course_detail->diplome_url))
                                          <p class="mt-1" id="div-diplome"> <a href="{{$course_detail->diplome_url}}" class="diplome-link" style="color:#17a2b8!important; font-size:12px;" target="_blank"><i class="fa fa-arrow-right"></i>  Visualiser le document sélectionner   </a> </p>
                                        @else
                                          <p class="mt-1" id="div-diplome">Aucun diplome sélectionner</p>
                                        @endif
                                      </div>
                                      <div class="form-group mb-3 mt-4 col-md-12">
                                          <p for="">Le cours a lieu : <span class="required"></span></p>
                                          @php
                                            $lieux = collect($course_lieu)->map(function($lieu){
                                              return $lieucourse->name ?? null;
                                            })->reject(function($lieu){
                                              return is_null($lieu) || empty($lieu);
                                            })->unique();
                                          @endphp
                                          @foreach($lieux as $lieu)
                                            <p>{{$lieu}}</p>
                                          @endforeach
                                      </div>
                                      <div class="form-group mb-3 mt-4 col-md-12">
                                          <p for="">Objectif principal du cours :</p>
                                          @php
                                            $courseObjectives = collect($course_objective)->map(function($courseObjective){
                                                return $courseObjective->objective->name ?? null;
                                            })->reject(function($courseObjective){
                                                return is_null($courseObjective) || empty($courseObjective);
                                            })->unique();
                                          @endphp
                                          @foreach($courseObjectives as $courseObjective)
                                            <p>{{$courseObjective}}</p>
                                          @endforeach
                                      </div>
                                      <div class="form-group mb-3 mt-4 col-md-12">
                                          <p>Type de cours : <span class="required"></span></p>
                                          <p>{{$course->course_type ?? ""}}</p>
                                      </div>

                                      <div class="form-group mb-3 mt-4 col-md-6">
                                        <label for="">Nombre de participant <span class="required"></span></label>
                                        <p>{{$course->detail->nb_person ?? ""}}</p>
                                      </div>

                                      <div class="toolbar toolbar-bottom" style="text-align: right;">
                                        <button type="button" class="btn btn-info prevbtn" name="button">Précédent</button>
                                        <button type="button" class="btn btn-success nextbtn" name="button">continuer</button>
                                      </div>
                                  </div>
                               </div>
                               <div  id="step-3" class="tab-pane" role="tabpanel" style="height: 400px; width:100%; overflow-x: auto;">
                                    <div class="mt-4">
                                        <div class="row">
                                          <div class="col-md-12">
                                            <table class="table table-bordered" id="availabityTable">
                                              <thead>
                                                <tr>
                                                  <th>Date de debut</th>
                                                  <th>Date de fin</th>
                                                  <th>Heure de depart</th>
                                                  <th>Heure de fin</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                @foreach($availabilities as $availability)
                                                  <tr>
                                                      <td>{{ $availability->date_debut }}</td>
                                                      <td>{{ $availability->date_fin }}</td>
                                                      <td>{{ $availability->start_time }}</td>
                                                      <td>{{ $availability->end_time }}</td>
                                                  </tr>
                                                @endforeach
                                              </tbody>
                                            </table>
                                          </div>
                                        </div>
                                        <div class="toolbar toolbar-bottom" style="text-align: right;margin-top:10px;">
                                          <button type="button" class="btn btn-info prevbtn" name="button">Précédent</button>
                                          <button type="button" class="btn btn-success nextbtn" name="button">continuer</button>
                                        </div>
                                    </div>
                               </div>
                               <div id="step-4" class="tab-pane" role="tabpanel">
                                  <div class="mt-4">
                                    <div class="form-group mt-4">
                                        <p>Prix du cours</p>
                                        <span>Sur une base d'une heure de cours</span>
                                    </div>
                                    <div class="form-group mt-4">
                                        <input id="price" class="form-control" name="price" type="number" min="0" step="1" value="{{$course->price ?? ''}}" style="font-weight: bold; border: 1px solid green; color: green;" disabled>
                                    </div>
                                    <div class="toolbar toolbar-bottom" style="text-align: right;">
                                      <button type="button" class="btn btn-info prevbtn" name="button">Précédent</button>
                                    </div>
                                  </div>
                               </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/smartwizard@5/dist/js/jquery.smartWizard.min.js" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function(){
    // SmartWizard initialize
    $('#smartwizard').smartWizard({
      toolbarSettings: {
        showNextButton: false, // show/hide a Next button
        showPreviousButton: false, // show/hide a Previous button
        toolbarExtraButtons: [] // Extra buttons to show on toolbar, array of jQuery input/buttons elements
      }
    });

    $(".prevbtn").on("click", function() {
      // Navigate previous
      $('#smartwizard').smartWizard("prev");
      return true;
    });

    $(".nextbtn").on("click", function() {
      // Navigate previous
      $('#smartwizard').smartWizard("next");
      return true;
    });
  });
</script>
@endsection
