@extends("customer_frontOffice.layouts_customer.master_customer")

@section('content')

@section('css')
<style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }

    a {
        color: black;
    }

    .wrap-resultats .resultats {
        font-size: 64px;
        font-weight: 900;
        color: #099;
        line-height: 100%;
        margin: 25px 0 7px;
        font-family: "Arial Black", "Arial Bold", Gadget, sans-serif;
    }

    .wrap-resultats .txt-resultats {
        text-transform: uppercase;
        font-size: 18px;
        font-weight: 700;
        line-height: 120%;
        color: #099;
    }

    #progressbar {
    margin-bottom: 3vh;
    overflow: hidden;
    color: rgb(252, 103, 49);
    padding-left: 0px;
    margin-top: 3vh
    }

    #progressbar li {
        list-style-type: none;
        font-size: x-small;
        width: 25%;
        float: left;
        position: relative;
        font-weight: 400;
        color: rgb(160, 159, 159)
    }

    #progressbar #step1:before {
        content: "";
        color: rgb(252, 103, 49);
        width: 5px;
        height: 5px;
        margin-left: 0px !important
    }

    #progressbar #step2:before {
        content: "";
        color: #fff;
        width: 5px;
        height: 5px;
        margin-left: 32%
    }

    #progressbar #step3:before {
        content: "";
        color: #fff;
        width: 5px;
        height: 5px;
        margin-right: 32%
    }

    #progressbar #step4:before {
        content: "";
        color: #fff;
        width: 5px;
        height: 5px;
        margin-right: 0px !important
    }

    #progressbar li:before {
        line-height: 29px;
        display: block;
        font-size: 12px;
        background: #ddd;
        border-radius: 50%;
        margin: auto;
        z-index: -1;
        margin-bottom: 1vh
    }

    #progressbar li:after {
        content: '';
        height: 2px;
        background: #ddd;
        position: absolute;
        left: 0%;
        right: 0%;
        margin-bottom: 2vh;
        top: 1px;
        z-index: 1
    }

    .progress-track {
        padding: 0 8%
    }

    #progressbar li:nth-child(2):after {
        margin-right: auto
    }

    #progressbar li:nth-child(1):after {
        margin: auto
    }

    #progressbar li:nth-child(3):after {
        float: left;
        width: 68%
    }

    #progressbar li:nth-child(4):after {
        margin-left: auto;
        width: 132%
    }

    #progressbar li.active {
        color: black
    }

    #progressbar li.active:before,
    #progressbar li.active:after {
        background: rgb(252, 103, 49)
    }

    #details {
    font-weight: 400
    }

    .info .col-5 {
        padding: 0
    }

    #heading {
        color: grey;
        line-height: 6vh
    }

    .pricing {
        background-color: #ddd3;
        padding: 2vh 8%;
        font-weight: 400;
        line-height: 2.5
    }

    .item_deat {
        font-weight: 400;
        line-height: 2.5
    }

    .pricing .col-3 {
        padding: 0
    }

    .total {
        padding: 2vh 8%;
        color: rgb(252, 103, 49);
        font-weight: bold
    }

    .total .col-3 {
        padding: 0
    }

    .modal-body {
      padding: 0 2rem;
    }

</style>
@endsection

@section('content')
<!-- header -->
<section>
    <header class="become-coach-header">
        <div class="become-coach-header-des">
            <h3 class="font-weight-bold text-center">Tableau de bord </h3>
            <p class="text-center">
                Gérez toutes vos informations
            </p>
        </div>
    </header>
</section>


<section class="py-5">
    <section class="">
        <div class="container">
            {{-- Breadcrumb --}}
            <ul class="breadcrumb">

                <li><a href="/">Accueil</a></li>
                <li><a href="/customer/dashboard">Dashboard</a></li>
                <li><a>Mes tickets d'event</a></li>
                {{-- <li><a href="/tous_les_articles/">Tous les articles</a></li> --}}
            </ul>
            @csrf
        </div>
    </section>
    <div class="container">
        <div class="row">
            {{-- <div class="col-xl-4 d-xl-block col-md-5 col-sm-12 d-none">
                <div class="profil-box-content mb-2">
                    <div>
                        <div class="mb-2">
                            <img src="{{$customer->photo_url}}" class="rounded-circle" height="60" width="60" alt="">
                        </div>
                        <p class="font-weight-bold">{{$customer->name}}</p>
                        <p>Statut : <span class="self-challenger-box">@if($customer->is_coach) Coach @else Self Challenger @endif</span></p>
                        <?php $date_start = Carbon\Carbon::parse($customer->created_at)->locale('fr_FR'); ?>
                        <p>Inscrit depuis le {{$date_start->isoFormat('LL')}}</p>
                        {!! $customer->profileCompletionProgress() !!}
                        <a href="{{ route('customer.manage_profil') }}" class="primary-text-color update-profil-link">Modifier mon
                            profil </a>
                    </div>
                </div>
                @if($customer->is_coach == null)
                <div class="profil-box-content">
                    <div class="row">
                        <div class="col">
                            <a href="{{url('/customer/packs')}}">
                                <center><img src="{{asset ('customer/images/contract.png') }}" class="text-center" width="30" height="30" alt=""></center>
                                <center><small>Abonnements</small></center>
                            </a>
                        </div>

                        <div class="col">
                            <center>
                                <small><img src="{{ asset('customer/images/mail.png') }}" class="text-center" width="30" height="30" alt=""></small>
                            </center>
                            <center>
                                <small>Messages</small>
                            </center>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col">
                            <a href="{{url('/customer/coupons')}}">
                                <center><img src="{{asset ('customer/images/coupon.png') }}" class="text-center" width="30" height="30" alt="">
                                </center>
                                <center><small>Bon de reductions </small></center>
                            </a>
                        </div>

                        <div class="col">
                            <center>
                                <small><img src="{{asset ('customer/images/ch.png') }}" class="text-center" width="30" height="30" alt=""></small>
                            </center>
                            <center>
                                <small>Mes coachs</small>
                            </center>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col">
                            <center><img src="{{asset ('customer/images/heart.png') }}" class="text-center" width="30" height="30" alt="">
                            </center>
                            <center><small>Bon de reductions </small></center>
                        </div>
                        <div class="col r b-bg py-2 b-bg">
                            <a href="/customer/tickets">
                                <center><img src="{{asset ('customer/images/ticket.png') }}" class="text-center" width="30" height="30" alt="">
                                </center>
                                <center>
                                    <small>Mes Tickets</small>
                                </center>
                            </a>
                        </div>
                    </div>
                </div>
                @endif
            </div> --}}
            {{-- <div class="col-xl-8 col-md-12"> --}}
            <div class="col-md-12">

                @if($tickets != null)
                    <div class="profil-box-content p-0 mb-2 r">
                        <div class="courses-entete r">
                            Mes Tickets d'event ({{$tickets->count()}})
                        </div>
                        <div class="courses-content">
                            @if(count($tickets) > 0)
                                {{-- <p>Tickets en cours</p> --}}
                                <div class="mt-4">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Status</th>
                                                <th>Code</th>
                                                <th>Paiement</th>
                                                <th>Event</th>
                                                <th>Date</th>
                                                <th>Nb Places</th>
                                                <th>Montant</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($tickets as $ticket)
                                                <tr id="wishlist_10136" data-product-id="{{$ticket->id}}">
                                                    <td> {!!$ticket->statusText()!!} </td>
                                                    <td>{!!$ticket->codeText()!!}</td>
                                                    <td>{!!$ticket->paymentStatusText()!!}</td>
                                                    <td>{{$ticket->event->title}}</td>
                                                    <td>{{$ticket->event->date_event}}</td>
                                                    <td>{{$ticket->seats_count}}</td>
                                                    <td>{!!$ticket->amountText()!!}</td>
                                                    {{-- <td> <button type="button" class="btn btn-success booking_detail" data-modal="customer" data-id="{{$ticket->id}}" name="button" style="font-size:10px" disabled title="En attente de confirmation">Valider</button> </td> --}}
                                                    <td>
                                                        <a href="/customer/tickets/pdf/{{$ticket->id}}" class="btn btn-info">Imprimer</a>

                                                        {{--<a href="/customer/tickets/view/{{$ticket->id}}" class="btn btn-success">Voir</a>--}}
                                                        
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <p class="text-center fs-l fw-500 pt-3">
                                    Vous n'avez aucun ticket pour l'instant
                                </p>
                                <div class="container d-flex justify-content-center">

                                    <a href="/all_events" class="mx-auto px-3 btn-article btn-secondary">Voir les events</a>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

</section>
@endsection
@push('js')

@endpush
