<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GOLDEN HEALTH</title>
    <link rel="icon" href="{{ asset('customer/images/gh1.png') }}">
    <!-- Montserrat Font -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap"
        rel="stylesheet">
    <!--  -->
    <link rel="stylesheet" href="{{ asset('customer/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('customer/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('customer/css/style.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://unpkg.com/flickity@2.2.1/dist/flickity.css">
    <script src="https://unpkg.com/flickity@2.2.1/dist/flickity.pkgd.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style>
        .card {
            position: relative;
            /* width: 300px; */
            width: auto;
            height: 320px;
            background: #000;
            overflow: hidden;
            border: none;
            box-shadow: 2px 2px 3px #666;
        }

        .card .image {
            /* background-size: auto; */
            width: 100%;
            height: 100%;
            overflow: hidden;
        }

        .card .image img {
            width: 100%;
            /* height: 100%; */
            display: block;
            height: auto;
            transition: .5s;
        }

        .card:hover .image img {
            opacity: .2;
            /* transform: translateX(30%); */
            transform: translateY(-30%);
        }

        .card .title {
            position: absolute;
            left: 50%;
            transform: translateX(-50%);
            text-align: center;
            /* top: 10px; */
            top: 50px;
            opacity: 0;
            transition: all .3s;
        }

        .card:hover .title {
            top: 10px;
            opacity: 1;
        }

        .card .details {
            position: absolute;
            bottom: -130px;
            width: 100%;
            height: 70%;
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
            /* background: #FFC107; */
            transition: .5s;
            /* transform-origin: left; */
            /* transform: perspective(2000px) rotateY(-90deg); */
            /* transform: translateY(100px); */
        }

        .card:hover .details {
            transform: translateY(-130px);
        }

        .card .details .center {
            padding: 10px;
            text-align: center;
            height: 100%;
            /* background: #e9ecef; */
            background: linear-gradient(rgba(0, 0, 0, .5), rgba(0, 0, 0, .5));
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
        }

        .card .details .center h1 {
            margin: 0;
            padding: 0;
            color: #1c6a49;
            opacity: 1;
            font-weight: 700;

            line-height: 20px;
            font-size: 22px;
            text-transform: uppercase;
            transition: all .3s;
        }

        .card:hover .details .center h1 {
            opacity: 0;
            margin-bottom: 20px;
        }

        .card .details .center h1 span {
            font-size: 14px;
            color: #262626;
        }

        .card .details .center p {
            margin: 10px 0;
            padding: 0;
            font-size: 12px;
            letter-spacing: 1px;
            color: transparent;
            transition: color .4s;
        }

        .card:hover .details .center p {
            /* color: #000; */
            color: #fff;
        }

    </style>

</head>

<body>
    <!-- nav -->
    <section>
        <div class="container">
            @auth('customer')
                @include('customer_frontOffice.profile.partials.header')
            @endauth
            @guest('customer')
                @include('customer_frontOffice.layouts_customer.navbar')
            @endguest
        </div>
    </section>
    <!-- discipline -->

    <section>
        <div class="container py-2">
            <h2 class="text-center how-it-works mb-5">Nos Disciplines</h2>
            <div class="row">

                @if (isset($disciplines) and !empty($disciplines))
                    @foreach ($disciplines as $discipline)
                        @if ($discipline)
                            <div class="col-lg-3 col-md-3 col-sm-12 mt-2">
                                <a href="/search_coach?discipline={{ base64_encode($discipline->id) }}&city=">
                                    <div class="mb-3">
                                        <div class="card">
                                            <div class="image"
                                                style="background-position: center; background-image: linear-gradient(rgba(0, 0, 0, .5), rgba(0, 0, 0, .5)), url({{ $discipline->image }}) !important;">
                                            </div>
                                            <div class="title">
                                                <h3 class="text-uppercase font-weight-bold text-secondary">
                                                    {{ $discipline->name ?? '' }}
                                                </h3>
                                            </div>
                                            <div class="details" style="">
                                                <div class="center">
                                                    <h1 class="text-uppercase">
                                                        {{ $discipline->name ?? '' }}
                                                    </h1>
                                                    <p class="" style="font-size: 10px !important;">
                                                        {!! $discipline->description ?? '' !!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </section>

    <!-- Advertissment -->

    <section class="py-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <h2><b>Gardez Votre Santé en poche</b> </h2>
                    <p>Télécharger l'application mobile</p>

                    <div class="">
                        <a href="#" target="_blank" class="btn btn-black mx-auto mt-2">
                            <span class="">
                                <img src="{{ asset('customer/images/google-play.png') }}" class="icon mr-2"
                                    width="24px" alt="">
                                Téléchargez sur <br>
                                Google Play
                            </span>
                        </a>&nbsp;&nbsp;
                        <a href="#" target="_blank" class="btn btn-black mx-auto mt-2">
                            <span class="">
                                <i class="fa fa-apple mr-2" aria-hidden="true"></i>
                                Téléchargez sur <br>
                                Apple Store
                            </span>
                        </a>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <img src="{{ asset('customer/images/jshdshd.png') }}" alt="" width="210">
                </div>
            </div>
        </div>
    </section>

    <!-- footer -->
    @include('customer_frontOffice.layouts_customer.footer')
    <!-- / Footer -->

    <script src="{{ asset('customer/js/jquery.min.js') }} "></script>
    <script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('customer/js/popper.min.js') }} "></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- GetButton.io widget -->
    <script type="text/javascript">
        (function() {
            var options = {
                whatsapp: "+2250777720507", // WhatsApp number
                call_to_action: "Nous contactez", // Call to action
                position: "right", // Position may be 'right' or 'left'
            };
            var proto = document.location.protocol,
                host = "getbutton.io",
                url = proto + "//static." + host;
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = url + '/widget-send-button/js/init.js';
            s.onload = function() {
                WhWidgetSendButton.init(host, proto, options);
            };
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        })();
    </script>
    <!-- /GetButton.io widget -->
    <script>
        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("slide-item");
            var dots = document.getElementsByClassName("circle-indicator");
            if (n > slides.length) {
                slideIndex = 1
            }
            if (n < 1) {
                slideIndex = slides.length
            }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " active";

            setTimeout(() => {
                plusSlides(1);
            }, 3000);
        }
    </script>
</body>

</html>
