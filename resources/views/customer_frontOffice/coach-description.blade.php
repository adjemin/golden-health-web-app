@extends("customer_frontOffice.layouts_customer.master_customer")
@section('css')
<style>
    body {
        background-color: rgba(238, 238, 238, 0.336);
    }

    .qui-somme-nous {
        line-height: 2.3;
    }

</style>
@endsection

@section('content')
<section class="py-5">
    <div class="container">
        <h1 class="font-weight-bold primary-text-color mb-3">Coachs </h1>

        <p class="font-weight-bold h6 mb-4">Être coach Golden Health c’est :</p>
        <ul>
            <li class="mb-4"><i>Être professionnel.</i></li>
            <li class="mb-4"><i>Avoir de l’éthique.</i></li>
            <li class="mb-4"><i>Avoir de l’empathie.</i></li>
            <li class="mb-4"><i>
                    Participer au respect et à la valorisation de la profession de coach sportif et en récolter les fruits. </i></li>
            <li><i>
                    Être coach Golden Health c’est aussi toucher une plus large audience à travers notre plateforme et nos campagnes de promotions et de sensibilisations à la pratique du sport.
                    Partagez-vous ces valeurs ?
                    Rejoignez-nous !!!
                </i></li>
        </ul>


        <p class="mb-5">Partagez-vous ces valeurs ? <br><br> Inscrivez-vous donc !!! </p>

        <a href="{{ route('become.coach') }}" class="primary-button">
            S'inscrire
        </a>


    </div>
</section>

@endsection
