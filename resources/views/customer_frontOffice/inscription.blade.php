@extends("customer_frontOffice.layouts_customer.master_customer")


@section('css')

<style>
    body {
        background-image: linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, .6)), url("customer/images/bg2.jpg");
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        position: relative;
    }

    a {
        color: black;
    }

</style>

@section('content')

    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7 offset-md-3 col-sm-12">
                    <div class="login-box">
                        <center>
                            <h4> <b>Créer mon compte </b> </h4>
                        </center>
                        <br>
                        <center>
                            <small>Déjà inscrit sur Golden Health ? &nbsp; <span><a href="#" class="secondary-text-color">Connectez-vous</a></span></small>
                        </center>
                        <div class="row mt-3">
                            <div class="col-md-6 mt-2">
                                <div class="login-google">
                                    <a href="#"> <img src="{{asset('customer/images/téléchargement.png')}}" width="15px" height="15px" alt="">
                                        &nbsp;
                                        &nbsp;
                                        Connexion avec
                                        google</a>
                                </div>
                            </div>
                            <div class="col-md-6 mt-2">
                                <div class="login-facebook">
                                    <a href="#"> <img src="{{asset('customer/images/facebook.png')}}" width="15px" height="15px" alt="">&nbsp; Connexion avec facebook</a>
                                </div>
                            </div>
                        </div>
                        <div class="row mx-auto mt-4">
                            <div class="mini-divider mt-3"></div> &nbsp;&nbsp;
                            <p>ou</p>&nbsp;&nbsp;
                            <div class="mini-divider mt-3"></div>
                        </div>

                        <form action="" class="">

                            <div class="row mb-4 ml-1">
                                <div class="form-check mr-4">
                                    <input class="form-check-input" type="radio" name="genre" id="genre-mr" value="option1" checked>
                                    <label class="form-check-label" for="genre-mr">
                                        M.
                                    </label>
                                </div>

                                <div class="form-check mr-4">
                                    <input class="form-check-input" type="radio" name="genre" id="genre-mme" value="option2">
                                    <label class="form-check-label" for="genre-mme">
                                        Mme
                                    </label>
                                </div>
                            </div>
                            <div class="form-group mt-3 ">
                                <input type="text" name="first_name" class="form-control input-style placeholder-style" id="first_name" aria-describedby="prenom" placeholder="Prénom">
                            </div>

                            <div class="form-group mt-3 ">
                                <input type="text" name="last_name" class="form-control input-style placeholder-style" id="last_name" aria-describedby="nom" placeholder="Nom">
                            </div>

                            <div class="form-group mt-3 ">
                                <input type="email" name="email" class="form-control input-style placeholder-style" id="email" aria-describedby="email" placeholder="Adresse e-mail profesionnelle">
                            </div>

                            <div class="form-group mt-2">
                                <input type="password" class="form-control input-style placeholder-style" id="password" aria-describedby="password" placeholder="Mot de passe">
                            </div>

                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                <label class="form-check-label" for="inlineCheckbox1">J'accepte les
                                    <a href="/conditions" class="secondary-text-color">conditions générales d'utilisation</a></label>
                            </div>

                            <div class="form-check mt-1">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                <label class="form-check-label" for="inlineCheckbox2">                   S'inscrire à la newsletter</label>
                            </div>

                        <a href="#" class="password-forget ">J'ai perdu mon mot de passe</a>
                        <a href="{{ route('profile') }}" class="btn login-button col-lg-12 mt-4">S'inscrire </a>

                        {{-- <input type="submit" class="btn login-button col-lg-12 mt-4" value="S'inscrire  "> --}}
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
