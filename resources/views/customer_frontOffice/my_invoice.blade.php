
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Golden Health - Invoice #92237</title>

    <link rel="stylesheet" href="{{ asset('customer/css/bootstrap.min.css') }} ">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{asset('invoices/invoice.css')}}" rel="stylesheet">
</head>
<body>

    <div class="container-fluid invoice-container">


            <div class="row invoice-header">
                <div class="invoice-col"  style="float:left; text-align:left">
                  <p><img src="https://res.cloudinary.com/ahoko/image/upload/v1600908196/gh1_vsk8g1.png" title="Golden" /></p>
                  <h3>Invoice #{{$invoice->id}}</h3>
                </div>
                <div class="invoice-col text-center">
                    @if($invoice->status == 'paid')
                    <div class="invoice-status">
                        <span class="pais">Paid</span>
                    </div>
                    @else
                    <div class="invoice-status">
                        <span class="unpaid">Unpaid</span>
                    </div>
                    <div class="small-text">
                      Due Date: {{$invoice->invoice_date}}
                    </div>
                    <div class="payment-btn-container hidden-print" align="center">
                        <form method="POST" action="#" id="paymentfrm">
                           {{ csrf_field() }}
                           <input type="hidden" name="booking_id" id="booking_id" value="{{$invoice->meta_data_id}}">
                          <button type="submit" class="btn btn-success btn-sm" id="btnPayNow" value="Submit">Payer maintenant</button>
                        </form>
                    </div>
                    @endif
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="invoice-col right">
                    <strong>Pay To</strong>
                    <address class="small-text">
                        GOLDEN HEALTH<br />
                        75008 Paris - France<br />
                      </address>
                </div>
                <div class="invoice-col">
                    <strong>Invoiced To</strong>
                      <address class="small-text">
                        {{$invoice->name}}<br />
                      </address>
                </div>
            </div>
            <div class="row">
                <div class="invoice-col right"></div>
                <div class="invoice-col" style="float:right; text-align:right">
                    <strong>Invoice Date</strong><br>
                    <span class="small-text">
                      {{$invoice->invoice_date}}<br><br>
                    </span>
                </div>
            </div>

            <br />



            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Invoice Items</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <td><strong>Description</strong></td>
                                    <td width="20%" class="text-center"><strong>Amount</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <td>Invoice #{{$invoice->id}}</td>
                                        <td class="text-center">{!!  $invoice->discount == 0 ? $invoice->total : $invoice->total - $invoice->discount !!} {{$invoice->currency_name}}</td>
                                    </tr>

                                <tr>
                                    <td class="total-row text-right"><strong>Sub Total</strong></td>
                                    <td class="total-row text-center">{{$invoice->subtotal}} {{$invoice->currency_name}}</td>
                                </tr>
                                @if($invoice->discount != 0)
                                <tr>
                                    <td class="total-row text-right"><strong>Discount</strong></td>
                                    <td class="total-row text-center">{{$invoice->discount}} {{$invoice->currency_name}}</td>
                                </tr>
                                @endif

                                <tr>
                                    <td class="total-row text-right"><strong>Total</strong></td>
                                    <td class="total-row text-center">{!!  $invoice->discount == 0 ? $invoice->total : $invoice->total - $invoice->discount !!} {{$invoice->currency_name}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="pull-right btn-group btn-group-sm hidden-print">
                <a href="javascript:window.print()" class="btn btn-primary"><i class="fas fa-print"></i> Print</a>
            </div>


    </div>

    <p class="text-center hidden-print"><a href="/">&laquo; Back to Client Area</a></a></p>

</body>
<script src="{{asset('customer/js/jquery.min.js') }}"></script>
<script src="{{ asset('customer/js/popper.min.js') }}"></script>
<script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.blockui.js') }}"></script>
<script src="https://www.cdn.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript"></script>
<script type="text/javascript">
$token = $("input[name='_token']").val();
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $token
    }
});
var AdjeminPay = AdjeminPay();

AdjeminPay.on('init', function (e) {
    // retourne une erreur au cas où votre API_KEY ou APPLICATION_ID est incorrecte
    console.log(e);
});

// Lance une requete ajax pour vérifier votre API_KEY et APPLICATION_ID et initie le paiement
AdjeminPay.init({
    apikey: "eyJpdiI6Ik5ObTNmMTFtMFwvV2ZkU3RJS",
    application_id: "2f699e",
    notify_url: "https://goldenhealth.adjemincloud.com/api/invoice_payments_notify"
});

</script>
<script src="{{asset('invoices/scripts.js')}}"></script>
</html>
