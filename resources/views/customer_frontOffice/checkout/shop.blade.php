


        {{-- <div id="maincard" class="col-11 col-sm-9 col-md-9 col-lg-9 col-xl-9 text-center p-0 mt-3 mb-2"> --}}
        <div id="maincard" class="col-xl-5 col-lg-6 col-md-10 col-sm-12 col-12 mx-auto text-center">
            <div class="card px-0 pt-4 pb-0 mt-3 mb-3 mx-auto bg-white">
                <h2><strong class="fs-title">Commander Article</strong></h2>
                {{-- <p>Veuillez remplir les champs suivants.</p> --}}
                <div class="row">
                    <div class="col-md-12 mx-0">
                        <form id="msform" action="" method="post" enctype="multipart/form-data">
                            <!-- progressbar -->
                            <ul id="progressbar">
                                @auth
                                @else
                                    {{-- TODO auth --}}
                                    <li style="width: 16.66%;" class="active" id="customer"><strong class="d-none d-md-inline active">Client</strong></li>
                                @endauth
                                <li style="width: @auth 20% @else 16.66% @endauth;" class="@auth active @endauth;" id="services"><strong class="d-none d-md-inline">Produit</strong></li>
                                {{-- <li style="width: 25%;" id="invoice"><strong class="d-none d-md-inline">Résumé services</strong></li> --}}
                                <li style="width: @auth 20% @else 16.66% @endauth;" id="delivery"><strong class="d-none d-md-inline">Livraison</strong></li>

                                <li style="width: @auth 20% @else 16.66% @endauth;" id="payment"><strong class="d-none d-md-inline">Paiement</strong></li>

                                <li style="width: @auth 20% @else 16.66% @endauth;" id="order"><strong class="d-none d-md-inline">Résumé</strong></li>

                                <li style="width: @auth 20% @else 16.66% @endauth;" id="checkout"><strong class="d-none d-md-inline"></strong></li>
                            </ul> <!-- fieldsets -->
                            @auth
                            @else
                                <fieldset id="fieldset_customer">
                                    <div class="p-3 m-3">
                                        <h2 class="fs-title">Connectez-vous</h2>
                                        <div class="row mx-auto">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="phone_number" id="phoneNumber" placeholder="Numéro de telephone" />
                                                <small class="error" id="phonenumberErrorText"></small>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="password" class="form-control" name="password" id="password" placeholder="Mot de passe">
                                                <small class="error" id="passwordErrorText"></small>
                                            </div>
                                        </div>
                                        <div class="m-3">
                                            <div class="text-left">
                                                <p>Vous n'avez pas de compte ? <a href="{{route('register')}}" class="text-secondary">Inscrivez-vous</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <a name="next" class="action-button bg-danger cart-screen-modal-close">Annuler</a>

                                    <a id="customer_tab_next_button" name="next" class="action-button next">Suivant</a>
                                </fieldset>
                            @endauth
                            <fieldset id="fieldset_service">
                                <div class="p-3 m-3">
                                    <div class="service-container text-left">
                                        <div class="">
                                            <div class="row">
                                                <div class="float-left">
                                                    <div style="max-height: 150px; max-width: 150px; overflow:hidden; border-radius:50%;">
                                                        <img src="{{$product->cover_url}}" id="main-cover" alt="" style="width:100%;height:auto">
                                                    </div>

                                                </div>
                                                <div class="col-sm">
                                                    <h3 class="font-weight-bold text-primary">{{$product->title}}</h3>
                                                    <p>
                                                        {{$product->description}}
                                                    </p>
                                                    <p>
                                                        <img src="{{asset('img/icon/delivery.png')}}" class="icon" alt="">
                                                        Livré par : <span>{{ $service }}</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="btn btn-rounded btn-secondary font-weight-bold ml-auto">
                                                {{ (int) $product->getPrice()}} {{$product->currency->symbol}}
                                                </div>
                                            </div>
                                            <!--  -->
                                            <div class="row">
                                                <div class="ml-auto my-3">
                                                    <span class="mr-3">Quantité</span>
                                                    <div class="adp-qty-widget d-inline-block">
                                                        <a class="adp-qty-widget-less" id="decrementQtyBtn" onclick="">-</a>
                                                        <a class="adp-qty" id="orderQty">1</a>
                                                        <a class="adp-qty-widget-more" id="incrementQtyBtn" onclick="">+</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  -->
                                            <div class="row">
                                                <div class="ml-auto">
                                                    <span class="mr-3">Total</span>
                                                    <div class="d-inline-block">
                                                        <h5> <span id="orderAmount">10.000</span><span>Fcfa</span></h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @auth
                                    <a name="next" class="action-button bg-danger cart-screen-modal-close">Annuler</a>
                                @else
                                    <a class="previous action-button-previous previous">Précédent</a>
                                @endauth
                                <a id="services_tab_next_button" class="action-button next">Suivant</a>
                            </fieldset>
                            <fieldset id="fieldset_delivery">
                                {{-- Payment --}}

                                <div class="p-3 m-3">
                                    <h2 class="fs-title">Livraison</h2>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group my-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="my-addon">Adresse de livraison :</span>
                                                    </div>
                                                    <input type="text" id="locat_delivery" class="form-control" name="address_delivery_name" placeholder="Aucune addresse choisie" readonly>
                                                </div>
                                                <div class="col-12">
                                                    <div style="height:200px; width: 100%" id="map_delivery"></div>
                                                </div>
                                                <div class="mt-3">
                                                    <div class="input-group">
                                                        {{-- <input class="form-control" type="text" name="" placeholder="Recipient's text" aria-label="Recipient's " aria-describedby="my-addon"> --}}
                                                        <input type="text" id="addr_delivery" class="form-control" name="addr_delivery" placeholder="Chercher un lieu de livraison" data-toggle="modal_delivery" data-target="#Modal_delivery" onkeyup="addr_delivery_search();"/>
                                                        <div class="input-group-append">
                                                            {{-- <span class="input-group-text" id="my-addon">Text</span> --}}
                                                            <button type="button" onclick="addr_delivery_search();" class="btn btn-info">Rechercher</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- Adresses list --}}
                                                <div class="row">
                                                    <div class="">
                                                        <div id="results_delivery"></div>

                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <input type="hidden" id="lat_delivery" name="location_delivery_lat"/>
                                                    <input type="hidden" id="lon_delivery" name="location_delivery_lng"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a name="previous" class="previous action-button-previous" >Précédent</a>
                                <a type="button" name="next" id="delivery_tab_next_button" class="action-button">Suivant</a>
                            </fieldset>
                            <fieldset id="fieldset_payment">
                                <div class="p-3 m-3">
                                    <h2 class="fs-title">Paiement</h2>
                                    <div class="" id="ajemin-choose-paymethod-helper-text">
                                        Sélectionnez un moyen de paiement
                                    </div>
                                    <div class="row" id="adjemin-paymethod-row">
                                        <div id="adjemin-paymethod-mobilemoney" class="adjemin-paymethod adjemin-active" onclick="selectMethod('cash');">
                                            <div class="adjemin-check">
                                                <img src="{{asset('img/check.png')}}" class="adjemin-checked" alt="checked">
                                            </div>
                                            <div class="adjemin-paymethod-img">
                                                <img src="{{asset('img/mobile-money.png')}}" class="adjemin-img" alt="Mobile Money">
                                            </div>
                                            <p>Cash à la livraison</p>
                                        </div>
                                        <div id="adjemin-paymethod-card" class="adjemin-paymethod  adjemin-inactive" onclick="selectMethod('online');">
                                            <div class="adjemin-check">
                                                <img src="{{asset('img/check.png')}}" class=" adjemin-checked" alt="checked">
                                            </div>
                                            <div class="adjemin-paymethod-img">
                                                <img src="{{asset('img/card.png')}}" class="adjemin-img" alt="Carte Bancaire">
                                            </div>
                                            <p>Payer en ligne</p>
                                        </div>
                                    </div>
                                </div>
                                <a name="previous" class="previous action-button-previous" >Précédent</a>
                                <a type="button" name="next" id="order_tab_next_button" class="action-button next" onclick="">Suivant</a>
                            </fieldset>
                            <fieldset id="fieldset_order">
                                <div class="p-3 m-3">
                                    <h2 class="fs-title">Résumé commande</h2>
                                    <div class="table-responsive table-full-width">
                                        <table class="table table-hover table-striped table-adjemin">
                                            <tbody class="text-left">
                                                <tr>
                                                    <th>Article</th>
                                                    <td class="text-right" id="orderReviewItemTitle"></td>
                                                </tr>
                                                <tr>
                                                    <th>Quantité</th>
                                                    <td class="text-right" id="orderReviewQuantity"></td>
                                                </tr>
                                                <tr>
                                                    <th>Prix</th>
                                                    <td class="text-right" id="orderReviewPrice"></td>
                                                </tr>
                                                <tr>
                                                    <th>Montant</th>
                                                    <td class="text-right" id="orderReviewAmount"></td>
                                                </tr>
                                                <tr>
                                                    <th>Taxes</th>
                                                    <td class="text-right" id="orderReviewTaxes"></td>
                                                </tr>
                                                <tr>
                                                    <th>Frais de livraison</th>
                                                    <td class="text-right" id="orderReviewDeliveryFees"></td>
                                                </tr>
                                                <tr>
                                                    <th>Moyen de paiment</th>
                                                    <td class="text-right" id="orderReviewPaymentMethod"></td>
                                                </tr>
                                            </tbody>
                                            {{-- Services info from $form_data --}}
                                            <tfoot>
                                                <tr>
                                                    <th colspan="6" style="text-align: right;">
                                                        Total à payer <span id="orderReviewTotal">0</span> Frcs
                                                    </th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-left">
                                            <p>
                                                L'article sera livré à : <i id="orderReviewDeliveryAddress" style="text-decoration: underline"></i>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <a name="previous" class="previous action-button-previous" >Précédent</a>
                                <a type="button" name="next" id="orderFinalizeBtn" class="action-button next" onclick="">Terminer</a>
                                {{-- <a href="" style="display: none" id="gotoFailTabBtn"></a> --}}

                            </fieldset>
                            <fieldset id="loading">
                                <div class="p-3 m-3">
                                    <h2 class="fs-title text-center">Veuillez patienter </h2> <br><br>
                                    <div class="row justify-content-center">
                                        <div class="loader"></div>
                                    </div> <br><br>
                                    <div class="row justify-content-center">
                                        <div class="col-7 text-center">
                                            <h5>Commande en cours d'enregistrement...</h5>
                                        </div>
                                    </div>
                                </div>
                                <a name="previous" class="previous action-button-previous d-none" >Précédent</a>
                                <a type="button" name="next" id="gotoFailTabBtn" class="action-button next d-none" onclick="">Terminer</a>
                            </fieldset>
                            <fieldset id="fieldset_failed">
                                <div class="p-3 m-3">
                                    <h2 class="text-center text-danger">Echec </h2> <br><br>
                                    <div class="row justify-content-center">
                                        <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/cancel--v1.png">
                                        </div>
                                    </div> <br><br>
                                    <div class="row justify-content-center">
                                        <div class="col-7 text-center">
                                            <h5 >Votre commande n'a pas pu être enregistrée...</h5>
                                            <p id="responseMessage"></p>
                                        </div>
                                    </div>
                                </div>
                                <a href="" style="display: none" id="gotoSuccessTabBtn"></a>
                            </fieldset>
                            <fieldset id="fieldset_checkout">
                                <div class="p-3 m-3">
                                    <h2 class="fs-title text-center">Validé !</h2> <br><br>
                                    <div class="row justify-content-center">
                                        <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="fit-image"> </div>
                                    </div> <br><br>
                                    <div class="row justify-content-center">
                                        <div class="col-7 text-center">
                                            <h5>Commande enregistrée avec succès 🥳 </h5>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>

            </div>
        </div>
