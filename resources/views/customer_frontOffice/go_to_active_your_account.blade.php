@extends("customer_frontOffice.layouts_customer.master_customer")
@section('css')
<style>
    body {
        background-color: rgba(238, 238, 238, 0.336);
    }
    a.accordion:hover{
        color: currentColor !important;
    }
    .accordion {
        display: inline-block;
        background-color: #eee;
        color: #444;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        transition: 0.4s;
    }
    .accordion:first-child{
        border-radius: 5px 5px 0 0;
    }
    .accordion:last-child{
        border-radius: 0 0 5px 5px;
    }


    .active, .accordion:hover {
        background-color: #ccc;
    }

    .accordion:after {
        content: '\002B';
        color: #777;
        font-weight: bold;
        float: right;
        margin-left: 5px;
    }

    .active:after {
        content: "\2212";
    }

    .panel {
        padding: 0 18px;
        background-color: white;
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.2s ease-out;
    }
    .panel p{
        padding: 15px;
        font-size: 17px;
    }

</style>

@endsection
@section('content')

<section class="py-md-5 p-y2">
    <div class="container">
        <div class="text-center p-md-5 p-2" style="border-radius: 5px; background:lightgrey;">
            <h1 class="font-weight-bold text-center primary-text-color mb-3">@if(!Auth::guard('customer')->user()->is_coach) Veuillez activer votre compte @else En Attente de confirmation par un Administrateur @endif</h1>
        </div>
    </div>
    <div class="container mt-3 mb-5">
            <div style="color: green">
                @include('flash::message')
            </div>

            <div class="container mt-md-5 mt-3 p-4 text-center">
                <div></div>
                <p class="h5">
                    @if(Auth::guard('customer')->user()->is_coach)
                        Un Administrateur va bientôt confirmer votre compte.
                    @elseif(!Auth::guard('customer')->user()->is_coach)
                        Consultez votre adresse email puis activez votre compte pour accéder à votre tableau de bord.
                        <form action="{{ route('resendEmailSubcription') }}" method="POST">
                            @csrf
                            <input type="hidden" name="id" value="{{ Auth::guard('customer')->user()->id }}">
                            <button type="submit" class="btn btn-primary btn-sm" >Renvoyer un e-mail</button>.
                        </form>
                    @endif
                </p>
                
            </div>
    </div>
</section>

@endsection

@section('scripts')

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;

    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}
</script>

<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous">
</script>
<script>
  $(function() {
    $("#resend_button").on("click",function(e) {
      e.preventDefault(); // cancel the link itself
      $.post(this.href);
    });
  });
</script>
@endsection
