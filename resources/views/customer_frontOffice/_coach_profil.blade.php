@extends("customer_frontOffice.layouts_customer.master_customer")
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
    <style>
    body {
        font-size: 13px;
        background-color: rgba(247, 247, 247, 0.973);
    }

    a {
        color: black;
    }
    .datepicker {
    background-color:#edaa0d !important;
    color: #fff !important;
    border: none;
    padding: 10px !important
}

.datepicker-dropdown:after {
    border-bottom: 6px solid #edaa0d
}

thead tr:nth-child(3) th {
    color: #fff !important;
    font-weight: bold;
    padding-top: 20px;
    padding-bottom: 10px
}

.dow,
.old-day,
.day,
.new-day {
    width: 40px !important;
    height: 40px !important;
    border-radius: 0px !important
}

.old-day:hover,
.day:hover,
.new-day:hover,
.month:hover,
.year:hover,
.decade:hover,
.century:hover {
    border-radius: 6px !important;
    background-color: #eee;
    color: #edaa0d;
}

.active {
    border-radius: 6px !important;
    background-image: linear-gradient(#90CAF9, #64B5F6) !important;
    color: #edaa0d !important
}

.disabled {
    color: #616161 !important
}

.prev,
.next,
.datepicker-switch {
    border-radius: 0 !important;
    padding: 20px 10px !important;
    text-transform: uppercase;
    font-size: 20px;
    color: #fff !important;
    opacity: 0.8
}

.prev:hover,
.next:hover,
.datepicker-switch:hover {
    background-color: inherit !important;
    opacity: 1
}

.cell {
    border: 1px solid #BDBDBD;
    border-radius: 5px;
    margin: 2px;
    cursor: pointer
}

.cell:hover {
    border: 1px solid #1c6a49;
}

.cell.select {
    background-color: #edaa0d;
    color: #fff
}

.fa-calendar {
    color: #fff;
    font-size: 30px;
    padding-top: 8px;
    padding-left: 5px;
    cursor: pointer
}

#price_total {
    display: block;
    font-weight: bold;
    margin: 5px;
}

</style>
@endsection
@section('content')
<section>
    <div class="container py-3">
        <div class="coach-identity">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div>
                        <img src="{{$coach->customer->photo_url}}" class="img-fluid" alt="">
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 py-3">
                    <div class="">
                        <div class="">
                            <h4 class="font-weight-bold">{{$coach->customer->name}}</h4>
                        </div>
                        <div class="">
                            <p>Coach {{$course->discipline->name}}</p>
                        </div>
                        <!--div class="">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc erat augue, dapibus
                                sed felis at, imperdiet pretium .</p>
                        </div-->
                        {{-- {!! $coach->ratingView() !!} --}}
                        <div class="mb-4">
                            @php
                                $rating = (int) $coach->rating;
                            @endphp
                            @for ($i = 0; $i < 5; $i++)
                                @if($i+ 1 <= $rating)
                                    <img src="{{ asset('customer/images/stars.png')}}" class="icon" alt="">
                                @else
                                    <img src="{{ asset('customer/images/star.png')}}" class="icon" alt="">
                                @endif
                            @endfor
                        </div>
                        <div>
                            <a href="#">
                                <div class="tarifs-button">
                                    Tarif Indicatif <br>
                                    <span class="secondary-text-color font-weight-bold h5" id="price_span">{{$course->price}} XOF</span>
                                </div>
                                <span id="price_total" style="display:none">Total à régler : <span id="span_price_content"></span>  </span>
                                <input type="hidden" name="course_price" id="course_price" value="{{$course->price}}">
                                <input type="hidden" name="final_price" id="final_price">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 py-3">
                    <div class="col-md-1O">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Je souhaite que</label>
                            <select id="packs" name="packs" class="form-control packs">
                                @if($packs)
                                    @foreach($packs as $pack)
                                        @if ($pack->slug == "cours-unique")
                                            <option value="{{$pack->id}}" discount="{{$pack->percentage_discount}}">{{$pack->name}}</option>
                                        @else
                                            <option value="{{$pack->id}}" discount="{{$pack->percentage_discount}}">{{$pack->name}} (- {{($pack->percentage_discount)}} %)</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                          <label for="nb_personnes">Nombre de personnes</label>
                            <select id="nb_personnes" name="nb_personnes" class="form-control nb_personnes">
                                @if($course->type_course == "cour-particulier")
                                <option value="1">Cours individuel</option>
                                @else
                                <optgroup label="{{$course->course_type_object->name}}">
                                  @for ($i = 2; $i <= $course->detail->nb_person; $i++) {
                                    <option value="{{$i}}">{{$i}} (pers)</option>
                                  @endfor
                                </optgroup>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="mb-3">
                        <button type="button" id="go-booking" class="secondary-button btn-block text-center">Réserver</button>
                    </div>
                    {{--<div class="mb-4">
                        <button type="button" id="go-available" class="primary-button btn-block">
                            <img src="{{asset('customer/images/calendar.png')}}" class="mr-2" width="20" height="20" alt="">
                            Disponibilité
                        </button>
                    </div>--}}

                    <div class="ask-queestion mt-5">
                        <img src="{{asset('customer/images/conversation (1).png')}}" height="30" width="30" alt=""> Posez votre
                        question
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- Details --}}

<section>
    <div class="container py-3">
        <div class="coach-identity">
            <div class="row">
                <div>
                    <h4 class="primary-text-color font-weight-bold mb-4">Son expérience de coach</h4>

                    <p>
                       @if ($course && $course->detail ) {!! $course->detail->experience !!} @endif
                    </p>
                </div>
            </div>
            <div class="row mt-3">
                <div>
                    <h4 class="primary-text-color font-weight-bold mb-2">Sa séance type</h4>

                    <p>
                      @if ($course && $course->detail )  {!! $course->detail->seance_type !!} @endif
                    </p>
                </div>
            </div>

            <!-- Caractéristiques -->
            <div>
                <div class="row mt-3 mb-4">
                    <div>
                        <h4 class="primary-text-color font-weight-bold mb-2"> Caractéristiques</h4>
                    </div>
                </div>

                <div class="row">
                    @if($course)
                      <div class="col-lg-3 col-md-3 col-sm-12">
                          <div class="text-center">
                              <img src="{{ asset('customer/images/university.png') }}" width="50" height="50" alt="">
                          </div>
                          <div class="text-center font-weight-bold mt-3">Diplôme</div>
                          <div class="text-center mt-3">
                            @if($course->detail && $course->detail->is_diplome)
                              Diplomé
                            @elseif($course->detail && $course->detail->is_formation)
                              En Formation
                            @endif
                          </div>
                      </div>
                    @endif
                    @if($course)
                      <div class="col-lg-3 col-md-3 col-sm-12">
                          <div class="text-center">
                              <img src="{{ asset('customer/images/pin.png') }}" width="50" height="50" alt="">
                          </div>
                          <div class="text-center font-weight-bold mt-3">Le cours a lieu </div>
                          <div class="text-center mt-3">
                              @if(count($course->lieu) > 0)
                                @foreach($course->lieu as $key => $lieu)
                                  <p>{{$lieu->lieucourse->name}}</p>
                                @endforeach
                              @endif
                          </div>
                      </div>
                    @endif
                    @if($course)
                      <div class="col-lg-3 col-md-3 col-sm-12">
                          <div class="text-center">
                              <img src="{{ asset('customer/images/religion (1).png') }}" width="50" height="50" alt="">
                          </div>
                          <div class="text-center font-weight-bold mt-3">Objectif principal du cours</div>
                          <div class="text-center mt-3">
                            @if(count($course->courseObjective) > 0)
                              @foreach($course->courseObjective as $key => $courseObjective)
                                <p>{{$courseObjective->objective->name}}</p>
                              @endforeach
                            @endif
                          </div>
                      </div>
                    @endif
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="text-center">
                            <img src="{{ asset('customer/images/team.png') }}" width="50" height="50" alt="">
                        </div>
                        <div class="text-center font-weight-bold mt-3">Nombre de personnes</div>
                        <div class="text-center mt-3">
                            @if($course && $course->detail && $course->detail->has_particulier == 1)
                              <p>Cours particuliers</p>
                            @endif
                            @if($course && $course->detail && $course->detail->has_collectif == 1)
                              <p>Semi-collectif (max 4 personnes)</p>
                            @endif

                        </div>
                    </div>
                </div>
                <!--  Portfolio --
                <div class="row mt-5 mb-2">
                    <div>
                        <h4 class="primary-text-color font-weight-bold mb-2"> Portfolio</h4>
                    </div>
                </div>

                <div class="row">

                    <div class="col-lg-4 col-md-4 col-sm-12 mt-2">
                        <div>
                            <img src="{{ asset('customer/images/401-felix-6175.png') }}" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 mt-2">
                        <div>
                            <img src="{{ asset('customer/images/401-felix-6175.png')}}" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 mt-2">
                        <div>
                            <img src="{{ asset('customer/images/401-felix-6175.png')}}" class="img-fluid" alt="">
                        </div>
                    </div>

                </div> -->

                <!--  Avis clients -->
                <div class="row mt-5 mb-2">
                    <div>
                        <h4 class="primary-text-color font-weight-bold mb-2"> Avis clients</h4>
                    </div>
                </div>

                <!--div class="row mt-5 ">
                    <div class="col-lg-2 col-md-2 col-sm-12">
                        <img src="{{ asset('customer/images/401-ted34') }}67-jj-a_2.png" height="80" width="80" class="rounded-circle "
                            alt="">
                    </div>

                    <div class="col-lg-10 col-md-10 col-sm-12">
                        <p class="font-weight-bold">Sandrine Tanoh le 09/11/2019</p>
                        <div class="mb-3">
                            <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
                            <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
                            <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
                            <a href="#"><img src="{{ asset('customer/images/star.png') }}" class="icon" alt=""></a>
                            <a href="#"><img src="{{ asset('customer/images/star.png') }}" class="icon" alt=""></a>
                        </div>
                        <div>
                            <p>
                                Attentif - Merveilleux - Sympathique - Bon conseils - Séance adaptée - Efficace
                                -
                                Contact facile - Pedagogue - Dynamique - Patient
                            </p>
                        </div>
                    </div>
                </div>
                <hr-->
                <!--div class="row mt-5 ">
                    <div class="col-lg-2 col-md-2 col-sm-12">
                        <img src="{{ asset('customer/images/401-ted3467-jj-a_2.png') }}" height="80" width="80" class="rounded-circle "
                            alt="">
                    </div>

                    <div class="col-lg-10 col-md-10 col-sm-12">
                        <p class="font-weight-bold">Sandrine Tanoh le 09/11/2019</p>
                        <div class="mb-3">
                            <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
                            <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
                            <a href="#"><img src="{{ asset('customer/images/stars.png') }}" class="icon" alt=""></a>
                            <a href="#"><img src="{{ asset('customer/images/star.png') }}" class="icon" alt=""></a>
                            <a href="#"><img src="{{ asset('customer/images/star.png') }}" class="icon" alt=""></a>
                        </div>
                        <div>
                            <p>
                                Attentif - Merveilleux - Sympathique - Bon conseils - Séance adaptée - Efficace
                                -
                                Contact facile - Pedagogue - Dynamique - Patient
                            </p>
                        </div>
                    </div>
                </div-->
            </div>

        </div>
    </div>
</section>

<!-- Modal availableModal -->
<div class="modal fade" id="availableModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Réservation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    {{--<div class="form-group">
                        <input type="text" id="datepicker-booking-available" class="datepicker" placeholder="Selectionner une date" name="date" required readonly>
                        <span class="fa fa-calendar"></span>
                    </div>
                    <div class="mt-2" id="show-content-avalable" style="display:none">
                        <div class="row text-center mx-0">
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="9:00AM">9:00AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="9:30AM">9:30AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="9:45AM">9:45AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="10:00AM">10:00AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="10:30AM">10:30AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="10:45AM">10:45AM</div>
                            </div>
                        </div>
                        <div class="row text-center mx-0">
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="11:00AM">11:00AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="11:30AM">11:30AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="11:45AM">11:45AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="12:00PM">12:00PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="12:30PM">12:30PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="12:45PM">12:45PM</div>
                            </div>
                        </div>
                        <div class="row text-center mx-0">
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="1:00PM">1:00PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="1:30PM">1:30PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="1:45PM">1:45PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="2:00PM">2:00PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="2:30PM">2:30PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="2:45PM">2:45PM</div>
                            </div>
                        </div>
                        <div class="row text-center mx-0">
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="3:00PM">3:00PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="3:30PM">3:30PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="4:15PM">4:15PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1" time="5:00PM">5:00PM</div>
                            </div>
                        </div>
                    </div>--}}
                    <h1>Sélectionner une date</h1>
                    @foreach($availabilities as $key => $value)
                        <h2>{{$key}}</h2>
                        <div class="row text-center my-4">
                            @foreach($value as $el)
                                <div class="col-md-2 col-4 my-2 px-2">
                                    <div class="cell py-1" data-id="{{$el['id']}}">{{$el['start_time']}} - {{$el['end_time']}}</div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="DateModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Choisissez un créneau</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   <input type="hidden" name="datepicker-booking" value="">
                    @foreach($availabilities as $key => $value)
                        <h2>{{$key}}</h2>
                        <div class="row text-center my-4">
                            @foreach($value as $el)
                                <div class="col-md-2 col-4 my-2 px-2">
                                    <div class="cell py-1" data-id="{{$el['id']}}">{{$el['start_time']}} - {{$el['end_time']}}</div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                    {{--<div class="form-group">
                        <input type="text" id="datepicker-booking" class="datepicker" placeholder="Selectionner une date" name="date" required readonly>
                        <span class="fa fa-calendar"></span>
                    </div>
                    <input type="hidden" name="input-time" id="input-time">
                    <div class="mt-2" id="show-time-content" style="display:none">
                        <div class="row text-center mx-0">
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="9:00AM">9:00AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="9:30AM">9:30AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="9:45AM">9:45AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="10:00AM">10:00AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="10:30AM">10:30AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="10:45AM">10:45AM</div>
                            </div>
                        </div>
                        <div class="row text-center mx-0">
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="11:00AM">11:00AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="11:30AM">11:30AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="11:45AM">11:45AM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="12:00PM">12:00PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="12:30PM">12:30PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="12:45PM">12:45PM</div>
                            </div>
                        </div>
                        <div class="row text-center mx-0">
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="1:00PM">1:00PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="1:30PM">1:30PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="1:45PM">1:45PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="2:00PM">2:00PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="2:30PM">2:30PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="2:45PM">2:45PM</div>
                            </div>
                        </div>
                        <div class="row text-center mx-0">
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="3:00PM">3:00PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="3:30PM">3:30PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="4:15PM">4:15PM</div>
                            </div>
                            <div class="col-md-2 col-4 my-1 px-2">
                                <div class="cell py-1 timepicker" time="5:00PM">5:00PM</div>
                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
        <button type="button" id="go-date-booking" class="btn btn-secondary">Réserver</button>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
<script>
var date;

$(document).ready(function(){

    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        startDate: '0d'
    });

    $('.cell').click(function(){
        $('.cell').removeClass('select');
        $(this).addClass('select');
        // $('#datepicker-booking').val($(this).data('id'))
        date = $(this).data('id')
    });

});

    $('#go-available').on('click', function(){
        $('#availableModal').modal('toggle');
    });

    $('#go-booking').on('click', function(){
        $('#DateModal').modal('toggle');
        //alert('ok');
    });

    $('#datepicker-booking-available').change(function(){
        $('#show-content-avalable').hide();
        $('#show-content-avalable').show();
    });

    $('#datepicker-booking').change(function(){
        $('#show-time-content').hide();
        $('#show-time-content').show();
    });

    $('.timepicker').on('click', function(){
        var time = $(this).attr('time');
        var date = $('#datepicker-booking').val();
        $('#input-time').val(time);
        console.log(time);
        console.log(date);
    });

    $('#packs').on('change', function(e){
        var pack = $(this).val();
        var discount = $('option:selected', this).attr('discount');
        var price = $('#course_price').val();
        console.log(pack);
        console.log(discount);
        if (parseInt(discount) > 0) {
            var discount_price  = (parseInt(price) * discount) / 100;
            var final_price =  (parseInt(price) - discount_price);
            $('#price_span').text(final_price +' XOF');
            $('#final_price').val(final_price);
            if (pack == 2) {
                var $res_price = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'XOF' }).format(final_price * 5);
                $('#span_price_content').text($res_price);
            } else if(pack == 3) {
                var $res_price = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'XOF' }).format(final_price * 10);
                $('#span_price_content').text($res_price);
            } else if(pack == 4) {
                var $res_price = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'XOF' }).format(final_price * 30);
                $('#span_price_content').text($res_price);
            }
            $('#price_total').show();
        }else {
            $('#final_price').val(price);
            $('#price_span').text(price +' XOF');
            $('#price_total').hide();
        }
    });
    $('#go-date-booking').on('click', function(){
        // var time = $('#input-time').val();
        // var date = $('#datepicker-booking').val();
        console.log($('#datepicker-booking'))
        // alert(date)
        var pack = $("#packs").val();
        var nbperson = $("#nb_personnes").val();

        // if (time && date) {
        if (date != null || date != "" || date != undefined) {
            var courseID = "{{$course->id}}";
            window.location.href = "/course/reserver/"+btoa(courseID)+"?date="+btoa(date)+"&pack="+btoa(pack)+"&nbperson="+nbperson;
            // window.location.href = "/course/reserver/"+btoa(courseID)+"?date="+btoa(date)+"&time="+btoa(time)+"&pack="+btoa(pack)+"&nbperson="+nbperson;
        } else {
            Swal.fire({
                icon: 'info',
                title: 'Oops...',
                text: 'Veuillez obligatoirement choisir une date et une heure !' ,
            });
        }
    });
</script>
@endpush
