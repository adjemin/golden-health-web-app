@extends("customer_frontOffice.layouts_customer.master_customer")

@section('css')
<style>
.img-circle {
    max-width: 100px !important;
    width: 100px !important;
    height: auto;
    border-radius: 50% !important;
}
#order p{
    margin: 0 0 7.5px;
}
</style>
@endsection

@section('content')
<section>
    <header class="profil-header">
        <div class="dashboard-header-des">
            <h1 class="font-weight-bold text-center">Paiement </h1>
        </div>
    </header>
</section>
<section id="order" style="margin-bottom:50px;">
    <div class="container">
        <div class="row mt-4">
            {{-- <div class="col-12"> --}}
            <div class="col-md-4">
                <div class="p-3 mx-3 r bg-white">
                    {{-- <h2 class="fs-title mb-5">Coupon</h2> --}}
                    <p>
                        Vos reductions sur le prix
                    </p>
                    <div class="form-group">
                        @forelse($coupons as $coupon)
                            <div class="text-primary font-weight-bold text-uppercase my-2 text-right">
                                coupon
                                {{$coupon->code."  (-".$coupon->pourcentage."%) "}} Ajouté pour une remise de {{$booking['price'] * ($coupon->pourcentage/100)}}
                            </div>
                            @php
                                $booking['price'] -= $booking['price'] * ($coupon->pourcentage/100);
                            @endphp
                        @empty
                        @endforelse
                            {{--<input class="form-control" type="text" name="coupon_code" placeholder="Entrez votre code">

                            <span id="coupon_error" class="text-danger font-weight-bold"></span>
                            <a id="submitCoupon" class="btn btn-article btn-primary w-100 mt-3">Vérifier</a>--}}
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="paiement_block">
                    <div id="HOOK_TOP_PAYMENT">
                        <div class="col-12">
                            <div id="order-detail-content" class="table_block table-responsive">
                                <table id="cart_summary" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="cart_product first_item">Produit</th>
                                            <th class="cart_description item">Description</th>
                                            <th class="cart_unit item">Prix unitaire</th>
                                            <th class="cart_quantity item">Quantité</th>
                                            <th class="cart_total last_item">Total</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr class="cart_total_price">
                                            <td colspan="4" class="total_price_container text-right"><span>Total TTC</span></td>
                                            <td colspan="2" class="price" id="total_price_container"> <span id="total_price">{{$booking['totalprice']}} CFA</span></td>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <tr id="product_8868_0_0_28129" class="cart_item address_28129 odd">
                                            <td class="cart_product">
                                                <a href="#">
                                                    <img class="replace-2x img-responsive img-circle" src="https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png" alt="Coach {{$discipline->name}} | {{$coach->customer->first_name}}" width="48" height="48"></a>
                                            </td>
                                            <td class="cart_description">
                                                <p class="product-name">Coach {{$discipline->name}} | {{$coach->customer->first_name}}</p>
                                                <p class="product-event"> Cours : <?php echo \Carbon\Carbon::parse($booking["start"])->format('d/m/Y - H:i'); ?></p>
                                                @if($booking['nbperson'] == 1)
                                                    <p class="nb-personnnes"> <span>Cours Unique</span></p>
                                                @else
                                                    <p class="nb-personnnes"> <span>Cours </span> pour {{$booking['nbperson']}} personnes</p>
                                                @endif
                                            </td>
                                            <td class="cart_unit" data-title="Prix du cours">
                                                <span class="price" id="product_price_8868_0_28129"> <span class="price">{{$booking['price']}} CFA</span></span>
                                            </td>
                                            <td class="cart_quantity text-center" data-title="Nombre de cours">
                                                <span> {{$booking['nb_lesson']}} </span>
                                            </td>
                                            <td class="cart_total" data-title="Total des cours">
                                                <span class="price" id="total_product_price_8868_0_28129"> {{$booking['totalprice']}} CFA</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="HOOK_PAYMENT">
                        <!--div class="col-md-6 hidden-xs">
                            <h3>INFORMATIONS</h3>
                        </div-->
                        <input type="hidden" id="booking_id" name="booking_id" />
                        <div class="col-md-10">
                            <div class="payment_module">
                                @auth('customer')
                                    {{ csrf_field() }}
                                    <input type="hidden" name="payement_method_slug" id="payement_method_slug" value="cash">
                                    <button type="submit" class="btn btn-success pull-right btn-block" id="save_and_pay">CONFIRMER LA RESERVATION</button>

                                @endauth
                                @guest('customer')
                                    <a href="/customer/login?action=checkout" class="btn btn-success pull-right btn-block">CONFIRMER LA RESERVATION</a>
                                @endguest
                                <p> Votre séance pourra débuter sous réserve de la confirmation du coach.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Cart --}}
        </div>
    </div>
</section>
@endsection
@push('js')
<script src="https://www.cdn.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript">
</script>
<script>
    $token = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });
</script>
{{-- Coupon adding Script --}}
{{-- <script>
    $("#submitCoupon").click(function name(params) {
        var _cuteLoader = `<div class="cute-loader"><div></div><div></div><div></div><div></div></div>`;
        var _cuteLoaderDark = `<div class="cute-loader dark"><div></div><div></div><div></div><div></div></div>`;

        var coupon_code = $("input[name='coupon_code']").val();
        var invoice_id = $("input[name='invoice_id'").val();

        $(this).html(_cuteLoader);
        var $btn = $(this);
        var $messageDiv = $("#coupon_error");

        if(coupon_code == ''){
            $("#coupon_error").html("Vous devez entrez un coupon");
            $("#submitCoupon").html("Vérifier")
            return false;
        }
        if(invoice_id == ''){
            $("#coupon_error").html("Erreur survenue");
            $("#submitCoupon").html("Veuillez réessayer plus tard")
            return false;
        }


        $.ajax({
                type: "POST",
                url: "{{ route('ajax.coupon.add') }}",
                data: {
                    "invoice_id": invoice_id,
                    "coupon_code": coupon_code,
                },
                dataType: "json",
                success: function (response) {
                    //
                    console.log(">> success");
                    console.log(response);
                    if(response.status == null){
                        $btn.html("Veuillez réessayer plus tard");
                        $messageDiv.html("Une erreur est survenu");
                        return false;
                    }
                    // managing status codes
                    if(response.status != "OK"){
                        $btn.html("OUPS");
                        if(response.error != null){
                            if(response.error.message != null){
                                $messageDiv.html(response.error.message);
                                setTimeout(() => {
                                    $btn.html("Réessayer");
                                }, 1000);
                                return false;
                            }
                            $btn.html("Réessayer")
                        }
                    }
                    $btn.html("Coupon Ajouté");
                    window.location.reload();
                },
                error: function(response){
                    console.log(">> erreur");
                    $btn.html("Veuillez réessayer plus tard");
                    $messageDiv.html("Une erreur est survenue");
                    console.log(response);
                }
        });
    });
</script> --}}

<script>
    $token = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });
    var AdjeminPay = AdjeminPay();

    AdjeminPay.on('init', function (e) {
        // retourne une erreur au cas où votre API_KEY ou APPLICATION_ID est incorrecte
        console.log(e);
    });

    // Lance une requete ajax pour vérifier votre API_KEY et APPLICATION_ID et initie le paiement
    AdjeminPay.init({
        apikey: "eyJpdiI6Ik5ObTNmMTFtMFwvV2ZkU3RJS",
        application_id: "2f699e",
        notify_url: "https://goldenhealth.adjemincloud.com/api/invoice_payments_notify"
    });
</script>
<script>
        /*
      * Block / Unblock UI
    */
    function blockUI() {
          $.blockUI({
              message: "<img src='{{asset('assets/img/loading.gif')}}' alt='Loading ...' />",
              overlayCSS: {
                  backgroundColor: '#1B2024',
                  opacity: 0.55,
                  cursor: 'wait'
              },
              css: {
                  border: '4px #999 solid',
                  padding: 15,
                  backgroundColor: '#fff',
                  color: 'inherit'
              }
          });
      }

      function unblockUI() {
          $.unblockUI();
      }

      $(document).ready(function(){
        $('#save_and_pay').on('click', function(e){
            e.preventDefault();
            blockUI();
            var oData = new FormData();
            oData.append("_token", $('input[name=_token]').val());
            oData.append("payement_method_slug", $("#payement_method_slug").val());
            oData.append("booking_id", $("#booking_id").val());

            $.ajax({
               url:"{{ route('saveBooking') }}",
               method:"POST",
               data:oData,
               contentType:false,
               cache:false,
               processData:false,
               success:function(response){
                 console.log(response.status);
                  Swal.fire({
                    icon: 'success',
                    title: 'Enregistrement effectué avec succès!',
                    text: 'Veuillez patienter la confirmation du coach afin de démarrer votre séance.',
                    showConfirmButton: false
                  });
                  setTimeout(function() {
                    window.location.href = "/customer/disciplines"
                    //window.location.href = "/customer/go/payment"
                  }, 500);
                  unblockUI();
                  payWithAdjeminPay({
                      amount: response.data.amount,
                      transactionId: response.data.transaction_id,
                      currency:"CFA",
                      designation: response.data.transaction_designation,
                      customField: response.data.status,
                      booking_id:response.data.booking_id
                  });
               },
               error: function(error){
                   unblockUI();
                   console.log(error);
                   Swal.fire({
                       icon: 'warning',
                       title: 'Oops...',
                       text: 'Une erreur s\'est lors de l\'enregistrement veuillez réessayer' ,
                   });
               }
            });
        });


        function payWithAdjeminPay(transactionData){
            if(!transactionData){
                alert(">>>> erreur paiement, veuillez réessayer");
                console.log("<< clicked");
            }

            $('#booking_id').val(transactionData.booking_id);
            console.log(">>> clicked");

            // Ecoute le feedback sur les erreurs
            AdjeminPay.on('error', function (e) {
                // la fonction que vous définirez ici sera exécutée en cas d'erreur
                console.log(e);
                console.log(">>> clicked");

            });

            // Lancer la procédure de paiement au click
            AdjeminPay.preparePayment({
                amount: transactionData.amount,
                transaction_id: transactionData.transactionId,
                currency: transactionData.currency,
                designation: transactionData.designation,
                custom: transactionData.customField
            });

            // Si l'étape précédante n'a pas d'erreur,
            // cette ligne génère et affiche l'interface de paiement AdjeminPay
            AdjeminPay.renderPaymentView();


            // Payment terminé
            AdjeminPay.on('paymentTerminated', function (e) {
                console.log('<<<<<<< Terminated !');
                console.log(e);

                // adjeminPaymentNotify(transactionData.transactionId, e);
                adjeminPaymentNotify(transactionData.transactionId, e);

            });
        }
    });
</script>
{{-- AdjeminPay notification workaround --}}
<script>

    $token = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });
    //
    function adjeminPaymentNotify(reference, data){
        //
        if(reference == null){
            console.log("<<< REf null");
            return false;
        }
        if(data == null){
            console.log("<<< Data null");
            return false;
        }
        if(data.status == null){
            console.log("<<< Status null");
            return false;
        }
        var trans = {
            'id': reference,
            'status': data.status,
            // 'status': "SUCCESSFUL",
        }

        console.log(">>> transData");
        console.log(trans);

        $.ajax({
                type: "POST",
                url: "{{ route('api.payments.notify') }}",
                data: {
                    "transaction_id": trans.id,
                    "status": trans.status,
                },
                dataType: "json",
                success: function (response) {
                    console.log("Response succes <<<");
                    console.log(response);
                    if(response.error != null){
                        $('#save_and_pay').html(response.error.message);
                    }

                    if(response.status != null){
                        if(response.status == "OK"){
                            // Correctly notified
                            // now redirect according to status
                            console.log(">>>>> PAYMENT SUCCESS");
                            setTimeout(()=> {
                                window.location.reload();
                                location.assign("/customer/dashboard");
                                //
                            }, 2000);
                        }
                    }
                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                    $('#save_and_pay').html("Erreur");
                    setTimeout(() => {
                        $('#save_and_pay').html("Un petit problème est survenu");
                    }, 2000);
                },
            });
    }
</script>

@endpush
