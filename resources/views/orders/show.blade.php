{{-- @extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('orders.index') }}">Commande</a>
            </li>
            <li class="breadcrumb-item active">Détail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Détails commande</strong>
                                  <a href="{{ route('orders.index') }}" class="btn btn-light">Retour</a>
                             </div>
                             <div class="card-body">
                                 @include('orders.show_fields')
                             </div>
                         </div>
                     </div>
                     <div class="col-lg-12">
                        <div class="table-responsive-sm">
                            <table class="table table-striped" id="orderItems-table">
                                <thead>
                                    <tr>
                                        <th>Order</th>
                                        <th>Status</th>
                                        <th>Creator</th>
                                        <th>Creator</th>
                                        <th>Creator Name</th>
                                        <th colspan="3">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($orderItems as $orderItem)
                                    <tr>
                                        <td>{{ $orderItem->order_id }}</td>
                                        <td>{{ $orderItem->status }}</td>
                                        <td>{{ $orderItem->creator }}</td>
                                        <td>{{ $orderItem->creator_id }}</td>
                                        <td>{{ $orderItem->creator_name }}</td>
                                        <td>
                                            {!! Form::open(['route' => ['orderItems.destroy', $orderItem->id], 'method' => 'delete']) !!}
                                            <div class='btn-group'>
                                                <a href="{{ route('orderItems.show', [$orderItem->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                                                <a href="{{ route('orderItems.edit', [$orderItem->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                                                {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                            </div>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
 --}}



 @extends('admin_backoffice.layouts.master')

 @section('title')
     Commande | {{ config('app.name') }}
 @endsection

 @section('content')
     <div class="container-fluid">
         <div class="animated fadeIn">

             <div class="row">
                 <div class="col-md-8">
                     <div class="card">
                         <div class="card-header card-header-primary">
                             <h4 class="card-title">Détails commande</h4>
                             <p class="card-category">Information commande</p>
                         </div>
                         <div class="card-body">
                            <p>Cliquez sur "client" pour avoir plus de détails</p>
                             {{-- <div class="row"> --}}
                                 @include('orders.show_fields')
                                 {{--<a href="{{ route('orders.edit', [$order->id]) }}"
                                     class="btn btn-primary pull-right">Modifier</a>--}}

                                 {{-- </div> --}}
                                 <div class="col-lg-12">
                                    <div class="table-responsive-sm">
                                        <table class="table table-striped" id="orderItems-table">
                                            <thead>
                                                <tr>
                                                    <th>Photo</th>
                                                    <th>Produit</th>
                                                    <th>Quantité</th>
                                                    <th>Couleur</th>
                                                    <th>Taille</th>
                                                    <th>Montant</th>
                                                    {{--
                                                      <th>Service</th>
                                                      <th colspan="3">Action</th>
                                                    --}}

                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if($order && $order->orderItems)
                                              @foreach($order->orderItems as $orderItem)
                                                  @if($orderItem && $orderItem->product)
                                                  <tr>
                                                      <td>
                                                        @if($orderItem->product)
                                                            <img  class="rounded-circle" src="{{ $orderItem->product->getImageUrl() }}" alt="Image Produit" style="width:40px; height:40px;">
                                                        @endif
                                                     </td>
                                                      <td>{{ $orderItem->product->title ?? '' }}</td>
                                                      <td>{{ $orderItem->quantity ?? '' }}</td>
                                                      <td style="background-color: {{ $orderItem->color ?? $orderItem->color_choice  ?? ''}};"></td>
                                                      <td>{{ $orderItem->size ?? $orderItem->size_choice ?? ''}}</td>
                                                      <td>{{ $orderItem->total_amount ?? '' }} {{ $orderItem->currency_name ?? '' }}</td>
                                                      {{-- <td>{{ $orderItem->service_slug }}</td> --}}
                                                      {{--<td>
                                                          {!! Form::open(['route' => ['orderItems.destroy', $orderItem->id], 'method' => 'delete']) !!}
                                                          <div class='btn-group'>
                                                               <a href="{{ route('orderItems.show', [$orderItem->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                                                              <a href="{{ route('orderItems.edit', [$orderItem->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                                                              {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}

                                                          </div>
                                                          {!! Form::close() !!}
                                                      </td>--}}
                                                  </tr>
                                                  @endif
                                              @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                 </div>

                         </div>
                     </div>
                 </div>
                 <div class="col-md-4">
                     <div class="card card-profile">
                         <div class="card-avatar">
                             <a href="javascript:;">
                                 <img class="img" src="{{ $order->photo_url ?? '' }}" />
                             </a>
                         </div>
                         <div class="card-body">
                             <h6 class="card-category text-gray">{{ $order->invoice->status??"" }}</h6>
                             {{-- <p class="card-description">
                                 {{ $order->bio }}
                             </p> --}}
                             <a href="{{ route('orders.index') }}" class="btn btn-primary btn-round">Retour</a>

                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 @endsection
