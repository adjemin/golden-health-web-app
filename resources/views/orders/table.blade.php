<div class="table-responsive">
    <table class="table printable" id="orders-table">
        <thead class="text-primary">
            <tr>
                <th>Nom client</th>
                <th>Status</th>
                <th>Montant</th>
                <th>Devise</th>
                <th>Paiement</th>
                {{-- <th>Frais de livraison</th> 
                <th>En attente</th>--}}
                {{--<th>Date de livraison</th>--}}
                {{-- <th>Est délivrée</th> --}}
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $order)
                <tr>
                    <td>{{ $order->customer->name ?? '' }}</td>
                    <td>{{ $order->invoice->status ?? '' }}</td>
                    <td>{{ $order->amount }}</td>
                    <td>{{ $order->currency_code }}</td>
                    <td>{{ $order->payment_method_slug }}</td>
                    {{-- <td>{{ $order->delivery_fees }}</td> --}}
                    {{--<td>{!! $order->is_waiting == 1 ? '<a class="btn btn-warning"> En attente</a>' : '<a class="btn btn-success"> Valider</a>'!!}</td>--}}
                    {{--<td>{{ $order->delivery_date }}</td>--}}
                    {{-- <td>{{ $order->is_delivery }}</td> --}}
                    <td>
                        {!! Form::open(['route' => ['orders.destroy', $order->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            @if(isset($order->invoice))
                                <a class="btn btn-success" href="/viewinvoice/{{$order->invoice->id}}"><i class="fa fa-print"></i></a> 
                            @endif
                            <a href="{{ route('orders.show', [$order->id]) }}" class='btn btn-ghost-success'><i
                                    class="fa fa-eye"></i></a>
                            {{--<a href="{{ route('orders.edit', [$order->id]) }}" class='btn btn-ghost-info'><i
                                    class="fa fa-edit"></i></a>--}}
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn
                            btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
