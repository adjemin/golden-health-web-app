<div class="row">
    <!-- Customer Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('customer_id', 'Client:') !!}
        @if(!is_null($order->customer))
            <p><a href="{{ url('/customers/'.$order->customer->id) }}">{{ $order->customer->name }}</a></p>
        @endif
    </div>


    <!-- Customer Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('phone_number', 'Contact:') !!}
        <p>{{ $order->customer->phone??"" }}</p>
    </div>
</div>

<div class="row">
    <!-- Customer Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('location_name', 'Adresse de livraison:') !!}
        <p>{{ $order->location_name??"" }}</p>
    </div>

    <!-- Status Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('status', 'Status:') !!}
        <p>{{ $order->invoice->status ?? '' }}</p>
    </div>
</div>

<div class="row">
    <!-- Customer Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('location_name', 'Information complementaire sur adresse de livraison:') !!}
        <p>{{ $order->location_detail??"Aucune information complémentaire" }}</p>
    </div>


</div>

<div class="row">
    <!-- Amount Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('amount', 'Amount:') !!}
        <p>{{ $order->amount??"0" }}</p>
    </div>

    <!-- Currency Code Field -->
    <div class="form-group  col-sm-6">
        {!! Form::label('currency_code', 'Currency Code:') !!}
        <p>{{ $order->currency_code??"" }}</p>
    </div>
</div>

<div class="row">
    <!-- Payment Method Slug Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('payment_method_slug', 'Payment Method Slug:') !!}
        <p>{{ $order->payment_method_slug }}</p>
    </div>

    {{--<div class="form-group col-sm-6">
        {!! Form::label('customer_id', 'Client :') !!}
        <p>{{ $order->customer->name }}</p>
    </div>--}}
</div>
{{-- @if($order->payment_method_slug == 'cash' && $order->status == 'waiting')
<div>
    {!! Form::open(['route' => ['orders.isPaid', $order->id], 'method' => 'post']) !!}
        {!! Form::button('Confirmer la commande', ['type' => 'submit', 'class' => 'btn btn-success', 'onclick' => "return confirm('Êtes vous sûr de confirmer la commande ?')"]) !!}
    {!! Form::close() !!}
</div>
@endif --}}
<div class="row">
    
    @if($order->status == 'waiting' && isset($order->invoice->payment_method) && $order->invoice->payment_method == "cash")
    <div class="form-group col-sm-6">
        {!! Form::open(['route' => ['orders.espece', $order->id], 'method' => 'post']) !!}
            {!! Form::button('Marquer payer en espece', ['type' => 'submit', 'class' => 'btn btn-info', 'onclick' => "return confirm('Êtes vous sûr de confirmer la commande ?')"]) !!}
        {!! Form::close() !!}
    </div>
    @endif

    @if($order->status == 'waiting')
    <div class="form-group col-sm-6">
        {!! Form::open(['route' => ['orders.changeStatus', $order->id], 'method' => 'post']) !!}
            {!! Form::hidden('status', 'confirmed') !!}
            {!! Form::button('Confirmer la commande', ['type' => 'submit', 'class' => 'btn btn-success', 'onclick' => "return confirm('Êtes vous sûr de confirmer la commande ?')"]) !!}
        {!! Form::close() !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::open(['route' => ['orders.changeStatus', $order->id], 'method' => 'post']) !!}
            {!! Form::hidden('status', 'canceled') !!}
            {!! Form::button('Annuler la commande', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Êtes vous sûr de confirmer la commande ?')"]) !!}
        {!! Form::close() !!}
    </div>
    @endif
    @if($order->status == 'confirmed')
    <div class="form-group col-sm-6">
        {!! Form::open(['route' => ['orders.changeStatus', $order->id], 'method' => 'post']) !!}
            {!! Form::hidden('status', 'delivered') !!}
            {!! Form::button('Marquer livré', ['type' => 'submit', 'class' => 'btn btn-success', 'onclick' => "return confirm('Êtes vous sûr de livrer ?')"]) !!}
        {!! Form::close() !!}
    </div>
    @endif
    {{--<!-- Is Waiting Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('is_waiting', 'Is Waiting:') !!}
        <p>{!! $order->is_waiting == 0 ? '<a class="btn btn-warning"> En attente</a>' : '<a class="btn btn-success"> Valider</a>'!!}</p>
    </div>--}}
</div>
{{--<!-- Delivery Fees Field -->
<div class="form-group">
    {!! Form::label('delivery_fees', 'Delivery Fees:') !!}
    <p>{{ $order->delivery_fees }}</p>
</div>


<!-- Delivery Date Field -->
<div class="form-group">
    {!! Form::label('delivery_date', 'Delivery Date:') !!}
    <p>{{ $order->delivery_date }}</p>
</div>
--}}
{{--
<!-- Is Delivery Field -->
<div class="form-group">
    {!! Form::label('is_delivery', 'Is Delivery:') !!}
    <p>{{ $order->is_delivery }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $order->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $order->updated_at }}</p>
</div>
--}}