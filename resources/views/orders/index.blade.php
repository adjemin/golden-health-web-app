{{-- @extends('layouts.app')

@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Orders</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        @include('flash::message')
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                        Orders
                        <a class="pull-right" href="{{ route('orders.create') }}"><i
                                class="fa fa-plus-square fa-lg"></i></a>
                    </div>
                    <div class="card-body">
                        @include('orders.table')
                        <div class="pull-right mr-3">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

--}}


@extends('admin_backoffice.layouts.master')

@section('title')
    Commandes | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Commandes</h4>
                            <p class="card-category"> Toutes les commandes</p>
                            {{--<div class="col text-right">
                                <a class="pull-right btn btn-sm btn-light" href="{{ route('orders.create') }}">Ajouter une
                                    commande</a>
                            </div>--}}
                        </div>
                        <div class="card-body" id="dynamic">
                            @include('orders.table')
                        </div>
                        <div class="mt-3 d-flex justify-content-center" id="link">
                            {{ $orders->links() }}
                        </div>
                        <div class="float-right">
                            <a href="/order/download/all" class="btn btn-success mr-1" >Exporter tableau complet</a>
                            <button id="print" class="btn btn-info">Exporter tableau actuel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        $('body').on('keyup', '#searchOrder', function() {
            // alert('ok')
            var searchOrder = $(this).val();
            $.ajax({
                method: 'POST',
                url: "{{ route('searchOrder') }}",
                dataType: "json",
                data: {
                    '_token': '{{ csrf_token() }}',
                    searchOrder: searchOrder
                },

                success: function(res) {
                    console.log(res);
                    $("#dynamic").html(res);
                    $("#link").html('');
                }
            })
        });

    </script>

@endsection