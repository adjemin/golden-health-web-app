<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::select('customer_id', $customerItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Amount:') !!}
    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    {!! Form::text('currency_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Method Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_method_slug', 'Payment Method Slug:') !!}
    {!! Form::text('payment_method_slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Fees Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_fees', 'Delivery Fees:') !!}
    {!! Form::text('delivery_fees', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_date', 'Delivery Date:') !!}
    {!! Form::text('delivery_date', null, ['class' => 'form-control','id'=>'delivery_date']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#delivery_date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Is Delivery Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_delivery', 'Is Delivery:') !!}
    {!! Form::text('is_delivery', null, ['class' => 'form-control','id'=>'is_delivery']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#is_delivery').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('orders.index') }}" class="btn btn-secondary">Cancel</a>
</div>
