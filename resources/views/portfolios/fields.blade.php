<!-- Coach Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coach_id', 'Coach Id:') !!}
    {!! Form::select('coach_id', $coachItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Images Url Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('images_url', 'Images Url:') !!}
    {!! Form::textarea('images_url', null, ['class' => 'form-control']) !!}
</div>

<!-- Video Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('video_url', 'Video Url:') !!}
    {!! Form::text('video_url', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('portfolios.index') }}" class="btn btn-secondary">Cancel</a>
</div>
