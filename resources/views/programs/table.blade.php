<div class="table-responsive-sm">
    <table class="table table-striped" id="programs-table">
        <thead>
            <tr>
                <th>Title</th>
                <th>Cover</th>
                <!-- <th>Description</th> -->
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($programs as $program)
            <tr>
                <td>{{ $program->title }}</td>
                <td><img width="150px;" src="{!! !is_null($program->cover) ? $program->cover : '' !!}" alt="{{ $program->title }}"></td>
                {{--<td>{{ $program->description }}</td>--}}
                <td>
                    {!! Form::open(['route' => ['programs.destroy', $program->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('programs.show', [$program->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('programs.edit', [$program->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>