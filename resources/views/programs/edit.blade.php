{{-- @extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('programs.index') !!}">Program</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Program</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($program, ['route' => ['programs.update', $program->id], 'method' => 'patch', 'files' => true]) !!}

                              @include('programs.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection --}}



@extends('admin_backoffice.layouts.master')

@section('title')
    Programme | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Modifier un programme</h4>
                            <p class="card-category">Information programme</p>
                        </div>
                        <div class="card-body">
                            {!! Form::model($program, ['route' => ['programs.update', $program->id], 'method' => 'patch']) !!}

                              @include('programs.fields')

                              {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
