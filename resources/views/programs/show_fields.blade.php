{{-- <!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $program->title }}</p>
</div> --}}


<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            <p>{{ $program->title }}</p>
        </div>
    </div>
</div>


{{-- <!-- Cover Field -->
<div class="form-group">
    {!! Form::label('cover', 'Cover:') !!}
    <p>{{ $program->cover }}</p>
</div> --}}

<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('cover', 'Cover:') !!}
            <img class="img-fluid zoomable" src="{{ $program->cover }}" height="90" />
        </div>
    </div>
</div>

{{-- <!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $program->description }}</p>
</div> --}}

<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            <p>{!! $program->description !!}</p>
        </div>
    </div>
</div>

{{-- <!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $program->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $program->updated_at }}</p>
</div> --}}