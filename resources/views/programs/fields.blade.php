<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            {!! Form::text('title', null, ['class' => 'form-control', 'onkeyup' => 'stringToSlug(this.value)']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control', 'readonly']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('cover', 'Image:') !!}
            {!! Form::file('cover') !!}
        </div>
    </div>

    @if ($program ?? '')
        <div class="col-md-6">
            <div class="form-group">
                <img src="{{ $program->cover }}" style="width: 50%; height: 50%; border: none" />
            </div>
        </div>
</div>
@else
<div class="col-md-6">
    <div class="form-group">
        <img style="width: 50%; height: 50%; border: none" id="output" />
    </div>
</div>
</div>
@endif

</div>

<div class="row mt-3">
    <div class="col-md-12">

        <div class="form-group">
            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
        </div>
    </div>
</div>

{{--
<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Titre:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div> --}}

{{--
<!-- Cover Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover', 'Image:') !!}
    {!! Form::file('cover') !!}
</div>
<div class="clearfix"></div> --}}

<!-- Description Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div> --}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('programs.index') }}" class="btn btn-secondary">Annuler</a>
</div>


<script>

    function stringToSlug(str) {

        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to   = "aaaaeeeeiiiioooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        // return str;
        document.getElementById("slug").value = str;
    }
</script>
