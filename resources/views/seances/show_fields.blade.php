<!-- Discipline Id Field -->
<div class="form-group">
    {!! Form::label('discipline_id', 'Discipline Id:') !!}
    <p>{{ $seance->discipline_id }}</p>
</div>

<!-- Coach Id Field -->
<div class="form-group">
    {!! Form::label('coach_id', 'Coach Id:') !!}
    <p>{{ $seance->coach_id }}</p>
</div>

<!-- Duration Field -->
<div class="form-group">
    {!! Form::label('duration', 'Duration:') !!}
    <p>{{ $seance->duration }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $seance->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $seance->updated_at }}</p>
</div>

