<div class="table-responsive-sm">
    <table class="table table-striped" id="seances-table">
        <thead>
            <tr>
                <th>Discipline Id</th>
        <th>Coach Id</th>
        <th>Duration</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($seances as $seance)
            <tr>
                <td>{{ $seance->discipline_id }}</td>
            <td>{{ $seance->coach_id }}</td>
            <td>{{ $seance->duration }}</td>
                <td>
                    {!! Form::open(['route' => ['seances.destroy', $seance->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('seances.show', [$seance->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('seances.edit', [$seance->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>