{{--
<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div> --}}

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            {!! Form::text('title', null, ['class' => 'form-control', 'onkeyup' => 'stringToSlug(this.value)']) !!}
        </div>
    </div>
    <div class="col-md-6">

        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control', 'readonly']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {{-- {!! Form::label('description', 'Description:') !!} --}}
            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            <strong>{!! Form::label('cover', 'Image:') !!}</strong>
            {!! Form::file('cover', ['class' => 'form-control', 'onchange' => 'loadFile(event)']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <img style="width: 50%; height: 50%; border: none" id="output" />
        </div>
    </div>
</div>

<script>
    var loadFile = function(event) {
        var reader = new FileReader();
        reader.onload = function() {
            var output = document.getElementById('output');
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };

</script>

{{--
<!-- Cover Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover', 'Cover:') !!}
    {!! Form::file('cover') !!}
</div> --}}
<div class="clearfix"></div>

{{-- <!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
   
</div>

<!-- Author Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('author_id', 'Author Id:') !!}
    {!! Form::text('author_id', null, ['class' => 'form-control']) !!}
</div> --}}

<div class="row mt-3">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('category_id', 'Catégorie:') !!}
            {{-- {!! Form::text('category_id', null, ['class' => 'form-control']) !!}
            --}}
            {!! Form::select('category_id', $categories->pluck('name', 'id'), null, ['class' => 'form-control',
            'required']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('status', 'Status:') !!}
            {!! Form::select('status', ['published' => 'PUBLIER', 'draft' => 'BROUILLON'], ['class' => 'form-control'])
            !!}
        </div>
    </div>
    <div class="col-md-4" style="display: none">

        <div class="form-group">
            {!! Form::label('author_id', 'Auteur:') !!}
            {!! Form::text('author_id', Auth::user()->id, ['class' => 'form-control']) !!}
            {{-- <input type="text" class="form-control" name="author_id" id="author_id"
                value="{{ Auth::user()->name }}" readonly /> --}}
        </div>
    </div>
</div>

{{--
<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Catégorie:') !!}
    {!! Form::text('category_id', null, ['class' => 'form-control']) !!}
    {!! Form::select('category_id', $categories->pluck('name', 'id'), null, ['class' => 'form-control', 'required']) !!}

</div> --}}

{{-- <!-- Shared Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shared_link', 'Shared Link:') !!}
    {!! Form::text('shared_link', null, ['class' => 'form-control']) !!}
</div> --}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('posts.index') }}" class="btn btn-secondary">Annuler</a>
</div>

<script>

    function stringToSlug(str) {

        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to   = "aaaaeeeeiiiioooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        // return str;
        document.getElementById("slug").value = str;
    }
</script>