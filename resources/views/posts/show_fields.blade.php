{{--
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $post->title }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $post->description }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{{ $post->slug }}</p>
</div> --}}

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            <p>{{ $post->title }}</p>
        </div>
    </div>
    <div class="col-md-6">

        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            <p>{{ $post->slug }}</p>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            <p>{!! $post->description !!}</p>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('cover', 'Image:') !!}
            {{-- <p>{{ $post->cover }}</p> --}}
            <img class="img-fluid zoomable" src="{{ $post->cover }}" height="90"/>
        </div>
    </div>
</div>


{{--
<!-- Cover Field -->
<div class="form-group">
    {!! Form::label('cover', 'Cover:') !!}
    <p>{{ $post->cover }}</p>
</div> --}}

{{--
<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $post->status }}</p>
</div>

<!-- Is Published Field -->
<div class="form-group">
    {!! Form::label('is_published', 'Is Published:') !!}
    <p>{{ $post->is_published }}</p>
</div>

<!-- Author Id Field -->
<div class="form-group">
    {!! Form::label('author_id', 'Author Id:') !!}
    <p>{{ $post->author_id }}</p>
</div> --}}

<div class="row mt-3">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('category_id', 'Catégorie:') !!}
            <p>{{ $post->getCategoryAttribute()->name }}</p>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('status', 'Status:') !!}
            <p>{{ $post->status == 'published' ? 'PUBLIE' : 'BROUILLON' }}</p>
        </div>
    </div>
    <div class="col-md-4">

        <div class="form-group">
            {!! Form::label('author_id', 'Auteur:') !!}
            <p>{{ $post->getAuthorAttribute()->name }}</p>
        </div>
    </div>
</div>

{{--
<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Category Id:') !!}
    <p>{{ $post->category_id }}</p>
</div> --}}

{{--
<!-- Shared Link Field -->
<div class="form-group">
    {!! Form::label('shared_link', 'Shared Link:') !!}
    <p>{{ $post->shared_link }}</p>
</div> --}}

<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('shared_link', 'Lien à partager:') !!}
            <p>{{ $post->shared_link }}</p>
        </div>
    </div>
</div>

{{--
<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $post->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $post->updated_at }}</p>
</div> --}}


<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('created_at', 'Created At:') !!}
            <p>{{ $post->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('updated_at', 'Updated At:') !!}
            <p>{{ $post->updated_at }}</p>
        </div>
    </div>
</div>
