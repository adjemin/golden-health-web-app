<div class="table-responsive">
    <table class="table" id="posts-table">
        <thead class="text-primary">
            <tr>
                <th>Titre</th>
                <th>Slug</th>
                <!-- <th>Description</th> -->
                {{-- <th>Cover</th> --}}
                <th>Status</th>
                {{-- <th>Is Published</th> --}}
                {{-- <th>Author Id</th> --}}
                <th>Catégorie</th>
                {{-- <th>Shared Link</th> --}}
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($posts as $post)
                <tr>
                    <td>{{ $post->title }}</td>
                    <td>{{ $post->slug }}</td>
                    {{--<td>{!! $post->description !!}</td>--}}
                    {{-- <td>{{ $post->cover }}</td> --}}
                    <td>{{ $post->status == 'published' ? 'PUBLIE' : 'BROUILLON' }}</td>
                    {{-- <td>{{ $post->is_published }}</td>
                    <td>{{ $post->author_id }}</td> --}}
                    <td>{{ $post->getCategoryAttribute()->name }}</td>
                    {{-- <td>{{ $post->shared_link }}</td> --}}
                    <td>
                        {!! Form::open(['route' => ['posts.destroy', $post->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('posts.show', [$post->id]) }}" class='btn btn-ghost-success'><i
                                    class="fa fa-eye"></i></a>
                            <a href="{{ route('posts.edit', [$post->id]) }}" class='btn btn-ghost-info'><i
                                    class="fa fa-edit"></i></a>
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn
                            btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
