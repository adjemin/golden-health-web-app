{{--@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('videotheques.index') }}">Videotheque</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('videotheques.index') }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('videotheques.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection--}}

@extends('admin_backoffice.layouts.master')

@section('title')
    Vidéotheque | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Détails vidéo</h4>
                            <p class="card-category">Information vidéo</p>
                        </div>
                        <div class="card-body">
                            @include('videotheques.show_fields')
                            <a href="{{ route('videotheques.edit', [$videotheque->id]) }}"
                                class="btn btn-primary pull-right">Modifier</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
