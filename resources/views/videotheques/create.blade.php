{{--
@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('videotheques.index') !!}">Videotheque</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="ropw">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Videotheque</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'videotheques.store', 'files' => true]) !!}

                                   @include('videotheques.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection 
--}}


@extends('admin_backoffice.layouts.master')

 @section('title')
    Vidéotheque | {{ config('app.name') }}
 @endsection
 
 @section('content')
     <div class="container-fluid">
         <div class="animated fadeIn">
             <div class="row">
                 <div class="col-md-12">
                     <div class="card">
                         <div class="card-header card-header-primary">
                             <h4 class="card-title">Ajouter une video</h4>
                             <p class="card-category">Informations video</p>
                         </div>
                         <div class="card-body">
                             {!! Form::open(['route' => 'videotheques.store', 'files' => true]) !!}
 
                             @include('videotheques.fields')
 
                             {!! Form::close() !!} 
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 @endsection