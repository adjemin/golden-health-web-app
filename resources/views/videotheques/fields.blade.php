<div class="row">
    <!-- Titre Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('titre', 'Titre:') !!}
        {!! Form::text('titre', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('link_youtube', 'Lien youtube:') !!}
        {!! Form::text('link_youtube', null, ['class' => 'form-control']) !!}
        <small>Exemple: pour le lien suivant https://www.youtube.com/watch?v=mmq5zZfmIws vous devrez enregistrer l'id de la video qui est <strong>"mmq5zZfmIws"</strong></small>
    </div>
</div>

<div class="row">
    <!-- Video Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('video', 'Video:') !!}
        {!! Form::file('video') !!}
    </div>
</div>


<div class="form-group">
    <video id="video_output" controls autoplay muted src="{{ $videotheque->video ?? $videotheque->link_youtube ?? '' }}"></video>
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {{--{!! Form::label('description', 'Description:') !!}--}}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
</div>

<script>
    // image_types = ['mp4','mov','flv','3gp','mov','avi','wmv','webm']
    
    // var loadFile = function(event) {
    //     if (event.files && event.files[0]) {
    //         var extension = event.files[0].name.split('.').pop().toLowerCase(),
    //             isImage = image_types.indexOf(extension) > -1;
    //         console.log(extension)
            
    //         var reader = new FileReader(),
    //             file = event.target.files[0];

    //         if(isImage){   
    //             console.log('is video');
    //             reader.onload = function() {
    //                 var output = document.getElementById('output');
    //                 output.src = reader.result;
    //             };
    //             reader.readAsDataURL(file);
    //         }
            
    //         console.log('not a valide file')
    //     }
        
    // };

</script>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('videotheques.index') }}" class="btn btn-secondary">Annuler</a>
</div>
