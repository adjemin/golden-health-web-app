<!-- Titre Field -->

<div class="form-group">
    {!! Form::label('titre', 'Titre:') !!}
    <p>{{ $videotheque->titre }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $videotheque->description !!}</p>
</div>

@if(!is_null($videotheque->link_youtube))
    <!-- Link Youtube	String Field -->
    <div class="embed-responsive embed-responsive-21by9 embed-responsive-16by9 embed-responsive-4by3 embed-responsive-1by1">
        {!! Form::label('link_youtube', 'Lien Youtube:') !!}
        <iframe class="embed-responsive-item" src="{{ $videotheque->link_youtube }}"></iframe>
    </div>
@endif

@if(!is_null($videotheque->video))
    <!-- Video Field -->
    <div class="embed-responsive embed-responsive-21by9 embed-responsive-16by9 embed-responsive-4by3 embed-responsive-1by1">
        {!! Form::label('video', 'Video:') !!}
        <iframe class="embed-responsive-item" src="{{ $videotheque->video }}"></iframe>
    </div>
@endif

{{--
<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $videotheque->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $videotheque->updated_at }}</p>
</div>

--}}