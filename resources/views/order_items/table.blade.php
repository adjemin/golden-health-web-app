<div class="table-responsive-sm">
    <table class="table table-striped" id="orderItems-table">
        <thead>
            <tr>
                <th>Order Id</th>
        <th>Status</th>
        <th>Creator</th>
        <th>Creator Id</th>
        <th>Creator Name</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($orderItems as $orderItem)
            <tr>
                <td>{{ $orderItem->order_id }}</td>
            <td>{{ $orderItem->status }}</td>
            <td>{{ $orderItem->creator }}</td>
            <td>{{ $orderItem->creator_id }}</td>
            <td>{{ $orderItem->creator_name }}</td>
                <td>
                    {!! Form::open(['route' => ['orderItems.destroy', $orderItem->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('orderItems.show', [$orderItem->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('orderItems.edit', [$orderItem->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>