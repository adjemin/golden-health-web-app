<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{{ $orderItem->order_id }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $orderItem->status }}</p>
</div>

<!-- Creator Field -->
<div class="form-group">
    {!! Form::label('creator', 'Creator:') !!}
    <p>{{ $orderItem->creator }}</p>
</div>

<!-- Creator Id Field -->
<div class="form-group">
    {!! Form::label('creator_id', 'Creator Id:') !!}
    <p>{{ $orderItem->creator_id }}</p>
</div>

<!-- Creator Name Field -->
<div class="form-group">
    {!! Form::label('creator_name', 'Creator Name:') !!}
    <p>{{ $orderItem->creator_name }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $orderItem->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $orderItem->updated_at }}</p>
</div>

