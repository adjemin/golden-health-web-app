<div class="table-responsive-sm">
    <table class="table table-striped" id="diplomes-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Image Url</th>
        <th>Customer Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($diplomes as $diplome)
            <tr>
                <td>{{ $diplome->name }}</td>
            <td>{{ $diplome->image_url }}</td>
            <td>{{ $diplome->customer_id }}</td>
                <td>
                    {!! Form::open(['route' => ['diplomes.destroy', $diplome->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('diplomes.show', [$diplome->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('diplomes.edit', [$diplome->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>