<div class="table-responsive-sm">
    <table class="table table-striped" id="lieuCourses-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Slug</th>
        <th>Description</th>
        <th>Active</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($lieuCourses as $lieuCourse)
            <tr>
                <td>{{ $lieuCourse->name }}</td>
            <td>{{ $lieuCourse->slug }}</td>
            <td>{{ $lieuCourse->description }}</td>
            <td>{{ $lieuCourse->active }}</td>
                <td>
                    {!! Form::open(['route' => ['lieuCourses.destroy', $lieuCourse->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('lieuCourses.show', [$lieuCourse->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('lieuCourses.edit', [$lieuCourse->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>