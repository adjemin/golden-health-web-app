<div class="table-responsive-sm">
    <table class="table table-striped" id="qualifications-table">
        <thead>
            <tr>
                <th>Period</th>
        <th>Qualifi Id</th>
        <th>Description</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($qualifications as $qualification)
            <tr>
                <td>{{ $qualification->period }}</td>
            <td>{{ $qualification->qualifi_id }}</td>
            <td>{{ $qualification->description }}</td>
                <td>
                    {!! Form::open(['route' => ['qualifications.destroy', $qualification->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('qualifications.show', [$qualification->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('qualifications.edit', [$qualification->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>