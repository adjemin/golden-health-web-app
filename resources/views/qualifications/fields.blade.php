<!-- Period Field -->
<div class="form-group col-sm-6">
    {!! Form::label('period', 'Period:') !!}
    {!! Form::text('period', null, ['class' => 'form-control']) !!}
</div>

<!-- Qualifi Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('qualifi_id', 'Qualifi Id:') !!}
    {!! Form::select('qualifi_id', $qualifiction_typeItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('qualifications.index') }}" class="btn btn-secondary">Cancel</a>
</div>
