<!-- Period Field -->
<div class="form-group">
    {!! Form::label('period', 'Period:') !!}
    <p>{{ $qualification->period }}</p>
</div>

<!-- Qualifi Id Field -->
<div class="form-group">
    {!! Form::label('qualifi_id', 'Qualifi Id:') !!}
    <p>{{ $qualification->qualifi_id }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $qualification->description }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $qualification->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $qualification->updated_at }}</p>
</div>

