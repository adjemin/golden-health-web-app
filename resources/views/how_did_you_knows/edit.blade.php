@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('howDidYouKnows.index') !!}">How Did You Know</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit How Did You Know</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($howDidYouKnow, ['route' => ['howDidYouKnows.update', $howDidYouKnow->id], 'method' => 'patch']) !!}

                              @include('how_did_you_knows.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection