<div class="table-responsive-sm">
    <table class="table table-striped" id="howDidYouKnows-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Name En</th>
        <th>User Source</th>
        <th>User Id</th>
        <th>Description</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($howDidYouKnows as $howDidYouKnow)
            <tr>
                <td>{{ $howDidYouKnow->name }}</td>
            <td>{{ $howDidYouKnow->name_en }}</td>
            <td>{{ $howDidYouKnow->user_source }}</td>
            <td>{{ $howDidYouKnow->user_id }}</td>
            <td>{{ $howDidYouKnow->description }}</td>
                <td>
                    {!! Form::open(['route' => ['howDidYouKnows.destroy', $howDidYouKnow->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('howDidYouKnows.show', [$howDidYouKnow->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('howDidYouKnows.edit', [$howDidYouKnow->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>