@extends('admin_backoffice.layouts.master')

@section('title')
    Analytique | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
               
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Analytique</h4>
                        </div>
                        <div class="card-body">
                            <div class="chart chart-sm">
                                <canvas id="chartjs-dashboard-line"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    {{-- Chart-js dashboard line --}}
    <script>
        jQuery(document).ready(function($){
            $.ajax({
                type: 'get',
                url: {{ url('')}},
                success: function (data) {
                    console.log(data)
                },
                error: function (data) {
                    console.log(data);
                }
            }); 
        });

    </script>
@endsection
