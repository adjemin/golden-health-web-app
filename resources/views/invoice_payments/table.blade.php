<div class="table-responsive-sm">
    <table class="table table-striped" id="invoicePayments-table">
        <thead>
            <tr>
                <th>Invoice Id</th>
        <th>Payment Method</th>
        <th>Payment Reference</th>
        <th>Amount</th>
        <th>Currency Code</th>
        <th>Creator Id</th>
        <th>Creator Name</th>
        <th>Creator Name</th>
        <th>Creator</th>
        <th>Status</th>
        <th>Status</th>
        <th>Is Waiting</th>
        <th>Is Completed</th>
        <th>Payment Gateway Trans Id</th>
        <th>Payment Gateway Custom</th>
        <th>Payment Gateway Currency</th>
        <th>Payment Gateway Amount</th>
        <th>Payment Gateway Payid</th>
        <th>Payment Gateway Payment Date</th>
        <th>Payment Gateway Payment Time</th>
        <th>Payment Gateway Payment Message</th>
        <th>Payment Gateway Payment Method</th>
        <th>Payment Gateway Payment Phone Prefixe</th>
        <th>Payment Gateway Payment Cel Phone Num</th>
        <th>Payment Gateway Ipn Ack</th>
        <th>Payment Gateway Created At</th>
        <th>Payment Gateway Updated At</th>
        <th>Payment Gateway Cpm Result</th>
        <th>Payment Gateway Trans Status</th>
        <th>Payment Gateway Designation</th>
        <th>Payment Gateway Buyer Name</th>
        <th>Payment Gateway Signature</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($invoicePayments as $invoicePayment)
            <tr>
                <td>{{ $invoicePayment->invoice_id }}</td>
            <td>{{ $invoicePayment->payment_method }}</td>
            <td>{{ $invoicePayment->payment_reference }}</td>
            <td>{{ $invoicePayment->amount }}</td>
            <td>{{ $invoicePayment->currency_code }}</td>
            <td>{{ $invoicePayment->creator_id }}</td>
            <td>{{ $invoicePayment->creator_name }}</td>
            <td>{{ $invoicePayment->creator_name }}</td>
            <td>{{ $invoicePayment->creator }}</td>
            <td>{{ $invoicePayment->status }}</td>
            <td>{{ $invoicePayment->status }}</td>
            <td>{{ $invoicePayment->is_waiting }}</td>
            <td>{{ $invoicePayment->is_completed }}</td>
            <td>{{ $invoicePayment->payment_gateway_trans_id }}</td>
            <td>{{ $invoicePayment->payment_gateway_custom }}</td>
            <td>{{ $invoicePayment->payment_gateway_currency }}</td>
            <td>{{ $invoicePayment->payment_gateway_amount }}</td>
            <td>{{ $invoicePayment->payment_gateway_payid }}</td>
            <td>{{ $invoicePayment->payment_gateway_payment_date }}</td>
            <td>{{ $invoicePayment->payment_gateway_payment_time }}</td>
            <td>{{ $invoicePayment->payment_gateway_payment_message }}</td>
            <td>{{ $invoicePayment->payment_gateway_payment_method }}</td>
            <td>{{ $invoicePayment->payment_gateway_payment_phone_prefixe }}</td>
            <td>{{ $invoicePayment->payment_gateway_payment_cel_phone_num }}</td>
            <td>{{ $invoicePayment->payment_gateway_ipn_ack }}</td>
            <td>{{ $invoicePayment->payment_gateway_created_at }}</td>
            <td>{{ $invoicePayment->payment_gateway_updated_at }}</td>
            <td>{{ $invoicePayment->payment_gateway_cpm_result }}</td>
            <td>{{ $invoicePayment->payment_gateway_trans_status }}</td>
            <td>{{ $invoicePayment->payment_gateway_designation }}</td>
            <td>{{ $invoicePayment->payment_gateway_buyer_name }}</td>
            <td>{{ $invoicePayment->payment_gateway_signature }}</td>
                <td>
                    {!! Form::open(['route' => ['invoicePayments.destroy', $invoicePayment->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('invoicePayments.show', [$invoicePayment->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('invoicePayments.edit', [$invoicePayment->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>