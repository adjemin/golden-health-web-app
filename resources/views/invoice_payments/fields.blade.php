<!-- Invoice Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('invoice_id', 'Invoice Id:') !!}
    {!! Form::select('invoice_id', $invoiceItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Method Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    {!! Form::text('payment_method', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Reference Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_reference', 'Payment Reference:') !!}
    {!! Form::text('payment_reference', null, ['class' => 'form-control']) !!}
</div>

<!-- Creator Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('creator_id', 'Creator Id:') !!}
    {!! Form::select('creator_id', $customerItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Creator Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('creator_name', 'Creator Name:') !!}
    {!! Form::select('creator_name', $customerItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Creator Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('creator_name', 'Creator Name:') !!}
    {!! Form::select('creator_name', $customerItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Creator Field -->
<div class="form-group col-sm-6">
    {!! Form::label('creator', 'Creator:') !!}
    {!! Form::text('creator', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Trans Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_trans_id', 'Payment Gateway Trans Id:') !!}
    {!! Form::text('payment_gateway_trans_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Custom Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_custom', 'Payment Gateway Custom:') !!}
    {!! Form::text('payment_gateway_custom', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Currency Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_currency', 'Payment Gateway Currency:') !!}
    {!! Form::text('payment_gateway_currency', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_amount', 'Payment Gateway Amount:') !!}
    {!! Form::text('payment_gateway_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Payid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_payid', 'Payment Gateway Payid:') !!}
    {!! Form::text('payment_gateway_payid', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Payment Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_payment_date', 'Payment Gateway Payment Date:') !!}
    {!! Form::text('payment_gateway_payment_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Payment Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_payment_time', 'Payment Gateway Payment Time:') !!}
    {!! Form::text('payment_gateway_payment_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Payment Message Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_payment_message', 'Payment Gateway Payment Message:') !!}
    {!! Form::text('payment_gateway_payment_message', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Payment Method Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_payment_method', 'Payment Gateway Payment Method:') !!}
    {!! Form::text('payment_gateway_payment_method', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Payment Phone Prefixe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_payment_phone_prefixe', 'Payment Gateway Payment Phone Prefixe:') !!}
    {!! Form::text('payment_gateway_payment_phone_prefixe', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Payment Cel Phone Num Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_payment_cel_phone_num', 'Payment Gateway Payment Cel Phone Num:') !!}
    {!! Form::text('payment_gateway_payment_cel_phone_num', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Ipn Ack Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_ipn_ack', 'Payment Gateway Ipn Ack:') !!}
    {!! Form::text('payment_gateway_ipn_ack', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_created_at', 'Payment Gateway Created At:') !!}
    {!! Form::text('payment_gateway_created_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_updated_at', 'Payment Gateway Updated At:') !!}
    {!! Form::text('payment_gateway_updated_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Cpm Result Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_cpm_result', 'Payment Gateway Cpm Result:') !!}
    {!! Form::text('payment_gateway_cpm_result', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Trans Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_trans_status', 'Payment Gateway Trans Status:') !!}
    {!! Form::text('payment_gateway_trans_status', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Designation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_designation', 'Payment Gateway Designation:') !!}
    {!! Form::text('payment_gateway_designation', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Buyer Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_buyer_name', 'Payment Gateway Buyer Name:') !!}
    {!! Form::text('payment_gateway_buyer_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Gateway Signature Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_gateway_signature', 'Payment Gateway Signature:') !!}
    {!! Form::text('payment_gateway_signature', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('invoicePayments.index') }}" class="btn btn-secondary">Cancel</a>
</div>
