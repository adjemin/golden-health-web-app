<!-- Invoice Id Field -->
<div class="form-group">
    {!! Form::label('invoice_id', 'Invoice Id:') !!}
    <p>{{ $invoicePayment->invoice_id }}</p>
</div>

<!-- Payment Method Field -->
<div class="form-group">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    <p>{{ $invoicePayment->payment_method }}</p>
</div>

<!-- Payment Reference Field -->
<div class="form-group">
    {!! Form::label('payment_reference', 'Payment Reference:') !!}
    <p>{{ $invoicePayment->payment_reference }}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{{ $invoicePayment->amount }}</p>
</div>

<!-- Currency Code Field -->
<div class="form-group">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    <p>{{ $invoicePayment->currency_code }}</p>
</div>

<!-- Creator Id Field -->
<div class="form-group">
    {!! Form::label('creator_id', 'Creator Id:') !!}
    <p>{{ $invoicePayment->creator_id }}</p>
</div>

<!-- Creator Name Field -->
<div class="form-group">
    {!! Form::label('creator_name', 'Creator Name:') !!}
    <p>{{ $invoicePayment->creator_name }}</p>
</div>

<!-- Creator Name Field -->
<div class="form-group">
    {!! Form::label('creator_name', 'Creator Name:') !!}
    <p>{{ $invoicePayment->creator_name }}</p>
</div>

<!-- Creator Field -->
<div class="form-group">
    {!! Form::label('creator', 'Creator:') !!}
    <p>{{ $invoicePayment->creator }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $invoicePayment->status }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $invoicePayment->status }}</p>
</div>

<!-- Is Waiting Field -->
<div class="form-group">
    {!! Form::label('is_waiting', 'Is Waiting:') !!}
    <p>{{ $invoicePayment->is_waiting }}</p>
</div>

<!-- Is Completed Field -->
<div class="form-group">
    {!! Form::label('is_completed', 'Is Completed:') !!}
    <p>{{ $invoicePayment->is_completed }}</p>
</div>

<!-- Payment Gateway Trans Id Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_trans_id', 'Payment Gateway Trans Id:') !!}
    <p>{{ $invoicePayment->payment_gateway_trans_id }}</p>
</div>

<!-- Payment Gateway Custom Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_custom', 'Payment Gateway Custom:') !!}
    <p>{{ $invoicePayment->payment_gateway_custom }}</p>
</div>

<!-- Payment Gateway Currency Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_currency', 'Payment Gateway Currency:') !!}
    <p>{{ $invoicePayment->payment_gateway_currency }}</p>
</div>

<!-- Payment Gateway Amount Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_amount', 'Payment Gateway Amount:') !!}
    <p>{{ $invoicePayment->payment_gateway_amount }}</p>
</div>

<!-- Payment Gateway Payid Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_payid', 'Payment Gateway Payid:') !!}
    <p>{{ $invoicePayment->payment_gateway_payid }}</p>
</div>

<!-- Payment Gateway Payment Date Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_payment_date', 'Payment Gateway Payment Date:') !!}
    <p>{{ $invoicePayment->payment_gateway_payment_date }}</p>
</div>

<!-- Payment Gateway Payment Time Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_payment_time', 'Payment Gateway Payment Time:') !!}
    <p>{{ $invoicePayment->payment_gateway_payment_time }}</p>
</div>

<!-- Payment Gateway Payment Message Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_payment_message', 'Payment Gateway Payment Message:') !!}
    <p>{{ $invoicePayment->payment_gateway_payment_message }}</p>
</div>

<!-- Payment Gateway Payment Method Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_payment_method', 'Payment Gateway Payment Method:') !!}
    <p>{{ $invoicePayment->payment_gateway_payment_method }}</p>
</div>

<!-- Payment Gateway Payment Phone Prefixe Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_payment_phone_prefixe', 'Payment Gateway Payment Phone Prefixe:') !!}
    <p>{{ $invoicePayment->payment_gateway_payment_phone_prefixe }}</p>
</div>

<!-- Payment Gateway Payment Cel Phone Num Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_payment_cel_phone_num', 'Payment Gateway Payment Cel Phone Num:') !!}
    <p>{{ $invoicePayment->payment_gateway_payment_cel_phone_num }}</p>
</div>

<!-- Payment Gateway Ipn Ack Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_ipn_ack', 'Payment Gateway Ipn Ack:') !!}
    <p>{{ $invoicePayment->payment_gateway_ipn_ack }}</p>
</div>

<!-- Payment Gateway Created At Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_created_at', 'Payment Gateway Created At:') !!}
    <p>{{ $invoicePayment->payment_gateway_created_at }}</p>
</div>

<!-- Payment Gateway Updated At Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_updated_at', 'Payment Gateway Updated At:') !!}
    <p>{{ $invoicePayment->payment_gateway_updated_at }}</p>
</div>

<!-- Payment Gateway Cpm Result Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_cpm_result', 'Payment Gateway Cpm Result:') !!}
    <p>{{ $invoicePayment->payment_gateway_cpm_result }}</p>
</div>

<!-- Payment Gateway Trans Status Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_trans_status', 'Payment Gateway Trans Status:') !!}
    <p>{{ $invoicePayment->payment_gateway_trans_status }}</p>
</div>

<!-- Payment Gateway Designation Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_designation', 'Payment Gateway Designation:') !!}
    <p>{{ $invoicePayment->payment_gateway_designation }}</p>
</div>

<!-- Payment Gateway Buyer Name Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_buyer_name', 'Payment Gateway Buyer Name:') !!}
    <p>{{ $invoicePayment->payment_gateway_buyer_name }}</p>
</div>

<!-- Payment Gateway Signature Field -->
<div class="form-group">
    {!! Form::label('payment_gateway_signature', 'Payment Gateway Signature:') !!}
    <p>{{ $invoicePayment->payment_gateway_signature }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $invoicePayment->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $invoicePayment->updated_at }}</p>
</div>
