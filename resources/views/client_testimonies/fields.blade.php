<div class="row">
    <!-- Client Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('client_name', 'Nom Client:') !!}
        {!! Form::text('client_name', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Client Photo Url Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('client_photo_url', 'Photo Client:') !!}
        {!! Form::file('client_photo_url') !!}
    </div>
</div>

<!-- Client Rating Field -->
<div class="form-group col-sm-6">
    {!! Form::label('client_rating', 'Note:') !!}
    {!! Form::number('client_rating', null, ['class' => 'form-control', 'min' => 1, 'max' => 5]) !!}
</div>

<!-- Client Feedback Field -->
<div class="form-group col-sm-6">
    {{--{!! Form::label('client_feedback', 'Temoignage:') !!}--}}
    {!! Form::text('client_feedback', null, ['class' => 'form-control', 'id' => 'description']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('clientTestimonies.index') }}" class="btn btn-secondary">Annuler</a>
</div>
