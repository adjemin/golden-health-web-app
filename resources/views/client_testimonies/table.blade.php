<div class="table-responsive-sm">
    <table class="table table-striped" id="clientTestimonies-table">
        <thead>
            <tr>
                <th>Photo</th>
                <th>Nom client</th>
                <!-- <th>Temoignage</th> -->
                <th>Note</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($clientTestimonies as $clientTestimony)
            <tr>
                <td><img height="80" src="{{ $clientTestimony->client_photo_url ?? 'https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png' }}" alt=""></td>
                <td>{{ $clientTestimony->client_name }}</td>
                {{--<td>{{ $clientTestimony->client_feedback }}</td>--}}
                <td>{{ $clientTestimony->client_rating }}</td>
                <td>
                    {!! Form::open(['route' => ['clientTestimonies.destroy', $clientTestimony->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('clientTestimonies.show', [$clientTestimony->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('clientTestimonies.edit', [$clientTestimony->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>