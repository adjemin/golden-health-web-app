<div class="row">
    <!-- Client Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('client_name', 'Client:') !!}
        <p>{{ $clientTestimony->client_name }}</p>
    </div>

    <!-- Client Rating Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('client_rating', 'Note:') !!}
        <p>{{ $clientTestimony->client_rating }}</p>
    </div>
</div>
<!-- Client Feedback Field -->
<div class="form-group">
    {!! Form::label('client_feedback', 'Message:') !!}
    <p>{!! $clientTestimony->client_feedback !!}</p>
</div>

{{--

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $clientTestimony->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $clientTestimony->updated_at }}</p>
</div>

--}}