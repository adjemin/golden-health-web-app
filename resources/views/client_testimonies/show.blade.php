@extends('admin_backoffice.layouts.master')

@section('title')
    Temoignage client | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Détails Temoignage client</h4>
                            <p class="card-category">Information temoignage</p>
                        </div>
                        <div class="card-body">
                            @include('client_testimonies.show_fields')
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <a href="javascript:;">
                                <img class="img-fluid" src="{{ $clientTestimony->client_photo_url ?? 'https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png' }}" alt="{{ $clientTestimony->client_name }}" />
                            </a>
                        </div>
                        <div class="card-body">
                            <a href="{{ route('clientTestimonies.index') }}" class="btn btn-primary btn-round">Retour</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
