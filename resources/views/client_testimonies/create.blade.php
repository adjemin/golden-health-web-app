@extends('admin_backoffice.layouts.master')

@section('title')
Temoignages | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Temoignages</h4>
                            <p class="card-category"> Ajouter un temoignage</p> 
                        </div>
                        <div class="card-body">
                            {!! Form::open(['route' => 'clientTestimonies.store', 'files' => true]) !!}

                                @include('client_testimonies.fields')

                            {!! Form::close() !!}
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection