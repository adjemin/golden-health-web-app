{{--
@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('coupons.index') !!}">Coupon</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Coupon</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($coupon, ['route' => ['coupons.update', $coupon->id], 'method' => 'patch', 'files' => true]) !!}

                              @include('coupons.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection
--}}

@extends('admin_backoffice.layouts.master')

@section('title')
    Coupon | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Modifier un coupon</h4>
                            <p class="card-category">Information coupon</p>
                        </div>
                        <div class="card-body">
                            {!! Form::model($coupon, ['route' => ['coupons.update', $coupon->id], 'method' => 'patch', 'files' => true]) !!}

                              @include('coupons.fields')

                              {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection