<div class="row">
    <!-- Titre Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('titre', 'Titre:') !!}
        <p>{{ $coupon->titre }}</p>
    </div>

    <!-- Code Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('code', 'Code:') !!}
        <p>{{ $coupon->code }}</p>
    </div>
</div>
<div class="row">
    <!-- Porcentage Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('pourcentage', 'Pourcentage:') !!}
        <p>{{ $coupon->pourcentage }}</p>
    </div>

    <!-- Nbr Person Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('nbr_person', 'Nbr Person:') !!}
        <p>{{ $coupon->nbr_person }}</p>
    </div>
</div>

<div class="row">
    <!-- Date Deb Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('date_deb', 'Date Debut:') !!}
        <p>{{ $coupon->date_deb }}</p>
    </div>
    
    <!-- Date Fin Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('date_fin', 'Date Fin:') !!}
        <p>{{ $coupon->date_fin }}</p>
    </div>

    <!-- Image Field -->
    {{--<div class="form-group col-sm-6">
        {!! Form::label('image', 'Image:') !!}
        <p>{{ $coupon->image }}</p>
    </div>--}}
</div>
<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('is_expired', 'Etat du coupon:') !!}
        {!! $coupon->is_expired() == false ? '<a class="btn btn-success">Actif</a>' : '<a class="btn btn-danger">Expiré</a>' !!}
    </div>
</div>
{{--<div class="row">
    <div class="form-group col-sm-12">
        {!! Form::label('descritpion', 'Descritpion:') !!}
        <p>{{ $coupon->descritpion }}</p>
    </div>
</div>
--}}
{{--
<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $coupon->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $coupon->updated_at }}</p>
</div>
--}}