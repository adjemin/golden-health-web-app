<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>LISTE DES COUPONS</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }

        body {
            margin: 0px;
        }

        * {
            font-family: Verdana, Arial, sans-serif;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .invoice table {
            margin: 15px;
            padding-left: 40px;
            padding-right: 40px;
        }

        .invoice h3 {
            margin-left: 15px;
            padding-left: 40px;
            padding-right: 40px;
        }

        .invoice h4 {
            margin-left: 15px;
            padding-left: 40px;
            padding-right: 40px;
        }

        .information {
            padding: 40px;
        }

        .information .logo {
            margin: 5px;
        }

        .information table {
            padding: 10px;
        }
    </style>
</head>
<body onload="window.print()">
    @php
        $varDate = now();
        $countCoupons = $coupons->count();
    @endphp
    <div class="information">
        <table width="100%">
            <tr>
                <td align="left" style="width: 40%;">
                    <img src="{{asset('customer/images/gh1.png')}}" alt="Logo" width="100" class="logo" />
                </td>
                
                <td align="right" style="width: 40%;">

                    <pre>
                    Abidjan, le {{ date('d/m/Y', strtotime($varDate)) }}
                </pre>
                </td>
            </tr>

        </table>
    </div>


    <br />

    <div class="invoice">
        <table width="100%">
            <thead>
                <tr align="left">
                    <th>Titre</th>
                    <th>Code</th>
                    <th>Pourcentage</th>
                    <th>Nombre Personne</th>
                    <th>Date Debut</th>
                    <th>Date Fin</th>
                </tr>
            </thead>
            <tbody>
                @foreach($coupons as $coupon)
                    <tr>
                        <td>{{ $coupon->titre }}</td>
                        <td>{{ $coupon->code }}</td>
                        <td>{{ $coupon->pourcentage }}</td>
                        <td>{{ $coupon->nbr_person }}</td>
                        <td>{{ $coupon->date_deb }}</td>
                        <td>{{ $coupon->date_fin }}</td>
                    </tr>
                @endforeach
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="3"></td>
                    <td align="right">Total : </td>
                    <td align="left" class="gray">{{ $countCoupons }}</td>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="information" style="position: absolute; bottom: 0;">
        <table width="100%">
            <tr>
                <td align="left" style="width: 50%;">
                    &copy; {{ date('Y') }} {{-- {{ config('app.url') }} --}} GOLDENHEALTH - Tous droits réservés.
                </td>
                <td align="right" style="width: 50%;">
                    Powered by <a href="{{ env('ADJEMIN_URL') }}" style="color: black">Adjemin</a>
                </td>
            </tr>

        </table>
    </div>
</body>

</html>