<div class="table-responsive-sm">
    <table class="table table-striped printTable" id="coupons-table">
        <thead>
            <tr>
                <th>Titre</th>
                <th>Code</th>
                <th>Pourcentage</th>
                <th>Nombre Personne</th>
                <th>Date Debut</th>
                <th>Date Fin</th>
                {{--<th>Image</th>--}}
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($coupons as $coupon)
            <tr>
                <td>{{ $coupon->titre }}</td>
                <td>{{ $coupon->code }}</td>
                <td>{{ $coupon->pourcentage }}</td>
                <td>{{ $coupon->nbr_person }}</td>
                <td>{{ $coupon->date_deb }}</td>
                <td>{{ $coupon->date_fin }}</td>
                {{--<td><img src="{{ $coupon->image }}" alt="" width="80"></td>--}}
                <td>
                    {!! Form::open(['route' => ['coupons.destroy', $coupon->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('coupons.show', [$coupon->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('coupons.edit', [$coupon->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>