{{--
@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('coupons.index') }}">Coupon</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('coupons.index') }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('coupons.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
--}}

@extends('admin_backoffice.layouts.master')

@section('title')
    Cours | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Détails coupon</h4>
                            <p class="card-category">Information coupon</p>
                        </div>
                        <div class="card-body">
                            @include('coupons.show_fields')
                        </div>
                    </div>
                </div>
                @if(!is_null($coupon->image))
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <a href="javascript:;">
                                <img class="img" src="{{$coupon->image}}"/>
                            </a>
                        </div>
                        <div class="card-body">
                            <a href="{{ route('coupons.index') }}" class="btn btn-primary btn-round">Retour</a>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection