<div class="row">
    <!-- Titre Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('titre', 'Titre:') !!}
        {!! Form::text('titre', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Code Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('code', 'Code:') !!}
        {!! Form::text('code', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">

    <!-- Porcentage Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('pourcentage', 'Pourcentage:') !!}
        {!! Form::number('pourcentage', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Nbr Person Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('nbr_person', 'Nombre Personne:') !!}
        {!! Form::number('nbr_person', null, ['class' => 'form-control', 'min'  => 0]) !!}
    </div>
</div>

<div class="row">
    <!-- Date Fin Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('date_deb', 'Date Debut:') !!}
        {!! Form::date('date_deb', null, ['class' => 'form-control','id'=>'date_deb']) !!}
    </div>

    <!-- Date Fin Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('date_fin', 'Date Fin:') !!}
        {!! Form::date('date_fin', null, ['class' => 'form-control','id'=>'date_fin']) !!}
    </div>
</div>

@push('scripts')
   <script type="text/javascript">
        $('#date_fin').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            icons: {
                up: "icon-arrow-up-circle icons font-2xl",
                down: "icon-arrow-down-circle icons font-2xl"
            },
            sideBySide: true
        })

        $('#date_deb').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            icons: {
                up: "icon-arrow-up-circle icons font-2xl",
                down: "icon-arrow-down-circle icons font-2xl"
            },
            sideBySide: true
        })
    </script>
@endpush


<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image') !!}
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('coupons.index') }}" class="btn btn-secondary">Cancel</a>
</div>
