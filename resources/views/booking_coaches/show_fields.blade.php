<div class="row">
    <!-- Course Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('course_id', 'Cours:') !!}
        @if(!is_null($bookingCoach->course))
            <p> <a href="{{ url('/courses/'.$bookingCoach->course->id) }}">{{ $bookingCoach->course->course_type ?? '' }}</a> </p>
        @endif
    </div>

    <!-- Customer Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('customer_id', 'Client:') !!}
        @if(!is_null($bookingCoach->customer))
            <p><a href="{{ url('/customers/'.$bookingCoach->customer->id) }}">{{ $bookingCoach->customer->name ?? '' }}</a></p>
        @endif
    </div>
</div>

<div class="row">
    <!-- Course Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('coach_id', 'Coach :') !!}
        @if(!is_null($bookingCoach->course) && !is_null($bookingCoach->course->coach))
            <p><a href="{{ '/coaches/'.$bookingCoach->course->coach->id }}">{{ $bookingCoach->course->coach->customer->name ?? '' }}</a></p>
        @endif
    </div>
</div>

<div class="row">
    <!-- Pack Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('pack_id', 'Pack:') !!}
        <p>{{ $bookingCoach->pack->name ?? '' }}</p>
    </div>

    <!-- Nb Personne Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('nb_personne', 'Nb Personne:') !!}
        <p>{{ $bookingCoach->nb_personne }}</p>
    </div>
</div>

<div class="row">
    <!-- Amount Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('amount', 'Prix:') !!}
        <p>{{ $bookingCoach->amount }}</p>
    </div>

    <!-- Booking Ref Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('booking_ref', 'Booking Ref:') !!}
        <p>{{ $bookingCoach->booking_ref }}</p>
    </div>
</div>

{{--
<!-- Course Lieu Id Field -->
<div class="form-group">
    {!! Form::label('course_lieu_id', 'Course Lieu Id:') !!}
    <p>{{ $bookingCoach->course_lieu_id }}</p>
</div>

<!-- Course Lieu Note Field -->
<div class="form-group">
    {!! Form::label('course_lieu_note', 'Course Lieu Note:') !!}
    <p>{{ $bookingCoach->course_lieu_note }}</p>
</div>

<!-- Address Name Field -->
<div class="form-group">
    {!! Form::label('address_name', 'Address Name:') !!}
    <p>{{ $bookingCoach->address_name }}</p>
</div>

<!-- Address Lat Field -->
<div class="form-group">
    {!! Form::label('address_lat', 'Address Lat:') !!}
    <p>{{ $bookingCoach->address_lat }}</p>
</div>

<!-- Address Lon Field -->
<div class="form-group">
    {!! Form::label('address_lon', 'Address Lon:') !!}
    <p>{{ $bookingCoach->address_lon }}</p>
</div>
--}}

<div class="row">
    <!-- Quantity Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('quantity', 'Quantity:') !!}
        <p>{{ $bookingCoach->quantity }}</p>
    </div>

    <!-- Coach Confirm Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('coach_confirm', 'Coach Confirm:') !!}
        <p>{!! $bookingCoach->coach_confirm == 1 ? '<span class="px-3 py-2 btn btn-success">Confirmer</span>' : '<span class="px-3 py-2 btn btn-danger">En attente</span>' !!}</p>
    </div>
</div>

{{--
<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $bookingCoach->status }}</p>
</div>

<!-- Status Name Field -->
<div class="form-group">
    {!! Form::label('status_name', 'Status Name:') !!}
    <p>{{ $bookingCoach->status_name }}</p>
</div>
--}}

<div class="row">
    <!-- Created At Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('created_at', 'Date de création:') !!}
        <p>{{ $bookingCoach->created_at }}</p>
    </div>

    <!-- Updated At Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('updated_at', 'Date de modification:') !!}
        <p>{{ $bookingCoach->updated_at }}</p>
    </div>
</div>

<button type="button" class="btn btn-success booking_detail float-right mr-3" data-modal="coach" data-id="{{$bookingCoach->id}}" name="button" style="font-size:10px" title="Voir plus de détails">Voir plus de détails</button>

