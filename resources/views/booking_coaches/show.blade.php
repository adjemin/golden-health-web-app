{{--
@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('bookingCoaches.index') }}">Booking Coach</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('bookingCoaches.index') }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('booking_coaches.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
--}}

@extends('admin_backoffice.layouts.master')

@section('title')
Booking course | {{ config('app.name') }}
@endsection

@section('css')
    <style>
        #progressbar {
            margin-bottom: 3vh;
            overflow: hidden;
            color: rgb(252, 103, 49);
            padding-left: 0px;
            margin-top: 3vh
            }

            #progressbar li {
                list-style-type: none;
                font-size: x-small;
                width: 25%;
                float: left;
                position: relative;
                font-weight: 400;
                color: rgb(160, 159, 159)
            }

            #progressbar #step1:before {
                content: "";
                color: rgb(252, 103, 49);
                width: 5px;
                height: 5px;
                margin-left: 0px !important
            }

            #progressbar #step2:before {
                content: "";
                color: #fff;
                width: 5px;
                height: 5px;
                margin-left: 32%
            }

            #progressbar #step3:before {
                content: "";
                color: #fff;
                width: 5px;
                height: 5px;
                margin-right: 32%
            }

            #progressbar #step4:before {
                content: "";
                color: #fff;
                width: 5px;
                height: 5px;
                margin-right: 0px !important
            }

            #progressbar li:before {
                line-height: 29px;
                display: block;
                font-size: 12px;
                background: #ddd;
                border-radius: 50%;
                margin: auto;
                z-index: -1;
                margin-bottom: 1vh
            }

            #progressbar li:after {
                content: '';
                height: 2px;
                background: #ddd;
                position: absolute;
                left: 0%;
                right: 0%;
                margin-bottom: 2vh;
                top: 1px;
                z-index: 1
            }

            .progress-track {
                padding: 0 8%
            }

            #progressbar li:nth-child(2):after {
                margin-right: auto
            }

            #progressbar li:nth-child(1):after {
                margin: auto
            }

            #progressbar li:nth-child(3):after {
                float: left;
                width: 68%
            }

            #progressbar li:nth-child(4):after {
                margin-left: auto;
                width: 132%
            }

            #progressbar li.active {
                color: black
            }

            #progressbar li.active:before,
            #progressbar li.active:after {
                background: rgb(252, 103, 49)
            }

            #details {
            font-weight: 400
            }

            .info .col-5 {
                padding: 0
            }

            #heading {
                color: grey;
                line-height: 6vh
            }

            .pricing {
                background-color: #ddd3;
                padding: 2vh 8%;
                font-weight: 400;
                line-height: 2.5
            }

            .item_deat {
                font-weight: 400;
                line-height: 2.5
            }

            .pricing .col-3 {
                padding: 0
            }

            .total {
                padding: 2vh 8%;
                color: rgb(252, 103, 49);
                font-weight: bold
            }

            .total .col-3 {
                padding: 0
            }

            .modal-body {
            padding: 0 2rem;
            }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Détails réservation</h4>
                            <p class="card-category">Information réservation</p>
                        </div>
                        <div class="card-body">
                            <p>Cliquez sur "cours, client, coach" pour avoir plus de détails</p>
                            @include('booking_coaches.show_fields')
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            @if(!is_null($bookingCoach->customer))
                            <img class="img-fluid zoomable" src="{{ $bookingCoach->customer->photo_url }}" height="90" />
                            @endif
                        </div>
                        <p class="mt-3 font-weight-bold">{{$bookingCoach->customer->name ?? ''}}</p>
                        <p> <span class="self-challenger-box">Self challenger</span> </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="bookingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Détail réservation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="booking-detail" class="mb-4">
                <input type="hidden" name="booking_id" id="input-booking" />
                <div class="info mb-2">
                    <div class="row">
                        <div class="col-7"></div>
                        <div  class="col-5 pull-right"> <span id="heading">Order No.</span><br> <span id="ref"></span> </div>
                    </div>
                </div>
                <div class="item_deat" id="customer_detail">
                    <div class="title"><strong>Information du client</strong> </div>
                    <div class="row">
                        <div class="col-9"> <span>Nom du client</span> </div>
                        <div class="col-3"> <span id="customer_name"></span> </div>

                        <div class="col-9"> <span>Contact </span> </div>
                        <div class="col-3"> <span id="contact"></span> </div>

                        <div class="col-9"> <span>Commune </span> </div>
                        <div class="col-3"> <span id="commune"></span> </div>

                        <div class="col-9"> <span>Quartier </span> </div>
                        <div class="col-3"> <span id="quartier"></span> </div>
                    </div>
                </div>
                <div class="item_deat" id="customer_detail">
                    <div class="title"><strong>Information Coach</strong> </div>
                    <div class="row">
                        <div class="col-9"> <span>Nom du Coach</span> </div>
                        <div class="col-3"> <span id="coach_name"></span> </div>

                        <div class="col-9"> <span>Contact </span> </div>
                        <div class="col-3"> <span id="coach_contact"></span> </div>

                        <div class="col-9"> <span>Commune </span> </div>
                        <div class="col-3"> <span id="coach_commune"></span> </div>

                        <div class="col-9"> <span>Quartier </span> </div>
                        <div class="col-3"> <span id="coach_quartier"></span> </div>
                    </div>
                </div>
                <div>

                    <div class="item_deat">
                        <div class="title"><strong>Information Réservation</strong> </div>
                        <div class="row">
                            <div class="col-9"> <span>Type Lieu</span> </div>
                            <div class="col-3"> <span id="lieu"></span> </div>
                        </div>

                        <div class="row">
                            <div class="col-9"> <span>Date début</span> </div>
                            <div class="col-3"> <span id="dateStart"></span> </div>
                        </div>
                        <div class="row">
                            <div class="col-9"> <span>Date fin</span> </div>
                            <div class="col-3"> <span id="dateEnd"></span> </div>
                        </div>
                        {{-- <div class="row">
                            <div class="col-9"> <span>Description du lieu</span> </div>
                            <div class="col-3"> <span id="description_lieu"></span> </div>
                        </div> --}}
                    </div>
                    <div class="pricing mt-2">
                        <div class="row">
                            <div class="col-9"> <span id="name-booking"></span> </div>
                            <div class="col-3"> <span id="price">0</span> </div>
                        </div>
                        <div class="row">
                            <div class="col-9"> <span>Quantité</span> </div>
                            <div class="col-3"> <span id="quantity">0</span> </div>
                        </div>
                    </div>
                    <div class="total">
                        <div class="row">
                            <div class="col-9"></div>
                            <div class="col-3"><big id="total"></big></div>
                        </div>
                    </div>
                </div>

                <div class="tracking">
                    <div class="title">Tracking Réservation</div>
                </div>
                <div class="progress-track">
                    <ul id="progressbar">
                        <li class="step0" id="step1">Confirmation Coach</li>
                        <li class="step0  text-right" id="step2">Début de la séance</li>
                        <li class="step0 text-right" id="step3">Terminée</li>
                    </ul>
                </div>
                {{--<div class="row">
                    <div class="col-4">
                        <div id="button-div" style="display:none">
                            <button type="button" class="btn btn-primary" id="confirm_reservation" onclick="return confirm('Confirmer la réservation?')">Confirmer la réservation</button>
                        </div>
                    </div>
                    <div class="col-4">
                        <div id="button-reject" style="display:none">
                            <button type="button" class="btn btn-danger" id="reject_reservation" onclick="return confirm('Rejeter la réservation?')">Rejeter la réservation</button>
                        </div>
                    </div>
                    <div class="col-4">
                    <div id="button-begin" style="display:none">
                        <button type="button" class="btn btn-primary" id="begin_reservation" onclick="return confirm('Démarrer la séance ?')">Démarrer la séance</button>
                    </div>
                    </div>
                    <div class="col-4">
                    <div id="button-terminer" style="display:none">
                        <button type="button" class="btn btn-primary" id="fin_reservation" onclick="return confirm('Terminer la séance ?')">Terminer la séance</button>
                    </div>
                    </div>
                </div>--}}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('assets/js/jquery.blockui.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        function blockUI() {
            $.blockUI({
                message: "<img src='{{asset('assets/img/loading.gif')}}' alt='Loading ...' />"
                , overlayCSS: {
                    backgroundColor: '#1B2024'
                    , opacity: 0.55
                    , cursor: 'wait'
                }
                , css: {
                    border: '4px #999 solid'
                    , padding: 15
                    , backgroundColor: '#fff'
                    , color: 'inherit'
                }
            });
        }

        function unblockUI() {
            $.unblockUI();
        }

        $(document).ready(function() {
            $('.booking_detail').on('click', function(e){
            var bookin_id = $(this).attr('data-id');
            var type_modal = $(this).attr('data-modal');
            
            blockUI();
            $.ajax({
                url: '/api/booking_coaches/'+bookin_id
                , dataType: 'json'
                , type: 'GET'
                , success: function(data) {
                    unblockUI();
                    console.log(data);
                    var resp = data.data;
                    discipline = resp.course.discipline != null ? resp.course.discipline.name : ''
                    /////////// ITEM BOOKING APPEND DATA MODAL

                    $('#booking-detail #input-booking').val(resp.id);
                    $('#booking-detail #name-booking').text( discipline+" - "+ resp.pack.name);
                    $('#booking-detail #price').text(resp.course.price+ " CFA");
                    $('#booking-detail #quantity').text(resp.quantity);
                    $('#booking-detail #ref').text(resp.booking_ref);
                    $('#booking-detail #total').text(resp.amount+ " CFA");
                    $('#booking-detail #dateStart').text(resp.start_date);
                    $('#booking-detail #dateEnd').text(resp.end_date);

                    // var response_lieu

                    $.ajax({
                        url: '/api/lieu_courses/'+resp.course_lieu_id,
                        dataType: 'json',
                        type: 'GET',
                        success: function(datax) {
                            console.log(datax);
                            response_lieu = datax.data
                            $('#booking-detail #lieu').text(datax.data.name);
                            $('#booking-detail #description_lieu').text(datax.data.description);
                        }
                    })
                            

                    /////////// CUSTOMER APPEND DATA MODAL
                    $('#booking-detail #customer_name').text( resp.customer.gender.charAt(0).toUpperCase()+ resp.customer.gender.slice(1).toLowerCase() +" "+resp.customer.first_name+ " "+resp.customer.last_name);
                    $('#booking-detail #contact').text( resp.customer.phone);
                    $('#booking-detail #commune').text( resp.customer.commune);
                    $('#booking-detail #quartier').text( resp.customer.quartier);
                    /////////// CUSTOMER APPEND DATA MODAL
                    $('#booking-detail #coach_name').text( resp.course.coach.customer.name);
                    $('#booking-detail #coach_contact').text( resp.course.coach.customer.phone);
                    $('#booking-detail #coach_commune').text( resp.course.coach.customer.commune);
                    $('#booking-detail #coach_quartier').text( resp.course.coach.customer.quartier);

                    ////////// UPDATE PROGRESS BAR STATE Order
                    if (resp.coach_confirm && resp.status < 2) {
                        $('#booking-detail #step1').addClass('active');
                        $('#booking-detail #button-begin').show();
                        $('#booking-detail #button-terminer').hide();
                    } else {
                        $('#booking-detail #step1').removeClass('active');
                        $('#booking-detail #button-begin').hide();
                    }
                    if (resp.status == 3) {
                        $('#booking-detail #step2').addClass('active');
                        $('#booking-detail #button-terminer').show();
                        $('#booking-detail #button-begin').hide();
                    }else {
                        $('#booking-detail #step2').removeClass('active');
                        $('#booking-detail #button-terminer').hide();
                    }
                    if (resp.status == 4) {
                        $('#booking-detail #step1').addClass('active');
                        $('#booking-detail #step2').addClass('active');
                        $('#booking-detail #step3').addClass('active');
                    } else {
                        $('#booking-detail #step3').removeClass('active');
                    }
                    if (resp.coach_confirm == false){
                        $('#booking-detail #button-div').show();
                    } else {
                        $('#booking-detail #button-div').hide();
                    }
                    if(resp.coach_confirm == false) {
                        $('#booking-detail #button-reject').show();
                    } else {
                        $('#booking-detail #button-reject').hide();
                    }
                    if (type_modal == "customer") {
                        $('#booking-detail #button-reject').hide();
                        $('#booking-detail #button-div').hide();
                        $('#booking-detail #customer_detail').hide();
                        $('#booking-detail #button-begin').hide();
                        $('#booking-detail #button-terminer').hide();
                    }
                    $('#bookingModal').modal('show');
                }
                , error: function(error) {
                    unblockUI();
                    console.log(error);
                }
            });
            e.preventDefault();
            });
        });
    </script>
@endsection