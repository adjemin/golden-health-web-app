<div class="table-responsive-sm">
    <table class="table table-striped printTable" id="bookingCoaches-table">
        <thead>
            <tr>
                <th>Cours</th>
                <th>Client</th>
                <th>Pack</th>
                <th>Nb Personne</th>
                <th>Prix</th>
                <th>Booking Ref</th>
                {{--<th>Course Lieu Id</th>
                <th>Course Lieu Note</th>
                <th>Address Name</th>
                <th>Address Lat</th>
                <th>Address Lon</th>--}}
                <th>Quantity</th>
                <th>Coach Confirm</th>
                {{--<th>Status</th>
                <th>Status Name</th>--}}
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($bookingCoaches as $bookingCoach)
            <tr>
                <td>{{ $bookingCoach->course->course_type ?? '' }}</td>
                <td>{{ $bookingCoach->customer->name ?? '' }}</td>
                <td>{{ $bookingCoach->pack->name ?? '' }}</td>
                <td>{{ $bookingCoach->nb_personne }}</td>
                <td>{{ $bookingCoach->amount }}</td>
                <td>{{ $bookingCoach->booking_ref }}</td>
                {{--<td>{{ $bookingCoach->course_lieu->lieu_course->name ?? '' }}</td>
                <td>{{ $bookingCoach->course_lieu_note }}</td>
                <td>{{ $bookingCoach->address_name }}</td>
                <td>{{ $bookingCoach->address_lat }}</td>
                <td>{{ $bookingCoach->address_lon }}</td>--}}
                <td>{{ $bookingCoach->quantity }}</td>
                <td>{!! $bookingCoach->coach_confirm == 1 ? '<span class="px-3 py-2 btn btn-success">Confirmer</span>' : '<span class="px-3 py-2 btn btn-danger">En attente</span>' !!}</td>
                {{--<td>{{ $bookingCoach->status }}</td>
                <td>{{ $bookingCoach->status_name }}</td>--}}
                <td>
                    {!! Form::open(['route' => ['bookingCoaches.destroy', $bookingCoach->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('bookingCoaches.show', [$bookingCoach->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        {{--<a href="{{ route('bookingCoaches.edit', [$bookingCoach->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>--}}
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>