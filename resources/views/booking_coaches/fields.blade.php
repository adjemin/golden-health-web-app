<!-- Course Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_id', 'Course Id:') !!}
    {!! Form::text('course_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Pack Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pack_id', 'Pack Id:') !!}
    {!! Form::text('pack_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Nb Personne Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nb_personne', 'Nb Personne:') !!}
    {!! Form::text('nb_personne', null, ['class' => 'form-control']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Amount:') !!}
    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Booking Ref Field -->
<div class="form-group col-sm-6">
    {!! Form::label('booking_ref', 'Booking Ref:') !!}
    {!! Form::text('booking_ref', null, ['class' => 'form-control']) !!}
</div>

<!-- Course Lieu Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_lieu_id', 'Course Lieu Id:') !!}
    {!! Form::text('course_lieu_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Course Lieu Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_lieu_note', 'Course Lieu Note:') !!}
    {!! Form::text('course_lieu_note', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_name', 'Address Name:') !!}
    {!! Form::text('address_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_lat', 'Address Lat:') !!}
    {!! Form::text('address_lat', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Lon Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_lon', 'Address Lon:') !!}
    {!! Form::text('address_lon', null, ['class' => 'form-control']) !!}
</div>

<!-- Quantity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quantity', 'Quantity:') !!}
    {!! Form::text('quantity', null, ['class' => 'form-control']) !!}
</div>

<!-- Coach Confirm Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coach_confirm', 'Coach Confirm:') !!}
    {!! Form::text('coach_confirm', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status_name', 'Status Name:') !!}
    {!! Form::text('status_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('bookingCoaches.index') }}" class="btn btn-secondary">Cancel</a>
</div>
