@extends('admin_backoffice.layouts.master')

@section('title')
    Administrateurs | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Administrateurs</h4>
                            <p class="card-category"> Modifier Administrateur</p>
                        </div>
                        <div class="card-body">
                            {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}
                                @include('users.fields')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>
@endsection