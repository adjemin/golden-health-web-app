<div class="row my-2 px-2">
    <!-- Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Nom:') !!}
        {{ $user->name }}
    </div>

    <!-- Email Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('email', 'Email:') !!}
        {{ $user->email }}
    </div>
</div>