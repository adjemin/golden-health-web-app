<div class="input-group mb-3">
    <input type="text" class="form-control {{ $errors->has('name')?'is-invalid':'' }}" name="name" value="{{ $user->name ?? old('name') }}"
            placeholder="Nom complet">
    @if ($errors->has('name'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
</div>
<div class="input-group mb-3">
    <input type="email" class="form-control {{ $errors->has('email')?'is-invalid':'' }}" name="email" value="{{ $user->email ?? old('email') }}" placeholder="Email">
    @if ($errors->has('email'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif
</div>
<div class="input-group mb-3">
    <input type="password" class="form-control {{ $errors->has('password')?'is-invalid':''}}" name="password" placeholder="Password">
    @if ($errors->has('password'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif
</div>
<div class="input-group mb-4">
    <input type="password" name="password_confirmation" class="form-control"
            placeholder="Confirm password">
    @if ($errors->has('password_confirmation'))
        <span class="help-block">
            <strong>{{ $errors->first('password_confirmation') }}</strong>
        </span>
    @endif
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('videotheques.index') }}" class="btn btn-secondary">Annuler</a>
</div>