<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Payer Via AdjeminPay</title>
</head>

<body>
    <script src="https://www.cdn.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript">
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

    <div id="result" style="display:none;">
        <h1 id="result-title"></h1>
        <p id="result-message"></p>
        <p id="result-status"></p>
    </div>
    @csrf

    <input type="hidden" id="apikey" value="{{$data['apikey']}}">
    <input type="hidden" id="notify_url" value="{{$data['notify_url']}}">
    <input type="hidden" id="application_id" value="{{$data['application_id']}}">
    <input type="hidden" id="amount" value="{{$data['amount']}}">

    @if(isset($data['redirect_to']))
        <input type="hidden" id="redirect_to" value="{{$data['redirect_to']}}">
    @endif

    <input type="hidden" id="currency" value="CFA">

    <!-- La longeur maximum d'un id de transaction est de 191 caractères -->
    <input type="hidden" id="transaction_id"
        value="{{$data['transaction_id']}}">

    <input type="hidden" id="custom" value="{{$data['custom']}}">

    <input type="hidden" id="designation" value="{{$data['designation']}}">

    {{-- <button id="payBtn">Payer</button> --}}

    <script>
        var AdjeminPay = AdjeminPay();

        AdjeminPay.on('init', function (e) {
            // retourne une erreur au cas où votre API_KEY ou APPLICATION_ID est incorrecte
            console.log(e);

        });

        // Lance une requete ajax pour vérifier votre API_KEY et APPLICATION_ID
        AdjeminPay.init({
            apikey: $('#apikey').val(),
            application_id: $('#application_id').val(),
            notify_url: $('#notify_url').val()
        });

        // Ecoute le feedback sur les erreurs
        AdjeminPay.on('error', function (e) {
            // la fonction que vous définirez ici sera exécutée en cas d'erreur
            console.log(e);
            $("#result-title").html(e.title);
            $("#result-message").html(e.message);
            $("#result-status").html(e.status);
        });
        AdjeminPay.preparePayment({
            amount: parseInt($('#amount').val()),
            transaction_id: $('#transaction_id').val(),
            currency: $('#currency').val(),
            designation: $('#designation').val(),
            custom: $('#custom').val()
        });

        AdjeminPay.renderPaymentView();

        // Payment terminé
        AdjeminPay.on('paymentTerminated', function (e) {
            console.log('<<<<<<< terminated !');
            console.log('>>>>>>> Paiement terminé !');

            $("#result-title").html(e.title);
            $("#result-message").html(e.message);
            $("#result-status").html(e.status);

            var transaction = {
                amount: parseInt($('#amount').val()),
                transactionId: $('#transaction_id').val(),
                currency: $('#currency').val(),
                designation: $('#designation').val(),
                customField: $('#custom').val()
            }

            //
            adjeminPaymentNotify(transaction.transactionId, e.status);
        });
    </script>
    <script>

    $token = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });
    //
    function adjeminPaymentNotify(reference, status){
        //
        if(reference == null){
            console.log("<<< REf null");
            return false;
        }
        if(status == null){
            console.log("<<< Status null");
            return false;
        }
        var trans = {
            'id': reference,
            'status': status
        }

        console.log(">>> transData");
        console.log(trans);

        $.ajax({
                type: "POST",
                url: "{{ route('api.payments.notify') }}",
                data: {
                    "transaction_id": trans.id,
                    "status": trans.status,
                    // "status": "SUCCESS", // sandbox mode
                },
                dataType: "json",
                success: function (response) {
                    console.log("Response succes <<<");
                    if(response.error != null){
                        window.history.back();
                        alert(response.error.message);
                        return false;
                    }

                    if(response.status != null){
                        if(response.status == "OK"){
                            // Correctly notified
                            // now redirect according to status
                            console.log(">>>>> PAYMENT SUCCESS");
                            // window.location.replace('/customer/shop/checkout_finish/'+trans.id);
                            window.location.replace($("#redirect_to").val());
                        }
                    }

                },
                error: function (response) {
                    console.log("Error :");
                    console.log(response);
                    alert("Erreur");
                    setTimeout(() => {
                        alert("Réessayer");
                    }, 2000);
                },
            });
    }
</script>
</body>

</html>
