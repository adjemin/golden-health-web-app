<div class="row">
    <!-- Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Titre:') !!}
        <p>{{ $advert->name }}</p>
    </div>

    <!-- Cover Url Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('cover_url', 'Cover Url:') !!}
        @if($advert->file_type != "video")
            <p>
                <img src="{{ $advert->cover_url }}" height="100" alt="{{ $advert->name }}">
            </p>
        @else
            <iframe src="{{ url($advert->cover_url) }}" class="embed-responsive-item"></iframe>
        @endif
    </div>
</div>

<div class="row">
    <!-- Description Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('description', 'Description:') !!}
        <p>{!! $advert->description !!}</p>
    </div>

    <!-- Redirect To Field -->
    <div class="form-group  col-sm-6">
        {!! Form::label('redirect_to', 'Redirect To:') !!}
        <p>{{ $advert->redirect_to }}</p>
    </div>
</div>

<div class="row">
    <!-- Priority Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('priority', 'Priorité:') !!}
        <p>{{ $advert->priority }}</p>
    </div>

    <!-- Partner Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('partner_id', 'Partenaire:') !!}
        <p>{{ $advert->partner->name ?? '' }}</p>
    </div>
</div>

{{--
<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $advert->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $advert->updated_at }}</p>
</div>

--}}