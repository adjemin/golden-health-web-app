{{--
@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('adverts.index') !!}">Advert</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Advert</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'adverts.store', 'files' => true]) !!}

                                   @include('adverts.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
--}}

@extends('admin_backoffice.layouts.master')

@section('title')
Annonce | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Annonces</h4>
                            <p class="card-category"> Ajouter une annonce</p> 
                        </div>
                        <div class="card-body">
                            {!! Form::open(['route' => 'adverts.store', 'files' => true]) !!}

                                @include('adverts.fields')

                            {!! Form::close() !!}
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection