<div class="table-responsive-sm">
    <table class="table table-striped" id="adverts-table">
        <thead>
            <tr>
                <th>Nom</th>
                <th>Type de fichier</th>
                {{--<th>Description</th>--}}
                <th>Lien redirection</th>
                <th>Priorité</th>
                <th>Partenaire</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($adverts as $advert)
            <tr>
                <td>{{ $advert->name }}</td>
                <td>{{ $advert->file_type }}</td>
                {{--<td>{!! $advert->description !!}</td>--}}
                <td>{{ $advert->redirect_to }}</td>
                <td>{{ $advert->priority }}</td>
                <td>{{ $advert->partner->name ?? '' }}</td>
                <td>
                    {!! Form::open(['route' => ['adverts.destroy', $advert->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('adverts.show', [$advert->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('adverts.edit', [$advert->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>