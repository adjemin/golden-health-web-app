<div class="row">
    <!-- Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
    <!-- Partner Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('partner_id', 'Partenaire:') !!}
        <select name="partner_id" id="partner_id">
            @foreach($partners as $partner)
                <option value="{{$partner->id}}" {!! $partner->id == old('partner_id') ? 'selected' : '' !!}>{{$partner->name}}</option>
            @endforeach
        </select>
        {{--{!! Form::number('partner_id', null, ['class' => 'form-control']) !!}--}}
    </div>
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {{--{!! Form::label('description', 'Description:') !!}--}}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
</div>

<div class="row">
    <!-- Redirect To Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('redirect_to', 'Redirect To:') !!}
        {!! Form::text('redirect_to', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Priority Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('priority', 'Priorité:') !!}
        {!! Form::number('priority', null, ['class' => 'form-control', 'min' => 1, 'max' => 10]) !!}
    </div>
</div>

<div class="row">
    <!-- Cover Url Field -->
    <div class="form-group col-sm-6">
        <strong>{!! Form::label('cover_url', 'Cover Url:') !!}</strong>
        {!! Form::file('cover_url', ['class' => 'form-control']) !!}
    </div>
    @if(isset($advert))
    <div class="col-md-6">
        <div class="form-group">
            @if($advert->file_type == "image")
                <img style="width: 50%; height: 50%; border: none" id="output" src="{{ $advert->cover_url }}" />
            @else
                <video id="video_output" controls muted src="{{ url($advert->cover_url) }}" width="300"></video>
            @endif
        </div>
    </div>
    @endif
    <script>
        // image_types = ['jpg', 'jpeg', 'png','mp4','mov','flv','3gp','mov','avi','wmv','webm']
        
        // var loadFile = function(event) {
        //     if (event.files && event.files[0]) {
        //         var extension = event.files[0].name.split('.').pop().toLowerCase(),
        //             isImage = image_types.indexOf(extension) > -1;
        //         console.log(extension)
                
        //         var reader = new FileReader(),
        //             file = event.target.files[0];

        //         if(isImage){   
        //             console.log('is image');
        //             reader.onload = function() {
        //                 var output = document.getElementById('output');
        //                 output.src = reader.result;
        //             };
        //             reader.readAsDataURL(file);
        //         }
        //         //  else{
        //         //     isVideo = video_types.indexOf(extension) > -1;
                    
        //         //     if(isVideo){
        //         //         console.log('is video');
        //         //         var blob = new Blob([reader.result], {type: file.type});
        //         //             url = URL.createObjectURL(blob),
        //         //             output = document.getElementById('video_output');
        //         //         output.src = url;

        //         //     }
        //         // }
        //     console.log('not a valide file')
        //     }
            
        // };

    </script>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 mt-3">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('adverts.index') }}" class="btn btn-secondary">Annuler</a>
</div>
