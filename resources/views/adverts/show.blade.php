{{--@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('adverts.index') }}">Advert</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('adverts.index') }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('adverts.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
--}}

@extends('admin_backoffice.layouts.master')

@section('title')
Annonce | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Annonces</h4>
                            <p class="card-category"> Détail annonce</p>   
                        </div>
                        <div class="card-body">
                            @include('adverts.show_fields')
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            @if($advert->file_type != "video")
                                <a href="javascript:;">
                                    <img class="img" src="{{ $advert->cover_url }}" />
                                </a>
                            @else
                                {{--<video src="{{ storage_path($advert->cover_url) }}" controls autoplay muted></video>--}}
                            @endif    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection