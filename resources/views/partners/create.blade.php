{{--
@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('partners.index') !!}">Partner</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Partner</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'partners.store']) !!}

                                   @include('partners.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
--}}

@extends('admin_backoffice.layouts.master')

@section('title')
    Partenaires | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Partenaires</h4>
                            <p class="card-category"> Ajouter partenaires</p>
                            <div class="col text-right">
                                <a class="pull-right btn btn-sm btn-light" href="{{ route('partners.create') }}">Enregistrer un partenaire</a>
                            </div>
                        </div>
                        <div class="card-body">
                            {!! Form::open(['route' => 'partners.store', 'files' => true]) !!}
                                @include('partners.fields')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection