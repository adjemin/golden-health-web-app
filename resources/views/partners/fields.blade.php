<div class="row my-2 px-2">
    <!-- Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Nom:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Email Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row my-2  px-2">
    <!-- Dial Code Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('dial_code', 'Dial Code:') !!}
        {!! Form::text('dial_code', null, ['class' => 'form-control']) !!}
    </div>

   
    <!-- Phone Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('phone_number', 'Numéro de téléphone:') !!}
        {!! Form::text('phone_number', null, ['class' => 'form-control']) !!}
    </div>
    
</div>
<div class="row my-2  px-2">
    <!-- Facebook Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('facebook_id', 'Facebook Id:') !!}
        {!! Form::text('facebook_id', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Twitter Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('twitter_id', 'Twitter Id:') !!}
        {!! Form::text('twitter_id', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row my-2  px-2">
    <!-- Youtube Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('youtube_id', 'Youtube Id:') !!}
        {!! Form::text('youtube_id', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Instagram Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('instagram', 'Instagram:') !!}
        {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row my-2  px-2">
    <!-- Website Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('website', 'Website:') !!}
        {!! Form::text('website', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Newsletter Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('newsletter', 'Newsletter:') !!} <br>
        {!! Form::select('gender', [0 => 'Non', 1 => 'Oui'], ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row my-2  px-2">
    <!-- Photo Url Field -->
    <div class="form-group col-sm-6">
        <strong>{!! Form::label('photo_url', 'Photo Url:') !!}</strong>
        {!! Form::file('photo_url', ['class' => 'form-control', 'onchange' => 'loadFile(event)']) !!}
        <small>Cliquez sur <strong>"Photo url"</strong></small>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <img style="width: 50%; height: 50%; border: none" id="output" />
        </div>
    </div>
</div>

<div class="row my-2">
    <!-- Gender Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('gender', 'Civilité:') !!}
        {!! Form::select('gender', ['Mr' => 'Mr', 'Mme' => 'Mme'], ['class' => 'form-control']) !!}
    </div>
    <!-- Submit Field -->
    <div class="form-group col-sm-6">
        {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('partners.index') }}" class="btn btn-secondary">Annuler</a>
    </div>
</div>

<script>
    var loadFile = function(event) {
        var reader = new FileReader();
        reader.onload = function() {
            var output = document.getElementById('output');
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };

</script>
