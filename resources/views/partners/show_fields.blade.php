<div class="row">
    <!-- Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Nom:') !!}
        <p>{{ $partner->name }}</p>
    </div>

    <!-- Email Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('email', 'Email:') !!}
        <p>{{ $partner->email }}</p>
    </div>
</div>

<div class="row">
    <!-- Dial Code Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('dial_code', 'Dial Code:') !!}
        <p>{{ $partner->dial_code }}</p>
    </div>

    <!-- Phone Number Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('phone_number', 'Numero de téléphone:') !!}
        <p>{{ $partner->phone_number }}</p>
    </div>
</div>

<div class="row">
    {{--<!-- Phone Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('phone', 'Téléphone:') !!}
        <p>{{ $partner->phone }}</p>
    </div>
--}}
    <!-- Facebook Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('facebook_id', 'Facebook:') !!}
        <p>{{ $partner->facebook_id }}</p>
    </div>
</div>

<div class="row">
    <!-- Twitter Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('twitter_id', 'Twitter:') !!}
        <p>{{ $partner->twitter_id }}</p>
    </div>

    <!-- Youtube Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('youtube_id', 'Youtube:') !!}
        <p>{{ $partner->youtube_id }}</p>
    </div>
</div>

<div class="row">
    <!-- Instagram Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('instagram', 'Instagram:') !!}
        <p>{{ $partner->instagram }}</p>
    </div>

    <!-- Website Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('website', 'Website:') !!}
        <p>{{ $partner->website }}</p>
    </div>
</div>

<div class="row">
    <!-- Service Type Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('service_type', 'Type de service:') !!}
        <p>{{ $partner->typeService() }}</p>
    </div>

    @if($partner->typeService() == 'Autres')
        <!-- Newsletter Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('service_type_other', 'Autres types de services:') !!}
            <p>{{ $partner->service_type_other ?? '' }}</p>
        </div>
    @endif
</div>

<div class="row">
    <!-- Photo Url Field -->
    {{--<div class="form-group col-sm-6">
        {!! Form::label('photo_url', 'Photo Url:') !!}
        <p>{{ $partner->photo_url }}</p>
    </div>--}}

    <!-- Gender Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('gender', 'Civilité:') !!}
        <p>{{ $partner->gender }}</p>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('discovery_source', 'comment avez-vous connu golden health ?:') !!}
            <p>{{ $partner->discovery_source ?? '' }}</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('descritpion', 'Parlez-nous un peu de vous :') !!}
            <p>{{ $partner->description ?? '' }}</p>
        </div>
    </div>
</div>

<div class="row">
    <!-- Created At Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('created_at', 'Created At:') !!}
        <p>{{ $partner->created_at }}</p>
    </div>

    <!-- Updated At Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('updated_at', 'Updated At:') !!}
        <p>{{ $partner->updated_at }}</p>
    </div>
</div>
