{{--
@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('partners.index') }}">Partner</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('partners.index') }}" class="btn btn-light">Retour</a>
                             </div>
                             <div class="card-body">
                                 @include('partners.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
--}}

@extends('admin_backoffice.layouts.master')

@section('title')
    Partenaires | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Détail Partenaire</h4>
                            <p class="card-category"> Information sur le partenaire</p>
                        </div>
                        <div class="card-body">
                            @include('partners.show_fields')
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <a href="javascript:;">
                                <img class="img" src="{{ $partner->photo_url }}" />
                            </a>
                        </div>
                        <div class="card-body">
                            <a href="{{ route('partners.index') }}" class="btn btn-primary btn-round mt-3">Retour</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection