<div class="table-responsive-sm">
    <table class="table table-striped printTable" id="partners-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                {{--<th>Dial Code</th>
                <th>Phone Number</th>--}}
                <th>Phone</th>
                {{--<th>Facebook Id</th>
                <th>Twitter Id</th>
                <th>Youtube Id</th>
                <th>Instagram</th>
                <th>Website</th>--}}
                <th>Service Type</th>
                {{--<th>Newsletter</th>
                <th>Photo Url</th>
                <th>Gender</th>--}}
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($partners as $partner)
            <tr>
                <td>{{ $partner->name }}</td>
                <td>{{ $partner->email }}</td>
                {{--<td>{{ $partner->dial_code }}</td>
                <td>{{ $partner->phone_number }}</td>--}}
                <td>{{ $partner->phone }}</td>
                {{--<td>{{ $partner->facebook_id }}</td>
                <td>{{ $partner->twitter_id }}</td>
                <td>{{ $partner->youtube_id }}</td>
                <td>{{ $partner->instagram }}</td>
                <td>{{ $partner->website }}</td>--}}
                <td>{{ $partner->typeService() }}</td>
                {{--<td>{{ $partner->newsletter }}</td>
                <td>{{ $partner->photo_url }}</td>
                <td>{{ $partner->gender }}</td>--}}
                <td>
                    {!! Form::open(['route' => ['partners.destroy', $partner->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('partners.show', [$partner->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('partners.edit', [$partner->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>