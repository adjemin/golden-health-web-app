<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>LISTE DES PARTENAIRES</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }

        body {
            margin: 0px;
        }

        * {
            font-family: Verdana, Arial, sans-serif;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .invoice table {
            margin: 15px;
            padding-left: 40px;
            padding-right: 40px;
        }

        .invoice h3 {
            margin-left: 15px;
            padding-left: 40px;
            padding-right: 40px;
        }

        .invoice h4 {
            margin-left: 15px;
            padding-left: 40px;
            padding-right: 40px;
        }

        .information {
            padding: 40px;
        }

        .information .logo {
            margin: 5px;
        }

        .information table {
            padding: 10px;
        }
    </style>
</head>
<body onload="window.print()">
    @php
        $varDate = now();
        $countPartners = $partners->count();
    @endphp
    <div class="information">
        <table width="100%">
            <tr>
                <td align="left" style="width: 40%;">
                    <img src="{{asset('customer/images/gh1.png')}}" alt="Logo" width="100" class="logo" />
                </td>
                
                <td align="right" style="width: 40%;">

                    <pre>
                    Abidjan, le {{ date('d/m/Y', strtotime($varDate)) }}
                </pre>
                </td>
            </tr>

        </table>
    </div>


    <br />

    <div class="invoice">
        <table width="100%">
            <thead>
                <tr align="left">
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Service Type</th>
                </tr>
            </thead>
            <tbody>
            @foreach($partners as $partner)
                <tr>
                    <td>{{ $partner->name }}</td>
                    <td>{{ $partner->email }}</td>
                    <td>{{ $partner->phone }}</td>
                    <td>{{ $partner->typeService() }}</td>
                </tr>
            @endforeach
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="3"></td>
                    <td align="right">Total : </td>
                    <td align="left" class="gray">{{ $countPartners }}</td>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="information" style="position: absolute; bottom: 0;">
        <table width="100%">
            <tr>
                <td align="left" style="width: 50%;">
                    &copy; {{ date('Y') }} {{-- {{ config('app.url') }} --}} GOLDENHEALTH - Tous droits réservés.
                </td>
                <td align="right" style="width: 50%;">
                    Powered by <a href="{{ env('ADJEMIN_URL') }}" style="color: black">Adjemin</a>
                </td>
            </tr>

        </table>
    </div>
</body>

</html>