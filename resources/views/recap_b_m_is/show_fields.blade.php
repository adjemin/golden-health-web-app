<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $recapBMI->description }}</p>
</div>

<!-- Interval Field -->
<div class="form-group">
    {!! Form::label('interval', 'Interval:') !!}
    <p>{{ $recapBMI->interval }}</p>
</div>

<!-- Color Field -->
<div class="form-group">
    {!! Form::label('color', 'Color:') !!}
    <p>{{ $recapBMI->color }}</p>
</div>

<!-- Weigth Field -->
<div class="form-group">
    {!! Form::label('weigth', 'Weigth:') !!}
    <p>{{ $recapBMI->weigth }}</p>
</div>

<!-- Height Field -->
<div class="form-group">
    {!! Form::label('height', 'Height:') !!}
    <p>{{ $recapBMI->height }}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{{ $recapBMI->customer_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $recapBMI->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $recapBMI->updated_at }}</p>
</div>

