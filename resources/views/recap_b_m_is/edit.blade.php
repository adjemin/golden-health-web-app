@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('recapBMIs.index') !!}">Recap B M I</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Recap B M I</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($recapBMI, ['route' => ['recapBMIs.update', $recapBMI->id], 'method' => 'patch']) !!}

                              @include('recap_b_m_is.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection