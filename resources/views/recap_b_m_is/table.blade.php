<div class="table-responsive-sm">
    <table class="table table-striped" id="recapBMIs-table">
        <thead>
            <tr>
                <th>Description</th>
        <th>Interval</th>
        <th>Color</th>
        <th>Weigth</th>
        <th>Height</th>
        <th>Customer Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($recapBMIs as $recapBMI)
            <tr>
                <td>{{ $recapBMI->description }}</td>
            <td>{{ $recapBMI->interval }}</td>
            <td>{{ $recapBMI->color }}</td>
            <td>{{ $recapBMI->weigth }}</td>
            <td>{{ $recapBMI->height }}</td>
            <td>{{ $recapBMI->customer_id }}</td>
                <td>
                    {!! Form::open(['route' => ['recapBMIs.destroy', $recapBMI->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('recapBMIs.show', [$recapBMI->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('recapBMIs.edit', [$recapBMI->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>