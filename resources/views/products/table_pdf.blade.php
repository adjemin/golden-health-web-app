{{--
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Document</title>
</head>
<body>
<div class="table-responsive">
    <table class="table printTable" id="products-table">
        <thead class="text-primary">
            <tr>
                <th>Medias</th>
                <th>Nom</th>
                <th>Couleur</th>
                <th>Tailles</th>
                {{--<th>Description</th>--}}
                {{-- <th>Description Metadata</th> --}}
                <th>Price</th>
                {{-- <th>Original Price</th> --}}
                {{-- <th>Fees</th> --}}
                {{-- <th>Currency Slug</th> --}}
                {{-- <th>Old Price</th> --}}
                {{-- <th>Has Promo</th> --}}
                {{-- <th>Promo Percentage</th> --}}

                {{-- <th>Is Sold</th> --}}
                {{-- <th>Sold At</th> --}}
                {{-- <th>Initiale Count</th> --}}
                {{-- <th>Published At</th> --}}
                {{-- <th>Is Published</th> --}}
                {{-- <th>Slug</th> --}}
                {{-- <th>Link</th> --}}
                {{-- <th>Delivery Services</th> --}}
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
                <tr>
                    <td>
                        @if (!empty($product->getImageUrl()))
                            <img src="{{ json_decode($product->getImageUrl())[0] ?? $product->getImageUrl() }}" alt="{{ $product->title }}" width="90">
                        @endif
                    </td>
                    <td>{{ $product->title }}</td>

                    <td>
                        {{-- @foreach ($product->getProductColor() as $color)
                            <p>{{ $color->name }}</p>
                        @endforeach --}}
                        <div class="row">
                            @foreach ($product->getProductColor() as $color)
                                {{-- <p>{{ $color->name }}</p> --}}
                                <div class="col" style="background-color:{{ $color->code_color }};">
                                    {{ $color->name }}
                                </div>
                            @endforeach
                        </div>
                    </td>
                    {{-- <td>{!! $product->description !!}</td>
                    --}}
                    {{-- <td>{{ $product->description_metadata }}</td>
                    --}}
                    <td>{{ $product->sizes }}</td>
                    <td>{{ $product->price }}</td>
                    {{-- <td>{{ $product->original_price }}</td>
                    --}}
                    {{-- <td>{{ $product->fees }}</td> --}}
                    {{-- <td>{{ $product->currency_slug }}</td>
                    <td>{{ $product->old_price }}</td>
                    <td>{{ $product->has_promo }}</td>
                    <td>{{ $product->promo_percentage }}</td> --}}

                    {{-- <td>{{ $product->is_sold }}</td>
                    <td>{{ $product->sold_at }}</td>
                    <td>{{ $product->initial_count }}</td>
                    <td>{{ $product->published_at }}</td>
                    <td>{{ $product->is_published }}</td>
                    <td>{{ $product->slug }}</td>
                    <td>{{ $product->link }}</td>
                    <td>{{ $product->delivery_services }}</td> --}}
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</body>
</html>
--}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>LISTE DES PRODUITS</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }

        body {
            margin: 0px;
        }

        * {
            font-family: Verdana, Arial, sans-serif;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .invoice table {
            margin: 15px;
            padding-left: 40px;
            padding-right: 40px;
        }

        .invoice h3 {
            margin-left: 15px;
            padding-left: 40px;
            padding-right: 40px;
        }

        .invoice h4 {
            margin-left: 15px;
            padding-left: 40px;
            padding-right: 40px;
        }

        .information {
            padding: 40px;
        }

        .information .logo {
            margin: 5px;
        }

        .information table {
            padding: 10px;
        }
    </style>
</head>
<body onload="window.print()">
    @php
        $varDate = now();
        //$countPartners = $partners->count();
    @endphp
    <div class="information">
        <table width="100%">
            <tr>
                <td align="left" style="width: 40%;">
                    <img src="{{asset('customer/images/gh1.png')}}" alt="Logo" width="100" class="logo" />
                </td>
                
                <td align="right" style="width: 40%;">

                    <pre>
                    Abidjan, le {{ date('d/m/Y', strtotime($varDate)) }}
                </pre>
                </td>
            </tr>

        </table>
    </div>


    <br />

    <div class="invoice">
        <table width="100%">
            <thead>
                <tr align="left">
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Service Type</th>
                </tr>
            </thead>
            <tbody>
            @foreach($partners as $partner)
                <tr>
                    <td>{{ $partner->name }}</td>
                    <td>{{ $partner->email }}</td>
                    <td>{{ $partner->phone }}</td>
                    <td>{{ $partner->typeService() }}</td>
                </tr>
            @endforeach
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="3"></td>
                    <td align="right">Total : </td>
                    <td align="left" class="gray">{{ $countPartners }}</td>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="information" style="position: absolute; bottom: 0;">
        <table width="100%">
            <tr>
                <td align="left" style="width: 50%;">
                    &copy; {{ date('Y') }} {{-- {{ config('app.url') }} --}} GOLDENHEALTH - Tous droits réservés.
                </td>
                <td align="right" style="width: 50%;">
                    Powered by <a href="{{ env('ADJEMIN_URL') }}" style="color: black">Adjemin</a>
                </td>
            </tr>

        </table>
    </div>
</body>

</html>