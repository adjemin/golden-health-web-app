<div class="table-responsive">
    <table class="table printTable" id="products-table">
        <thead class="text-primary">
            <tr>
                <th>Medias</th>
                <th>Nom</th>
                <th>Couleur</th>
                <th>Tailles</th>
                {{--<th>Description</th>--}}
                {{-- <th>Description Metadata</th> --}}
                <th>Price</th>
                {{-- <th>Original Price</th> --}}
                {{-- <th>Fees</th> --}}
                {{-- <th>Currency Slug</th> --}}
                {{-- <th>Old Price</th> --}}
                {{-- <th>Has Promo</th> --}}
                {{-- <th>Promo Percentage</th> --}}

                {{-- <th>Is Sold</th> --}}
                {{-- <th>Sold At</th> --}}
                {{-- <th>Initiale Count</th> --}}
                {{-- <th>Published At</th> --}}
                {{-- <th>Is Published</th> --}}
                {{-- <th>Slug</th> --}}
                {{-- <th>Link</th> --}}
                {{-- <th>Delivery Services</th> --}}
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
                <tr>
                    <td>
                        @if (!empty($product->getImageUrl()))
                            <img src="{{ json_decode($product->getImageUrl())[0] ?? $product->getImageUrl() }}" alt="{{ $product->title }}" width="90">
                        @endif
                    </td>
                    <td>{{ $product->title }}</td>

                    <td>
                        {{-- @foreach ($product->getProductColor() as $color)
                            <p>{{ $color->name }}</p>
                        @endforeach --}}
                        <div class="row">
                            @foreach ($product->getProductColor() as $color)
                                {{-- <p>{{ $color->name }}</p> --}}
                                <div class="col" style="background-color:{{ $color->code_color }};">
                                    {{ $color->name }}
                                </div>
                            @endforeach
                        </div>
                    </td>
                    {{-- <td>{!! $product->description !!}</td>
                    --}}
                    {{-- <td>{{ $product->description_metadata }}</td>
                    --}}
                    <td>{{ $product->sizes }}</td>
                    <td>{{ $product->price }}</td>
                    {{-- <td>{{ $product->original_price }}</td>
                    --}}
                    {{-- <td>{{ $product->fees }}</td> --}}
                    {{-- <td>{{ $product->currency_slug }}</td>
                    <td>{{ $product->old_price }}</td>
                    <td>{{ $product->has_promo }}</td>
                    <td>{{ $product->promo_percentage }}</td> --}}

                    {{-- <td>{{ $product->is_sold }}</td>
                    <td>{{ $product->sold_at }}</td>
                    <td>{{ $product->initial_count }}</td>
                    <td>{{ $product->published_at }}</td>
                    <td>{{ $product->is_published }}</td>
                    <td>{{ $product->slug }}</td>
                    <td>{{ $product->link }}</td>
                    <td>{{ $product->delivery_services }}</td> --}}
                    <td>
                        {!! Form::open(['route' => ['products.destroy', $product->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('products.show', [$product->id]) }}" class='btn btn-ghost-success'><i
                                    class="fa fa-eye"></i></a>
                            <a href="{{ route('products.edit', [$product->id]) }}" class='btn btn-ghost-info'><i
                                    class="fa fa-edit"></i></a>
                            {!! Form::button('<i class="fa fa-trash"></i>', [
                            'type' => 'submit',
                            'class' => 'btn
                            btn-ghost-danger',
                            'onclick' => "return confirm('Are you sure?')",
                            ]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
