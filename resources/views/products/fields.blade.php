<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            {!! Form::text('title', null, ['class' => 'form-control', 'onkeyup' => 'stringToSlug(this.value)']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control', 'readonly']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">

        <div class="form-group">
            {{-- {!! Form::label('description', 'Description:') !!}
            --}}
            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('colors', 'Couleur:') !!}
            <select name="colors[]" id="color" multiple size="12">
                @foreach($colors as $color)
                    <option style="background-color:{{$color->code_color}};" value="{{$color->id}}">{{$color->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('sizes', 'Taille :') !!}
            {!! Form::text('sizes', $product->sizes ?? null, ['class' => 'form-control']) !!}
            <small>Ajouter les différentes tailles séparer une virgurle</small>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('partner_id', 'Partenaire :') !!}
            <select name="partner_id" id="partner_id" class="form-control">
                <option value="">Choisir un partenaire</option>
                @foreach($partners as $partner)
                    <option value="{{$partner->id}}">{{$partner->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('original_price', 'Prix:') !!}
            {!! Form::number('original_price', $product->original_price ?? 0, ['class' => 'form-control', 'min' => 0]) !!}
        </div>
    </div>
    <div class="col-md-4">

        <div class="form-group">
            {!! Form::label('fees', 'Frais de transport:') !!}
            {!! Form::number('fees', $product->fees ?? 0, ['class' => 'form-control', 'min' => 0, 'value' => 0]) !!}
        </div>
    </div>
    <div class="col-md-4">

        <div class="form-group">
            {!! Form::label('currency_slug', 'Devise:') !!}
            <select name="currency_slug" id="currency_slug" class="form-control">
                <option selected value="XOF">Franc CFA</option>
            </select>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('has_promo', 'Est en Promo?') !!}
            <select name="has_promo" id="has_promo" class="form-control" onchange="showPercentage(this.value)">
                <option value="0">NON</option>
                <option value="1">OUI</option>
            </select>
        </div>
    </div>
    <div class="col-md-4" style="display:none;" id="div_percentage">
        <div class="form-group">
            {!! Form::label('promo_percentage', 'Promo Percentage:') !!}
            {!! Form::number('promo_percentage', $product->promo_percentage ?? 0, ['class' => 'form-control', 'min' => 0, 'max' => 100]) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('product_categories', "Categories de l'article:") !!}
            <select name="product_categories[]" id="product_categories" class="form-control" multiple  size="30">
                @foreach($categories as $categorie)
                    @if(!isset($product))
                        <option value="{{$categorie->id}}" >{{$categorie->name}}</option>
                    @else
                        <option value="{{$categorie->id}}" {!! in_array($categorie, $product->categories->toArray()) ? 'selected' : ''  !!}>{{$categorie->name}}</option>
                    @endif
                @endforeach
            </select>
            <small>Vous avez la possibilité d'en sélectionner plusieurs en maintenant la touche "ctrl" et sélectionnant les categories de votres choix</small>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('medias', 'Images:') !!}
            {{--<input type="file" name="medias[]" class="form-control" id="medias" onchange="loadFile(this)"  multiple>--}}
            {!! Form::file('medias[]', ['class' => 'form-control', 'onchange' => 'loadFile(this)', 'multiple'=>true,'accept'=>'image/*']) !!}
            {{--<br><small>Cliquez sur "Images" pour uploader vos medias</small> --}}
        </div>
    </div>
</div>

<div class="col-sm-6 gallery" style="max-width: 80%;">
    {{--<div class="form-group">
        <img style="width: 50%; height: 50%; border: none" id="output" />
    </div>--}}
</div>

<script>
    var loadFile = function(input){
        if (input.files) {
            var filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).attr('height', 80).appendTo('.gallery');
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    } 
    
    // var loadFile = function(event) {
    //     var reader = new FileReader();
    //     reader.onload = function() {
    //         var output = document.getElementById('output');
    //         output.src = reader.result;
    //     };
    //     reader.readAsDataURL(event.target.files[0]);
    // };

</script>

<div class="row mt-3">
    <div class="col-md-4">
        {!! Form::label('is_sold', 'Est disponible?') !!}
        <select name="is_sold" id="is_sold" class="form-control" onchange="showSold(this.value)">
            <option value="0">NON</option>
            <option value="1">OUI</option>
        </select>
    </div>
    <div class="col-md-4" style="display:none" id="div_sold">
        <!-- Initiale Count Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('initial_count', ' Quantité disponible:') !!}
            {!! Form::number('initial_count', $product->initial_count ?? 0, ['class' => 'form-control', 'min'=>0]) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('sold_at', 'Date cloture de vente:') !!}
            {!! Form::date('sold_at', null, ['class' => 'form-control', 'id' => 'sold_at']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    {{--s<div class="col-md-6">
        <div class="form-group">
            {!! Form::label('link', 'Lien:') !!}
            {!! Form::text('link', null, ['class' => 'form-control']) !!}
        </div>
    </div>--}}
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('is_published', 'Publier?') !!}
            <select name="is_published" id="is_published" class="form-control">
                <option value="0">NON</option>
                <option value="1">OUI</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('gender', 'Genre :') !!}
            <select name="gender" id="gender" class="form-control">
                <option value="Unisexe">Unisexe</option>
                <option value="Homme">Homme</option>
                <option value="Femme">Femme</option>
            </select>
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('products.index') }}" class="btn btn-secondary">Annuler</a>
</div>

<script>
    function stringToSlug(str) {

        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        // return str;
        document.getElementById("slug").value = str;
    }

    function showPercentage(val){
        if(val == 1){
            document.getElementById("div_percentage").style.display= 'block';
        }else{
            document.getElementById("div_percentage").style.display= 'none';
        }
    }

    function showSold(val){
        if(val == 1){
            document.getElementById("div_sold").style.display= 'block';
        }else{
            document.getElementById("div_sold").style.display= 'none';
        }
    }
</script>
