{{-- @extends('layouts.app')

@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ route('products.index') }}">Product</a>
    </li>
    <li class="breadcrumb-item active">Detail</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        @include('coreui-templates::common.errors')
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Details</strong>
                        <a href="{{ route('products.index') }}" class="btn btn-light">Back</a>
                    </div>
                    <div class="card-body">
                        @include('products.show_fields')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}

@extends('admin_backoffice.layouts.master')

@section('title')
    Produit | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Détails produit</h4>
                            <p class="card-category">Information produit</p>
                        </div>
                        <div class="card-body">
                            {{-- <div class="row"> --}}
                                @include('products.show_fields')
                                <a href="{{ route('products.edit', [$product->id]) }}"
                                    class="btn btn-primary pull-right">Modifier</a>

                                {{-- </div> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <img class="img-fluid zoomable" src="{{ $product->getImageUrl() }}" height="90" />
                        </div>
                        <div class="card-body">
                            <h6 class="card-category text-gray">{{ $product->name }}</h6>
                            {{-- <p class="card-description">
                                {{ $product->description }}
                            </p> --}}
                            <a href="{{ route('products.index') }}" class="btn btn-primary btn-round">Retour</a>
                            
                        </div>
                    </div>
                    <div class="card mt-3">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Nouvel arrivage</h4>
                            <p class="card-category">Complèter le stock de produit</p>
                        </div>
                        <div class="card-body">
                            <form action="/product/stock/{{$product->id}}" method="post">
                                @csrf
                                <input class="form-control" type="number" placeholder="Stock à ajouter" name="stock" min=1 id="stock">
                                <input class="btn btn-primary btn-round" type="submit" value="Enregistrer">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="http://static.tumblr.com/xz44nnc/o5lkyivqw/jquery-1.3.2.min.js"></script>
<script>
    $('img.zoomable').css({
        cursor: 'pointer'
    }).on('click', function() {
        var img = $(this);
        var bigImg = $('<img />').css({
            'max-width': '100%',
            'max-height': '100%',
            'display': 'inline'
        });
        bigImg.attr({
            src: img.attr('src'),
            alt: img.attr('alt'),
            title: img.attr('title')
        });
        var over = $('<div />').text(' ').css({
            'height': '100%',
            'width': '100%',
            'background': 'rgba(0,0,0,.82)',
            'position': 'fixed',
            'top': 0,
            'left': 0,
            'opacity': 0.0,
            'cursor': 'pointer',
            'z-index': 9999,
            'text-align': 'center'
        }).append(bigImg).bind('click', function() {
            $(this).fadeOut(300, function() {
                $(this).remove();
            });
        }).insertAfter(this).animate({
            'opacity': 1
        }, 300);
    });

</script>
@endsection
