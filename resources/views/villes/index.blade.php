{{--@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Villes</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             villes
                             <a class="pull-right" href="{{ route('villes.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                         </div>
                         <div class="card-body">
                             @include('villes.table')
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection--}}

@extends('admin_backoffice.layouts.master')

 @section('title')
    Ville | {{ config('app.name') }}
 @endsection
 
 @section('content')
     <div class="container-fluid">
         <div class="animated fadeIn">
             <div class="clearfix"></div>
 
             @include('flash::message')
 
             <div class="clearfix"></div>
             <div class="row">
                 <div class="col-md-12">
                     <div class="card">
                         <div class="card-header card-header-primary">
                             <h4 class="card-title ">Ville</h4>
                             <p class="card-category"> Toutes les Villes ou communes</p>
                             <div class="col text-right">
                                 <a class="pull-right btn btn-sm btn-light" href="{{ route('villes.create') }}">Ajouter une villes</a>
                             </div>
                         </div>
                         <div class="card-body">
                             @include('villes.table')
                         </div>
                         <div class="mt-3 d-flex justify-content-center">
                             {{-- $videotheques->links() --}}
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 @endsection