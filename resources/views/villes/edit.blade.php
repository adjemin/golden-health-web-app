{{--@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('villes.index') !!}">Ville</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Ville</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($ville, ['route' => ['villes.update', $ville->id], 'method' => 'patch']) !!}

                              @include('villes.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection--}}

@extends('admin_backoffice.layouts.master')

@section('title')
    Ville | {{ config('app.name') }}
@endsection
 
@section('content')
     <div class="container-fluid">
         <div class="animated fadeIn">
             <div class="row">
                 <div class="col-md-12">
                     <div class="card">
                         <div class="card-header card-header-primary">
                             <h4 class="card-title">Ajouter une Ville ou Commune</h4>
                         </div>
                         <div class="card-body">
                         {!! Form::model($ville, ['route' => ['villes.update', $ville->id], 'method' => 'patch']) !!}

                            @include('villes.fields')

                        {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
