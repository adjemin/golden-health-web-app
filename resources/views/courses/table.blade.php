<div class="table-responsive">
    <table class="table" id="courses-table">
        <thead class="text-primary">
            <tr>
                {{-- <th>Pack</th> --}}
                <th>Discipline</th>
                {{-- <th>Titre</th> --}}
                {{-- <th>Nbr Participants</th> --}}
                {{-- <th>Description</th> --}}
                <th>Price</th>
                <th>Status</th>
                <th>Type Course</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($courses as $course)
                <tr>
                    {{-- <td>{{ $course->pack_id ?? '---' }}</td> --}}
                    <td>{{ $course->getDisciplineAttribute()->name ?? '---' }}</td>
                    {{-- <td>{{ $course->title ?? '---' }}</td> --}}
                    {{-- <td>{{ $course->person_number ?? '---' }}</td> --}}
                    {{-- <td>{{ $course->description ?? '---' }}</td> --}}
                    <td>{{ $course->price ?? '---' }}</td>
                    <td>@if($course->active == 1) <span class="btn btn-success px-3 py-2">Actif</span> @else <span class="btn btn-danger px-3 py-2">Inactif</span> @endif </td>
                    <td>{{ $course->getCourseTypeObjectAttribute()->name ?? '---'}}</td>
                    <td>
                        {!! Form::open(['route' => ['courses.destroy', $course->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('courses.show', [$course->id]) }}" class='btn btn-ghost-success'><i
                                    class="fa fa-eye"></i></a>
                            {{--<a href="{{ route('courses.edit', [$course->id]) }}" class='btn btn-ghost-info'><i
                                    class="fa fa-edit"></i></a>--}}
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn
                            btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
