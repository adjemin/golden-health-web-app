{{-- <!-- Pack Id Field -->
<div class="form-group">
    {!! Form::label('pack_id', 'Pack Id:') !!}
    <p>{{ $course->pack_id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $course->title }}</p>
</div>

<!-- Person Number Field -->
<div class="form-group">
    {!! Form::label('person_number', 'Person Number:') !!}
    <p>{{ $course->person_number }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $course->description }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $course->price }}</p>
</div>

<!-- Type Course Field -->
<div class="form-group">
    {!! Form::label('type_course', 'Type Course:') !!}
    <p>{{ $course->type_course }}</p>
</div> --}}

{{--
<div class="row mt-3">
    <!-- Pack Id Field -->
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('pack_id', 'Pack:') !!}
            <p>{{ $course->pack_id }}</p>
        </div>
    </div>
    <!-- Title Field -->
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('title', 'Titre du cours:') !!}
            <p>{{ $course->title }}</p>
        </div>
    </div>
</div>
--}}


<div class="row mt-3">
    {{-- <!-- Person Number Field -->
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('person_number', 'Nombre de participants:') !!}
            <p>{{ $course->person_number }}</p>
        </div>
    </div>

    <!-- Price Field -->
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('price', 'Prix:') !!}
            <p>{{ $course->price }}</p>
        </div>
    </div> --}}

    <!-- Description Field -->
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('description', 'Type de séance:') !!}
            <p>{{ $course->detail->seance_type ?? '' }}</p>
        </div>
    </div>

    <!-- Description Field -->
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('description', 'Experience:') !!}
            <p>{{ $course->detail->experience ?? ''}}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('discipline', 'Discipline:') !!}
            <p>{{ $course->discipline->name ?? '' }}</p>
        </div>
    </div>
    <!-- Type Course Field -->
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('price', 'Prix:') !!}
            <p>{{ $course->price ?? '---' }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Type Course Field -->
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('type_course', 'Type de Course:') !!}
            <p>{{ $course->getCourseTypeObjectAttribute()->name ?? '---' }}</p>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('has_particulier', 'Est particulier?') !!}
            @if(!is_null($course->detail))
            <p>{{ $course->detail->has_particulier == 1 ? 'OUI' : 'NON' }}</p>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('type_course', 'Est collectif?') !!}
            @if(!is_null($course->detail))
                <p>{{ $course->detail->has_particulier == 1 ? 'OUI' : 'NON' }}</p>
            @endif
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('diplome', 'Diplome:') !!}
            <p>
                @if(!is_null($course->detail))
                    <a href="{{ $course->detail->diplome_url ?? '#' }}" class="btn btn-info"> {!! $course->detail->diplome_name ?? "Voir diplome" !!}</a>
                @endif
            </p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('created_at', 'Date de création:') !!}
            <p>{{ $course->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('updated_at', 'Date de modification:') !!}
            <p>{{ $course->updated_at }}</p>
        </div>
    </div>
</div>

{{-- <!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $course->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $course->updated_at }}</p>
</div> --}}

