<div class="row mt-3">
    <!-- Pack Id Field -->
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('pack_id', 'Choisir le pack:') !!}
            {!! Form::select('pack_id', $packItems->pluck('name', 'id'), null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <!-- Title Field -->
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('title', 'Titre du cours:') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>


<div class="row mt-3">
    <!-- Person Number Field -->
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('person_number', 'Nombre de participants:') !!}
            {!! Form::text('person_number', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Price Field -->
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('price', 'Prix:') !!}
            {!! Form::text('price', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Description Field -->
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>


<div class="row mt-3">
    <!-- Type Course Field -->
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('type_course', 'Type de Course:') !!}
            {!! Form::text('type_course', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>




<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregister', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('courses.index') }}" class="btn btn-secondary">Annuler</a>
</div>
