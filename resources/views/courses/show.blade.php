{{-- @extends('layouts.app')

@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ route('courses.index') }}">Course</a>
    </li>
    <li class="breadcrumb-item active">Detail</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        @include('coreui-templates::common.errors')
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Details</strong>
                        <a href="{{ route('courses.index') }}" class="btn btn-light">Back</a>
                    </div>
                    <div class="card-body">
                        @include('courses.show_fields')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}



@extends('admin_backoffice.layouts.master')

@section('title')
    Cours | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Détails cours</h4>
                            <p class="card-category">Information cours</p>
                        </div>
                        <div class="card-body">
                            {{-- <div class="row"> --}}
                                @include('courses.show_fields')
                                {{-- <div class="row">
                                    <div class="form-group col-sm-6">
                                        <a href="{{ route('courses.edit', [$course->id]) }}"
                                            class="btn btn-primary pull-right">Modifier</a>
                                    </div>
                                </div> --}}
                                {{-- </div> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <a href="javascript:;">
                                {{-- {{ dd($course->coach->getCustomerAttribute()->photo_url) }} --}}
                                <img class="img" src="@if ($course->coach->getCustomerAttribute())
                                {{ $course->coach->getCustomerAttribute()->photo_url }}
                                @else
                                https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png
                                @endif" />
                            </a>
                        </div>
                        <div class="card-body">
                            <h6 class="card-category text-gray"><a href="{{ url('/coaches/'.$course->coach->getCustomerAttribute()->id) }}" target="_blank"></a>{{ $course->coach->getCustomerAttribute()->name ?? '' }}</h6>
                            {{-- <p class="card-description">
                                {{ $course->bio }}
                            </p> --}}
                            {{-- <a href="{{ route('courses.index') }}" class="btn btn-secondary btn-round">Activer le cours</a> --}}
                            <div class="form-group">
                                Changer le status du cours.
                                {!! Form::open(['route' => ['course.changeStatus', $course->id], 'method' =>
                                'post']) !!}
                                @if ($course->active == 1)
                                    {!! Form::hidden('active', 0) !!}
                                    {!! Form::button('Désactiver', ['type' => 'submit', 'class' => 'btn
                                    btn-ghost-danger', 'onclick' => "return confirm('Êtes vous sûr de confirmer la
                                    commande ?')"]) !!}
                                @else
                                    {!! Form::hidden('active', 1) !!}
                                    {!! Form::button('Activer', ['type' => 'submit', 'class' => 'btn
                                    btn-ghost-danger', 'onclick' => "return confirm('Êtes vous sûr de confirmer la
                                    commande ?')"]) !!}
                                @endif
                                {!! Form::close() !!}
                            </div>
                            <a href="{{ route('courses.index') }}" class="btn btn-primary btn-round">Retour</a>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Détails Disponibilité</h4>
                            <p class="card-category">Information Disponibilité</p>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped mt-3">
                                <thead>
                                    <tr>
                                        <th>Discipline</th>
                                        <th>Date de debut</th>
                                        <th>Date de fin</th>
                                        <th>Heure de depart</th>
                                        <th>Heure de fin</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($course->getAvailabilitiesAttribute() as $availability)
                                        <tr>
                                            <th>{{ $course->discipline->name }}</th>
                                            <td>{{ $availability->date_debut }}</td>
                                            <td>{{ $availability->date_fin }}</td>
                                            <td>{{ $availability->start_time }}</td>
                                            <td>{{ $availability->end_time }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
