{{-- @extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('categories.index') }}">Category</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('categories.index') }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('categories.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
 --}}

 @extends('admin_backoffice.layouts.master')

 @section('title')
     Catégorie | {{ config('app.name') }}
 @endsection
 
 @section('content')
     <div class="container-fluid">
         <div class="animated fadeIn">
 
             <div class="row">
                 <div class="col-md-8">
                     <div class="card">
                         <div class="card-header card-header-primary">
                             <h4 class="card-title">Détails catégorie</h4>
                             <p class="card-category">Information catégorie</p>
                         </div>
                         <div class="card-body">
                             {{-- <div class="row"> --}}
                                 @include('categories.show_fields')
                                 <a href="{{ route('categories.edit', [$category->id]) }}"
                                     class="btn btn-primary pull-right">Modifier</a>
 
                                 {{-- </div> --}}
                         </div>
                     </div>
                 </div>
                 <div class="col-md-4">
                     <div class="card card-profile">
                         <div class="card-avatar">
                             <a href="javascript:;">
                                 <img class="img" src="{{ $category->image ?? '---' }}" height="90"/>
                             </a>
                         </div>
                         <div class="card-body">
                             <h6 class="card-category text-gray">{{ $category->name ?? '---' }}</h6>
                             <p class="card-description">
                                 {{ $category->description ?? '---' }}
                             </p>
                             <a href="{{ route('categories.index') }}" class="btn btn-primary btn-round">Retour</a>
 
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 @endsection
 