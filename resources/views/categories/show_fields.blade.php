{{-- <!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $category->name }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{{ $category->slug }}</p>
</div> --}}

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('name', 'Nom:') !!}
            <p>{{ $category->name }}</p>
        </div>
    </div>
    <div class="col-md-6">

        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            <p>{{ $category->slug }}</p>
        </div>
    </div>
</div>
{{-- 
<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $category->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $category->updated_at }}</p>
</div>
 --}}
