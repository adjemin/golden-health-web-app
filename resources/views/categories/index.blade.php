{{-- @extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Categories</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Categories
                             <a class="pull-right" href="{{ route('categories.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                         </div>
                         <div class="card-body">
                             @include('categories.table')
                              <div class="pull-right mr-3">
                                     
        @include('coreui-templates::common.paginate', ['records' => $categories])

                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

 --}}

 @extends('admin_backoffice.layouts.master')

 @section('title')
     Catégories | {{ config('app.name') }}
 @endsection
 
 @section('content')
     <div class="container-fluid">
         <div class="animated fadeIn">
             <div class="clearfix"></div>
 
             @include('flash::message')
 
             <div class="clearfix"></div>
             <div class="row">
                 <div class="col-md-12">
                     <div class="card">
                         <div class="card-header card-header-primary">
                             <h4 class="card-title ">Catégories</h4>
                             <p class="card-category"> Toutes les catégories</p>
                             <div class="col text-right">
                                 <a class="pull-right btn btn-sm btn-light" href="{{ route('categories.create') }}">Ajouter
                                     une
                                     catégorie</a>
                             </div>
                         </div>
                         <div class="card-body">
                             @include('categories.table')
                         </div>
                         <div class="mt-3 d-flex justify-content-center">
                             {{ $categories->links() }}
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 @endsection
 