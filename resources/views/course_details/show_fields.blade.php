<!-- Course Id Field -->
<div class="form-group">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{{ $courseDetails->course_id }}</p>
</div>

<!-- Experience Field -->
<div class="form-group">
    {!! Form::label('experience', 'Experience:') !!}
    <p>{{ $courseDetails->experience }}</p>
</div>

<!-- Seance Type Field -->
<div class="form-group">
    {!! Form::label('seance_type', 'Seance Type:') !!}
    <p>{{ $courseDetails->seance_type }}</p>
</div>

<!-- Is Diplome Field -->
<div class="form-group">
    {!! Form::label('is_diplome', 'Is Diplome:') !!}
    <p>{{ $courseDetails->is_diplome }}</p>
</div>

<!-- Is Formation Field -->
<div class="form-group">
    {!! Form::label('is_formation', 'Is Formation:') !!}
    <p>{{ $courseDetails->is_formation }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $courseDetails->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $courseDetails->updated_at }}</p>
</div>

