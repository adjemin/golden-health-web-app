<!-- Course Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_id', 'Course Id:') !!}
    {!! Form::text('course_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Experience Field -->
<div class="form-group col-sm-6">
    {!! Form::label('experience', 'Experience:') !!}
    {!! Form::text('experience', null, ['class' => 'form-control']) !!}
</div>

<!-- Seance Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('seance_type', 'Seance Type:') !!}
    {!! Form::text('seance_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Diplome Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_diplome', 'Is Diplome:') !!}
    {!! Form::text('is_diplome', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Formation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_formation', 'Is Formation:') !!}
    {!! Form::text('is_formation', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('courseDetails.index') }}" class="btn btn-secondary">Cancel</a>
</div>
