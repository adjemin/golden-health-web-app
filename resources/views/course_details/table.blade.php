<div class="table-responsive-sm">
    <table class="table table-striped" id="courseDetails-table">
        <thead>
            <tr>
                <th>Course Id</th>
        <th>Experience</th>
        <th>Seance Type</th>
        <th>Is Diplome</th>
        <th>Is Formation</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($courseDetails as $courseDetails)
            <tr>
                <td>{{ $courseDetails->course_id }}</td>
            <td>{{ $courseDetails->experience }}</td>
            <td>{{ $courseDetails->seance_type }}</td>
            <td>{{ $courseDetails->is_diplome }}</td>
            <td>{{ $courseDetails->is_formation }}</td>
                <td>
                    {!! Form::open(['route' => ['courseDetails.destroy', $courseDetails->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('courseDetails.show', [$courseDetails->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('courseDetails.edit', [$courseDetails->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>