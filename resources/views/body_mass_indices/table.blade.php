<div class="table-responsive-sm">
    <table class="table table-striped" id="bodyMassIndices-table">
        <thead>
            <tr>
                <th>Date</th>
        <th>Value Bmi</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($bodyMassIndices as $bodyMassIndex)
            <tr>
            <td>{{ $bodyMassIndex->customer_id }}</td>
            <td>{{ $bodyMassIndex->date }}</td>
            <td>{{ $bodyMassIndex->value_BMI }}</td>
                <td>
                    {!! Form::open(['route' => ['bodyMassIndices.destroy', $bodyMassIndex->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('bodyMassIndices.show', [$bodyMassIndex->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('bodyMassIndices.edit', [$bodyMassIndex->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>