<!-- customer_id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer ID:') !!}
    <p>{{ $bodyMassIndex->customer_id }}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{{ $bodyMassIndex->date }}</p>
</div>

<!-- Value Bmi Field -->
<div class="form-group">
    {!! Form::label('value_BMI', 'Value Bmi:') !!}
    <p>{{ $bodyMassIndex->value_BMI }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $bodyMassIndex->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $bodyMassIndex->updated_at }}</p>
</div>

