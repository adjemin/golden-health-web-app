@extends('admin_backoffice.layouts.master')

@section('title')
Contacts | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Contacts</h4>
                            <p class="card-category"> Détail Contact</p>   
                        </div>
                        <div class="card-body">
                            @include('contacts.show_fields')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection