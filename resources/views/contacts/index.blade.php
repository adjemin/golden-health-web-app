
@extends('admin_backoffice.layouts.master')

@section('title')
Contacts | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Contacts</h4>
                            <p class="card-category"> Toutes les contacts</p>   
                        </div>
                        <div class="card-body">
                            @include('contacts.table')
                        </div>
                        <div class="mt-3 d-flex justify-content-center">
                            {{ $contacts->links() }}
                        </div>
                        <div class="float-right">
                            <a href="/contact/download/all" class="btn btn-success mr-1" >Exporter tableau complet</a>
                            <button id="print" class="btn btn-info">Exporter tableau actuel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
