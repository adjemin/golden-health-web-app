<div class="row">
    <!-- Customer Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Nom:') !!}
        <p>{{ $contact->name }}</p>
    </div>

    <!-- Coach Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('phone', 'Contact:') !!}
        <p>{{ $contact->phone }}</p>
    </div>
</div>

<div class="row">
    <!-- Email Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('email', 'Email:') !!}
        <p>{{ $contact->email }}</p>
    </div>

    <!-- Subject Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('subject', 'Sujet:') !!}
        <p>{{ $contact->subject }}</p>
    </div>
</div>

<!-- Message Field -->
<div class="form-group">
    {!! Form::label('message', 'Message:') !!}
    <p>{{ $contact->message }}</p>
</div>