<div class="table-responsive-sm">
    <table class="table table-striped" id="services-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Name En</th>
        <th>Slug</th>
        <th>Description</th>
        <th>Description En</th>
        <th>Logo</th>
        <th>Price</th>
        <th>Price En</th>
        <th>Price Description</th>
        <th>Price Description En</th>
        <th>Price Is Variable</th>
        <th>Currency</th>
        <th>Delay</th>
        <th>Delay En</th>
        <th>Delay Time</th>
        <th>Delay Time Unit</th>
        <th>Rank</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($services as $service)
            <tr>
                <td>{{ $service->name }}</td>
            <td>{{ $service->name_en }}</td>
            <td>{{ $service->slug }}</td>
            <td>{{ $service->description }}</td>
            <td>{{ $service->description_en }}</td>
            <td>{{ $service->logo }}</td>
            <td>{{ $service->price }}</td>
            <td>{{ $service->price_en }}</td>
            <td>{{ $service->price_description }}</td>
            <td>{{ $service->price_description_en }}</td>
            <td>{{ $service->price_is_variable }}</td>
            <td>{{ $service->currency }}</td>
            <td>{{ $service->delay }}</td>
            <td>{{ $service->delay_en }}</td>
            <td>{{ $service->delay_time }}</td>
            <td>{{ $service->delay_time_unit }}</td>
            <td>{{ $service->rank }}</td>
                <td>
                    {!! Form::open(['route' => ['services.destroy', $service->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('services.show', [$service->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('services.edit', [$service->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>