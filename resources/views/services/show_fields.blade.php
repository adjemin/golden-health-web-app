<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $service->name }}</p>
</div>

<!-- Name En Field -->
<div class="form-group">
    {!! Form::label('name_en', 'Name En:') !!}
    <p>{{ $service->name_en }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{{ $service->slug }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $service->description }}</p>
</div>

<!-- Description En Field -->
<div class="form-group">
    {!! Form::label('description_en', 'Description En:') !!}
    <p>{{ $service->description_en }}</p>
</div>

<!-- Logo Field -->
<div class="form-group">
    {!! Form::label('logo', 'Logo:') !!}
    <p>{{ $service->logo }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $service->price }}</p>
</div>

<!-- Price En Field -->
<div class="form-group">
    {!! Form::label('price_en', 'Price En:') !!}
    <p>{{ $service->price_en }}</p>
</div>

<!-- Price Description Field -->
<div class="form-group">
    {!! Form::label('price_description', 'Price Description:') !!}
    <p>{{ $service->price_description }}</p>
</div>

<!-- Price Description En Field -->
<div class="form-group">
    {!! Form::label('price_description_en', 'Price Description En:') !!}
    <p>{{ $service->price_description_en }}</p>
</div>

<!-- Price Is Variable Field -->
<div class="form-group">
    {!! Form::label('price_is_variable', 'Price Is Variable:') !!}
    <p>{{ $service->price_is_variable }}</p>
</div>

<!-- Currency Field -->
<div class="form-group">
    {!! Form::label('currency', 'Currency:') !!}
    <p>{{ $service->currency }}</p>
</div>

<!-- Delay Field -->
<div class="form-group">
    {!! Form::label('delay', 'Delay:') !!}
    <p>{{ $service->delay }}</p>
</div>

<!-- Delay En Field -->
<div class="form-group">
    {!! Form::label('delay_en', 'Delay En:') !!}
    <p>{{ $service->delay_en }}</p>
</div>

<!-- Delay Time Field -->
<div class="form-group">
    {!! Form::label('delay_time', 'Delay Time:') !!}
    <p>{{ $service->delay_time }}</p>
</div>

<!-- Delay Time Unit Field -->
<div class="form-group">
    {!! Form::label('delay_time_unit', 'Delay Time Unit:') !!}
    <p>{{ $service->delay_time_unit }}</p>
</div>

<!-- Rank Field -->
<div class="form-group">
    {!! Form::label('rank', 'Rank:') !!}
    <p>{{ $service->rank }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $service->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $service->updated_at }}</p>
</div>

