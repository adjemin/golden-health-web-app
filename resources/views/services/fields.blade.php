<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Name En Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name_en', 'Name En:') !!}
    {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Description En Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description_en', 'Description En:') !!}
    {!! Form::textarea('description_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Logo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('logo', 'Logo:') !!}
    {!! Form::text('logo', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Price En Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price_en', 'Price En:') !!}
    {!! Form::text('price_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('price_description', 'Price Description:') !!}
    {!! Form::textarea('price_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Description En Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('price_description_en', 'Price Description En:') !!}
    {!! Form::textarea('price_description_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency', 'Currency:') !!}
    {!! Form::text('currency', null, ['class' => 'form-control']) !!}
</div>

<!-- Delay Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delay', 'Delay:') !!}
    {!! Form::text('delay', null, ['class' => 'form-control','id'=>'delay']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#delay').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Delay En Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delay_en', 'Delay En:') !!}
    {!! Form::text('delay_en', null, ['class' => 'form-control','id'=>'delay_en']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#delay_en').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Delay Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delay_time', 'Delay Time:') !!}
    {!! Form::text('delay_time', null, ['class' => 'form-control','id'=>'delay_time']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#delay_time').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Delay Time Unit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delay_time_unit', 'Delay Time Unit:') !!}
    {!! Form::text('delay_time_unit', null, ['class' => 'form-control','id'=>'delay_time_unit']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#delay_time_unit').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Rank Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rank', 'Rank:') !!}
    {!! Form::number('rank', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('services.index') }}" class="btn btn-secondary">Cancel</a>
</div>
