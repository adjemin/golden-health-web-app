<div class="table-responsive-sm">
    <table class="table table-striped" id="notes-table">
        <thead>
            <tr>
                <th>Customer Id</th>
        <th>Order Id</th>
        <th>Message</th>
        <th>Rate</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($notes as $note)
            <tr>
                <td>{{ $note->customer_id }}</td>
            <td>{{ $note->order_id }}</td>
            <td>{{ $note->message }}</td>
            <td>{{ $note->rate }}</td>
                <td>
                    {!! Form::open(['route' => ['notes.destroy', $note->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('notes.show', [$note->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('notes.edit', [$note->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>