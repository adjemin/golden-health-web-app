<div class="table-responsive-sm">
    <table class="table table-striped" id="distinctions-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Date Debut</th>
        <th>Date Fin</th>
        <th>Fichier Url</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($distinctions as $distinction)
            <tr>
                <td>{{ $distinction->name }}</td>
            <td>{{ $distinction->date_debut }}</td>
            <td>{{ $distinction->date_fin }}</td>
            <td>{{ $distinction->fichier_url }}</td>
                <td>
                    {!! Form::open(['route' => ['distinctions.destroy', $distinction->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('distinctions.show', [$distinction->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('distinctions.edit', [$distinction->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>