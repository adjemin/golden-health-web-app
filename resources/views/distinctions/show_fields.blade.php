<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $distinction->name }}</p>
</div>

<!-- Date Debut Field -->
<div class="form-group">
    {!! Form::label('date_debut', 'Date Debut:') !!}
    <p>{{ $distinction->date_debut }}</p>
</div>

<!-- Date Fin Field -->
<div class="form-group">
    {!! Form::label('date_fin', 'Date Fin:') !!}
    <p>{{ $distinction->date_fin }}</p>
</div>

<!-- Fichier Url Field -->
<div class="form-group">
    {!! Form::label('fichier_url', 'Fichier Url:') !!}
    <p>{{ $distinction->fichier_url }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $distinction->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $distinction->updated_at }}</p>
</div>

