<div class="table-responsive-sm">
    <table class="table table-striped" id="objectives-table">

      <thead>
          <tr>
              <th>Name</th>
      <th>Slug</th>
      <th>Description</th>
      <th>Active</th>
              <th colspan="3">Action</th>
          </tr>
      </thead>
      <tbody>
      @foreach($objectives as $objective)
          <tr>
          <td>{{ $objective->name }}</td>
          <td>{{ $objective->slug }}</td>
          <td>{{ $objective->description }}</td>
          <td>{{ $objective->active }}</td>
              <td>
                  {!! Form::open(['route' => ['objectives.destroy', $objective->id], 'method' => 'delete']) !!}
                  <div class='btn-group'>
                      <a href="{{ route('objectives.show', [$objective->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                      <a href="{{ route('objectives.edit', [$objective->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                      {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                  </div>
                  {!! Form::close() !!}
              </td>
          </tr>
      @endforeach
      </tbody>
    </table>
</div>
