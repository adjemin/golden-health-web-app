<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Montserrat Font -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap"
        rel="stylesheet">
    <!-- Mobile Css -->
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('css/adjemin.css')}}">
    <link rel="stylesheet" href="{{asset('css/css/forms.css')}}">
    <title>Adjemin - 404</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <link rel="icon" href="{{asset('img/logo.png')}}">

</head>

<body class="">
    <div id="fixed-area" class="bg-pale text-primary text-center">
        <div id="position-relative" class="container px-4">
            <div class="brand">
                <img src="{{asset('img/people_.png')}}" alt="" class="img-fluid">
            </div>
            <img src="{{asset('img/sprite_up.png')}}" alt="" class="sprite_up">
            <img src="{{asset('img/sprite_down.png')}}" alt="" class="sprite_down">
            <div class="text">
                <!-- <h5>Regain control</h5> -->
                <h4 class="">
                    Rejoignez la communauté Adjemin
                </h4>
                <h2 class="font-weight-bold">
                    Téléchargement gratuit sur :
                </h2>
                <div class="d-flex flex-sm-row flex-column align-items-center justify-content-between">
                    <a href="{{env('APP_GOOGLE_PLAY')}}" target="_blank" class="btn btn-black mx-auto mt-2">
                        <span class="d-flex align-items-center justify-content-start">
                            <img src="{{asset('img/icon/google-play.png')}}" class="icon mr-2" width="24px" alt="">
                            Téléchargez sur <br>
                            Google Play
                        </span>
                    </a>
                    <a  href="{{env('APP_GOOGLE_PLAY')}}" target="_blank"  class="btn btn-black mx-auto mt-2">
                        <span class="d-flex align-items-center justify-content-start">
                            <i class="fa fa-apple mr-2" aria-hidden="true"></i>
                            Téléchargez sur <br>
                            Apple Store
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div> <!-- /fixed area -->
    <div id="flex-area" class="">
        <div id="form-area" class=" ">
            <div id="formarea-container" class="container-fluid">
                <div class="container-fluid mt-5 pt-5" id="form-container">
                    <div class="d-flex justify-content-center">
                        <h1 class="text-secondary font-weight-bold text-center">
                            404 - Page Not Found
                        </h1>
                    </div>
                    <div class="d-flex justify-content-center">
                        <img src="{{asset('img/logo.png')}}" alt="" class="mx-auto" width="100px" height="auto">
                    </div>
                    <div class="d-flex justify-content-center">
                        <p class="text-center">
                            La page que vous recherchez n'existe pas ou a été déplacée.
                        </p>
                    </div>
                    <div class="mt-3">
                        <div class="d-flex flex-column flex-md-row justify-content-between align-items-center align-items-md-baseline">
                            <a href="/" class="btn btn-secondary p-3 font-weight-bold text-uppercase mx-auto">Retourner à l'accueil</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
</body>

</html>
