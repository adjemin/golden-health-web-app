<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="{{ route('home') }}">Dashboard</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end">
            @if ( Request::is('coaches') || Request::is('customers') ||  Request::is('products') ||  Request::is('partners') ||  Request::is('events') ||  Request::is('bookingCoaches') ||  Request::is('orders') )
            <form class="navbar-form">
                <div class="input-group no-border">
                    @if ( Request::is('coaches') )
                    <input type="text" name="searchCoach" id="searchCoach" value="" class="form-control" placeholder="Chercher...">
                    @elseif ( Request::is('customers') )
                    <input type="text" name="searchCustomer" id="searchCustomer" value="" class="form-control" placeholder="Chercher...">
                    @elseif ( Request::is('products') )
                    <input type="text" name="searchProduct" id="searchProduct" value="" class="form-control" placeholder="Chercher...">
                    @elseif ( Request::is('partners') )
                    <input type="text" name="searchpartner" id="searchPartner" value="" class="form-control" placeholder="Chercher...">
                    @elseif ( Request::is('events') )
                    <input type="text" name="searchEvent" id="searchEvent" value="" class="form-control" placeholder="Chercher...">
                    @elseif ( Request::is('bookingCoaches') )
                    <input type="text" name="searchBookingCoach" id="searchBookingCoach" value="" class="form-control" placeholder="Chercher...">
                    @elseif ( Request::is('orders') )
                    <input type="text" name="searchOrder" id="searchOrder" value="" class="form-control" placeholder="Chercher...">
                    @endif
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </div>
            </form>
            @endif
            
            <ul class="navbar-nav">
                {{-- <li class="nav-item">
                    <a class="nav-link" href="javascript:;">
                        <i class="material-icons">dashboard</i>
                        <p class="d-lg-none d-md-block">
                            Stats
                        </p>
                    </a>
                </li> --}}
                {{-- <li class="nav-item dropdown">
                    <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">notifications</i>
                        <span class="notification">5</span>
                        <p class="d-lg-none d-md-block">
                            Some Actions
                        </p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Mike John responded to your email</a>
                        <a class="dropdown-item" href="#">You have 5 new tasks</a>
                        <a class="dropdown-item" href="#">You're now friend with Andrew</a>
                        <a class="dropdown-item" href="#">Another Notification</a>
                        <a class="dropdown-item" href="#">Another One</a>
                    </div>
                </li> --}}
                <li class="nav-item dropdown">
                    <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">person</i>
                        <p class="d-lg-none d-md-block">
                            Account
                        </p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                    {{-- <a class="dropdown-item" href="#">Profile</a>
                        <a class="dropdown-item" href="#">Settings</a> --}}
                        <div class="dropdown-divider"></div>
                        {{-- <a class="dropdown-item" href="#">Déconnexion</a> --}}
                        <a href="{{ url('/logout') }}" class="dropdown-item"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Déconnexion
                        </a>
                        <form id="logout-form" action="{{ url('/customer/logout') }}" method="POST"
                            style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- End Navbar -->
