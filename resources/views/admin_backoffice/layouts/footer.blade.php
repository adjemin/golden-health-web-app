<footer class="footer">
    <div class="container-fluid">
      <nav class="float-left">
        <ul>
          <li>
            <a href="{{ route('home') }}">
              GoldenHealth
            </a>
          </li>
        </ul>
      </nav>
      <div class="copyright float-right">
        &copy;
        <script>
          document.write(new Date().getFullYear())
        </script>, powered by
        <a href="https://adjemin.com" target="_blank">Adjemin</a>.
      </div>
      <!-- your footer here -->
    </div>
</footer>
