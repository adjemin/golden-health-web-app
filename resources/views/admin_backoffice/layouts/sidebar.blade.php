  {{-- <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">

   <div class="logo"><a href="http://www.creative-tim.com" class="simple-text logo-normal">
        Creative Tim
      </a></div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li class="nav-item active  ">
          <a class="nav-link" href="./dashboard.html">
            <i class="material-icons">dashboard</i>
            <p>Dashboard</p>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/analytique">
            <i class="material-icons">analytics</i>
            <p>Statistique</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="./user.html">
            <i class="material-icons">person</i>
            <p>User Profile</p>
          </a>
        </li> <li class="nav-item ">
          <a class="nav-link" href="./tables.html">
            <i class="material-icons">content_paste</i>
            <p>Table List</p>
          </a>
        </li> <li class="nav-item ">
          <a class="nav-link" href="./typography.html">
            <i class="material-icons">library_books</i>
            <p>Typography</p>
          </a>
        </li>

        <li class="nav-item ">
          <a class="nav-link" href="./icons.html">
            <i class="material-icons">bubble_chart</i>
            <p>Icons</p>
          </a>
        </li>


        <li class="nav-item ">
          <a class="nav-link" href="./notifications.html">
            <i class="material-icons">notifications</i>
            <p>Notifications</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="./rtl.html">
            <i class="material-icons">language</i>
            <p>RTL Support</p>
          </a>
        </li>

      </ul>
    </div>
  </div> --}}
  <div class="sidebar-wrapper">
    <ul class="nav">

        <li class="nav-item {{ Request::is('home*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('home') }}">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('sendMail*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('sendMail') }}">
              <i class="material-icons">mail_outline</i>
              <p>Messages</p>
            </a>
        </li>
        <!-- <li class="nav-item">
          <a class="nav-link" href="/analytique">
            <i class="material-icons">analytics</i>
            <p>Statistique</p>
          </a>
        </li> -->

        <li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('users.index') }}">
              <i class="material-icons">admin_panel_settings</i>
              <p>Administrateurs</p>
            </a>
        </li>

        <li class="nav-item {{ Request::is('coaches*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('coaches.index') }}">
                <i class="material-icons">accessibility_new</i>
                <p>Coachs</p>
            </a>
        </li>

        <li class="nav-item {{ Request::is('courses*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('courses.index') }}">
                <i class="material-icons">menu_book</i>
                <p>Cours</p>
            </a>
        </li>

        <li class="nav-item {{ Request::is('partners*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('partners.index') }}">
                <i class="material-icons">group</i>
                <p>Partenaires</p>
            </a>
        </li>

        <li class="nav-item {{ Request::is('customers*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('customers.index') }}">
                <i class="material-icons">person</i>
                <p>Self challenger</p>
            </a>
        </li>
        {{-- <li class="nav-item {{ Request::is('languages*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('languages.index') }}">
                <i class="material-icons">language</i>
                <p>Languages</p>
            </a>
        </li> --}}
        {{-- <li class="nav-item {{ Request::is('availabilities*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('availabilities.index') }}">
                <i class="material-icons"> calendar_today </i>
                <p>Disponibilités</p>
            </a>
        </li> --}}
        <li class="nav-item {{ Request::is('disciplines*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('disciplines.index') }}">
                <i class="material-icons">sports</i>
                <p>Disciplines</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('events*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('events.index') }}">
                <i class="material-icons">event</i>
                <p>Events</p>
            </a>
        </li>
        {{-- <li class="nav-item {{ Request::is('coachDisciplines*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('coachDisciplines.index') }}">
                <i class="material-icons">sports</i>
                <p>Coach Disciplines</p>
            </a>
        </li> --}}
        {{-- <li class="nav-item {{ Request::is('packs*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('packs.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Packs</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('packSubscriptions*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('packSubscriptions.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Pack Subscriptions</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('portfolios*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('portfolios.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Portfolios</p>
            </a>
        </li> --}}
        <li class="nav-item {{ Request::is('products*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('products.index') }}">
                <i class="material-icons">category</i>
                <p>Produits</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('product-reservation*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('product-reservation.index') }}">
                <i class="material-icons">category</i>
                <p>Réservation articles</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('orders*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('orders.index') }}">
                <i class="material-icons">list_alt</i>
                <p>Commandes</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('categories*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('categories.index') }}">
                <i class="material-icons">local_offer</i>
                <p>Catégories</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('posts*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('posts.index') }}">
                <i class="material-icons">create</i>
                <p>Articles</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('bookingCoaches*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('bookingCoaches.index') }}">
                <i class="material-icons">book</i>
                <p>Réservation</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('adverts*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('adverts.index') }}">
                <i class="material-icons">announcement</i>
                <p>Annonces</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('videotheques*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('videotheques.index') }}">
                <i class="material-icons">perm_media</i>
                <p>Videothèques</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('coupons*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('coupons.index') }}">
                <i class="material-icons">code</i>
                <p>Coupons</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('clientTestimonies*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('clientTestimonies.index') }}">
                <i class="material-icons">messages</i>
                <p>Témoignages Clients</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('contacts*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('contacts.index') }}">
                <i class="material-icons">contact_page</i>
                <p>Contacts</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('programs*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('programs.index') }}">
                <i class="material-icons">assignment</i>
                <p>Programmes</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('reviews*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('reviews.index') }}">
                <i class="material-icons">rate_review</i>
                <p>Avis clients</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('slides*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('slides.index') }}">
                <i class="material-icons">slideshow</i>
                <p>Slides</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('villes*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('villes.index') }}">
                <i class="material-icons">business</i>
                <p>Villes ou communes</p>
            </a>
        </li>

        {{-- <li class="nav-item {{ Request::is('bodyMassIndices*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('bodyMassIndices.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Indice de masse corporel</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('recapBMIs*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('recapBMIs.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Recap B M Is</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('addresses*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('addresses.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Adresses</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('coupons*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('coupons.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Coupons</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('invoices*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('invoices.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Factures</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('courses*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('courses.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Cours</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('services*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('services.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Services</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('experiences*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('experiences.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Expériences</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('qualifications*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('qualifications.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Qualifications</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('qualificationTypes*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('qualificationTypes.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Qualification Types</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('invoicePayments*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('invoicePayments.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Invoice Payments</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('notes*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('notes.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Notes</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('advice*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('advice.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Advice</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('objectives*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('objectives.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Objectives</p>
            </a>
        </li>
        <li class="nav-item {{ Request::is('seances*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('seances.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <p>Seances</p>
            </a>
        </li> --}}

</ul>
</div>
