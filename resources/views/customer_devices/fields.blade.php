<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Firebase Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('firebase_id', 'Firebase Id:') !!}
    {!! Form::text('firebase_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Device Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('device_id', 'Device Id:') !!}
    {!! Form::text('device_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Device Model Field -->
<div class="form-group col-sm-6">
    {!! Form::label('device_model', 'Device Model:') !!}
    {!! Form::text('device_model', null, ['class' => 'form-control']) !!}
</div>

<!-- Device Os Field -->
<div class="form-group col-sm-6">
    {!! Form::label('device_os', 'Device Os:') !!}
    {!! Form::text('device_os', null, ['class' => 'form-control']) !!}
</div>

<!-- Device Os Version Field -->
<div class="form-group col-sm-6">
    {!! Form::label('device_os_version', 'Device Os Version:') !!}
    {!! Form::text('device_os_version', null, ['class' => 'form-control']) !!}
</div>

<!-- Device Model Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('device_model_type', 'Device Model Type:') !!}
    {!! Form::text('device_model_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Device Meta Data Field -->
<div class="form-group col-sm-6">
    {!! Form::label('device_meta_data', 'Device Meta Data:') !!}
    {!! Form::text('device_meta_data', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('customerDevices.index') }}" class="btn btn-secondary">Cancel</a>
</div>
