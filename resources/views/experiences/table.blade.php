<div class="table-responsive-sm">
    <table class="table table-striped" id="experiences-table">
        <thead>
            <tr>
                <th>Experience</th>
        <th>Title</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($experiences as $experience)
            <tr>
                <td>{{ $experience->experience }}</td>
            <td>{{ $experience->title }}</td>
                <td>
                    {!! Form::open(['route' => ['experiences.destroy', $experience->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('experiences.show', [$experience->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('experiences.edit', [$experience->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>