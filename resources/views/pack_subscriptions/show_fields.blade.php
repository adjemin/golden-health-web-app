<!-- Pack Id Field -->
<div class="form-group">
    {!! Form::label('pack_id', 'Pack Id:') !!}
    <p>{{ $packSubscription->pack_id }}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{{ $packSubscription->customer_id }}</p>
</div>

<!-- Coach Id Field -->
<div class="form-group">
    {!! Form::label('coach_id', 'Coach Id:') !!}
    <p>{{ $packSubscription->coach_id }}</p>
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    <p>{{ $packSubscription->is_active }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $packSubscription->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $packSubscription->updated_at }}</p>
</div>

