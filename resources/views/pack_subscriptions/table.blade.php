<div class="table-responsive-sm">
    <table class="table table-striped" id="packSubscriptions-table">
        <thead>
            <tr>
                <th>Pack Id</th>
        <th>Customer Id</th>
        <th>Coach Id</th>
        <th>Is Active</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($packSubscriptions as $packSubscription)
            <tr>
                <td>{{ $packSubscription->pack_id }}</td>
            <td>{{ $packSubscription->customer_id }}</td>
            <td>{{ $packSubscription->coach_id }}</td>
            <td>{{ $packSubscription->is_active }}</td>
                <td>
                    {!! Form::open(['route' => ['packSubscriptions.destroy', $packSubscription->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('packSubscriptions.show', [$packSubscription->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('packSubscriptions.edit', [$packSubscription->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>