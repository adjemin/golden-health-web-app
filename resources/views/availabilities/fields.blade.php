<!-- Day Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('day_time', 'Day Time:') !!}
    {!! Form::text('day_time', null, ['class' => 'form-control','id'=>'day_time']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#day_time').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Coach Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coach_id', 'Coach Id:') !!}
    {!! Form::select('coach_id', $coachItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('availabilities.index') }}" class="btn btn-secondary">Cancel</a>
</div>
