<!-- Date Debut Field -->
<div class="form-group">
    {!! Form::label('date_debut', 'Date Debut:') !!}
    <p>{{ $availability->date_debut }}</p>
</div>

<!-- Date Fin Field -->
<div class="form-group">
    {!! Form::label('date_fin', 'Date Fin:') !!}
    <p>{{ $availability->date_fin }}</p>
</div>

<!-- Day Time Field -->
<div class="form-group">
    {!! Form::label('day_time', 'Day Time:') !!}
    <p>{{ $availability->day_time }}</p>
</div>

<!-- Coach Id Field -->
<div class="form-group">
    {!! Form::label('coach_id', 'Coach Id:') !!}
    <p>{{ $availability->coach_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $availability->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $availability->updated_at }}</p>
</div>

