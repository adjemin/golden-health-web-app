<!-- Coach Id Field -->
<div class="form-group">
    {!! Form::label('coach_id', 'Coach Id:') !!}
    <p>{{ $coachDiscipline->coach_id }}</p>
</div>

<!-- Discipline Id Field -->
<div class="form-group">
    {!! Form::label('discipline_id', 'Discipline Id:') !!}
    <p>{{ $coachDiscipline->discipline_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $coachDiscipline->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $coachDiscipline->updated_at }}</p>
</div>

