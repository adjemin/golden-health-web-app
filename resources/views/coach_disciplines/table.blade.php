<div class="table-responsive-sm">
    <table class="table table-striped" id="coachDisciplines-table">
        <thead>
            <tr>
                <th>Coach Id</th>
        <th>Discipline Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($coachDisciplines as $coachDiscipline)
            <tr>
                <td>{{ $coachDiscipline->coach_id }}</td>
            <td>{{ $coachDiscipline->discipline_id }}</td>
                <td>
                    {!! Form::open(['route' => ['coachDisciplines.destroy', $coachDiscipline->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('coachDisciplines.show', [$coachDiscipline->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('coachDisciplines.edit', [$coachDiscipline->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>