<!-- Coach Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coach_id', 'Coach Id:') !!}
    {!! Form::select('coach_id', $coachItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Discipline Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('discipline_id', 'Discipline Id:') !!}
    {!! Form::select('discipline_id', $disciplineItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('coachDisciplines.index') }}" class="btn btn-secondary">Cancel</a>
</div>
