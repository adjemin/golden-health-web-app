@extends('admin_backoffice.layouts.master')

@section('title')
    Dashboard | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">accessibility_new</i>
                            </div>
                            <p class="card-category">Coachs</p>
                            <h3 class="card-title">{{ $countCoach }}
                                {{-- <small>GB</small> --}}
                            </h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons text-warning">visibility</i>
                                <a href="{{ route('coaches.index') }}">Voir plus...</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-success card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">person</i>
                            </div>
                            <p class="card-category">Utilisateurs</p>
                            <h3 class="card-title">{{ $countCustomers }}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons text-success">visibility</i>
                                <a href="{{ route('customers.index') }}">Voir plus...</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-info card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">sports</i>
                            </div>
                            <p class="card-category">Disciplines</p>
                            <h3 class="card-title">{{ $countDisciplines }}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons text-info">visibility</i>
                                <a href="{{ route('disciplines.index') }}">Voir plus...</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">event</i>
                            </div>
                            <p class="card-category">Events</p>
                            <h3 class="card-title">{{ $countEvents }}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons text-primary">visibility</i>
                                <a href="{{ route('events.index') }}">Voir plus...</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--<div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Utilisateurs</h4>
                        </div>
                        <div class="card-body">
                            <div class="chart chart-sm">
                                <canvas id="chartjs-dashboard-line"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Commandes</h4>
                            <p class="card-category"> Toutes les commandes</p>
                            <div class="col text-right">
                                <a class="pull-right btn btn-sm btn-light" href="{{ route('orders.create') }}">Ajouter une
                                    commande</a>
                            </div>
                        </div>
                        <div class="card-body">
                            @include('orders.table')
                        </div>
                        <div class="mt-3 d-flex justify-content-center">
                            {{ $orders->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('scripts')
    {{-- Chart-js dashboard line --}}
    {{--<script>
        $(function() {
            var ctx = document.getElementById('chartjs-dashboard-line').getContext("2d");
            var gradient = ctx.createLinearGradient(0, 0, 0, 225);
            gradient.addColorStop(0, 'rgba(215, 227, 244, 1)');
            gradient.addColorStop(1, 'rgba(215, 227, 244, 0)');
            // Line chart
            new Chart(document.getElementById("chartjs-dashboard-line"), {
                type: 'line',
                data: {
                    // labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                    
                    labels: @json($months),
                    datasets: [{
                        label: "Utilisateurs",
                        fill: true,
                        backgroundColor: gradient,
                        // borderColor: window.theme.primary,
                        data: @json($amountData)
                        
                        // data: [
                        // 	2115,
                        // 	1562,
                        // 	1584,
                        // 	1892,
                        // 	1587,
                        // 	1923,
                        // 	2566,
                        // 	2448,
                        // 	2805,
                        // 	3438,
                        // 	2917,
                        // 	3327
                        // ]
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    tooltips: {
                        intersect: false
                    },
                    hover: {
                        intersect: true
                    },
                    plugins: {
                        filler: {
                            propagate: false
                        }
                    },
                    scales: {
                        xAxes: [{
                            reverse: true,
                            gridLines: {
                                color: "rgba(0,0,0,0.0)"
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                stepSize: 10000
                            },
                            display: true,
                            borderDash: [3, 3],
                            gridLines: {
                                color: "rgba(0,0,0,0.0)"
                            }
                        }]
                    }
                }
            });
        });

    </script>--}}
@endsection
