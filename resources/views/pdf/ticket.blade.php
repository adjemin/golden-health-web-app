
@php
use SimpleSoftwareIO\QrCode\Facades\QrCode;
@endphp

<!DOCTYPE html>
<html>
<head>
  <title>HTML to API - event tickets</title>
  <link href='https://fonts.googleapis.com/css?family=Lobster|Kreon:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ asset('customer/css/responsive.css') }} ">
    <link rel="stylesheet" href="{{ asset('customer/css/style.css') }} ">

  <!-- <link rel="stylesheet" href="styles/main.css" media="screen" charset="utf-8"/> -->
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>

<style>

  
</style>

<body class="pt-5">
  <div class="container mt-5">
    <section>
      <div class="circle">
        <div class="event">{{$ticket->event->title}}</div>
        <div class="title">{{$ticket->customer->name}}</div>
      </div>
      <div class="special"></div>
      <div class="special"></div>
      <div class="special"></div>
      <div class="special">
        <div class="seats">
          {{-- <span class="label">section</span><span>A</span> --}}
        </div>
      </div>
      <div class="special">
        <div class="seats">
          {{-- <span class="label">row</span><span>13</span> --}}
        </div>
        <div class="text-uppercase">
            {{-- SATURDAY, AUGUST 25 2020 --}}
            {{$ticket->event->date_event}}
        </div>
      </div>
      <div class="special">
        <div class="seats">
          {{-- <span class="label">seat</span><span>120</span> --}}
        </div>
        <div class="text-uppercase">
            {{-- THE PLAZA, NEW YORK --}}
            {{$ticket->venue}}
        </div>
      </div>
    </section>

    <aside>
        <figure>
        {{-- // Insert generated qr code here ! --}}
            {{QrCode::size(280)->format('svg')->generate($ticket->code)}}
        </figure>
        {{-- <a onclick="print();" class="btn btn-info ml-auto">Imprimer</a> --}}
    </aside>
  </div>
</body>

</html>
