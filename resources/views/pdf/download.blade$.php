@inject('date', '\App\Utils\Date')
@inject('others', '\App\Utils\QRCodeGenerator')
<!doctype html>
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel='stylesheet' id='bootstrap-css'  href='<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/css/bootstrap.min.css" integrity="sha384-SI27wrMjH3ZZ89r4o+fGIJtnzkAnFs3E4qz9DIYioCQ5l9Rd/7UAa8DHcaL8jkWt" crossorigin="anonymous">' type='text/css' media='all' />
    <title>Ticket</title>
</head>
<body>
    <style>
    *{
        box-sizing: border-box;
    }
    body{
        border: 1px solid rgb(225, 225, 225);
    }
    .card{
        
        padding:10px;
    }

    #event_cover{
        object-fit:cover;
        width: 100%;
        height: auto;
        background-size: 100% 100%;
    }

    #btn_download{
        background-color: rgb(245, 63, 123);
        color: white; 
    }

    #code{
        object-fit:cover;
        width:100%;
        height: auto;
        background-size: 100% 100%;
    }
    </style>

    <div class="card">
        <div class="card-body">                                
            <div class="row">
                <div class="col-md-6">
                    <img id="event_cover" src="{{asset(''.$ticket->event->cover_url)}}" alt="">
                </div>
                <br>
                <div class="col-md-12">
                    <span class="card-title">
                        {{$ticket->event->title}}
                    </span>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-6">
                    <small class="card-text">
                        {{ !is_null($ticket->event->venue) ? $ticket->event->venue : ''}}
                        <br>
                        <span>{{ $date->get_day($ticket->event->date_event, 'fr')}} {{ $date->get_day_number($ticket->event->date_event)}} {{$date->get_month($ticket->event->date_event, 'fr')}} {{$date->get_year($ticket->event->date_event) }}</span>
                    </small>
                    <br>
                    <br>
                    <small class="text-muted">
                        <strong> Référence: </strong> #{{$ticket->code}}.
                        <br>
                        <strong> Quantité: </strong> @if(!is_null($ticket->event->entry_free)){{$ticket->seats_count.' x '.$ticket->event->entry_free.' CFA'}}. @else {{$ticket->seats_count.' x 0 CFA'}}. @endif
                        <br>
                        <strong> Prix total: </strong>@if(!is_null($ticket->event->entry_free)) {{$ticket->amount.' CFA'}}. @else Gratuit @endif
                        <br>
                        <strong> Client: </strong>  {{ strtoupper($ticket->customer->name) }}
                        <br>
                        <strong> Date: </strong>  {{ $date->get_day($ticket->event->date_event, 'fr')}} {{ $date->get_day_number($ticket->event->date_event)}} {{$date->get_month($ticket->event->date_event, 'fr')}} {{$date->get_year($ticket->event->date_event) }} @if(!is_null($ticket->event->date_event_end) && ($ticket->event->date_event_end != $ticket->event->date_event)) au {{ $date->get_day($ticket->event->date_event_end, 'fr')}} {{ $date->get_day_number($ticket->event->date_event_end)}} {{$date->get_month($ticket->event->date_event_end, 'fr')}} {{$date->get_year($ticket->event->date_event_end) }} @endif à {{ $ticket->event->start_time }}
                        <br>
                    </small>
                    <br>
                    <br>
                    <hr style="background-color: rgb(225, 225, 225); color: rgb(225, 225, 225);">
                    <br>
                    <br>
                    <div style="float:right; width:150px; height:100px;">
                        <img id="code" src="data:image/png;base64,{{ $others->qrcode($ticket->code, 'png')}}" alt="qrcode">
                    </div>
                    <p class="text-muted" style="float:left;">
                        <small style="font-size: 10px;">Powered by {{config('app.name')}}</small>
                    </p>
                </div>               
            </div>
            
            <div class="row">
                       
            </div>
        </div>
    </div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/js/bootstrap.min.js" integrity="sha384-3qaqj0lc6sV/qpzrc1N5DC6i1VRn/HyX4qdPaiEFbn54VjQBEU341pvjz7Dv3n6P" crossorigin="anonymous"></script></body>
</html>
