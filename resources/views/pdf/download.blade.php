@inject('date', '\App\Utils\Date')
@inject('others', '\App\Utils\QRCodeGenerator')
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Golden Health - {{$ticket->event->title}}</title>
    <link rel="stylesheet" href="{{ asset('customer/css/bootstrap.min.css') }} ">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{asset('invoices/invoice.css')}}" rel="stylesheet">
    <style>
        *{
            box-sizing: border-box;
        }
        body{
            border: 1px solid rgb(225, 225, 225);
        }
        .card{
            
            padding:10px;
        }

        #event_cover{
            object-fit:cover;
            width: 100%;
            height: auto;
            background-size: 100% 100%;
        }

        #btn_download{
            background-color: rgb(245, 63, 123);
            color: white; 
        }

        #code{
            object-fit:cover;
            width:100%;
            height: auto;
            background-size: 100% 100%;
        }
    </style>
</head>
<body>

    <div class="container-fluid invoice-container">
            <div class="row invoice-header">
                <div class="invoice-col"  style="float:left; text-align:left">
                  <p><img src="https://res.cloudinary.com/ahoko/image/upload/v1600908196/gh1_vsk8g1.png" title="Golden" /></p>
                  <h3>Ticket # {{$ticket->code}}</h3>
                  <p>{{ !is_null($ticket->event->venue) ? $ticket->event->venue : ''}}</p>
                </div>
                <div class="invoice-col text-center">
                    <div class="invoice-status">
                        <div style="float:right; width:150px; height:100px;">
                            bases
                            {!! base64_decode($others->qrcode($ticket->code, 'svg')) !!}
                            {{-- <img id="code" src="data:image/svg;base64,{{ $others->qrcode($ticket->code, 'svg')}}" alt="qrcode"> --}}
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="invoice-col right">
                <strong>Evenement : </strong><br>
                    <span class="small-text">
                        {{$ticket->event->title}}
                    </span>
                </div>
                <div class="invoice-col" style="float:right; text-align:right">
                    <strong>Date</strong><br>
                    <span class="small-text">
                    {{ $date->get_day($ticket->event->date_event, 'fr')}} {{ $date->get_day_number($ticket->event->date_event)}} {{$date->get_month($ticket->event->date_event, 'fr')}} {{$date->get_year($ticket->event->date_event) }} @if(!is_null($ticket->event->date_event_end) && ($ticket->event->date_event_end != $ticket->event->date_event)) au {{ $date->get_day($ticket->event->date_event_end, 'fr')}} {{ $date->get_day_number($ticket->event->date_event_end)}} {{$date->get_month($ticket->event->date_event_end, 'fr')}} {{$date->get_year($ticket->event->date_event_end) }} @endif à {{ $ticket->event->start_time }}<br><br>
                    </span>
                </div>
            </div>
            <br />
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Détails</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <td><strong>Description</strong></td>
                                    <td width="20%" class="text-center"><strong>Amount</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Acheter le {{ $date->get_day($ticket->created_at, 'fr')}} {{ $date->get_day_number($ticket->created_at)}} {{$date->get_month($ticket->created_at, 'fr')}} {{$date->get_year($ticket->created_at) }}<span class="float-right"> {!! $ticket->statusText() !!} {{ $ticket->seats_count.' place(s)' }}</span></td>
                                    <td class="text-center">{!! $ticket->amountText() !!}</td>
                                </tr>
                                
                                                                <tr>
                                    <td class="total-row text-right"><strong>Sub Total</strong></td>
                                    <td class="total-row text-center">{!! $ticket->amountText() !!}</td>
                                </tr>

                                <tr>
                                    <td class="total-row text-right"><strong>Total</strong></td>
                                    <td class="total-row text-center">{!! $ticket->amountText() !!}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="pull-right btn-group btn-group-sm hidden-print">
                <a href="javascript:window.print()" class="btn btn-primary"><i class="fas fa-print"></i> Print</a>
            </div>


    </div>
    <p class="text-center hidden-print"><a href="/">&laquo; Back to Client Area</a></a></p>
</body>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/js/bootstrap.min.js" integrity="sha384-3qaqj0lc6sV/qpzrc1N5DC6i1VRn/HyX4qdPaiEFbn54VjQBEU341pvjz7Dv3n6P" crossorigin="anonymous"></script></body>
{{--
<script src="{{asset('customer/js/jquery.min.js') }}"></script>
<script src="{{ asset('customer/js/popper.min.js') }}"></script>
<script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
--}}
</html>
