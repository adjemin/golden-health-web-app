<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('date_event', 'Date de debut:') !!}
            @if(isset($event))
                {!! Form::date('date_event', \Carbon\Carbon::parse($event->date_event)->format('Y-m-d') ?? null, ['class' => 'form-control', 'id' => 'date_event','value' => 0]) !!}
            @else
                {!! Form::date('date_event', null, ['class' => 'form-control', 'id' => 'date_event','value' => 0]) !!}
            @endif
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('date_event_end', 'Date de fin:') !!}
            @if(isset($event))
                {!! Form::date('date_event_end', \Carbon\Carbon::parse($event->date_event_end)->format('Y-m-d') ?? null, ['class' => 'form-control', 'id' =>'date_event_end']) !!}
            @else
            {!! Form::date('date_event_end', null, ['class' => 'form-control', 'id' =>'date_event_end']) !!}
            @endif
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('start_time', 'Heure de début:') !!}
            {!! Form::time('start_time', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('end_time', 'Heure de fin:') !!}
            {!! Form::time('end_time', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<!-- Title Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div> --}}

<!-- Date Event Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('date_event', 'Date Event:') !!}
    {!! Form::text('date_event', null, ['class' => 'form-control', 'id' => 'date_event']) !!}
</div> --}}

@push('scripts')
    <script type="text/javascript">
        $('#date_event').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            icons: {
                up: "icon-arrow-up-circle icons font-2xl",
                down: "icon-arrow-down-circle icons font-2xl"
            },
            sideBySide: true
        })

        $('#date_event_end').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            icons: {
                up: "icon-arrow-up-circle icons font-2xl",
                down: "icon-arrow-down-circle icons font-2xl"
            },
            sideBySide: true
        });
    </script>
@endpush


<!-- Place Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('venue', 'Lieu:') !!}
    {!! Form::text('venue', null, ['class' => 'form-control']) !!}
</div> --}}

<!-- Description Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div> --}}
<div class="row mt-3">
    <div class="col-md-6" id="is_free">
        <div class="form-group">
            {!! Form::label('is_free', 'Est gratuit :') !!}
            <select name="is_free" id="is_free" onchange="showPrice(this.value)">
                @if (isset($event))
                    {!! $event->entry_fee == 0
                    ? '<option value="OUI" selected>OUI</option>
                    <option value="NON">NON</option>'
                    : '<option value="OUI">OUI</option>
                    <option value="NON" selected>NON</option>' !!}
                @else
                    <option value="OUI" selected>OUI</option>
                    <option value="NON">NON</option>
                @endif
            </select>
        </div>
    </div>
    @if (isset($event))
        {!! $event->entry_fee == 0
        ? '<div class="col-md-6" id="entry_free" style="display:none;">'
            : '<div class="col-md-6" id="entry_free">' !!}
            @else
                <div class="col-md-6" id="entry_free" style="display:none;">
    @endif
    <div class="form-group">
        {!! Form::label('entry_fee', 'Prix :') !!}
        {!! Form::number('entry_fee', null, ['class' => 'form-control', 'min' => 0, 'value' => 0]) !!}
    </div>
</div>

{{--<div class="col-md-6">
    <div class="form-group">
        {!! Form::label('entry_fee', 'Type Event :') !!}
        <select name="event_type_id" id="event_type_id">
            @foreach ($event_types as $event_type)
                <option value="{{ $event_type->id }}"> {{ $event_type->name }} </option>
            @endforeach
        </select>
    </div>
</div>--}}
</div>


<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('venue', 'Lieu:') !!}
            {!! Form::text('venue', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('place_number', 'Nombre de place:') !!}
            {!! Form::number('place_number', null, ['class' => 'form-control', 'min' => 0, 'value' => 0]) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {{-- {!! Form::label('description', 'Description:') !!}
            --}}
            {!! Form::text('description', null, ['id' => 'description', 'class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            <strong>{!! Form::label('cover_url', 'Images:') !!}</strong>
            {!! Form::file('cover_url', ['class' => 'form-control', 'onchange' => 'loadFile(event)']) !!}
            {{-- <br><small>Cliquez sur "Images" pour uploader vos medias</small>
            --}}
        </div>
    </div>
</div>
@if ($event ?? '')
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <img src="{{ $event->cover_url }}" style="width: 50%; height: 50%; border: none" />
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class="form-group">
                <img style="width: 50%; height: 50%; border: none" id="output" />
            </div>
        </div>
    </div>
@endif
<div class="row mt-3">
    {{--<div class="col-md-6">
        <div class="form-group">
            <input type="checkbox" name="is_free" value="0">
        </div>
    </div>--}}


</div>

<script>
    var loadFile = function(event) {
        var reader = new FileReader();
        reader.onload = function() {
            var output = document.getElementById('output');
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };

    function showPrice(val) {
        if (val == 'NON') {
            document.getElementById("entry_free").style.display = 'block';
        } else {
            document.getElementById("entry_free").style.display = 'none';
        }
    }

</script>

<!-- Place Number Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('place_number', 'Place Number:') !!}
    {!! Form::text('place_number', null, ['class' => 'form-control']) !!}
</div> --}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('events.index') }}" class="btn btn-secondary">Annuler</a>
</div>
