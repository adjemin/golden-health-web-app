{{-- @extends('layouts.app')

@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ route('events.index') }}">Event</a>
    </li>
    <li class="breadcrumb-item active">Detail</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        @include('coreui-templates::common.errors')
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Details</strong>
                        <a href="{{ route('events.index') }}" class="btn btn-light">Back</a>
                    </div>
                    <div class="card-body">
                        @include('events.show_fields')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}


@extends('admin_backoffice.layouts.master')

@section('title')
    Event | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Détails event</h4>
                            <p class="card-category">Information event</p>
                        </div>
                        <div class="card-body">
                            {{-- <div class="row"> --}}
                                @include('events.show_fields')
                                <a href="{{ route('events.edit', [$event->id]) }}"
                                    class="btn btn-primary pull-right">Modifier</a>
                                {{-- </div> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <a href="javascript:;">
                                <img class="img" src="{{ $event->cover_url ?? asset('customer/images/IMAGE.png') }}" />
                            </a>
                        </div>
                        <div class="card-body">
                            <h6 class="card-category text-gray">{{ $event->title }}</h6>
                            {{-- <p class="card-description">
                                {{ $event->bio }}
                            </p> --}}
                            <a href="{{ route('events.index') }}" class="btn btn-primary btn-round">Retour</a>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Ticket acheter <span class="badge badge-info">{{$event->participants()->count()}}</span></h4>
                            <p class="card-category">Liste des tickets</p>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="customers-table">
                                    <thead class="text-primary">
                                        <tr>
                                            <th>Code ticket</th>
                                            <th>Nbre places</th>
                                            <th>Prix</th>
                                            <th>Status du ticket</th>
                                            <th>Nom</th>
                                            <th>Civilité</th>
                                            <th>Email</th>
                                            <th>Contact</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($event->participants as $customer)
                                            @php
                                                $eventTicket = \App\EventTicket::where('customer_id', $customer->id)->where('event_id', $event->id)->first();
                                            @endphp
                                            <tr>
                                                <td>{{ $eventTicket->code  ?? '' }}</td>
                                                <td>{{ $eventTicket->seats_count ?? '1'  }}</td>
                                                <td>{{ $eventTicket->amount  ?? '' }}</td>
                                                <td>{!! $eventTicket->statusText() !!}</td>
                                                <td>{{ $customer->name }}</td>
                                                <td>{{ $customer->gender }}</td>
                                                <td>{{ $customer->email }}</td>
                                                <td>{{ $customer->phone }}</td>
                                                <td>{{ $customer->type_account }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
