<div class="table-responsive">
    <table class="table printTable" id="events-table">
        <thead class="text-primary">
            <tr>
                <th>Titre</th>
                <th>Date</th>
                <th>Lieu</th>
                <th>Nombre de place</th>
                {{--<th>Description</th>--}}
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($events as $event)
                <tr>
                    <td>{{ $event->title }}</td>
                    <td>{{ $event->date_event }}</td>
                    <td>{{ $event->venue }}</td>
                    <td>{{ (int) $event->place_number - (int) $event->participants->count() }}</td>
                    {{--<td>{{ Str::limit($event->description,  100) }}</td>--}}
                    <td>
                        {!! Form::open(['route' => ['events.destroy', $event->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('events.show', [$event->id]) }}" class='btn btn-ghost-success'><i
                                    class="fa fa-eye"></i></a>
                            <a href="{{ route('events.edit', [$event->id]) }}" class='btn btn-ghost-info'><i
                                    class="fa fa-edit"></i></a>
                            {!! Form::button('<i class="fa fa-trash"></i>', [
                            'type' => 'submit',
                            'class' => 'btn
                            btn-ghost-danger',
                            'onclick' => "return confirm('Are you sure?')",
                            ]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
