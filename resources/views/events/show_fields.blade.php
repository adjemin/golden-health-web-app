<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            <p>{{ $event->title }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('date_event', 'Date de debut:') !!}
            <p>{{ $event->date_event }}</p>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('date_event_end', 'Date de fin:') !!}
            <p>{{ $event->date_event_end ?? '' }}</p>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('start_time', 'Heure de début:') !!}
            <p>{{ $event->start_time ?? '' }}</p>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('end_time', 'Heure de fin:') !!}
            <p>{{ $event->end_time ?? '' }}</p>
        </div>
    </div>
</div>
{{--
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $event->title }}</p>
</div> --}}

{{--
<!-- Date Event Field -->
<div class="form-group">
    {!! Form::label('date_event', 'Date Event:') !!}
    <p>{{ $event->date_event }}</p>
</div> --}}

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('venue', 'Lieu:') !!}
            <p>{{ $event->venue }}</p>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('place_number', 'Nombre initiale de Place:') !!}
            <p>{{ $event->place_number }}</p>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('place_number', 'Place restante:') !!}
            <p>{{ (int) $event->place_number - (int) $event->participants->count()}}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('entry_fee', 'Prix :') !!}
            <p>{!! $event->showEntryFees() !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            <p>{!! $event->description !!}</p>
        </div>
    </div>
</div>

{{--
<!-- Place Field -->
<div class="form-group">
    {!! Form::label('place', 'Place:') !!}
    <p>{{ $event->place }}</p>
</div> --}}

{{-- <!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $event->description }}</p>
</div> --}}

{{-- <!-- Place Number Field -->
<div class="form-group">
    {!! Form::label('place_number', 'Place Number:') !!}
    <p>{{ $event->place_number }}</p>
</div> --}}

{{-- <!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $event->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $event->updated_at }}</p>
</div> --}}

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('created_at', 'Date de création:') !!}
            <p>{{ $event->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('updated_at', 'Date de modification:') !!}
            <p>{{ $event->updated_at }}</p>
        </div>
    </div>
</div>
