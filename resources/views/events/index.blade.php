{{-- @extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Events</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            @include('flash::message')
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>
                            Events
                            <a class="pull-right" href="{{ route('events.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                        </div>
                        <div class="card-body">
                            @include('events.table')
                            <div class="pull-right mr-3">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

--}}


@extends('admin_backoffice.layouts.master')

@section('title')
    Events | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Event</h4>
                            <p class="card-category"> Tous les events</p>
                            <div class="col text-right">
                                <a class="pull-right btn btn-sm btn-light" href="{{ route('events.create') }}">Ajouter un event</a>
                            </div>
                        </div>
                        <div class="card-body" id="dynamic">
                            @include('events.table')
                        </div>
                        <div class="mt-3 d-flex justify-content-center" id="link">
                            {{ $events->links() }}
                        </div>
                        <div class="float-right">
                            <a href="/event/download/all" class="btn btn-success mr-1" >Exporter tableau complet</a>
                            <button id="print" class="btn btn-info">Exporter tableau actuel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        $('body').on('keyup', '#searchEvent', function() {
            // alert('ok')
            var searchEvent = $(this).val();
            $.ajax({
                method: 'POST',
                url: "{{ route('searchEvent') }}",
                dataType: "json",
                data: {
                    '_token': '{{ csrf_token() }}',
                    searchEvent: searchEvent
                },
                success: function(res) {
                    console.log(res);
                    $("#dynamic").html(res);
                    $("#link").html('');
                }
            })
        });
    </script>
@endsection