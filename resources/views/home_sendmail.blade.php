@extends('admin_backoffice.layouts.master')

@section('title')
    Message | {{ config('app.name') }}
@endsection
 
@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="clearfix"></div>

                @include('flash::message')
                <div class="clearfix"></div>

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Créer un message personnalisé</h4>
                            <p class="card-category">Envoyer des message aux utilisateurs</p>
                        </div>
                        <div class="card-body">
                            {!! Form::open(['route' => 'sendMailPost', 'files' => true]) !!}
                                <div class="row">
                                    <h5 class="px-3 py-2">Contenu du message</h5>
                                </div>
                                <div class="row py-1">
                                    <div class="col-6">
                                        <div class="form-group">
                                            {!! Form::label('subject', 'Sujet :') !!}
                                            {!! Form::text('subject', old('subject'), ['class' => 'form-control', 'required' =>  'required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            {!! Form::label('greeting', 'Salutations :') !!}
                                            {!! Form::text('greeting', old('greeting'), ['class' => 'form-control', 'required' =>  'required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            {!! Form::label('thanks', 'Remerciements :') !!}
                                            {!! Form::text('thanks', old('thanks'), ['class' => 'form-control', 'required' =>  'required']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            {!! Form::textarea('body', old('body'), ['class' => 'form-control', 'placeholder' => 'message', 'id' => 'description']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <h5 class="px-3">Ajouter une action</h5>
                                    <small>Cette partie est falcultative</small> <br>
                                    <div class="clearfix"></div><br>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            {!! Form::label('action_url', 'Action url:') !!}
                                            {!! Form::text('action_url', old('action_url'), ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            {!! Form::label('action_message', 'Action Message:') !!}
                                            {!! Form::text('action_message', old('action_message'), ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <h5 class="px-3">Ajouter des medias</h5>
                                    <small> Cette partie est falcultative</small><br>
                                    <div class="clearfix"></div><br>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('medias', 'Images:') !!}
                                            {!! Form::file('medias[]', ['class' => 'form-control', 'onchange' => 'loadFile(this)', 'multiple'=>true,'accept'=>'image/*']) !!}
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-12 gallery" style="max-width: 80% !important;"></div>
                                <div class="form-group col-sm-12">
                                    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
                                    <!-- <button type="button" class="btn btn-primary">Enregistrer</button> -->
                                    <a href="{{ route('products.index') }}" class="btn btn-secondary">Annuler</a>
                                </div>
                                <script>
                                    var loadFile = function(input){
                                        if (input.files) {
                                            var filesAmount = input.files.length;
                                            for (i = 0; i < filesAmount; i++) {
                                                var reader = new FileReader();
                                                reader.onload = function(event) {
                                                    $($.parseHTML('<img>')).attr('src', event.target.result).attr('height', 80).appendTo('.gallery');
                                                }
                                                reader.readAsDataURL(input.files[i]);
                                            }
                                        }
                                    } 
                                </script>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection