{{-- @extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('disciplines.index') !!}">Discipline</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Discipline</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'disciplines.store', 'files' => true]) !!}

                                   @include('disciplines.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
 --}}


 @extends('admin_backoffice.layouts.master')

 @section('title')
     Discipline | {{ config('app.name') }}
 @endsection

 @section('content')
     <div class="container-fluid">
         <div class="animated fadeIn">
             <div class="row">
                 <div class="col-md-12">
                     <div class="card">
                         <div class="card-header card-header-primary">
                             <h4 class="card-title">Créer une discipline</h4>
                             <p class="card-category">Information discipline</p>
                         </div>
                         <div class="card-body">
                             {!! Form::open(['route' => 'disciplines.store', 'files' => true]) !!}

                             @include('disciplines.fields')

                             {!! Form::close() !!}


                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 @endsection
