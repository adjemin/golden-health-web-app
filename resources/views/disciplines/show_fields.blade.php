
<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('name', 'Nom:') !!}
            <p>{{ $discipline->name }}</p>
        </div>
    </div>
    <div class="col-md-6">

        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            <p>{{ $discipline->slug }}</p>
        </div>
    </div>
</div>

<!-- Name Field -->
{{-- <div class="form-group">
    {!! Form::label('name', 'Name:') !!}

</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{{ $discipline->slug }}</p>
</div> --}}
<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            <p>{!! $discipline->description !!}</p>
        </div>
    </div>
</div>



{{-- <!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $discipline->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $discipline->updated_at }}</p>
</div> --}}

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('created_at', 'Date de création:') !!}
            <p>{{ $discipline->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('updated_at', 'Date de modification:') !!}
            <p>{{ $discipline->updated_at }}</p>
        </div>
    </div>
</div>

