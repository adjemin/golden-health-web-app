<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('name', 'Nom:') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'onkeyup' => 'stringToSlug(this.value)']) !!}
        </div>
    </div>
    <div class="col-md-6">

        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control', 'readonly']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {{--{!! Form::label('description', 'Description:')
            !!}--}}
            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
        </div>
    </div>
    <div class="col-md-6">

        <div class="form-group">
            {!! Form::label('image', 'Image:') !!}

            {{--{!! Form::text('image', null, ['class' => 'form-control'])
            !!}--}}
            {!! Form::file('image', ['class' => 'form-control', 'onchange' => 'loadFile(event)']) !!}
            {{-- <br><small>Cliquez sur "Image" pour uploader vos medias</small>
            --}}
        </div>
    </div>
</div>

@if ($discipline ?? '')
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <img src="{{ $discipline->image }}" style="width: 50%; height: 50%; border: none" />
            </div>
        </div>
    </div>

@else
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class="form-group">
                <img style="width: 50%; height: 50%; border: none" id="output" />
            </div>
        </div>
    </div>
@endif



{{--
<!-- Name Field -->
<div class="form-group col-sm-6">

</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">

</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">

</div> --}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('disciplines.index') }}" class="btn btn-secondary">Annuler</a>
</div>

<script>
    function stringToSlug(str) {

        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        // return str;
        document.getElementById("slug").value = str;
    }

    var loadFile = function(event) {
        var reader = new FileReader();
        reader.onload = function() {
            var output = document.getElementById('output');
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };

</script>
