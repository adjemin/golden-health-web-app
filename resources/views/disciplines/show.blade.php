{{-- @extends('layouts.app')

@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ route('disciplines.index') }}">Discipline</a>
    </li>
    <li class="breadcrumb-item active">Detail</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        @include('coreui-templates::common.errors')
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Details</strong>
                        <a href="{{ route('disciplines.index') }}" class="btn btn-light">Back</a>
                    </div>
                    <div class="card-body">
                        @include('disciplines.show_fields')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}


@extends('admin_backoffice.layouts.master')

@section('title')
    Discipline | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Détails discipline</h4>
                            <p class="card-category">Information discipline</p>
                        </div>
                        <div class="card-body">
                            {{-- <div class="row"> --}}
                                @include('disciplines.show_fields')
                                <a href="{{ route('disciplines.edit', [$discipline->id]) }}"
                                    class="btn btn-primary pull-right">Modifier</a>

                                {{-- </div> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <a href="javascript:;">
                                <img class="img" src="{{ $discipline->image }}" height="90"/>
                            </a>
                        </div>
                        <div class="card-body">
                            <h6 class="card-category text-gray">{{ $discipline->name }}</h6>
                            <p class="card-description">
                                {!! $discipline->description !!}
                            </p>
                            <a href="{{ route('disciplines.index') }}" class="btn btn-primary btn-round">Retour</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
