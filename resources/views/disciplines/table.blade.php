<div class="table-responsive">
    <table class="table" id="disciplines-table">
        <thead class="text-primary">
            <tr>
                <th>Image</th>
                <th>Nom</th>
                <th>Slug</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($disciplines as $discipline)
                <tr>
                    <td><img src="{{ $discipline->image }}" alt="{{ $discipline->slug }}" width="90"></td>
                    <td>{{ $discipline->name }}</td>
                    <td>{{ $discipline->slug }}</td>
                    {{--<td>{{ Str::limit($discipline->description, 50) }}</td>--}}
                    <td>
                        {{-- {!! Form::open(['route' => ['disciplines.destroy',
                        $discipline->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('disciplines.show', [$discipline->id]) }}"
                                class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                            <a href="{{ route('disciplines.edit', [$discipline->id]) }}" class='btn btn-ghost-info'><i
                                    class="fa fa-edit"></i></a>
                            {!! Form::button('<i class="fa fa-trash"></i>', [
                            'type' => 'submit',
                            'class' => 'btn
                            btn-ghost-danger',
                            'onclick' => "return confirm('Are you sure?')",
                            ]) !!}
                        </div>
                        {!! Form::close() !!} --}}

                        {!! Form::open(['route' => ['disciplines.destroy', $discipline->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('disciplines.show', [$discipline->id]) }}"
                                class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                            <a href="{{ route('disciplines.edit', [$discipline->id]) }}" class='btn btn-ghost-info'><i
                                    class="fa fa-edit"></i></a>
                            {!! Form::button('<i class="fa fa-trash"></i>', [
                            'type' => 'submit',
                            'class' => 'btn
                            btn-ghost-danger',
                            'onclick' => "return confirm('Are you sure?')",
                            ]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
