    <header id="sticky-div">
        <div class="container-fluid py-3 bg-primary text-white">
            <div class="row align-items-center">
                <div class="col-md-6 col-12 text-center text-md-left">
                    Téléchargez notre application pour plus de fonctionnalités
                </div>
                <div class="col-md-6 col-12 mt-2 mt-md-0 download-btn d-flex justify-content-end">
                    <a href="{{env('APP_GOOGLE_PLAY')}}" target="_blank" class="btn btn-black">
                        <span class="d-flex align-items-center justify-content-start">
                            <img src="{{asset('img/icon/google-play.png')}}" class="icon mr-2" width="20px" alt="">
                            Téléchargez sur <br>
                            Google Play
                        </span>
                    </a>
                    <a href="{{env('APP_APPSTORE')}}" target="_blank" class="btn btn-black ml-2">
                        <span class="d-flex align-items-center justify-content-start">
                            <i class="fa fa-apple mr-2" aria-hidden="true"></i>
                            Téléchargez sur <br>
                            Apple Store
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </header>

    @include('layouts.inc.ui_script')
