<style>
#preloader {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: #fff;
}
.loader{
    position: relative;
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #2DC068;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite; /* Safari */
    animation: spin 2s linear infinite;
}

.loader img{
    position: relative;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 15px;
    display: block;
    height: auto;
}

/* Safari */
@-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}
</style>

<div id="preloader">
    <div class="loader">
        <img src="{{('img/logo.png')}}" alt="">
    </div>
</div>

<script>
    window.onload = function(){
        document.querySelector("#preloader").style.display = "none";
    }
</script>
