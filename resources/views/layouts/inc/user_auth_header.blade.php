<div class="d-md-block d-flex flex-column">
    @if(!auth()->guard('customer')->check())
        <a href="{{route('register')}}" class="btn btn-outline-secondary">
            <i class="fa fa-user mr-2 d-none d-md-inline" aria-hidden="true"></i>
            Inscription
        </a>
        <a href="{{route('login')}}" class="btn btn-primary mt-2 mt-md-0">
            <i class="fa fa-sign-in mr-2 d-none d-md-inline" aria-hidden="true"></i>
            Connexion
        </a>
    @else
        <div class="mr-0 mr-md-4 d-flex flex-column flex-md-row align-items-center justify-content-between">
            <div class="mr-2" style="width:50px;height:50px; border-radius:50%; overflow:hidden; display:inline-block; background-color:#2DC068;">
                <img src="{{Auth::guard('customer')->user()->photo_url}}" alt="">
            </div>
            {{Auth::guard('customer')->user()->name}}
            <a href="{{url('/logout')}}" class="btn btn-primary mt-2 mt-md-0 ml-md-3">
                <i class="fa fa-sign-out mr-2 d-none d-md-inline" aria-hidden="true"></i>
                Deconnexion
            </a>
        </div>
    @endif
</div>
