
<!-- Modal -->
<div class="modal fade" id="whatsappGroupModal" tabindex="-1" role="dialog" aria-labelledby="whatsappGroupModalLabel" aria-hidden="true" style="z-index: 8888;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{-- <h5 class="modal-title text-secondary font-weight-bold" id="whatsappGroupModalLabel">Rejoignez-nous !</h5> --}}
                <button type="button" class="close text-secondary" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="" class="bg-pale text-primary text-center">
                <div class="">
                    <img src="{{asset('img/500_.jpeg')}}" alt="" class="img-fluid">
                </div>
                <div id="position-relative" class="container px-md-4 px-2">
                    <div class="text">
                        <!-- <h5>Regain control</h5> -->
                        <h4 class="mt-2">
                            Rejoins le groupe whatsapp des <span class="text500">Ambassadeurs</span>
                        </h4>
                        {{-- <h2 class="font-weight-bold text-center">
                            Téléchargement gratuit sur :
                        </h2> --}}
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex flex-column flex-md-row justify-content-between">
                <a href="{{env('WHATSAPP_GROUP_URL')}}" class="btn btn-secondary mx-auto mt-2 text-white text-capitalize text500">
                    Avec plaisir !
                </a>
                <a href="#" class="btn btn-primary mx-auto mt-2 ml-2" data-dismiss="modal"aria-label="Close">
                    Non, merci.
                </a>
            </div>
        </div>
    </div>
</div>
