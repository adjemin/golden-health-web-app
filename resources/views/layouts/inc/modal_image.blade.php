<div id="modalImage" class="modal-image">
    <span class="modal-image-close">&times;</span>
    <img class="modal-image-content" id="modal-image-img">
    <div id="modal-image-caption"></div>
</div>
<script>
// Get the modal
    var modalImage = document.getElementById("modalImage");

    // Get the image and insert it inside the modal - use its "alt" text as a caption

    var img = document.getElementById("main-cover");
    var modalImg = document.getElementById("modal-image-img");
    var captionText = document.getElementById("modal-image-caption");
    img.onclick = function(){
        modalImage.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var spanImage = document.getElementsByClassName("modal-image-close")[0];

    // When the user clicks on <span> (x), close the modal
    spanImage.onclick = function() {
        modalImage.style.display = "none";
    }
</script>
