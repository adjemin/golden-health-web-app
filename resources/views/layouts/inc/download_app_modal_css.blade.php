<style>
    .modalDialog {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background: rgba(0, 0, 0, 0.8);
        z-index: 99999;
        /* opacity:0; */
        -webkit-transition: opacity 100ms ease-in;
        -moz-transition: opacity 100ms ease-in;
        transition: opacity 100ms ease-in;
        pointer-events: none;
    }
    .modalDialog:target {
        opacity:1;
        pointer-events: auto;
    }
    .modalDialog > div {
        width: 35%;
        position: relative;
        margin: 5% auto;
        /* padding: 15px; */
        border-radius: 8px;
        background: #fff;
    }

    .close {
        font-family: Arial, Helvetica, sans-serif;
        background: #043E57;
        color: #fff;
        line-height: 25px;
        position: absolute;
        font-size: small;
        right: -12px;
        text-align: center;
        top: -10px;
        width: 34px;
        height: 34px;
        text-decoration: none;
        font-weight: bold;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        border-radius: 50%;
        -moz-box-shadow: 1px 1px 3px #000;
        -webkit-box-shadow: 1px 1px 3px #000;
        box-shadow: 1px 1px 3px #000;
        padding-top: 5px;
    }
    .close:hover {
        background: #fff;
    }
    .facturationModal {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background: rgba(0, 0, 0, 0.8);
        z-index: 99999;
        opacity:0;
        -webkit-transition: opacity 100ms ease-in;
        -moz-transition: opacity 100ms ease-in;
        transition: opacity 100ms ease-in;
        pointer-events: none;
    }
    .facturationModal:target {
        opacity:1;
        pointer-events: auto;
    }
    .facturationModal > div {
        width: 50%;
        position: relative;
        margin: 2% auto;
        /* padding: 15px; */
        border-radius: 8px;
        background: #fff;
    }
</style>
