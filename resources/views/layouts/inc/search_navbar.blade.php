    <nav class="">
        <div class="container-fluid d-flex justify-content-between align-items-center py-1">
            <div class="d-flex align-items-center justify-content-between">
                <a href="/"><img src="{{asset('img/logo.png')}}" width="80px" alt=""></a>
                <!-- Search -->
                <div class="justify-content-center ml-md-5 d-none d-md-flex">
                    <form action="/recherche" class="row justify-content-center d-md-flex d-none">
                        <div class="col-12">
                            <div class="search-input">
                                <div class="input d-flex align-items-center mr-md-5">
                                    <!-- <i class="fa fa-search text-secondary mx-2" aria-hidden="true"></i> -->
                                    <img src="{{asset('img/icon/search@2x.png')}}" width="20px" class="mx-2 mr-3" alt="">
                                    <input type="text" class="" placeholder="{!! session('search') ?? 'Rechercher' !!}">
                                </div>
                                <input type="submit"  class="btn btn-primary px-2 py-1 font-weight-bold" value="Rechercher">
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            @if(\Auth::guard('customer')->check())
                <div class="d-flex flex-column flex-md-row align-items-center justify-content-between">
                    <a href="{{route('register')}}" class="btn btn-outline-secondary  ml-0 ml-md-2">
                        <i class="fa fa-user mr-2" aria-hidden="true"></i>
                        Inscription
                    </a>
                    <a href="{{route('login')}}" class="btn btn-primary mt-2 ml-0 ml-md-2 mt-md-0">
                        <i class="fa fa-sign-in mr-2" aria-hidden="true"></i>
                        Connexion
                    </a>
                </div>
            {{--@else
                <div class="mr-0 mr-md-4 d-flex flex-column flex-md-row align-items-center justify-content-between">
                    <div class="mr-2" style="width:50px;height:50px; border-radius:50%; overflow:hidden; display:inline-block; background-color:#2DC068;">
                        <img src="{{Auth::guard('customer')->user()->photo_url}}" alt="">
                    </div>
                    {{Auth::guard('customer')->user()->name}}
                    <a href="{{url('/logout')}}" class="btn btn-primary mt-2 mt-md-0 ml-md-3">
                        <i class="fa fa-sign-out mr-2 d-none d-md-inline" aria-hidden="true"></i>
                        Deconnexion
                    </a>
                </div>--}}
            @endif
        </div>
    </nav>
