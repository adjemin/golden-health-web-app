
    <!--modals-->
    <div id="openDownloadModal" class="facturationModal">
        <div>
            <a href="#close" title="Close" class="close">X</a>
            <div id="" class="bg-pale text-primary text-center p-5">
            <div id="position-relative" class="container px-4">
                <div class="brand">
                    <img src="{{asset('img/people_.png')}}" alt="" class="img-fluid">
                </div>
                <img src="{{asset('img/sprite_up.png')}}" alt="" class="sprite_up">
                <img src="{{asset('img/sprite_down.png')}}" alt="" class="sprite_down">
                <div class="text">
                    <!-- <h5>Regain control</h5> -->
                    <h4 class="">
                        Rejoignez la communauté Adjemin
                    </h4>
                    <h2 class="font-weight-bold">
                        Téléchargement gratuit sur :
                    </h2>
                    <div class="d-flex flex-sm-row flex-column align-items-center justify-content-between">
                        <a href="" class="btn btn-black mx-auto mt-2">
                            <span class="d-flex align-items-center justify-content-start">
                                <img src="{{asset('img/icon/google-play.png')}}" class="icon mr-2" width="24px" alt="">
                                Téléchargez sur <br>
                                Google Play
                            </span>
                        </a>
                        <a href="" class="btn btn-black mx-auto mt-2">
                            <span class="d-flex align-items-center justify-content-start">
                                <i class="fa fa-apple mr-2" aria-hidden="true"></i>
                                Téléchargez sur <br>
                                Apple Store
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div> <!-- /fixed area -->
        </div>
    </div>
