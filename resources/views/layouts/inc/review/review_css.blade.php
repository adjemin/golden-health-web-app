<style>
    /* Color radio inputs */
    input[type=radio] {
        display: none;
    }
    input[type=radio]:checked + label span {
        transform: scale(1.25);
        color: var(--secondary) !important;
    }
    input[type=radio]:checked + label .red {
        border: 2px solid #fff;
    }
    label {
        display: inline-block;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        margin-right: 10px;
        cursor: pointer;
    }
    label:hover span {
        transform: scale(1.25);
        color: var(--secondary) !important;
    }
    label:hover,
    label:hover ~ span {
        transform: scale(1.25);
        color: var(--secondary) !important;
    }
    label span {
        display: block;
        border-radius: 50%;
        width: 100%;
        height: 100%;
        transition: transform 0.2s ease-in-out;
    }
    .rating-text span{
        display: none;
    }
</style>
