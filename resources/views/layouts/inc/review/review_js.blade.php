<script>
    function setRating(rating){

        var selectedRatingInput = $("#rating_"+rating);
        // get parent span // then span siblings
        var spans = selectedRatingInput.parent('span').prevAll();

        // then each input label childs of those siblings
        spans.each(function(index){
            $(this).children().each(function(i){
                $(this).children().css('color', "#edaa0d");
                $(this).children().css('transform', "scale(1.25)");
            });
        });
        var spans = selectedRatingInput.parent('span').nextAll().each(function(index){
            $(this).children().each(function(i){
                $(this).children().css('color', "black");
                $(this).children().css('transform', "scale(1)");
            });
        });
        $("#message").html("");
    }
</script>
