
    <div class="row">
        <span class="rating-stars">
            <span>
                <input class="radio-rating" id="rating_1" type="radio" name="rating" value="1"/>
                    <label for="rating_1" onclick="setRating('1');">
                        <span class="span-rating rating_1">
                            <i class="material-icons">star</i>
                        </span>
                    </label>
            </span>
            <span>
                <input class="radio-rating" id="rating_2" type="radio" name="rating" value="2"/>
                    <label for="rating_2" onclick="setRating('2');">
                        <span class="span-rating rating_2">
                            <i class="material-icons">star</i>
                        </span>
                    </label>
            </span>
            <span>
                <input class="radio-rating" id="rating_3" type="radio" name="rating" value="3"/>
                    <label for="rating_3" onclick="setRating('3');">
                        <span class="span-rating rating_3">
                            <i class="material-icons">star</i>
                        </span>
                    </label>
            </span>
            <span>
                <input class="radio-rating" id="rating_4" type="radio" name="rating" value="4"/>
                    <label for="rating_4" onclick="setRating('4');">
                        <span class="span-rating rating_4">
                            <i class="material-icons">star</i>
                        </span>
                    </label>
            </span>
            <span>
                <input class="radio-rating" id="rating_5" type="radio" name="rating" value="5"/>
                    <label for="rating_5" onclick="setRating('5');">
                        <span class="span-rating rating_5">
                            <i class="material-icons">star</i>
                        </span>
                    </label>
            </span>
        </span>
        <span class="rating-text fw-600 ml-2">
            <span>Je déteste !</span>
            <span>Je n'aime pas.</span>
            <span>J'ai un avis mitigé</span>
            <span>J'aime.</span>
            <span>J'adore !</span>
        </span>
    </div>