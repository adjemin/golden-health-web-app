
<!-- Modal -->
<div class="modal fade" id="downloadModal" tabindex="-1" role="dialog" aria-labelledby="downloadModalLabel" aria-hidden="true" style="z-index: 7777;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{-- <h5 class="modal-title text-secondary font-weight-bold" id="downloadModalLabel">Rejoignez-nous !</h5> --}}
                <button type="button" class="close text-secondary" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="" class="bg-pale text-primary text-center p-md-5">
                <div id="position-relative" class="container px-md-4 px-2">
                    <div class="brand">
                        <img src="{{asset('img/people_.png')}}" alt="" class="img-fluid">
                    </div>
                    <div class="text">
                        <!-- <h5>Regain control</h5> -->
                        <h4 class="">
                            Rejoignez la communauté Adjemin
                        </h4>
                        <h2 class="font-weight-bold text-center">
                            Téléchargement gratuit sur :
                        </h2>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex flex-column flex-md-row justify-content-between">
                <a href="{{env('APP_GOOGLE_PLAY')}}" target="_blank" class="btn btn-black mx-auto mt-2">
                    <span class="d-flex align-items-center justify-content-start">
                        <img src="{{asset('img/icon/google-play.png')}}" class="icon mr-2" width="24px" alt="">
                        Téléchargez sur <br>
                        Google Play
                    </span>
                </a>
                <a href="{{env('APP_APPSTORE')}}" target="_blank" class="btn btn-black mx-auto mt-2 ml-2">
                    <span class="d-flex align-items-center justify-content-start">
                        <i class="fa fa-apple mr-2" aria-hidden="true"></i>
                        Téléchargez sur <br>
                        Apple Store
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>
