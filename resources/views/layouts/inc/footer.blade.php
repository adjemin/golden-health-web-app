    <footer id="footer" class="text-white pb-5 pt-5 bg-secondary-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-5 pb-5">
                    <div class="footer-desc-text">
                        <a href="/" class="text-white">
                            <!-- <h2 class="title pb-2 pt-5 pt-md-0">AdjeminPay</h2> -->
                            <img src="{{asset('img/logo.png')}}" alt="" width="100px">
                        </a>
                        <p>
                            {{env('APP_DESCRIPTION')}}
                        </p>
                    </div>
                    <div class="copyrights mt-4 d-none d-md-block">
                        © 2020
                        <a href="/" class="inline-link">
                            {{env('APP_NAME')}}
                        </a>
                        . Tous Droits Réservés.
                        <span>
                            Powered by <a href="{{env('ADJEMIN_URL')}}" class ="text-secondary fw-500">Adjemin</a>
                        </span>
                    </div>
                </div>
                <div class="col-md pl-md-4 mt-md-5 pb-5 d-flex flex-column justify-content-between">
                    <div class="links mb-4 mb-md-0">
                        <h4 class="title pb-2 text-secondary">A propos</h4>
                        <a  @include('layouts.inc.modal_btn')  class="text-link">Qui sommes-nous ?</a>
                        <a  @include('layouts.inc.modal_btn')  class="text-link">Nos services</a>
                        <a  @include('layouts.inc.modal_btn')  class="text-link">Nos Réalisations</a>
                        <a  @include('layouts.inc.modal_btn')  class="text-link">Partenaires</a>
                        <a href="{{route('become-ambassador')}}"  class="text-link">Devenir Ambassadeur</a>
                        <a href="/jobs"  class="text-link mb-3">Travailler à Adjemin</a>
                    </div>
                    <div class="d-flex justify-content-between align-items-end pr-md-5">
                        <a target="_blank" href="{{env('APP_FACEBOOK')}}" class="inline-link"><img src="{{asset('img/index/fb.png')}}" alt="" class="img-fluid"
                                width="30px"></a>
                        <a target="_blank" href="{{env('APP_LINKEDIN')}}" class="inline-link"><img src="{{asset('img/index/in.png')}}" alt="" class="img-fluid"
                                width="30px"></a>
                        <a target="_blank" href="{{env('APP_TWITTER')}}" class="inline-link"><img src="{{asset('img/index/tw.png')}}" alt="" class="img-fluid"
                                width="30px"></a>
                    </div>
                </div>
                <div class="col-md  mt-md-5">
                    <h4 class="title pb-2 text-secondary">Ressources</h4>
                    <a  @include('layouts.inc.modal_btn')  class="text-link">
                        Comment ça marche
                    </a>
                    <a href="{{ route('terms')}}" class="text-link">
                        Conditions d’utilisation
                    </a>
                    <a href="{{ route('terms')}}" class="text-link">
                        Politique de confidentialité
                    </a>
                    <a  @include('layouts.inc.modal_btn')   class="text-link">
                        Contact
                    </a>
                </div>
            </div> <!-- /row -->
            <div class="copyrights mt-5 d-md-none d-block">
                © 2020
                <a href="/" class="inline-link" title="{{env('APP_DEVS')}}">
                    {{env('APP_NAME')}}
                </a>
                . Tous Droits Réservés.
                <span>
                    Powered by <a href="{{env('ADJEMIN_URL')}}" class ="text-secondary fw-500">Adjemin</a>
                </span>
            </div>
        </div>
    </footer>
