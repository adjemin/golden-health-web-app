
<!-- Modal -->
<a id="join500ModalBtn" data-toggle="modal" data-target="#join500Modal" style="display: none;"></a>

<div class="modal fade" id="join500Modal" tabindex="-1" role="dialog" aria-labelledby="join500ModalLabel" aria-hidden="true" style="z-index: 9999;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{-- <h5 class="modal-title text-secondary font-weight-bold" id="join500ModalLabel">Rejoignez-nous !</h5> --}}
                <button type="button" class="close text-secondary" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="" class="bg-pale text-primary text-center">
                <div class="">
                    <img src="{{asset('img/500.jpg')}}" alt="" class="img-fluid">
                </div>
                <div id="position-relative" class="container px-md-4 px-2">
                    <div class="text">
                        <!-- <h5>Regain control</h5> -->
                        <h4 class="mt-2">
                            Deviens <span class="text500">Ambassadeur</span> de Adjemin
                        </h4>
                        {{-- <h2 class="font-weight-bold text-center">
                            Téléchargement gratuit sur :
                        </h2> --}}
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex flex-column flex-md-row justify-content-between">
                <a href="{{ route('become-an-ambassador')}}" class="btn btn-secondary mx-auto mt-2 text-white text-capitalize text500">
                    Oui, je veux bien
                </a>
                <a href="#" class="btn btn-primary mx-auto mt-2 ml-2" data-dismiss="modal" aria-label="Close">
                    Non, merci.
                </a>
            </div>
        </div>
    </div>
</div>

<script>
    // document.onload() = function(){
        setTimeout(() => {
            document.getElementById("join500ModalBtn").click();
            // document.getElementById("join500Modal").classList.add("show");
            // document.getElementById("downloadModal").click();

            // window.location.assign("{{env('WHATSAPP_GROUP_URL')}}");
        }, 60*2000); // 2 mins
    // }
</script>
