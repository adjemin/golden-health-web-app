<li class="nav-item {{ Request::is('coaches*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('coaches.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Coaches</span>
    </a>
</li>
<li class="nav-item {{ Request::is('customers*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('customers.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Customers</span>
    </a>
</li>
<li class="nav-item {{ Request::is('languages*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('languages.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Languages</span>
    </a>
</li>
<li class="nav-item {{ Request::is('availabilities*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('availabilities.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Availabilities</span>
    </a>
</li>
<li class="nav-item {{ Request::is('disciplines*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('disciplines.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Disciplines</span>
    </a>
</li>
<li class="nav-item {{ Request::is('coachDisciplines*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('coachDisciplines.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Coach Disciplines</span>
    </a>
</li>
<li class="nav-item {{ Request::is('packs*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('packs.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Packs</span>
    </a>
</li>
<li class="nav-item {{ Request::is('packSubscriptions*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('packSubscriptions.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Pack Subscriptions</span>
    </a>
</li>
<li class="nav-item {{ Request::is('portfolios*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('portfolios.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Portfolios</span>
    </a>
</li>
<li class="nav-item {{ Request::is('events*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('events.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Events</span>
    </a>
</li>
<li class="nav-item {{ Request::is('orders*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orders.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Orders</span>
    </a>
</li>
<li class="nav-item {{ Request::is('bodyMassIndices*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('bodyMassIndices.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Body Mass Indices</span>
    </a>
</li>
<li class="nav-item {{ Request::is('recapBMIs*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('recapBMIs.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Recap B M Is</span>
    </a>
</li>
<li class="nav-item {{ Request::is('addresses*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('addresses.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Addresses</span>
    </a>
</li>
<li class="nav-item {{ Request::is('coupons*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('coupons.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Coupons</span>
    </a>
</li>
<li class="nav-item {{ Request::is('invoices*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('invoices.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Invoices</span>
    </a>
</li>
<li class="nav-item {{ Request::is('courses*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('courses.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Courses</span>
    </a>
</li>
<li class="nav-item {{ Request::is('services*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('services.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Services</span>
    </a>
</li>
<li class="nav-item {{ Request::is('products*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('products.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Products</span>
    </a>
</li>
<li class="nav-item {{ Request::is('experiences*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('experiences.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Experiences</span>
    </a>
</li>
<li class="nav-item {{ Request::is('qualifications*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('qualifications.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Qualifications</span>
    </a>
</li>
<li class="nav-item {{ Request::is('qualificationTypes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('qualificationTypes.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Qualification Types</span>
    </a>
</li>
<li class="nav-item {{ Request::is('invoicePayments*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('invoicePayments.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Invoice Payments</span>
    </a>
</li>
<li class="nav-item {{ Request::is('notes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('notes.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Notes</span>
    </a>
</li>
<li class="nav-item {{ Request::is('advice*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('advice.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Advice</span>
    </a>
</li>
<li class="nav-item {{ Request::is('objectives*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('objectives.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Objectives</span>
    </a>
</li>
<li class="nav-item {{ Request::is('seances*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('seances.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Seances</span>
    </a>
</li>
<li class="nav-item {{ Request::is('howDidYouKnows*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('howDidYouKnows.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>How Did You Knows</span>
    </a>
</li>
<li class="nav-item {{ Request::is('partners*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('partners.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Partners</span>
    </a>
</li>
<li class="nav-item {{ Request::is('connexions*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('connexions.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Connexions</span>
    </a>
</li>
<li class="nav-item {{ Request::is('categories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('categories.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Categories</span>
    </a>
</li>
<li class="nav-item {{ Request::is('posts*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('posts.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Posts</span>
    </a>
</li>
<li class="nav-item {{ Request::is('orderItems*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orderItems.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Order Items</span>
    </a>
</li>
<li class="nav-item {{ Request::is('courseDetails*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('courseDetails.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Course Details</span>
    </a>
</li>
<li class="nav-item {{ Request::is('courseLieus*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('courseLieus.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Course Lieus</span>
    </a>
</li>
<li class="nav-item {{ Request::is('lieuCourses*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('lieuCourses.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Lieu Courses</span>
    </a>
</li>
<li class="nav-item {{ Request::is('courseObjectives*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('courseObjectives.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Course Objectives</span>
    </a>
</li>
<li class="nav-item {{ Request::is('bookingCoaches*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('bookingCoaches.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Booking Coaches</span>
    </a>
</li>
<li class="nav-item {{ Request::is('productCategories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('productCategories.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Product Categories</span>
    </a>
</li>
<li class="nav-item {{ Request::is('adverts*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('adverts.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Adverts</span>
    </a>
</li>
<li class="nav-item {{ Request::is('distinctions*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('distinctions.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Distinctions</span>
    </a>
</li>
<li class="nav-item {{ Request::is('videotheques*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('videotheques.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Videotheques</span>
    </a>
</li>


<li class="nav-item {{ Request::is('clientTestimonies*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('clientTestimonies.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Client Testimonies</span>
    </a>
</li>
<li class="nav-item {{ Request::is('programs*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('programs.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Programs</span>
    </a>
</li>
<li class="nav-item {{ Request::is('slides*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('slides.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Slides</span>
<li class="nav-item {{ Request::is('diplomes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('diplomes.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Diplomes</span>
    </a>
</li>
<li class="nav-item {{ Request::is('customerDevices*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('customerDevices.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Customer Devices</span>
    </a>
</li>
<li class="nav-item {{ Request::is('customerNotifications*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('customerNotifications.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Customer Notifications</span>
    </a>
</li>


<li class="nav-item {{ Request::is('reviews*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('reviews.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Reviews</span>
    </a>
</li>
<li class="nav-item {{ Request::is('villes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('villes.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Villes</span>
    </a>
</li>
