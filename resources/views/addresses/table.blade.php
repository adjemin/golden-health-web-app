<div class="table-responsive-sm">
    <table class="table table-striped" id="addresses-table">
        <thead>
            <tr>
                <th>Longitude</th>
        <th>Latitude</th>
        <th>Address Name</th>
        <th>Customer Id</th>
        <th>Coach Id</th>
        <th>Place</th>
        <th>Type Building</th>
        <th>Photo</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($addresses as $address)
            <tr>
                <td>{{ $address->longitude }}</td>
            <td>{{ $address->latitude }}</td>
            <td>{{ $address->address_name }}</td>
            <td>{{ $address->customer_id }}</td>
            <td>{{ $address->coach_id }}</td>
            <td>{{ $address->place }}</td>
            <td>{{ $address->type_building }}</td>
            <td>{{ $address->photo }}</td>
                <td>
                    {!! Form::open(['route' => ['addresses.destroy', $address->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('addresses.show', [$address->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('addresses.edit', [$address->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>