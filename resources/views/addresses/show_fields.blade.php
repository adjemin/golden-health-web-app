<!-- Longitude Field -->
<div class="form-group">
    {!! Form::label('longitude', 'Longitude:') !!}
    <p>{{ $address->longitude }}</p>
</div>

<!-- Latitude Field -->
<div class="form-group">
    {!! Form::label('latitude', 'Latitude:') !!}
    <p>{{ $address->latitude }}</p>
</div>

<!-- Address Name Field -->
<div class="form-group">
    {!! Form::label('address_name', 'Address Name:') !!}
    <p>{{ $address->address_name }}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{{ $address->customer_id }}</p>
</div>

<!-- Coach Id Field -->
<div class="form-group">
    {!! Form::label('coach_id', 'Coach Id:') !!}
    <p>{{ $address->coach_id }}</p>
</div>

<!-- Place Field -->
<div class="form-group">
    {!! Form::label('place', 'Place:') !!}
    <p>{{ $address->place }}</p>
</div>

<!-- Type Building Field -->
<div class="form-group">
    {!! Form::label('type_building', 'Type Building:') !!}
    <p>{{ $address->type_building }}</p>
</div>

<!-- Photo Field -->
<div class="form-group">
    {!! Form::label('photo', 'Photo:') !!}
    <p>{{ $address->photo }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $address->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $address->updated_at }}</p>
</div>

