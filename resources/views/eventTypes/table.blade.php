<div class="table-responsive">
    <table class="table" id="disciplines-table">
        <thead class="text-primary">
            <tr>
                <th>Nom</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($eventTypes as $eventType)
                <tr>
                    <td>{{ $eventType->name }}</td>
                    <td>
                        {!! Form::open(['route' => ['eventTypes.destroy', $eventType->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('eventTypes.show', [$eventType->id]) }}"
                                class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                            <a href="{{ route('eventTypes.edit', [$eventType->id]) }}" class='btn btn-ghost-info'><i
                                    class="fa fa-edit"></i></a>
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', onclick' => "return confirm('Êtes vous sûr de vouloir ?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
