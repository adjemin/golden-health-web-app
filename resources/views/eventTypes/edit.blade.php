@extends('admin_backoffice.layouts.master')

@section('title')
    Event Type | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Modifier un event type</h4>
                            <p class="card-category">Information event type</p>
                        </div>
                        <div class="card-body">
                            {!! Form::model($eventType, ['route' => ['eventTypes.update', $eventTypes->id], 'method' => 'patch']) !!}

                              @include('eventTypes.fields')

                              {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
