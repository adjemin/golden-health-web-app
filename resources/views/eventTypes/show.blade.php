{{-- @extends('layouts.app')

@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ route('disciplines.index') }}">Discipline</a>
    </li>
    <li class="breadcrumb-item active">Detail</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        @include('coreui-templates::common.errors')
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Details</strong>
                        <a href="{{ route('disciplines.index') }}" class="btn btn-light">Back</a>
                    </div>
                    <div class="card-body">
                        @include('disciplines.show_fields')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}


@extends('admin_backoffice.layouts.master')

@section('title')
    Event Type | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Détails event type</h4>
                            <p class="card-category">Information event type</p>
                        </div>
                        <div class="card-body">
                            @include('eventTypes.show_fields')
                            <a href="{{ route('eventTypes.edit', [$eventType->id]) }}"
                                class="btn btn-primary pull-right">Modifier</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
