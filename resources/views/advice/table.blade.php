<div class="table-responsive-sm">
    <table class="table table-striped" id="advice-table">
        <thead>
            <tr>
                <th>Customer Id</th>
        <th>Coach Id</th>
        <th>Message</th>
        <th>Rate</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($advice as $advice)
            <tr>
                <td>{{ $advice->customer_id }}</td>
            <td>{{ $advice->coach_id }}</td>
            <td>{{ $advice->message }}</td>
            <td>{{ $advice->rate }}</td>
                <td>
                    {!! Form::open(['route' => ['advice.destroy', $advice->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('advice.show', [$advice->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('advice.edit', [$advice->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>