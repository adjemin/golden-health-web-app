
 @extends('admin_backoffice.layouts.master')

 @section('title')
     Produit | {{ config('app.name') }}
 @endsection
 
 @section('content')
     <div class="container-fluid">
         <div class="animated fadeIn">
             <div class="row">
               
                 <div class="col-md-12">
                    @include('flash::message')
                    <div class="clearfix"></div>
                     <div class="card">
                         <div class="card-header card-header-primary">
                             <h4 class="card-title">Créer une réservation de produit</h4>
                             <p class="card-category">Information de la réservation</p>
                         </div>
                         <div class="card-body">
                            @include('coreui-templates::common.errors')
                             {!! Form::open(['route' => 'product-reservation.store']) !!}
 
                             @include('product_reservation.fields')
 
                             {!! Form::close() !!}
 
 
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 @endsection
  