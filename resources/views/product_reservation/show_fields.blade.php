{{--
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $product->title }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $product->description !!}</p>
</div> --}}

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            <p>{{ $product->title }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            <p>{{ $product->slug }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            <p>{!! $product->description !!}</p>
        </div>
    </div>
</div>

{{--
<!-- Description Metadata Field -->
<div class="form-group">
    {!! Form::label('description_metadata', 'Description Metadata:') !!}
    <p>{{ $product->description_metadata }}</p>
</div> --}}

{{--
<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $product->price }}</p>
</div>

<!-- Original Price Field -->
<div class="form-group">
    {!! Form::label('original_price', 'Original Price:') !!}
    <p>{{ $product->original_price }}</p>
</div>

<!-- Fees Field -->
<div class="form-group">
    {!! Form::label('fees', 'Fees:') !!}
    <p>{{ $product->fees }}</p>
</div>

<!-- Currency Slug Field -->
<div class="form-group">
    {!! Form::label('currency_slug', 'Currency Slug:') !!}
    <p>{{ $product->currency_slug }}</p>
</div> --}}
<div class="row mt-3">
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('price', 'Prix final:') !!}
            <p>{{ $product->price }}</p>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('original_price', 'Prix de base:') !!}
            <p>{{ $product->original_price }}</p>
        </div>
    </div>
    {{--<div class="col-md-3">
        <div class="form-group">
            {!! Form::label('fees', 'Frais de transport:') !!}
            <p>{{ $product->fees }}</p>
        </div>
    </div>--}}
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('currency_slug', 'Devise:') !!}
            <p>{{ $product->currency_slug }}</p>
        </div>
    </div>
</div>
{{--
<!-- Old Price Field -->
<div class="form-group">
    {!! Form::label('old_price', 'Old Price:') !!}
    <p>{{ $product->old_price }}</p>
</div>

<!-- Has Promo Field -->
<div class="form-group">
    {!! Form::label('has_promo', 'Has Promo:') !!}
    <p>{{ $product->has_promo }}</p>
</div>

<!-- Promo Percentage Field -->
<div class="form-group">
    {!! Form::label('promo_percentage', 'Promo Percentage:') !!}
    <p>{{ $product->promo_percentage }}</p>
</div> --}}

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('has_promo', 'Est en promo?') !!}
            <p>{{ $product->has_promo === 1 ? 'OUI' : 'NON' }}</p>
        </div>
    </div>
    <div class="col-md-6">

        <div class="form-group">
            {!! Form::label('promo_percentage', 'Réduction:') !!}
            <p>{{ $product->promo_percentage }}%</p>
        </div>
    </div>
</div>

<!-- Medias Field -->
<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('medias', 'Medias:') !!}
            {!! $product->getImagesWidget() !!}
            {{-- @if(count(json_decode($product->getImageUrl())) > 1 )
                @foreach(json_decode($product->getImageUrl()) as $image)
                    <img class="img-fluid zoomable" src="{{ $image }}" height="90" />
                @endforeach
            @else
                <img class="img-fluid zoomable" src="{{ $product->getImageUrl() }}" height="90" />
            @endif --}}
        </div>
    </div>
</div>

<!-- Couleur Field -->
<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('colors', 'Couleurs:') !!}
            <div class="row">
                @foreach ($product->getProductColor() as $color)
                    {{-- <p>{{ $color->name }}</p> --}}
                    <div class="col" style="background-color:{{ $color->code_color }};">
                        {{ $color->name }}
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<!-- Tailles Field -->
<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('sizes', 'Tailles:') !!}
            <p>{{ $product->sizes }}</p>
        </div>
    </div>
</div>

{{--
<!-- Sold At Field --><div class="form-group">
    {!!  Form::label('sold_at', 'Sold At:') !!}
    <p>{{$product->sold_at}}</p>
</div> --}}

<div class=" row mt-3">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('initial_count', 'Quatité disponible:') !!}
                        <p>{{ $product->initial_count > 0 ? $product->initial_count : 0 }}</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('is_sold', 'Est disponible?') !!}
                        <p>{{ $product->is_sold == 1 ? 'OUI' : 'NON' }}</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('sold_at', 'Date cloture de vente:') !!}
                        <p>{{ $product->sold_at }}</p>
                    </div>
                </div>
            </div>

            <!-- Initiale Count Field -->






            <div class="row mt-3">
                <div class="col-md-6">
                    <!-- Published At Field -->
                    <div class="form-group">
                        {!! Form::label('published_at', 'Published At:') !!}
                        <p>{{ $product->published_at }}</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- Is Published Field -->
                    <div class="form-group">
                        {!! Form::label('is_published', 'Est publié?') !!}
                        <p>{{ $product->is_published == 1 ? 'OUI' : 'NON' }}</p>
                    </div>
                </div>
            </div>


            {{--
            <!-- Slug Field -->
            <div class="form-group">
                {!! Form::label('slug', 'Slug:') !!}
                <p>{{ $product->slug }}</p>
            </div> --}}

            <!-- Link Field -->
            <div class="row mt-3">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('link', 'Link:') !!}
                        <p>{{ $product->link }}</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('gender', 'Genre:') !!}
                        <p>{{ $product->gender }}</p>
                    </div>
                </div>
            </div>

            <!-- Link Field -->
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="form-group">
                        {!! Form::label('product_categories', 'Categories du produit: ') !!}
                        <p>
                            @foreach ($product->categories as $categorie)
                                {{ $categorie->categorie->name }}
                                @if (!$loop->last)
                                    -
                                @endif
                            @endforeach
                        </p>
                    </div>
                </div>
            </div>

            <!-- Delivery Services Field -->
            {{-- <div class="form-group">
                {!! Form::label('delivery_services', 'Delivery Services:') !!}
                <p>{{ $product->delivery_services }}</p>
            </div> --}}

            <div class="row mt-3">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('created_at', 'Created At:') !!}
                        <p>{{ $product->created_at }}</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('updated_at', 'Updated At:') !!}
                        <p>{{ $product->updated_at }}</p>
                    </div>
                </div>
            </div>
