<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            <label for="produit">Produit</label>
            <select name="product_id" class="form-control" id="produit" required>
                <option value="">Selectionner ...</option>
               
                @if (isset($reservation))
                    @foreach ($products as $product)
                        @if ($reservation->product_id == $product->id)
                            <option selected value="{{ $product->id }}">{{ $product->title }}</option>
                        @else
                            <option value="{{ $product->id }}">{{ $product->title }}</option>
                        @endif
                    @endforeach
                    
                @else
                    @foreach ($products as $product)
                        <option value="{{ $product->id }}">{{ $product->title }}</option>
                    @endforeach
                @endif
                   
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for='customer_name'> Nom du client:</label>
            <input type='text' name='customer_name' id='customer_name' value="{{ $reservation->customer_name ?? '' }}" class='form-control' required>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for='customer_email'>Email du client:</label>
            <input type='text' name='customer_email' id='customer_email' value="{{ $reservation->customer_email??'' }}"class='form-control' required>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for='customer_phone'>Contact du client:</label>
            <input type='text' name='customer_phone' id='customer_phone' value="{{ $reservation->customer_phone??'' }}" class='form-control' />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for='product_quantity'>Quanté réservée:</label>
            <input type='number' name='product_quantity' id='product_quantity' id='product_quantity' value="{{ $reservation->product_quantity??'1' }}" class='form-control'>
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('product-reservation.index') }}" class="btn btn-secondary">Retour</a>
</div>

<script>
    function stringToSlug(str) {

        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        // return str;
        document.getElementById("slug").value = str;
    }

    function showPercentage(val){
        if(val == 1){
            document.getElementById("div_percentage").style.display= 'block';
        }else{
            document.getElementById("div_percentage").style.display= 'none';
        }
    }

    function showSold(val){
        if(val == 1){
            document.getElementById("div_sold").style.display= 'block';
        }else{
            document.getElementById("div_sold").style.display= 'none';
        }
    }
</script>
