<div class="table-responsive">
    <table class="table printTable" id="products-table">
        <thead class="text-primary">
            <tr>
                <th>Produit</th>
                <th>Nom</th>
                <th>Prix</th>
                <th>Quantité</th>
                <th>Nom client</th>
                <th>Email</th>
                <th>Telephone</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($reservations as $reservation)
                <tr>
                    <td>
                        @if (!empty($reservation->product->getImageUrl()))
                            <img src="{{ json_decode($reservation->product->getImageUrl())[0] ?? $reservation->product->getImageUrl() }}" alt="{{ $reservation->product->title }}" width="90">
                        @endif
                    </td>
                    <td>{{ $reservation->product->title }}</td>

                    <td>{{ $reservation->product->price }} {{ $reservation->product->currency_slug }}</td>

                    <td>{{ $reservation->product_quantity }}</td>
                    <td>{{ $reservation->customer_name }}</td>
                    <td>{{ $reservation->customer_email }}</td>
                    <td>{{ $reservation->customer_phone }}</td>

                    
                    <td>
                        {!! Form::open(['route' => ['product-reservation.destroy', $reservation->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            
                            <a href="{{ route('product-reservation.edit', [$reservation->id]) }}" class='btn btn-ghost-info'><i
                                    class="fa fa-edit"></i></a>
                            {!! Form::button('<i class="fa fa-trash"></i>', [
                            'type' => 'submit',
                            'class' => 'btn
                            btn-ghost-danger',
                            'onclick' => "return confirm('Are you sure?')",
                            ]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
