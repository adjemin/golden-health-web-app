

@extends('admin_backoffice.layouts.master')

@section('title')
    Produit | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Modifier une réservation de produit</h4>
                            <p class="card-category">Information de la réservation</p>
                        </div>
                        <div class="card-body">
                            @include('coreui-templates::common.errors')
                            {!! Form::model($reservation, ['route' => ['product-reservation.update', $reservation->id], 'method' => 'patch']) !!}

                              @include('product_reservation.fields')

                              {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection