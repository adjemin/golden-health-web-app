{{-- @extends('layouts.app')

@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Customers</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        @include('flash::message')
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                        Customers
                        <a class="pull-right" href="{{ route('customers.create') }}"><i
                                class="fa fa-plus-square fa-lg"></i></a>
                    </div>
                    <div class="card-body">
                        @include('customers.table')
                        <div class="pull-right mr-3">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

--}}

@extends('admin_backoffice.layouts.master')

@section('title')
    Utilisateurs | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Self challenger</h4>
                            <p class="card-category"> Tous les Self challenger</p>
                            <div class="col text-right">
                                <a class="pull-right btn btn-sm btn-light" href="{{ route('customers.create') }}">Ajouter un
                                Self challenger</a>
                            </div>
                        </div>
                        <div class="card-body" id="dynamic">
                            @include('customers.table')
                        </div>
                        <div class="mt-3 d-flex justify-content-center" id="link">
                            {{ $customers->links() }}
                        </div>
                        <div class="float-right">
                            <a href="/customers/download/all" class="btn btn-success mr-1" >Exporter tableau complet</a>
                            <button id="print" class="btn btn-info">Exporter tableau actuel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        $('body').on('keyup', '#searchCustomer', function() {
            // alert('ok')
            var searchCustomer = $(this).val();
            $.ajax({
                method: 'POST',
                url: "{{ route('searchCustomer') }}",
                dataType: "json",
                data: {
                    '_token': '{{ csrf_token() }}',
                    searchCustomer: searchCustomer
                },

                success: function(res) {
                    console.log(res);
                    // var BigDiv = '';

                    $("#dynamic").html(res);
                    $("#link").html('');

                    // $.each(res, function(index, value) {

                    //     BigDiv = `<div class="row mt-5">
                    //     <div class="col-xl-12 mb-5 mb-xl-0">
                    //         <div class="card shadow">
                    //             <div class="table-responsive">
                    //                 <!-- Projects table -->
                    //                 <table class="table align-items-center table-flush">
                    //                     <thead class="thead-light">
                    //                         <tr>
                    //                             <th scope="col">Nom</th>
                    //                             <th scope="col">Téléphone</th>
                    //                             <th scope="col">Email</th>
                    //                             <th scope="col">Note</th>
                    //                             <th scope="col">Expérience</th>
                    //                             <th scope="col">Qualification</th>
                    //                             <th colspan="3">Action</th>
                    //                         </tr>
                    //                     </thead>
                    //                 <tbody>
                    //                         <tr>
                    //                             <th>
                    //                                 ${value.name}
                    //                             </th>
                    //                             <td>
                    //                                 ${value.phone}
                    //                             </td>

                    //                             <td>
                    //                                 ${value.email}
                    //                             </td>

                    //                             <td>
                    //                                 ${value.gender}
                    //                             </td>

                    //                             <td>
                    //                                 <form action="">
                    //                                     <div class='btn-group'>
                    //                                         <a href="{{ url('/customers/${value.id}') }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                    //                                         <a href="{{ url('/customers/${value.id}/edit') }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                    //                                         <button type="submit" class="btn btn-ghost-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash" ></i></button>
                    //                                     </div>
                    //                                 <form/>
                    //                             </td>
                    //                         </tr>
                    //                 </tbody>
                    //             </table>
                    //         </div>
                    //     </div>
                    // </div>`;

                    //     $('#dynamic').append(BigDiv);


                    // });
                }
            })
        });

    </script>

@endsection