<div class="table-responsive">
    <table class="table printTable" id="customers-table">
        <thead class="text-primary">
            <tr>
                {{-- <th>Last Name</th>
                <th>First Name</th> --}}
                <th>Name</th>
                {{-- <th>Dial Code</th>
                <th>Phone Number</th> --}}
                <th>Phone</th>
                {{-- <th>Is Phone Verifed</th>
                <th>Phone Verifed At</th>
                <th>Is Active</th>
                <th>Activation Date</th> --}}
                <th>Email</th>
                {{-- <th>Photo Url</th> --}}
                <th>Gender</th>
                {{-- <th>Bio</th>
                <th>Birthday</th>
                <th>Birth Location</th>
                <th>Language</th>
                <th>Location Address</th>
                <th>Location Latitude</th>
                <th>Location Longitude</th>
                <th>Location Gps</th>
                <th>Password</th> --}}
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($customers as $customer)
                <tr>
                    {{-- <td>{{ $customer->last_name }}</td>
                    <td>{{ $customer->first_name }}</td> --}}
                    <td>{{ $customer->name }}</td>
                    {{-- <td>{{ $customer->dial_code }}</td>
                    <td>{{ $customer->phone_number }}</td> --}}
                    <td>{{ $customer->phone }}</td>
                    {{-- <td>{{ $customer->is_phone_verifed }}</td>
                    <td>{{ $customer->phone_verifed_at }}</td>
                    <td>{{ $customer->is_active }}</td>
                    <td>{{ $customer->activation_date }}</td> --}}
                    <td>{{ $customer->email }}</td>
                    {{-- <td>{{ $customer->photo_url }}</td> --}}
                    <td>{{ $customer->gender }}</td>
                    {{-- <td>{{ $customer->bio }}</td>
                    <td>{{ $customer->birthday }}</td>
                    <td>{{ $customer->birth_location }}</td>
                    <td>{{ $customer->language }}</td>
                    <td>{{ $customer->location_address }}</td>
                    <td>{{ $customer->location_latitude }}</td>
                    <td>{{ $customer->location_longitude }}</td>
                    <td>{{ $customer->location_gps }}</td>
                    <td>{{ $customer->password }}</td> --}}
                    <td>
                       {{--  {!! Form::open(['route' => ['customers.destroy', $customer->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('customers.show', [$customer->id]) }}" class='btn btn-ghost-success'><i
                                    class="fa fa-eye"></i></a>
                            <a href="{{ route('customers.edit', [$customer->id]) }}" class='btn btn-ghost-info'><i
                                    class="fa fa-edit"></i></a>
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn
                            btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!} --}}

                        {!! Form::open(['route' => ['customers.destroy', $customer->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('customers.show', [$customer->id]) }}" class='btn btn-ghost-success'><i
                                    class="fa fa-eye"></i></a>
                            <a href="{{ route('customers.edit', [$customer->id]) }}" class='btn btn-ghost-info'><i
                                    class="fa fa-edit"></i></a>
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn
                            btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
