{{-- <div class="col-md-8"> --}}
    <div class="row mt-3">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('last_name', 'Nom:') !!}
                <p>{{ $customer->last_name }}</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('first_name', 'Prénom(s):') !!}
                <p>{{ $customer->first_name }}</p>
            </div>
        </div>
    </div>

    {{-- <div class="row mt-3">
        <div class="col-md-12">
            <div class="form-group">
                {!! Form::label('name', 'Nom prénom(s):') !!}
                <p>{{ $customer->name }}</p>
            </div>
        </div>
    </div> --}}

    {{--
    <!-- Last Name Field -->
    <div class="form-group">
        {!! Form::label('last_name', 'Last Name:') !!}
        <p>{{ $customer->last_name }}</p>
    </div>

    <!-- First Name Field -->
    <div class="form-group">
        {!! Form::label('first_name', 'First Name:') !!}
        <p>{{ $customer->first_name }}</p>
    </div> --}}

    <!-- Name Field -->
    {{-- <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        <p>{{ $customer->name }}</p>
    </div> --}}

    <!-- Dial Code Field -->
    {{-- <div class="form-group">
        {!! Form::label('dial_code', 'Dial Code:') !!}
        <p>{{ $customer->dial_code }}</p>
    </div>

    <!-- Phone Number Field -->
    <div class="form-group">
        {!! Form::label('phone_number', 'Phone Number:') !!}
        <p>{{ $customer->phone_number }}</p>
    </div> --}}

    <!-- Phone Field -->
    {{-- <div class="form-group">
        {!! Form::label('phone', 'Phone:') !!}
        <p>{{ $customer->phone }}</p>
    </div> --}}

    <div class="row mt-3">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('phone', 'Phone:') !!}
                <p>{{ $customer->phone }}</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('email', 'Email:') !!}
                <p>{{ $customer->email }}</p>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('birthday', 'Date de naissance:') !!}
                <p>{{ $customer->birthday }}</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('discovery_source', 'comment avez-vous connu golden health ?:') !!}
                <p>{{ $customer->discovery_source ?? '' }}</p>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('commune', 'Commune:') !!}
                <p>{{ $customer->commune ?? '' }}</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('quartier', 'Quartier:') !!}
                <p>{{ $customer->quartier ?? '' }}</p>
            </div>
        </div>
    </div>
    {{--
    <!-- Is Phone Verifed Field -->
    <div class="form-group">
        {!! Form::label('is_phone_verifed', 'Is Phone Verifed:') !!}
        <p>{{ $customer->is_phone_verifed }}</p>
    </div>

    <!-- Phone Verifed At Field -->
    <div class="form-group">
        {!! Form::label('phone_verifed_at', 'Phone Verifed At:') !!}
        <p>{{ $customer->phone_verifed_at }}</p>
    </div>

    <!-- Is Active Field -->
    <div class="form-group">
        {!! Form::label('is_active', 'Is Active:') !!}
        <p>{{ $customer->is_active }}</p>
    </div>

    <!-- Activation Date Field -->
    <div class="form-group">
        {!! Form::label('activation_date', 'Activation Date:') !!}
        <p>{{ $customer->activation_date }}</p>
    </div>

    <!-- Email Field -->
    <div class="form-group">
        {!! Form::label('email', 'Email:') !!}
        <p>{{ $customer->email }}</p>
    </div>
    --}}
{{-- </div> --}}


<!-- Photo Url Field -->
{{-- <div class="form-group">
    {!! Form::label('photo_url', 'Photo Url:') !!}
    <p>{{ $customer->photo_url }}</p>
</div> --}}

<!-- Gender Field -->
{{-- <div class="form-group">
    {!! Form::label('gender', 'Gender:') !!}
    <p>{{ $customer->gender }}</p>
</div> --}}

<!-- Bio Field -->
{{-- <div class="form-group">
    {!! Form::label('bio', 'Bio:') !!}
    <p>{{ $customer->bio }}</p>
</div> --}}

<!-- Birthday Field -->
{{-- <div class="form-group">
    {!! Form::label('birthday', 'Birthday:') !!}
    <p>{{ $customer->birthday }}</p>
</div>

<!-- Birth Location Field -->
<div class="form-group">
    {!! Form::label('birth_location', 'Birth Location:') !!}
    <p>{{ $customer->birth_location }}</p>
</div>

<!-- Language Field -->
<div class="form-group">
    {!! Form::label('language', 'Language:') !!}
    <p>{{ $customer->language }}</p>
</div>

<!-- Location Address Field -->
<div class="form-group">
    {!! Form::label('location_address', 'Location Address:') !!}
    <p>{{ $customer->location_address }}</p>
</div>

<!-- Location Latitude Field -->
<div class="form-group">
    {!! Form::label('location_latitude', 'Location Latitude:') !!}
    <p>{{ $customer->location_latitude }}</p>
</div>

<!-- Location Longitude Field -->
<div class="form-group">
    {!! Form::label('location_longitude', 'Location Longitude:') !!}
    <p>{{ $customer->location_longitude }}</p>
</div>

<!-- Location Gps Field -->
<div class="form-group">
    {!! Form::label('location_gps', 'Location Gps:') !!}
    <p>{{ $customer->location_gps }}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{{ $customer->password }}</p>
</div> --}}

<!-- Created At Field -->
{{-- <div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $customer->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $customer->updated_at }}</p>
</div> --}}

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('created_at', 'Date de création:') !!}
            <p>{{ $customer->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('updated_at', 'Date de modification:') !!}
            <p>{{ $customer->updated_at }}</p>
        </div>
    </div>
</div>
