{{-- @extends('layouts.app')

@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ route('customers.index') }}">Customer</a>
    </li>
    <li class="breadcrumb-item active">Detail</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        @include('coreui-templates::common.errors')
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Details</strong>
                        <a href="{{ route('customers.index') }}" class="btn btn-light">Back</a>
                    </div>
                    <div class="card-body">
                        @include('customers.show_fields')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}

@extends('admin_backoffice.layouts.master')

@section('title')
    Utilisateur | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Détails Self challenger</h4>
                            <p class="card-category">Information Self challenger</p>
                        </div>
                        <div class="card-body">
                            {{-- <div class="row"> --}}
                                @include('customers.show_fields')
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <a href="{{ route('customers.edit', [$customer->id]) }}" class="btn btn-primary pull-right">Modifier</a>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        Changer le status de client.
                                        {!! Form::open(['route' => ['customer.changeStatus', $customer->id], 'method' => 'post']) !!}
                                            @if($customer->is_active == 1)
                                                {!! Form::hidden('is_active', 0) !!}
                                                {!! Form::button('Désactiver le compte', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Êtes vous sûr de confirmer le changement de compte ?')"]) !!}
                                            @else
                                                {!! Form::hidden('is_active', 1) !!}
                                                {!! Form::button('Activer le compte', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Êtes vous sûr de confirmer le changement de compte ?')"]) !!}
                                            @endif
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                {{-- </div> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <a href="javascript:;">
                                <img class="img" src="{{ $customer->photo_url }}" />
                            </a>
                        </div>
                        <div class="card-body">
                            <h6 class="card-category text-gray">{{ $customer->name }}</h6>
                            <p class="card-description">
                                {{ $customer->bio }}
                            </p>
                            <a href="{{ route('customers.index') }}" class="btn btn-primary btn-round">Retour</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
