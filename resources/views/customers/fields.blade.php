<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('last_name', 'Nom:') !!}
            {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('first_name', 'Prénom(s):') !!}
            {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{{--
<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
</div>

<!-- First Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
</div> --}}

{{--
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div> --}}

{{--
<!-- Dial Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dial_code', 'Dial Code:') !!}
    {!! Form::text('dial_code', null, ['class' => 'form-control']) !!}
</div> --}}

{{--
<!-- Phone Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    {!! Form::text('phone_number', null, ['class' => 'form-control']) !!}
</div> --}}


<div class="form-group col-sm-6">


</div>
<div class="row mt-3">
    <div class="col-md-6">
        <!-- Phone Field -->
        <div class="form-group">
            {!! Form::label('phone', 'Phone:') !!}
            {!! Form::text('phone', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <!-- Email Field -->
            {!! Form::label('email', 'Email:') !!}
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<!-- Is Phone Verifed Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('is_phone_verifed', 'Is Phone Verifed:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_phone_verifed', 0) !!}
        {!! Form::checkbox('is_phone_verifed', '1', null) !!}
    </label>
</div>


<!-- Phone Verifed At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_verifed_at', 'Phone Verifed At:') !!}
    {!! Form::text('phone_verifed_at', null, ['class' => 'form-control', 'id' => 'phone_verifed_at']) !!}
</div>

@push('scripts')
<script type="text/javascript">
    $('#phone_verifed_at').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        useCurrent: true,
        icons: {
            up: "icon-arrow-up-circle icons font-2xl",
            down: "icon-arrow-down-circle icons font-2xl"
        },
        sideBySide: true
    })

</script>
@endpush


<!-- Is Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_active', 'Is Active:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_active', 0) !!}
        {!! Form::checkbox('is_active', '1', null) !!}
    </label>
</div>


<!-- Activation Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('activation_date', 'Activation Date:') !!}
    {!! Form::text('activation_date', null, ['class' => 'form-control', 'id' => 'activation_date']) !!}
</div>

@push('scripts')
<script type="text/javascript">
    $('#activation_date').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        useCurrent: true,
        icons: {
            up: "icon-arrow-up-circle icons font-2xl",
            down: "icon-arrow-down-circle icons font-2xl"
        },
        sideBySide: true
    })

</script>
@endpush
--}}



{!! Form::hidden('type_account', 'challenger') !!}

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Photo Url Field -->
        <div class="form-group">
            {!! Form::label('medias', 'Photo Url:') !!}
            {!! Form::file('medias', ['class' => 'form-control', 'onchange' => 'loadFile(event)']) !!}
            <small>Cliquez sur <strong>"Photo url"</strong></small>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Gender Field -->

        <div class="form-group">
            {!! Form::label('gender', 'Gender:') !!}
            {!! Form::select('gender', ['Mr' => 'Mr', 'Mme' => 'Mme'], ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<!-- Bio Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('bio', 'Bio:') !!}
    {!! Form::text('bio', null, ['class' => 'form-control']) !!}
</div>

<!-- Birthday Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birthday', 'Birthday:') !!}
    {!! Form::text('birthday', null, ['class' => 'form-control', 'id' => 'birthday']) !!}
</div>

@push('scripts')
<script type="text/javascript">
    $('#birthday').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        useCurrent: true,
        icons: {
            up: "icon-arrow-up-circle icons font-2xl",
            down: "icon-arrow-down-circle icons font-2xl"
        },
        sideBySide: true
    })

</script>
@endpush


<!-- Birth Location Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birth_location', 'Birth Location:') !!}
    {!! Form::text('birth_location', null, ['class' => 'form-control']) !!}
</div>

<!-- Language Field -->
<div class="form-group col-sm-6">
    {!! Form::label('language', 'Language:') !!}
    {!! Form::text('language', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_address', 'Location Address:') !!}
    {!! Form::text('location_address', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_latitude', 'Location Latitude:') !!}
    {!! Form::text('location_latitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_longitude', 'Location Longitude:') !!}
    {!! Form::text('location_longitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Gps Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_gps', 'Location Gps:') !!}
    {!! Form::text('location_gps', null, ['class' => 'form-control']) !!}
</div> --}}

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Password Field -->
        <div class="form-group">
            {!! Form::label('password', 'Password:') !!}
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <img style="width: 50%; height: 50%; border: none" id="output" />
        </div>
    </div>
    <script>
        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function() {
                var output = document.getElementById('output');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };

    </script>

    {{--<div class="col-md-6">
        <div class="form-group">
            {!! Form::label('bio', 'Bio:') !!}
            {!! Form::textarea('bio', null, ['class' => 'form-control']) !!}
        </div>
    </div>--}}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('customers.index') }}" class="btn btn-secondary">Annuler</a>
</div>
