<div class="row">
    <!-- Titre Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('titre', 'Titre:') !!}
        <p>{{ $slide->titre }}</p>
    </div>

    <!-- Subtitle Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('subtitle', 'Sous-titre:') !!}
        <p>{!! $slide->subtitle !!}</p>
    </div>
</div>

<!-- Page Field -->
<div class="form-group">
    {!! Form::label('page', 'Page:') !!}
    <p>{{ $slide->page }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p><img src="{{ $slide->image }}" alt="{{ $slide->titre }}" height="500"></p>
</div>


{{--

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $slide->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $slide->updated_at }}</p>
</div>

--}}