@extends('admin_backoffice.layouts.master')

@section('title')
Slides | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Slides</h4>
                            <p class="card-category"> Détail Slide</p>   
                        </div>
                        <div class="card-body">
                            @include('slides.show_fields')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection