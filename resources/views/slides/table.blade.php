<div class="table-responsive-sm">
    <table class="table table-striped" id="slides-table">
        <thead>
            <tr>
                <th>Titre</th>
                <th>Sous-titre</th>
                <th>Image</th>
                <th>Page</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($slides as $slide)
            <tr>
                <td>{{ $slide->titre }}</td>
                <td>{!! $slide->subtitle !!}</td>
                <td><img src="{{ $slide->image }}" alt="{{ $slide->titre }}" height="100"></td>
                <td>{{ $slide->page }}</td>
                <td>
                    {!! Form::open(['route' => ['slides.destroy', $slide->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('slides.show', [$slide->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('slides.edit', [$slide->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>