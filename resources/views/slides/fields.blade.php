
<div class="row">
    <!-- Image Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('image', 'Image:') !!}
        {!! Form::file('image') !!}
    </div>
    <!-- Page Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('page', 'Page:') !!}
        <select name="page" id="page" onchange="showdiv(this.value)">
            <option value="home">Page Accueil</option>
            <option value="event">Page Event</option>
        </select>
    </div>
</div>
@if(isset($slide))
    @if($slide->page == "event")
        <div class="row" id="title_subtitle">
    @else
        <div class="row" id="title_subtitle" style="display:none;">
    @endif
@else
    <div class="row" id="title_subtitle" style="display:none;">
@endif
    <!-- Titre Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('titre', 'Titre:') !!}
        {!! Form::text('titre', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Subtitle Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {{--{!! Form::label('subtitle', 'Subtitle:') !!}--}}
        {!! Form::textarea('subtitle', null, ['class' => 'form-control', 'id' => 'description']) !!}
    </div>
</div>

@if(isset($slide))
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <img src="{{ $slide->image }}" style="width: 50%; height: 50%; border: none" />
            </div>
        </div>
    </div>
@endif
<div class="clearfix"></div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('slides.index') }}" class="btn btn-secondary">Annuler</a>
</div>

<script>
    function showdiv(val){
        if(val == 'event'){
            document.getElementById("title_subtitle").style.display= 'block';
        }else{
            document.getElementById("title_subtitle").style.display= 'none';
        }
    }
</script>
