@extends('admin_backoffice.layouts.master')

@section('title')
    Slide | {{ config('app.name') }}
@endsection
 
@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Créer un slide</h4>
                            <p class="card-category">Information slide</p>
                        </div>
                        <div class="card-body">
                            {!! Form::open(['route' => 'slides.store', 'files' => true]) !!}

                            @include('slides.fields')

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection