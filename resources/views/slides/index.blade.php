@extends('admin_backoffice.layouts.master')

@section('title')
Slides | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Slides</h4>
                            <p class="card-category"> Toutes les slides</p>
                            <div class="col text-right">
                                <a class="pull-right btn btn-sm btn-light" href="{{ route('slides.create') }}">Ajouter un slide</a>
                            </div> 
                        </div>
                        <div class="card-body">
                            @include('slides.table')
                        </div>
                        <div class="mt-3 d-flex justify-content-center">
                            {{ $slides->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection