<div class="table-responsive-sm">
    <table class="table table-striped" id="packs-table">
        <thead>
            <tr>
                <th>Slug</th>
        <th>Name</th>
        <th>Description</th>
        <th>Quantity</th>
        <th>Quantity Fuel</th>
        <th>Price</th>
        <th>Image Url</th>
        <th>Currency Code</th>
        <th>Time Limit</th>
        <th>Time Limit Unit</th>
        <th>Has Delay</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($packs as $pack)
            <tr>
                <td>{{ $pack->slug }}</td>
            <td>{{ $pack->name }}</td>
            <td>{{ $pack->description }}</td>
            <td>{{ $pack->quantity }}</td>
            <td>{{ $pack->quantity_fuel }}</td>
            <td>{{ $pack->price }}</td>
            <td>{{ $pack->image_url }}</td>
            <td>{{ $pack->currency_code }}</td>
            <td>{{ $pack->time_limit }}</td>
            <td>{{ $pack->time_limit_unit }}</td>
            <td>{{ $pack->has_delay }}</td>
                <td>
                    {!! Form::open(['route' => ['packs.destroy', $pack->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('packs.show', [$pack->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('packs.edit', [$pack->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>