<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{{ $pack->slug }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $pack->name }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $pack->description }}</p>
</div>

<!-- Quantity Field -->
<div class="form-group">
    {!! Form::label('quantity', 'Quantity:') !!}
    <p>{{ $pack->quantity }}</p>
</div>

<!-- Quantity Fuel Field -->
<div class="form-group">
    {!! Form::label('quantity_fuel', 'Quantity Fuel:') !!}
    <p>{{ $pack->quantity_fuel }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $pack->price }}</p>
</div>

<!-- Image Url Field -->
<div class="form-group">
    {!! Form::label('image_url', 'Image Url:') !!}
    <p>{{ $pack->image_url }}</p>
</div>

<!-- Currency Code Field -->
<div class="form-group">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    <p>{{ $pack->currency_code }}</p>
</div>

<!-- Time Limit Field -->
<div class="form-group">
    {!! Form::label('time_limit', 'Time Limit:') !!}
    <p>{{ $pack->time_limit }}</p>
</div>

<!-- Time Limit Unit Field -->
<div class="form-group">
    {!! Form::label('time_limit_unit', 'Time Limit Unit:') !!}
    <p>{{ $pack->time_limit_unit }}</p>
</div>

<!-- Has Delay Field -->
<div class="form-group">
    {!! Form::label('has_delay', 'Has Delay:') !!}
    <p>{{ $pack->has_delay }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $pack->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $pack->updated_at }}</p>
</div>

