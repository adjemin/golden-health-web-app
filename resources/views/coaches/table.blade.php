<div class="table-responsive">
    <table class="table printTable" id="coaches-table">
        <thead class="text-primary">
            <tr>
                {{-- <th>Last Name</th>
                <th>First Name</th> --}}
                <th>Nom</th>
                {{-- <th>Dial Code</th>
                <th>Phone Number</th> --}}
                <th>Téléphone</th>
                <th>Email</th>
                {{-- <th>Is Phone Verifed</th>
                <th>Phone Verifed At</th>--}}
                <th>Is Active</th>
                {{--<th>Activation Date</th>
                <th>Photo Id</th>--}}
                {{--<th>Photo</th>
                <th>Civilité</th>--}}
                {{--<th>Bio</th>
                <th>Birthday</th>
                <th>Birth Location</th>
                <th>Location Address</th>
                <th>Location Latitude</th>
                <th>Location Longitude</th>
                <th>Location Gps</th>
                <th>Note</th>
                <th>Expérience</th>
                <th>Qualification</th> --}}
                {{-- <th>Facebook Id</th>
                <th>Twitter Id</th>
                <th>Youtube Id</th> --}}
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($coaches as $coach)
                <tr>
                    {{-- <td>{{ $coach->last_name }}</td>
                    <td>{{ $coach->first_name }}</td> --}}
                    <td>{{ $coach->customer->name ?? ''}}</td>
                    {{-- <td>{{ $coach->dial_code }}</td>
                    <td>{{ $coach->phone_number }}</td> --}}
                    <td>{{ $coach->customer->phone ?? '' }}</td>
                    <td>{{ $coach->customer->email ?? '' }}</td>
                    {{-- <td>{{ $coach->is_phone_verifed }}</td>
                    <td>{{ $coach->phone_verifed_at }}</td>--}}
                    <td>{!! $coach->is_active == 1 ? '<span class="btn btn-success px-3 py-2">Actif</span>' : '<span class="btn btn-danger px-3 py-2">Inactif</span>'  !!}</td>
                    {{--<td>{{ $coach->activation_date }}</td>
                    <td>{{ $coach->photo_id }}</td>--}} 
                    {{--<td>{!! !empty($coach->photo_url) ? '<img src="$coach->photo_url" alt="">' : '' !!}</td>
                    <td>{{ $coach->gender }}</td>--}}
                    {{--<td>{{ $coach->bio }}</td>
                    <td>{{ $coach->birthday }}</td>
                    <td>{{ $coach->birth_location }}</td>
                    <td>{{ $coach->location_address }}</td>
                    <td>{{ $coach->location_latitude }}</td>
                    <td>{{ $coach->location_longitude }}</td>
                    <td>{{ $coach->location_gps }}</td>
                    <td>{{ $coach->rating }}</td>
                    <td>{{ $coach->experience }}</td>
                    <td>{{ $coach->qualification_id }}</td> --}}
                    {{-- <td>{{ $coach->facebook_id }}</td>
                    <td>{{ $coach->twitter_id }}</td>
                    <td>{{ $coach->youtube_id }}</td> --}}
                    <td>
                        {!! Form::open(['route' => ['coaches.destroy', $coach->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('coaches.show', [$coach->id]) }}" class='btn btn-ghost-success'><i
                                    class="fa fa-eye"></i></a>
                            <a href="{{ route('coaches.edit', [$coach->id]) }}" class='btn btn-ghost-info'><i
                                    class="fa fa-edit"></i></a>
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn
                            btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
