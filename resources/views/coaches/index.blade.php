{{-- @extends('layouts.app')

@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Coaches</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        @include('flash::message')
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                        Coaches
                        <a class="pull-right" href="{{ route('coaches.create') }}"><i
                                class="fa fa-plus-square fa-lg"></i></a>
                    </div>
                    <div class="card-body">
                        @include('coaches.table')
                        <div class="pull-right mr-3">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
{{-- - --}}
@extends('admin_backoffice.layouts.master')

@section('title')
    Coachs | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Coachs</h4>
                            <p class="card-category"> Tous les coachs</p>
                            <div class="col text-right">
                                <a class="pull-right btn btn-sm btn-light" href="{{ route('coaches.create') }}">Ajouter un
                                    coach</a>
                            </div>
                        </div>
                        <div class="card-body" id="dynamic">
                            @include('coaches.table')
                        </div>
                        <div class="mt-3 d-flex justify-content-center" id="link">
                            {{ $coaches->links() }}
                        </div>
                        <div class="float-right">
                            <a href="/coach/download/all" class="btn btn-success mr-1" >Exporter tableau complet</a>
                            <button id="print" class="btn btn-info">Exporter tableau actuel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        $('body').on('keyup', '#searchCoach', function() {
            // alert('ok')
            var searchCoach = $(this).val();
            $.ajax({
                method: 'POST',
                url: "{{ route('searchCoach') }}",
                dataType: "json",
                data: {
                    '_token': '{{ csrf_token() }}',
                    searchCoach: searchCoach
                },

                success: function(res) {
                    console.log(res);

                    // var BigDiv = '';

                    $("#dynamic").html(res);
                    $("#link").html('');

                    // $.each(res, function(index, value) {

                    //     BigDiv = `<div class="row mt-5">
                    //     <div class="col-xl-12 mb-5 mb-xl-0">
                    //         <div class="card shadow">
                    //             <div class="table-responsive">
                    //                 <!-- Projects table -->
                    //                 <table class="table align-items-center table-flush">
                    //                     <thead class="thead-light">
                    //                         <tr>
                    //                             <th scope="col">Nom</th>
                    //                             <th scope="col">Téléphone</th>
                    //                             <th scope="col">Email</th>
                    //                             <th scope="col">Note</th>
                    //                             <th scope="col">Expérience</th>
                    //                             <th scope="col">Qualification</th>
                    //                             <th colspan="3">Action</th>
                    //                         </tr>
                    //                     </thead>
                    //                 <tbody>
                    //                         <tr>
                    //                             <th>
                    //                                 ${value.customer["name"]}
                    //                             </th>
                    //                             <td>
                    //                                 ${value.customer["phone"]}
                    //                             </td>

                    //                             <td>
                    //                                 ${value.customer["email"]}
                    //                             </td>

                    //                             <td>
                    //                                 ${value.rating}
                    //                             </td>

                    //                             <td>
                    //                                 ${value.experience}
                    //                             </td>

                    //                             <td>
                    //                                 ${value.qualification_id}
                    //                             </td>

                    //                             <td>
                    //                                 <form action="">
                    //                                     <div class='btn-group'>
                    //                                         <a href="{{ url('/coaches/${value.id}') }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                    //                                         <a href="{{ url('/coaches/${value.id}/edit') }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                    //                                         <button type="submit" class="btn btn-ghost-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash" ></i></button>
                    //                                     </div>
                    //                                 <form/>
                    //                             </td>
                    //                         </tr>
                    //                 </tbody>
                    //             </table>
                    //         </div>
                    //     </div>
                    // </div>`;

                    //     $('#dynamic').append(BigDiv);


                    // });
                }
            })
        });

    </script>

@endsection
