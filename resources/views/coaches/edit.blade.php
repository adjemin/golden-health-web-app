@extends('admin_backoffice.layouts.master')

@section('title')
    Coachs | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Modifier Coach</h4>
                            <p class="card-category">Modifier un nouveau coach</p>
                        </div>
                        <div class="card-body">
                            {!! Form::model($coach, ['route' => ['coaches.update', $coach->id], 'method' => 'patch', 'files' => true]) !!}

                            @include('coaches.fields')

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <a href="javascript:;">
                                <img class="img" id="output" src="{{ $coach->customer->photo_url }}" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection