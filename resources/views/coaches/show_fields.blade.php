<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('last_name', 'Nom:') !!}
            <p>{{ $coach->customer->last_name }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('first_name', 'Prénom(s):') !!}
            <p>{{ $coach->customer->first_name }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('phone', 'Phone:') !!}
            <p>{{ $coach->customer->phone }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            <p>{{ $coach->customer->email }}</p>
        </div>
    </div>
</div>

{{-- <!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $coach->name }}</p>
</div> --}}

{{-- <!-- Dial Code Field -->
<div class="form-group">
    {!! Form::label('dial_code', 'Dial Code:') !!}
    <p>{{ $coach->dial_code }}</p>
</div>

<!-- Phone Number Field -->
<div class="form-group">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    <p>{{ $coach->phone_number }}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $coach->phone }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $coach->email }}</p>
</div>

<!-- Is Phone Verifed Field -->
<div class="form-group">
    {!! Form::label('is_phone_verifed', 'Is Phone Verifed:') !!}
    <p>{{ $coach->is_phone_verifed }}</p>
</div>

<!-- Phone Verifed At Field -->
<div class="form-group">
    {!! Form::label('phone_verifed_at', 'Phone Verifed At:') !!}
    <p>{{ $coach->phone_verifed_at }}</p>
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    <p>{{ $coach->is_active }}</p>
</div>

<!-- Activation Date Field -->
<div class="form-group">
    {!! Form::label('activation_date', 'Activation Date:') !!}
    <p>{{ $coach->activation_date }}</p>
</div>

<!-- Photo Id Field -->
<div class="form-group">
    {!! Form::label('photo_id', 'Photo Id:') !!}
    <p>{{ $coach->photo_id }}</p>
</div>

<!-- Photo Url Field -->
<div class="form-group">
    {!! Form::label('photo_url', 'Photo Url:') !!}
    <p>{{ $coach->photo_url }}</p>
</div> --}}



<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('gender', 'Gender:') !!}
            <p>{{ $coach->customer->gender }}</p>
        </div>
    </div>
    {{-- <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('bio', 'Bio:') !!}
            <p>{{ $coach->bio }}</p>
        </div>
    </div>--}}
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Birthday Field -->
        <div class="form-group">
            {!! Form::label('birthday', 'Birthday:') !!}
            <p>{{ $coach->customer->birthday }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Birthday Field -->
        <div class="form-group">
            {!! Form::label('facebook_id', 'Facebook:') !!}
            <p>{{ $coach->customer->facebook_id }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Birthday Field -->
        <div class="form-group">
            {!! Form::label('google_id', 'Google:') !!}
            <p>{{ $coach->customer->google_id }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Birthday Field -->
        <div class="form-group">
            {!! Form::label('instagram_url', 'Instagram :') !!}
            <p>{{ $coach->instagram_url }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Birthday Field -->
        <div class="form-group">
            {!! Form::label('youtube_url', 'Youtube:') !!}
            <p>{{ $coach->youtube_url }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Birthday Field -->
        <div class="form-group">
            {!! Form::label('site_web', 'Site Web :') !!}
            <p>{{ $coach->site_web }}</p>
        </div>
    </div>
</div>

{{--
<!-- Birth Location Field -->
<div class="form-group">
    {!! Form::label('birth_location', 'Birth Location:') !!}
    <p>{{ $coach->birth_location }}</p>
</div>

<!-- Location Address Field -->
<div class="form-group">
    {!! Form::label('location_address', 'Location Address:') !!}
    <p>{{ $coach->location_address }}</p>
</div>

<!-- Location Latitude Field -->
<div class="form-group">
    {!! Form::label('location_latitude', 'Location Latitude:') !!}
    <p>{{ $coach->location_latitude }}</p>
</div>

<!-- Location Longitude Field -->
<div class="form-group">
    {!! Form::label('location_longitude', 'Location Longitude:') !!}
    <p>{{ $coach->location_longitude }}</p>
</div>

<!-- Location Gps Field -->
<div class="form-group">
    {!! Form::label('location_gps', 'Location Gps:') !!}
    <p>{{ $coach->location_gps }}</p>
</div> --}}

<!-- Rating Field -->
{{-- <div class="form-group">
    {!! Form::label('rating', 'Rating:') !!}
    <p>{{ $coach->rating }}</p>
</div>

<!-- Experience Field -->
<div class="form-group">
    {!! Form::label('experience', 'Expérience:') !!}
    <p>{{ $coach->experience }}</p>
</div> --}}


{{--<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('rating', 'Note:') !!}
            <p>{{ $coach->rating }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('experience', 'Expérience:') !!}
            <p>{{ $coach->experience }}</p>
        </div>
    </div>
</div>--}}

{{-- <!-- Qualification Id Field -->
<div class="form-group">
    {!! Form::label('qualification_id', 'Qualification Id:') !!}
    <p>{{ $coach->qualification_id }}</p>
</div> --}}

<div class="row mt-3">
    {{-- <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('qualification_id', 'Qualification:') !!}
            <p>{{ $coach->qualification_id ?? '' }}</p>
        </div>
    </div>--}}
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('commune', 'Commune:') !!}
            <p>{{ $coach->customer->commune ?? '' }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('quartier', 'Quartier:') !!}
            <p>{{ $coach->customer->quartier ?? '' }}</p>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('discovery_source', 'comment avez-vous connu golden health ?:') !!}
            <p>{{ $coach->discovery_source ?? '' }}</p>
        </div>
    </div>
</div>
{{--<div class="row mt-3">
</div>
--}}
{{-- <!-- Facebook Id Field -->
<div class="form-group">
    {!! Form::label('facebook_id', 'Facebook Id:') !!}
    <p>{{ $coach->facebook_id }}</p>
</div>

<!-- Twitter Id Field -->
<div class="form-group">
    {!! Form::label('twitter_id', 'Twitter Id:') !!}
    <p>{{ $coach->twitter_id }}</p>
</div>

<!-- Youtube Id Field -->
<div class="form-group">
    {!! Form::label('youtube_id', 'Youtube Id:') !!}
    <p>{{ $coach->youtube_id }}</p>
</div> --}}


<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('created_at', 'Date de création:') !!}
            <p>{{ $coach->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('updated_at', 'Date de modification:') !!}
            <p>{{ $coach->updated_at }}</p>
        </div>
    </div>
</div>

