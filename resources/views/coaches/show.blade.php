{{-- @extends('layouts.app')

@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ route('coaches.index') }}">Coach</a>
    </li>
    <li class="breadcrumb-item active">Detail</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        @include('coreui-templates::common.errors')
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Details</strong>
                        <a href="{{ route('coaches.index') }}" class="btn btn-light">Back</a>
                    </div>
                    <div class="card-body">
                        @include('coaches.show_fields')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}


@extends('admin_backoffice.layouts.master')

@section('title')
    Coach | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Détails coach</h4>
                            <p class="card-category">Information coach</p>
                        </div>
                        <div class="card-body">
                            {{-- <div class="row"> --}}
                                @include('coaches.show_fields')
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <a href="{{ route('coaches.edit', [$coach->id]) }}" class="btn btn-primary pull-right">Modifier</a>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        Changer le status de coach.
                                        {!! Form::open(['route' => ['coach.changeStatus', $coach->id], 'method' => 'post']) !!}
                                            @if($coach->is_active == 1)
                                                {!! Form::hidden('is_active', 0) !!}
                                                {!! Form::button('Désactiver le compte', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Êtes vous sûr de vouloir désactiver le compte ?')"]) !!}
                                            @else
                                                {!! Form::hidden('is_active', 1) !!}
                                                {!! Form::button('Activer le compte', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Êtes vous sûr de vouloir activer le compte ?')"]) !!}
                                            @endif
                                        {!! Form::close() !!}
                                    </div>
                                </div>

                                {{-- </div> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <a href="javascript:;">
                                <img class="img" src="{{ $coach->customer->photo_url }}" />
                            </a>
                        </div>
                        <div class="card-body">
                            <h6 class="card-category text-gray">{{ $coach->customer->name }}</h6>
                            <p class="card-description">
                                {{ $coach->bio }}
                            </p>
                            <a href="{{ route('coaches.index') }}" class="btn btn-primary btn-round">Retour</a>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Disciplines</h4>
                            <p class="card-category"> Toutes les disciplines du coach</p>
                            {{-- <div class="col text-right">
                                <a class="pull-right btn btn-sm btn-light" href="{{ route('disciplines.create') }}">Ajouter
                                    une
                                    discipline</a>
                            </div> --}}
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="disciplines-table">
                                    <thead class="text-primary">
                                        <tr>
                                            <th>Nom</th>
                                            <th>Slug</th>
                                            <th colspan="3">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($disciplines = $coach->getDisciplinesAttribute() as $discipline)
                                            @if(!is_null($discipline->discipline) || !is_null($discipline))
                                            <tr>
                                                <td>{{ $discipline->name ?? '' }}</td>
                                                <td>{{ $discipline->slug ?? '' }}</td>
                                                <td>
                                                    {!! Form::open(['route' => ['disciplines.destroy', $discipline->id],
                                                    'method' => 'delete']) !!}
                                                    <div class='btn-group'>
                                                        <a href="{{ route('disciplines.show', [$discipline->id ?? '']) }}"
                                                            class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="mt-3 d-flex justify-content-center">
                            {{-- {{ $disciplines->links() }} --}}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Portfolio</h4>
                            <p class="card-category"> Portfolio du coach</p>
                            @foreach ($coach->portfolios as $portfolio)
                                <div class="col-lg-4 col-md-4 col-sm-12 mb-4">
                                    <div class="py-3 px-3">
                                        <div class="">
                                            <h5 class="font-weight-bold">{{ $portfolio->title }}</h5>
                                            <div class="embed-responsive embed-responsive-21by9 embed-responsive-16by9 embed-responsive-4by3 embed-responsive-1by1">
                                                @if($portfolio->media_type == "video") 
                                                    <iframe class="embed-responsive-item" src="{{$portfolio->media_path}}" frameborder="0" allowfullscreen></iframe>
                                                @else 
                                                    <img src="{{ $portfolio->media_path }}" alt="{{ $portfolio->title }}" type="">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Cours</h4>
                            <p class="card-category"> Tous les cours du coach</p>
                            {{-- <div class="col text-right">
                                <a class="pull-right btn btn-sm btn-light" href="{{ route('disciplines.create') }}">Ajouter
                                    une
                                    discipline</a>
                            </div> --}}
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="disciplines-table">
                                    <thead class="text-primary">
                                        <tr>
                                            <th>Discipline</th>
                                            <th>Prix</th>
                                            <th>Status</th>
                                            <th colspan="3">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($courses = $coach->courses()->get() as $course)
                                            @if(!is_null($course->courses) || !is_null($course))
                                            <tr>
                                                <td>{{ $course->discipline->name ?? '' }}</td>
                                                <td>{{ $course->price }} {{ $course->currency_name }}</td>
                                                <td>@if($course->active == 1) <span class="badge badge-success">Actif</span> @else <span class="badge badge-danger">Inactif</span> @endif </td>
                                                <td>
                                                    {!! Form::open(['route' => ['courses.destroy', $course->id],
                                                    'method' => 'delete']) !!}
                                                    <div class='btn-group'>
                                                        <a href="{{ route('courses.show', [$course->id ?? '']) }}"
                                                            class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="mt-3 d-flex justify-content-center">
                            {{-- {{ $disciplines->links() }} --}}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            {{--<h4 class="card-title ">Distinctions</h4>--}}
                            <h4 class="card-title ">Diplome</h4>
                            <p class="card-category"> Tous les diplomes du coach</p>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="distinctions-table">
                                    <thead class="text-primary">
                                        <tr>
                                            <th>Nom</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($diplomes = $coach->getDiplomeAttribute() as $diplome)
                                            @if(!is_null($diplome))
                                            <tr>
                                                <td>{{ $diplome->diplome_name ?? '' }}</td>
                                                <td><a href="{!! !is_null($diplome->image_url) ? $diplome->image_url : '#' !!}" class="btn btn-success p-1">Voir plus de détails</a></td>
                                            </tr>
                                            @endif
                                        @endforeach
                                        {{--@foreach ($distinctions = $coach->distinctions as $distinction)
                                            @if(!is_null($distinction))
                                            <tr>
                                                <td>{{ $distinction->name ?? '' }}</td>
                                                <td>{{ $distinction->date_debut }}</td>
                                                <td>{{ $distinction->date_fin }}</td>
                                                <td><a href="{!! !is_null($distinction->fichier_url) ? $distinction->fichier_url : '#' !!}" class="btn btn-success p-1">Voir plus de détails</a></td>
                                            </tr>
                                            @endif
                                        @endforeach--}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="mt-3 d-flex justify-content-center">
                            {{-- {{ $disciplines->links() }} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
