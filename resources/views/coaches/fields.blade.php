<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
            <label class="bmd-label-floating">Nom</label>
            @if(isset($customer))
                <input type="text" class="form-control" name="first_name" value="{{ $customer->first_name ?? old('first_name') }}">
            @else
                <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
            @endif
        </div>
        @if ($errors->has('first_name'))
            <span class="help-block">
                <strong>{{ $errors->first('first_name') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
            <label class="bmd-label-floating">Prénoms</label>
            @if(isset($customer))
                <input type="text" class="form-control" name="last_name" value="{{ $customer->last_name ?? old('last_name') }}">
            @else
                <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
            @endif
        </div>
        @if ($errors->has('last_name'))
            <span class="help-block">
                <strong>{{ $errors->first('last_name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="bmd-label-floating">Email</label>
            @if(isset($customer))
                <input type="email" class="form-control" name="email" value="{{ $customer->email ?? old('email') }}">
            @else
                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
            @endif
        </div>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <input type="hidden" name="type_account" value="coach">
    <div class="form-group col-sm-6 {{ $errors->has('site_web_coach') ? ' has-error' : '' }}">
        {{--{!! Form::label('site_web_coach', 'Site Coach:') !!}--}}
        @if(isset($customer))
            {!! Form::text('site_web_coach', $coach->site_web_coach ?? old('site_web_coach') , ['class' => 'form-control', 'placeholder' =>  'Site Coach :']) !!}
        @else
            {!! Form::text('site_web_coach', old('site_web_coach') , ['class' => 'form-control', 'placeholder' =>  'Site Coach :']) !!}
        @endif
        @if ($errors->has('site_web_coach'))
            <span class="help-block">
                <strong>{{ $errors->first('site_web_coach') }}</strong>
            </span>
        @endif
    </div>
</div>

<!-- Photo Id Field -->
{{--
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', null, ['class' => 'form-control']) !!}
</div>


<!-- Bio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bio', 'Bio:') !!}
    {!! Form::textarea('bio', null, ['class' => 'form-control']) !!}
</div>

<!-- Rating Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating', 'Note:') !!}
    {!! Form::number('rating', null, ['class' => 'form-control', 'min'=>0, 'max'=>5]) !!}
</div>--}}

{{--<!-- Experience Field -->
<div class="form-group col-sm-6">
    {!! Form::label('experience', 'Experience:') !!}
    {!! Form::text('experience', null, ['class' => 'form-control']) !!}
</div>--}}
<div class="row">
    <!-- Facebook Url Field -->
    <div class="form-group col-sm-6 {{ $errors->has('facebook_url') ? ' has-error' : '' }}">
        {{--{!! Form::label('facebook_url', 'Facebook Url:') !!}--}}
        @if(isset($customer))
            {!! Form::text('facebook_url', $coach->facebook_url ?? old('facebook_url'), ['class' => 'form-control', 'placeholder' =>  'Facebook Url:']) !!}
        @else
            {!! Form::text('facebook_url', old('facebook_url'), ['class' => 'form-control', 'placeholder' =>  'Facebook Url:']) !!}
        @endif
        @if ($errors->has('facebook_url'))
            <span class="help-block">
                <strong>{{ $errors->first('facebook_url') }}</strong>
            </span>
        @endif
    </div>

    <!-- Instagram Url Field -->
    <div class="form-group col-sm-6 {{ $errors->has('instagram_url') ? ' has-error' : '' }}">
        {{--{!! Form::label('instagram_url', 'Instagram Url:') !!}--}}
        @if(isset($customer))
            {!! Form::text('instagram_url', $coach->instagram_url ?? old('instagram_url'), ['class' => 'form-control', 'placeholder' =>  'Instagram Url:']) !!}
        @else
            {!! Form::text('instagram_url', old('instagram_url'), ['class' => 'form-control', 'placeholder' =>  'Instagram Url:']) !!}
        @endif
        @if ($errors->has('instagram_url'))
            <span class="help-block">
                <strong>{{ $errors->first('instagram_url') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="row">
    <!-- Youtube Url Field -->
    <div class="form-group col-sm-6 {{ $errors->has('youtube_url') ? ' has-error' : '' }}">
        {{--{!! Form::label('youtube_url', 'Youtube Url:') !!}--}}
        @if(isset($customer))
            {!! Form::text('youtube_url', $coach->youtube_url ?? old('youtube_url'), ['class' => 'form-control', 'placeholder' =>  'Instagram Url:']) !!}
        @else
            {!! Form::text('youtube_url', old('youtube_url'), ['class' => 'form-control', 'placeholder' =>  'Instagram Url:']) !!}
        @endif
        @if ($errors->has('youtube_url'))
            <span class="help-block">
                <strong>{{ $errors->first('youtube_url') }}</strong>
            </span>
        @endif
    </div>

    <!-- Is Active Field -->
    {{--<div class="form-group col-sm-6">--}}
        {{--{!! Form::label('is_active', 'Est actif:') !!}--}}
        <label class="checkbox-inline">
        Est actif: {!! Form::hidden('is_active', 0) !!}
        {!! Form::checkbox('is_active', 1)  !!}
        </label>
    {{--</div>--}}
</div>

<div class="row">
    <div class="form-group col-sm-6">
        {{--<div class="form-check mr-4">
            <input class="form-check-input" type="radio" name="gender" id="genre-mr" value="mr" checked>
            <label class="form-check-label" for="genre-mr">
                Mr
            </label>
        </div>
        <div class="form-check mr-4">
            <input class="form-check-input" type="radio" name="gender" id="genre-mme" value="mme">
            <label class="form-check-label" for="genre-mme">
                Mme
            </label>
        </div>--}}
        {!! Form::label('gender', 'Civilité ?:') !!}
        <select name="gender" class="custom-select input-style placeholder-style" name="gender">
            @if(isset($customer))
                <option value="Mr" {!! $customer->gender == "Mr"  ? 'selected' : '' !!} >Monsieur</option>
                <option value="Mme" {!! $customer->gender == "Mme"  ? 'selected' : '' !!}>Madame</option>
            @else
                <option value="Mr" >Monsieur</option>
                <option value="Mme">Madame</option>
            @endif
        </select>
        @if ($errors->has('gender'))
            <span class="help-block">
                <strong>{{ $errors->first('gender') }}</strong>
            </span>
        @endif
    </div>
    <!-- Bio Field -->
    <div class="form-group col-sm-6 {{ $errors->has('discovery_source') ? ' has-error' : '' }}">
        {!! Form::label('discovery_source', 'Comment avez-vous connu Golden Health ?:') !!}
        {{--{!! Form::text('discovery_source', null, ['class' => 'form-control']) !!}--}}
        <select name="discovery_source" class="custom-select input-style placeholder-style" id="discovery_source">
            <option value="" {!! old('discovery_source') == "" ? "selected" : "" !!}></option>
            <option value="search_engine"  {!! old('discovery_source') == "search_engine" ? "selected" : "" !!}> Moteur de recherche (Bing, Google, Yahoo...)</option>
            <option value="website" {!! old('discovery_source') == "website" ? "selected" : "" !!}> Site Internet autre qu'un moteur de recherche (Blog, presse...)</option>
            <option value="written-press" {!! old('discovery_source') == "written-press" ? "selected" : "" !!}> Presse écrite</option>
            <option value="social-media" {!! old('discovery_source') == "social-media" ? "selected" : "" !!}> Réseaux sociaux (Facebook, Instagram...)</option>
            <option value="word-of-mouth" {!! old('discovery_source') == "word-of-mouth" ? "selected" : "" !!}> Bouche à oreilles (Amis, famille, collègue...)</option>
            <option value="corporate-lesson" {!! old('discovery_source') == "corporate-lesson" ? "selected" : "" !!}> Cours en entreprise</option>
            <option value="subway-display" {!! old('discovery_source') == "subway-display" ? "selected" : "" !!}> Affichage métro</option>
            <option value="coach" {!! old('discovery_source') == "coach" ? "selected" : "" !!}> Par un coach</option>
            <option value="other" {!! old('discovery_source') == "other" ? "selected" : "" !!}> Autre (Salon, évènement...)</option>
        </select>
        @if ($errors->has('discovery_source'))
            <span class="help-block">
                <strong>{{ $errors->first('discovery_source') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('photo_url') ? ' has-error' : '' }}">
            <strong>{!! Form::label('photo_url', 'Images:') !!}</strong>
            {!! Form::file('photo_url', ['class' => 'form-control', 'onchange' => 'loadFile(event)']) !!}
            {{-- <br><small>Cliquez sur "Images" pour uploader vos medias</small> --}}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="bmd-label-floating">Mot de password</label>
            <input type="password" class="form-control" name="password">
        </div>
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="bmd-label-floating">Confirmation mot de passe</label>
            <input type="password" class="form-control" name="password_confirmation">
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Sauvegarder', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('coaches.index') }}" class="btn btn-secondary">Annuler</a>
</div>
<script>
    var loadFile = function(event) {
        var reader = new FileReader();
        reader.onload = function() {
            var output = document.getElementById('output');
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };

</script>
