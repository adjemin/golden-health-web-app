<!-- Course Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_id', 'Course Id:') !!}
    {!! Form::text('course_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Lieu Course Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lieu_course_id', 'Lieu Course Id:') !!}
    {!! Form::text('lieu_course_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('courseLieus.index') }}" class="btn btn-secondary">Cancel</a>
</div>
