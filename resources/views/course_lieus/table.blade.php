<div class="table-responsive-sm">
    <table class="table table-striped" id="courseLieus-table">
        <thead>
            <tr>
                <th>Course Id</th>
        <th>Lieu Course Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($courseLieus as $courseLieu)
            <tr>
                <td>{{ $courseLieu->course_id }}</td>
            <td>{{ $courseLieu->lieu_course_id }}</td>
                <td>
                    {!! Form::open(['route' => ['courseLieus.destroy', $courseLieu->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('courseLieus.show', [$courseLieu->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('courseLieus.edit', [$courseLieu->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>