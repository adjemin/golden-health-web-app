<!-- Course Id Field -->
<div class="form-group">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{{ $courseLieu->course_id }}</p>
</div>

<!-- Lieu Course Id Field -->
<div class="form-group">
    {!! Form::label('lieu_course_id', 'Lieu Course Id:') !!}
    <p>{{ $courseLieu->lieu_course_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $courseLieu->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $courseLieu->updated_at }}</p>
</div>

