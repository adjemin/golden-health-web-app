<div class="table-responsive-sm">
    <table class="table table-striped" id="qualificationTypes-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Slug</th>
        <th>Abreviation</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($qualificationTypes as $qualificationType)
            <tr>
                <td>{{ $qualificationType->name }}</td>
            <td>{{ $qualificationType->slug }}</td>
            <td>{{ $qualificationType->abreviation }}</td>
                <td>
                    {!! Form::open(['route' => ['qualificationTypes.destroy', $qualificationType->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('qualificationTypes.show', [$qualificationType->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('qualificationTypes.edit', [$qualificationType->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>