<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $qualificationType->name }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{{ $qualificationType->slug }}</p>
</div>

<!-- Abreviation Field -->
<div class="form-group">
    {!! Form::label('abreviation', 'Abreviation:') !!}
    <p>{{ $qualificationType->abreviation }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $qualificationType->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $qualificationType->updated_at }}</p>
</div>

