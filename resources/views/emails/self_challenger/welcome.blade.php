<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta content="width=device-width" name="viewport" />
    <!--[if !mso]><!-->
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('customer/img/brand.png') }}">
    {{-- Tui --}}
    <link type="text/css" href="https://uicdn.toast.com/tui-color-picker/v2.2.6/tui-color-picker.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('dist/tui-image-editor.css') }}" rel="stylesheet">
    {{-- Tui end --}}

    <title>Confirmation d’inscription</title>

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    {{--
    <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}
    {{--
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    {{-- <link rel="stylesheet" href="{{ asset('customer/css/responsive.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('customer/css/akwaba.css') }}">
    <style>
    :root {

        --main: #1c6a49;
        --secondary: #edaa0d;
        --secondary-dark: #cf940a;
    }

    html,
    body,
    .body {
        height: 100% !important;
        background-color: #ececec;
    }

    .body {
        max-width: 500px;
        text-align: left !important;
        margin: auto;
    }

    .text-primary {
        color: #1c6a49;
    }

    .btn {
        text-decoration: none;
        padding: 10px;
        background-color: #26a85a;
        border-radius: 3px;
        color: white;
    }

    .btn:hover {
        background-color: #31c96e
    }

    .card {
        position: absolute;
        top: 20%;
        transform: translate(0, -20%);
        max-width: 500px;
        background: white;
        padding: 20px;
        border-radius: 10px;
    }
    tr{
        border: none !important;
    }
    </style>
</head>

<body style="background: #ececec; padding: 50px;">
    <table class="body"  style="position: absolute;top: 10%;transform: translate(0, -10%);max-width: 500px;background: white;padding: 20px;border-radius: 10px;">
        <tbody class="card bg-white">
            <tr>
                <td>
                <h1 class="text-primary mt-2" style="color: #1c6a49;">
                    Bienvenue à Golden Health
                </h1>
                </td>
            </tr>
            <tr>
                <td>
                    <h2>
                        MERCI D'AVOIR CRÉÉ VOTRE COMPTE CLIENT SUR GOLDEN HEALTH.
                    </h2>
                </td>
            </tr>
            <tr>
                <td>
                    <!-- <a href=""
                        target="_blank" class="btn" style="text-decoration: none;padding: 10px;background-color: #26a85a;border-radius: 3px;color: white;">                Cliquez Ici pour Accepter</a> -->

                    <!--<p class="lead mt-3" style="margin-top:15px">
                        Bienvenue sur la plateforme Golden Health, votre requête a été prise en compte, nous vous contacterons dans un délai de 48H.
                    </p> -->
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>
