<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta content="width=device-width" name="viewport" />
    <!--[if !mso]><!-->
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('customer/img/brand.png') }}">
    {{-- Tui --}}
    <link type="text/css" href="https://uicdn.toast.com/tui-color-picker/v2.2.6/tui-color-picker.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('dist/tui-image-editor.css') }}" rel="stylesheet">
    {{-- Tui end --}}

    <title>Note d'information</title>

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    {{--
    <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}
    {{--
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    {{-- <link rel="stylesheet" href="{{ asset('customer/css/responsive.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('customer/css/akwaba.css') }}">
    <style>
    :root {

        --main: #1c6a49;
        --secondary: #edaa0d;
        --secondary-dark: #cf940a;
    }

    html,
    body,
    .body {
        height: 100% !important;
        background-color: #ececec;
    }

    .body {
        max-width: 500px;
        text-align: left !important;
        margin: auto;
    }

    .text-primary {
        color: #1c6a49;
    }

    .btn {
        text-decoration: none;
        padding: 10px;
        background-color: #26a85a;
        border-radius: 3px;
        color: white;
    }

    .btn:hover {
        background-color: #31c96e
    }

    .card {
        position: absolute;
        top: 20%;
        transform: translate(0, -20%);
        max-width: 500px;
        background: white;
        padding: 20px;
        border-radius: 10px;
    }
    tr{
        border: none !important;
    }
    .button-action{
        box-sizing: border-box;
        font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';
        border-radius: 4px;
        color: #fff;
        display: inline-block;
        overflow: hidden;
        text-decoration: none;
        background-color: #2d3748;
        border-bottom: 8px solid #2d3748;
        border-left: 18px solid #2d3748;
        border-right: 18px solid #2d3748;
        border-top: 8px solid #2d3748;
    }
    </style>
</head>

<body style="background: #ececec; padding: 50px;">
    <table class="body"  style="position: absolute;top: 10%;transform: translate(0, -10%);max-width: 500px;background: white;padding: 20px;border-radius: 10px;">
        <tbody class="card bg-white">
            <tr>
                <td>
                <h1 class="text-primary mt-2" style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';color:#3d4852;font-size:18px;font-weight:bold;margin-top:0;text-align:left">
                    {{-- Nouvelle inscription {{$data['accountType']}} --}}
                    @if(isset($data['greeting']))
                        {!!$data['greeting']!!}
                    @endif
                </h1>
                </td>
            </tr>
            <tr>
                <td>
                    @if(isset($data['body']))
                        {!! $data['body'] !!}
                    @endif
                </td>

            </tr>
            <tr>
                <div style="margin: 15px 0; text-align:center;">
                    @if(isset($data['action']))
                        <a  style="box-sizing: border-box;
                            font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';
                            border-radius: 4px;
                            color: #fff;
                            display: inline-block;
                            overflow: hidden;
                            text-decoration: none;
                            background-color: #2d3748;
                            border-bottom: 8px solid #2d3748;
                            border-left: 18px solid #2d3748;
                            border-right: 18px solid #2d3748;
                            border-top: 8px solid #2d3748;"
                        href="{{ $data['action']['url'] }}"
                        rel="noopener" target="_blank">
                            {{ $data['action']['message'] }}
                        </a>
                    @endif
                <div>
            </tr>
            <tr>
                <td>
                    @if(isset($data['thanks']))
                        <p style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:16px;line-height:1.5em;margin-top:0;text-align:left">
                            {!! $data['thanks'] !!}
                        </p>
                    @endif
                </td>
            </tr>
            <tr>
                <td>
                   <p style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:16px;line-height:1.5em;margin-top:0;text-align:left">
                    Regards,<br/>
                    Goldenhealth
                    </p>
                </td>
            </tr>

        </tbody>
        <tfoot>
            <tr>
                <td>
                    <p style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';line-height:1.5em;margin-top:0;text-align:left;font-size:14px">

                        Si vous rencontrez des difficultés en cliquant sur le bouton "{{ $data['action']['message'] }}", copiez et collez l'URL ci-dessous dans votre navigateur Web: {{ $data['action']['url'] }}
                    </p>
                </td>
            </tr>
        </tfoot>
    </table>

</body>

</html>
