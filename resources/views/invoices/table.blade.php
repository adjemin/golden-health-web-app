<div class="table-responsive-sm">
    <table class="table table-striped" id="invoices-table">
        <thead>
            <tr>
                <th>Order Id</th>
        <th>Customer Id</th>
        <th>Reference</th>
        <th>Service</th>
        <th>Link</th>
        <th>Subtotal</th>
        <th>Tax</th>
        <th>Fees Delivery</th>
        <th>Total</th>
        <th>Status</th>
        <th>Is Paid By Customer</th>
        <th>Is Paid By Delivery Service</th>
        <th>Currency Code</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($invoices as $invoice)
            <tr>
                <td>{{ $invoice->order_id }}</td>
            <td>{{ $invoice->customer_id }}</td>
            <td>{{ $invoice->reference }}</td>
            <td>{{ $invoice->service }}</td>
            <td>{{ $invoice->link }}</td>
            <td>{{ $invoice->subtotal }}</td>
            <td>{{ $invoice->tax }}</td>
            <td>{{ $invoice->fees_delivery }}</td>
            <td>{{ $invoice->total }}</td>
            <td>{{ $invoice->status }}</td>
            <td>{{ $invoice->is_paid_by_customer }}</td>
            <td>{{ $invoice->is_paid_by_delivery_service }}</td>
            <td>{{ $invoice->currency_code }}</td>
                <td>
                    {!! Form::open(['route' => ['invoices.destroy', $invoice->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('invoices.show', [$invoice->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('invoices.edit', [$invoice->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>