<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{{ $invoice->order_id }}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{{ $invoice->customer_id }}</p>
</div>

<!-- Reference Field -->
<div class="form-group">
    {!! Form::label('reference', 'Reference:') !!}
    <p>{{ $invoice->reference }}</p>
</div>

<!-- Service Field -->
<div class="form-group">
    {!! Form::label('service', 'Service:') !!}
    <p>{{ $invoice->service }}</p>
</div>

<!-- Link Field -->
<div class="form-group">
    {!! Form::label('link', 'Link:') !!}
    <p>{{ $invoice->link }}</p>
</div>

<!-- Subtotal Field -->
<div class="form-group">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    <p>{{ $invoice->subtotal }}</p>
</div>

<!-- Tax Field -->
<div class="form-group">
    {!! Form::label('tax', 'Tax:') !!}
    <p>{{ $invoice->tax }}</p>
</div>

<!-- Fees Delivery Field -->
<div class="form-group">
    {!! Form::label('fees_delivery', 'Fees Delivery:') !!}
    <p>{{ $invoice->fees_delivery }}</p>
</div>

<!-- Total Field -->
<div class="form-group">
    {!! Form::label('total', 'Total:') !!}
    <p>{{ $invoice->total }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $invoice->status }}</p>
</div>

<!-- Is Paid By Customer Field -->
<div class="form-group">
    {!! Form::label('is_paid_by_customer', 'Is Paid By Customer:') !!}
    <p>{{ $invoice->is_paid_by_customer }}</p>
</div>

<!-- Is Paid By Delivery Service Field -->
<div class="form-group">
    {!! Form::label('is_paid_by_delivery_service', 'Is Paid By Delivery Service:') !!}
    <p>{{ $invoice->is_paid_by_delivery_service }}</p>
</div>

<!-- Currency Code Field -->
<div class="form-group">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    <p>{{ $invoice->currency_code }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $invoice->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $invoice->updated_at }}</p>
</div>

