<!-- Course Id Field -->
<div class="form-group">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{{ $courseObjective->course_id }}</p>
</div>

<!-- Objective Id Field -->
<div class="form-group">
    {!! Form::label('objective_id', 'Objective Id:') !!}
    <p>{{ $courseObjective->objective_id }}</p>
</div>
<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $courseObjective->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $courseObjective->updated_at }}</p>
</div>
