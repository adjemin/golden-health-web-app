<div class="table-responsive-sm">
    <table class="table table-striped" id="courseObjectives-table">
      <thead>
          <tr>
              <th>Course Id</th>
              <th>Objective Id</th>
              <th colspan="3">Action</th>
          </tr>
      </thead>
      <tbody>
      @foreach($courseObjectives as $courseObjective)
          <tr>
          <td>{{ $courseObjective->course_id }}</td>
          <td>{{ $courseObjective->objective_id }}</td>
          <td>
              {!! Form::open(['route' => ['courseObjectives.destroy', $courseObjective->id], 'method' => 'delete']) !!}
              <div class='btn-group'>
                  <a href="{{ route('courseObjectives.show', [$courseObjective->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                  <a href="{{ route('courseObjectives.edit', [$courseObjective->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                  {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
              </div>
              {!! Form::close() !!}
          </td>
      </tr>
      @endforeach
      </tbody>
    </table>
</div>
