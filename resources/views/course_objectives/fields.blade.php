
<!-- Course Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_id', 'Course Id:') !!}
    {!! Form::select('course_id', $courseItems, null, ['class' => 'form-control']) !!}
</div>

<!-- objective Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('objective_id', 'Objective Id:') !!}
    {!! Form::select('objective_id', $objectiveItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('courseObjectives.index') }}" class="btn btn-secondary">Cancel</a>
</div>
