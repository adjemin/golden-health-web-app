<div class="row">
    <!-- Customer Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('customer_id', 'Client:') !!}
        <p>{{ $review->customer->name ??  "---" }}</p>
    </div>

    <!-- Rating Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('rating', 'Note:') !!}
        <p>{{ $review->rating }}</p>
    </div>
</div>

<div class="row">
    <!-- Title Field -->
    {{--<div class="form-group col-sm-6">
        {!! Form::label('title', 'Titre:') !!}
        <p>{{ $review->title }}</p>
    </div>--}}

    <!-- Content Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('content', 'Contenu:') !!}
        <p>{{ $review->content }}</p>
    </div>
</div>

<div class="row">
    <!-- Service Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('service', 'Service:') !!}
        <p>{{ $review->service }}</p>
    </div>

    <!-- Source Field -->
    <div class="form-group">
        {!! Form::label('source', 'Source:') !!}
        @if($review->source == "product")
            <p><a href="{{ url('/products/'.$review->product->id) }}">{{ $review->product->title }}</a></p>
        @else
            <p><a href="{{url('/coach_profil/'.base64_encode($review->course->id))}}">Coach {{ $review->course->discipline->name ?? '' }}</a></p>
        @endif
    </div>
</div>

{{--
<!-- Souce Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('source_id', 'Souce Id:') !!}
    <p>{{ $review->source_id }}</p>
</div>
--}}

<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $review->created_at }}</p>
</div>

{{--
<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $review->updated_at }}</p>
</div>
--}}