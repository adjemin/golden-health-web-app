{{--
@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('reviews.index') }}">Review</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('reviews.index') }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('reviews.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
--}}

@extends('admin_backoffice.layouts.master')

@section('title')
    Avis clients | {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Avis clients</h4>
                            <p class="card-category"> Detail de l'avis du client</p>
                        </div>
                        <div class="card-body">
                            @include('reviews.show_fields')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection