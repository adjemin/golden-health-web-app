<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Title En Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title_en', 'Title En:') !!}
    {!! Form::text('title_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtitle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subtitle', 'Subtitle:') !!}
    {!! Form::text('subtitle', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtitle En Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subtitle_en', 'Subtitle En:') !!}
    {!! Form::text('subtitle_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Action Field -->
<div class="form-group col-sm-6">
    {!! Form::label('action', 'Action:') !!}
    {!! Form::text('action', null, ['class' => 'form-control']) !!}
</div>

<!-- Action By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('action_by', 'Action By:') !!}
    {!! Form::text('action_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Meta Data Field -->
<div class="form-group col-sm-6">
    {!! Form::label('meta_data', 'Meta Data:') !!}
    {!! Form::text('meta_data', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Notification Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_notification', 'Type Notification:') !!}
    {!! Form::text('type_notification', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Read Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_read', 'Is Read:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_read', 0) !!}
        {!! Form::checkbox('is_read', '1', null) !!}
    </label>
</div>


<!-- Is Received Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_received', 'Is Received:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_received', 0) !!}
        {!! Form::checkbox('is_received', '1', null) !!}
    </label>
</div>


<!-- Data Field -->
<div class="form-group col-sm-6">
    {!! Form::label('data', 'Data:') !!}
    {!! Form::text('data', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Data Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('data_id', 'Data Id:') !!}
    {!! Form::text('data_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('customerNotifications.index') }}" class="btn btn-secondary">Cancel</a>
</div>
