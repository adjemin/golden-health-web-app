<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $customerNotification->title }}</p>
</div>

<!-- Title En Field -->
<div class="form-group">
    {!! Form::label('title_en', 'Title En:') !!}
    <p>{{ $customerNotification->title_en }}</p>
</div>

<!-- Subtitle Field -->
<div class="form-group">
    {!! Form::label('subtitle', 'Subtitle:') !!}
    <p>{{ $customerNotification->subtitle }}</p>
</div>

<!-- Subtitle En Field -->
<div class="form-group">
    {!! Form::label('subtitle_en', 'Subtitle En:') !!}
    <p>{{ $customerNotification->subtitle_en }}</p>
</div>

<!-- Action Field -->
<div class="form-group">
    {!! Form::label('action', 'Action:') !!}
    <p>{{ $customerNotification->action }}</p>
</div>

<!-- Action By Field -->
<div class="form-group">
    {!! Form::label('action_by', 'Action By:') !!}
    <p>{{ $customerNotification->action_by }}</p>
</div>

<!-- Meta Data Field -->
<div class="form-group">
    {!! Form::label('meta_data', 'Meta Data:') !!}
    <p>{{ $customerNotification->meta_data }}</p>
</div>

<!-- Type Notification Field -->
<div class="form-group">
    {!! Form::label('type_notification', 'Type Notification:') !!}
    <p>{{ $customerNotification->type_notification }}</p>
</div>

<!-- Is Read Field -->
<div class="form-group">
    {!! Form::label('is_read', 'Is Read:') !!}
    <p>{{ $customerNotification->is_read }}</p>
</div>

<!-- Is Received Field -->
<div class="form-group">
    {!! Form::label('is_received', 'Is Received:') !!}
    <p>{{ $customerNotification->is_received }}</p>
</div>

<!-- Data Field -->
<div class="form-group">
    {!! Form::label('data', 'Data:') !!}
    <p>{{ $customerNotification->data }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $customerNotification->user_id }}</p>
</div>

<!-- Data Id Field -->
<div class="form-group">
    {!! Form::label('data_id', 'Data Id:') !!}
    <p>{{ $customerNotification->data_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $customerNotification->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $customerNotification->updated_at }}</p>
</div>

