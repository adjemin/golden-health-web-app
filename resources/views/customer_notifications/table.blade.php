<div class="table-responsive-sm">
    <table class="table table-striped" id="customerNotifications-table">
        <thead>
            <tr>
                <th>Title</th>
        <th>Title En</th>
        <th>Subtitle</th>
        <th>Subtitle En</th>
        <th>Action</th>
        <th>Action By</th>
        <th>Meta Data</th>
        <th>Type Notification</th>
        <th>Is Read</th>
        <th>Is Received</th>
        <th>Data</th>
        <th>User Id</th>
        <th>Data Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($customerNotifications as $customerNotification)
            <tr>
                <td>{{ $customerNotification->title }}</td>
            <td>{{ $customerNotification->title_en }}</td>
            <td>{{ $customerNotification->subtitle }}</td>
            <td>{{ $customerNotification->subtitle_en }}</td>
            <td>{{ $customerNotification->action }}</td>
            <td>{{ $customerNotification->action_by }}</td>
            <td>{{ $customerNotification->meta_data }}</td>
            <td>{{ $customerNotification->type_notification }}</td>
            <td>{{ $customerNotification->is_read }}</td>
            <td>{{ $customerNotification->is_received }}</td>
            <td>{{ $customerNotification->data }}</td>
            <td>{{ $customerNotification->user_id }}</td>
            <td>{{ $customerNotification->data_id }}</td>
                <td>
                    {!! Form::open(['route' => ['customerNotifications.destroy', $customerNotification->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('customerNotifications.show', [$customerNotification->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('customerNotifications.edit', [$customerNotification->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>