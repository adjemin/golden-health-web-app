<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CustomerFrontOfficeController@index');
Route::get('/commentaires/{slug}', 'CustomerFrontOfficeController@getReviews');

/***** Events****/
Route::get('/all_events', 'CustomerFrontOfficeController@allEvent')->name('events.all');
Route::get('/show_event/{slug}', 'CustomerFrontOfficeController@showEvent');

Route::post('/ajax/event/participate', 'AjaxController@eventParticipate')->name('ajax.event.participate');
Route::post('/ajax/event/prepareCheckout', 'AjaxController@eventPrepareCheckout')->name('ajax.event.prepareCheckout');

Route::get('/ajax/event/{invoiceId}/{eventId}/checkout/{seatsCount}', 'AjaxController@eventShowCheckout')->name('ajax.event.showCheckout');

Route::post('/ajax/event/list', 'AjaxController@eventList')->name('ajax.event.list');
Route::post('/ajax/event/search', 'AjaxController@eventSearch')->name('ajax.event.search');
Route::post('/ajax/event/participate/cancel', 'AjaxController@eventParticipateCancel')->name('ajax.event.participate.cancel');
/***** Cart *****/
Route::post('/ajax/cart/add', 'AjaxController@CartAdd')->name('ajax.cart.add');
Route::post('/ajax/cart/addDetail', 'AjaxController@CartAddDetail')->name('ajax.cart.addDetail');
Route::post('/ajax/cart/addSize', 'AjaxController@CartAddSize')->name('ajax.cart.addSize');
Route::post('/ajax/cart/update', 'AjaxController@cartUpdate')->name('ajax.cart.update');

Route::post('/ajax/cart/unique/manage', 'AjaxController@cartManageUnique')->name('ajax.cart.manageUnique');

// Notification result pages
Route::get('/customer/shop/checkout_finish/{transId}', 'AjaxController@shopCheckoutFinish');
Route::get('/customer/event/checkout_finish/{transId}', 'AjaxController@eventCheckoutFinish');

// Adding coupon code
Route::post('/ajax/coupon/add', 'AjaxController@couponAdd')->name('ajax.coupon.add');
Route::post('/ajax/coupon/add/storage', 'AjaxController@couponAddStorage')->name('ajax.coupon.addStorage');


// Route::get('/customer/shop/checkout/{invoiceId}', 'AjaxController@shopCheckout');
Route::get('/customer/shop/checkout/{invoiceId}', 'AjaxController@shopCheckout');

Route::post('/ajax/cart/manage', 'AjaxController@cartManage')->name('ajax.cart.manage');

Route::get('/ajax/cart/remove/{id}', 'AjaxController@cartRemove')->name('ajax.cart.remove');
Route::get('/ajax/cart/increment/{id}', 'AjaxController@cartIncrement')->name('ajax.cart.increment');
Route::get('/ajax/cart/decrement/{id}', 'AjaxController@cartDecrement')->name('ajax.cart.decrement');

Route::post('/ajax/cart/clear', 'AjaxController@cartClear')->name('ajax.cart.clear');
Route::post('/ajax/shop/createOrder', 'AjaxController@createOrder')->name('ajax.shop.createOrder');
Route::post('/ajax/shop/createOrderOnly', 'AjaxController@createOrderOnly')->name('ajax.shop.createOrderOnly');


// Adjeminpay from Invoice
// the order is already created, the invoice too,
// the controller creates the invoicepayment then sends it to the payment gate with the desired return route
Route::post('/payment/fromInvoice', 'PaymentController@fromInvoice')->name('payment.from.invoice');

// Moved to api
// Route::post('ajax/payment/notify', 'InvoicePaymentAPIController@notify')->name('ajax.payment.notify'); // adjemipay notify

// /***** Events****/
// Route::post('/payments/notify', 'InvoicePaymentAPIController@notify'); // adjemipay notify

// Route::post('/payments/cinet_pay/notify', 'InvoicePaymentAPIController@notifyCinetPay');
// Cart components
// Route::group(['prefix' => '/ajax/x/s/c/'], function () {
//     Route::get('/cart_quantity','AjaxController@CartComponentQuantity');
// });

/*****Shop Basket****/
Route::get('/basket', 'CustomerFrontOfficeController@showBasket')->name('shop.basket');
Route::get('/basket/add/{prodId}', 'CustomerFrontOfficeController@addBasket')->name('basket.add');

/*****Shop Checkout****/
Route::get('/checkout', 'CustomerFrontOfficeController@showCheckout')->name('shop.checkout');

Route::get('/product_details/{slug}', 'CustomerFrontOfficeController@productDetailsShop')->name('shop.product_details');
Route::get('/shop', 'CustomerFrontOfficeController@indexShop')->name('shop');
Route::get('/filter/shop', 'CustomerFrontOfficeController@filterShop');

/***** Reserver ****/
Route::get('/reservation', 'CustomerFrontOfficeController@reservation')->name('reservation');

Route::get('/become_coach', 'CustomerFrontOfficeController@beCoach')->name('become.coach');
Route::post('/register/become_coach', 'CustomerFrontOfficeController@beCoach_register');


Route::get('/become_partner', 'CustomerFrontOfficeController@bePartner')->name('become.partner');
Route::post('/register/partner', 'CustomerFrontOfficeController@RegisterPartner');
// Route::get('/coachs', 'CustomerFrontOfficeController@allCoach')->name('coachs');
Route::get('/search_coach', 'CustomerFrontOfficeController@allCoach')->name('coachs');
Route::get('/coach_profil/{slug}', 'CustomerFrontOfficeController@showCoachProfil')->name('coach_profil');
Route::get('/course/reserver/{id}', 'CustomerFrontOfficeController@showReservation');
Route::get('/course/{course_id}/rate/{id}', 'CustomerFrontOfficeController@showReservation');
Route::post('/booking/course', 'CustomerFrontOfficeController@cartReservation');

Route::get('/final/checkout', 'CustomerFrontOfficeController@FinalBooking');

Route::get('/dashboard', 'CustomerFrontOfficeController@showDashboard')->name('dashboard');
Route::get('/connexion', 'CustomerFrontOfficeController@showConnexion')->name('connexion');
Route::get('/inscription', 'CustomerFrontOfficeController@showInscription')->name('inscription');
Route::get('/blog', 'CustomerFrontOfficeController@showBlog')->name('blog');
Route::get('/blog-article/{slug}', 'CustomerFrontOfficeController@showBlogArticle')->name('blog-article');
Route::get('/self-challenger', 'CustomerFrontOfficeController@showSelfChallenger')->name('self-challenger');
Route::get('/coach-description', 'CustomerFrontOfficeController@showCoachDescription')->name('coach-description');
Route::get('/programmes', 'CustomerFrontOfficeController@showProgrammes')->name('programmes');

Route::get('/programme/{slug}', 'CustomerFrontOfficeController@showProgramme')->name('programme');

Route::get('/qui-sommes-nous', 'CustomerFrontOfficeController@showQuiSommesNous')->name('qui-sommes-nous');
Route::get('/contact', 'CustomerFrontOfficeController@showContact')->name('contact');
// Route::post('/contact', 'CustomerFrontOfficeController@storeContact')->name('contact');
Route::post('/contact', 'ContactRequestController@store')->name('contact');
Route::get('/contacts', 'ContactRequestController@index')->name('contacts.index');
Route::get('/contacts/{id}', 'ContactRequestController@show')->name('contacts.show');
Route::delete('/contacts/{id}', 'ContactRequestController@destroy')->name('contacts.destroy');
Route::get('/faq', 'CustomerFrontOfficeController@showFaq')->name('faq');
Route::get('/register-course', 'CustomerFrontOfficeController@showRegisterCourse')->name('register-course');


// **** Terms, Conditions, Privacy Cookies Legal stuff
Route::group(['prefix' => 'legal'], function () {

    Route::get('/privacy', 'CustomerFrontOfficeController@legalPrivacy')->name('privacy');
    Route::get('/terms', 'CustomerFrontOfficeController@legalTerms')->name('terms');
});



Route::get('/filter/coachs', 'CustomerFrontOfficeController@filterCoach');

 // OAuth Routes
 Route::get('auth/{provider}', 'CustomerAuth\LoginController@redirectToProvider');
 Route::get('auth/{provider}/callback', 'CustomerAuth\LoginController@handleProviderCallback');

//  Route::get('auth/google', 'CustomerAuth\LoginController@redirectToGoogle')->name("login.google");
//  Route::get('auth/google/callback', 'CustomerAuth\LoginController@handleGoogleCallback');

/////////////// BEGIN ROUTE HOME PAGE ///////////////

/**** AUTH CUSTOMER *****/
Route::group( ['prefix' => 'customer'], function (){
    Route::get('/login', 'CustomerAuth\LoginController@showLoginForm')->name('customer.login');
    Route::post('/login', 'CustomerAuth\LoginController@login');
    Route::post('/logout', 'CustomerAuth\LoginController@logout')->name('customer.logout');

    Route::get('/register', 'CustomerAuth\RegisterController@showRegistrationForm')->name('customer.register');
    Route::post('/register', 'CustomerAuth\RegisterController@register');
    Route::get('/confirm-challenger-email', 'CustomerAuth\RegisterController@confirmChallengerEmail');


    Route::get('/register/self-challenger', 'CustomerFrontOfficeController@registerFormSelfChallenger')->name('customer.register.self-challenger');
    Route::get('/register/coach', 'CustomerFrontOfficeController@registerFormCoach')->name('customer.register.coach');
    Route::get('/register/partner', 'CustomerFrontOfficeController@registerFormPartner')->name('customer.register.partner');

    Route::post('/password/email', 'CustomerAuth\ForgotPasswordController@sendResetLinkEmail')->name('customer_password.request');
    Route::post('/password/reset', 'CustomerAuth\ResetPasswordController@reset')->name('customer_password.email');
    Route::get('/password/reset', 'CustomerAuth\ForgotPasswordController@showLinkRequestForm')->name('customer_password.reset');
    Route::get('/password/reset/{token}', 'CustomerAuth\ResetPasswordController@showResetForm')->name('customer_showResetForm');

    Route::get('/redirectTo', 'CustomerFrontOfficeController@loginAndRedirectTo')->name('customer.redirectTo');

    /*Route::get('/confirmation/{customer}/{token}', 'CustomerAuth\ConfirmationController@store')->name('confirmation');*/

    Route::get('/dashboard', 'CustomerDashboard\DashboardController@showDashboard')->name('customer.dashboard');
    Route::get('/manage_profil', 'CustomerDashboard\DashboardController@showManagePofil')->name('customer.manage_profil');

    Route::get('/coach/diplome', 'CustomerDashboard\DashboardController@getDiplome')->name('coach.diplome');
    Route::get('/coach/portfolio', 'CustomerDashboard\DashboardController@getPortfolio')->name('coach.portfolio');
    Route::post('/coach/diplome/save', 'CustomerDashboard\DashboardController@UploadDiplome');
    Route::post('/coach/portfolio/save', 'CustomerDashboard\DashboardController@UploadPortfolio');
    Route::post('/coach/diplome/delete/{id}', 'CustomerDashboard\DashboardController@destroyDiplome');
    Route::post('/coach/portfolio/delete/{id}', 'CustomerDashboard\DashboardController@destroyPortfolio');

    /***** Profile ****/
    Route::get('/profile', 'CustomerDashboard\DashboardController@showManagePofil')->name('customer.profile');
    Route::get('/profile/settings', 'CustomerDashboard\DashboardController@settingManagePofil')->name('customer.profile.settings');
    Route::post('/update/profile', 'CustomerDashboard\DashboardController@managePofil');
    Route::post('/update/avatar', 'CustomerDashboard\DashboardController@updatePofileAvatar');
    Route::post('/update/settings', 'CustomerDashboard\DashboardController@updateSettingProfile');

    Route::post('/calcul/imc', 'CustomerDashboard\DashboardController@calculCustomerImc');


    /**** COACH MANAGE ****/
    Route::get('/coach/lessons', 'CustomerDashboard\DashboardController@CoachLesson')->name('coach.lessons');
    Route::get('/lessons/manage', 'CustomerDashboard\DashboardController@CoachLessonManage');
    Route::get('/lessons/manage/show/{id}', 'CustomerDashboard\DashboardController@CoachLessonManageShow');
    Route::delete('/lessons/delete/{id}', 'CustomerDashboard\DashboardController@CoachLessonDelete')->name('courseCoaoch.destroy');
    ////// STORE COURSE CART
    Route::post('/lessons/store/cart', 'CustomerDashboard\DashboardController@LessonStoreCart');
    Route::post('/lessons/save', 'CustomerDashboard\DashboardController@CoachLessonSave');

    Route::get('/coach/calendar', 'CustomerDashboard\DashboardController@CoachCalendar')->name('coach.calendar');
    Route::post('/coach/calendar', 'CustomerDashboard\DashboardController@CoachCalendarPost')->name('coach.calendar.post');

    /******** BOOKING COACH **************/
    Route::post('/save/booking', 'CustomerDashboard\DashboardController@saveBookingCoach')->name('saveBooking');

    /*************** BOOKING PAYMENT **************/
    Route::post('/booking/payment', 'CustomerDashboard\DashboardController@GoPaymentHandler')->name('booking.payment');

    Route::get('/page/payment', 'CustomerDashboard\DashboardController@PagePay');

    Route::get('/go/payment', 'CustomerDashboard\DashboardController@PagePay');

    // Route::post('/payment/notify', 'CustomerDashboard\DashboardController@PayNotify');

    Route::get('viewinvoice/{id}', 'CustomerDashboard\DashboardController@getInvoice');

    ////// REVIEWBOOKING ////////////////
    Route::post('/save/review', 'CustomerDashboard\DashboardController@saveReview');

    Route::get('/tickets', 'CustomerDashboard\DashboardController@tickets');
    Route::get('/tickets/pdf/{id}', 'CustomerDashboard\DashboardController@ticketPdf');
    Route::get('/tickets/view/{id}', 'CustomerDashboard\DashboardController@ticketView');
    Route::get('/tickets/{id}', 'CustomerDashboard\DashboardController@ticket');
    Route::get('/orders/shop', 'CustomerDashboard\DashboardController@shopOrders');
    Route::get('/orders/shop/{orderId}', 'CustomerDashboard\DashboardController@shopOrder');
    Route::get('/notifications', 'CustomerDashboard\DashboardController@notifications');
    Route::get('/coupons', 'CustomerDashboard\DashboardController@coupons');
    Route::get('/packs', 'CustomerDashboard\DashboardController@packs');
    Route::get('/coaches', 'CustomerDashboard\DashboardController@coaches');

    // Liste à garder pour plus tard
    Route::get('/wishlist/toggle/{id}', 'CustomerDashboard\DashboardController@wishlistToggle')->name('wishlist.toggle');
    Route::get('/wishlist', 'CustomerDashboard\DashboardController@showWishlist')->name('wishlist.index');
    // Customer reviews
    Route::get('/reviews', 'CustomerDashboard\DashboardController@showReviews')->name('reviews.index');
    Route::get('/reviews/create/{orderId}/{productId}', 'CustomerDashboard\DashboardController@createReviews')->name('customer.reviews.create');
    Route::get('/reviews/course/{courseId}', 'CustomerDashboard\DashboardController@createReviewsCourse')->name('customer.reviews.create_course');
    Route::post('/reviews/store', 'CustomerDashboard\DashboardController@storeReviews')->name('customer.reviews.store');
    Route::post('/reviews/store_course', 'CustomerDashboard\DashboardController@storeReviewsCourse')->name('customer.reviews.store_course');

});

// ** ** Disciplines
Route::get('/customer/disciplines', 'CustomerFrontOfficeController@disciplines');

/////////////// END ROUTE HOME PAGE ///////////////

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes([
    'verify' => true,
    'register'  => false
]);

// Route::get('/home', 'HomeController@index')->middleware('verified');
Route::get('/analytique', 'AnalytiqueController@index');

Route::get('/partner/download/all', 'PartnerController@printAll');
Route::get('/customers/download/all', 'CustomerController@printAll');
Route::get('/product/download/all', 'ProductController@printAll');
Route::get('/event/download/all', 'EventController@printAll');
Route::get('/order/download/all', 'OrderController@printAll');
Route::get('/coupon/download/all', 'CouponController@printAll');
Route::get('/booking/download/all', 'BookingCoachController@printAll');
Route::get('/contact/download/all', 'ContactRequestController@printAll');
Route::get('/coach/download/all', 'CoachController@printAll');


Route::resource('coaches', 'CoachController');
Route::post('find/coach', 'CoachController@searchCoach')->name('searchCoach');
Route::post('find/product', 'ProductController@searchProduct')->name('searchProduct');
Route::post('find/partner', 'PartnerController@searchPartner')->name('searchPartner');
Route::post('find/event', 'EventController@searchEvent')->name('searchEvent');
Route::post('find/order', 'OrderController@searchOrder')->name('searchOrder');
Route::post('find/bookingCoach', 'BookingCoachController@searchBookingCoach')->name('searchBookingCoach');
Route::resource('product-reservation', 'ProductReservationController');

Route::resource('customers', 'CustomerController');
Route::post('find/customer', 'CustomerController@searchCustomer')->name('searchCustomer');

Route::resource('languages', 'LanguageController');

Route::resource('availabilities', 'AvailabilityController');

Route::resource('disciplines', 'DisciplineController');

Route::resource('coachDisciplines', 'CoachDisciplineController');

Route::resource('packs', 'PackController');

Route::resource('packSubscriptions', 'PackSubscriptionController');

Route::resource('portfolios', 'PortfolioController');

Route::resource('events', 'EventController');

Route::resource('orders', 'OrderController');
Route::post('/order/espece/{id}', 'OrderController@espece')->name('orders.espece');
Route::post('/order/isPaid/{id}', 'OrderController@changeStatus')->name('orders.isPaid');
Route::post('/order/changeStatus/{id}', 'OrderController@changeStatus')->name('orders.changeStatus');
Route::post('/customer/changeStatus/{id}', 'CustomerController@changeStatus')->name('customer.changeStatus');
Route::post('/coach/changeStatus/{id}', 'CoachController@changeStatus')->name('coach.changeStatus');

Route::resource('bodyMassIndices', 'BodyMassIndexController');

Route::resource('recapBMIs', 'RecapBMIController');

Route::resource('recapBMIs', 'RecapBMIController');

Route::resource('addresses', 'AddressController');

Route::resource('coupons', 'CouponController');

Route::resource('invoices', 'InvoiceController');

Route::resource('courses', 'CourseController');
Route::post('/course/changeStatus/{id}', 'CourseController@changeStatus')->name('course.changeStatus');
Route::get('/sendMail', 'HomeController@sendMail')->name('sendMail');
Route::post('/sendMail', 'HomeController@sendMailPost')->name('sendMailPost');
Route::get('viewinvoice/{id}', 'OrderController@invoiceFacture');

Route::resource('services', 'ServiceController');

Route::resource('products', 'ProductController');
Route::post('/product/stock/{id}', 'ProductController@stock')->name('stock_update');

Route::resource('experiences', 'ExperienceController');

Route::resource('qualifications', 'QualificationController');

Route::resource('qualificationTypes', 'QualificationTypeController');

Route::resource('invoicePayments', 'InvoicePaymentController');

Route::resource('invoicePayments', 'InvoicePaymentController');

Route::resource('notes', 'NoteController');

Route::resource('advice', 'AdviceController');

Route::resource('objectives', 'ObjectiveController');

Route::resource('seances', 'SeanceController');


Route::resource('howDidYouKnows', 'HowDidYouKnowController');

Route::resource('partners', 'PartnerController');


Route::resource('connexions', 'ConnexionController');


Route::resource('categories', 'CategoryController');

Route::resource('posts', 'PostController');

Route::resource('orderItems', 'OrderItemController');


Route::resource('courseDetails', 'courseDetailsController');

Route::resource('courseLieus', 'courseLieuController');

Route::resource('lieuCourses', 'LieuCourseController');

Route::resource('courseObjectives', 'CourseObjectiveController');

Route::resource('bookingCoaches', 'BookingCoachController');


Route::resource('productCategories', 'ProductCategoryController');

Route::resource('adverts', 'AdvertController');
Route::resource('users', 'UserController');


Route::get('/mail/test', function () {

    $user = [
        'name' => 'Mahedi Hasan',
        'info' => 'Laravel Developer'
    ];

    \Mail::to('jacques.bagui@gmail.com')->send(new \App\Mail\NewMail($user));

    dd("success");
});

Route::get('videotheque', 'CustomerFrontOfficeController@videothequeIndex');

Route::resource('distinctions', 'distinctionController');

Route::resource('videotheques', 'VideothequeController');





Route::resource('clientTestimonies', 'ClientTestimonyController');

Route::resource('programs', 'ProgramController');


Route::resource('slides', 'SlideController');
Route::resource('diplomes', 'DiplomeController');

Route::resource('customerDevices', 'CustomerDeviceController');

Route::resource('customerNotifications', 'CustomerNotificationController');
Route::post('resend-email', 'CustomerNotificationController@resendEmailSubcription')->name('resendEmailSubcription');





Route::resource('reviews', 'ReviewController');

Route::get('/fetchVisitorsAndPageViews', 'AnalytiqueController@fetchVisitorsAndPageViews');
Route::get('/fetchTotalVisitorsAndPageViews', 'AnalytiqueController@fetchTotalVisitorsAndPageViews');
Route::get('/fetchMostVisitedPages', 'AnalytiqueController@fetchMostVisitedPages');
Route::get('/fetchTimeOnPages', 'AnalytiqueController@fetchTimeOnPages');
Route::get('/fetchTopReferrers', 'AnalytiqueController@fetchTopReferrers');
Route::get('/fetchUserTypes', 'AnalytiqueController@fetchUserTypes');
Route::get('/fetchBrowsers', 'AnalytiqueController@fetchBrowsers');



Route::resource('villes', 'villeController');