<?php
use App\Models\Customer;
use App\Models\Coach;
use App\Models\CoachDiscipline;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::resource('coaches', 'CoachAPIController');

Route::resource('customers', 'CustomerAPIController');
Route::get('/reviewCourse/{course_id}', 'ReviewAPIController@reviewCourse');
Route::get('/reviewProduct/{product_id}', 'ReviewAPIController@reviewProduct');
Route::resource('customers', 'CustomerAPIController');
Route::post('customers/auth', 'CustomerAPIController@login');
Route::post('customers/auth_social', 'CustomerAPIController@loginWithSocial');
Route::post('customers/register', 'CustomerAPIController@store');


Route::resource('languages', 'LanguageAPIController');

Route::resource('availabilities', 'AvailabilityAPIController');

Route::resource('disciplines', 'DisciplineAPIController');

Route::resource('coach_disciplines', 'CoachDisciplineAPIController');

Route::resource('packs', 'PackAPIController');

Route::resource('pack_subscriptions', 'PackSubscriptionAPIController');

Route::resource('portfolios', 'PortfolioAPIController');

Route::resource('events', 'EventAPIController');
Route::get('find_events', 'EventAPIController@findEvents');
Route::post('participate_events', 'EventAPIController@participateEvent');
Route::get('customers/{id}/tickets', 'EventAPIController@findTicketsByCustomer');

Route::resource('orders', 'OrderAPIController');
Route::get('customers/{customerId}/product_orders', "OrderAPIController@findOrdersByCustomer");

Route::resource('body_mass_indices', 'BodyMassIndexAPIController');

Route::resource('recap_b_m_is', 'RecapBMIAPIController');

Route::resource('addresses', 'AddressAPIController');

Route::resource('coupons', 'CouponAPIController');

Route::resource('invoices', 'InvoiceAPIController');

Route::resource('courses', 'CourseAPIController');

Route::get('courses/{id}/coaches', 'CourseAPIController@findCoursesByCoach');


Route::resource('services', 'ServiceAPIController');

Route::resource('products', 'ProductAPIController');
Route::post('order_products', 'ProductAPIController@orderProduct');


Route::resource('experiences', 'ExperienceAPIController');

Route::resource('qualifications', 'QualificationAPIController');

Route::resource('qualification_types', 'QualificationTypeAPIController');

Route::resource('invoice_payments', 'InvoicePaymentAPIController');

Route::post('invoice_payments_notify', 'InvoicePaymentAPIController@notify')->name('payments.notify');

Route::get('invoice_payments_notify', 'InvoicePaymentAPIController@notify');
Route::get('customers/{customerId}/payments', 'InvoicePaymentAPIController@findPaymentsByUser');

Route::resource('notes', 'NoteAPIController');

Route::resource('advice', 'AdviceAPIController');

Route::resource('objectives', 'ObjectiveAPIController');

Route::resource('seances', 'SeanceAPIController');

Route::resource('how_did_you_knows', 'HowDidYouKnowAPIController');

Route::resource('partners', 'PartnerAPIController');

//Route::resource('connexion', 'ConnexionAPIController');

// Route::resource('disciplineIdCoach', 'DisciplineIdCoachAPIController');


Route::get('disciplines/{id}/coaches', function($id){

    $courses = \App\Models\Course::where(['discipline_id' => $id, 'active' => true])->get();

    return  $courses;
});

Route::get('coaches/find/courses_by_discipline', function(Request $request){

    $courses = \App\Models\Course::where([
        'discipline_id' => $request->discipline_id,
        'coach_id' => $request->coach_id
        ])->get();

    return  $courses;
});


Route::get('customers/{id}/coaches', function($id){

    $coachs  = DB::select(
        DB::raw(
            "SELECT customers.* from coaches JOIN customers on customers.id = coaches.id WHERE coaches.id=$id"
        )
    );


    // dd($coachs);

    return $coachs;
});

Route::get('coaches/{id}/portfolios', function($id){

    $portfolios  = DB::select(
        DB::raw(
            "SELECT portfolios.* from coaches JOIN portfolios on  coaches.id = portfolios.coach_id  WHERE portfolios.coach_id =$id"
        )
    );

    // dd($portfolios);

    return $portfolios;
});


Route::get('coaches/{id}/coach_caracteristiques', function($id){

    $coachCaracteristique  = DB::select(
        DB::raw(
           "SELECT coach_caracteristiques.* from coaches JOIN coach_caracteristiques on coaches.id = coach_caracteristiques.coach_id WHERE coach_caracteristiques.coach_id = $id"
        )
    );

    // dd($coachCaracteristique);

    return $coachCaracteristique;
});

Route::post('search_coach_by_location_and_discipline', 'CoachAPIController@searchCoachesByLocationAndDiscipline');
Route::get('search_coach_by_location_and_discipline', 'CoachAPIController@searchCoachesByLocationAndDiscipline');
Route::get('search_coach_by_location_and_discipline_v2', 'CoachAPIController@searchCoachesByLocationAndDisciplineV2');
Route::post('search_courses_by_filter', 'CoachAPIController@filter');


// Route::post('connexion', 'ConnexionAPIController@connexion');

// Route::post("/connexions")

Route::group( [
    'prefix' => 'auth'
], function () {
    Route::post('login', 'CustomerAuthAPIController@login');
    Route::post('signup', 'CustomerAuthAPIController@signup');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'CustomerAuthAPIController@logout');
        Route::get('user', 'CustomerAuthAPIController@user');
    });
});

Route::post('reservationSeance', 'SeanceAPIController@reservationSeance');
Route::post('ConfirmRejectSeance', 'SeanceAPIController@ConfirmRejectSeance');

Route::resource('coach_caracteristiques', 'CoachCaracteristiqueAPIController');

Route::resource('categories', 'CategoryAPIController');

Route::resource('posts', 'PostAPIController');
Route::get('feeds', 'PostAPIController@allFeeds');

Route::resource('order_items', 'OrderItemAPIController');

Route::resource('course_details', 'courseDetailsAPIController');

Route::resource('course_lieus', 'courseLieuAPIController');

Route::resource('lieu_courses', 'LieuCourseAPIController');

Route::resource('course_objectives', 'CourseObjectiveAPIController');

Route::resource('booking_coaches', 'BookingCoachAPIController');
Route::get('booking_coaches_by_coach/{coachId}', 'BookingCoachAPIController@findByCoach');
Route::get('booking_coaches_by_customer/{customerId}', 'BookingCoachAPIController@findByCustomer');

Route::post('booking_coaches_change_status', 'BookingCoachAPIController@changeStatus');

Route::resource('product_categories', 'ProductCategoryAPIController');

Route::resource('adverts', 'AdvertAPIController');

Route::resource('distinctions', 'distinctionAPIController');





Route::resource('client_testimonies', 'ClientTestimonyAPIController');

Route::resource('programs', 'ProgramAPIController');

Route::resource('diplomes', 'DiplomeAPIController');

Route::resource('product_categories', 'ProductCategoryAPIController');

Route::resource('customer_devices', 'CustomerDeviceAPIController');

Route::resource('customer_notifications', 'CustomerNotificationAPIController');





Route::resource('reviews', 'ReviewAPIController');


Route::resource('villes', 'villeAPIController');

Route::get('/statistique/{customerId}', 'CustomerAPIController@statistique');
Route::get('/praticTime/{coachId}', 'AvailabilityAPIController@praticTime');

Route::get('get_coach_for_customer/{customerId}', 'CustomerAPIController@coaches');
Route::get('get_diplome_for_coach/{coachId}', 'CustomerAPIController@diplomes');

Route::get('wishlist/{customerId}', 'CustomerAPIController@wishlist');
Route::get('customer/coupons/{customerId}', 'CustomerAPIController@coupons');
Route::get('showReviews/{customerId}', 'ReviewAPIController@showReviews');
Route::post('storeReviewsCourse', 'ReviewAPIController@storeReviewsCourse');
Route::post('storeReviews', 'ReviewAPIController@storeReviews');

Route::post("checkCoupon", "CouponAPIController@checkCoupon");