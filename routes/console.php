<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use App\Console\Commands\NotifyBirthday;
use App\Console\Commands\NotifyEveryThreeHour;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('notify:every_three_hour', function () {
    NotifyEveryThreeHour::exec();
})->describe('this command will notify the self-challenger or coachs 3 hour before the beginnig of a course');

Artisan::command('notify:every_birthday', function () {
    NotifyBirthday::exec();
})->describe('this command will notify the self-challenger or coachs every year');